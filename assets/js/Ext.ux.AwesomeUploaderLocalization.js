Ext.ns('Ext.ux');

Ext.ux.AwesomeUploaderLocalization = {
	english:{
		browseButtonText: 'Browse...' //Note this is only for the "standard" upload button. The flash multi-select uploader uses an image sprite
		,browseWindowFileDescription: 'All Files' 
		,uploaderAlertErrorTitle: 'Error uploading:'
		,uploaderAlertErrorPrefix: 'Upload Error'
		,fileSizeError: '<BR><b>File size exceeds allowed limit.</b><BR>'
		,fileSizeEventText: 'File size exceeds allowed limit.'
		,uploadAbortedMessage: '<BR><b>Upload Aborted</b><BR>'
		,uploadErrorMessage: 'Error uploading file:'
	}
};
