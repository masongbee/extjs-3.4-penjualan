Ext.namespace("Ext.ux.plugins");
Ext.ux.plugins.PanelCollapseHorizontal = Ext.extend(Ext.util.Observable, {
	horizontalHeader: null,
	collapsedHorizontal: false,
	constructor: function(config) {
		this.horizontalHeader = (config || {}).horizontalHeader;
		this.collapsedHorizontal = (config || {}).collapsedHorizontal;
	},
	init: function(panel) {
		if (panel.collapsible || panel.collapsed) {
			return;
		}
		if (!this.horizontalHeader) {
			var text = panel.title || '';
			var headerTxt = '';
			var length = text.length;
			var isTag = false;
			for (var j = 0; j < length; j++) {
				var charTxt = text.substring(j, j + 1);
				if (charTxt == '<') {
					isTag = true;
				}
				if (!isTag) {
					headerTxt += '<br/>' + charTxt;
				}
				if (isTag && charTxt == '>') {
					isTag = false;
				}

			}
			this.horizontalHeader = '<div style="text-align:center;overflow:hidden;zoom:1;color:#15428b;font:bold 11px tahoma,arial,verdana,sans-serif;line-height:15px;">' + headerTxt + '</div>';
		}
		Ext.apply(panel, {
			collapsibleHorizontal: true,
			originalOnRender: panel.onRender,
			slideAnchor: 'l',
			collapseEl: 'el',
			collapsedCls: 'x-panel-collapsed-horizontal x-panel-collapsed',
			collapsedHorizontal: this.collapsedHorizontal,
			horizontalHeader: this.horizontalHeader,
			onResize: panel.onResize.createSequence(function(w, h) {
				this.getCollapsedEl().setStyle('height', h - (Ext.isIE ? 0 : 2));
			}),
			onRender: function(ct, position) {
				this.targetEl = panel.collapserTarget || ct;
				this.tools = this.tools ? this.tools.slice(0) : [];
				this.tools.push({
					cls: 'x-tool-collapse-west',
					overCls: 'x-tool-collapse-west-over',
					id: 'collapse-west',
					handler : this.toggleCollapse,
					scope: this
				});
				this.on("afterlayout", function() {
					if (this.collapsedHorizontal) {
						this.collapsed = false;
						this.collapse(false);
						this.getCollapsedEl().setStyle('height', this.el.getSize(true).height);
						this.getCollapsedEl().setStyle('width', Ext.isIE ? 22 : 20);
						this.getCollapsedEl().alignTo(this.collapserAligner || this.el, this.collapserAlignDir || 'tl-tl');
					}
				}, this, {single: true});
				this.originalOnRender(ct, position);
			},
			getCollapsedEl : function() {
				if (!this.collapsedEl) {
					if (!this.toolTemplate) {
						var tt = new Ext.Template(
								'<div class="x-tool x-tool-{id}">&#160;</div>'
								);
						tt.disableFormats = true;
						tt.compile();
						Ext.Panel.prototype.toolTemplate = tt;
					}
					this.collapsedEl = this.targetEl.createChild({
						cls: "x-layout-collapsed",
						id: this.id + '-hr-collapsed',
						html: this.horizontalHeader
					});
					this.collapsedEl.enableDisplayMode('block');
					this.getCollapsedEl().setStyle('height', this.el.getSize(true).height - (Ext.isIE ? 0 : 2));
					this.getCollapsedEl().setStyle('width', Ext.isIE ? 22 : 20);
					this.getCollapsedEl().alignTo(this.collapserAligner || this.el, this.collapserAlignDir || 'tl-tl');
					var t = this.toolTemplate.insertFirst(
							this.collapsedEl.dom, {id:'expand-west'}, true);
					t.addClassOnOver('x-tool-expand-west-over');
					t.on('click', this.expand, this, {stopEvent:true});
				}
				return this.collapsedEl;
			},
			onCollapse: function() {
				this.getCollapsedEl().slideIn('l', {duration:.2});
				this.el.slideOut('l',
						Ext.apply(this.createEffect(true, this.afterCollapse, this),
								this.collapseDefaults));
				this.bwrap.hide();

			},
			onExpand : function() {
				this.bwrap.show();
				this.el.slideIn('l',
						Ext.apply(this.createEffect(true, this.afterExpand, this),
								this.expandDefaults));
				this.getCollapsedEl().slideOut('l', {duration:.2});
			}
		});
	}
});