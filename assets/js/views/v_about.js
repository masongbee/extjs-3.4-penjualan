/* path: assets\js\view
   buat terlebih dahulu folder view jika belum ada
*/
var aboutWindow;
var aboutForm;
var id;

var about_labelField;

Ext.onReady(function(){
  Ext.QuickTips.init();
	
	about_labelField= new Ext.form.Label({
		id: 'about_labelField',
		readOnly: true,
		html: '<p>ABC</p>'
	});
	
	
	aboutForm = new Ext.FormPanel({
		url: 'index.php?c=c_about',
		labelAlign: 'top',
		bodyStyle:'padding:5px',
		x:0,
		y:0,
		width: 400, 
		height: 360,
		items: [{
			layout:'column',
			border:false,
			items:[{
				columnWidth:0.99,
				layout: 'form',
				border:false,
				items: [about_labelField] 
			}]
		}],
		monitorValid:true,
		buttons: [{
				text: 'Close',
				handler: function(){
				// because of the global vars, we can only instantiate one window... so let's just hide it.
				aboutWindow.hide();
				mainPanel.remove(mainPanel.getActiveTab().getId());
			}
		}]
		
	});
	
	/* Form Advanced Search */
	aboutWindow = new Ext.Window({
		title: 'About Us',
		closable:false,
		closeAction: 'hide',
		resizable: false,
		plain:true,
		layout: 'fit',
		x: 0,
		y: 0,
		modal: true,
		renderTo: 'elwindow_about',
		items: aboutForm
	});
	aboutForm.getForm().load();
  	aboutWindow.show();
  	
});