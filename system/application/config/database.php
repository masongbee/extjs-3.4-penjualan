<?php
$active_group = "default";
$active_record = TRUE;

// DATABASE MASTER ONLINE
$db['ONLINEDB']['hostname'] = "localhost";
$db['ONLINEDB']['username'] = "root";
$db['ONLINEDB']['password'] = "";
$db['ONLINEDB']['database'] = "bidb";
$db['ONLINEDB']['dbdriver'] = "mysql";
$db['ONLINEDB']['dbprefix'] = "";
$db['ONLINEDB']['pconnect'] = FALSE;
$db['ONLINEDB']['db_debug'] = FALSE;
$db['ONLINEDB']['cache_on'] = FALSE;
$db['ONLINEDB']['autoinit'] = FALSE;
$db['ONLINEDB']['cachedir'] = "";
$db['ONLINEDB']['char_set'] = "utf8";
$db['ONLINEDB']['dbcollat'] = "utf8_general_ci";

$db['ONLINEDBLOCAL']['hostname'] = "localhost";
$db['ONLINEDBLOCAL']['username'] = "root";
$db['ONLINEDBLOCAL']['password'] = "";
$db['ONLINEDBLOCAL']['database'] = "bidb";
$db['ONLINEDBLOCAL']['dbdriver'] = "mysql";
$db['ONLINEDBLOCAL']['dbprefix'] = "";
$db['ONLINEDBLOCAL']['pconnect'] = FALSE;
$db['ONLINEDBLOCAL']['db_debug'] = FALSE;
$db['ONLINEDBLOCAL']['cache_on'] = FALSE;
$db['ONLINEDBLOCAL']['autoinit'] = FALSE;
$db['ONLINEDBLOCAL']['cachedir'] = "";
$db['ONLINEDBLOCAL']['char_set'] = "utf8";
$db['ONLINEDBLOCAL']['dbcollat'] = "utf8_general_ci";

$db['default']['hostname'] = "localhost";
$db['default']['username'] = "root";
$db['default']['password'] = "";
$db['default']['database'] = "bidb";
$db['default']['dbdriver'] = "mysql";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

$db['default_log']['hostname'] = "localhost";
$db['default_log']['username'] = "root";
$db['default_log']['password'] = "";
$db['default_log']['database'] = "bidb_log";
$db['default_log']['dbdriver'] = "mysql";
$db['default_log']['dbprefix'] = "";
$db['default_log']['pconnect'] = FALSE;
$db['default_log']['db_debug'] = TRUE;
$db['default_log']['cache_on'] = FALSE;
$db['default_log']['cachedir'] = "";
$db['default_log']['char_set'] = "utf8";
$db['default_log']['dbcollat'] = "utf8_general_ci";

$db['default_report']['hostname'] = "localhost";
$db['default_report']['username'] = "root";
$db['default_report']['password'] = "";
$db['default_report']['database'] = "bidb";
$db['default_report']['dbdriver'] = "mysql";
$db['default_report']['dbprefix'] = "";
$db['default_report']['pconnect'] = TRUE;
$db['default_report']['db_debug'] = TRUE;
$db['default_report']['cache_on'] = FALSE;
$db['default_report']['cachedir'] = "";
$db['default_report']['char_set'] = "utf8";
$db['default_report']['dbcollat'] = "utf8_general_ci";

/* End of file database.php */
/* Location: ./system/application/config/database.php */