<?php


class C_login extends Controller {

	function C_login()
	{
		parent::Controller();
		$this->load->model('m_login', '', TRUE);
		$this->load->model('m_main', '', TRUE);
		session_start();
		$data["dummy"]='';
		$push_view=$this->load->view("v_hidden",$data,TRUE);
		$ip_address=$_SERVER['REMOTE_ADDR'];
		$ip_address=  str_replace(".", "_", $ip_address);
		$file = fopen("push/userlog_".$ip_address.".php",'w+');
		fwrite($file, $push_view);	
		fclose($file);
				
		if(isset($_SESSION[SESSION_USERID])){
			if($_SESSION[SESSION_USERID]!==""){
				setcookie('user',$_SESSION[SESSION_USERID]);
				header("location: index.php?c=main");
			}
		}
	}
	
	function index()
	{
		$data["background"]=$this->m_main->get_background();
		$this->load->vars($data);
		$this->load->view('v_login');
	}
	
	function verify() 
	{
		if( isset($_POST['username']) && isset($_POST['password'])) 
		{
			$u	= strtolower($_POST['username']);
			$pw	= md5($_POST['password']);
			$auth = $this->m_login->verifyUser($u, $pw);
			if($auth){
				setcookie('user',$u);
				echo "{success:true}";
			} else{
				echo "{success:false,msg:'Username or Password incorrect'}";
			}
		} else {		
		    echo "{success:false,msg:'Please fill the Requirement Field!'}";
		}
	}
	
	function logout(){
		if(isset($_POST['username'])){ $username=$_POST['username']; }
		else{
			if(isset($_SESSION[SESSION_USERID])){$username=$_SESSION[SESSION_USERID];}
			else{$username='';}
		}
		$auth = $this->m_login->verifyLogout($username);
		if($auth){
			foreach ($_COOKIE as $key=>$value)
			{setcookie( $key,$value,1,'/');}
		    unset($_SESSION[SESSION_USERID]);
		    echo "{success:true}";
		}else{
		    echo "{success:false}";
		}
	 }
}
?>