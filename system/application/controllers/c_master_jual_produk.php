<?php
//class of master_jual_produk
class C_master_jual_produk extends Controller {

	//constructor
	function C_master_jual_produk(){
		parent::Controller();
		session_start();
		$this->load->model('m_master_jual_produk', '', TRUE);		
	}
	
	function cek_sisa_stok(){
		$produk_id = (integer) (isset($_POST['produk_id']) ? $_POST['produk_id'] : 0);
		$result = $this->m_master_jual_produk->cek_sisa_stok($produk_id);
		echo $result;
	}
	
	function card_edc_jenis_list(){
		$query = isset($_POST['query']) ? @$_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);
		$result=$this->m_master_jual_produk->card_edc_jenis_list($query,$start,$end);
		echo $result;
	}
	
	function card_edc_list(){
		$query = isset($_POST['query']) ? @$_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);
		$result=$this->m_master_jual_produk->card_edc_list($query,$start,$end);
		echo $result;
	}
	
	function card_promo_list(){
		$query = isset($_POST['query']) ? @$_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);
		$result=$this->m_master_jual_produk->card_promo_list($query,$start,$end);
		echo $result;
	}		
	
	//set index
	function index(){
		$this->load->helper('asset');
		$this->load->view('main/v_master_jual_produk');
		
		$data["dummy"]='';
		$push_view=$this->load->view("main/p_push_creator",$data,TRUE);
		$ip_address=$_SERVER['REMOTE_ADDR'];
		$ip_address=  str_replace(".", "_", $ip_address);
		$file = fopen("push/kasir_".$ip_address.".php",'w+');
		fwrite($file, $push_view);	
		fclose($file);
	}
	
	function laporan(){
		$this->load->view('main/v_lap_jual_produk');
	}
	
	function print_laporan(){
		$tgl_awal=(isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);
		$tgl_akhir=(isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);
		$bulan=(isset($_POST['bulan']) ? @$_POST['bulan'] : @$_GET['bulan']);
		$tahun=(isset($_POST['tahun']) ? @$_POST['tahun'] : @$_GET['tahun']);
		$opsi=(isset($_POST['opsi']) ? @$_POST['opsi'] : @$_GET['opsi']);
		$biasa=(isset($_POST['biasa']) ? @$_POST['biasa'] : @$_GET['biasa']);
		$grooming=(isset($_POST['grooming']) ? @$_POST['grooming'] : @$_GET['grooming']);
		$lainnya=(isset($_POST['lainnya']) ? @$_POST['lainnya'] : @$_GET['lainnya']);
		$opsi_status=(isset($_POST['opsi_status']) ? @$_POST['opsi_status'] : @$_GET['opsi_status']);
		$periode=(isset($_POST['periode']) ? @$_POST['periode'] : @$_GET['periode']);
		$group=(isset($_POST['group']) ? @$_POST['group'] : @$_GET['group']);
		$compliment=(isset($_POST['compliment']) ? @$_POST['compliment'] : @$_GET['compliment']);
		$lain=(isset($_POST['lain']) ? @$_POST['lain'] : @$_GET['lain']);
		$free=(isset($_POST['free']) ? @$_POST['free'] : @$_GET['free']);
		$compli_only=(isset($_POST['compli_only']) ? @$_POST['compli_only'] : @$_GET['compli_only']);
		$grooming_nonvoucher=(isset($_POST['grooming_nonvoucher']) ? @$_POST['grooming_nonvoucher'] : @$_GET['grooming_nonvoucher']);
		$grooming_voucher=(isset($_POST['grooming_voucher']) ? @$_POST['grooming_voucher'] : @$_GET['grooming_voucher']);
		$grooming_owner_voucher=(isset($_POST['grooming_owner_voucher']) ? @$_POST['grooming_owner_voucher'] : @$_GET['grooming_owner_voucher']);
		$grooming_owner_nonvoucher=(isset($_POST['grooming_owner_nonvoucher']) ? @$_POST['grooming_owner_nonvoucher'] : @$_GET['grooming_owner_nonvoucher']);
		
		$data["jenis"]='Produk';
		
		if($periode=="all"){
			$data["periode"]="Semua Periode";
		}else if($periode=="bulan"){
			$tgl_awal=$tahun."-".$bulan;
			$data["periode"]=get_ina_month_name($bulan,'long')." ".$tahun;
		}else if($periode=="tanggal"){
			$date = substr($tgl_awal,8,2);
			$month = substr($tgl_awal,5,2);
			$year = substr($tgl_awal,0,4);
			$tgl_awal_show = $date.'-'.$month.'-'.$year;
			
			$date_akhir = substr($tgl_akhir,8,2);
			$month_akhir = substr($tgl_akhir,5,2);
			$year_akhir = substr($tgl_akhir,0,4);
			$tgl_akhir_show = $date_akhir.'-'.$month_akhir.'-'.$year_akhir;
			$data["periode"]="Periode : ".$tgl_awal_show." s/d ".$tgl_akhir_show;
		}

		$data["data_print"]=$this->m_master_jual_produk->get_laporan($tgl_awal,$tgl_akhir,$periode,$opsi,$opsi_status,$group,$biasa,$grooming_nonvoucher,$grooming_voucher,$lainnya,$compliment,$grooming_owner_voucher,$grooming_owner_nonvoucher,$compli_only, $lain);
		
		if(!file_exists("print")){
			mkdir("print");
		}
		
		$data['group'] = $group;

		if($opsi=='rekap'){
			$data["opsi"]='Rekap';
			switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_rekap_jual_tanggal.php",$data,TRUE);break;
				case "Customer": $print_view=$this->load->view("main/p_rekap_jual_customer.php",$data,TRUE);break;
				case "Voucher": $print_view=$this->load->view("main/p_rekap_jual_voucher.php",$data,TRUE);break;
				case "EDC": $print_view=$this->load->view("main/p_rekap_jual_card.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_rekap_jual.php",$data,TRUE);break;
			}
			$print_file=fopen("print/report_jproduk.html","w");
			fwrite($print_file, $print_view);
			echo '1';
		}else if($opsi=='detail'){
			$data["opsi"]='Detail';
			if($group =='No Faktur BPOM'){
				$data["group"]='No Faktur BPOM';
			}elseif($group =='No Faktur (Format 2)'){
				$data["group"]='No Faktur (Format 2)';
			}
			
			if($biasa == 'true'){	
				$data["jns_faktur"]='';
				/*if($lainnya=='true' && $grooming=='true' && $compliment=='true'){
					$data["jns_faktur"]='Biasa | Free';
				}else if($lainnya=='false' && $grooming=='false' && $compliment=='false'){
					$data["jns_faktur"]='Biasa (Net Sales)';
				}else if($lainnya=='false' && $grooming=='false' && $compliment=='true'){
					$data["jns_faktur"]='Biasa | Compliment';
				}else if($lainnya=='true' && $grooming=='false' && $compliment=='true'){
					$data["jns_faktur"]='Biasa | Lainnya | Compliment';
				}else if($lainnya=='false' && $grooming=='true' && $compliment=='true' ){
					$data["jns_faktur"]='Biasa  | Grooming | Compliment';
				}else if($lainnya=='true' && $grooming=='true' && $compliment=='false' ){
					$data["jns_faktur"]='Biasa | Grooming | Lainnya';
				}else if($lainnya=='true' && $grooming=='false' && $compliment=='false'){
					$data["jns_faktur"]='Biasa | Lainnya';
				}else if($lainnya=='false' && $grooming=='true' && $compliment=='false' ){
					$data["jns_faktur"]='Biasa | Grooming';
				}*/
			}else{
				$data["jns_faktur"]='';
				/*if($lainnya=='true' && $grooming=='true' && $compliment=='true'){
					$data["jns_faktur"]='Free';
				}else if($lainnya=='false' && $grooming=='false' && $compliment=='false'){
					$data["jns_faktur"]='Tidak ada pilihan';
				}else if($lainnya=='false' && $grooming=='false' && $compliment=='true'){
					$data["jns_faktur"]='Free Compliment';
				}else if($lainnya=='true' && $grooming=='false' && $compliment=='true'){
					$data["jns_faktur"]='Free Compliment | Lainnya';
				}else if($lainnya=='false' && $grooming=='true' && $compliment=='true' ){
					$data["jns_faktur"]='Free | Grooming | Compliment';
				}else if($lainnya=='true' && $grooming=='true' && $compliment=='false' ){
					$data["jns_faktur"]='Free Grooming | Lainnya';
				}else if($lainnya=='true' && $grooming=='false' && $compliment=='false'){
					$data["jns_faktur"]='Free Lainnya';
				}else if($lainnya=='false' && $grooming=='true' && $compliment=='false' ){
					$data["jns_faktur"]='Free Grooming';
				}*/
			}
			
			switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
				case "Tanggal (Format 2 PPN)": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
				case "Customer": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
				case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
				case "Referal": $print_view=$this->load->view("main/p_detail_jual_sales.php",$data,TRUE);break;
				case "Jenis Diskon": $print_view=$this->load->view("main/p_detail_jual_diskon.php",$data,TRUE);break;
				case "EDC": $print_view=$this->load->view("main/p_detail_jual_card.php",$data,TRUE);break;
				case "Group 1": $print_view=$this->load->view("main/p_detail_jual_group.php",$data,TRUE);break;
				case "No Faktur BPOM": $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;//tambah ini
				case "Karyawan": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
				case "No Faktur (Format 2)": $print_view=$this->load->view("main/p_detail_jual2.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_detail_jual_semua.php",$data,TRUE);break;
			}			
			
			$print_file=fopen("print/report_jproduk.html","w");
			fwrite($print_file, $print_view);
			fclose($print_file);
			echo '1'; 
		}/*
		else if($opsi=='grooming'){
			$data["opsi"]='Grooming';
                        $data["group"]=$group;
			switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
				case "Tanggal 2": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
				case "Karyawan": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
                                case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;
			}
			$print_file=fopen("print/report_jproduk.html","w");
			fwrite($print_file, $print_view);
			fclose($print_file);
			echo '1'; 
			
		}
		else if($opsi=='free'){
			$data["opsi"]='Free';
                        $data["group"]=$group;
			switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
				case "Tanggal 2": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
				case "Customer": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
                case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;
			}
			$print_file=fopen("print/report_jproduk.html","w");
			fwrite($print_file, $print_view);
			fclose($print_file);
			echo '1'; 
			
		}*/
	}
	
	function export_to_excel(){
	    $tgl_awal=(isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);
	    $tgl_akhir=(isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);
	    $bulan=(isset($_POST['bulan']) ? @$_POST['bulan'] : @$_GET['bulan']);
	    $tahun=(isset($_POST['tahun']) ? @$_POST['tahun'] : @$_GET['tahun']);
	    $opsi=(isset($_POST['opsi']) ? @$_POST['opsi'] : @$_GET['opsi']);
	    $opsi_status=(isset($_POST['opsi_status']) ? @$_POST['opsi_status'] : @$_GET['opsi_status']);
	    $periode=(isset($_POST['periode']) ? @$_POST['periode'] : @$_GET['periode']);
	    $group=(isset($_POST['group']) ? @$_POST['group'] : @$_GET['group']);
	    $biasa=(isset($_POST['biasa']) ? @$_POST['biasa'] : @$_GET['biasa']);
	    $grooming=(isset($_POST['grooming']) ? @$_POST['grooming'] : @$_GET['grooming']);
	    $lainnya=(isset($_POST['lainnya']) ? @$_POST['lainnya'] : @$_GET['lainnya']);
	    $compliment=(isset($_POST['compliment']) ? @$_POST['compliment'] : @$_GET['compliment']);
		$lain=(isset($_POST['lain']) ? @$_POST['lain'] : @$_GET['lain']);
		$free=(isset($_POST['free']) ? @$_POST['free'] : @$_GET['free']);
		$compli_only=(isset($_POST['compli_only']) ? @$_POST['compli_only'] : @$_GET['compli_only']);
		$grooming_nonvoucher=(isset($_POST['grooming_nonvoucher']) ? @$_POST['grooming_nonvoucher'] : @$_GET['grooming_nonvoucher']);
		$grooming_voucher=(isset($_POST['grooming_voucher']) ? @$_POST['grooming_voucher'] : @$_GET['grooming_voucher']);
		$grooming_owner_voucher=(isset($_POST['grooming_owner_voucher']) ? @$_POST['grooming_owner_voucher'] : @$_GET['grooming_owner_voucher']);
		$grooming_owner_nonvoucher=(isset($_POST['grooming_owner_nonvoucher']) ? @$_POST['grooming_owner_nonvoucher'] : @$_GET['grooming_owner_nonvoucher']);
		
	    $data["jenis"]='Produk';	    
	    
	    if($periode=="all"){
		    $data["periode"]="Semua Periode";
	    }else if($periode=="bulan"){
		    $tgl_awal=$tahun."-".$bulan;
		    $data["periode"]=get_ina_month_name($bulan,'long')." ".$tahun;
	    }else if($periode=="tanggal"){
		    $date = substr($tgl_awal,8,2);
		    $month = substr($tgl_awal,5,2);
		    $year = substr($tgl_awal,0,4);
		    $tgl_awal_show = $date.'-'.$month.'-'.$year;

		    $date_akhir = substr($tgl_akhir,8,2);
		    $month_akhir = substr($tgl_akhir,5,2);
		    $year_akhir = substr($tgl_akhir,0,4);
		    $tgl_akhir_show = $date_akhir.'-'.$month_akhir.'-'.$year_akhir;

		    //$tgl_awal_show = $tgl_awal;
		    //$tgl_awal_show = date("d-m-Y", $tgl_awal);
		    //$tgl_akhir_show = $tgl_akhir;
		    //$tgl_akhir_show = date("d-m-Y", $tgl_akhir);
		    $data["periode"]="Periode : ".$tgl_awal_show." s/d ".$tgl_akhir_show;
	    }

	    $data["data_print"]=$this->m_master_jual_produk->get_laporan($tgl_awal,$tgl_akhir,$periode,$opsi,$opsi_status,$group,$biasa,$grooming_nonvoucher,$grooming_voucher,$lainnya,$compliment,$grooming_owner_voucher,$grooming_owner_nonvoucher,$compli_only, $lain);
	    
	    $print_view = "";
	    
	    if($opsi=='rekap'){
			$data["opsi"]='Rekap';
			switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_rekap_jual_tanggal.php",$data,TRUE);break;
				case "Customer": $print_view=$this->load->view("main/p_rekap_jual_customer.php",$data,TRUE);break;
				case "Voucher": $print_view=$this->load->view("main/p_rekap_jual_voucher.php",$data,TRUE);break;
				case "EDC": $print_view=$this->load->view("main/p_rekap_jual_card.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_rekap_jual.php",$data,TRUE);break;
			} 
	    }else if($opsi=='detail'){
			$data["opsi"]='Detail';
			
			if($group =='No Faktur BPOM'){
				$data["group"]='No Faktur BPOM';
			}elseif($group =='No Faktur (Format 2)'){
				$data["group"]='No Faktur (Format 2)';
			}else{
				$data["group"]='No Faktur';
			}
			
			if($biasa == 'true'){				
				if($lainnya=='true' && $grooming=='true' && $compliment=='true'){
					$data["jns_faktur"]='Biasa | Free';
				}else if($lainnya=='false' && $grooming=='false' && $compliment=='false'){
					$data["jns_faktur"]='Biasa (Net Sales)';
				}else if($lainnya=='false' && $grooming=='false' && $compliment=='true'){
					$data["jns_faktur"]='Biasa | Compliment';
				}else if($lainnya=='true' && $grooming=='false' && $compliment=='true'){
					$data["jns_faktur"]='Biasa | Lainnya | Compliment';
				}else if($lainnya=='false' && $grooming=='true' && $compliment=='true' ){
					$data["jns_faktur"]='Biasa  | Grooming | Compliment';
				}else if($lainnya=='true' && $grooming=='true' && $compliment=='false' ){
					$data["jns_faktur"]='Biasa | Grooming | Lainnya';
				}else if($lainnya=='true' && $grooming=='false' && $compliment=='false'){
					$data["jns_faktur"]='Biasa | Lainnya';
				}else if($lainnya=='false' && $grooming=='true' && $compliment=='false' ){
					$data["jns_faktur"]='Biasa | Grooming';
				}
			}else{
				if($lainnya=='true' && $grooming=='true' && $compliment=='true'){
					$data["jns_faktur"]='Free';
				}else if($lainnya=='false' && $grooming=='false' && $compliment=='false'){
					$data["jns_faktur"]='Tidak ada pilihan';
				}else if($lainnya=='false' && $grooming=='false' && $compliment=='true'){
					$data["jns_faktur"]='Free Compliment';
				}else if($lainnya=='true' && $grooming=='false' && $compliment=='true'){
					$data["jns_faktur"]='Free Compliment | Lainnya';
				}else if($lainnya=='false' && $grooming=='true' && $compliment=='true' ){
					$data["jns_faktur"]='Free | Grooming | Compliment';
				}else if($lainnya=='true' && $grooming=='true' && $compliment=='false' ){
					$data["jns_faktur"]='Free Grooming | Lainnya';
				}else if($lainnya=='true' && $grooming=='false' && $compliment=='false'){
					$data["jns_faktur"]='Free Lainnya';
				}else if($lainnya=='false' && $grooming=='true' && $compliment=='false' ){
					$data["jns_faktur"]='Free Grooming';
				}
			}
			
			if ($opsi_status=='semua') {	
				switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
				case "Tanggal (Format 2 - PPN)": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
				case "Customer": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
				case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
				case "Referal": $print_view=$this->load->view("main/p_detail_jual_sales.php",$data,TRUE);break;
				case "Jenis Diskon": $print_view=$this->load->view("main/p_detail_jual_diskon.php",$data,TRUE);break;
				case "EDC": $print_view=$this->load->view("main/p_detail_jual_card.php",$data,TRUE);break;
				case "Group 1": $print_view=$this->load->view("main/p_detail_jual_group.php",$data,TRUE);break;
				case "No Faktur BPOM": $print_view=$this->load->view("main/p_detail_jual_semua.php",$data,TRUE);break;//tambah ini
				case "No Faktur (Format 2)": $print_view=$this->load->view("main/p_detail_jual_semua2.php",$data,TRUE);break;
				case "Karyawan": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_detail_jual_semua.php",$data,TRUE);break;
				} 
			} else if ($opsi_status=='tertutup') {
				switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
				case "Tanggal (Format 2 - PPN)": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
				case "Customer": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
				case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
				case "Referal": $print_view=$this->load->view("main/p_detail_jual_sales.php",$data,TRUE);break;
				case "Jenis Diskon": $print_view=$this->load->view("main/p_detail_jual_diskon.php",$data,TRUE);break;
				case "EDC": $print_view=$this->load->view("main/p_detail_jual_card.php",$data,TRUE);break;
				case "Group 1": $print_view=$this->load->view("main/p_detail_jual_group.php",$data,TRUE);break;
				case "No Faktur BPOM": $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;//tambah ini
				case "No Faktur (Format 2)": $print_view=$this->load->view("main/p_detail_jual2.php",$data,TRUE);break;
				case "Karyawan": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;
				}
			}
	    }else if($opsi=='grooming'){
			$data["opsi"]='Grooming';
			$data["group"]=$group;
			switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
				case "Tanggal 2": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
				case "Karyawan": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
				case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;
			}
	    }else if($opsi=='free'){
			$data["opsi"]='Free';
			$data["group"]=$group;
			switch($group){
				case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
				case "Tanggal 2": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
				case "Customer": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
				case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
				default: $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;
			}
	    }
	    	    
	    $file_name = "laporan_produk_" . $opsi . "_" . str_replace(" ", "_", $group) . "_";
	    
	    if($periode=="bulan")
		$file_name .= $bulan . "_" . $tahun . ".xls";
	    else
		$file_name .= str_replace("-", "_", $tgl_awal_show) . "_sd_" . str_replace("-", "_", $tgl_akhir_show) . ".xls";
	    
	    if(!file_exists("print")){
		mkdir("print");
	    }
	    
	    $this->load->plugin('to_excel');
	    $print_file=fopen("print/$file_name","w+");
	    fwrite($print_file, $print_view);
	    
	    echo "1-$file_name";
	}
	
	function export_to_pdf(){
	    $tgl_awal=(isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);
	    $tgl_akhir=(isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);
	    $bulan=(isset($_POST['bulan']) ? @$_POST['bulan'] : @$_GET['bulan']);
	    $tahun=(isset($_POST['tahun']) ? @$_POST['tahun'] : @$_GET['tahun']);
	    $opsi=(isset($_POST['opsi']) ? @$_POST['opsi'] : @$_GET['opsi']);
	    $opsi_status=(isset($_POST['opsi_status']) ? @$_POST['opsi_status'] : @$_GET['opsi_status']);
	    $periode=(isset($_POST['periode']) ? @$_POST['periode'] : @$_GET['periode']);
	    $group=(isset($_POST['group']) ? @$_POST['group'] : @$_GET['group']);

	    $data["jenis"]='Produk';
	    $data["export"]='pdf';
	    
	    if($periode=="all"){
		    $data["periode"]="Semua Periode";
	    }else if($periode=="bulan"){
		    $tgl_awal=$tahun."-".$bulan;
		    $data["periode"]=get_ina_month_name($bulan,'long')." ".$tahun;
	    }else if($periode=="tanggal"){
		    $date = substr($tgl_awal,8,2);
		    $month = substr($tgl_awal,5,2);
		    $year = substr($tgl_awal,0,4);
		    $tgl_awal_show = $date.'-'.$month.'-'.$year;

		    $date_akhir = substr($tgl_akhir,8,2);
		    $month_akhir = substr($tgl_akhir,5,2);
		    $year_akhir = substr($tgl_akhir,0,4);
		    $tgl_akhir_show = $date_akhir.'-'.$month_akhir.'-'.$year_akhir;
		    $data["periode"]="Periode : ".$tgl_awal_show." s/d ".$tgl_akhir_show;
	    }

	    $data["data_print"]=$this->m_master_jual_produk->get_laporan($tgl_awal,$tgl_akhir,$periode,$opsi,$opsi_status,$group);
	    
	    $this->load->library('Pdf');
	    
	    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);	    
	    $pdf->SetTitle('Laporan Penjualan Produk');
	    $pdf->setPrintHeader(false);
	    $pdf->setPrintFooter(false);
	    $pdf->SetDisplayMode('real', 'default');
	    
	    
	    $print_view = "";
	    
	    if($opsi=='rekap'){
		$data["opsi"]='Rekap';
		switch($group){
		    case "Tanggal": $print_view=$this->load->view("main/p_rekap_jual_tanggal.php",$data,TRUE);break;
		    case "Customer": $print_view=$this->load->view("main/p_rekap_jual_customer.php",$data,TRUE);break;
		    case "Voucher": $print_view=$this->load->view("main/p_rekap_jual_voucher.php",$data,TRUE);break;
		    default: $print_view=$this->load->view("main/p_rekap_jual.php",$data,TRUE);break;
		} 
	    }else if($opsi=='detail'){
		$data["opsi"]='Detail';
		if($group =='No Faktur BPOM'){
		    $data["group"]='No Faktur BPOM';
		}elseif($group =='No Faktur (Format 2)'){
		    $data["group"]='No Faktur (Format 2)';
		}else{
		    $data["group"]='No Faktur';
		}
		if ($opsi_status=='semua') {	
		    switch($group){
			case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
			case "Tanggal (Format 2 - PPN)": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
			case "Customer": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
			case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
			case "Referal": $print_view=$this->load->view("main/p_detail_jual_sales.php",$data,TRUE);break;
			case "Jenis Diskon": $print_view=$this->load->view("main/p_detail_jual_diskon.php",$data,TRUE);break;
			case "Group 1": $print_view=$this->load->view("main/p_detail_jual_group.php",$data,TRUE);break;
			case "No Faktur BPOM": $print_view=$this->load->view("main/p_detail_jual_semua.php",$data,TRUE);break;//tambah ini
			case "No Faktur (Format 2)": $print_view=$this->load->view("main/p_detail_jual_semua2.php",$data,TRUE);break;
			default: $print_view=$this->load->view("main/p_detail_jual_semua.php",$data,TRUE);break;
		    } 
		} else if ($opsi_status=='tertutup') {
		    switch($group){
			case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
			case "Tanggal (Format 2 - PPN)": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
			case "Customer": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
			case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
			case "Referal": $print_view=$this->load->view("main/p_detail_jual_sales.php",$data,TRUE);break;
			case "Jenis Diskon": $print_view=$this->load->view("main/p_detail_jual_diskon.php",$data,TRUE);break;
			case "Group 1": $print_view=$this->load->view("main/p_detail_jual_group.php",$data,TRUE);break;
			case "No Faktur BPOM": $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;//tambah ini
			case "No Faktur (Format 2)": $print_view=$this->load->view("main/p_detail_jual2.php",$data,TRUE);break;
			default: $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;
		    }
		}
	    }
	    else if($opsi=='grooming'){
		$data["opsi"]='Grooming';
		$data["group"]=$group;
		switch($group){
		    case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
		    case "Tanggal 2": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
		    case "Karyawan": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
		    case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
		    default: $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;
		}
	    }
		else if($opsi=='free'){
		$data["opsi"]='Free';
		$data["group"]=$group;
		switch($group){
		    case "Tanggal": $print_view=$this->load->view("main/p_detail_jual_tanggal.php",$data,TRUE);break;
		    case "Tanggal 2": $print_view=$this->load->view("main/p_detail_jual_tanggal2.php",$data,TRUE);break;
		    case "Customer": $print_view=$this->load->view("main/p_detail_jual_customer.php",$data,TRUE);break;
		    case "Produk": $print_view=$this->load->view("main/p_detail_jual_produk.php",$data,TRUE);break;
		    default: $print_view=$this->load->view("main/p_detail_jual.php",$data,TRUE);break;
		}
	    }
	    
	    $file_name = "laporan_produk_" . $opsi . "_" . str_replace(" ", "_", $group) . "_";
	    
	    if($periode=="bulan")
		$file_name .= $bulan . "_" . $tahun . ".pdf";
	    else
		$file_name .= str_replace("-", "_", $tgl_awal_show) . "_sd_" . str_replace("-", "_", $tgl_akhir_show) . ".pdf";
	    
	    $pdf->SetMargins(2, 5);	    
	    $pdf->AddPage();		    		    

	    $pdf->writeHTML($print_view, true, true, false, false, '');
	    $pdf->Output($file_name, 'F');

	    echo "1-$file_name";
	}
	
	function get_reveral_list(){
		$query = isset($_POST['query']) ? $_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$result=$this->m_master_jual_produk->get_reveral_list($query,$start,$end);
		echo $result;
	}
	
	
	
	function get_bank_list(){
		$result=$this->m_public_function->get_bank_list();
		echo $result;
	}
	
	function get_customer_list(){
		$query = isset($_POST['query']) ? $_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$result=$this->m_public_function->get_customer_list($query,$start,$end);
		echo $result;
	}
	
	function get_auto_catatan_customer2(){
		$note_customer = (integer) (isset($_POST['note_customer']) ? $_POST['note_customer'] : $_GET['note_customer']);
		$result=$this->m_public_function->get_auto_catatan_customer2($note_customer);
		echo $result;
	}
	
	function get_produk_list(){
		$query = isset($_POST['query']) ? $_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$aktif = trim(@$_POST["aktif"]);
		$result = $this->m_master_jual_produk->get_produk_list($query,$start,$end,$aktif);
		echo $result;
	}
	
	function get_produk_list_barcode(){
		$member_no = isset($_POST['member_no']) ? $_POST['member_no'] : "";
		$result=$this->m_master_jual_produk->get_produk_list_barcode($member_no);
		echo $result;
	}
	
	function get_satuan_list(){
		$result = $this->m_public_function->get_satuan_list();
		echo $result;
	}
	
	function get_satuan_bydjproduk_list(){
		$query = (integer) (isset($_POST['query']) ? $_POST['query'] : 0);
		$produk_id = (integer) (isset($_POST['produk_id']) ? $_POST['produk_id'] : 0);
		$satuan_id = (integer) (isset($_POST['satuan_id']) ? $_POST['satuan_id'] : 0);
		$result = $this->m_master_jual_produk->get_satuan_bydjproduk_list($query, $produk_id, $satuan_id);
		echo $result;
	}
	
	function get_satuan_bydjproduk_list2(){
		$query = (integer) (isset($_POST['query']) ? $_POST['query'] : 0);
		$produk_id = (integer) (isset($_POST['produk_id']) ? $_POST['produk_id'] : 0);
		$satuan_id = (integer) (isset($_POST['satuan_id']) ? $_POST['satuan_id'] : 0);
		$result = $this->m_master_jual_produk->get_satuan_bydjproduk_list2($query, $produk_id, $satuan_id);
		echo $result;
	}
	
	function get_kwitansi_by_ref(){
		$ref_id = (isset($_POST['no_faktur']) ? $_POST['no_faktur'] : $_GET['no_faktur']);
		$cara_bayar_ke = @$_POST['cara_bayar_ke'];
		$cara_bayar_1 = @$_POST['cara_bayar_1'];
		$cara_bayar_2 = @$_POST['cara_bayar_2'];
		$result = $this->m_public_function->get_kwitansi_by_ref($ref_id , $cara_bayar_ke, $cara_bayar_1, $cara_bayar_2);
		echo $result;
	}
	
	function get_gift_voucher_by_ref(){
		$ref_id = (isset($_POST['no_faktur']) ? $_POST['no_faktur'] : $_GET['no_faktur']);
		$cara_bayar_ke = @$_POST['cara_bayar_ke'];
		$cara_bayar_1 = @$_POST['cara_bayar_1'];
		$cara_bayar_2 = @$_POST['cara_bayar_2'];
		$result = $this->m_public_function->get_gift_voucher_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1, $cara_bayar_2);
		echo $result;
	}
	
	function get_cek_by_ref(){
		$ref_id = (isset($_POST['no_faktur']) ? $_POST['no_faktur'] : $_GET['no_faktur']);
		$cara_bayar_ke = @$_POST['cara_bayar_ke'];
		$cara_bayar_1 = @$_POST['cara_bayar_1'];
		$cara_bayar_2 = @$_POST['cara_bayar_2'];
		$result = $this->m_public_function->get_cek_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1, $cara_bayar_2);
		echo $result;
	}
	
	function get_card_by_ref(){
		$ref_id = (isset($_POST['no_faktur']) ? $_POST['no_faktur'] : $_GET['no_faktur']);
		$cara_bayar_ke = @$_POST['cara_bayar_ke'];
		$cara_bayar_1 = @$_POST['cara_bayar_1'];
		$cara_bayar_2 = @$_POST['cara_bayar_2'];
		$result = $this->m_public_function->get_card_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1, $cara_bayar_2);
		echo $result;
	}
	
	function get_transfer_by_ref(){
		$ref_id = (isset($_POST['no_faktur']) ? $_POST['no_faktur'] : $_GET['no_faktur']);
		$cara_bayar_ke = @$_POST['cara_bayar_ke'];
		$cara_bayar_1 = @$_POST['cara_bayar_1'];
		$cara_bayar_2 = @$_POST['cara_bayar_2'];
		$result = $this->m_public_function->get_transfer_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1, $cara_bayar_2);
		echo $result;
	}
	
	function get_tunai_by_ref(){
		$ref_id = (isset($_POST['no_faktur']) ? $_POST['no_faktur'] : $_GET['no_faktur']);
		$cara_bayar_ke = @$_POST['cara_bayar_ke'];
		$cara_bayar_1 = @$_POST['cara_bayar_1'];
		$cara_bayar_2 = @$_POST['cara_bayar_2'];
		$result = $this->m_public_function->get_tunai_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1, $cara_bayar_2);
		echo $result;
	}
	
	function get_voucher_by_ref(){
		$ref_id = (isset($_POST['no_faktur']) ? $_POST['no_faktur'] : $_GET['no_faktur']);
		$cara_bayar_ke = @$_POST['cara_bayar_ke'];
		$cara_bayar_1 = @$_POST['cara_bayar_1'];
		$cara_bayar_2 = @$_POST['cara_bayar_2'];
		$result = $this->m_public_function->get_voucher_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1, $cara_bayar_2);
		echo $result;
	}
	
	function  get_voucher_list(){
		$query = isset($_POST['query']) ? $_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$result=$this->m_master_jual_produk->get_voucher_list($query,$start,$end);
		echo $result;
	}
	
	function  get_kwitansi_list(){
		$query = isset($_POST['query']) ? $_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$kwitansi_cust=trim(@$_POST["kwitansi_cust"]);
		$result=$this->m_public_function->get_kwitansi_list($query,$start,$end,$kwitansi_cust);
		echo $result;
	}
	
	function get_member_by_cust(){
		$member_cust = (integer) (isset($_POST['member_cust']) ? $_POST['member_cust'] : $_GET['member_cust']);
		$result=$this->m_public_function->get_member_by_cust_ol($member_cust);
		echo $result;
	}
	
	function kuitansi_list(){
		$cust_id = (integer) (isset($_POST['cust_id']) ? $_POST['cust_id'] : $_GET['cust_id']);
		$result=$this->m_public_function->kuitansi_list($cust_id);
		echo $result;
	}
	
	function get_nik(){
		$karyawan_id = (integer) (isset($_POST['karyawan_id']) ? $_POST['karyawan_id'] : $_GET['karyawan_id']);
		$result=$this->m_public_function->get_auto_karyawan_sip($karyawan_id); 
		echo $result;
	}
	
	function push_insert(){
	
		$cust_id=$_POST['id_cust'];
	
		$item_id=$_POST['id_item'];
		$item_id=json_decode(stripslashes($item_id));
		
		$item_jenis=$_POST['item_jenis'];
		$item_jenis=json_decode(stripslashes($item_jenis));

		$item_jml=$_POST['jumlah_beli'];
		$item_jml=json_decode(stripslashes($item_jml));

		$item_hrgsat=$_POST['harga_satuan'];
		$item_hrgsat=json_decode(stripslashes($item_hrgsat));

		$item_diskon=$_POST['diskon'];
		$item_diskon=json_decode(stripslashes($item_diskon));

		$vcr_produk=isset($_POST['vcr_produk']) ? (integer)$_POST['vcr_produk'] : 0;
		$vcr_rawat=isset($_POST['vcr_rawat']) ? (integer)$_POST['vcr_rawat'] : 0;
		$vcr_paket=isset($_POST['vcr_paket']) ? (integer)$_POST['vcr_paket'] : 0;
		
		$count=(integer) (isset($_POST['count']) ? $_POST['count'] : 0);
		
		if($cust_id=='UMUM')
			$cust_id = 10;
		
		$cust_info=$this->m_public_function->get_customer_nama($cust_id);
		$cust_info=explode('<>',$cust_info);
		if(!$cust_info[2]){
			if($cust_info[1]=='P'){
				$cust_nama='IBU '.$cust_info[0];
			}else{
				$cust_nama='BAPAK '.$cust_info[0];
			}
		}else{
			$cust_nama=$cust_info[2].' '.$cust_info[0];
		}
		$produk_nama=array();
		$rawat_nama=array();
		$paket_nama=array();
		
		$produk_jml=array();
		$rawat_jml=array();
		$paket_jml=array();
		
		$produk_diskon=array();
		$rawat_diskon=array();
		$paket_diskon=array();
		
		$produk_subtotal=array();
		$rawat_subtotal=array();
		$paket_subtotal=array();
		
		$produk_total=0;
		$rawat_total=0;
		$paket_total=0;
		
		for($i=0;$i<$count;$i++){
		    if($item_jenis[$i]=='Jual Produk'){
				$temp=$this->m_public_function->get_produk_nama($item_id[$i]);
				array_push($produk_nama,$temp);
				$tempsub=((integer)$item_hrgsat[$i]*(integer)$item_jml[$i])-((integer)$item_diskon[$i]/100*((integer)$item_hrgsat[$i]*(integer)$item_jml[$i]));
				$produk_total+=$tempsub;
				array_push($produk_subtotal,number_format($tempsub,0,",","."));
				array_push($produk_jml,$item_jml[$i]);
				array_push($produk_diskon,$item_diskon[$i]);
			}else if($item_jenis[$i]=='Perawatan' || $item_jenis[$i]=='Ambil Paket'){
				$temp=$this->m_public_function->get_perawatan_nama($item_id[$i]);
				array_push($rawat_nama,$temp);
				$tempsub=((integer)$item_hrgsat[$i]*(integer)$item_jml[$i])-((integer)$item_diskon[$i]/100*((integer)$item_hrgsat[$i]*(integer)$item_jml[$i]));
				$rawat_total+=$tempsub;
				array_push($rawat_subtotal,number_format($tempsub,0,",","."));
				array_push($rawat_jml,$item_jml[$i]);
				array_push($rawat_diskon,$item_diskon[$i]);
			}else{
				$temp=$this->m_public_function->get_paket_nama($item_id[$i]);
				array_push($paket_nama,$temp);
				$tempsub=((integer)$item_hrgsat[$i]*(integer)$item_jml[$i])-((integer)$item_diskon[$i]/100*((integer)$item_hrgsat[$i]*(integer)$item_jml[$i]));
				$paket_total+=$tempsub;
				array_push($paket_subtotal,number_format($tempsub,0,",","."));
				array_push($paket_jml,$item_jml[$i]);
				array_push($paket_diskon,$item_diskon[$i]);
			}
		}
		$grandtotal=0;
		
		$item_voucher=$vcr_produk+$vcr_rawat+$vcr_paket;
		$grandtotal= number_format($produk_total+$rawat_total+$paket_total-$item_voucher,0,",",".");
		
		$produk_total= number_format($produk_total-$vcr_produk,0,",",".");
		$rawat_total= number_format($rawat_total-$vcr_rawat,0,",",".");
		$paket_total= number_format($paket_total-$vcr_paket,0,",",".");
		
		$vcr_produk= number_format($vcr_produk,0,",",".");
		$vcr_rawat= number_format($vcr_rawat,0,",",".");
		$vcr_paket= number_format($vcr_paket,0,",",".");
		
		$content=array(
		    "produk_nama"=>$produk_nama,
		    "produk_jumlah"=>$produk_jml,
		    "produk_diskon"=>$produk_diskon,
		    "produk_subtotal"=>$produk_subtotal,
			"rawat_nama"=>$rawat_nama,
		    "rawat_jumlah"=>$rawat_jml,
		    "rawat_diskon"=>$rawat_diskon,
		    "rawat_subtotal"=>$rawat_subtotal,
			"paket_nama"=>$paket_nama,
		    "paket_jumlah"=>$paket_jml,
		    "paket_diskon"=>$paket_diskon,
		    "paket_subtotal"=>$paket_subtotal
		);
		$post=array(
		    "title"=>"Selamat Datang, ".$cust_nama,
			"cust_id"=>$cust_id,
		    "content"=>$content,
		    "vcr_produk"=>$vcr_produk,
			"vcr_rawat"=>$vcr_rawat,
			"vcr_paket"=>$vcr_paket,
			"tot_produk"=>$produk_total,
			"tot_rawat"=>$rawat_total,
			"tot_paket"=>$paket_total,
		    "total"=>$grandtotal
		);
		
		$address=str_replace('.', '_', $_SERVER['REMOTE_ADDR']);
		$fp = fopen('push/results_'.$address.'.json', 'w');
		fwrite($fp, json_encode($post));
		fclose($fp);   
		echo '1';
	}
	
	function push_reset(){
		$cust_id=$_POST['id_cust'];
	
		$content=array(
		    "produk_nama"=>array(),
		    "produk_jumlah"=>array(),
		    "produk_diskon"=>array(),
		    "produk_subtotal"=>array(),
			"rawat_nama"=>array(),
		    "rawat_jumlah"=>array(),
		    "rawat_diskon"=>array(),
		    "rawat_subtotal"=>array(),
			"paket_nama"=>array(),
		    "paket_jumlah"=>array(),
		    "paket_diskon"=>array(),
		    "paket_subtotal"=>array()
		);
		$post=array(
		    "title"=>"",
			"cust_id"=>$cust_id,
		    "content"=>array(),
		    "vcr_produk"=>0,
			"vcr_rawat"=>0,
			"vcr_paket"=>0,
			"tot_produk"=>0,
			"tot_rawat"=>0,
			"tot_paket"=>0,
		    "total"=>0
		);
		
		$address=str_replace('.', '_', $_SERVER['REMOTE_ADDR']);
		$fp = fopen('push/results_'.$address.'.json', 'w');
		fwrite($fp, json_encode($post));
		fclose($fp);   
		echo '1';
	}
	
	function  detail_detail_jual_produk_list(){
		$query = isset($_POST['query']) ? $_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$master_id = (integer) (isset($_POST['master_id']) ? $_POST['master_id'] : $_GET['master_id']);
		$result=$this->m_master_jual_produk->detail_detail_jual_produk_list($master_id,$query,$start,$end);
		echo $result;
	}
	
	function get_master_id(){
		$result=$this->m_master_jual_produk->get_master_id();
		echo $result;
	}
	
	function detail_detail_jual_produk_insert(){
		$dproduk_id = $_POST['dproduk_id'];
		$array_dproduk_id = json_decode(stripslashes($dproduk_id));
		
		$dproduk_master=trim(@$_POST["dproduk_master"]);
		
		$dproduk_karyawan = $_POST['dproduk_karyawan']; 
		$array_dproduk_karyawan = json_decode(stripslashes($dproduk_karyawan));
		
		$dproduk_produk = $_POST['dproduk_produk']; 
		$array_dproduk_produk = json_decode(stripslashes($dproduk_produk));
		
		$dproduk_satuan = $_POST['dproduk_satuan']; 
		$array_dproduk_satuan = json_decode(stripslashes($dproduk_satuan));
		
		$dproduk_jumlah = $_POST['dproduk_jumlah']; 
		$array_dproduk_jumlah = json_decode(stripslashes($dproduk_jumlah));
		
		$dproduk_harga = $_POST['dproduk_harga']; 
		$array_dproduk_harga = json_decode(stripslashes($dproduk_harga));
		
		$dproduk_subtotal_net = $_POST['dproduk_subtotal_net']; 
		$array_dproduk_subtotal_net = json_decode(stripslashes($dproduk_subtotal_net));
		
		$dproduk_diskon = $_POST['dproduk_diskon']; 
		$array_dproduk_diskon = json_decode(stripslashes($dproduk_diskon));
		
		$dproduk_diskon_jenis = $_POST['dproduk_diskon_jenis']; 
		$array_dproduk_diskon_jenis = json_decode(stripslashes($dproduk_diskon_jenis));
		
		$dproduk_sales = $_POST['dproduk_sales']; 
		$array_dproduk_sales = json_decode(stripslashes($dproduk_sales));
		
		$cetak_jproduk=trim(@$_POST['cetak_jproduk']);
		
		$result=$this->m_master_jual_produk->detail_detail_jual_produk_insert($array_dproduk_id ,$dproduk_master ,$array_dproduk_karyawan, $array_dproduk_produk ,$array_dproduk_satuan ,$array_dproduk_jumlah ,$array_dproduk_harga ,$array_dproduk_subtotal_net ,$array_dproduk_diskon ,$array_dproduk_diskon_jenis ,$array_dproduk_sales ,$cetak_jproduk);
		echo $result;
	}
	
	
	function get_action(){
		$task = $_POST['task'];
		switch($task){
			case "LIST":
				$this->master_jual_produk_list();
				break;
			case "UPDATE":
				$this->master_jual_produk_update();
				break;
			case "CREATE":
				$this->master_jual_produk_create();
				break;
			case "CEK":
				$this->master_jual_produk_pengecekan();
				break;	
			case "DELETE":
				$this->master_jual_produk_delete();
				break;
			case "SEARCH":
				$this->master_jual_produk_search();
				break;
			case "PRINT":
				$this->master_jual_produk_print();
				break;
			case "EXCEL":
				$this->master_jual_produk_export_excel();
				break;
			case "BATAL":
				$this->master_jual_produk_batal();
				break;
			case "DDELETE":
				$this->detail_jual_produk_delete();
				break;
			default:
				echo "{failure:true}";
				break;
		}
	}
	
	function master_jual_produk_list(){
		
		$query = isset($_POST['query']) ? $_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$result=$this->m_master_jual_produk->master_jual_produk_list($query,$start,$end);
		echo $result;
	}

	function get_allkaryawan_list(){
		$query = isset($_POST['query']) ? $_POST['query'] : "";
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$result=$this->m_master_jual_produk->get_allkaryawan_list($query,$start,$end);
		echo $result;
	}
	
	function cek_userpass(){
		$username = (isset($_POST['username']) ? $_POST['username'] : $_GET['username']);
		$password = (isset($_POST['password']) ? $_POST['password'] : $_GET['password']);
		$result=$this->m_public_function->cek_userpass($username, $password, 37);
		echo $result;
	}
	
	function master_jual_produk_pengecekan(){
	
		$tanggal_pengecekan=trim(@$_POST["tanggal_pengecekan"]);
	
		$result=$this->m_public_function->pengecekan_dokumen($tanggal_pengecekan);
		echo $result;
	}
	
	function master_jual_produk_update(){
		$cetak_jproduk=trim(@$_POST["cetak_jproduk"]);
		$jproduk_id=trim(@$_POST["jproduk_id"]);
		$jproduk_grooming=trim(@$_POST["jproduk_grooming"]);
		$jproduk_nobukti=trim(@$_POST["jproduk_nobukti"]);
		$jproduk_nobukti=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_nobukti);
		$jproduk_nobukti=str_replace("'", '"',$jproduk_nobukti);
		
		$jproduk_cust=trim(@$_POST["jproduk_cust"]);
		$jproduk_tanggal=trim(@$_POST["jproduk_tanggal"]);
		$jproduk_diskon=trim(@$_POST["jproduk_diskon"]);
		
		$jproduk_stat_dok=trim(@$_POST["jproduk_stat_dok"]);
		$jproduk_stat_dok=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_stat_dok);
		$jproduk_stat_dok=str_replace("'", '"',$jproduk_stat_dok);
		
		$jproduk_cara=trim(@$_POST["jproduk_cara"]);
		$jproduk_cara=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara);
		$jproduk_cara=str_replace("'", '"',$jproduk_cara);
		
		$jproduk_cara2=trim(@$_POST["jproduk_cara2"]);
		$jproduk_cara2=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara2);
		$jproduk_cara2=str_replace("'", '"',$jproduk_cara2);
		
		$jproduk_cara3=trim(@$_POST["jproduk_cara3"]);
		$jproduk_cara3=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara3);
		$jproduk_cara3=str_replace("'", '"',$jproduk_cara3);
		
		$jproduk_keterangan=trim(@$_POST["jproduk_keterangan"]);
		$jproduk_keterangan=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_keterangan);
		$jproduk_keterangan=str_replace("'", '"',$jproduk_keterangan);
		
		$jproduk_ket_disk=trim(@$_POST["jproduk_ket_disk"]);
		$jproduk_ket_disk=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_ket_disk);
		$jproduk_ket_disk=str_replace("'", '"',$jproduk_ket_disk);
		
		$jproduk_cashback=trim($_POST["jproduk_cashback"]);
		$potong_point=trim($_POST["potong_point"]);
		
		//tunai
		$jproduk_tunai_nilai=trim($_POST["jproduk_tunai_nilai"]);
		//tunai-2
		$jproduk_tunai_nilai2=trim($_POST["jproduk_tunai_nilai2"]);
		//tunai-3
		$jproduk_tunai_nilai3=trim($_POST["jproduk_tunai_nilai3"]);
		//vgrooming
		$jproduk_vgrooming_nilai=trim(@$_POST["jproduk_vgrooming_nilai"]);
		//voucher
		$jproduk_voucher_no=trim($_POST["jproduk_voucher_no"]);
		$jproduk_voucher_cashback=trim($_POST["jproduk_voucher_cashback"]);
		//voucher-2
		$jproduk_voucher_no2=trim($_POST["jproduk_voucher_no2"]);
		$jproduk_voucher_cashback2=trim($_POST["jproduk_voucher_cashback2"]);
		//voucher-3
		$jproduk_voucher_no3=trim($_POST["jproduk_voucher_no3"]);
		$jproduk_voucher_cashback3=trim($_POST["jproduk_voucher_cashback3"]);
		
		//bayar
		$jproduk_bayar=trim($_POST["jproduk_bayar"]);
		$jproduk_subtotal=trim($_POST["jproduk_subtotal"]);
		$jproduk_total=trim($_POST["jproduk_total"]);
		$jproduk_hutang=trim($_POST["jproduk_hutang"]);
		//card
		$jproduk_card_nama=trim($_POST["jproduk_card_nama"]);
		$jproduk_card_edc=trim($_POST["jproduk_card_edc"]);
		$jproduk_card_promo=trim($_POST["jproduk_card_promo"]);
		$jproduk_card_no=trim($_POST["jproduk_card_no"]);
		$jproduk_card_nilai=trim($_POST["jproduk_card_nilai"]);
		//card-2
		$jproduk_card_nama2=trim($_POST["jproduk_card_nama2"]);
		$jproduk_card_edc2=trim($_POST["jproduk_card_edc2"]);
		$jproduk_card_promo2=trim($_POST["jproduk_card_promo2"]);
		$jproduk_card_no2=trim($_POST["jproduk_card_no2"]);
		$jproduk_card_nilai2=trim($_POST["jproduk_card_nilai2"]);
		//card-3
		$jproduk_card_nama3=trim($_POST["jproduk_card_nama3"]);
		$jproduk_card_edc3=trim($_POST["jproduk_card_edc3"]);
		$jproduk_card_promo3=trim($_POST["jproduk_card_promo3"]);
		$jproduk_card_no3=trim($_POST["jproduk_card_no3"]);
		$jproduk_card_nilai3=trim($_POST["jproduk_card_nilai3"]);
		//kwitansi
		$jproduk_kwitansi_no=trim($_POST["jproduk_kwitansi_no"]);
		$jproduk_kwitansi_nama=trim(@$_POST["jproduk_kwitansi_nama"]);
		$jproduk_kwitansi_nama=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_nama);
		$jproduk_kwitansi_nama=str_replace("'", '"',$jproduk_kwitansi_nama);
		$jproduk_kwitansi_nilai=trim($_POST["jproduk_kwitansi_nilai"]);
		$jproduk_kwitansi_jenis=trim(@$_POST["jproduk_kwitansi_jenis"]);
		$jproduk_kwitansi_jenis=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_jenis);
		$jproduk_kwitansi_jenis=str_replace("'", '"',$jproduk_kwitansi_jenis);
		$jual_kwitansi_id=trim($_POST["jual_kwitansi_id"]);
		//kwitansi-2
		$jproduk_kwitansi_no2=trim($_POST["jproduk_kwitansi_no2"]);
		$jproduk_kwitansi_nama2=trim(@$_POST["jproduk_kwitansi_nama2"]);
		$jproduk_kwitansi_nama2=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_nama2);
		$jproduk_kwitansi_nama2=str_replace("'", '"',$jproduk_kwitansi_nama2);
		$jproduk_kwitansi_nilai2=trim($_POST["jproduk_kwitansi_nilai2"]);
		$jproduk_kwitansi_jenis2=trim(@$_POST["jproduk_kwitansi_jenis2"]);
		$jproduk_kwitansi_jenis2=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_jenis2);
		$jproduk_kwitansi_jenis2=str_replace("'", '"',$jproduk_kwitansi_jenis2);
		$jual_kwitansi_id2=trim($_POST["jual_kwitansi_id2"]);
		//kwitansi-3
		$jproduk_kwitansi_no3=trim($_POST["jproduk_kwitansi_no3"]);
		$jproduk_kwitansi_nama3=trim(@$_POST["jproduk_kwitansi_nama3"]);
		$jproduk_kwitansi_nama3=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_nama3);
		$jproduk_kwitansi_nama3=str_replace("'", '"',$jproduk_kwitansi_nama3);
		$jproduk_kwitansi_nilai3=trim($_POST["jproduk_kwitansi_nilai3"]);
		$jproduk_kwitansi_jenis3=trim(@$_POST["jproduk_kwitansi_jenis3"]);
		$jproduk_kwitansi_jenis3=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_jenis3);
		$jproduk_kwitansi_jenis3=str_replace("'", '"',$jproduk_kwitansi_jenis3);
		$jual_kwitansi_id3=trim($_POST["jual_kwitansi_id3"]);
		//cek
		$jproduk_cek_nama=trim($_POST["jproduk_cek_nama"]);
		$jproduk_cek_no=trim($_POST["jproduk_cek_no"]);
		$jproduk_cek_valid=trim($_POST["jproduk_cek_valid"]);
		$jproduk_cek_bank=trim($_POST["jproduk_cek_bank"]);
		$jproduk_cek_nilai=trim($_POST["jproduk_cek_nilai"]);
		//cek-2
		$jproduk_cek_nama2=trim($_POST["jproduk_cek_nama2"]);
		$jproduk_cek_no2=trim($_POST["jproduk_cek_no2"]);
		$jproduk_cek_valid2=trim($_POST["jproduk_cek_valid2"]);
		$jproduk_cek_bank2=trim($_POST["jproduk_cek_bank2"]);
		$jproduk_cek_nilai2=trim($_POST["jproduk_cek_nilai2"]);
		//cek-3
		$jproduk_cek_nama3=trim($_POST["jproduk_cek_nama3"]);
		$jproduk_cek_no3=trim($_POST["jproduk_cek_no3"]);
		$jproduk_cek_valid3=trim($_POST["jproduk_cek_valid3"]);
		$jproduk_cek_bank3=trim($_POST["jproduk_cek_bank3"]);
		$jproduk_cek_nilai3=trim($_POST["jproduk_cek_nilai3"]);
		//transfer
		$jproduk_transfer_bank=trim($_POST["jproduk_transfer_bank"]);
		$jproduk_transfer_nama=trim($_POST["jproduk_transfer_nama"]);
		$jproduk_transfer_nilai=trim($_POST["jproduk_transfer_nilai"]);
		//transfer-2
		$jproduk_transfer_bank2=trim($_POST["jproduk_transfer_bank2"]);
		$jproduk_transfer_nama2=trim($_POST["jproduk_transfer_nama2"]);
		$jproduk_transfer_nilai2=trim($_POST["jproduk_transfer_nilai2"]);
		//transfer-3
		$jproduk_transfer_bank3=trim($_POST["jproduk_transfer_bank3"]);
		$jproduk_transfer_nama3=trim($_POST["jproduk_transfer_nama3"]);
		$jproduk_transfer_nilai3=trim($_POST["jproduk_transfer_nilai3"]);
		
		//Data Detail Penjualan Produk
		$dproduk_id = $_POST['dproduk_id']; 
		$dproduk_count = $_POST['dproduk_count']; 
		$dcount_dproduk_id = $_POST['dcount_dproduk_id']; 
		$array_dproduk_id = json_decode(stripslashes($dproduk_id));
		
		$dproduk_produk = $_POST['dproduk_produk']; 
		$array_dproduk_produk = json_decode(stripslashes($dproduk_produk));
		
		$dproduk_satuan = $_POST['dproduk_satuan']; 
		$array_dproduk_satuan = json_decode(stripslashes($dproduk_satuan));
		
		$dproduk_jumlah = $_POST['dproduk_jumlah']; 
		$array_dproduk_jumlah = json_decode(stripslashes($dproduk_jumlah));
		
		$dproduk_harga = $_POST['dproduk_harga']; 
		$array_dproduk_harga = json_decode(stripslashes($dproduk_harga));
		
		$dproduk_diskon_jenis = $_POST['dproduk_diskon_jenis']; 
		$array_dproduk_diskon_jenis = json_decode(stripslashes($dproduk_diskon_jenis));
		
		$dproduk_diskon = $_POST['dproduk_diskon']; 
		$array_dproduk_diskon = json_decode(stripslashes($dproduk_diskon));
		
		$dproduk_karyawan = $_POST['dproduk_karyawan']; 
		$array_dproduk_karyawan = json_decode(stripslashes($dproduk_karyawan));
		
		$result = $this->m_master_jual_produk->master_jual_produk_update($jproduk_id ,$jproduk_nobukti ,$jproduk_cust , $jproduk_tanggal
																		 ,$jproduk_stat_dok ,$jproduk_diskon ,$jproduk_cara ,$jproduk_cara2
																		 ,$jproduk_cara3 ,$jproduk_keterangan ,$jproduk_cashback, $potong_point
																		 ,$jproduk_tunai_nilai ,$jproduk_tunai_nilai2 ,$jproduk_tunai_nilai3, $jproduk_vgrooming_nilai,$jproduk_voucher_no ,$jproduk_voucher_cashback ,$jproduk_voucher_no2,$jproduk_voucher_cashback2 ,$jproduk_voucher_no3
																		 ,$jproduk_voucher_cashback3 ,$jproduk_bayar ,$jproduk_subtotal
																		 ,$jproduk_total ,$jproduk_hutang ,$jproduk_kwitansi_no
																		 ,$jproduk_kwitansi_nama ,$jproduk_kwitansi_nilai ,$jproduk_kwitansi_no2
																		 ,$jproduk_kwitansi_nama2 ,$jproduk_kwitansi_nilai2 ,$jproduk_kwitansi_no3
																		 ,$jproduk_kwitansi_nama3 ,$jproduk_kwitansi_nilai3 ,$jproduk_card_nama
																		 ,$jproduk_card_edc ,$jproduk_card_promo ,$jproduk_card_no ,$jproduk_card_nilai
																		 ,$jproduk_card_nama2 ,$jproduk_card_edc2 ,$jproduk_card_promo2 ,$jproduk_card_no2
																		 ,$jproduk_card_nilai2 ,$jproduk_card_nama3 ,$jproduk_card_edc3 ,$jproduk_card_promo3
																		 ,$jproduk_card_no3 ,$jproduk_card_nilai3 ,$jproduk_cek_nama
																		 ,$jproduk_cek_no ,$jproduk_cek_valid ,$jproduk_cek_bank
																		 ,$jproduk_cek_nilai ,$jproduk_cek_nama2 ,$jproduk_cek_no2
																		 ,$jproduk_cek_valid2 ,$jproduk_cek_bank2 ,$jproduk_cek_nilai2
																		 ,$jproduk_cek_nama3 ,$jproduk_cek_no3 ,$jproduk_cek_valid3
																		 ,$jproduk_cek_bank3 ,$jproduk_cek_nilai3 ,$jproduk_transfer_bank
																		 ,$jproduk_transfer_nama ,$jproduk_transfer_nilai ,$jproduk_transfer_bank2
																		 ,$jproduk_transfer_nama2 ,$jproduk_transfer_nilai2
																		 ,$jproduk_transfer_bank3 ,$jproduk_transfer_nama3 ,$jproduk_transfer_nilai3
																		 ,$cetak_jproduk ,$jproduk_ket_disk
																		 ,$array_dproduk_id ,$array_dproduk_produk ,$array_dproduk_satuan
																		 ,$array_dproduk_jumlah ,$array_dproduk_harga ,$array_dproduk_diskon_jenis
																		 ,$array_dproduk_diskon ,$array_dproduk_karyawan, $jproduk_grooming
																		 ,$jual_kwitansi_id, $jual_kwitansi_id2, $jual_kwitansi_id3
																		 ,$dproduk_count, $dcount_dproduk_id
																		 ,$jproduk_kwitansi_jenis,$jproduk_kwitansi_jenis2,$jproduk_kwitansi_jenis3);
		echo $result;
	}
	
	function master_jual_produk_create(){
		$cetak_jproduk=trim(@$_POST["cetak_jproduk"]);
		$jproduk_grooming=trim(@$_POST["jproduk_grooming"]);
		$jproduk_nobukti=trim(@$_POST["jproduk_nobukti"]);
		$jproduk_nobukti=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_nobukti);
		$jproduk_nobukti=str_replace("'", '"',$jproduk_nobukti);
		$jproduk_cust=trim(@$_POST["jproduk_cust"]);
		$jproduk_tanggal=trim(@$_POST["jproduk_tanggal"]);
		$jproduk_diskon=trim(@$_POST["jproduk_diskon"]);
		$jproduk_cara=trim(@$_POST["jproduk_cara"]);
		$jproduk_cara=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara);
		$jproduk_cara=str_replace("'", '"',$jproduk_cara);
		
		$jproduk_cara2=trim(@$_POST["jproduk_cara2"]);
		$jproduk_cara2=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara2);
		$jproduk_cara2=str_replace("'", '"',$jproduk_cara2);
		
		$jproduk_cara3=trim(@$_POST["jproduk_cara3"]);
		$jproduk_cara3=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara3);
		$jproduk_cara3=str_replace("'", '"',$jproduk_cara3);
		
		$jproduk_stat_dok=trim(@$_POST["jproduk_stat_dok"]);
		$jproduk_stat_dok=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_stat_dok);
		$jproduk_stat_dok=str_replace("'", '"',$jproduk_stat_dok);
		
		$jproduk_keterangan=trim(@$_POST["jproduk_keterangan"]);
		$jproduk_keterangan=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_keterangan);
		$jproduk_keterangan=str_replace("'", '"',$jproduk_keterangan);
		
		$jproduk_ket_disk=trim(@$_POST["jproduk_ket_disk"]);
		$jproduk_ket_disk=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_ket_disk);
		$jproduk_ket_disk=str_replace("'", '"',$jproduk_ket_disk);
		
		$jproduk_cashback=trim($_POST["jproduk_cashback"]);
		$potong_point=trim($_POST["potong_point"]);
		//$jproduk_voucher=trim($_POST["jproduk_voucher"]);
		//tunai
		$jproduk_tunai_nilai=trim($_POST["jproduk_tunai_nilai"]);
		//tunai-2
		$jproduk_tunai_nilai2=trim($_POST["jproduk_tunai_nilai2"]);
		//tunai-3
		$jproduk_tunai_nilai3=trim($_POST["jproduk_tunai_nilai3"]);
		//vgrooming
		$jproduk_vgrooming_nilai=trim(@$_POST["jproduk_vgrooming_nilai"]);
		//voucher
		$jproduk_voucher_no=trim($_POST["jproduk_voucher_no"]);
		$jproduk_voucher_cashback=trim($_POST["jproduk_voucher_cashback"]);
		//voucher-2
		$jproduk_voucher_no2=trim($_POST["jproduk_voucher_no2"]);
		$jproduk_voucher_cashback2=trim($_POST["jproduk_voucher_cashback2"]);
		//voucher-3
		$jproduk_voucher_no3=trim($_POST["jproduk_voucher_no3"]);
		$jproduk_voucher_cashback3=trim($_POST["jproduk_voucher_cashback3"]);
		//bayar
		$jproduk_bayar=trim($_POST["jproduk_bayar"]);
		$jproduk_subtotal=trim($_POST["jproduk_subtotal"]);
		$jproduk_total=trim($_POST["jproduk_total"]);
		$jproduk_hutang=trim($_POST["jproduk_hutang"]);
		//card
		$jproduk_card_nama=trim($_POST["jproduk_card_nama"]);
		$jproduk_card_edc=trim($_POST["jproduk_card_edc"]);
		$jproduk_card_promo=trim($_POST["jproduk_card_promo"]);
		$jproduk_card_no=trim($_POST["jproduk_card_no"]);
		$jproduk_card_nilai=trim($_POST["jproduk_card_nilai"]);
		//card-2
		$jproduk_card_nama2=trim($_POST["jproduk_card_nama2"]);
		$jproduk_card_edc2=trim($_POST["jproduk_card_edc2"]);
		$jproduk_card_promo2=trim($_POST["jproduk_card_promo2"]);
		$jproduk_card_no2=trim($_POST["jproduk_card_no2"]);
		$jproduk_card_nilai2=trim($_POST["jproduk_card_nilai2"]);
		//card-3
		$jproduk_card_nama3=trim($_POST["jproduk_card_nama3"]);
		$jproduk_card_edc3=trim($_POST["jproduk_card_edc3"]);
		$jproduk_card_promo3=trim($_POST["jproduk_card_promo3"]);
		$jproduk_card_no3=trim($_POST["jproduk_card_no3"]);
		$jproduk_card_nilai3=trim($_POST["jproduk_card_nilai3"]);
		//kwitansi
		$jproduk_kwitansi_no=trim($_POST["jproduk_kwitansi_no"]);
		$jproduk_kwitansi_nama=trim(@$_POST["jproduk_kwitansi_nama"]);
		$jproduk_kwitansi_nama=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_nama);
		$jproduk_kwitansi_nama=str_replace("'", '"',$jproduk_kwitansi_nama);
		$jproduk_kwitansi_nilai=trim($_POST["jproduk_kwitansi_nilai"]);
		$jproduk_kwitansi_jenis=trim(@$_POST["jproduk_kwitansi_jenis"]);
		$jproduk_kwitansi_jenis=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_jenis);
		$jproduk_kwitansi_jenis=str_replace("'", '"',$jproduk_kwitansi_jenis);
		$jual_kwitansi_id=trim($_POST["jual_kwitansi_id"]);
		//kwitansi-2
		$jproduk_kwitansi_no2=trim($_POST["jproduk_kwitansi_no2"]);
		$jproduk_kwitansi_nama2=trim(@$_POST["jproduk_kwitansi_nama2"]);
		$jproduk_kwitansi_nama2=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_nama2);
		$jproduk_kwitansi_nama2=str_replace("'", '"',$jproduk_kwitansi_nama2);
		$jproduk_kwitansi_nilai2=trim($_POST["jproduk_kwitansi_nilai2"]);
		$jproduk_kwitansi_jenis2=trim(@$_POST["jproduk_kwitansi_jenis2"]);
		$jproduk_kwitansi_jenis2=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_jenis2);
		$jproduk_kwitansi_jenis2=str_replace("'", '"',$jproduk_kwitansi_jenis2);
		$jual_kwitansi_id2=trim($_POST["jual_kwitansi_id2"]);
		//kwitansi-3
		$jproduk_kwitansi_no3=trim($_POST["jproduk_kwitansi_no3"]);
		$jproduk_kwitansi_nama3=trim(@$_POST["jproduk_kwitansi_nama3"]);
		$jproduk_kwitansi_nama3=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_nama3);
		$jproduk_kwitansi_nama3=str_replace("'", '"',$jproduk_kwitansi_nama3);
		$jproduk_kwitansi_nilai3=trim($_POST["jproduk_kwitansi_nilai3"]);
		$jproduk_kwitansi_jenis3=trim(@$_POST["jproduk_kwitansi_jenis3"]);
		$jproduk_kwitansi_jenis3=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_kwitansi_jenis3);
		$jproduk_kwitansi_jenis3=str_replace("'", '"',$jproduk_kwitansi_jenis3);
		$jual_kwitansi_id3=trim($_POST["jual_kwitansi_id3"]);
		//cek
		$jproduk_cek_nama=trim($_POST["jproduk_cek_nama"]);
		$jproduk_cek_no=trim($_POST["jproduk_cek_no"]);
		$jproduk_cek_valid=trim($_POST["jproduk_cek_valid"]);
		$jproduk_cek_bank=trim($_POST["jproduk_cek_bank"]);
		$jproduk_cek_nilai=trim($_POST["jproduk_cek_nilai"]);
		//cek-2
		$jproduk_cek_nama2=trim($_POST["jproduk_cek_nama2"]);
		$jproduk_cek_no2=trim($_POST["jproduk_cek_no2"]);
		$jproduk_cek_valid2=trim($_POST["jproduk_cek_valid2"]);
		$jproduk_cek_bank2=trim($_POST["jproduk_cek_bank2"]);
		$jproduk_cek_nilai2=trim($_POST["jproduk_cek_nilai2"]);
		//cek-3
		$jproduk_cek_nama3=trim($_POST["jproduk_cek_nama3"]);
		$jproduk_cek_no3=trim($_POST["jproduk_cek_no3"]);
		$jproduk_cek_valid3=trim($_POST["jproduk_cek_valid3"]);
		$jproduk_cek_bank3=trim($_POST["jproduk_cek_bank3"]);
		$jproduk_cek_nilai3=trim($_POST["jproduk_cek_nilai3"]);
		//transfer
		$jproduk_transfer_bank=trim($_POST["jproduk_transfer_bank"]);
		$jproduk_transfer_nama=trim($_POST["jproduk_transfer_nama"]);
		$jproduk_transfer_nilai=trim($_POST["jproduk_transfer_nilai"]);
		//transfer-2
		$jproduk_transfer_bank2=trim($_POST["jproduk_transfer_bank2"]);
		$jproduk_transfer_nama2=trim($_POST["jproduk_transfer_nama2"]);
		$jproduk_transfer_nilai2=trim($_POST["jproduk_transfer_nilai2"]);
		//transfer-3
		$jproduk_transfer_bank3=trim($_POST["jproduk_transfer_bank3"]);
		$jproduk_transfer_nama3=trim($_POST["jproduk_transfer_nama3"]);
		$jproduk_transfer_nilai3=trim($_POST["jproduk_transfer_nilai3"]);
		
		//Data Detail Penjualan Produk
		$dproduk_id = $_POST['dproduk_id']; 
		$array_dproduk_id = json_decode(stripslashes($dproduk_id));
		
		$dproduk_produk = $_POST['dproduk_produk']; 
		$array_dproduk_produk = json_decode(stripslashes($dproduk_produk));
		
		$dproduk_satuan = $_POST['dproduk_satuan']; 
		$array_dproduk_satuan = json_decode(stripslashes($dproduk_satuan));
		
		$dproduk_jumlah = $_POST['dproduk_jumlah']; 
		$array_dproduk_jumlah = json_decode(stripslashes($dproduk_jumlah));
		
		$dproduk_harga = $_POST['dproduk_harga']; 
		$array_dproduk_harga = json_decode(stripslashes($dproduk_harga));
		
		$dproduk_diskon_jenis = $_POST['dproduk_diskon_jenis']; 
		$array_dproduk_diskon_jenis = json_decode(stripslashes($dproduk_diskon_jenis));
		
		$dproduk_diskon = $_POST['dproduk_diskon']; 
		$array_dproduk_diskon = json_decode(stripslashes($dproduk_diskon));
		
		$dproduk_karyawan = $_POST['dproduk_karyawan']; 
		$array_dproduk_karyawan = json_decode(stripslashes($dproduk_karyawan));
		
		$app_cust_nama_baru=trim(@$_POST["app_cust_nama_baru"]);
		$app_cust_nama_baru=str_replace("/(<\/?)(p)([^>]*>)", "",$app_cust_nama_baru);
		$app_cust_nama_baru=str_replace("'", "",$app_cust_nama_baru);
		
		$app_cust_member_no_baru=trim(@$_POST["app_cust_member_no_baru"]);
		$app_cust_member_no_baru=str_replace("/(<\/?)(p)([^>]*>)", "",$app_cust_member_no_baru);
		$app_cust_member_no_baru=str_replace("'", "",$app_cust_member_no_baru);
		
		$app_cust_hp_baru=trim(@$_POST["app_cust_hp_baru"]);
		$app_cust_hp_baru=str_replace("/(<\/?)(p)([^>]*>)", "",$app_cust_hp_baru);
		$app_cust_hp_baru=str_replace("'", "",$app_cust_hp_baru);
		
		$member_ultah_baru=trim(@$_POST["member_ultah_baru"]);
		
		$result=$this->m_master_jual_produk->master_jual_produk_create($jproduk_nobukti ,$jproduk_cust ,$jproduk_tanggal
																	   ,$jproduk_stat_dok ,$jproduk_diskon ,$jproduk_cara
																	   ,$jproduk_cara2 ,$jproduk_cara3 ,$jproduk_keterangan
																	   ,$jproduk_cashback ,$potong_point, $jproduk_tunai_nilai ,$jproduk_tunai_nilai2
																	   ,$jproduk_tunai_nilai3 ,$jproduk_vgrooming_nilai, $jproduk_voucher_no,$jproduk_voucher_cashback ,$jproduk_voucher_no2 ,$jproduk_voucher_cashback2 ,$jproduk_voucher_no3,$jproduk_voucher_cashback3 ,$jproduk_bayar ,$jproduk_subtotal
																	   ,$jproduk_total ,$jproduk_hutang ,$jproduk_kwitansi_no
																	   ,$jproduk_kwitansi_nama ,$jproduk_kwitansi_nilai ,$jproduk_kwitansi_no2
																	   ,$jproduk_kwitansi_nama2 ,$jproduk_kwitansi_nilai2 ,$jproduk_kwitansi_no3
																	   ,$jproduk_kwitansi_nama3 ,$jproduk_kwitansi_nilai3 ,$jproduk_card_nama
																	   ,$jproduk_card_edc ,$jproduk_card_promo ,$jproduk_card_no ,$jproduk_card_nilai
																	   ,$jproduk_card_nama2 ,$jproduk_card_edc2 ,$jproduk_card_promo2 ,$jproduk_card_no2
																	   ,$jproduk_card_nilai2 ,$jproduk_card_nama3 ,$jproduk_card_edc3 ,$jproduk_card_promo3
																	   ,$jproduk_card_no3 ,$jproduk_card_nilai3 ,$jproduk_cek_nama
																	   ,$jproduk_cek_no ,$jproduk_cek_valid ,$jproduk_cek_bank
																	   ,$jproduk_cek_nilai ,$jproduk_cek_nama2 ,$jproduk_cek_no2
																	   ,$jproduk_cek_valid2 ,$jproduk_cek_bank2 ,$jproduk_cek_nilai2
																	   ,$jproduk_cek_nama3 ,$jproduk_cek_no3 ,$jproduk_cek_valid3
																	   ,$jproduk_cek_bank3 ,$jproduk_cek_nilai3 ,$jproduk_transfer_bank
																	   ,$jproduk_transfer_nama ,$jproduk_transfer_nilai ,$jproduk_transfer_bank2
																	   ,$jproduk_transfer_nama2 ,$jproduk_transfer_nilai2 ,$jproduk_transfer_bank3
																	   ,$jproduk_transfer_nama3 ,$jproduk_transfer_nilai3 ,$cetak_jproduk
																	   ,$jproduk_ket_disk
																	   ,$array_dproduk_id ,$array_dproduk_produk ,$array_dproduk_satuan
																	   ,$array_dproduk_jumlah ,$array_dproduk_harga ,$array_dproduk_diskon_jenis
																	   ,$array_dproduk_diskon ,$array_dproduk_karyawan, $jproduk_grooming
																	   , $jual_kwitansi_id, $jual_kwitansi_id2, $jual_kwitansi_id3
																	   ,$jproduk_kwitansi_jenis,$jproduk_kwitansi_jenis2,$jproduk_kwitansi_jenis3, $app_cust_member_no_baru, $member_ultah_baru, $app_cust_hp_baru, $app_cust_nama_baru);
		echo $result;
	}

	function master_jual_produk_delete(){
		$ids = $_POST['ids']; 
		$pkid = json_decode(stripslashes($ids));
		$result=$this->m_master_jual_produk->master_jual_produk_delete($pkid);
		echo $result;
	}
    
	function detail_jual_produk_delete(){
		$dproduk_id = trim(@$_POST["dproduk_id"]); 
		$result=$this->m_master_jual_produk->detail_jual_produk_delete($dproduk_id);
		echo $result;
	}
	
	function master_jual_produk_batal(){
		$jproduk_id=trim($_POST["jproduk_id"]);
		$jproduk_tanggal=trim(@$_POST["jproduk_tanggal"]);
		$result=$this->m_master_jual_produk->master_jual_produk_batal($jproduk_id, $jproduk_tanggal);
		echo $result;
	}

	function master_jual_produk_search(){
		$jproduk_nobukti=trim(@$_POST["jproduk_nobukti"]);
		$jproduk_nobukti=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_nobukti);
		$jproduk_nobukti=str_replace("'", '"',$jproduk_nobukti);
		$jproduk_cust=trim(@$_POST["jproduk_cust"]);
		$jproduk_tanggal=trim(@$_POST["jproduk_tanggal"]);
		$jproduk_tanggal_akhir=trim(@$_POST["jproduk_tanggal_akhir"]);
		$jproduk_diskon=trim(@$_POST["jproduk_diskon"]);
		$jproduk_cara=trim(@$_POST["jproduk_cara"]);
		$jproduk_cara=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara);
		$jproduk_cara=str_replace("'", '"',$jproduk_cara);
		$jproduk_keterangan=trim(@$_POST["jproduk_keterangan"]);
		$jproduk_keterangan=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_keterangan);
		$jproduk_keterangan=str_replace("'", '"',$jproduk_keterangan);
		$jproduk_stat_dok=trim(@$_POST["jproduk_stat_dok"]);
		$jproduk_stat_dok=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_stat_dok);
		$jproduk_stat_dok=str_replace("'", '"',$jproduk_stat_dok);
		$jproduk_sortby=trim(@$_POST["jproduk_sortby"]);
		$jproduk_sortby=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_sortby);
		$jproduk_sortby=str_replace("'", '"',$jproduk_sortby);
		
		$start = (integer) (isset($_POST['start']) ? $_POST['start'] : $_GET['start']);
		$end = (integer) (isset($_POST['limit']) ? $_POST['limit'] : $_GET['limit']);
		$result = $this->m_master_jual_produk->master_jual_produk_search($jproduk_nobukti, $jproduk_cust, $jproduk_tanggal, $jproduk_tanggal_akhir, $jproduk_diskon, $jproduk_cara, $jproduk_keterangan, $jproduk_stat_dok,$jproduk_sortby, $start, $end);
		echo $result;
	}


	function master_jual_produk_print(){
		$jproduk_nobukti=trim(@$_POST["jproduk_nobukti"]);
		$jproduk_nobukti=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_nobukti);
		$jproduk_nobukti=str_replace("'", '"',$jproduk_nobukti);
		$jproduk_cust=trim(@$_POST["jproduk_cust"]);
		$jproduk_tanggal=trim(@$_POST["jproduk_tanggal"]);
		$jproduk_tanggal_akhir=trim(@$_POST["jproduk_tanggal_akhir"]);
		$jproduk_diskon=trim(@$_POST["jproduk_diskon"]);
		$jproduk_cara=trim(@$_POST["jproduk_cara"]);
		$jproduk_cara=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara);
		$jproduk_cara=str_replace("'", '"',$jproduk_cara);
		$jproduk_keterangan=trim(@$_POST["jproduk_keterangan"]);
		$jproduk_keterangan=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_keterangan);
		$jproduk_keterangan=str_replace("'", '"',$jproduk_keterangan);
		$jproduk_stat_dok=trim(@$_POST["jproduk_stat_dok"]);
		$jproduk_stat_dok=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_stat_dok);
		$jproduk_stat_dok=str_replace("'", '"',$jproduk_stat_dok);
		$option=$_POST['currentlisting'];
		$filter=$_POST["query"];
		
		$data["data_print"] = $this->m_master_jual_produk->master_jual_produk_print($jproduk_nobukti, $jproduk_cust, $jproduk_tanggal, $jproduk_tanggal_akhir, $jproduk_diskon, $jproduk_cara, $jproduk_keterangan, $jproduk_stat_dok, $option, $filter);
		$print_view=$this->load->view("main/p_master_jual_produk.php",$data,TRUE);
		if(!file_exists("print")){
			mkdir("print");
		}
		$print_file=fopen("print/master_jual_produklist.html","w+");
		fwrite($print_file, $print_view);
		echo '1';
	}

	function master_jual_produk_export_excel(){
		$jproduk_nobukti=trim(@$_POST["jproduk_nobukti"]);
		$jproduk_nobukti=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_nobukti);
		$jproduk_nobukti=str_replace("'", '"',$jproduk_nobukti);
		$jproduk_cust=trim(@$_POST["jproduk_cust"]);
		$jproduk_tanggal=trim(@$_POST["jproduk_tanggal"]);
		$jproduk_tanggal_akhir=trim(@$_POST["jproduk_tanggal_akhir"]);
		$jproduk_diskon=trim(@$_POST["jproduk_diskon"]);
		$jproduk_cara=trim(@$_POST["jproduk_cara"]);
		$jproduk_cara=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_cara);
		$jproduk_cara=str_replace("'", '"',$jproduk_cara);
		$jproduk_keterangan=trim(@$_POST["jproduk_keterangan"]);
		$jproduk_keterangan=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_keterangan);
		$jproduk_keterangan=str_replace("'", '"',$jproduk_keterangan);
		$jproduk_stat_dok=trim(@$_POST["jproduk_stat_dok"]);
		$jproduk_stat_dok=str_replace("/(<\/?)(p)([^>]*>)", "",$jproduk_stat_dok);
		$jproduk_stat_dok=str_replace("'", '"',$jproduk_stat_dok);
		$option=$_POST['currentlisting'];
		$filter=$_POST["query"];
		$opsi_excel=(isset($_POST['opsi_excel']) ? @$_POST['opsi_excel'] : @$_GET['opsi_excel']);
		
		$query = $this->m_master_jual_produk->master_jual_produk_export_excel($jproduk_nobukti, $jproduk_cust, $jproduk_tanggal, $jproduk_tanggal_akhir, $jproduk_diskon, $jproduk_cara, $jproduk_keterangan, $jproduk_stat_dok, $option, $filter);
		$this->load->plugin('to_excel');
		to_excel($query,"master_jual_produk");
		echo '1';
			
	}
	
	function print_paper(){
		$jproduk_id=trim(@$_POST["jproduk_id"]);
		
		$result = $this->m_master_jual_produk->print_paper($jproduk_id);
		$iklan = $this->m_master_jual_produk->iklan();
		$rs=$result->row();
		$rsiklan=$iklan->row();
		$detail_jproduk=$result->result();
		
		$cara_bayar=$this->m_master_jual_produk->cara_bayar($jproduk_id);
		$cara_bayar2=$this->m_master_jual_produk->cara_bayar2($jproduk_id);
		$cara_bayar3=$this->m_master_jual_produk->cara_bayar3($jproduk_id);
		
		$data['jproduk_nobukti']=$rs->jproduk_nobukti;
		$data['jproduk_tanggal']=date('d-m-Y', strtotime($rs->jproduk_tanggal));
		$data['jproduk_jam']=date('d-m-Y H:i');
		$data['jproduk_karyawan']=$rs->jproduk_karyawan;
		$data['jproduk_karyawan_no']=$rs->jproduk_karyawan_no;
		$data['cust_member_no']=$rs->cust_member_no;
		$data['cust_nama']=$rs->cust_nama;
		$data['cust_preward_total']=$rs->cust_preward_total;
		$data['iklantoday_keterangan']=$rsiklan->iklantoday_keterangan;
		$data['cust_alamat']=$rs->cust_alamat;
		$data['jumlah_subtotal']=ubah_rupiah($rs->jumlah_subtotal);
		//$data['jumlah_tunai']=ubah_rupiah($rs->jtunai_nilai);
		$data['jumlah_bayar']=$rs->jproduk_bayar;
		$data['jproduk_diskon']=$rs->jproduk_diskon;
		$data['jproduk_cashback']=$rs->jproduk_cashback;
		$data['potong_point']=$rs->potong_point;
		$data['jproduk_creator']=$rs->jproduk_creator;
		//$data['jproduk_totalbiaya']=$rs->jproduk_totalbiaya;
		$data['detail_jproduk']=$detail_jproduk;
		
		if($cara_bayar!==NULL){
			if($cara_bayar->jproduk_cara=='gift_voucher'){
				$cara_bayar->jproduk_cara='Gift Voucher';
			}
		
			$data['cara_bayar1']=$cara_bayar->jproduk_cara;
			$data['nilai_bayar1']=$cara_bayar->bayar_nilai;
		}else{
			$data['cara_bayar1']="";
			$data['bayar_nilai1']="";
		}
		
		if($cara_bayar2!==NULL){
			if($cara_bayar2->jproduk_cara2=='gift_voucher'){
				$cara_bayar2->jproduk_cara2='Gift Voucher';
			}
			
			$data['cara_bayar2']=$cara_bayar2->jproduk_cara2;
			$data['nilai_bayar2']=$cara_bayar2->bayar2_nilai;
		}else{
			$data['cara_bayar2']="";
			$data['nilai_bayar2']="";
		}
		
		if($cara_bayar3!==NULL){
			if($cara_bayar3->jproduk_cara3=='gift_voucher'){
				$cara_bayar3->jproduk_cara3='Gift Voucher';
			}
		
			$data['cara_bayar3']=$cara_bayar3->jproduk_cara3;
			$data['nilai_bayar3']=$cara_bayar3->bayar3_nilai;
		}else{
			$data['cara_bayar3']="";
			$data['nilai_bayar3']="";
		}
		
		$viewdata=$this->load->view("main/jproduk_formcetak",$data,TRUE);
		$file = fopen("jproduk_paper.html",'w');
		fwrite($file, $viewdata);	
		fclose($file);
		echo '1';        
	}
	
	function print_paper2(){
		$jproduk_id=trim(@$_POST["jproduk_id"]);
		
		$result = $this->m_master_jual_produk->print_paper2($jproduk_id);
		$iklan = $this->m_master_jual_produk->iklan();
		$rs=$result->row();
		$rsiklan=$iklan->row();
		$detail_jproduk=$result->result();
		
		$cara_bayar=$this->m_master_jual_produk->cara_bayar($jproduk_id);
		$cara_bayar2=$this->m_master_jual_produk->cara_bayar2($jproduk_id);
		$cara_bayar3=$this->m_master_jual_produk->cara_bayar3($jproduk_id);
		
		$data['jproduk_nobukti']=$rs->jproduk_nobukti;
		$data['jproduk_tanggal']=date('d-m-Y', strtotime($rs->jproduk_tanggal));
		$data['jproduk_jam']=date('d-m-Y H:i');
		$data['jproduk_karyawan']=$rs->jproduk_karyawan;
		$data['jproduk_karyawan_no']=$rs->jproduk_karyawan_no;
		$data['cust_no']=$rs->cust_no;
		$data['cust_nama']=$rs->cust_nama;
		$data['iklantoday_keterangan']=$rsiklan->iklantoday_keterangan;
		$data['cust_alamat']=$rs->cust_alamat;
		$data['jumlah_subtotal']=ubah_rupiah($rs->jumlah_subtotal);
		$data['jumlah_bayar']=$rs->jproduk_bayar;
		$data['jproduk_diskon']=$rs->jproduk_diskon;
		$data['jproduk_cashback']=$rs->jproduk_cashback;
		$data['jproduk_creator']=$rs->jproduk_creator;
		$data['detail_jproduk']=$detail_jproduk;
		
		if($cara_bayar!==NULL){
			if($cara_bayar->jproduk_cara=='gift_voucher'){
				$cara_bayar->jproduk_cara='Gift Voucher';
			}
		
			$data['cara_bayar1']=$cara_bayar->jproduk_cara;
			$data['nilai_bayar1']=$cara_bayar->bayar_nilai;
		}else{
			$data['cara_bayar1']="";
			$data['bayar_nilai1']="";
		}
		
		if($cara_bayar2!==NULL){
			if($cara_bayar2->jproduk_cara2=='gift_voucher'){
				$cara_bayar2->jproduk_cara2='Gift Voucher';
			}
			
			$data['cara_bayar2']=$cara_bayar2->jproduk_cara2;
			$data['nilai_bayar2']=$cara_bayar2->bayar2_nilai;
		}else{
			$data['cara_bayar2']="";
			$data['nilai_bayar2']="";
		}
		
		if($cara_bayar3!==NULL){
			if($cara_bayar3->jproduk_cara3=='gift_voucher'){
				$cara_bayar3->jproduk_cara3='Gift Voucher';
			}
		
			$data['cara_bayar3']=$cara_bayar3->jproduk_cara3;
			$data['nilai_bayar3']=$cara_bayar3->bayar3_nilai;
		}else{
			$data['cara_bayar3']="";
			$data['nilai_bayar3']="";
		}

		$viewdata=$this->load->view("main/jproduk_formcetak2",$data,TRUE);
		$file = fopen("jproduk_paper2.html",'w');
		fwrite($file, $viewdata);	
		fclose($file);
		echo '1';        
	}
	
	function print_only(){
		$jproduk_id=trim(@$_POST["jproduk_id"]);
		
		$result = $this->m_master_jual_produk->print_paper($jproduk_id);
		$iklan = $this->m_master_jual_produk->iklan();
		$rs=$result->row();
		$rsiklan=$iklan->row();
		$detail_jproduk=$result->result();
		
		$cara_bayar=$this->m_master_jual_produk->cara_bayar($jproduk_id);
		$cara_bayar2=$this->m_master_jual_produk->cara_bayar2($jproduk_id);
		$cara_bayar3=$this->m_master_jual_produk->cara_bayar3($jproduk_id);
		
		$data['jproduk_nobukti']=$rs->jproduk_nobukti;
		$data['jproduk_jam']=date('d-m-Y H:i');
		$data['jproduk_karyawan']=$rs->jproduk_karyawan;
		$data['jproduk_karyawan_no']=$rs->jproduk_karyawan_no;
		$data['jproduk_tanggal']=date('d-m-Y', strtotime($rs->jproduk_tanggal));
		$data['cust_no']=$rs->cust_no;
		$data['cust_nama']=$rs->cust_nama;
		$data['iklantoday_keterangan']=$rsiklan->iklantoday_keterangan;
		$data['cust_alamat']=$rs->cust_alamat;
		$data['jumlah_subtotal']=ubah_rupiah($rs->jumlah_subtotal);
		$data['jumlah_bayar']=$rs->jproduk_bayar;
		$data['jproduk_diskon']=$rs->jproduk_diskon;
		$data['jproduk_cashback']=$rs->jproduk_cashback;
		$data['jproduk_creator']=$rs->jproduk_creator;
		$data['detail_jproduk']=$detail_jproduk;
		
		if($cara_bayar!==NULL){
			if($cara_bayar->jproduk_cara=='gift_voucher'){
				$cara_bayar->jproduk_cara='Gift Voucher';
			}
		
			$data['cara_bayar1']=$cara_bayar->jproduk_cara;
			$data['nilai_bayar1']=$cara_bayar->bayar_nilai;
		}else{
			$data['cara_bayar1']="";
			$data['bayar_nilai1']="";
		}
		
		if($cara_bayar2!==NULL){
			if($cara_bayar2->jproduk_cara2=='gift_voucher'){
				$cara_bayar2->jproduk_cara2='Gift Voucher';
			}
			
			$data['cara_bayar2']=$cara_bayar2->jproduk_cara2;
			$data['nilai_bayar2']=$cara_bayar2->bayar2_nilai;
		}else{
			$data['cara_bayar2']="";
			$data['nilai_bayar2']="";
		}
		
		if($cara_bayar3!==NULL){
			if($cara_bayar3->jproduk_cara3=='gift_voucher'){
				$cara_bayar3->jproduk_cara3='Gift Voucher';
			}
		
			$data['cara_bayar3']=$cara_bayar3->jproduk_cara3;
			$data['nilai_bayar3']=$cara_bayar3->bayar3_nilai;
		}else{
			$data['cara_bayar3']="";
			$data['nilai_bayar3']="";
		}
			
		$viewdata=$this->load->view("main/jproduk_formcetak_printonly",$data,TRUE);
		$file = fopen("jproduk_paper.html",'w');
		fwrite($file, $viewdata);	
		fclose($file);
		echo '1';        
	}
	
	function print_only2(){
		$jproduk_id=trim(@$_POST["jproduk_id"]);		
		$result = $this->m_master_jual_produk->print_paper2($jproduk_id);
		$iklan = $this->m_master_jual_produk->iklan();
		$jrawat_cust=trim(@$_POST["jrawat_cust"]);
		$jproduk_tanggal=trim(@$_POST["jproduk_tanggal"]);
		$option=trim(@$_POST["option"]);
		$mode='printonly';
		
		$rs=$result->row();
		$rsiklan=$iklan->row();
		$detail_jproduk=$result->result();
		
		$ttd = 'Acc';
		
		$cara_bayar=$this->m_master_jual_produk->cara_bayar($jproduk_id);
		$cara_bayar2=$this->m_master_jual_produk->cara_bayar2($jproduk_id);
		$cara_bayar3=$this->m_master_jual_produk->cara_bayar3($jproduk_id);
		
		$data['jproduk_nobukti']=$rs->jproduk_nobukti;
		$data['jproduk_jam']=date('d-m-Y H:i');
		$data['jproduk_karyawan']=$rs->jproduk_karyawan;
		$data['jproduk_karyawan_no']=$rs->jproduk_karyawan_no;
		$data['jproduk_tanggal']=date('d-m-Y', strtotime($rs->jproduk_tanggal));
		$data['cust_no']=$rs->cust_no;
		$data['cust_nama']=$rs->cust_nama;
		$data['iklantoday_keterangan']=$rsiklan->iklantoday_keterangan;
		$data['cust_alamat']=$rs->cust_alamat;
		$data['jumlah_subtotal']=ubah_rupiah($rs->jumlah_subtotal);
		$data['jumlah_bayar']=$rs->jproduk_bayar;
		$data['jproduk_diskon']=$rs->jproduk_diskon;
		$data['jproduk_cashback']=$rs->jproduk_cashback;
		$data['detail_jproduk']=$detail_jproduk;
		
		if($cara_bayar!==NULL){
			if($cara_bayar->jproduk_cara=='gift_voucher'){
				$cara_bayar->jproduk_cara='Gift Voucher';
			}
		
			$data['cara_bayar1']=$cara_bayar->jproduk_cara;
			$data['nilai_bayar1']=$cara_bayar->bayar_nilai;
		}else{
			$data['cara_bayar1']="";
			$data['bayar_nilai1']="";
		}
		
		if($cara_bayar2!==NULL){
			if($cara_bayar2->jproduk_cara2=='gift_voucher'){
				$cara_bayar2->jproduk_cara2='Gift Voucher';
			}
			
			$data['cara_bayar2']=$cara_bayar2->jproduk_cara2;
			$data['nilai_bayar2']=$cara_bayar2->bayar2_nilai;
		}else{
			$data['cara_bayar2']="";
			$data['nilai_bayar2']="";
		}
		
		if($cara_bayar3!==NULL){
			if($cara_bayar3->jproduk_cara3=='gift_voucher'){
				$cara_bayar3->jproduk_cara3='Gift Voucher';
			}
		
			$data['cara_bayar3']=$cara_bayar3->jproduk_cara3;
			$data['nilai_bayar3']=$cara_bayar3->bayar3_nilai;
		}else{
			$data['cara_bayar3']="";
			$data['nilai_bayar3']="";
		}
			
		$info = $this->m_public_function->get_info();
		$data['info_nama'] = @$info->info_nama;
		$data['info_alamat'] = @$info->info_alamat;
		
		if($option=='print_only2'){
			$viewdata=$this->load->view("main/jproduk_formcetak_printonly2",$data,TRUE);
			$file = fopen("jproduk_paper2.html",'w');
			fwrite($file, $viewdata);	
			fclose($file);
			echo '1';   
		}else{
			$tgl = date('d', strtotime($data['jproduk_tanggal']));
			$bulan = date('m', strtotime($data['jproduk_tanggal']));
			$tahun = date('Y', strtotime($data['jproduk_tanggal']));
			
			if($bulan == 01){
				$bulan = ' Januari ';
			}else if($bulan == 02){
				$bulan = ' Februari ';
			}else if($bulan == 03){
				$bulan = ' Maret ';
			}else if($bulan == 04){
				$bulan = ' April ';
			}else if($bulan == 05){
				$bulan = ' Mei ';
			}else if($bulan == 06){
				$bulan = ' Juni ';
			}else if($bulan == 07){
				$bulan = ' Juli ';
			}else if($bulan == 08){
				$bulan = ' Agustus ';
			}else if($bulan == 09){
				$bulan = ' September ';
			}else if($bulan == 10){
				$bulan = ' Oktober ';
			}else if($bulan == 11){
				$bulan = ' November ';
			}else if($bulan == 12){
				$bulan = ' Desember ';
			}
			
			$data['jproduk_tanggal'] = $tgl.$bulan.$tahun;
			$data['ttd'] = $ttd;
			
			$viewdata=$this->load->view("main/jproduk_formcetak_fp",$data,TRUE);
			$file = fopen("jproduk_print_fp.html",'w');
			fwrite($file, $viewdata);	
			fclose($file);
			echo '2';   
		}		
	}
	
	function JEncode($arr){
		if (version_compare(PHP_VERSION,"5.2","<"))
		{    
			require_once("./JSON.php"); //if php<5.2 need JSON class
			$json = new Services_JSON();//instantiate new json object
			$data=$json->encode($arr);  //encode the data in json format
		} else {
			$data = json_encode($arr);  //encode the data in json format
		}
		return $data;
	}
	
	function JDecode($arr){
		if (version_compare(PHP_VERSION,"5.2","<"))
		{    
			require_once("./JSON.php"); //if php<5.2 need JSON class
			$json = new Services_JSON();//instantiate new json object
			$data=$json->decode($arr);  //decode the data in json format
		} else {
			$data = json_decode($arr);  //decode the data in json format
		}
		return $data;
	}
	
	// Encodes a YYYY-MM-DD into a MM-DD-YYYY string
	function codeDate ($date) {
	  $tab = explode ("-", $date);
	  $r = $tab[1]."/".$tab[2]."/".$tab[0];
	  return $r;
	}
	
}
?>