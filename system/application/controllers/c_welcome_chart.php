<?php

//class of info
class C_welcome_chart extends Controller {

	//constructor
	function C_welcome_chart(){
		parent::Controller();
		session_start();
		$this->load->model('m_tutup', '', TRUE);
	}
	
	//set index
	function index(){
		$this->load->helper('asset');
		$this->load->view('main/v_welcome_chart');
	}		
	
	//event handler action
	function get_action(){
	    $task = $_POST['task'];
	    switch($task){
		case "SEARCH":
		    $this->welcome_chart_search();
		    break;
		case "UPDATE":
		    $this->welcome_chart_update('', '');
		    break;
		case "RECALC":
		    $this->welcome_chart_recalc('', '');
		    break;
		case "KONTRIBUSI_MEDIS":
		    $this->welcome_chart_kontribusi_medis();
		    break;
		case "KONTRIBUSI_MEDIS_UPDATE":
		    $this->welcome_chart_kontribusi_medis_update('', '', 0, '');
		    break;
		case "KONTRIBUSI_NON_MEDIS":
		    $this->welcome_chart_kontribusi_non_medis();
		    break;
		case "KONTRIBUSI_NON_MEDIS_UPDATE":
		    $this->welcome_chart_kontribusi_non_medis_update('', '', 0, '');
		    break;
		case "KONTRIBUSI_PRODUK":
		    $this->welcome_chart_kontribusi_produk();
		    break;
		case "KONTRIBUSI_PRODUK_UPDATE":
		    $this->welcome_chart_kontribusi_produk_update('', '', 0, '');
		    break;
		case "TOP_SPENDER_NETSALES":
		    $this->welcome_chart_top_spender_netsales();
		    break;
		case "TOP_SPENDER_NETSALES_UPDATE":
		    $this->welcome_chart_top_spender_netsales_update('', '', '', '');
		    break;
		case "TOP_SPENDER_CASHIN":
		    $this->welcome_chart_top_spender_cashin();
		    break;
		case "TOP_SPENDER_CASHIN_UPDATE":
		    $this->welcome_chart_top_spender_cashin_update('', '', '', '');
		    break;
		case "STOK_MINIMUM":
		    $this->welcome_chart_stok_minimum();
		    break;
		case "STOK_MINIMUM_UPDATE":
		    $this->welcome_chart_stok_minimum_update('', '', 0, '');
		    break;
		case "JATUH_TEMPO":
		    $this->welcome_chart_jatuh_tempo();
		    break;
		case "ULTAH":
		    $this->welcome_chart_ultah();
		    break;
		case "JATUH_TEMPO_UPDATE":
		    $this->welcome_chart_jatuh_tempo_update('', '', 0, '');
		    break;
		case "REKAP_JUAL":
		    $this->welcome_chart_rekap_jual();
		    break;
		case "REKAP_JUAL_UPDATE":
		    $this->welcome_chart_rekap_jual_update('', '', '', '', '', '', '');
		    break;
		case "CHART_KUNJUNGAN":
		    $this->prepare_chart_kunjungan('');
		    break;
		case "CHART_SPENDING_AVG":
		    $this->prepare_chart_spending_avg();
		    break;
		case "CHART_NETSALES":
		    $this->prepare_chart_netsales();
		    break;
		case "CHART_NETSALES_PRODUK":
		    $this->prepare_chart_netsales_produk();
		    break;
		case "CHART_PD_NETSALES";
		    $this->prepare_chart_product_dokter_netsales();
		    break;
		case "CEK_TUTUP_BUKU";
		    $this->cek_tutup_buku();
		    break;
		default:
		    echo "{failure:true}";
		    break;
	    }
	}
	
	function cek_tutup_buku(){
		$bulan		= (isset($_POST['bulan']) ? @$_POST['bulan'] : @$_GET['bulan']);
		$tahun		= (isset($_POST['tahun']) ? @$_POST['tahun'] : @$_GET['tahun']);
		
		$result		= $this->m_tutup->cek_tutup_buku($bulan, $tahun);
		echo $result; 
	}

	function welcome_chart_search(){
		$bulan_awal		= (isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal		= (isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir	= (isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir 	= (isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		$tgl_awal	= $tahun_awal."-".$bulan_awal;
		$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
		
		$result		= $this->m_public_function->welcome_chart_search($tgl_awal, $tgl_akhir);
		
		$this->load->library('highcharts');

		$result_data = explode(",",$result,2);
		$count = strlen($result_data[1]) - 1;
		$data_parse = json_decode("{".substr($result_data[1], 0, $count), true);
		$arr_data = $data_parse['results'];
		
		$t_netsales_produk = 0;
		$t_netsales_total = 0;
		$t_kunjungan_total = 0;

		for($i=0;$i<count($arr_data);$i++){
			if($arr_data[$i]['td_kategori'] == 'Penjualan Produk')
				$t_netsales_produk = $arr_data[$i]['td_pencapaian'];
			else if($arr_data[$i]['td_kategori'] == 'Total Penjualan')
				$t_netsales_total = $arr_data[$i]['td_pencapaian'];
			//else if($arr_data[$i]['td_kategori'] == 'Kunjungan')
			//	$t_kunjungan_total = $arr_data[$i]['td_pencapaian'];
		}

		if($t_netsales_total == 0)
			$t_netsales_total = 1;

		if($t_kunjungan_total == 0)
			$t_kunjungan_total = 1;
	
	/*	$serieskey[0] = "Kunjungan Cust Baru";
		$seriesvalue[0] = round($t_kunjungan_baru / $t_kunjungan_total, 1);

		$serieskey[1] = "Kunjungan Cust Lama";
		$seriesvalue[1] = round($t_kunjungan_lama / $t_kunjungan_total, 1);*/
		
		$data['title'] = "Kunjungan";
		$data['chartWidth'] = "220";
		$data['chartHeight'] = "250";		
	//	$data['series'] = $serieskey;
	//	$data['seriesvalue'] = $seriesvalue;
		$data['seriesname'] = "Total";		
		
		$print_view=$this->load->view("main/template_chart_pie_legend.php",$data,TRUE);
	
		if(!file_exists("print")){
		    mkdir("print");
		}
		
		$filename = "print/proporsi_kunjungan_graph.php";
		
		if(file_exists($filename)){
		    unlink($filename);
		}

		$print_file=fopen("print/proporsi_kunjungan_graph.php","w+");
		
		$fwrite = fwrite($print_file, $print_view);
				
		/*$serieskey[0] = "Net Sales Medis";
		$seriesvalue[0] = round($t_netsales_medis / $t_netsales_total, 1);

		$serieskey[1] = "Net Sales Estetik";
		$seriesvalue[1] = round($t_netsales_estetik / $t_netsales_total, 1);*/

		$serieskey[2] = "Penjualan Produk";
		$seriesvalue[2] = round($t_netsales_produk / $t_netsales_total, 1);
		
		$data['title'] = "Net Sales";
		$data['chartWidth'] = "220";
		$data['chartHeight'] = "250";		
		$data['series'] = $serieskey;
		$data['seriesvalue'] = $seriesvalue;
		$data['seriesname'] = "Total";		
		
		$print_view=$this->load->view("main/template_chart_pie_legend.php",$data,TRUE);
	
		if(!file_exists("print")){
		    mkdir("print");
		}
		
		$filename = "print/proporsi_netsales_graph.php";
		
		if(file_exists($filename)){
		    unlink($filename);
		}

		$print_file=fopen("print/proporsi_netsales_graph.php","w+");
		
		$fwrite = fwrite($print_file, $print_view);
			
		echo $result; 
	}

	function welcome_chart_rekap_jual(){
		$tgl_awal = (isset($_POST['rekap_penjualan_tglapp_start']) ? @$_POST['rekap_penjualan_tglapp_start'] : @$_GET['rekap_penjualan_tglapp_start']);
		$tgl_akhir = (isset($_POST['rekap_penjualan_tglapp_end']) ? @$_POST['rekap_penjualan_tglapp_end'] : @$_GET['rekap_penjualan_tglapp_end']);
		$orderby = (isset($_POST['orderby']) ? @$_POST['orderby'] : @$_GET['orderby']);
		$rekap_penjualan_jenis = (isset($_POST['rekap_penjualan_jenis']) ? @$_POST['rekap_penjualan_jenis'] : @$_GET['rekap_penjualan_jenis']);
		
		/*if($rekap_penjualan_jenis == 'Perawatan_dan_Paket')
			$t_jenis = 'perawatan';
		else*/
			$t_jenis = strtolower($rekap_penjualan_jenis);

		$arr_data = array();
		
		//if($rekap_penjualan_jenis == 'Produk'){
			$label = 'trjp';
		/*} else if($rekap_penjualan_jenis == 'Perawatan_dan_Paket'){
			$label = 'trjr';
		} else {
			$label = 'trjk';
		}*/

		if($orderby == 'rp')
			$ordercol = $label.'_tot_net';
		else
			$ordercol = $label.'_tot_jum_item';

		if (substr($tgl_awal, 0, 7) != substr($tgl_akhir, 0, 7)){
			$sql = "select 
					".$label."_kode, ".$label."_nama, sum(".$label."_tot_jum_item) as ".$label."_tot_jum_item,
					sum(".$label."_tot_net) as ".$label."_tot_net, ".$label."_date_update
				from 
					temp_rekap_jual_".$t_jenis."
				where 
					".$label."_periode_awal = '".substr($tgl_awal, 0, 7)."' 
					and ".$label."_periode_akhir = '".substr($tgl_akhir, 0, 7)."'
				group by ".$label."_kode
				order by ".$ordercol." desc
				limit 10";
			$arr_data = $this->db->query($sql)->result_array();
		}else{
			$sql = "select 
					".$label."_kode, ".$label."_nama, sum(".$label."_tot_jum_item) as ".$label."_tot_jum_item,
					sum(".$label."_tot_net) as ".$label."_tot_net, ".$label."_date_update
				from 
					temp_rekap_jual_".$t_jenis."
				where 
					".$label."_periode_awal >= '".substr($tgl_awal, 0, 7)."' 
					and ".$label."_periode_akhir <= '".substr($tgl_akhir, 0, 7)."'
					and ".$label."_periode_awal = ".$label."_periode_akhir
				group by ".$label."_kode
				order by ".$ordercol." desc
				limit 10";
			$arr_data = $this->db->query($sql)->result_array();
		}
		
		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}
	
	function welcome_chart_recalc($tgl_awal, $tgl_akhir){
		set_time_limit(1200);
		$bulan_awal		= (isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal		= (isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir	= (isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir 	= (isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		if(empty($tgl_awal) && empty($tgl_akhir)){
			$tgl_awal	= $tahun_awal."-".$bulan_awal;
			$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;	
		}
		
		$result		= $this->m_public_function->laporan_netsales_recalc($bulan_awal, $tahun_awal, $tgl_awal, $tgl_akhir);
		
		return 1; 
	}

	function welcome_chart_update($tgl_awal, $tgl_akhir){
		$bulan_awal		= (isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal		= (isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir	= (isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir 	= (isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		if(empty($tgl_awal) && empty($tgl_akhir)){
			$tgl_awal	= $tahun_awal."-".$bulan_awal;
			$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;	
		}
		
		$result		= $this->m_public_function->welcome_chart_update($tgl_awal, $tgl_akhir);
		
		return 1; 
	}

	function welcome_chart_generate_daily(){
		$tgl_awal = date('Y-m');
		$tgl_akhir = date('Y-m');
		$start = 0;
		$limit = 10;

		$generate_pencapaian = $this->welcome_chart_update($tgl_awal, $tgl_akhir);
		
		$tgl_awal = date('Y-m-01');
		$tgl_akhir = date('Y-m-t');
		$limit = 1000;
		
		$generate_kontribusi_produk = $this->welcome_chart_kontribusi_produk_update($tgl_awal, $tgl_akhir, $start, $limit);

		$limit = 10;
		$generate_top_spender_netsales = $this->welcome_chart_top_spender_netsales_update($tgl_awal, $tgl_akhir, $start, $limit);
		$generate_top_spender_cashin = $this->welcome_chart_top_spender_cashin_update($tgl_awal, $tgl_akhir, $start, $limit);
		$generate_stok_minimum = $this->welcome_chart_stok_minimum_update($tgl_awal, $tgl_akhir, $start, $limit);
		$generate_jatuh_tempo = $this->welcome_chart_jatuh_tempo_update($tgl_awal, $tgl_akhir, $start, $limit);
		$generate_rekap_jual_produk = $this->welcome_chart_rekap_jual_update($tgl_awal, $tgl_akhir, $start, $limit, 'Produk', 'rp');

		$sql_log = "insert into log_system (log_procedure_name, log_execution_time) values ('generate_dashboard', now());";
		$log = $this->db->query($sql_log);
	}

	function welcome_chart_kontribusi_medis_update($tgl_awal, $tgl_akhir, $start, $limit){
		set_time_limit(1200);
		$end = $limit;
		
		if(empty($tgl_awal))
			$tgl_awal = (isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);

		if(empty($tgl_akhir))
			$tgl_akhir = (isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);

		if(empty($start) && $start != 0)
			$start = (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);

		if(empty($limit))
			$end = (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);

		$arr_rp = array();
		$arr_dokter = array();
		$arr_data = array();
		$t_total_kontribusi = 0;

		$this->load->model('m_lap_jum_tindakan_all_dokter', '', TRUE);
		$this->load->model('m_lap_jum_cust_dokter', '', TRUE);

		$bulan = date('m');
		$cabang = 'default';
		$opsi_periode = $periode = 'tanggal';
		$opsi_perawatan = 'semua';
		$report_rp = 1;
		$report_group1 = $report_group2 = 0;
		$report_groupby = 'Semua';
		$tanpa_perawatan_lain = 'false';
		$tahun = date('Y');
		$group1 = $perawatan = $trawat_dokter = '';
		$tglstart = $trawat_tglapp_start = $tgl_awal;
		$tglend = $trawat_tglapp_end = $tgl_akhir;
		
		$result_dokter = $this->m_lap_jum_tindakan_all_dokter->report_daftar_dokter($tgl_awal,$periode, $trawat_tglapp_start ,$trawat_tglapp_end ,$trawat_dokter, $report_rp, $report_group1, $report_group2, $report_groupby, $start,$end, $cabang, 0);
		
		$result_data_dokter = explode(",",$result_dokter,2);
		$count_dokter = strlen($result_data_dokter[1]) - 1;
		$data_parse_dokter = json_decode("{".substr($result_data_dokter[1], 0, $count_dokter), true);
		$arr_dokter = $data_parse_dokter['results'];
		$arr_dokter_search = array();

		if(!empty($arr_dokter)){
			for($i=0;$i<count($arr_dokter);$i++){
				$arr_dokter_search[$i] = $arr_dokter[$i]['dokter_id'];
			}
		}

		$result_rp = $this->m_lap_jum_tindakan_all_dokter->report_tindakan_search($tgl_awal,$periode, $trawat_tglapp_start ,$trawat_tglapp_end, $report_rp, $report_group1, $report_group2, $report_groupby, $start, $end, $cabang, $arr_dokter_search);
		
		$result_data_rp = explode(",",$result_rp,2);
		$count_rp = strlen($result_data_rp[1]) - 1;
		$data_parse_rp = json_decode("{".substr($result_data_rp[1], 0, $count_rp), true);
		$arr_rp = $data_parse_rp['results'];
		$dashboard = true;

		$result_cust = $this->m_lap_jum_cust_dokter->laporan_jum_cust_dokter($opsi_periode, $tglstart, $tglend, $bulan, $tahun, $opsi_perawatan, $perawatan, $tanpa_perawatan_lain, $group1, $cabang, $dashboard);
		
		$result_data_cust = explode(",",$result_cust,2);
		$count_cust = strlen($result_data_cust[1]) - 1;
		$data_parse_cust = json_decode("{".substr($result_data_cust[1], 0, $count_cust), true);
		$arr_cust = $data_parse_cust['results'];

		if(!empty($arr_dokter)){
			for($i=0;$i<count($arr_dokter);$i++) {
				$t_ref = 'tjt_ref'.$i;
				$arr_data[$i]['tkm_karyawan_id'] = $arr_dokter[$i]['dokter_id'];
				$arr_data[$i]['tkm_karyawan_nama'] = $arr_dokter[$i]['karyawan_nama'];
				$arr_data[$i]['tkm_kontribusi_rp'] = $arr_rp[count($arr_rp)-1][$t_ref];
				$arr_data[$i]['tkm_total_cust'] = 0;

				foreach($arr_cust as $idx => $value) {
					if($value['karyawan_id'] == $arr_data[$i]['tkm_karyawan_id'])
						$arr_data[$i]['tkm_total_cust'] = $value['total'];
				}

				$t_total_cust = $arr_data[$i]['tkm_total_cust'];
				if($t_total_cust == 0)
					$t_total_cust = 1;
				
				$arr_data[$i]['tkm_spending_avg'] = round($arr_data[$i]['tkm_kontribusi_rp'] / $t_total_cust);

				$t_total_kontribusi += $arr_rp[count($arr_rp)-1][$t_ref];
			}

			for($i=0;$i<count($arr_dokter);$i++) {
				if($t_total_kontribusi == 0)
					$t_total_kontribusi = 1;

				$arr_data[$i]['tkm_kontribusi_persen'] = round((($arr_data[$i]['tkm_kontribusi_rp'] / $t_total_kontribusi) * 100), 2);
			}
		}
		
		$sql = "delete from temp_kontribusi_medis where tkm_periode_awal = '".substr($tgl_awal, 0, 7)."' and tkm_periode_akhir = '".substr($tgl_akhir, 0, 7)."'";
		$del_temp = $this->db->query($sql);

		foreach($arr_data as $idx => $row){
			$row['tkm_periode_awal'] = $tgl_awal;
			$row['tkm_periode_akhir'] = $tgl_akhir;
			$row['tkm_date_update'] = date('Y-m-d H:i:s');
			$this->db->insert('temp_kontribusi_medis', $row);
		}

		return 1;
	}

	function welcome_chart_kontribusi_medis(){
		$tgl_awal = (isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);
		$tgl_akhir = (isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);
		$t_arr_data = $arr_data = array();
		$t_total_kontribusi = 0;
		
		$sql = "select 
					tkm_karyawan_id, tkm_karyawan_nama, sum(tkm_kontribusi_rp) as tkm_kontribusi_rp, sum(tkm_total_cust) as tkm_total_cust
					,(sum(tkm_kontribusi_rp) / sum(tkm_total_cust)) as tkm_spending_avg, tkm_date_update
				from 
					temp_kontribusi_medis 
				where 
					tkm_periode_awal >= '".substr($tgl_awal, 0, 7)."' 
					and tkm_periode_akhir <= '".substr($tgl_akhir, 0, 7)."'
					and tkm_periode_awal = tkm_periode_akhir
				group by tkm_karyawan_id
				order by tkm_kontribusi_rp desc";
		$t_arr_data = $this->db->query($sql)->result_array();

		$i = 0;
		foreach($t_arr_data as $value){
			$arr_data[$i] = $value;
			$t_total_kontribusi += $value['tkm_kontribusi_rp'];
			$i++;
		}

		$i = 0;
		foreach($arr_data as $value){
			$arr_data[$i]['tkm_kontribusi_persen'] = round((($value['tkm_kontribusi_rp'] / $t_total_kontribusi) * 100), 2);
			$i++;
		}

		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}

	function welcome_chart_kontribusi_non_medis_update($tgl_awal, $tgl_akhir, $start, $limit){
		set_time_limit(1200);
		$end = $limit;
		
		if(empty($tgl_awal))
			$tgl_awal = (isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);

		if(empty($tgl_akhir))
			$tgl_akhir = (isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);

		if(empty($start) && $start != 0)
			$start = (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);

		if(empty($limit))
			$end = (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);

		$arr_rp = array();
		$arr_dokter = array();
		$arr_cust_dokter = array();
		$arr_data = array();
		$t_total_kontribusi = 0;

		$this->load->model('m_lap_jum_tindakan_all_therapis', '', TRUE);

		$bulan = date('m');
		$cabang = 'default';
		$opsi_periode = $periode = 'tanggal';
		$opsi_perawatan = 'semua';
		$report_rp = 1;
		$report_group2 = $report_group1 = 0;
		$report_groupby = 'Semua';
		$tanpa_perawatan_lain = 'false';
		$tahun = date('Y');
		$group1 = $perawatan = $trawat_dokter = '';
		$tglstart = $trawat_tglapp_start = $tgl_awal;
		$tglend = $trawat_tglapp_end = $tgl_akhir;
		
		$result_dokter = $this->m_lap_jum_tindakan_all_therapis->report_daftar_dokter($tgl_awal, $periode, $trawat_tglapp_start, $trawat_tglapp_end, $trawat_dokter, $report_rp, $report_group1, $report_group2, $report_groupby, $start, $end, $cabang, 0);
		
		$result_data_dokter = explode(",",$result_dokter,2);
		$count_dokter = strlen($result_data_dokter[1]) - 1;
		$data_parse_dokter = json_decode("{".substr($result_data_dokter[1], 0, $count_dokter), true);
		$arr_dokter = $data_parse_dokter['results'];

		$result_rp = $this->m_lap_jum_tindakan_all_therapis->report_tindakan_therapis_search($tgl_awal, $periode, $trawat_tglapp_start, $trawat_tglapp_end, $report_rp, $report_group1, $report_group2, $report_groupby, $start, $end, $cabang);
		
		$result_data_rp = explode(",",$result_rp,2);
		$count_rp = strlen($result_data_rp[1]) - 1;
		$data_parse_rp = json_decode("{".substr($result_data_rp[1], 0, $count_rp), true);
		$arr_rp = $data_parse_rp['results'];

		$result_cust_dokter = $this->m_lap_jum_tindakan_all_therapis->report_tindakan_search_cust_dokter($tgl_awal,$periode, $trawat_tglapp_start, $trawat_tglapp_end);
		if(!empty($result_cust_dokter))
			$arr_cust_dokter = $result_cust_dokter;

		if(!empty($arr_dokter)){
			for($i=0;$i<count($arr_dokter);$i++) {
				$t_ref = 'tjtt_ref'.$i;
				$arr_data[$i]['tknm_karyawan_id'] = $arr_dokter[$i]['dokter_id'];
				$arr_data[$i]['tknm_karyawan_nama'] = $arr_dokter[$i]['karyawan_nama'];
				$arr_data[$i]['tknm_kontribusi_rp'] = $arr_rp[count($arr_rp)-1][$t_ref];
				$t_total_kontribusi += $arr_rp[count($arr_rp)-1][$t_ref];

				if(!empty($arr_cust_dokter[$arr_dokter[$i]['dokter_id']])){
					$arr_data[$i]['tknm_total_cust'] = $arr_cust_dokter[$arr_dokter[$i]['dokter_id']]['jml_cust'];
					$arr_data[$i]['tknm_spending_avg'] = $arr_data[$i]['tknm_kontribusi_rp'] / $arr_cust_dokter[$arr_dokter[$i]['dokter_id']]['jml_cust'];
				}
			}

			for($i=0;$i<count($arr_dokter);$i++) {
				if($t_total_kontribusi == 0)
					$t_total_kontribusi = 1;

				$arr_data[$i]['tknm_kontribusi_persen'] = round((($arr_data[$i]['tknm_kontribusi_rp'] / $t_total_kontribusi) * 100), 2);
			}
		}
		
		$sql = "delete from temp_kontribusi_non_medis where tknm_periode_awal = '".substr($tgl_awal, 0, 7)."' and tknm_periode_akhir = '".substr($tgl_akhir, 0, 7)."'";
		$del_temp = $this->db->query($sql);

		foreach($arr_data as $idx => $row){
			$row['tknm_periode_awal'] = $tgl_awal;
			$row['tknm_periode_akhir'] = $tgl_akhir;
			$row['tknm_date_update'] = date('Y-m-d H:i:s');
			$this->db->insert('temp_kontribusi_non_medis', $row);
		}

		return 1;
	}

	function welcome_chart_kontribusi_non_medis(){
		$tgl_awal = (isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);
		$tgl_akhir = (isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);
		$t_arr_data = $arr_data = array();
		$t_total_kontribusi = 0;
		
		$sql = "select 
					tknm_karyawan_id, tknm_karyawan_nama, sum(tknm_kontribusi_rp) as tknm_kontribusi_rp, sum(tknm_total_cust) as tknm_total_cust
					,(sum(tknm_kontribusi_rp) / sum(tknm_total_cust)) as tknm_spending_avg, tknm_date_update
				from 
					temp_kontribusi_non_medis 
				where 
					tknm_periode_awal >= '".substr($tgl_awal, 0, 7)."' 
					and tknm_periode_akhir <= '".substr($tgl_akhir, 0, 7)."'
					and tknm_periode_awal = tknm_periode_akhir
				group by tknm_karyawan_id
				order by tknm_kontribusi_rp desc";
		$t_arr_data = $this->db->query($sql)->result_array();

		$i = 0;
		foreach($t_arr_data as $value){
			$arr_data[$i] = $value;
			$t_total_kontribusi += $value['tknm_kontribusi_rp'];
			$i++;
		}

		$i = 0;
		foreach($arr_data as $value){
			$arr_data[$i]['tknm_kontribusi_persen'] = round((($value['tknm_kontribusi_rp'] / $t_total_kontribusi) * 100), 2);
			$i++;
		}
		
		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}

	function welcome_chart_kontribusi_produk_update($tgl_awal, $tgl_akhir, $start, $limit){
		set_time_limit(1200);
		$end = $limit;
		
		if(empty($tgl_awal))
			$tgl_awal = (isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);

		if(empty($tgl_akhir))
			$tgl_akhir = (isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);

		if(empty($start) && $start != 0)
			$start = (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);

		if(empty($limit))
			$end = (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);

		$arr_rp = array();
		$arr_dokter = array();
		$arr_cust_dokter = array();
		$arr_data = array();
		$t_total_kontribusi = 0;

		$this->load->model('m_lap_jum_all_produk', '', TRUE);

		$bulan = date('m');
		$cabang = 'default';
		$opsi_periode = $periode = 'tanggal';
		$opsi_perawatan = 'semua';
		$report_rp = 1;
		$report_group2 = $report_group1 = 0;
		$report_groupby = 'Semua';
		$tanpa_perawatan_lain = 'false';
		$tahun = date('Y');
		$group1 = $perawatan = $trawat_dokter = '';
		$tglstart = $trawat_tglapp_start = $tgl_awal;
		$tglend = $trawat_tglapp_end = $tgl_akhir;
		
		$result_dokter = $this->m_lap_jum_all_produk->report_daftar_karyawan($tgl_awal, $periode, $trawat_tglapp_start ,$trawat_tglapp_end ,$trawat_dokter, $report_rp, $report_group1, $report_group2,$report_groupby, $start,$end, $cabang, 0);
		
		$result_data_dokter = explode(",",$result_dokter,2);
		$count_dokter = strlen($result_data_dokter[1]) - 1;
		$data_parse_dokter = json_decode("{".substr($result_data_dokter[1], 0, $count_dokter), true);
		$arr_dokter = $data_parse_dokter['results'];

		$result_rp = $this->m_lap_jum_all_produk->report_tindakan_search($tgl_awal,$periode, $trawat_tglapp_start, $trawat_tglapp_end, $report_rp, $report_group1, $report_group2, $report_groupby, $start, $end, $cabang);
		
		$result_data_rp = explode(",",$result_rp,2);
		$count_rp = strlen($result_data_rp[1]) - 1;
		$data_parse_rp = json_decode("{".substr($result_data_rp[1], 0, $count_rp), true);
		$arr_rp = $data_parse_rp['results'];

		$result_cust_dokter = $this->m_lap_jum_all_produk->report_tindakan_search_cust_dokter($tgl_awal,$periode, $trawat_tglapp_start, $trawat_tglapp_end);
		if(!empty($result_cust_dokter))
			$arr_cust_dokter = $result_cust_dokter;

		if(!empty($arr_dokter)){
			for($i=0;$i<count($arr_dokter);$i++) {
				$t_ref = 'tjjp_ref'.$i;
				$arr_data[$i]['tkp_karyawan_id'] = $arr_dokter[$i]['karyawan_id'];
				$arr_data[$i]['tkp_karyawan_nama'] = $arr_dokter[$i]['karyawan_nama'];
				$arr_data[$i]['tkp_kontribusi_rp'] = $arr_rp[count($arr_rp)-1][$t_ref];
				$t_total_kontribusi += $arr_rp[count($arr_rp)-1][$t_ref];

				if(!empty($arr_cust_dokter[$arr_dokter[$i]['karyawan_id']])){
					$arr_data[$i]['tkp_total_cust'] = $arr_cust_dokter[$arr_dokter[$i]['karyawan_id']]['jml_cust'];
					$arr_data[$i]['tkp_spending_avg'] = $arr_data[$i]['tkp_kontribusi_rp'] / $arr_cust_dokter[$arr_dokter[$i]['karyawan_id']]['jml_cust'];
				}
			}

			for($i=0;$i<count($arr_dokter);$i++) {
				if($t_total_kontribusi == 0)
					$t_total_kontribusi = 1;

				$arr_data[$i]['tkp_kontribusi_persen'] = round((($arr_data[$i]['tkp_kontribusi_rp'] / $t_total_kontribusi) * 100), 2);
			}
		}
		
		$sql = "delete from temp_kontribusi_produk where tkp_periode_awal = '".substr($tgl_awal, 0, 7)."' and tkp_periode_akhir = '".substr($tgl_akhir, 0, 7)."'";
		$del_temp = $this->db->query($sql);

		foreach($arr_data as $idx => $row){
			$row['tkp_periode_awal'] = $tgl_awal;
			$row['tkp_periode_akhir'] = $tgl_akhir;
			$row['tkp_date_update'] = date('Y-m-d H:i:s');
			$this->db->insert('temp_kontribusi_produk', $row);
		}

		return 1;
	}

	function welcome_chart_kontribusi_produk(){
		$tgl_awal = (isset($_POST['tgl_awal']) ? @$_POST['tgl_awal'] : @$_GET['tgl_awal']);
		$tgl_akhir = (isset($_POST['tgl_akhir']) ? @$_POST['tgl_akhir'] : @$_GET['tgl_akhir']);
		$t_arr_data = $arr_data = array();
		$t_total_kontribusi = 0;
		
		$sql = "select 
					tkp_karyawan_id, tkp_karyawan_nama, sum(tkp_kontribusi_rp) as tkp_kontribusi_rp, sum(tkp_total_cust) as tkp_total_cust
					,(sum(tkp_kontribusi_rp) / sum(tkp_total_cust)) as tkp_spending_avg, tkp_date_update
				from 
					temp_kontribusi_produk 
				where 
					tkp_periode_awal >= '".substr($tgl_awal, 0, 7)."' 
					and tkp_periode_akhir <= '".substr($tgl_akhir, 0, 7)."'
					and tkp_periode_awal = tkp_periode_akhir
				group by tkp_karyawan_id
				order by tkp_kontribusi_rp desc";
		$t_arr_data = $this->db->query($sql)->result_array();

		$i = 0;
		foreach($t_arr_data as $value){
			$arr_data[$i] = $value;
			$t_total_kontribusi += $value['tkp_kontribusi_rp'];
			$i++;
		}

		$i = 0;
		foreach($arr_data as $value){
			$arr_data[$i]['tkp_kontribusi_persen'] = round((($value['tkp_kontribusi_rp'] / $t_total_kontribusi) * 100), 2);
			$i++;
		}
		
		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}

	function welcome_chart_top_spender_netsales_update($tgl_awal, $tgl_akhir, $start, $limit){
		set_time_limit(1200);
		$end = $limit;
		
		if(empty($tgl_awal))
			$tgl_awal = (isset($_POST['trawat_tglstart']) ? @$_POST['trawat_tglstart'] : @$_GET['trawat_tglstart']);

		if(empty($tgl_akhir))
			$tgl_akhir = (isset($_POST['trawat_tglend']) ? @$_POST['trawat_tglend'] : @$_GET['trawat_tglend']);

		if(empty($start) && $start != 0)
			$start = (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);

		if(empty($limit))
			$end = (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);

		$arr_data = array();

		$this->load->model('m_report_top_spender_netsales', '', TRUE);

		$umur_end = $umur_start = $trawat_id = $top_jenis = $top_jumlah = '';
		$dashboard = true;
		$trawat_tglstart = $tgl_awal;
		$trawat_tglend = $tgl_akhir;
		
		$result_data = $this->m_report_top_spender_netsales->top_spender_netsales_search($trawat_tglstart, $trawat_tglend, $top_jenis, $top_jumlah, $umur_start, $umur_end, $dashboard);
		
		$result_data_data = explode(",",$result_data,2);
		$count_data = strlen($result_data_data[1]) - 1;
		$data_parse_data = json_decode("{".substr($result_data_data[1], 0, $count_data), true);
		$arr_data = $data_parse_data['results'];

		$sql = "delete from temp_top_spender_netsales where ttsn_periode_awal = '".substr($tgl_awal, 0, 7)."' and ttsn_periode_akhir = '".substr($tgl_akhir, 0, 7)."'";
		$del_temp = $this->db->query($sql);

		if(!empty($arr_data)){
			foreach($arr_data as $idx => $row){
				$t_data = array();
				$t_data['ttsn_cust_no'] = $row['cust_no'];
				$t_data['ttsn_cust_nama'] = $row['cust_nama'];
				$t_data['ttsn_total'] = $row['total'];
				$t_data['ttsn_periode_awal'] = $tgl_awal;
				$t_data['ttsn_periode_akhir'] = $tgl_akhir;
				$t_data['ttsn_date_update'] = date('Y-m-d H:i:s');
				$this->db->insert('temp_top_spender_netsales', $t_data);
			}	
		}

		return 1;
	}

	function welcome_chart_top_spender_netsales(){
		$tgl_awal = (isset($_POST['trawat_tglstart']) ? @$_POST['trawat_tglstart'] : @$_GET['trawat_tglstart']);
		$tgl_akhir = (isset($_POST['trawat_tglend']) ? @$_POST['trawat_tglend'] : @$_GET['trawat_tglend']);
		$arr_data = array();
		
		$sql = "select 
					ttsn_cust_no, ttsn_cust_nama, sum(ttsn_total) as ttsn_total, ttsn_date_update
				from 
					temp_top_spender_netsales 
				where 
					ttsn_periode_awal >= '".substr($tgl_awal, 0, 7)."' 
					and ttsn_periode_akhir <= '".substr($tgl_akhir, 0, 7)."'
					and ttsn_periode_awal = ttsn_periode_akhir
				group by ttsn_cust_no
				order by ttsn_total desc
				limit 10";
		$arr_data = $this->db->query($sql)->result_array();
		
		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}

	function welcome_chart_top_spender_cashin_update($tgl_awal, $tgl_akhir, $start, $limit){
		set_time_limit(1200);
		$end = $limit;
		
		if(empty($tgl_awal))
			$tgl_awal = (isset($_POST['trawat_tglapp_start']) ? @$_POST['trawat_tglapp_start'] : @$_GET['trawat_tglapp_start']);

		if(empty($tgl_akhir))
			$tgl_akhir = (isset($_POST['trawat_tglapp_end']) ? @$_POST['trawat_tglapp_end'] : @$_GET['trawat_tglapp_end']);

		if(empty($start) && $start != 0)
			$start = (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);

		if(empty($limit))
			$end = (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);

		$arr_data = array();

		$this->load->model('m_report_top_spender', '', TRUE);

		$trawat_id = $top_jenis = $top_jumlah = '';
		$dashboard = true;
		$trawat_tglapp_start = $tgl_awal;
		$trawat_tglapp_end = $tgl_akhir;
		
		$result_data = $this->m_report_top_spender->top_spender_search($trawat_id, $trawat_tglapp_start, $trawat_tglapp_end, $top_jenis, $top_jumlah, $start, $end, $dashboard);
		
		$result_data_data = explode(",",$result_data,2);
		$count_data = strlen($result_data_data[1]) - 1;
		$data_parse_data = json_decode("{".substr($result_data_data[1], 0, $count_data), true);
		$arr_data = $data_parse_data['results'];

		$sql = "delete from temp_top_spender_cashin where ttsc_periode_awal = '".substr($tgl_awal, 0, 7)."' and ttsc_periode_akhir = '".substr($tgl_akhir, 0, 7)."'";
		$del_temp = $this->db->query($sql);

		if(!empty($arr_data)){
			foreach($arr_data as $idx => $row){
				$t_data = array();
				$t_data['ttsc_cust_no'] = $row['cust_no'];
				$t_data['ttsc_cust_nama'] = $row['cust_nama'];
				$t_data['ttsc_total'] = $row['total'];
				$t_data['ttsc_periode_awal'] = $tgl_awal;
				$t_data['ttsc_periode_akhir'] = $tgl_akhir;
				$t_data['ttsc_date_update'] = date('Y-m-d H:i:s');
				$this->db->insert('temp_top_spender_cashin', $t_data);
			}	
		}

		return 1;
	}

	function welcome_chart_top_spender_cashin(){
		$tgl_awal = (isset($_POST['trawat_tglapp_start']) ? @$_POST['trawat_tglapp_start'] : @$_GET['trawat_tglapp_start']);
		$tgl_akhir = (isset($_POST['trawat_tglapp_end']) ? @$_POST['trawat_tglapp_end'] : @$_GET['trawat_tglapp_end']);
		$arr_data = array();
		
		$sql = "select 
					ttsc_cust_no, ttsc_cust_nama, sum(ttsc_total) as ttsc_total, ttsc_date_update
				from 
					temp_top_spender_cashin
				where 
					ttsc_periode_awal >= '".substr($tgl_awal, 0, 7)."' 
					and ttsc_periode_akhir <= '".substr($tgl_akhir, 0, 7)."'
					and ttsc_periode_awal = ttsc_periode_akhir
				group by ttsc_cust_no
				order by ttsc_total desc
				limit 10";
		$arr_data = $this->db->query($sql)->result_array();
		
		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}

	function welcome_chart_stok_minimum_update($tgl_awal, $tgl_akhir, $start, $limit){
		set_time_limit(1200);
		$end = $limit;
		
		if(empty($tgl_awal))
			$tgl_awal = (isset($_POST['tanggal_start']) ? @$_POST['tanggal_start'] : @$_GET['tanggal_start']);

		if(empty($tgl_akhir))
			$tgl_akhir = (isset($_POST['tanggal_end']) ? @$_POST['tanggal_end'] : @$_GET['tanggal_end']);

		if(empty($start) && $start != 0)
			$start = (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);

		if(empty($limit))
			$end = (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);

		$arr_data = array();

		$this->load->model('m_stok_mutasi_new', '', TRUE);

		$group1_id = $group2_id = $produk_id = $query = '';
		$tahun = '<?php echo date("Y") ?>';
		$bulan = '<?php echo date("m") ?>';
		$gudang = 2; 
		$jenis = 1; 
		$mutasi_jumlah = 'Semua'; 
		$opsi_produk = 'all'; 
		$opsi_satuan = 'default'; 
		$periode = 'tanggal'; 
		$stok_akhir = $stok_awal = 'Semua'; 
		$stok_keluar = $stok_masuk = 'Semua';
		$dashboard = true;
		$tanggal_start = $tgl_awal;
		$tanggal_end = $tgl_akhir;
		$orderby = 'stok';
		
		$t_tgl_awal = $tahun."-".$bulan;

		$result_data = $this->m_stok_mutasi_new->stok_mutasi_list($bulan, $tahun, $t_tgl_awal, $periode, $gudang, $jenis, $produk_id, $group1_id, $group2_id, $opsi_produk, $opsi_satuan, $tanggal_start, $tanggal_end, $query, $start, $end, $mutasi_jumlah, $stok_akhir, $stok_awal, $stok_masuk, $stok_keluar, $orderby);
		
		$result_data_data = explode(",",$result_data,2);
		$count_data = strlen($result_data_data[1]) - 1;
		$data_parse_data = json_decode("{".substr($result_data_data[1], 0, $count_data), true);
		$arr_data = $data_parse_data['results'];

		$sql = "delete from temp_stok_minimum where tsm_periode_awal = '".substr($tgl_awal, 0, 7)."' and tsm_periode_akhir = '".substr($tgl_akhir, 0, 7)."'";
		$del_temp = $this->db->query($sql);

		if(!empty($arr_data)){
			foreach($arr_data as $idx => $row){
				$t_data = array();
				$t_data['tsm_produk_id'] = $row['produk_id'];
				$t_data['tsm_produk_kode'] = $row['produk_kode'];
				$t_data['tsm_produk_nama'] = $row['produk_nama'];
				$t_data['tsm_satuan_nama'] = $row['satuan_nama'];
				$t_data['tsm_stok_akhir'] = $row['stok_akhir'];
				$t_data['tsm_stok_min'] = $row['stok_min'];
				$t_data['tsm_stok_kekurangan'] = $row['stok_kekurangan'];
				$t_data['tsm_periode_awal'] = $tgl_awal;
				$t_data['tsm_periode_akhir'] = $tgl_akhir;
				$t_data['tsm_date_update'] = date('Y-m-d H:i:s');
				$this->db->insert('temp_stok_minimum', $t_data);
			}	
		}

		return 1;
	}

	function welcome_chart_stok_minimum(){
		$tgl_awal = (isset($_POST['tanggal_start']) ? @$_POST['tanggal_start'] : @$_GET['tanggal_start']);
		$tgl_akhir = (isset($_POST['tanggal_end']) ? @$_POST['tanggal_end'] : @$_GET['tanggal_end']);
		$arr_data = array();
		
		$sql = "select * from temp_stok_minimum where tsm_periode_awal = '".substr($tgl_awal, 0, 7)."' and tsm_periode_akhir = '".substr($tgl_akhir, 0, 7)."' order by tsm_stok_kekurangan asc";
		$arr_data = $this->db->query($sql)->result_array();
		
		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}
	
	function welcome_chart_jatuh_tempo(){
		$sql = " SELECT
						m.invoice_id, m.invoice_no, m.invoice_no_auto, s.supplier_id, s.supplier_nama, '0' as terima_no, m.invoice_terbayar,
						m.invoice_tanggal, m.invoice_diskon, m.invoice_cashback, m.invoice_biaya, m.invoice_uangmuka, m.invoice_total_barang,
						m.invoice_total_tagihan, m.invoice_jatuhtempo, m.invoice_jatuh_tempo_hari, m.invoice_penagih, m.invoice_keterangan, m.invoice_bank, m.invoice_bank, m.invoice_status, m.invoice_post,
						m.invoice_creator, m.invoice_date_create, m.invoice_update, m.invoice_date_update, m.invoice_revised
					FROM master_invoice m
					LEFT JOIN detail_invoice_tbeli dt ON m.invoice_id=dt.dtbeli_master
					LEFT JOIN master_terima_beli tb on tb.terima_id=dt.dtbeli_terima_id
					LEFT JOIN supplier s ON s.supplier_id = m.invoice_supplier
					WHERE curdate() between DATE_SUB(date_format(m.invoice_jatuhtempo,'%Y-%m-%d'), INTERVAL 3 DAY) and date_format(m.invoice_jatuhtempo,'%Y-%m-%d') AND m.invoice_status = 'Tertutup'
					GROUP BY m.invoice_id ORDER BY m.invoice_no_auto DESC";
		$arr_data = $this->db->query($sql)->result_array();
		
		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}
	
	function welcome_chart_ultah(){
		$tanggal = (isset($_POST['tanggal']) ? @$_POST['tanggal'] : @$_GET['tanggal']);
		$bulan = (isset($_POST['bulan']) ? @$_POST['bulan'] : @$_GET['bulan']);
		
		$sql = "select c.cust_nama, date_format(c.cust_tgllahir,'%Y-%m-%d') as cust_tgllahir
					from customer c
					where (day(c.cust_tgllahir-3)='".$tanggal."' OR day(c.cust_tgllahir-2)='".$tanggal."' OR day(c.cust_tgllahir-1)='".$tanggal."' OR day(c.cust_tgllahir)='".$tanggal."') AND month(c.cust_tgllahir)='".$bulan."'
					ORDER by day(c.cust_tgllahir)";
		$arr_data = $this->db->query($sql)->result_array();
		
		$nbrows = count($arr_data);
		if($nbrows > 0){
			$jsonresult = json_encode($arr_data);
			echo '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			echo '({"total":"0", "results":""})';
		}
	}

	function welcome_chart_rekap_jual_update($tgl_awal, $tgl_akhir, $start, $limit, $rekap_penjualan_jenis, $orderby){
		set_time_limit(1200);
		$end = $limit;
		
		if(empty($tgl_awal))
			$tgl_awal = (isset($_POST['rekap_penjualan_tglapp_start']) ? @$_POST['rekap_penjualan_tglapp_start'] : @$_GET['rekap_penjualan_tglapp_start']);

		if(empty($tgl_akhir))
			$tgl_akhir = (isset($_POST['rekap_penjualan_tglapp_end']) ? @$_POST['rekap_penjualan_tglapp_end'] : @$_GET['rekap_penjualan_tglapp_end']);

		if(empty($start) && $start != 0)
			$start = (isset($_POST['start']) ? @$_POST['start'] : @$_GET['start']);

		if(empty($limit))
			$end = (isset($_POST['limit']) ? @$_POST['limit'] : @$_GET['limit']);

		if(empty($rekap_penjualan_jenis))
			$rekap_penjualan_jenis = (isset($_POST['rekap_penjualan_jenis']) ? @$_POST['rekap_penjualan_jenis'] : @$_GET['rekap_penjualan_jenis']);

		if(empty($orderby))
			$orderby = (isset($_POST['orderby']) ? @$_POST['orderby'] : @$_GET['orderby']);
		if(empty($rekap_penjualan_group))
			$rekap_penjualan_group = (isset($_POST['rekap_penjualan_group']) ? @$_POST['rekap_penjualan_group'] : @$_GET['rekap_penjualan_group']);
		$arr_data = array();

		$this->load->model('m_report_rekap_penjualan', '', TRUE);

		$rekap_penjualan_status_pajak = $rekap_penjualan_group_1 /*= $rekap_penjualan_group*/ = '';
		$dashboard = true;
		$rekap_penjualan_periode = 'tanggal';
		$rekap_penjualan_tglapp_start = $tgl_awal;
		$rekap_penjualan_tglapp_end = $tgl_akhir;

		/*if($rekap_penjualan_jenis == 'Perawatan_dan_Paket')
			$t_jenis = 'perawatan';
		else*/
			$t_jenis = strtolower($rekap_penjualan_jenis);

		if($rekap_penjualan_jenis == 'Produk'){
			$label = 'trjp';



		} else if($rekap_penjualan_jenis == 'Perawatan_dan_Paket'){
			$label = 'trjr';
		} else {
			$label = 'trjk';
		}
		
		$result_data = $this->m_report_rekap_penjualan->rekap_penjualan_search($rekap_penjualan_periode, $rekap_penjualan_tglapp_start, $rekap_penjualan_tglapp_end, $rekap_penjualan_jenis, $rekap_penjualan_group, $rekap_penjualan_group_1, $rekap_penjualan_status_pajak, $start, $end, $dashboard, $orderby);
		
		$result_data_data = explode(",",$result_data,2);
		$count_data = strlen($result_data_data[1]) - 1;
		$data_parse_data = json_decode("{".substr($result_data_data[1], 0, $count_data), true);
		$arr_data = $data_parse_data['results'];

		$sql = "delete from temp_rekap_jual_".$t_jenis." where ".$label."_periode_awal = '".substr($tgl_awal, 0, 7)."' and ".$label."_periode_akhir = '".substr($tgl_akhir, 0, 7)."'";
		$del_temp = $this->db->query($sql);

		if(!empty($arr_data)){
			foreach($arr_data as $idx => $row){
				$t_data = array();
				$t_data[$label.'_kode'] = $row['kode'];
				$t_data[$label.'_nama'] = $row['nama'];
				$t_data[$label.'_tot_jum_item'] = $row['tot_jum_item'];
				$t_data[$label.'_tot_net'] = $row['tot_net'];
				$t_data[$label.'_periode_awal'] = $tgl_awal;
				$t_data[$label.'_periode_akhir'] = $tgl_akhir;
				$t_data[$label.'_date_update'] = date('Y-m-d H:i:s');
				$this->db->insert('temp_rekap_jual_'.$t_jenis, $t_data);
			}
		}

		return 1;
	}

	function prepare_chart_kunjungan($type)
	{
		$bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		$tgl_awal	= $tahun_awal."-".$bulan_awal;
		$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
		
		$result		= $this->m_public_function->get_laporan_kunjungan_dashboard($tgl_awal, $tgl_akhir, $type);
		$result_target	= $this->m_public_function->get_kunjungan_target_from_sr($tgl_awal, $tgl_akhir, $type);
		
		if ($result_target==1)
			echo 1;
		else {
		
			$result_data_target	= explode(",",$result_target,2);
			$count_target 		= strlen($result_data_target[1]) - 1;
			$data_parse_target 	= json_decode("{".substr($result_data_target[1],0,$count_target),true);
			
			if(empty($data_parse_target['results'][0]['total_target']))
				$data_parse_target['results'][0]['total_target'] = 1;

			echo $result / $data_parse_target['results'][0]['total_target'];
		}
	}

	function prepare_chart_spending_avg()
	{
		$bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		$tgl_awal	= $tahun_awal."-".$bulan_awal;
		$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
		
		$result			= $this->m_public_function->get_laporan_spending_avg_dashboard($tgl_awal, $tgl_akhir);
		$result_target	= $this->m_public_function->get_spending_avg_target_from_sr($tgl_awal, $tgl_akhir);
		
		if ($result_target==1)
			echo 1;
		else {
		
			$result_data_target	= explode(",",$result_target,2);
			$count_target 		= strlen($result_data_target[1]) - 1;
			$data_parse_target 	= json_decode("{".substr($result_data_target[1],0,$count_target),true);
			
			if(empty($data_parse_target['results'][0]['total_target']))
				$data_parse_target['results'][0]['total_target'] = 1;

			echo $result / $data_parse_target['results'][0]['total_target'];
		}
	}	
	
	function prepare_chart_netsales()
	{
		$bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		$tgl_awal	= $tahun_awal."-".$bulan_awal;
		$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
		
		$result			= $this->m_public_function->get_laporan_netsales_dashboard($tgl_awal, $tgl_akhir);
		$result_target	= $this->m_public_function->get_netsales_target_from_sr($tgl_awal, $tgl_akhir);
		
		if ($result_target==1)
			echo 1;
		else {
		
			$result_data_target	= explode(",",$result_target,2);
			$count_target 		= strlen($result_data_target[1]) - 1;
			$data_parse_target 	= json_decode("{".substr($result_data_target[1],0,$count_target),true);
			
			if(empty($data_parse_target['results'][0]['total_target']))
				$data_parse_target['results'][0]['total_target'] = 1;

			echo $result / $data_parse_target['results'][0]['total_target'];
		}
	}	
	
	function prepare_chart_netsales_medis()
	{
		$bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		$tgl_awal	= $tahun_awal."-".$bulan_awal;
		$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
		
		$result			= $this->m_public_function->get_laporan_netsales_medis_dashboard($tgl_awal, $tgl_akhir);
		$result_target	= $this->m_public_function->get_laporan_target_perawatan_medis_dashboard($tgl_awal, $tgl_akhir);				
		
		if ($result_target==1)
			echo 1;
		else {
		
			$result_data_target	= explode(",",$result_target,2);
			$count_target 		= strlen($result_data_target[1]) - 1;
			$data_parse_target 	= json_decode("{".substr($result_data_target[1],0,$count_target),true);			
			
			if(empty($data_parse_target['results'][0]['total_target']))
				$data_parse_target['results'][0]['total_target'] = 1;

			echo $result / $data_parse_target['results'][0]['total_target'];
		}
	}	
	
	function prepare_chart_netsales_estetik()
	{
		$bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		$tgl_awal	= $tahun_awal."-".$bulan_awal;
		$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
		
		$result			= $this->m_public_function->get_laporan_netsales_estetik_dashboard($tgl_awal, $tgl_akhir);
		$result_target	= $this->m_public_function->get_laporan_target_perawatan_nonmedis_dashboard($tgl_awal, $tgl_akhir);				
			
		if ($result_target==1)
			echo 1;
		else {
		
			$result_data_target	= explode(",",$result_target,2);
			$count_target 		= strlen($result_data_target[1]) - 1;
			$data_parse_target 	= json_decode("{".substr($result_data_target[1],0,$count_target),true);			
			
			if(empty($data_parse_target['results'][0]['total_target']))
				$data_parse_target['results'][0]['total_target'] = 1;

			echo $result / $data_parse_target['results'][0]['total_target'];
		}
	}
	
	function prepare_chart_netsales_produk()
	{
		$bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
		$tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
		$bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
		$tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);
		
		$tgl_awal	= $tahun_awal."-".$bulan_awal;
		$tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
		
		$result			= $this->m_public_function->get_laporan_netsales_produk_dashboard($tgl_awal, $tgl_akhir);
		$result_target	= $this->m_public_function->get_laporan_target_produk_dashboard($tgl_awal, $tgl_akhir);						
			
		if ($result_target==1)
			echo 1;
		else {
		
			$result_data_target	= explode(",",$result_target,2);
			$count_target 		= strlen($result_data_target[1]) - 1;
			$data_parse_target 	= json_decode("{".substr($result_data_target[1],0,$count_target),true);			
			
			if(empty($data_parse_target['results'][0]['total_target']))
				$data_parse_target['results'][0]['total_target'] = 1;

			echo $result / $data_parse_target['results'][0]['total_target'];
		}
	}
	
	function prepare_chart_aesthetic_terapis_netsales(){
	    $bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
	    $tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
	    $bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
	    $tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);

	    $tgl_awal	= $tahun_awal."-".$bulan_awal;
	    $tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
	    
	    $result = $this->m_public_function->get_laporan_aesthetic_netsales_dashboard($tgl_awal, $tgl_akhir);
	    $terapis = $this->m_public_function->get_laporan_jumlah_terapis_dashboard($tgl_awal, $tgl_akhir);	    
	    $target = $this->m_public_function->get_laporan_target_perawatan_nonmedis_dashboard($tgl_awal, $tgl_akhir);
	    
	    if ($target==1){
		echo 1;
	    }
	    else {
		$result_data = explode(",",$result,2);
		$count = strlen($result_data[1]) - 1;
		
		$result_terapis = explode(",",$terapis,2);
		$count_terapis = strlen($result_terapis[1]) - 1;
	    
		$result_target	= explode(",",$target,2);
		$count_target 	= strlen($result_target[1]) - 1;

		$data_parse 		= json_decode("{".substr($result_data[1],0,$count),true);
		$data_parse_terapis	= json_decode("{".substr($result_terapis[1],0,$count_terapis),true);
		$data_parse_target 	= json_decode("{".substr($result_target[1],0,$count_target),true);
		
		//Jumlah net sales non medis di bandingkan dengan jumlah terapis
		$nilai_total_nonmedis = $data_parse['results'][0]['nilai_total_nonmedis'];		
		$average =  $nilai_total_nonmedis / $data_parse_terapis['results'][0]['jumlah_terapis'];
		$total_target = $data_parse_target['results'][0]['total_target'];
		
		//Save to database, to show in grid
		$sql_del = "DELETE FROM temp_dashboard WHERE td_periode_awal = '$tgl_awal' AND td_periode_akhir = '$tgl_akhir' AND td_kategori = 'Aesthetic Net Sales - Terapis'";
		$this->db->query($sql_del);

		$sql_ins = "INSERT temp_dashboard(td_kategori, td_target, td_pencapaian, td_periode_awal, td_periode_akhir)
			    VALUES ('Aesthetic Net Sales - Terapis', '$total_target', '$average', '$tgl_awal', '$tgl_akhir')";

		$this->db->query($sql_ins);

		//Jumlah rata-rata dibandingkan dengan target
		echo  $average / $total_target;
	    }
	}
	
	function prepare_chart_aesthetic_bed_netsales(){
	    $bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
	    $tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
	    $bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
	    $tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);

	    $tgl_awal	= $tahun_awal."-".$bulan_awal;
	    $tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
	    
	    $result = $this->m_public_function->get_laporan_aesthetic_netsales_dashboard($tgl_awal, $tgl_akhir);
	    $bed = $this->m_public_function->get_laporan_jumlah_bed_aesthetic_dashboard($tgl_awal, $tgl_akhir);
	    $target = $this->m_public_function->get_laporan_target_perawatan_nonmedis_dashboard($tgl_awal, $tgl_akhir);

	    if ($target==1){
		echo 1;
	    }
	    else {
		$result_data = explode(",",$result,2);
		$count = strlen($result_data[1]) - 1;
		
		$result_bed = explode(",",$bed,2);
		$count_bed = strlen($result_bed[1]) - 1;
	    
		$result_target	= explode(",",$target,2);
		$count_target 	= strlen($result_target[1]) - 1;

		$data_parse = json_decode("{".substr($result_data[1],0,$count),true);
		$data_parse_bed	= json_decode("{".substr($result_bed[1],0,$count_bed),true);
		$data_parse_target = json_decode("{".substr($result_target[1],0,$count_target),true);
		
		//Jumlah net sales non medis di bandingkan dengan jumlah bed aesthetic
		$nilai_total_nonmedis = $data_parse['results'][0]['nilai_total_nonmedis'];		
		$average =  $nilai_total_nonmedis / $data_parse_bed['results'][0]['jumlah_bed_aesthetic'];
		$total_target = $data_parse_target['results'][0]['total_target'];
		
		//Save to database, to show in grid
		$sql_del = "DELETE FROM temp_dashboard WHERE td_periode_awal = '$tgl_awal' AND td_periode_akhir = '$tgl_akhir' AND td_kategori = 'Aesthetic Net Sales - Bed'";
		$this->db->query($sql_del);

		$sql_ins = "INSERT temp_dashboard(td_kategori, td_target, td_pencapaian, td_periode_awal, td_periode_akhir)
			    VALUES ('Aesthetic Net Sales - Bed', '$total_target', '$average', '$tgl_awal', '$tgl_akhir')";

		$this->db->query($sql_ins);
		
		//Jumlah rata-rata dibandingkan dengan target
		echo  $average / $total_target;
	    }
	}
	
	function prepare_chart_medis_dokter_netsales(){
	    $bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
	    $tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
	    $bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
	    $tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);

	    $tgl_awal	= $tahun_awal."-".$bulan_awal;
	    $tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
	    
	    $result = $this->m_public_function->get_laporan_medis_netsales_dashboard($tgl_awal, $tgl_akhir);
	    $dokter = $this->m_public_function->get_laporan_jumlah_dokter_dashboard($tgl_awal, $tgl_akhir);
	    $target = $this->m_public_function->get_laporan_target_perawatan_medis_dashboard($tgl_awal, $tgl_akhir);

	    if ($target==1){
		echo 1;
	    }
	    else {
		$result_data = explode(",",$result,2);
		$count = strlen($result_data[1]) - 1;
	    
		$result_dokter = explode(",",$dokter,2);
		$count_dokter = strlen($result_dokter[1]) - 1;
	    
		$result_target	= explode(",",$target,2);
		$count_target 	= strlen($result_target[1]) - 1;

		$data_parse = json_decode("{".substr($result_data[1],0,$count),true);
		$data_parse_dokter = json_decode("{".substr($result_dokter[1],0,$count_dokter),true);
		$data_parse_target = json_decode("{".substr($result_target[1],0,$count_target),true);
		
		//Jumlah net sales medis di bandingkan dengan jumlah dokter
		$nilai_total_medis = $data_parse['results'][0]['nilai_total_medis'];		
		$average =  $nilai_total_medis / $data_parse_dokter['results'][0]['jumlah_dokter'];
		$total_target = $data_parse_target['results'][0]['total_target'];

		//Save to database, to show in grid
		$sql_del = "DELETE FROM temp_dashboard WHERE td_periode_awal = '$tgl_awal' AND td_periode_akhir = '$tgl_akhir' AND td_kategori = 'Medis Net Sales - Dokter'";
		$this->db->query($sql_del);

		$sql_ins = "INSERT temp_dashboard(td_kategori, td_target, td_pencapaian, td_periode_awal, td_periode_akhir)
			    VALUES ('Medis Net Sales - Dokter', '$total_target', '$average', '$tgl_awal', '$tgl_akhir')";

		$this->db->query($sql_ins);
		
		//Jumlah rata-rata dibandingkan dengan target
		echo  $average / $total_target;
	    }
	}
	
	function prepare_chart_medis_bed_netsales(){
	    $bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
	    $tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
	    $bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
	    $tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);

	    $tgl_awal	= $tahun_awal."-".$bulan_awal;
	    $tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
	    
	    $result = $this->m_public_function->get_laporan_medis_netsales_dashboard($tgl_awal, $tgl_akhir);
	    $bed = $this->m_public_function->get_laporan_jumlah_bed_medis_dashboard($tgl_awal, $tgl_akhir);
	    $target = $this->m_public_function->get_laporan_target_perawatan_medis_dashboard($tgl_awal, $tgl_akhir);	    

	    if ($target==1){
		echo 1;
	    }
	    else {		
		$result_data = explode(",",$result,2);
		$count = strlen($result_data[1]) - 1;
	    
		$result_bed = explode(",",$bed,2);
		$count_bed = strlen($result_bed[1]) - 1;
	    
		$result_target	= explode(",",$target,2);
		$count_target 	= strlen($result_target[1]) - 1;

		$data_parse = json_decode("{".substr($result_data[1],0,$count),true);
		$data_parse_bed = json_decode("{".substr($result_bed[1],0,$count_bed),true);
		$data_parse_target = json_decode("{".substr($result_target[1],0,$count_target),true);

		//Jumlah net sales medis di bandingkan dengan jumlah bed medis
		$nilai_total_medis = $data_parse['results'][0]['nilai_total_medis'];		
		$average =  $nilai_total_medis / $data_parse_bed['results'][0]['jumlah_bed_medis'];
		$total_target = $data_parse_target['results'][0]['total_target'];

		//Save to database, to show in grid
		$sql_del = "DELETE FROM temp_dashboard WHERE td_periode_awal = '$tgl_awal' AND td_periode_akhir = '$tgl_akhir' AND td_kategori = 'Medis Net Sales - Bed'";
		$this->db->query($sql_del);

		$sql_ins = "INSERT temp_dashboard(td_kategori, td_target, td_pencapaian, td_periode_awal, td_periode_akhir)
			    VALUES ('Medis Net Sales - Bed', '$total_target', '$average', '$tgl_awal', '$tgl_akhir')";

		$this->db->query($sql_ins);
		
		//Jumlah rata-rata dibandingkan dengan target
		echo  $average / $total_target;
	    }
	}
	
	function prepare_chart_product_dokter_netsales(){
	    $bulan_awal=(isset($_POST['bulan_awal']) ? @$_POST['bulan_awal'] : @$_GET['bulan_awal']);
	    $tahun_awal=(isset($_POST['tahun_awal']) ? @$_POST['tahun_awal'] : @$_GET['tahun_awal']);
	    $bulan_akhir=(isset($_POST['bulan_akhir']) ? @$_POST['bulan_akhir'] : @$_GET['bulan_akhir']);
	    $tahun_akhir=(isset($_POST['tahun_akhir']) ? @$_POST['tahun_akhir'] : @$_GET['tahun_akhir']);

	    $tgl_awal	= $tahun_awal."-".$bulan_awal;
	    $tgl_akhir	= $tahun_akhir."-".$bulan_akhir;
	    
	    $result = $this->m_public_function->get_laporan_produk_netsales_dashboard($tgl_awal, $tgl_akhir);
	    $dokter = $this->m_public_function->get_laporan_jumlah_dokter_dashboard($tgl_awal, $tgl_akhir);
	    $target = $this->m_public_function->get_laporan_target_produk_dashboard($tgl_awal, $tgl_akhir);

	    if ($target==1){
		echo 1;
	    }
	    else {
		$result_data = explode(",",$result,2);
		$count = strlen($result_data[1]) - 1;
	    
		$result_dokter = explode(",",$dokter,2);
		$count_dokter = strlen($result_dokter[1]) - 1;
	    
		$result_target	= explode(",",$target,2);
		$count_target 	= strlen($result_target[1]) - 1;

		$data_parse = json_decode("{".substr($result_data[1],0,$count),true);
		$data_parse_dokter = json_decode("{".substr($result_dokter[1],0,$count_dokter),true);
		$data_parse_target = json_decode("{".substr($result_target[1],0,$count_target),true);
		
		//Jumlah net sales produk di bandingkan dengan jumlah dokter
		$nilai_total_medis = $data_parse['results'][0]['nilai_total_produk'];		
		$average =  $nilai_total_medis / $data_parse_dokter['results'][0]['jumlah_dokter'];
		$total_target = $data_parse_target['results'][0]['total_target'];

		//Save to database, to show in grid
		$sql_del = "DELETE FROM temp_dashboard WHERE td_periode_awal = '$tgl_awal' AND td_periode_akhir = '$tgl_akhir' AND td_kategori = 'Produk Net Sales - Dokter'";
		$this->db->query($sql_del);

		$sql_ins = "INSERT temp_dashboard(td_kategori, td_target, td_pencapaian, td_periode_awal, td_periode_akhir)
			    VALUES ('Produk Net Sales - Dokter', '$total_target', '$average', '$tgl_awal', '$tgl_akhir')";

		$this->db->query($sql_ins);
		
		//Jumlah rata-rata dibandingkan dengan target
		echo  $average / $total_target;
	    }
	}
		
	// Encodes a SQL array into a JSON formated string
	function JEncode($arr){
		if (version_compare(PHP_VERSION,"5.2","<"))
		{    
			require_once("./JSON.php"); //if php<5.2 need JSON class
			$json = new Services_JSON();//instantiate new json object
			$data=$json->encode($arr);  //encode the data in json format
		} else {
			$data = json_encode($arr);  //encode the data in json format
		}
		return $data;
	}
	
	// Encodes a YYYY-MM-DD into a MM-DD-YYYY string
	function codeDate ($date) {
	  $tab = explode ("-", $date);
	  $r = $tab[1]."/".$tab[2]."/".$tab[0];
	  return $r;
	}
	
}
?>