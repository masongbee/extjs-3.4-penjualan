<?
class M_hpp extends Model{
		
	//constructor
	function M_hpp() {
		parent::Model();
	}

	function get_rawat_list($filter, $start, $end){
		
		$sql = "SELECT r.rawat_id, r.rawat_kode, r.rawat_nama FROM perawatan r WHERE rawat_aktif = 'Aktif'";
		
		if($filter<>""){
			$sql .= eregi("WHERE",$sql)?" AND ":" WHERE ";
			$sql .=   "(rawat_kode LIKE '%".addslashes($filter)."%' OR 
						rawat_nama LIKE '%".addslashes($filter)."%')";
		}

		$sql.=" ORDER BY rawat_kode ASC ";

		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;		
		$result = $this->db->query($limit);   


		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}

	}
	
	function get_pracikan_list($filter, $start, $end){
		
		$sql = "SELECT p.produk_id, p.produk_kode, p.produk_nama
				FROM produk p
				WHERE p.produk_racikan =  1 AND p.produk_aktif = 'Aktif'";
		
		if($filter<>""){
			$sql .= eregi("WHERE",$sql)?" AND ":" WHERE ";
			$sql .=   "(produk_kode LIKE '%".addslashes($filter)."%' OR 
						produk_nama LIKE '%".addslashes($filter)."%')";
		}

		$sql.=" ORDER BY produk_kode ASC ";

		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;		
		$result = $this->db->query($limit);   

		if($nbrows>0){
			foreach($result->result() as $row)
				$arr[] = $row;
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} 
		else
			return '({"total":"0", "results":""})';
		
	}
	
	function get_produk_list($filter,$start,$end,$satuan){
		if($satuan=='default')
			$sql="select distinct * from vu_produk_satuan_default WHERE produk_aktif='Aktif'";
		else
			$sql="select distinct * from vu_produk_satuan_terkecil WHERE produk_aktif='Aktif'";
		//echo $sql;

		if($filter<>""){
			$sql.=eregi("WHERE",$sql)?" AND ":" WHERE ";
			$sql.="( produk_kode LIKE '%".addslashes($filter)."%' OR 
					 produk_nama LIKE '%".addslashes($filter)."%' OR 
					 satuan_kode LIKE '%".addslashes($filter)."%' OR 
					 satuan_nama LIKE '%".addslashes($filter)."%')";
		}

		$sql.=" ORDER BY produk_kode ASC ";

		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;		
		$result = $this->db->query($limit);   


		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}

	}
	
	function get_group1($query,$start,$end){
			$sql="SELECT produk_group.* FROM produk_group 
			    LEFT JOIN kategori on group_kelompok=kategori_id
			    WHERE group_aktif='Aktif' and kategori_jenis='produk'";

			if($query<>""){
				$sql .=eregi("WHERE",$sql)? " AND ":" WHERE ";
				$sql .= " (group_nama LIKE '%".addslashes($query)."%' OR group_kode LIKE '%".addslashes($query)."%')";
			}

			$result = $this->db->query($sql);

			$nbrows = $result->num_rows();
			$limit = $sql." LIMIT ".$start.",".$end;			
			$result = $this->db->query($limit); 

			if($nbrows>0){
				foreach($result->result() as $row){
					$arr[] = $row;
				}
				$jsonresult = json_encode($arr);
				return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
			} else {
				return '({"total":"0", "results":""})';
			}
	}

	function get_periode(){
		$sql="SELECT distinct substr(min(tanggal),1,7) as min_date, substr(max(tanggal),1,7) as max_date from vu_hpp_tanggal";
		$query=$this->db->query($sql);
		if($query->num_rows()>0){
			$ds=$query->row();
			$min_date=$ds->min_date;
			$max_date=$ds->max_date;
			$min_year=substr($min_date,0,4);
			$max_year=substr($max_date,0,4);
			$range_year=$max_year-$min_year;

			$min_month=substr($min_date,5,2);
			$max_month=substr($max_date,5,2);

			//echo $min_year." s/d ".$max_year;
			//echo $min_month."-".$max_month;
			//min year
			$j=0;
			for($i=12;$i>=(int)$min_month;$i--){
				$data[$j]["periode_tanggal"]=$min_year."-".(strlen($i)==1?"0".$i:$i);
				$j++;
			}
			//range year
			for($i=(int)$min_year;$i<(int)$max_year;$i++){
				for($k=1;$k<=12;$k++){
					$data[$j]["periode_tanggal"]=$i."-".(strlen($k)==1?"0".$k:$k);
					$j++;
				}
			}

			//max year
			for($i=1;$i<=$max_month;$i++){
				$data[$j]["periode_tanggal"]=$max_year."-".(strlen($i)==1?"0".$i:$i);
				$j++;
			}

			return $data;
		}
		else
			return "";

	}
		
	function get_qty_awal($tanggal_awal, $produk_id, $konversi_nilai){
			
		//Saldo awal master produk (dari semua gudang)
		$sql_saldo_awal=
		   "SELECT
				(produk_saldo_awal + produk_saldo_awal2 + produk_saldo_awal3 + produk_saldo_awal4) /*/ konversi_nilai*/  as jumlah,
				produk_tgl_nilai_saldo_awal
			FROM produk/*, satuan_konversi*/
			WHERE 
				/*konversi_produk = produk_id
				AND	konversi_default = true
				AND*/
				produk_id = '".$produk_id."'
				AND produk_tgl_nilai_saldo_awal <= ".$tanggal_awal;
				
		$rs_saldo_awal	= $this->db->query($sql_saldo_awal) or die("Error - 1.1 : ".$sql_saldo_awal);
		
		//dari hasil query di atas, akan diketahui apakah tanggal_awal >= produk_tgl_nilai_saldo_awal? jika tidak maka return 0
		if($rs_saldo_awal->num_rows()>0){
					
			$row_saldo_awal	= $rs_saldo_awal->row();
		
			$sql_konversi =    "SELECT konversi_nilai
								FROM vu_produk_satuan_default 
								WHERE produk_aktif = 'Aktif' AND produk_id = '$produk_id'";
			$result	= $this->db->query($sql_konversi);
			$row_konversi = $result->row();
			
			
			//Saldo transaksi, dihitung sejak produk_tgl_nilai_saldo_awal di master produk
			
			$sql_kategori	= "SELECT produk_kategori FROM produk WHERE produk_id = $produk_id";
			$rs_kategori	= $this->db->query($sql_kategori);
			$row_kategori	= $rs_kategori->row();
			
			//produk retail
			if ($row_kategori->produk_kategori == 1 || $row_kategori->produk_kategori == 14)
				$sql = 
				   "SELECT
						IFNULL(SUM(stok_masuk - stok_keluar), 0) as stok_saldo
					FROM
					(
						SELECT
							ks.ks_masuk * sk.konversi_nilai / $konversi_nilai as stok_masuk,
							ks.ks_keluar * sk.konversi_nilai / $konversi_nilai as stok_keluar
						FROM kartu_stok_fix ks
						LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
						WHERE 
							ks.ks_produk_id = '".$produk_id."'
							AND ks.ks_tgl_faktur >= '".$row_saldo_awal->produk_tgl_nilai_saldo_awal."' 
							AND ks.ks_tgl_faktur < ".$tanggal_awal."
							AND (ks.ks_gudang_id = 2 OR ks.ks_gudang_id = 1)
							
						UNION ALL
						
						SELECT
							0 as stok_masuk,
							d.cabin_jumlah / $konversi_nilai as stok_keluar /*krn pasti sat. terkecil*/
						FROM detail_pakai_cabin d
						WHERE 
							d.cabin_produk = '".$produk_id."'
							AND date_format(cabin_date_create,'%Y-%m-%d') >= '".$row_saldo_awal->produk_tgl_nilai_saldo_awal."' 
							AND date_format(cabin_date_create,'%Y-%m-%d') <".$tanggal_awal."
							AND (d.cabin_gudang = 2 OR d.cabin_gudang = 1)
							AND date_format(cabin_date_create,'%Y-%m-%d') >= '2016-06-01' #perbaikan sejak 1 juni 2016, supaya tidak mengganggu hpp sebelumnya
					) 
					AS ks_gabungan";
			
			//produk lainnya
			else
				$sql = 
				   "SELECT
						IFNULL(SUM(stok_masuk - stok_keluar), 0) as stok_saldo
					FROM
					(
						SELECT
							ks.ks_masuk * sk.konversi_nilai / $konversi_nilai as stok_masuk,
							ks.ks_keluar * sk.konversi_nilai / $konversi_nilai as stok_keluar
						FROM kartu_stok_fix ks
						LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
						WHERE 
							ks.ks_produk_id = '".$produk_id."'
							AND ks.ks_tgl_faktur >= '".$row_saldo_awal->produk_tgl_nilai_saldo_awal."' 
							AND ks.ks_tgl_faktur < ".$tanggal_awal."
							AND ks.ks_gudang_id <> 99
							#AND ks.ks_gudang_id <> 2
							#2015-10-29: MB dari GR ke Kabin tidak lagi dianggap barang masuk. Yang dianggap barang masuk adalah dari PB
							
						UNION ALL
						
						SELECT
							0 as stok_masuk,
							d.cabin_jumlah * sk.konversi_nilai / $konversi_nilai as stok_keluar /*krn pasti sat. terkecil*/
						FROM 
							detail_pakai_cabin d
							LEFT JOIN satuan_konversi sk ON d.cabin_satuan = sk.konversi_satuan AND d.cabin_produk = sk.konversi_produk
						WHERE 
							d.cabin_produk = '".$produk_id."'
							AND date_format(cabin_date_create,'%Y-%m-%d') >= '".$row_saldo_awal->produk_tgl_nilai_saldo_awal."' 
							AND date_format(cabin_date_create,'%Y-%m-%d') <".$tanggal_awal."
					) 
					AS ks_gabungan";
									
			$query = $this->db->query($sql);
		
			if($query->num_rows()>0){
				$row = $query->row();
				$total = $row_saldo_awal->jumlah + $row->stok_saldo;
				//print_r(' sql: '.$sql.', saldo awal: '.$row_saldo_awal->jumlah.', stok saldo: '.$row->stok_saldo);
				return $total;
			}
			elseif($query->num_rows()==0){
				return $row_saldo_awal->jumlah;
				//print_r($tanggal_awal.$row_saldo_awal->jumlah);
			}
		}
		else{
			return 0;
		}
	}

	function get_nilai_awal($tanggal_awal, $bulan_sebelum, $tahun, $produk_id){
	
		//Saldo awal master produk (dari semua gudang), sementara hanya bisa jika tanggalnya 1
		$sql_saldo_awal=
		   "SELECT
				(produk_nilai_saldo_awal + produk_nilai_saldo_awal2 + produk_nilai_saldo_awal3 + produk_nilai_saldo_awal4) as produk_nilai_saldo_awal,				
				IF(produk_tgl_khusus_nilai_saldo_awal = '0000-00-00', produk_tgl_nilai_saldo_awal, produk_tgl_khusus_nilai_saldo_awal) AS produk_tgl_nilai_saldo_awal
			FROM produk
			WHERE 
				produk_id = '".$produk_id."'
				AND IF(produk_tgl_khusus_nilai_saldo_awal = '0000-00-00', produk_tgl_nilai_saldo_awal, produk_tgl_khusus_nilai_saldo_awal) <= ".$tanggal_awal;
				
		$rs_saldo_awal	= $this->db->query($sql_saldo_awal) or die("Error - 1.1 : ".$sql_saldo_awal);
		
		//dari hasil query di atas, akan diketahui apakah tanggal_awal >= produk_tgl_nilai_saldo_awal? jika tidak maka return 0
		if($rs_saldo_awal->num_rows()>0){
			$row_saldo_awal	= $rs_saldo_awal->row();					
			
			if($row_saldo_awal->produk_tgl_nilai_saldo_awal == str_replace("'", "", $tanggal_awal))				
				return $row_saldo_awal->produk_nilai_saldo_awal;							
			else{
				/*$sql = "SELECT
							(hpp_nilai_awal + hpp_nilai_masuk - hpp_nilai_keluar) as nilai_awal
						FROM hpp_bulan
						WHERE
							hpp_bulan = $bulan_sebelum AND hpp_tahun = $tahun
							AND hpp_produk_id = $produk_id";*/
				//query di atas memiliki kelemahan jika bulan sebelumnya kosong / tdk ada transaksi, shg menghasilkan null
				
				$sql = "SELECT
							(hpp_nilai_awal + hpp_nilai_masuk - hpp_nilai_keluar) as nilai_awal
						FROM hpp_bulan
						WHERE
							hpp_bulan <= $bulan_sebelum AND hpp_tahun = $tahun
							AND hpp_produk_id = $produk_id
						ORDER BY hpp_bulan DESC LIMIT 1";
				
				$rs_sql 	= $this->db->query($sql);
				$row_sql	= $rs_sql->row();
				
				if($rs_sql->num_rows()>0)
					return $row_sql->nilai_awal;											
				else return 0;
			}	
		}
		else return 0;
	}

	function get_qty_masuk($periode_start, $periode_end, $produk_id, $konversi_nilai, &$qty_retur){
					
		//cek dulu, apakah $periode_start tidak lebih kecil dari inisialisasi awal master produk (produk_tgl_nilai_saldo_awal)
		$sql_tgl_awal =
			   "SELECT produk_tgl_nilai_saldo_awal
				FROM produk
				WHERE 
					produk_id = '".$produk_id."'
					AND produk_tgl_nilai_saldo_awal <= $periode_start";
				
		$query_tgl_awal	= $this->db->query($sql_tgl_awal);
		
		if($query_tgl_awal->num_rows()>0){
			
			$sql_kategori	= "SELECT produk_kategori FROM produk WHERE produk_id = $produk_id";
			$rs_kategori	= $this->db->query($sql_kategori);
			$row_kategori	= $rs_kategori->row();
			
			//produk retail
			if ($row_kategori->produk_kategori == 1 || $row_kategori->produk_kategori == 14){
				$sql = "SELECT
							/*IFNULL(SUM((ks.ks_masuk * sk.konversi_nilai / $konversi_nilai) - 
											(ks.ks_keluar * sk.konversi_nilai / $konversi_nilai)), 0) as qty_masuk,*/
							IFNULL(SUM(IF(ks.ks_jenis != 'retur_jual_produk', 
										  (ks.ks_masuk * sk.konversi_nilai / $konversi_nilai) - 
											(ks.ks_keluar * sk.konversi_nilai / $konversi_nilai), 0)), 0) AS qty_masuk,
							IFNULL(SUM(IF(ks.ks_jenis = 'retur_jual_produk', 
										  (ks.ks_masuk * sk.konversi_nilai / $konversi_nilai) - 
											(ks.ks_keluar * sk.konversi_nilai / $konversi_nilai), 0)), 0) AS qty_masuk_retur
						FROM kartu_stok_fix ks
						LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
						WHERE 
							ks.ks_produk_id = '".$produk_id."'
							AND ks.ks_tgl_faktur BETWEEN ".$periode_start." AND ".$periode_end."
							AND (ks.ks_jenis = 'terima_beli' OR ks.ks_jenis = 'terima_beli_bonus' OR ks.ks_jenis = 'retur_jual_produk'
									OR (ks.ks_jenis = 'mutasi_racik' AND ks.ks_masuk > 0)
									OR (ks.ks_jenis = 'mutasi' AND ks.ks_masuk > 0 AND ks.ks_no_faktur LIKE 'MB-K%') #khusus dari Kabin
									OR (ks.ks_jenis = 'koreksi_stok' AND ks.ks_masuk > 0)
									OR (ks.ks_jenis = 'retur_beli' AND ks.ks_masuk <> 0))
							AND (ks.ks_gudang_id = 2 OR ks.ks_gudang_id = 1)";				
			}			
			//produk lainnya
			else
				$sql = "SELECT
							/*IFNULL(SUM((ks.ks_masuk * sk.konversi_nilai / $konversi_nilai) - 
											(ks.ks_keluar * sk.konversi_nilai / $konversi_nilai)), 0) as qty_masuk,*/
							IFNULL(SUM(IF(ks.ks_jenis != 'retur_jual_produk', 
										  (ks.ks_masuk * sk.konversi_nilai / $konversi_nilai) - 
											(ks.ks_keluar * sk.konversi_nilai / $konversi_nilai), 0)), 0) AS qty_masuk,
							0 AS qty_masuk_retur
						FROM kartu_stok_fix ks
						LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
						WHERE 
							ks.ks_produk_id = '".$produk_id."'
							AND ks.ks_tgl_faktur BETWEEN ".$periode_start." AND ".$periode_end."
							AND (ks.ks_jenis = 'terima_beli' OR ks.ks_jenis = 'terima_beli_bonus' OR ks.ks_jenis = 'retur_jual_produk' 
									OR (ks.ks_jenis = 'mutasi_racik' AND ks.ks_masuk > 0)
									#OR (ks.ks_jenis = 'mutasi' AND ks.ks_masuk > 0 AND ks.ks_no_faktur LIKE 'MB-GR%' AND ks.ks_gudang_id <> 1) #Khusus dari GR dan tidak ke GB
									OR (ks.ks_jenis = 'koreksi_stok' AND ks.ks_masuk <> 0)
									OR (ks.ks_jenis = 'retur_beli' AND ks.ks_masuk <> 0))
							#AND ks.ks_gudang_id <> 2
							#2015-10-29: MB dari GR ke Kabin tidak lagi dianggap barang masuk. Yang dianggap barang masuk adalah dari PB";
			
			$query_nbeli = $this->db->query($sql);
			$nbeli = $query_nbeli->row();
						
			$qty_retur = $nbeli->qty_masuk_retur;
			return $nbeli->qty_masuk;
		}
		else 
			return 0;
	}
		
	function get_nilai_retur($periode_start, $periode_end, $produk_id, $konversi_nilai){
					
		//cek dulu, apakah $periode_start tidak lebih kecil dari inisialisasi awal master produk (produk_tgl_nilai_saldo_awal)
		$sql_tgl_awal =
			   "SELECT 
					IF(produk_tgl_khusus_nilai_saldo_awal = '0000-00-00', produk_tgl_nilai_saldo_awal, produk_tgl_khusus_nilai_saldo_awal) AS produk_tgl_nilai_saldo_awal
				FROM produk
				WHERE 
					produk_id = '".$produk_id."'
					AND IF(produk_tgl_khusus_nilai_saldo_awal = '0000-00-00', produk_tgl_nilai_saldo_awal, produk_tgl_khusus_nilai_saldo_awal) <= ".$periode_start;
				
		$query_tgl_awal	= $this->db->query($sql_tgl_awal);
		
		if($query_tgl_awal->num_rows()>0){	
			$sql = "select 
						ifnull(sum(d.drproduk_jumlah * (h.hpp_nilai_keluar / h.hpp_qty_keluar)), 0) as nilai_retur
					from 
						detail_retur_jual_produk d
						join master_retur_jual_produk m on m.rproduk_id = d.drproduk_master
						join master_jual_produk mj on mj.jproduk_id = m.rproduk_nobuktijual
						left join hpp_bulan h on h.hpp_produk_id = d.drproduk_produk and h.hpp_satuan_id = d.drproduk_satuan
					where
						d.drproduk_produk = '".$produk_id."' 
						and h.hpp_tahun = date_format(mj.jproduk_tanggal, '%Y') 
						and h.hpp_bulan = date_format(mj.jproduk_tanggal, '%m')
						and m.rproduk_tanggal >= ".$periode_start."
						and m.rproduk_tanggal <= ".$periode_end."
						and m.rproduk_stat_dok = 'Tertutup'";		
			$query_nretur = $this->db->query($sql);
			
			if($query_nretur->num_rows()){
				$nretur =$query_nretur->row();
				return $nretur->nilai_retur;
			}else
				return 0;
		}
		else return 0;
	}	
	
	function get_nilai_masuk($periode_start, $periode_end, $produk_id, $konversi_nilai){
	//rp_retur tidak termasuk, karena rp retur mengikuti bulan berjalan (dihitung terpisah)
					
		//cek dulu, apakah $periode_start tidak lebih kecil dari inisialisasi awal master produk (produk_tgl_nilai_saldo_awal)
		$sql_tgl_awal =
			   "SELECT 
					IF(produk_tgl_khusus_nilai_saldo_awal = '0000-00-00', produk_tgl_nilai_saldo_awal, produk_tgl_khusus_nilai_saldo_awal) AS produk_tgl_nilai_saldo_awal
				FROM produk
				WHERE 
					produk_id = '".$produk_id."'
					AND IF(produk_tgl_khusus_nilai_saldo_awal = '0000-00-00', produk_tgl_nilai_saldo_awal, produk_tgl_khusus_nilai_saldo_awal) <= ".$periode_start;
				
		$query_tgl_awal	= $this->db->query($sql_tgl_awal);
		
		if($query_tgl_awal->num_rows()>0){				
		
			$sql_kategori	= "SELECT produk_kategori FROM produk WHERE produk_id = $produk_id";
			$rs_kategori	= $this->db->query($sql_kategori);
			$row_kategori	= $rs_kategori->row();
			
			//produk retail
			if ($row_kategori->produk_kategori == 1 || $row_kategori->produk_kategori == 14)			
				$sql = "SELECT 
							ifnull(SUM((ks.ks_masuk_rp /** sk.konversi_nilai / $konversi_nilai*/) -
											(ks.ks_keluar_rp /** sk.konversi_nilai / $konversi_nilai*/)), 0) as masuk_rp
						FROM kartu_stok_fix ks
						LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
						WHERE 
							ks.ks_produk_id = '".$produk_id."'
							AND ks.ks_tgl_faktur BETWEEN ".$periode_start." AND ".$periode_end."
							AND (ks.ks_jenis = 'terima_beli' /*OR ks.ks_jenis = 'retur_jual_produk' */
									OR (ks.ks_jenis = 'mutasi_racik' AND ks.ks_masuk > 0)
									OR (ks.ks_jenis = 'mutasi' AND ks.ks_masuk > 0 AND ks.ks_no_faktur LIKE 'MB-K%') #khusus dari Kabin
									OR (ks.ks_jenis = 'koreksi_stok' AND ks.ks_masuk > 0)
									OR (ks.ks_jenis = 'retur_beli' AND ks.ks_masuk <> 0))
							AND (ks.ks_gudang_id = 2 OR ks.ks_gudang_id = 1)";															
			
			//produk lainnya
			else
				$sql = "SELECT 
							ifnull(SUM((ks.ks_masuk_rp /** sk.konversi_nilai / $konversi_nilai*/) -
											(ks.ks_keluar_rp /** sk.konversi_nilai / $konversi_nilai*/)), 0) as masuk_rp
						FROM kartu_stok_fix ks
						LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
						WHERE 
							ks.ks_produk_id = '".$produk_id."'
							AND ks.ks_tgl_faktur BETWEEN ".$periode_start." AND ".$periode_end."
							AND (ks.ks_jenis = 'terima_beli' /*OR ks.ks_jenis = 'retur_jual_produk' */
									OR (ks.ks_jenis = 'mutasi_racik' AND ks.ks_masuk > 0)
									#OR (ks.ks_jenis = 'mutasi' AND ks.ks_masuk > 0)
									OR (ks.ks_jenis = 'koreksi_stok' AND ks.ks_masuk > 0)
									OR (ks.ks_jenis = 'retur_beli' AND ks.ks_masuk <> 0))
							#AND ks.ks_gudang_id <> 2
							#2015-10-29: MB dari GR ke Kabin tidak lagi dianggap barang masuk. Yang dianggap barang masuk adalah dari PB";
			
			$query_nbeli = $this->db->query($sql);
			
			if($query_nbeli->num_rows()){
				$nbeli =$query_nbeli->row();
				//print_r($nbeli->masuk_rp);
				return $nbeli->masuk_rp;
			}else
				return 0;
		}
		else return 0;
	}	
	
	function get_qty_keluar($periode_start, $periode_end, $produk_id, $konversi_nilai){
					
		//cek dulu, apakah $periode_start tidak lebih kecil dari inisialisasi awal master produk (produk_tgl_nilai_saldo_awal)
		$sql_tgl_awal =
			   "SELECT produk_tgl_nilai_saldo_awal
				FROM produk
				WHERE 
					produk_id = '".$produk_id."'
					AND produk_tgl_nilai_saldo_awal <= $periode_start";
				
		$query_tgl_awal	= $this->db->query($sql_tgl_awal);
		
		if($query_tgl_awal->num_rows()>0){
			
			$sql_kategori	= "SELECT produk_kategori FROM produk WHERE produk_id = $produk_id";
			$rs_kategori	= $this->db->query($sql_kategori);
			$row_kategori	= $rs_kategori->row();
			
			//produk retail & lain-lain
			if ($row_kategori->produk_kategori == 1 || $row_kategori->produk_kategori == 14 || $row_kategori->produk_kategori == 13)
				$sql = "SELECT
							IFNULL(SUM(stok_keluar - stok_masuk), 0) as qty_keluar
						FROM
						(
							SELECT
								ks.ks_masuk * sk.konversi_nilai / $konversi_nilai as stok_masuk,
								ks.ks_keluar * sk.konversi_nilai / $konversi_nilai as stok_keluar
							FROM kartu_stok_fix ks
							LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
							WHERE 
								ks.ks_produk_id = '".$produk_id."'
								AND ks.ks_tgl_faktur BETWEEN ".$periode_start." AND ".$periode_end."
								AND (ks.ks_jenis = 'jual_produk' OR ks.ks_jenis = 'retur_produk' 
										OR ((ks.ks_jenis = 'koreksi_stok' OR ks.ks_jenis = 'mutasi_keluar' 
														OR ks.ks_jenis = 'mutasi_racik' 
														OR ks.ks_jenis = 'mutasi_retail_kabin') #mutasi retail ke kabin
												AND ks.ks_keluar > 0))
								AND (ks.ks_gudang_id = 2 OR ks.ks_gudang_id = 1)
						)
						AS ks_gabungan";
			
			//produk lainnya
			else
				$sql = "SELECT 
							IFNULL(SUM(stok_keluar - stok_masuk), 0) as qty_keluar
						FROM
						(
							SELECT
								ks.ks_masuk * sk.konversi_nilai / $konversi_nilai as stok_masuk,
								ks.ks_keluar * sk.konversi_nilai / $konversi_nilai as stok_keluar
							FROM kartu_stok_fix ks
							LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
							WHERE 
								ks.ks_produk_id = '".$produk_id."'
								AND ks.ks_tgl_faktur BETWEEN ".$periode_start." AND ".$periode_end."
								AND (ks.ks_jenis = 'jual_produk' OR ks.ks_jenis = 'retur_produk' 
										OR ((ks.ks_jenis = 'koreksi_stok' OR ks.ks_jenis = 'mutasi_keluar' 
														OR ks.ks_jenis = 'mutasi_racik' #OR ks.ks_jenis = 'mutasi' #seharusnya mutasi ke GR saja, namun querynya belum bisa
														) 
												AND ks.ks_keluar > 0))
								AND ks.ks_gudang_id <> 2

							UNION ALL	

							SELECT
								0 as stok_masuk,
								d.cabin_jumlah * sk.konversi_nilai / $konversi_nilai as stok_keluar /*krn pasti sat. terkecil*/
							FROM
								detail_pakai_cabin d
								LEFT JOIN satuan_konversi sk ON d.cabin_satuan = sk.konversi_satuan AND d.cabin_produk = sk.konversi_produk
							WHERE 
								d.cabin_produk = '".$produk_id."'
								AND date_format(cabin_date_create,'%Y-%m-%d') BETWEEN ".$periode_start." AND ".$periode_end."
						)
						AS ks_gabungan";
									
			$query_nbeli = $this->db->query($sql);			
			$nbeli =$query_nbeli->row();
			return $nbeli->qty_keluar;		
		}
		else 
			return 0;
	}
		
	function get_qty_akhir($tanggal_akhir, $produk_id){
	
		//Saldo Awal Master Produk (dari semua gudang)
		$sql_saldo_awal=
		   "SELECT
				(produk_saldo_awal + produk_saldo_awal2 + produk_saldo_awal3 + produk_saldo_awal4) / konversi_nilai  as jumlah,
				produk_tgl_nilai_saldo_awal
			FROM produk, satuan_konversi
			WHERE 
				konversi_produk = produk_id
				AND	konversi_default = true
				AND	produk_id = '".$produk_id."'
				AND produk_tgl_nilai_saldo_awal < ".$tanggal_akhir;
				
		$rs_saldo_awal	= $this->db->query($sql_saldo_awal) or die("Error - 1.1 : ".$sql_saldo_awal);
		
		//dari hasil query di atas, akan diketahui apakah tanggal_akhir >= produk_tgl_nilai_saldo_awal? jika tidak maka return 0
		if($rs_saldo_awal->num_rows()>0){
					
			$row_saldo_awal	= $rs_saldo_awal->row();
		
			//Saldo transaksi, dihitung sejak produk_tgl_nilai_saldo_awal di master produk
			
			$sql_kategori	= "SELECT produk_kategori FROM produk WHERE produk_id = $produk_id";
			$rs_kategori	= $this->db->query($sql_kategori);
			$row_kategori	= $rs_kategori->row();
			
			//produk retail
			if ($row_kategori->produk_kategori == 1 || $row_kategori->produk_kategori == 14)						

				$sql = "SELECT
							IFNULL(SUM(stok_masuk - stok_keluar), 0) as stok_saldo
						FROM
						(
							SELECT
								ks.ks_masuk * sk.konversi_nilai / $konversi_nilai as stok_masuk,
								ks.ks_keluar * sk.konversi_nilai / $konversi_nilai as stok_keluar
							FROM kartu_stok_fix ks
							LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
							WHERE 
								ks.ks_produk_id = '".$produk_id."'
								AND ks.ks_tgl_faktur BETWEEN '".$row_saldo_awal->produk_tgl_nilai_saldo_awal."' AND ".$tanggal_akhir."
								AND (ks.ks_gudang_id = 2 OR ks.ks_gudang_id = 1)
						) 
						AS ks_gabungan";
			
			//produk lainnya
			else
				
				$sql = "SELECT
							IFNULL(SUM(stok_masuk - stok_keluar), 0) as stok_saldo
						FROM
						(
							SELECT
								ks.ks_masuk * sk.konversi_nilai / $konversi_nilai as stok_masuk,
								ks.ks_keluar * sk.konversi_nilai / $konversi_nilai as stok_keluar
							FROM kartu_stok_fix ks
							LEFT JOIN satuan_konversi sk ON (sk.konversi_satuan = ks.ks_satuan_id) AND (sk.konversi_produk = ks.ks_produk_id)
							WHERE 
								ks.ks_produk_id = '".$produk_id."'
								AND ks.ks_tgl_faktur BETWEEN '".$row_saldo_awal->produk_tgl_nilai_saldo_awal."' AND ".$tanggal_akhir."
								AND ks.ks_gudang_id <> 2

							UNION ALL

							SELECT
								0 as stok_masuk,
								d.cabin_jumlah * sk.konversi_nilai / $konversi_nilai as stok_keluar /*krn pasti sat. terkecil*/
							FROM
								detail_pakai_cabin d
								LEFT JOIN satuan_konversi sk ON d.cabin_satuan = sk.konversi_satuan AND d.cabin_produk = sk.konversi_produk
							WHERE 
								d.cabin_produk = '".$produk_id."'
								AND date_format(cabin_date_create,'%Y-%m-%d') BETWEEN '".$row_saldo_awal->produk_tgl_nilai_saldo_awal."' AND ".$tanggal_akhir."
						) 
						AS ks_gabungan";
									
			$query = $this->db->query($sql);
		
		
			if($query->num_rows()>0){
				$row = $query->row();
				$total = $row_saldo_awal->jumlah + $row->stok_saldo;
				//print_r(' sql: '.$sql.', saldo awal: '.$row_saldo_awal->jumlah.', stok saldo: '.$row->stok_saldo);
				return $total;
			}
			elseif($query->num_rows()==0){
				return $row_saldo_awal->jumlah;
				//print_r($tanggal_akhir.$row_saldo_awal->jumlah);
			}
		}
		else{
			return 0;
		}
	}

	function faktur_hpp_generate($jns_faktur, $master_id){
		
		if($jns_faktur == 'jual_produk'){
			$sql_konversi =	   "SELECT	
									v.konversi_nilai, v.satuan_id, v.produk_id, 
									date_format(m.jproduk_tanggal, '%Y') AS tahun, date_format(m.jproduk_tanggal, '%m') AS bulan
								FROM detail_jual_produk d
								LEFT JOIN master_jual_produk m ON m.jproduk_id = d.dproduk_master
								LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.dproduk_produk
								WHERE d.dproduk_master = $master_id";
			$result	= $this->db->query($sql_konversi);						
		}
		elseif($jns_faktur == 'retur_produk'){
			$sql_konversi =	   "SELECT	
									v.konversi_nilai, v.satuan_id, v.produk_id,
									date_format(m.rproduk_tanggal, '%Y') AS tahun, date_format(m.rproduk_tanggal, '%m') AS bulan
								FROM detail_retur_jual_produk d
								LEFT JOIN master_retur_jual_produk m ON m.rproduk_id = d.drproduk_master
								LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.drproduk_produk
								WHERE m.rproduk_nobukti = '$master_id'";
			$result	= $this->db->query($sql_konversi);						
		}
		elseif($jns_faktur == 'terima_beli'){
			$sql_konversi =	   "SELECT	
									v.konversi_nilai, v.satuan_id, v.produk_id, 
									date_format(m.terima_tanggal, '%Y') AS tahun, date_format(m.terima_tanggal, '%m') AS bulan
								FROM detail_terima_beli d
								LEFT JOIN master_terima_beli m ON m.terima_id = d.dterima_master
								LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.dterima_produk
								WHERE d.dterima_master = '$master_id'";
			$result	= $this->db->query($sql_konversi);						
		}
		elseif($jns_faktur == 'mutasi'){
			$sql_konversi =	   "SELECT	
									v.konversi_nilai, v.satuan_id, v.produk_id, 
									date_format(m.mutasi_tanggal, '%Y') AS tahun, date_format(m.mutasi_tanggal, '%m') AS bulan
								FROM detail_mutasi d
								LEFT JOIN master_mutasi m ON m.mutasi_id = d.dmutasi_master
								LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.dmutasi_produk
								WHERE d.dmutasi_master = '$master_id'";
			$result	= $this->db->query($sql_konversi);						
		}
		elseif($jns_faktur == 'order_beli'){
			//pengecekan apakah PB sudah ada PT lakukan di v (jadi perubahan harga di OP tidak lg ngefek utk PB yg sudah ada PTnya
			
			$sql_konversi =	   "SELECT 
									distinct v.konversi_nilai, v.satuan_id, v.produk_id, 
									date_format(m.terima_tanggal, '%Y') AS tahun, date_format(m.terima_tanggal, '%m') AS bulan
								FROM detail_terima_beli d
								LEFT JOIN master_terima_beli m on m.terima_id = d.dterima_master
								LEFT JOIN master_order_beli mo ON mo.order_id = m.terima_order
								LEFT JOIN detail_order_beli do ON do.dorder_master = mo.order_id
								LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.dterima_produk
								WHERE 
									do.dorder_id = $master_id AND do.dorder_produk = d.dterima_produk";
			$result	= $this->db->query($sql_konversi);						
		}
		elseif($jns_faktur == 'terima_invoice'){
			$sql_konversi =	   "SELECT	
									v.konversi_nilai, v.satuan_id, v.produk_id, 
									date_format(m.invoice_tanggal, '%Y') AS tahun, date_format(m.invoice_tanggal, '%m') AS bulan
								FROM detail_invoice d
								LEFT JOIN master_invoice m ON m.invoice_id = d.dinvoice_master
								LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.dinvoice_produk
								WHERE d.dinvoice_master = $master_id";
			$result	= $this->db->query($sql_konversi);						
		}
		elseif($jns_faktur == 'retur_beli'){
			$sql_konversi =	   "SELECT	
									v.konversi_nilai, v.satuan_id, v.produk_id, 
									date_format(m.rbeli_tanggal, '%Y') AS tahun, date_format(m.rbeli_tanggal, '%m') AS bulan
								FROM detail_retur_beli d
								LEFT JOIN master_retur_beli m ON m.rbeli_id = d.drbeli_master
								LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.drbeli_produk
								WHERE d.drbeli_master = $master_id";
			$result	= $this->db->query($sql_konversi);						
		}
		elseif($jns_faktur == 'koreksi_stok'){
			$sql_konversi =	   "SELECT	
									v.konversi_nilai, v.satuan_id, v.produk_id, 
									date_format(m.koreksi_tanggal, '%Y') AS tahun, date_format(m.koreksi_tanggal, '%m') AS bulan
								FROM detail_koreksi_stok d
								LEFT JOIN master_koreksi_stok m ON m.koreksi_id = d.dkoreksi_master
								LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.dkoreksi_produk
								WHERE d.dkoreksi_master = $master_id";
			$result	= $this->db->query($sql_konversi);						
		}
		
		foreach($result->result() as $row_konversi){
			$this->hpp_generate_bulan($row_konversi->produk_id, $row_konversi->satuan_id, $row_konversi->konversi_nilai, $row_konversi->tahun, $row_konversi->bulan);
			//$this->hpp_generate($row_konversi->produk_id, $row_konversi->satuan_id, $row_konversi->konversi_nilai, $row_konversi->tahun);
		}
	}
	
	//generate hpp untuk produk tertentu di nilai terima produk racik
	function hpp_generate_produk_racik($produk_id, $tahun, $bulan){
		$sql_konversi =	   "SELECT	
									v.konversi_nilai, v.satuan_id, v.produk_id
							FROM vu_produk_satuan_default v
							WHERE v.produk_id = $produk_id";
		
		$result	= $this->db->query($sql_konversi);										
				
		foreach($result->result() as $row_konversi)
			$this->hpp_generate_bulan($row_konversi->produk_id, $row_konversi->satuan_id, $row_konversi->konversi_nilai, $tahun, $bulan);
	}
	
	//function utk update temp_nilai_terima_racik
	function hpp_generate_mutasi_racik($mutasi_id){
		$sql = "SELECT
					d.dmutasi_produk, date_format(m.mutasi_tanggal, '%Y') AS tahun, date_format(m.mutasi_tanggal, '%m') AS bulan,
					v.konversi_nilai, v.satuan_id
				FROM detail_mutasi d
				LEFT JOIN master_mutasi m ON m.mutasi_id = d.dmutasi_master
				LEFT JOIN vu_produk_satuan_default v on v.produk_id = d.dmutasi_produk
				WHERE m.mutasi_id = $mutasi_id";
		
		$result	= $this->db->query($sql);
		
		//masih menggunakan hpp_rawat_list, karena di dalamnya ada script utk mengupdate temp_nilai_terima_racik (modulnya gabung dg hpp perawatan)
		foreach($result->result() as $row){
			
			//print_r('dmutasi_produk : ' . $row->dmutasi_produk . ', satuan_id : ' . $row->satuan_id . ', konversi_nilai : ' . $row->konversi_nilai . ', bulan : ' . $row->bulan . ', tahun : ' . $row->tahun);			
			
			$this->hpp_rawat_list($row->dmutasi_produk, '', '', $row->bulan, $row->tahun, 1, 'produk_racik');									
			
			$this->hpp_generate_bulan($row->dmutasi_produk, $row->satuan_id, $row->konversi_nilai, $row->tahun, $row->bulan);
			
		}
	}
	
	//function utk update temp_nilai_terima_racik
	function hpp_generate_mutasi_racik_akhir_bulan(){
		$sql = "SELECT
					p.produk_id, year(now()) AS tahun, lpad(month(now()), 2, '0') AS bulan, v.konversi_nilai, v.satuan_id
				FROM produk p
				LEFT JOIN vu_produk_satuan_default v on v.produk_id = p.produk_id
				WHERE 
					p.produk_aktif = 'Aktif' and p.produk_racikan = 1 and v.konversi_nilai is not null";
		$result	= $this->db->query($sql);
		
		//masih menggunakan hpp_rawat_list, karena di dalamnya ada script utk mengupdate temp_nilai_terima_racik (modulnya gabung dg hpp perawatan)
		foreach($result->result() as $row){
			
			//print_r('dmutasi_produk : ' . $row->dmutasi_produk . ', satuan_id : ' . $row->satuan_id . ', konversi_nilai : ' . $row->konversi_nilai . ', bulan : ' . $row->bulan . ', tahun : ' . $row->tahun);			
			
			$this->hpp_rawat_list($row->produk_id, '', '', $row->bulan, $row->tahun, 1, 'produk_racik');									
			
			$this->hpp_generate_bulan($row->produk_id, $row->satuan_id, $row->konversi_nilai, $row->tahun, $row->bulan, true);
			
		}
		
		//insert ke tabel log
		$sql = "insert into log_system(log_procedure_name, log_execution_time) values ('generate hpp mutasi racik akhir bulan',now())";
		$this->db->query($sql);
	}
	
	//function utk update temp_nilai_terima_racik
	function hpp_generate_akhir_bulan(){
		$sql = "SELECT
					p.produk_id, year(now()) AS tahun, lpad(month(now()), 2, '0') AS bulan, v.konversi_nilai, v.satuan_id
				FROM 
					produk p
					LEFT JOIN vu_produk_satuan_default v on v.produk_id = p.produk_id
					left join (
						select 
							distinct(pracikan_produk) 
						from 
							produk_racikan pr2
							join produk p2 on pr2.pracikan_master = p2.produk_id
						where
							p2.produk_aktif = 'Aktif'
					) pr on pr.pracikan_produk = p.produk_id
				WHERE 
					p.produk_aktif = 'Aktif' and p.produk_racikan = 0 and pr.pracikan_produk is null and v.konversi_nilai is not null";
		$result	= $this->db->query($sql);
		
		foreach($result->result() as $row){
			$this->hpp_generate_bulan($row->produk_id, $row->satuan_id, $row->konversi_nilai, $row->tahun, $row->bulan, true);
			
		}
		
		//insert ke tabel log
		$sql = "insert into log_system(log_procedure_name, log_execution_time) values ('generate hpp akhir bulan',now())";
		$this->db->query($sql);
	}
	
	function hpp_rawat_list($produk_racik_id, $rawat_id, $group1_id, $bulan, $tahun, $hpp_rawat_generate, $opsi){
		$sql_perawatan_where = "";
		if(($opsi == 'semua') && ($hpp_rawat_generate == 1)){
			$sql_konversi =	"SELECT DISTINCT konversi_nilai, satuan_id, produk_id
						    FROM perawatan_konsumsi d
								LEFT JOIN perawatan r ON r.rawat_id = d.krawat_master
								LEFT JOIN vu_produk_satuan_default p ON p.produk_id = d.krawat_produk
						    WHERE r.rawat_aktif = 'Aktif'";
			$result	= $this->db->query($sql_konversi);
						
			foreach($result->result() as $row_konversi)
				$this->hpp_generate($row_konversi->produk_id, $row_konversi->satuan_id, $row_konversi->konversi_nilai, $tahun);
		}		
		
		if($opsi == 'group1'){
			$sql_perawatan_where = "AND g.group_id = $group1_id";
			
			if ($hpp_rawat_generate == 1){
				$sql_konversi =	"SELECT DISTINCT konversi_nilai, satuan_id, produk_id
								FROM perawatan_konsumsi d
									LEFT JOIN perawatan r ON r.rawat_id = d.krawat_master
									LEFT JOIN vu_produk_satuan_default p ON p.produk_id = d.krawat_produk
								WHERE r.rawat_aktif = 'Aktif' AND rawat_group = $group1_id";
				$result	= $this->db->query($sql_konversi);

				foreach($result->result() as $row_konversi)
					$this->hpp_generate($row_konversi->produk_id, $row_konversi->satuan_id, $row_konversi->konversi_nilai, $tahun);
			}						
		}	
		
		elseif($opsi == 'rawat'){	//per perawatan
			$sql_perawatan_where = "AND r.rawat_id = $rawat_id";
			
			if ($hpp_rawat_generate == 1){	
				$sql_konversi = "SELECT konversi_nilai, satuan_id, produk_id
								FROM perawatan_konsumsi d
									LEFT JOIN perawatan r ON r.rawat_id = d.krawat_master
									LEFT JOIN vu_produk_satuan_default p ON p.produk_id = d.krawat_produk
								WHERE r.rawat_aktif = 'Aktif' AND rawat_id = $rawat_id";
				$result	= $this->db->query($sql_konversi);
				$row_konversi = $result->row();

				foreach($result->result() as $row_konversi)
					$this->hpp_generate($row_konversi->produk_id, $row_konversi->satuan_id, $row_konversi->konversi_nilai, $tahun);
			}						
		}
		elseif($opsi == 'produk_racik'){
		    if ($hpp_rawat_generate == 1){
				$sql_hpp_produk =  
				   "SELECT
					   hpp_bulan, hpp_tahun, SUM(hpp_nilai_satuan) AS hpp_nilai_satuan,
						(SELECT produk_nama FROM produk WHERE produk_id = $produk_racik_id) AS produk_nama
					FROM 
					(
						SELECT 
							hpp_bulan, hpp_tahun, 
							IFNULL(hb.hpp_nilai_keluar / hb.hpp_qty_keluar * pr.pracikan_jumlah /
										 (sk_hpp.konversi_nilai / sk.konversi_nilai)
									, 0) AS hpp_nilai_satuan
						FROM produk_racikan pr 
						LEFT JOIN hpp_bulan hb ON hb.hpp_produk_id = pr.pracikan_produk
						LEFT JOIN produk p ON hb.hpp_produk_id = p.produk_id
						LEFT JOIN satuan s ON pr.pracikan_satuan = s.satuan_id
						LEFT JOIN satuan_konversi sk_hpp ON sk_hpp.konversi_satuan = hb.hpp_satuan_id AND 
														sk_hpp.konversi_produk = pr.pracikan_produk
						LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = pr.pracikan_satuan AND 
														sk.konversi_produk = pr.pracikan_produk														
						WHERE 
							hb.hpp_tahun = $tahun AND hb.hpp_bulan = $bulan AND 
							p.produk_aktif = 'Aktif' AND pr.pracikan_master = $produk_racik_id
					) AS temp
					GROUP BY hpp_bulan, hpp_tahun";
			
				$result	= $this->db->query($sql_hpp_produk);

				foreach($result->result() as $row_hpp_produk){
					$this->update_nilai_terima_produk_racik($row_hpp_produk->hpp_bulan, $row_hpp_produk->hpp_tahun, $produk_racik_id, $row_hpp_produk->produk_nama, $row_hpp_produk->hpp_nilai_satuan);
					$this->hpp_list($produk_racik_id, '', $row_hpp_produk->hpp_tahun, '', $row_hpp_produk->hpp_bulan, $row_hpp_produk->hpp_tahun, 1, '', 'produk', 'Semua', 'Semua', 'Semua', 'Semua');
				}
		    }
		}
		elseif($opsi == 'produk_racik_all'){
		    if ($hpp_rawat_generate == 1){
				$sql_hpp_produk =  
				   "SELECT
					   hpp_bulan, hpp_tahun, SUM(hpp_nilai_satuan) AS hpp_nilai_satuan, produk_nama, pracikan_master
					FROM 
					(
						SELECT 
							hpp_bulan, hpp_tahun, 
							IFNULL(hb.hpp_nilai_keluar / hb.hpp_qty_keluar * pr.pracikan_jumlah /
										 (sk_hpp.konversi_nilai / sk.konversi_nilai)
									, 0) AS hpp_nilai_satuan, pr.pracikan_master, p2.produk_nama
						FROM produk_racikan pr 
						LEFT JOIN hpp_bulan hb ON hb.hpp_produk_id = pr.pracikan_produk
						LEFT JOIN produk p ON hb.hpp_produk_id = p.produk_id
						LEFT JOIN produk p2 ON pr.pracikan_master = p2.produk_id
						LEFT JOIN satuan s ON pr.pracikan_satuan = s.satuan_id
						LEFT JOIN satuan_konversi sk_hpp ON sk_hpp.konversi_satuan = hb.hpp_satuan_id AND 
														sk_hpp.konversi_produk = pr.pracikan_produk
						LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = pr.pracikan_satuan AND 
														sk.konversi_produk = pr.pracikan_produk														
						WHERE 
							hb.hpp_tahun = $tahun AND hb.hpp_bulan = $bulan AND 
							p.produk_aktif = 'Aktif' and p2.produk_aktif = 'Aktif'
					) AS temp
					GROUP BY hpp_bulan, hpp_tahun, produk_nama, pracikan_master";
			
				$result	= $this->db->query($sql_hpp_produk);

				foreach($result->result() as $row_hpp_produk){
					$this->update_nilai_terima_produk_racik($row_hpp_produk->hpp_bulan, $row_hpp_produk->hpp_tahun, $row_hpp_produk->pracikan_master, $row_hpp_produk->produk_nama, $row_hpp_produk->hpp_nilai_satuan);
					$this->hpp_list($row_hpp_produk->pracikan_master, '', $row_hpp_produk->hpp_tahun, '', $row_hpp_produk->hpp_bulan, $row_hpp_produk->hpp_tahun, 1, '', 'produk', 'Semua', 'Semua', 'Semua', 'Semua');
				}
					
		    }
		}
		
		$sql_list = "";
		$nbrows_perawatan = -1;
		if($opsi == "produk_racik" or $opsi == "produk_racik_all"){
		    $sql_list = "SELECT CONCAT('(', p.produk_kode, ') ', p.produk_nama) AS perawatan_nama,
					p2.produk_id, p2.produk_kode, p2.produk_nama, pr.pracikan_jumlah AS krawat_jumlah, s.satuan_kode,
					ROUND(IFNULL(hb.hpp_nilai_keluar, 0) / IFNULL(hb.hpp_qty_keluar, 0) / IFNULL(sk.konversi_nilai, 0), 2) AS hpp_satuan_terkecil, 
					pr.pracikan_jumlah * ROUND(IFNULL(hb.hpp_nilai_keluar, 0) / IFNULL(hb.hpp_qty_keluar, 0) / IFNULL(sk.konversi_nilai, 0), 2) AS sub_total, 
					0 AS jumlah_tindakan, 0 AS total
				 FROM produk_racikan pr
					LEFT JOIN hpp_bulan hb ON pr.pracikan_produk = hb.hpp_produk_id
				    LEFT JOIN produk p ON pr.pracikan_master = p.produk_id
				    LEFT JOIN produk p2 ON pr.pracikan_produk = p2.produk_id
				    LEFT JOIN satuan s ON pr.pracikan_satuan = s.satuan_id
					LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = hb.hpp_satuan_id AND sk.konversi_produk = pr.pracikan_produk
				 WHERE p.produk_aktif = 'Aktif' AND hb.hpp_bulan = $bulan AND hb.hpp_tahun = $tahun ";

			if($opsi == "produk_racik")
				$sql_list .= " AND pr.pracikan_master = $produk_racik_id";
		} else {
			$sql_perawatan = "SELECT r.rawat_id 
							FROM perawatan r
								LEFT JOIN produk_group g ON g.group_id = r.rawat_group
							WHERE r.rawat_aktif = 'Aktif' $sql_perawatan_where 
							ORDER BY r.rawat_id";
		    $result_perawatan = $this->db->query($sql_perawatan);
			$nbrows_perawatan = $result_perawatan->num_rows();
			
			if($nbrows_perawatan>0){
				/*empty tabel hpp_rawat_pakai_cabin_temp & hpp_rawat_total_temp*/
				$sql_empty = "DELETE FROM hpp_rawat_pakai_cabin_temp WHERE cabin_user_creator = '".$_SESSION[SESSION_USERID]."'";
				$result = $this->db->query($sql_empty);

				$sql_empty = "DELETE FROM hpp_rawat_konsumsi_temp WHERE hpp_user_creator = '".$_SESSION[SESSION_USERID]."'";
				$result = $this->db->query($sql_empty);
				/*eos*/
				

				/*setting rawat_id pencarian*/
				$row_rawat_id = "";
				$j = 0;
				foreach($result_perawatan->result() as $rw_rawat_id){
					$row_rawat_id .= $rw_rawat_id->rawat_id;
					if ($j <> $nbrows_perawatan-1){
						$row_rawat_id .= ',';
					}					
					$j++;					
				}
				/*eos*/
				
				/*insert ke tabel hpp_rawat_pakai_cabin_temp*/
				$sql_insert = "INSERT INTO hpp_rawat_pakai_cabin_temp
								SELECT 0 AS alias_cabin_id, dpc1.cabin_dtrawat, dpc1.cabin_rawat, dpc1.cabin_produk, dpc1.cabin_bukti, dpc1.cabin_dapaket_id,
										dpc1.cabin_satuan, dpc1.cabin_jumlah, jr.drawat_jumlah as jumlah_tindakan, dpc1.cabin_date_create, dpc1.cabin_tgl_membuat,
										dpc1.cabin_update, dpc1.cabin_date_update, dpc1.cabin_revised, dpc1.cabin_gudang, dpc1.cabin_cust,
										'".$_SESSION[SESSION_USERID]."' AS cabin_user_creator
								 FROM detail_pakai_cabin dpc1
								 JOIN (select jrawat_nobukti, drawat_jumlah
									   from master_jual_rawat
									   join detail_jual_rawat on drawat_master = jrawat_id
									   where jrawat_tanggal LIKE '$tahun-$bulan%' and jrawat_stat_dok = 'Tertutup'
											 and drawat_rawat in ($row_rawat_id)
									   group by jrawat_nobukti
									   
									   union all
									   
									   select jpaket_nobukti, dapaket_jumlah
									   from detail_ambil_paket
									   join master_jual_paket on jpaket_id = dapaket_jpaket
									   where dapaket_tgl_ambil LIKE '$tahun-$bulan%' and dapaket_stat_dok = 'Tertutup'
											 and dapaket_item in ($row_rawat_id)
									   group by jpaket_nobukti
									  ) jr on jr.jrawat_nobukti = dpc1.cabin_bukti
								WHERE dpc1.cabin_rawat IN ($row_rawat_id)
									  AND dpc1.cabin_date_create LIKE '$tahun-$bulan%'
								ORDER BY dpc1.cabin_id ";
				$result = $this->db->query($sql_insert);
				/*eos*/			
				
				/*insert tabel hpp_rawat_konsumsi_temp yg sesuai dengan std bahan*/
				$sql_insert = "INSERT INTO hpp_rawat_konsumsi_temp
								SELECT 
									0 AS hpp_rawat_id, d.krawat_master, p.produk_id, p.produk_kode, p.produk_nama, d.krawat_jumlah, s.satuan_kode, p.produk_aktif,
									r.rawat_nama, r.rawat_kode,
									IF(d.krawat_alternatif = 'true', 'Ya', 'Tidak') AS bahan_alternatif,
									(IFNULL(hpp_nilai_keluar / hpp_qty_keluar, 0) / sk.konversi_nilai) AS hpp_satuan_terkecil,
									(IFNULL(hpp_nilai_keluar / hpp_qty_keluar, 0) / sk.konversi_nilai) * IFNULL(d.krawat_jumlah, 0) AS sub_total,
									ifnull(SUM(IFNULL(dpc.cabin_jumlah, 0))/d.krawat_jumlah, 0) AS jumlah_tindakan,
									ifnull(SUM(IFNULL(dpc.cabin_jumlah, 0))/d.krawat_jumlah, 0) * d.krawat_jumlah AS jumlah_pemakaian,
									ifnull(SUM(IFNULL(dpc.cabin_jumlah, 0))/d.krawat_jumlah, 0) * (IFNULL(hpp_nilai_keluar / hpp_qty_keluar, 0) / sk.konversi_nilai) * IFNULL(d.krawat_jumlah, 0) AS total,
									'".$_SESSION[SESSION_USERID]."' AS hpp_user_creator
								FROM perawatan_konsumsi d
									LEFT JOIN perawatan r ON r.rawat_id = d.krawat_master
									LEFT JOIN produk p ON p.produk_id = d.krawat_produk
									LEFT JOIN satuan s ON s.satuan_id = d.krawat_satuan
									LEFT JOIN hpp_rawat_pakai_cabin_temp dpc ON dpc.cabin_rawat = d.krawat_master AND dpc.cabin_produk = d.krawat_produk
																		AND dpc.cabin_satuan = d.krawat_satuan
									LEFT JOIN hpp_bulan h ON h.hpp_produk_id = d.krawat_produk AND h.hpp_bulan = $bulan AND h.hpp_tahun = $tahun
									LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = h.hpp_satuan_id AND sk.konversi_produk = d.krawat_produk
								WHERE r.rawat_aktif = 'Aktif' AND dpc.cabin_user_creator = '".$_SESSION[SESSION_USERID]."'
									  AND d.krawat_master in ($row_rawat_id)
								GROUP BY d.krawat_produk, d.krawat_master
								ORDER BY d.krawat_id ";
				$result = $this->db->query($sql_insert);
				/*eos*/
				
				/*insert tabel hpp_rawat_konsumsi_temp yg tidak sesuai dengan std bahan*/
				$sql_rawat_produk = "select krawat_produk from perawatan_konsumsi where krawat_master in ($row_rawat_id) order by krawat_id ";
				$rs_rawat_produk = $this->db->query($sql_rawat_produk);
				$nbrows_rawat_produk = $rs_rawat_produk->num_rows();
				
				$query_produk_id = "";
				if($nbrows_rawat_produk>0){
					$j = 0;
					$row_produk_id = "";
					foreach($rs_rawat_produk->result() as $rw_produk_id){
						$row_produk_id .= $rw_produk_id->krawat_produk;
						if ($j <> $nbrows_rawat_produk-1){
							$row_produk_id .= ',';
						}					
						$j++;					
					}
					$query_produk_id = " AND dpc.cabin_produk not in ($row_produk_id) ";
				}
				
				$sql_insert = "INSERT INTO hpp_rawat_konsumsi_temp
								SELECT 
									0 AS hpp_rawat_id, dpc.cabin_rawat, p.produk_id, p.produk_kode, p.produk_nama, dpc.cabin_jumlah, s.satuan_kode, p.produk_aktif,
									r.rawat_nama, r.rawat_kode, '-' AS bahan_alternatif,
									(IFNULL(hpp_nilai_keluar / hpp_qty_keluar, 0) / sk.konversi_nilai) AS hpp_satuan_terkecil,
									(IFNULL(hpp_nilai_keluar / hpp_qty_keluar, 0) / sk.konversi_nilai) * IFNULL(dpc.cabin_jumlah, 0) AS sub_total,
									ifnull(SUM(IFNULL(dpc.cabin_jumlah, 0))/dpc.cabin_jumlah, 0) AS jumlah_tindakan,
									SUM(IFNULL(dpc.cabin_jumlah, 0)) AS jumlah_pemakaian,
									(IFNULL(hpp_nilai_keluar / hpp_qty_keluar, 0) / sk.konversi_nilai) *  ifnull(SUM(IFNULL(dpc.cabin_jumlah, 0))/dpc.cabin_jumlah, 0) AS total,
									'".$_SESSION[SESSION_USERID]."' AS hpp_user_creator
								FROM hpp_rawat_pakai_cabin_temp dpc
									LEFT JOIN perawatan r ON r.rawat_id = dpc.cabin_rawat
									LEFT JOIN produk p ON p.produk_id = dpc.cabin_produk
									LEFT JOIN satuan s ON s.satuan_id = dpc.cabin_satuan
									LEFT JOIN hpp_bulan h ON h.hpp_produk_id = dpc.cabin_produk AND h.hpp_bulan = $bulan AND h.hpp_tahun = $tahun									
									LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = dpc.cabin_satuan AND sk.konversi_produk = dpc.cabin_produk
								WHERE r.rawat_aktif = 'Aktif' AND dpc.cabin_user_creator = '".$_SESSION[SESSION_USERID]."'
									  AND dpc.cabin_rawat in ($row_rawat_id)
									  $query_produk_id
								GROUP BY dpc.cabin_produk, dpc.cabin_rawat
								ORDER BY dpc.cabin_id";
				$result = $this->db->query($sql_insert);						
				/*eos*/
				
				
				foreach($result_perawatan->result() as $row_perawatan){			
					//total tindakan
					$sql_jml_tindakan =
						   "SELECT
							(
								SELECT IFNULL(SUM(d.drawat_jumlah), 0)
								FROM detail_jual_rawat d
								LEFT JOIN master_jual_rawat m ON m.jrawat_id = d.drawat_master
								WHERE 
									date_format(m.jrawat_tanggal, '%Y-%m') = '$tahun-$bulan'
									AND d.drawat_rawat = $row_perawatan->rawat_id
									AND m.jrawat_stat_dok = 'Tertutup'
							)		
								+
							(	
								SELECT IFNULL(SUM(d2.dapaket_jumlah), 0)
								FROM detail_ambil_paket d2
								WHERE
									date_format(d2.dapaket_tgl_ambil, '%Y-%m') = '$tahun-$bulan'
									AND d2.dapaket_item = $row_perawatan->rawat_id
									AND d2.dapaket_stat_dok = 'Tertutup'
							)
							AS jumlah_tindakan";

					$result_jml_tindakan	= $this->db->query($sql_jml_tindakan);
					$row_jml_tindakan		= $result_jml_tindakan->row();
					//eos
					
					
					//total HPP sesuai std bahan utama & HPP rata-rata
					$sql_tot_hpp_utama = "select krawat_master, format(sum(sub_total), 2) as hpp_total_utama
										FROM hpp_rawat_konsumsi_temp
										WHERE krawat_master = $row_perawatan->rawat_id AND bahan_alternatif = 'Tidak'
											  AND hpp_user_creator = '".$_SESSION[SESSION_USERID]."' ";
					$rs_tot_hpp_utama	= $this->db->query($sql_tot_hpp_utama);
					$rs_tot_hpp_utama	= $rs_tot_hpp_utama->row();
					$hpp_utama = "'".$rs_tot_hpp_utama->hpp_total_utama."'";	//diconvert ke string
					
					$sql_tot_hpp_rata = "select krawat_master, format(ifnull(sum(total)/jumlah_tindakan, 0), 2) as hpp_rata_rata
										FROM hpp_rawat_konsumsi_temp
										WHERE krawat_master = $row_perawatan->rawat_id 
											  AND hpp_user_creator = '".$_SESSION[SESSION_USERID]."' ";
					$rs_tot_hpp_rata	= $this->db->query($sql_tot_hpp_rata);
					$rs_tot_hpp_rata	= $rs_tot_hpp_rata->row();					
					$hpp_rata_rata = "'".$rs_tot_hpp_rata->hpp_rata_rata."'";	//diconvert ke string
					//eos
							

					if($sql_list <> ""){	//semua perawatan
						$sql_list .= "	UNION ALL  ";
					}
					
					$sql_list .= "SELECT CONCAT('(', rawat_kode, ') ', rawat_nama, ', Jml Tindakan: ', $row_jml_tindakan->jumlah_tindakan,
											'|HPP sesuai Standar Bahan Utama (Rp): ', $hpp_utama,
											'|HPP Rata-rata (Rp): ', $hpp_rata_rata
											) AS perawatan_nama,
									krawat_master, produk_id, produk_kode, produk_nama, krawat_jumlah, satuan_kode, produk_aktif,
									bahan_alternatif, hpp_satuan_terkecil, sub_total, jumlah_tindakan, jumlah_pemakaian, total
								FROM hpp_rawat_konsumsi_temp d
								WHERE krawat_master = $row_perawatan->rawat_id AND hpp_user_creator = '".$_SESSION[SESSION_USERID]."' ";		
				}
				
				$sql_list .= " ORDER BY produk_aktif, bahan_alternatif, produk_kode ";
			}
		}	
		
		if($nbrows_perawatan > 0 || $nbrows_perawatan == -1){
			$result = $this->db->query($sql_list);
			$nbrows = $result->num_rows();
		} else {
			$nbrows = $nbrows_perawatan;
		}
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function hpp_list($produk_id, $group1_id, $tahun_cari, $bulan_cari, $bulan, $tahun_akun, $hpp_generate, $jenis, $opsi, $stok_awal, $masuk, $keluar, $stok_akhir){
		if($hpp_generate == 1){
			if($opsi == 'semua'){
				$sql_konversi =    "SELECT konversi_nilai, satuan_id, produk_id, produk_nama
									FROM vu_produk_satuan_default 
									WHERE produk_aktif = 'Aktif' AND produk_kategori=$jenis";		
			}		
			elseif($opsi == 'group1'){					
				$sql_konversi =    "SELECT konversi_nilai, satuan_id, produk_id, produk_nama
									FROM vu_produk_satuan_default 
									WHERE produk_aktif = 'Aktif' AND produk_group = $group1_id";
				if($jenis){
					$sql_konversi.=" AND produk_kategori=$jenis";
				}
			}	
			elseif($opsi == 'produk'){
				$sql_konversi =    "SELECT konversi_nilai, satuan_id, produk_id, produk_nama
									FROM vu_produk_satuan_default 
									WHERE produk_aktif = 'Aktif' AND produk_id = $produk_id";
				if($jenis){
					$sql_konversi.=" AND produk_kategori=$jenis";
				}
			};
			
			/*Generate HPP*/
			$sql_racik = $sql_konversi." AND produk_racikan = 1";
			$sql_racik = "select * from ($sql_racik) rc JOIN (select pracikan_master from produk_racikan group by pracikan_master) pr ON pr.pracikan_master = rc.produk_id";
			$result_racik = $this->db->query($sql_racik);
			$nbrows_produk_racik = $result_racik->num_rows();		
			
			if($nbrows_produk_racik>0){
				$row_produk_racik_id = "";
				$j = 0;
				foreach($result_racik->result() as $rw_produk_racik_id){
					$row_produk_racik_id .= $rw_produk_racik_id->produk_id;
					if ($j <> $nbrows_produk_racik-1){
						$row_produk_racik_id .= ',';
					}					
					$j++;					
				}
			
				// generate std bahan dari produk racikan terlebih dahulu
				$sql_bahan_racikan = "SELECT pracikan_produk AS produk_id FROM produk_racikan
									  WHERE pracikan_master IN ($row_produk_racik_id)";
				$result_bahan_racikan	= $this->db->query($sql_bahan_racikan);
				$nbrows_bahan_racik = $result_bahan_racikan->num_rows();
				
				if($nbrows_bahan_racik>0){
					$row_bahan_racik_id = "";
					$j = 0;
					foreach($result_bahan_racikan->result() as $rw_bahan_racik_id){
						$row_bahan_racik_id .= $rw_bahan_racik_id->produk_id;
						if ($j <> $nbrows_bahan_racik-1){
							$row_bahan_racik_id .= ',';
						}					
						$j++;					
					}
				}

				$sql_bhn_racik_konversi = "SELECT konversi_nilai, satuan_id, produk_id
											FROM vu_produk_satuan_default 
											WHERE produk_aktif = 'Aktif' AND produk_id IN ($row_bahan_racik_id)";
				$rs_bhn_racik_konversi	= $this->db->query($sql_bhn_racik_konversi);
				
				foreach($rs_bhn_racik_konversi->result() as $row_bhn_racik_konv){
					$this->hpp_generate_bulan($row_bhn_racik_konv->produk_id, $row_bhn_racik_konv->satuan_id, $row_bhn_racik_konv->konversi_nilai, $tahun_akun, $bulan);
				}		
				//eos

				
				//generate produk racikan
				foreach($result_racik->result() as $row_racik_konversi){
					//generate nilai terima produk racikan terlebih dahulu
					$sql_hpp_produk =  
					   "SELECT
						   SUM(hpp_nilai_satuan) AS hpp_nilai_satuan
						FROM 
						(
							SELECT 
								hpp_bulan, hpp_tahun, 
								IFNULL(hb.hpp_nilai_keluar / hb.hpp_qty_keluar * pr.pracikan_jumlah /
											 (sk_hpp.konversi_nilai / sk.konversi_nilai)
										, 0) AS hpp_nilai_satuan
							FROM produk_racikan pr 
							LEFT JOIN hpp_bulan hb ON hb.hpp_produk_id = pr.pracikan_produk
							LEFT JOIN produk p ON hb.hpp_produk_id = p.produk_id
							LEFT JOIN satuan s ON pr.pracikan_satuan = s.satuan_id
							LEFT JOIN satuan_konversi sk_hpp ON sk_hpp.konversi_satuan = hb.hpp_satuan_id AND 
															sk_hpp.konversi_produk = pr.pracikan_produk
							LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = pr.pracikan_satuan AND 
															sk.konversi_produk = pr.pracikan_produk														
							WHERE 
								hb.hpp_tahun = $tahun_akun AND hb.hpp_bulan = $bulan AND 
								p.produk_aktif = 'Aktif' AND pr.pracikan_master = $row_racik_konversi->produk_id
						) AS temp
						GROUP BY hpp_bulan, hpp_tahun";
					$rs_hpp_produk	= $this->db->query($sql_hpp_produk)->row();				
					$this->update_nilai_terima_produk_racik($bulan, $tahun_akun, $row_racik_konversi->produk_id, $row_racik_konversi->produk_nama, $rs_hpp_produk->hpp_nilai_satuan);
					//eos
					
					$this->hpp_generate_bulan($row_racik_konversi->produk_id, $row_racik_konversi->satuan_id, $row_racik_konversi->konversi_nilai, $tahun_akun, $bulan);
				}
				/*eos*/			
			}
			
			//generate produk non racikan
			$sql_produk_non_racik = $sql_konversi;
			
			if($nbrows_produk_racik>0){		//yang selain std bahan dari produk racikan
				$sql_produk_non_racik .= " AND produk_id NOT IN ($row_bahan_racik_id)";
			}
			$result	= $this->db->query($sql_produk_non_racik);
						
			foreach($result->result() as $row_konversi){
				$this->hpp_generate_bulan($row_konversi->produk_id, $row_konversi->satuan_id, $row_konversi->konversi_nilai, $tahun_akun, $bulan);
			}
			//eos
		}

		$query =   "SELECT
						produk_id, concat('(', produk_kode, ') ', produk_nama, ' | Satuan: ', satuan_nama) as produk_kode_nama,
						produk_kode, produk_nama, satuan_nama,
						case hpp_bulan
							when 1 then 'Jan'
							when 2 then 'Peb'
							when 3 then 'Mar'
							when 4 then 'Apr'
							when 5 then 'Mei'
							when 6 then 'Jun'
							when 7 then 'Jul'
							when 8 then 'Agu'
							when 9 then 'Sep'
							when 10 then 'Okt'
							when 11 then 'Nov'
							when 12 then 'Des'
						end as hpp_bulan2,
						hpp_qty_awal, hpp_nilai_awal,
						hpp_qty_akhir, hpp_nilai_akhir,
						hpp_qty_masuk - hpp_qty_retur AS hpp_qty_masuk, hpp_nilai_masuk - hpp_nilai_retur AS hpp_nilai_masuk,
						hpp_qty_retur, hpp_nilai_retur,
						hpp_qty_keluar,	hpp_nilai_keluar,
						if(hpp_nilai_awal = hpp_nilai_akhir and hpp_qty_awal = hpp_qty_akhir, 
							ifnull(hpp_nilai_awal / hpp_qty_awal, 0), 
							ifnull(hpp_nilai_keluar / hpp_qty_keluar, 0)
						) as hpp_nilai_keluar_satuan
					FROM hpp_bulan
					LEFT JOIN produk on produk_id = hpp_produk_id
					LEFT JOIN produk_group on group_id = produk_group
					LEFT JOIN satuan on satuan_id = hpp_satuan_id
					WHERE 
						#(hpp_qty_masuk > 0 OR hpp_qty_keluar >0)
						#AND
						hpp_tahun = $tahun_cari ";
			
		if($opsi == 'produk' and $produk_id != ''){
			$query .= "AND hpp_produk_id = '$produk_id' ";
			if($jenis){
				$query .= "AND produk_kategori= '$jenis' ";
			}
		}
		
		if($opsi == 'group1' and $group1_id != '') {
			$query .= "AND group_id = '$group1_id' ";
			if($jenis){
				$query .= "AND produk_kategori= '$jenis' ";
			}
		} else
			if($jenis){
				$query .= "AND produk_kategori= '$jenis' ";
			}

		if($bulan_cari != ''){
			$query .= "AND hpp_bulan = '$bulan_cari' ";
		}
	
		//Filter Stok Awal
		if($stok_awal=='='){
			$query.="	and hpp_qty_awal = '0' ";
		}elseif($stok_awal=='<'){
			$query.="	and hpp_qty_awal < '0' ";
		}elseif($stok_awal=='> 0'){
			$query.="	and hpp_qty_awal > '0' ";
		}
		
		//Filter Stok Masuk
		if($masuk=='S'){
			$query.=" and ((hpp_qty_masuk - hpp_qty_retur) like '%' ";
		}elseif($masuk=='='){
			$query.=" and ((hpp_qty_masuk - hpp_qty_retur) = '0' ";
		}elseif($masuk=='<'){
			$query.=" and ((hpp_qty_masuk - hpp_qty_retur) < '0' ";
		}elseif($masuk=='> 0'){
			$query.=" and ((hpp_qty_masuk - hpp_qty_retur) > '0' ";
		}
		
		//Filter Stok Keluar
		if($keluar=='S'){
			$query.=" or hpp_qty_keluar like '%' )";
		}elseif($keluar=='='){
			$query.=" or hpp_qty_keluar = '0' )";
		}elseif($keluar=='<'){
			$query.=" or hpp_qty_keluar < '0' )";
		}elseif($keluar=='> 0'){
			$query.=" or hpp_qty_keluar > '0' )";
		}
		
		//Filter Stok Akhir
		if($stok_akhir=='='){
			$query.=" and hpp_qty_akhir = '0' ";
		}elseif($stok_akhir=='<'){
			$query.=" and hpp_qty_akhir < '0' ";
		}elseif($stok_akhir=='> 0'){
			$query.=" and hpp_qty_akhir > '0' ";
		}elseif($stok_akhir=='!='){
			$query.=" and hpp_qty_akhir <> '0' ";
		}

		$query .= "ORDER BY hpp_bulan, produk_id ";
		$_SESSION[$_SESSION['dbname']]['sql_hpp'] = $query;		//disimpan ke session untuk kebutuhan print & export2excel
		
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		/*$limit = $query." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit);  
		*/
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
		
	function hpp_generate_bulan($produk_id, $satuan_id, $konversi_nilai, $tahun, $bulan_mulai, $check_generate=false){
		
		$tahun_ini	= date('Y');
		$bulan_ini	= date('m');						
		
		$bulan[1] = '01'; $bulan[2] = '02'; $bulan[3] = '03'; $bulan[4] = '04'; $bulan[5] = '05'; $bulan[6] = '06';
		$bulan[7] = '07'; $bulan[8] = '08'; $bulan[9] = '09'; $bulan[10] = '10'; $bulan[11] = '11'; $bulan[12] = '12';
		
		$i_start = (int)$bulan_mulai;

		if($check_generate == true){
			$t_arr = array();			
			$t_start = $i_start;

			for($i=1;$i<=$t_start;$i++){
				$t_arr[$i] = 0;
			}

			$sql_check = "select * from hpp_bulan where hpp_tahun = '$tahun_ini' and hpp_produk_id = '$produk_id' and hpp_satuan_id = '$satuan_id' order by hpp_bulan asc";
			$data_check = $this->db->query($sql_check)->result_array();

			foreach($data_check as $row){
				$row['hpp_bulan'] = (int)$row['hpp_bulan'];	
				$t_arr[$row['hpp_bulan']] = $row['hpp_id'];
			}

			for($i=1;$i<=$t_start;$i++){
				if($t_arr[$i] == 0){
					$i_start = $bulan_mulai = $i;
					break;
				} 
			}
		}		
		
		//tahun yang sama -> generate dilakukan dari bulan terpilih s/d bulan ini
		if ($tahun == $tahun_ini)
			$i_end = (int)$bulan_ini;		
		
		//tahun sebelumnya -> generate dilakukan dari bulan terpilih s/d bulan 12
		elseif ($tahun < $tahun_ini)			
			$i_end = 12;		

		$sql_del = "DELETE FROM hpp_bulan
					WHERE 
						hpp_produk_id = $produk_id AND hpp_tahun = $tahun AND hpp_bulan >= $bulan_mulai AND hpp_bulan <= $i_end";
		$this->db->query($sql_del);
		
		for($i = $i_start; $i <= $i_end; $i++){
			
			//inisialisasi variable tanggal
			$tgl_awal	= "'".$tahun.'-'.$bulan[$i].'-01'."'";

			$last_day 	= cal_days_in_month(CAL_GREGORIAN, $bulan[$i], $tahun);
			$tgl_akhir	= "'".$tahun.'-'.$bulan[$i].'-'.$last_day."'";

			//inisialisasi tgl 1 hari sebelumnya
			if($bulan[$i]=='01'){
				$bulan_sebelum = '12';
				$tahun_sebelum = $tahun - 1;
				$last_day_sebelum 	= cal_days_in_month(CAL_GREGORIAN, $bulan_sebelum, $tahun_sebelum);
			}
			else{
				$bulan_sebelum = $bulan[$i - 1];
				$tahun_sebelum = $tahun;
				$last_day_sebelum 	= cal_days_in_month(CAL_GREGORIAN, $bulan_sebelum, $tahun_sebelum);
			}
			$tgl_akhir_bulan_sebelum	= "'".$tahun_sebelum.'-'.$bulan_sebelum.'-'.$last_day_sebelum."'";

			$qty_awal		= 0; $nilai_awal	= 0;
			$qty_masuk		= 0; $nilai_masuk	= 0; 
			$qty_retur		= 0; $nilai_retur	= 0;
			$qty_keluar		= 0; $nilai_keluar	= 0;
			$qty_akhir		= 0; $nilai_akhir	= 0;

			$qty_awal		= $this->get_qty_awal($tgl_awal, $produk_id, $konversi_nilai); 
			$nilai_awal		= $this->get_nilai_awal($tgl_awal, $bulan_sebelum, $tahun_sebelum, $produk_id);
			$qty_masuk		= $this->get_qty_masuk($tgl_awal, $tgl_akhir, $produk_id, $konversi_nilai, $qty_retur); //qty_retur tidak termasuk
			$nilai_masuk	= $this->get_nilai_masuk($tgl_awal, $tgl_akhir, $produk_id, $konversi_nilai);	//rp_retur tidak termasuk, karena rp retur mengikuti bulan berjalan (dihitung terpisah di bawah)			
			$qty_keluar		= $this->get_qty_keluar($tgl_awal, $tgl_akhir, $produk_id, $konversi_nilai);
			
			if (($qty_awal + $qty_masuk) == 0)
				$nilai_keluar = 0;
			else{
				$nilai_keluar	= ($nilai_awal + $nilai_masuk) / ($qty_awal + $qty_masuk) * $qty_keluar;
				//print_r($nilai_awal.','.$nilai_masuk.','.$qty_awal.','.$qty_masuk.','.$qty_keluar);
				$nilai_masuk	= $nilai_masuk + $nilai_retur;
			}
		
			$qty_akhir		= $qty_awal + $qty_masuk - $qty_keluar;
			$nilai_akhir	= $nilai_awal + $nilai_masuk - $nilai_keluar;

			$data_hpp = array(
				"hpp_bulan"=>$bulan[$i],
				"hpp_tahun"=>$tahun,
				"hpp_produk_id"=>$produk_id,
				"hpp_satuan_id"=>$satuan_id,
				"hpp_qty_awal"=>$qty_awal,
				"hpp_nilai_awal"=>$nilai_awal,
				"hpp_qty_masuk"=>$qty_masuk,
				"hpp_nilai_masuk"=>$nilai_masuk,
				"hpp_qty_retur"=>$qty_retur,
				"hpp_nilai_retur"=>$nilai_retur,
				"hpp_qty_keluar"=>$qty_keluar,
				"hpp_nilai_keluar"=>$nilai_keluar,
				"hpp_qty_akhir"=>$qty_akhir,
				"hpp_nilai_akhir"=>$nilai_akhir					
			);
			$this->db->insert('hpp_bulan', $data_hpp);		

			if($qty_retur > 0){
				$sql = "select * from hpp_bulan where hpp_bulan = '".$bulan[$i]."' and hpp_tahun = '".$tahun."' and hpp_produk_id = '".$produk_id."' and hpp_satuan_id = '".$satuan_id."'";
				$t_data = $this->db->query($sql)->row_array();

				$qty_masuk += $qty_retur;
				if (($qty_awal + $qty_masuk - $qty_retur) == 0)
					$nilai_keluar = 0;
				else{
					$nilai_keluar	= ($nilai_awal + $nilai_masuk) / ($qty_awal + $qty_masuk - $qty_retur) * $qty_keluar;
					$nilai_retur	= $this->get_nilai_retur($tgl_awal, $tgl_akhir, $produk_id, $konversi_nilai);
					$nilai_masuk	= $nilai_masuk + $nilai_retur;
				}
			
				$qty_akhir		= $qty_awal + $qty_masuk - $qty_keluar;
				$nilai_akhir	= $nilai_awal + $nilai_masuk - $nilai_keluar;

				$data_hpp = array(
					"hpp_bulan"=>$bulan[$i],
					"hpp_tahun"=>$tahun,
					"hpp_produk_id"=>$produk_id,
					"hpp_satuan_id"=>$satuan_id,
					"hpp_qty_awal"=>$qty_awal,
					"hpp_nilai_awal"=>$nilai_awal,
					"hpp_qty_masuk"=>$qty_masuk,
					"hpp_nilai_masuk"=>$nilai_masuk,
					"hpp_qty_retur"=>$qty_retur,
					"hpp_nilai_retur"=>$nilai_retur,
					"hpp_qty_keluar"=>$qty_keluar,
					"hpp_nilai_keluar"=>$nilai_keluar,
					"hpp_qty_akhir"=>$qty_akhir,
					"hpp_nilai_akhir"=>$nilai_akhir					
				);
				$this->db->where('hpp_bulan' ,$bulan[$i]);
				$this->db->where('hpp_tahun' ,$tahun);
				$this->db->where('hpp_produk_id' ,$produk_id);
				$this->db->where('hpp_satuan_id' ,$satuan_id);
				$this->db->update('hpp_bulan', $data_hpp);
			}
		}														
	}
	
	function hpp_generate($produk_id, $satuan_id, $konversi_nilai, $tahun){
		
		$sql_del = "DELETE FROM hpp_bulan
					WHERE 
						hpp_produk_id = $produk_id AND hpp_tahun = $tahun";
		$this->db->query($sql_del);
		
		$bulan[1] = '01'; $bulan[2] = '02'; $bulan[3] = '03'; $bulan[4] = '04'; $bulan[5] = '05'; $bulan[6] = '06';
		$bulan[7] = '07'; $bulan[8] = '08'; $bulan[9] = '09'; $bulan[10] = '10'; $bulan[11] = '11'; $bulan[12] = '12';
		
		$time_start = microtime(true);
		
		for($i = 1; $i <= 12; $i++){
			
			//inisialisasi variable tanggal
			$tgl_awal	= "'".$tahun.'-'.$bulan[$i].'-01'."'";
			
			$last_day 	= cal_days_in_month(CAL_GREGORIAN, $bulan[$i], $tahun);
			$tgl_akhir	= "'".$tahun.'-'.$bulan[$i].'-'.$last_day."'";
			
			//inisialisasi tgl 1 hari sebelumnya
			if($bulan[$i]=='01'){
				$bulan_sebelum = '12';
				$tahun_sebelum = $tahun - 1;
				$last_day_sebelum 	= cal_days_in_month(CAL_GREGORIAN, $bulan_sebelum, $tahun_sebelum);
			}
			else{
				$bulan_sebelum = $bulan[$i - 1];
				$tahun_sebelum = $tahun;
				$last_day_sebelum 	= cal_days_in_month(CAL_GREGORIAN, $bulan_sebelum, $tahun_sebelum);
			}
			$tgl_akhir_bulan_sebelum	= "'".$tahun_sebelum.'-'.$bulan_sebelum.'-'.$last_day_sebelum."'";
					
			$qty_awal		= 0;
			$nilai_awal		= 0;
			$qty_masuk		= 0;
			$qty_retur		= 0;
			$nilai_masuk	= 0;
			$qty_keluar		= 0;
			$nilai_keluar	= 0;
			$qty_akhir		= 0;
			$nilai_akhir	= 0;
			
			$qty_awal		= $this->get_qty_awal($tgl_awal, $produk_id, $konversi_nilai); 
			$nilai_awal		= $this->get_nilai_awal($tgl_awal, $bulan_sebelum, $tahun_sebelum, $produk_id);
			$qty_masuk		= $this->get_qty_masuk($tgl_awal, $tgl_akhir, $produk_id, $konversi_nilai, $qty_retur);
			$nilai_masuk	= $this->get_nilai_masuk($tgl_awal, $tgl_akhir, $produk_id, $konversi_nilai);				
			$qty_keluar		= $this->get_qty_keluar($tgl_awal, $tgl_akhir, $produk_id, $konversi_nilai);
			
			if($qty_awal == 0) 
				$nilai_keluar = 0;
			else
				//$nilai_keluar	= $nilai_awal / $qty_awal * $qty_keluar;
				$nilai_keluar	= ($nilai_awal + $nilai_masuk) / ($qty_awal + $qty_masuk) * $qty_keluar;
			
			$qty_akhir		= $qty_awal + $qty_masuk - $qty_keluar;
			$nilai_akhir	= $nilai_awal + $nilai_masuk - $nilai_keluar;
							
			$data_hpp = array(
				"hpp_bulan"=>$bulan[$i],
				"hpp_tahun"=>$tahun,
				"hpp_produk_id"=>$produk_id,
				"hpp_satuan_id"=>$satuan_id,
				"hpp_qty_awal"=>$qty_awal,
				"hpp_nilai_awal"=>$nilai_awal,
				"hpp_qty_masuk"=>$qty_masuk,
				"hpp_nilai_masuk"=>$nilai_masuk,
				"hpp_qty_keluar"=>$qty_keluar,
				"hpp_nilai_keluar"=>$nilai_keluar,
				"hpp_qty_akhir"=>$qty_akhir,
				"hpp_nilai_akhir"=>$nilai_akhir					
			);
			$this->db->insert('hpp_bulan', $data_hpp); 										
		
/*			$time_end	= microtime(true);
			$time		= $time_end - $time_start;
			print_r($i.' : '.$time.'
					');		
			
			$time_start	= $time_end;
*/		
		}										
	}
	
	function update_nilai_terima_produk_racik($bulan, $tahun, $produk_id, $produk_nama, $nilai_satuan){
		$bulan = str_pad($bulan, 2, "0", STR_PAD_LEFT);

		//inisialisasi variable tanggal
		$tgl_awal	= "$tahun-$bulan-01";
		$last_day 	= cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
		$tgl_akhir	= "$tahun-$bulan-$last_day";
		
		//apakah data boleh diedit
		$sql_editable = 
		   "SELECT tnt_id, tnt_rp FROM temp_nilai_terima_racik
			WHERE 
				tnt_produk = $produk_id AND DATE_FORMAT(tnt_tgl_awal, '%Y-%m') = '$tahun-$bulan' AND tnt_editable = 'false'";
				
		$result = $this->db->query($sql_editable);
		$row_tnt = $result->row();
		
		if($this->db->affected_rows()>0)
			//jika ditemukan data yang tidak boleh diedit, maka ganti $nilai_satuan dengan nilai yang ditemukan
			$nilai_satuan = $row_tnt->tnt_rp;					
		else{
			//jika tidak ditemukan, maka lakukan:
			
			//Hapus semua produk dalam bulan dan tahun terpilih
			$sql_del = "DELETE FROM temp_nilai_terima_racik
						WHERE 
							tnt_produk = $produk_id 
							AND DATE_FORMAT(tnt_tgl_awal, '%Y-%m') = '$tahun-$bulan'";
			$this->db->query($sql_del);
					
			//Inputkan data harga satuan ke tabel "temp_nilai_terima_racik"
			$data_tnt = array(
				"tnt_tgl_awal"=>$tgl_awal,
				"tnt_tgl_akhir"=>$tgl_akhir,
				"tnt_produk"=>$produk_id,
				"tnt_ket"=>$produk_nama,
				"tnt_rp"=>$nilai_satuan,
				"tnt_editable"=>'true'
			);
			$this->db->insert('temp_nilai_terima_racik', $data_tnt);
		}
			
		//Update harga masuk di kartu_stok_fix
		$sql_update = 
		   "UPDATE kartu_stok_fix ks
			LEFT JOIN satuan_konversi sk_hpp ON sk_hpp.konversi_default = 'true' AND sk_hpp.konversi_produk = $produk_id
			LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = ks.ks_satuan_id AND sk.konversi_produk = ks.ks_produk_id
			SET 
				ks.ks_masuk_rp = ks.ks_masuk * $nilai_satuan / (sk_hpp.konversi_nilai / sk.konversi_nilai)
			WHERE 
				ks.ks_produk_id = $produk_id AND ks.ks_masuk <> 0 AND ks.ks_jenis <> 'koreksi_stok' #koreksi stok masuk nilainya selalu 0
				AND (ks.ks_tgl_faktur BETWEEN '$tgl_awal' AND '$tgl_akhir')
				AND ks.ks_jenis <> 'terima_beli'";
		$result = $this->db->query($sql_update);
						
	}

	function hpp_print($produk_id, $group1_id, $tahun_cari, $bulan_mulai, $tahun_akun, $jenis, $opsi){
		$sql = $_SESSION[$_SESSION['dbname']]['sql_hpp'];
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();  

		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			return $arr;
		} else {
			return NULL;
		}
	}

	function detail_faktur($drawat_rawat, $produk_id, $bulan, $tahun){
		//ambil no bukti berdasarkan produk_id
		$sql_nobuk = "SELECT cabin_bukti FROM hpp_rawat_pakai_cabin_temp
					  WHERE cabin_rawat = $drawat_rawat AND cabin_produk = $produk_id AND cabin_user_creator = '".$_SESSION[SESSION_USERID]."' ";
		$rs_nobuk = $this->db->query($sql_nobuk);
		$nbrows = $rs_nobuk->num_rows();

		if($nbrows > 0){
			$row_nobuk = "";
			$j = 0;
			foreach($rs_nobuk->result() as $rw_nobuk){
				$row_nobuk .= "'".$rw_nobuk->cabin_bukti."'";
				if ($j <> $nbrows-1){
					$row_nobuk .= ',';
				}					
				$j++;					
			}			
			
			$sql = "SELECT 'Detail' AS rawat_detail, dpc.cabin_rawat, date_format(dpc.cabin_date_create, '%Y-%m-%d') AS hpp_rawat_dfaktur_tanggal,
							dpc.cabin_bukti AS hpp_rawat_dfaktur_nofaktur, mjp.rawat_jumlah AS hpp_rawat_dfaktur_jumlah,
							SUM((IFNULL(hpp_nilai_keluar / hpp_qty_keluar, 0) / sk.konversi_nilai) * IFNULL(dpc.cabin_jumlah, 0)) AS hpp_rawat_dfaktur_hpp,
							CONCAT('(', c.cust_no, ') ', c.cust_nama) AS cust
					FROM hpp_rawat_pakai_cabin_temp dpc
					JOIN (
							select 
								mjr.jrawat_nobukti, mjr.jrawat_tanggal, sum(ifnull(djr.drawat_jumlah,0)) as rawat_jumlah,
								mjr.jrawat_cust AS cust_id
							from master_jual_rawat mjr
							left join detail_jual_rawat djr on djr.drawat_master = mjr.jrawat_id
							where date_format(mjr.jrawat_tanggal, '%Y-%m') = '$tahun-$bulan' and mjr.jrawat_stat_dok = 'Tertutup'
								  and djr.drawat_rawat = $drawat_rawat
								  and mjr.jrawat_nobukti in ($row_nobuk)
							group by mjr.jrawat_nobukti, mjr.jrawat_tanggal
							union all
							select 
								jp.jpaket_nobukti, dap.dapaket_tgl_ambil, sum(ifnull(dap.dapaket_jumlah,0)) as rawat_jumlah,
								dap.dapaket_cust AS cust_id
							from detail_ambil_paket dap
							left join master_jual_paket jp on dap.dapaket_jpaket = jp.jpaket_id
							where date_format(dap.dapaket_tgl_ambil, '%Y-%m') = '$tahun-$bulan' and dapaket_stat_dok = 'Tertutup'
								  and dap.dapaket_item = $drawat_rawat
								  and jp.jpaket_nobukti in ($row_nobuk)
							group by jp.jpaket_nobukti, dap.dapaket_tgl_ambil
						) mjp ON mjp.jrawat_nobukti = dpc.cabin_bukti AND mjp.jrawat_tanggal = date_format(dpc.cabin_date_create, '%Y-%m-%d')
					LEFT JOIN customer c ON c.cust_id = mjp.cust_id
					LEFT JOIN hpp_bulan h ON h.hpp_produk_id = dpc.cabin_produk AND h.hpp_bulan = $bulan AND h.hpp_tahun = $tahun
					LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = h.hpp_satuan_id AND sk.konversi_produk = dpc.cabin_produk
					WHERE dpc.cabin_rawat = $drawat_rawat AND dpc.cabin_user_creator = '".$_SESSION[SESSION_USERID]."'
					GROUP BY dpc.cabin_rawat, dpc.cabin_bukti, dpc.cabin_date_create
					ORDER BY dpc.cabin_date_create";

			$result = $this->db->query($sql);
			$nbrows = $result->num_rows();
		}
	
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function detail_rawat($cabin_bukti, $cabin_rawat, $cabin_tanggal){	
		$sql = "SELECT dpc.cabin_bukti, p.produk_kode AS hpp_rawat_drawat_kode, p.produk_nama AS hpp_rawat_drawat_produk,
				SUM(IFNULL(dpc.cabin_jumlah, 0)) AS hpp_rawat_drawat_jumlah_pakai,
				SUM(IFNULL(dpc.cabin_jumlah, 0)) * (IFNULL(h.hpp_nilai_keluar / h.hpp_qty_keluar, 0) / sk.konversi_nilai) AS hpp_rawat_drawat_total
				FROM hpp_rawat_pakai_cabin_temp dpc
				LEFT JOIN produk p ON p.produk_id = dpc.cabin_produk
				LEFT JOIN hpp_bulan h ON h.hpp_produk_id = dpc.cabin_produk
										 AND h.hpp_bulan = date_format(dpc.cabin_date_create, '%m') AND h.hpp_tahun = date_format(dpc.cabin_date_create, '%Y')
				LEFT JOIN satuan_konversi sk ON sk.konversi_satuan = h.hpp_satuan_id AND sk.konversi_produk = dpc.cabin_produk				
				WHERE dpc.cabin_bukti = '$cabin_bukti' AND dpc.cabin_rawat = $cabin_rawat
					  AND date_format(dpc.cabin_date_create, '%Y-%m-%d') = '$cabin_tanggal' AND dpc.cabin_user_creator = '".$_SESSION[SESSION_USERID]."'
				GROUP BY dpc.cabin_produk, dpc.cabin_satuan
				ORDER BY dpc.cabin_produk";

		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();

		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}

	function get_group1_perawatan_list($query,$start,$end){
		$sql="SELECT group_id,group_nama,group_kode,
				group_duproduk,group_dmproduk,group_dm2produk,group_dultah, group_dcard, group_dkolega, group_dkeluarga, group_downer, group_dgrooming,group_dwartawan, group_dstaffdokter, group_dstaffnondokter,
				group_durawat,group_dmrawat,group_dm2rawat,group_dupaket,group_dmpaket,group_dm2paket, kategori_nama,kategori_id 
				FROM produk_group,kategori 
				WHERE group_kelompok=kategori_id AND kategori_jenis='perawatan' AND group_aktif='Aktif' AND kategori_aktif='Aktif'";
		
		if($query<>""){
			$sql .=eregi("WHERE",$sql)? " AND ":" WHERE ";
			$sql .= " (group_nama LIKE '%".addslashes($query)."%' OR group_kode LIKE '%".addslashes($query)."%')";
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}	
	
}
?>