<?php 

class M_main extends Model{

	function M_main(){
		parent::Model();
	}

	function get_menus(){
		if($_SESSION[SESSION_USERID]==='Super Admin')
			$sql="SELECT * FROM menus P 
					WHERE P.menu_parent=0 
					ORDER BY P.menu_position ASC";
		else
			$sql="SELECT * FROM menus P 
					WHERE P.menu_parent=0 
					    AND P.menu_id IN(SELECT C.menu_parent FROM vu_menus C
									 WHERE C.menu_parent=P.menu_id
									 AND C.group_id='".$_SESSION[SESSION_GROUPID]."') 
					    AND SUBSTRING(menu_aktif_cabang, (SELECT CASE cabang_value WHEN 100 THEN 30 WHEN 101 THEN 31 WHEN 103 THEN 33 ELSE cabang_value END AS cabang_value FROM cabang c INNER JOIN info i ON c.cabang_id = i.info_cabang), 1) = 1
					ORDER BY P.menu_position ASC";
	
		$query=$this->db->query($sql);
		$rs=$query->result_array();
		return $rs;
	}
	
	
	function get_background(){
		$background="";
		$sql="select info_background from info";
		$query=$this->db->query($sql);
		if($query->num_rows()){
			$rs=$query->row();
			$background=$rs->info_background;
		}
		return $background;
	}	
	
	
	function get_sub_menus(){
		if($_SESSION[SESSION_USERID]==='Super Admin')
			$sql="SELECT * FROM menus WHERE menu_parent<>0 ORDER BY menu_parent, menu_position ASC";
		else
			$sql="SELECT * FROM vu_menus 
			    WHERE group_id='".$_SESSION[SESSION_GROUPID]."' AND menu_parent<>0
				AND SUBSTRING(menu_aktif_cabang, (SELECT CASE cabang_value WHEN 100 THEN 30 WHEN 101 THEN 31 WHEN 103 THEN 33 ELSE cabang_value END AS cabang_value FROM cabang c INNER JOIN info i ON c.cabang_id = i.info_cabang), 1) = 1
			    ORDER BY menu_parent, menu_position ASC ";
		
		$query=$this->db->query($sql);
		$rs=$query->result_array();
		return $rs;
	}
	
	function get_shortcuts(){
		if($_SESSION[SESSION_USERID]==='Super Admin')
			$sql="SELECT * FROM menus WHERE menu_parent<>0 AND menu_leftpanel='Y' ORDER BY menu_parent, menu_position ASC";
		else
			$sql="SELECT * FROM vu_menus 
			    WHERE menu_parent<>0 AND group_id='".$_SESSION[SESSION_GROUPID]."' AND menu_leftpanel='Y' 
				AND SUBSTRING(menu_aktif_cabang, (SELECT CASE cabang_value WHEN 100 THEN 30 WHEN 101 THEN 31 WHEN 103 THEN 33 ELSE cabang_value END AS cabang_value FROM cabang c INNER JOIN info i ON c.cabang_id = i.info_cabang), 1) = 1
			    ORDER BY menu_parent, menu_position ASC";
		
		$query=$this->db->query($sql);
		$rs=$query->result_array();
		return $rs;
	}
	
	
	function get_db_name(){
		$this->cabang = $this->load->database("default", TRUE);
		return $this->cabang->database;
	}
	
}
?>