<? 
class M_master_jual_produk extends Model{

	function cek_sisa_stok($produk_id){
		$sql="SELECT 
				ifnull((p.produk_saldo_awal2),0)+
				ifnull(sum((ks.ks_masuk*sk.konversi_nilai/p.konversi_nilai)),0)-
				ifnull(sum((ks.ks_keluar*sk.konversi_nilai/p.konversi_nilai)),0) AS stok_akhir, 
				p.produk_stok_min
			FROM vu_produk_satuan_default p
			LEFT JOIN kartu_stok_fix ks ON (ks.ks_produk_id = p.produk_id AND ks.ks_tgl_faktur >= p.produk_tgl_nilai_saldo_awal AND ks.ks_gudang_id = '2')
			LEFT JOIN satuan_konversi sk ON (ks.ks_produk_id = sk.konversi_produk and ks.ks_satuan_id = sk.konversi_satuan)
			WHERE p.produk_id='".$produk_id."'";		
		

		$result = $this->db->query($sql);		
		$data=$result->row();
		$stok_akhir=(int)$data->stok_akhir;
		$produk_stok_min=(int)$data->produk_stok_min;
		
		$stok_real=$stok_akhir-$produk_stok_min;
		return '({"stok_akhir":'.$stok_akhir.',"stok_min":'.$produk_stok_min.'})';		
	}
	
	function card_edc_jenis_list($filter,$start,$end){
		$query =   "SELECT edc_jenis_id as produk_edc_jenis_id, edc_jenis_nama as produk_edc_jenis_nama FROM card_edc_jenis where edc_jenis_aktif='Aktif' ";
		
		if ($filter<>"" && $query !=""){
			$query .=eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " (edc_jenis_nama LIKE '%".addslashes($filter)."%' )";
			$limit = $query;
		}
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		if($end=="") $end=15;
		$limit = $query." LIMIT ".$start.",".$end;
		$result = $this->db->query($limit);
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}

		function card_edc_list($filter,$start,$end){
		$query =   "SELECT edc_id as produk_edc_id, edc_nama as produk_edc_nama FROM card_edc where edc_aktif='Aktif' ";
		
		
		if ($filter<>"" && $query !=""){
			$query .=eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " (edc_nama LIKE '%".addslashes($filter)."%' )";
			$limit = $query;
		}
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		if($end=="") $end=15;
		$limit = $query." LIMIT ".$start.",".$end;
		$result = $this->db->query($limit);
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function card_promo_list($filter,$start,$end){
		$query =   "SELECT promo_id as produk_promo_id, promo_nama as produk_promo_nama FROM card_edc_promo where promo_aktif='Aktif' ";
		
		
		if ($filter<>"" && $query !=""){
			$query .=eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " (promo_nama LIKE '%".addslashes($filter)."%' )";
			$limit = $query;
		}
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		if($end=="") $end=15;
		$limit = $query." LIMIT ".$start.",".$end;
		$result = $this->db->query($limit);
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}	
	
	function get_allkaryawan_list($query,$start,$end){
		$sql="SELECT karyawan_id,karyawan_no,karyawan_username,karyawan_nama,karyawan_tgllahir,karyawan_alamat
		FROM karyawan where karyawan_aktif='Aktif'";
		if($query<>""){
			$sql=$sql." and (karyawan_no like '%".$query."%' or karyawan_nama like '%".$query."%') ";
		}
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit);  
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_laporan($tgl_awal,$tgl_akhir,$periode,$opsi,$opsi_status,$group,$biasa,$grooming_nonvoucher,$grooming_voucher,$lainnya,$compliment,$grooming_owner_voucher,$grooming_owner_nonvoucher,$compli_only, $lain){
		
		switch($group){
			case "Tanggal": $order_by=" ORDER BY tanggal ASC, no_bukti ASC";break;
			case "Customer": $order_by=" ORDER BY cust_nama ASC";break;
			case "Karyawan": $order_by=" ORDER BY cust_nama ASC, no_bukti DESC";break;
			case "No Faktur": $order_by=" ORDER BY no_bukti ASC";break;
			case "Produk": $order_by=" ORDER BY group_kode,satuan_nama, tanggal ASC";break;
			case "Referal": $order_by=" ORDER BY sales ASC, produk_group ASC, produk_nama ASC";break;
			case "Jenis Diskon": $order_by=" ORDER BY diskon_jenis";break;
			case "Group 1": $order_by=" ORDER BY group_nama, produk_nama";break;
			case "Tanggal (Format 2 PPN)": $order_by=" ORDER BY tanggal, no_bukti ASC";break;
			default: $order_by=" ORDER BY no_bukti ASC";break;
		}
		
		if($opsi_status=='semua'){
			$status_vu = " vu.jproduk_stat_dok<>'Terbuka' ";
			$status = " jproduk_stat_dok<>'Terbuka' ";
		}else{
			$status_vu = " vu.jproduk_stat_dok='Tertutup' ";
			$status	= " jproduk_stat_dok='Tertutup' ";
		}
		
		if($periode=='bulan'){
			$tgl_vu	 = " date_format(tanggal,'%Y-%m')='".$tgl_awal."' ";
			$tgl	 = " date_format(jproduk_tanggal,'%Y-%m')='".$tgl_awal."' ";
		}else{
			$tgl_vu	 = " date_format(tanggal,'%Y-%m-%d')>='".$tgl_awal."' AND date_format(tanggal,'%Y-%m-%d')<='".$tgl_akhir."' ";
			$tgl	 = " date_format(jproduk_tanggal,'%Y-%m-%d')>='".$tgl_awal."' 
						AND date_format(jproduk_tanggal,'%Y-%m-%d')<='".$tgl_akhir."' ";
		}
		
		if($opsi=='rekap'){		/*rekap hanya bisa Biasa(net sales) karena kalau rekap tdk bs split diskon, pdhl 1 faktur bs macam2 jenis diskon*/
			
			if($periode=='all')
				$sql="SELECT distinct vu_trans_produk.*
						FROM vu_trans_produk 
						WHERE jproduk_stat_dok='Tertutup'";
			else if($periode=='bulan')
				$sql="SELECT distinct vu_trans_produk.*
						FROM vu_trans_produk 
						WHERE jproduk_stat_dok='Tertutup' AND date_format(tanggal,'%Y-%m')='".$tgl_awal."'";
			else if($periode=='tanggal')
				$sql="SELECT distinct vu_trans_produk.*
						FROM vu_trans_produk 
						WHERE jproduk_stat_dok='Tertutup' AND date_format(tanggal,'%Y-%m-%d')>='".$tgl_awal."' AND date_format(tanggal,'%Y-%m-%d')<='".$tgl_akhir."'";
			
			$sql .= $order_by;
			
		}else if($opsi=='detail'){			
				$sql="SELECT vu.produk_id,
					vu.jumlah_barang,
					vu.harga_satuan,
					vu.satuan_kode,
					vu.diskon,
					vu.diskon_jenis,
					vu.sales,
					vu.no_bukti,
					vu.no_bukti_2,
					vu.tanggal,
					vu.keterangan,
					vu.produk_bpom,
					vu.jproduk_grooming,
					vu.diskon_umum,
					vu.voucher,
					vu.no_voucher,
					vu.voucher_medis,
					vu.totalbiaya,
					vu.bayar,
					vu.cust_id,
					vu.cust_member,
					vu.produk_kode,
					vu.produk_kategori,
					vu.produk_nama,
					vu.produk_satuan,
					vu.produk_group,
					vu.produk_jenis,
					vu.satuan_nama,
					vu.diskon_nilai,
					vu.subtotal,
					vu.jproduk_stat_dok,
					vu.status,
					vu.bayar_card,
					vu.bayar_cek,
					vu.bayar_kuitansi,
					vu.bayar_transfer,
					vu.bayar_tunai,
					vu.bayar_gift_voucher,
					vu.bayar_vgrooming,
					vu.kredit,
					vu.time,
					jml, 
					produk_group.group_nama AS group_nama ,
					vu.cust_nama as cust_nama,
					vu.cust_no as cust_no
					FROM vu_detail_jual_produk vu
					left join kategori k on vu.produk_kategori = k.kategori_id
					left join karyawan on (karyawan_id = jproduk_grooming)
					LEFT JOIN produk_group ON produk_group.group_id = vu.produk_group
					LEFT JOIN (SELECT jproduk_nobukti, jproduk_stat_dok, jproduk_tanggal, jproduk_id,count(*) AS jml 
								FROM detail_jual_produk 
								LEFT JOIN master_jual_produk ON dproduk_master=jproduk_id 
								WHERE ".$status." AND ".$tgl." 
								GROUP BY jproduk_nobukti)xp ON vu.no_bukti=xp.jproduk_nobukti
					WHERE ".$status_vu." AND ".$tgl_vu.$opsi_lainnya." ".$order_by;
					
			if($group=="No Faktur BPOM"){
				$sql_bpom = "SELECT no_bukti FROM vu_detail_jual_produk vu LEFT JOIN produk_group ON produk_group.group_id = vu.produk_group WHERE jproduk_stat_dok='Tertutup' AND date_format(tanggal,'%Y-%m-%d')>='".$tgl_awal."' AND date_format(tanggal,'%Y-%m-%d')<='".$tgl_akhir."' AND vu.produk_bpom = 0 ".$order_by;
				
				$sql= "SELECT data.* FROM ( ".$sql." ) data WHERE no_bukti NOT IN (".$sql_bpom.")";
			}
		}
		
		if($group=="EDC") {
			$sql = "select jc.jcard_edc, jc.jcard_nama, jc.jcard_edc_promo as keterangan,
							mjp.jproduk_tanggal as tanggal, mjp.jproduk_nobukti as no_bukti,
							c.cust_no, c.cust_nama, sum(jc.jcard_nilai) as total
					from master_jual_produk mjp
						join jual_card jc on jc.jcard_ref = mjp.jproduk_nobukti
						left join customer c on c.cust_id = mjp.jproduk_cust
					where jproduk_stat_dok='Tertutup' and $tgl
					group by jc.jcard_edc, jc.jcard_nama, jcard_edc_promo, jc.jcard_ref
					order by jc.jcard_edc asc , jc.jcard_nama asc, jcard_edc_promo asc, jc.jcard_ref asc
					";
		}		
		$this->db = $this->load->database("default_report", TRUE);
		$query=$this->db->query($sql);
		$this->db = $this->load->database("default", TRUE);
		return $query->result();
	}
		
	function get_reveral_list($query,$start,$end){
		$sql = "SELECT 
					karyawan_id, karyawan_no, karyawan_username, karyawan_nama, karyawan_tgllahir, karyawan_alamat
				FROM karyawan 
				WHERE 
					karyawan_aktif = 'Aktif' 
					AND (karyawan_cabang = (SELECT info_cabang FROM info limit 1) 
							OR substring(karyawan_cabang2, (SELECT cabang_value FROM cabang	
															LEFT JOIN info ON cabang.cabang_id = info.info_cabang
															WHERE info.info_cabang = cabang.cabang_id), 1) = '1')";
		if($query<>""){
			$sql=$sql." and (karyawan_no like '%".$query."%' or karyawan_nama like '%".$query."%' or karyawan_username like '%".$query."%') ";
		}
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit);  
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
		
	function get_produk_list($query,$start,$end,$aktif){
		$rs_rows=0;
		if($aktif=='yes'){
			$sql="select produk_id, produk_harga, produk_kode, kategori_nama,
							produk_du, produk_dm, produk_dm2, produk_dultah, produk_dcard, produk_dkolega,
							produk_dkeluarga, produk_downer, produk_dgrooming, produk_dwartawan, produk_dstaffdokter,
							produk_dstaffnondokter, produk_dpromo,produk_dpromo2, produk_dcompliment, produk_nama, produk_edit_harga,
							sk.konversi_nilai as djkonversi_nilai, sk.konversi_satuan as djkonversi_satuan
					  from produk p
					  left join kategori k ON p.produk_kategori = k.kategori_id
					  left join satuan_konversi sk ON sk.konversi_produk=p.produk_id and sk.konversi_default = true
					  WHERE produk_aktif = 'Aktif'";
		}else{
			$sql="select *, '' as djkonversi_nilai, '' as djkonversi_satuan from vu_produk";
		}
		
		if($query<>"" /*&& is_numeric($query)==false*/){
			$sql.=eregi("WHERE",$sql)? " AND ":" WHERE ";
			$sql.=" (produk_kodelama like '%".$query."%' or produk_kode like '%".$query."%' or produk_nama like '%".$query."%' ) ";
		}else{
			if($rs_rows){
				$filter="";
				$sql.=eregi("WHERE",$sql)? " AND ":" WHERE ";
				foreach($rs->result() as $row_dproduk){
					
					$filter.="OR produk_id='".$row_dproduk->dproduk_produk."' ";
				}
				$sql=$sql."(".substr($filter,2,strlen($filter)).")";
			}
		}
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		/*if(($end!=0)  && ($aktif<>'yesno')){
			$limit = $sql." LIMIT ".$start.",".$end;			
			$result = $this->db->query($limit);
		}*/
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_produk_list_barcode($member_no){
		$rs_rows=0;
		$sql="select produk_id, produk_harga, produk_kode, kategori_nama,
					produk_du, produk_dm, produk_dm2, produk_dultah, produk_dcard, produk_dkolega,
					produk_dkeluarga, produk_downer, produk_dgrooming, produk_dwartawan, produk_dstaffdokter,
					produk_dstaffnondokter, produk_dpromo,produk_dpromo2, produk_dcompliment, produk_nama, produk_edit_harga,
					sk.konversi_nilai as djkonversi_nilai, sk.konversi_satuan as djkonversi_satuan
			  from produk p
			  left join kategori k ON p.produk_kategori = k.kategori_id
			  left join satuan_konversi sk ON sk.konversi_produk=p.produk_id and sk.konversi_default = true
			  WHERE produk_aktif = 'Aktif' and produk_kode = '".$member_no."' ";
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_satuan_bydjproduk_list($djproduk_id, $produk_id, $satuan_id){
		$sql_satuan = "SELECT sk.konversi_nilai AS konversi_nilai FROM satuan_konversi sk
						WHERE sk.konversi_produk = '$produk_id' AND sk.konversi_default = 'true'";
		$query_tgl=$this->db->query($sql_satuan);
		$tgl=$query_tgl->row();
		if($produk_id=='')
			$konversi_nilai=1;
		else
			$konversi_nilai=$tgl->konversi_nilai;
		
		if($satuan_id <> ''){
			$query_satuan = "AND satuan_id = '$satuan_id'";
		}else{
			$query_satuan = "";
		}
		
		if($djproduk_id<>0)
			$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_harga,produk_edit_harga 
					FROM satuan 
					LEFT JOIN satuan_konversi ON(konversi_satuan=satuan_id) 
					LEFT JOIN produk ON(konversi_produk=produk_id)
					LEFT JOIN detail_jual_produk ON(dproduk_produk=produk_id) 
					LEFT JOIN master_jual_produk ON(dproduk_master=jproduk_id) 
					WHERE jproduk_id='$djproduk_id'";
		
		if($produk_id<>0)
			$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_edit_harga,
					IF(konversi_default = 'false', produk_harga/$konversi_nilai, produk_harga) as produk_harga
					FROM satuan 
					LEFT JOIN satuan_konversi ON(konversi_satuan=satuan_id) 
					LEFT JOIN produk ON(konversi_produk=produk_id) 
					WHERE produk_id='$produk_id'".$query_satuan;
			
		if($djproduk_id==0 && $produk_id==0)
			$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_harga,produk_edit_harga 
					FROM produk,satuan_konversi,satuan 
					WHERE produk_id=konversi_produk AND konversi_satuan=satuan_id";
		//$sql="SELECT satuan_id,satuan_nama,satuan_kode FROM satuan";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_satuan_bydjproduk_list2($djproduk_id, $produk_id, $satuan_id){
		$sql_satuan = "SELECT sk.konversi_nilai AS konversi_nilai FROM satuan_konversi sk
						WHERE sk.konversi_produk = '$produk_id' AND sk.konversi_default = 'true'";
		$query_tgl=$this->db->query($sql_satuan);
		$tgl=$query_tgl->row();
		if($produk_id=='')
			$konversi_nilai=1;
		else
			$konversi_nilai=$tgl->konversi_nilai;
		
		if($satuan_id <> ''){
			$query_satuan = "AND satuan_id = '$satuan_id'";
		}else{
			$query_satuan = "";
		}
		
		if($djproduk_id<>0)
			$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_harga,produk_edit_harga 
					FROM satuan 
					LEFT JOIN satuan_konversi ON(konversi_satuan=satuan_id) 
					LEFT JOIN produk ON(konversi_produk=produk_id)
					LEFT JOIN detail_jual_produk ON(dproduk_produk=produk_id) 
					LEFT JOIN master_jual_produk ON(dproduk_master=jproduk_id) 
					WHERE jproduk_id='$djproduk_id'";
		
		if($produk_id<>0)
			$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_edit_harga,
					IF(konversi_default = 'false', produk_harga/$konversi_nilai, produk_harga) as produk_harga
					FROM satuan 
					LEFT JOIN satuan_konversi ON(konversi_satuan=satuan_id) 
					LEFT JOIN produk ON(konversi_produk=produk_id) 
					WHERE produk_id='$produk_id'".$query_satuan;
			
		if($djproduk_id==0 && $produk_id==0)
			$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_harga,produk_edit_harga 
					FROM produk,satuan_konversi,satuan 
					WHERE produk_id=konversi_produk AND konversi_satuan=satuan_id";
		//$sql="SELECT satuan_id,satuan_nama,satuan_kode FROM satuan";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_satuan_byproduk_list($jproduk_id, $produk_id){
		$rs_rows=0;
		
		if($produk_id!=0 && is_numeric($produk_id)==true){
			$sql="SELECT satuan_id, satuan_nama, konversi_nilai, satuan_kode, konversi_default, produk_harga FROM satuan_konversi LEFT JOIN produk ON(konversi_produk=produk_id) LEFT JOIN satuan ON(konversi_satuan=satuan_id) WHERE konversi_produk='$produk_id'";
			$rs=$this->db->query($sql);
			$rs_rows=$rs->num_rows();
			if($produk_id<>"" && is_numeric($produk_id)==false){
				$sql.=eregi("WHERE",$sql)? " AND ":" WHERE ";
				$sql.=" (satuan_nama like '%".$produk_id."%' or satuan_kode like '%".$produk_id."%') ";
			}
			
			$result = $this->db->query($sql);
			$nbrows = $result->num_rows();
			/*if($end!=0){
				$limit = $sql." LIMIT ".$start.",".$end;			
				$result = $this->db->query($limit);
			}*/
			if($nbrows>0){
				foreach($result->result() as $row){
					$arr[] = $row;
				}
				$jsonresult = json_encode($arr);
				return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
			} else {
				return '({"total":"0", "results":""})';
			}
		}elseif($jproduk_id!=0 && is_numeric($jproduk_id)==true){
			$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_harga FROM produk,satuan_konversi,satuan WHERE produk_id=konversi_produk AND konversi_satuan=satuan_id AND produk_id='$jproduk_id'";
			if($jproduk_id==0)
				$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_harga FROM produk,satuan_konversi,satuan WHERE produk_id=konversi_produk AND konversi_satuan=satuan_id";
			$query = $this->db->query($sql);
			$nbrows = $query->num_rows();
			if($nbrows>0){
				foreach($query->result() as $row){
					$arr[] = $row;
				}
				$jsonresult = json_encode($arr);
				return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
			} else {
				return '({"total":"0", "results":""})';
			}
		}
		
	}
		
	//function for detail
	//get record list
	function detail_detail_jual_produk_list($master_id,$query,$start,$end) {
		//$query="SELECT *,konversi_nilai FROM detail_jual_produk LEFT JOIN satuan_konversi ON(dproduk_produk=konversi_produk AND dproduk_satuan=konversi_satuan) WHERE dproduk_master='".$master_id."'";
		
		$query = "SELECT djp.*, p.produk_nama, s.satuan_nama, k.karyawan_username, mjp.jproduk_bayar, mjp.jproduk_diskon, 
				    djp.dproduk_harga*djp.dproduk_jumlah AS dproduk_subtotal,
				    djp.dproduk_harga*djp.dproduk_jumlah*((100-djp.dproduk_diskon)/100) AS dproduk_subtotal_net, 
				    p.produk_point, djp.dproduk_harga AS produk_harga_default, p.produk_kode
			    FROM detail_jual_produk djp
				LEFT JOIN master_jual_produk mjp ON(dproduk_master=jproduk_id) 
				LEFT JOIN satuan s ON(djp.dproduk_satuan=s.satuan_id)
				LEFT JOIN satuan_konversi sk ON(dproduk_produk=konversi_produk AND dproduk_satuan=konversi_satuan) 				
				LEFT JOIN karyawan k ON(djp.dproduk_karyawan=k.karyawan_id)
				LEFT JOIN produk p ON(dproduk_produk=produk_id) 
			    WHERE dproduk_master='".$master_id."'";
		
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		$limit = $query." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit);  
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	//end of function
	
	function get_voucher_list($query,$start,$end){
		$query = "SELECT * FROM voucher,voucher_kupon where kvoucher_master=voucher_id";
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		$limit = $query." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit); 
	
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	//get master id, note : not done yet
	function get_master_id() {
		$query = "SELECT max(jproduk_id) AS master_id FROM master_jual_produk WHERE jproduk_creator='".$_SESSION[SESSION_USERID]."'";
		$result = $this->db->query($query);
		if($result->num_rows()){
			$data=$result->row();
			$master_id=$data->master_id;
			return $master_id;
		}else{
			return '0';
		}
	}
	//eof
	
	function get_point_per_rupiah(){
		$query = "SELECT setmember_point_perrp FROM member_setup LIMIT 1";
		$result = $this->db->query($query);
		if($result->num_rows()){
			$data=$result->row();
			$setmember_point_perrp=$data->setmember_point_perrp;
			return $setmember_point_perrp;
		}else{
			return 0;
		}
	}
	
	function catatan_piutang_update($jproduk_id){
		/* INSERT db.master_lunas_piutang */
		/*$dti_lpiutang=array(
			"lpiutang_faktur"=>$jproduk_nobukti,
			"lpiutang_cust"=>$jproduk_cust,
			"lpiutang_faktur_tanggal"=>$jproduk_tanggal,
			"lpiutang_total"=>$piutang_total,
			"lpiutang_sisa"=>$piutang_total,
			"lpiutang_jenis_transaksi"=>'jual_produk',
			"lpiutang_stat_dok"=>'Terbuka'
		);
		$this->db->insert('master_lunas_piutang', $dti_lpiutang);
		if($this->db->affected_rows()){
			return 1;
		}*/
		$sql="SELECT * FROM vu_piutang_jproduk WHERE jproduk_id='$jproduk_id'";
		$rs=$this->db->query($sql);
		if($rs->num_rows()){
			$rs_record=$rs->row_array();
			$lpiutang_faktur=$rs_record["jproduk_nobukti"];
			$lpiutang_cust=$rs_record["jproduk_cust"];
			$lpiutang_faktur_tanggal=$rs_record["jproduk_tanggal"];
			$lpiutang_total=$rs_record["piutang_total"];
			/* ini artinya: No.Faktur Penjualan Produk ini masih BELUM LUNAS */
			/* untuk itu, No.Faktur ini akan dimasukkan ke db.master_lunas_piutang sebagai daftar yang harus ditagihkan ke Customer */
			
			/* Checking terlebih dahulu ke db.master_lunas_piutang WHERE =$lpiutang_faktur:
			* JIKA 'ada' ==> Lakukan UPDATE db.master_lunas_piutang
			* JIKA 'tidak ada' ==> Lakukan INSERT db.master_lunas_piutang
			*/
			$sql="SELECT lpiutang_id FROM master_lunas_piutang WHERE lpiutang_faktur='$lpiutang_faktur'";
			$rs=$this->db->query($sql);
			if($rs->num_rows()){
				/* 1. DELETE db.master_lunas_piutang AND db.detail_jual_piutang
				 * 2. INSERT to db.master_lunas_piutang
				*/
				$sql = "DELETE detail_lunas_piutang, master_lunas_piutang
					FROM master_lunas_piutang
					LEFT JOIN detail_lunas_piutang ON(dpiutang_master=lpiutang_id)
					WHERE lpiutang_faktur='".$lpiutang_faktur."'";
				$this->db->query($sql);
				if($this->db->affected_rows()>-1){
					//INSERT to db.master_lunas_piutang
					$dti_lpiutang=array(
					"lpiutang_faktur"=>$lpiutang_faktur,
					"lpiutang_faktur_id"=>$jproduk_id,
					"lpiutang_cust"=>$lpiutang_cust,
					"lpiutang_faktur_tanggal"=>$lpiutang_faktur_tanggal,
					"lpiutang_total"=>$lpiutang_total,
					"lpiutang_sisa"=>$lpiutang_total,
					"lpiutang_jenis_transaksi"=>'jual_produk',
					"lpiutang_stat_dok"=>'Terbuka'
					);
					$this->db->insert('master_lunas_piutang', $dti_lpiutang);
					if($this->db->affected_rows()){
						return 1;
					}
				}
			}else{
				/* INSERT db.master_lunas_piutang */
				$dti_lpiutang=array(
				"lpiutang_faktur"=>$lpiutang_faktur,
				"lpiutang_faktur_id"=>$jproduk_id,
				"lpiutang_cust"=>$lpiutang_cust,
				"lpiutang_faktur_tanggal"=>$lpiutang_faktur_tanggal,
				"lpiutang_total"=>$lpiutang_total,
				"lpiutang_sisa"=>$lpiutang_total,
				"lpiutang_jenis_transaksi"=>'jual_produk',
				"lpiutang_stat_dok"=>'Terbuka'
				);
				$this->db->insert('master_lunas_piutang', $dti_lpiutang);
				if($this->db->affected_rows()){
					return 1;
				}
			}
		}else{
			return 1;
		}
	}
	
	function catatan_piutang_batal($jproduk_id){
		/* 1. Cari jproduk_nobukti
		 * 2. UPDATE db.master_lunas_piutang.lpiutang_stat_dok = 'Batal'
		*/
		$datetime_now = date('Y-m-d H:i:s');
		
		$sql = "SELECT jproduk_nobukti FROM master_jual_produk WHERE jproduk_id='".$jproduk_id."'";
		$rs = $this->db->query($sql);
		if($rs->num_rows()){
			$record = $rs->row_array();
			$jproduk_nobukti = $record['jproduk_nobukti'];
			
			//UPDATE db.master_lunas_piutang.lpiutang_stat_dok = 'Batal'
			$sqlu = "UPDATE master_lunas_piutang
				SET lpiutang_stat_dok='Batal'
					,lpiutang_update='".@$_SESSION[SESSION_USERID]."'
					,lpiutang_date_update='".$datetime_now."'
					,lpiutang_revised=(lpiutang_revised+1)
				WHERE lpiutang_faktur='".$jproduk_nobukti."'";
			$this->db->query($sqlu);
			if($this->db->affected_rows()>-1){
				return 1;
			}else{
				return 1;
			}
		}else{
			return 1;
		}
		
	}
	
	function member_point_update($jproduk_id, $jproduk_cust){
		$date_now=date('Y-m-d');		
		$percent_reward = $this->m_public_function->get_percent_point_reward();
		
		/* Perhitungan point reward sudah berubah, 1% dari net sales nya pada tanggal 14-01-2014 */
		/*$sqlu = "UPDATE master_jual_produk
			SET jproduk_preward = ((jproduk_totalbiaya * $percent_reward) / 100)
			WHERE jproduk_id = $jproduk_id";*/
			
		$sqlu = "UPDATE customer LEFT JOIN master_jual_produk ON jproduk_cust = cust_id
			SET cust_preward_total = cust_preward_total-potong_point +(((jproduk_totalbiaya+potong_point) * $percent_reward) / 100)
			WHERE jproduk_id = $jproduk_id and jproduk_cust = $jproduk_cust";
		
		$this->db->query($sqlu);
		
		$t_err_msg = $this->db->_error_message();

		if(empty($t_err_msg)){			
		    /* Perhitungan point reward sudah berubah, 1% dari net sales nya pada tanggal 14-01-2014 */
		    /* Sudah tidak diperlukan lagi untuk mengupdate cust_preward 22-01-2014 		    
		    $sqlu_cust = 
		       "UPDATE customer c					
			SET c.cust_preward = c.cust_preward + (SELECT m.jproduk_preward FROM master_jual_produk m WHERE m.jproduk_id = $jproduk_id)
			WHERE c.cust_id = (SELECT m2.jproduk_cust FROM master_jual_produk m2 WHERE m2.jproduk_id = $jproduk_id)";

		    $this->db->query($sqlu_cust);

		    if($this->db->affected_rows()>-1){
			return 1;
		    }
		    */
		    return 1;
		}else{
		    return 0;
		}
		
	}
	
	function cara_bayar_batal($jproduk_id){
		//updating db.jual_card ==> pembatalan
		$sqlu_jcard = "UPDATE jual_card JOIN master_jual_produk ON(jual_card.jcard_ref=master_jual_produk.jproduk_nobukti)
			SET jual_card.jcard_stat_dok = master_jual_produk.jproduk_stat_dok
			WHERE master_jual_produk.jproduk_id='$jproduk_id'";
		$this->db->query($sqlu_jcard);
		
		//updating db.jual_vgrooming ==> pembatalan
		$sqlu_jvgrooming = "UPDATE jual_vgrooming JOIN master_jual_produk ON(jual_vgrooming.jvgrooming_ref=master_jual_produk.jproduk_nobukti)
			SET jual_vgrooming.jvgrooming_stat_dok = master_jual_produk.jproduk_stat_dok
			WHERE master_jual_produk.jproduk_id='$jproduk_id'";
		$this->db->query($sqlu_jvgrooming);

		//updating db.jual_cek ==> pembatalan
		$sqlu_jcek = "UPDATE jual_cek JOIN master_jual_produk ON(jual_cek.jcek_ref=master_jual_produk.jproduk_nobukti)
			SET jual_cek.jcek_stat_dok = master_jual_produk.jproduk_stat_dok
			WHERE master_jual_produk.jproduk_id='$jproduk_id'";
		$this->db->query($sqlu_jcek);

		//updating db.jual_kwitansi ==> pembatalan
		$sqlu_jkwitansi = "UPDATE jual_kwitansi JOIN master_jual_produk ON(jual_kwitansi.jkwitansi_ref=master_jual_produk.jproduk_nobukti)
			SET jual_kwitansi.jkwitansi_stat_dok = master_jual_produk.jproduk_stat_dok
			WHERE master_jual_produk.jproduk_id='$jproduk_id'";
		$this->db->query($sqlu_jkwitansi);

			/* UPDATE db.cetak_kwitansi.kwitansi_sisa <== dikembalikan sesuai dengan nilai kwitansi yg diambil di Faktur ini. */
			$sql = "SELECT jkwitansi_master
				FROM jual_kwitansi
				JOIN master_jual_produk ON(jkwitansi_ref=jproduk_nobukti)
				WHERE jproduk_id='".$jproduk_id."'";
			$rs = $this->db->query($sql);
			if($rs->num_rows()){
				foreach($rs->result() as $row){
					//updating sisa kwitansi
					$sqlu = "UPDATE cetak_kwitansi
								LEFT JOIN (SELECT sum(jkwitansi_nilai) AS total_kwitansi
									,jkwitansi_master 
								FROM jual_kwitansi
								WHERE jkwitansi_master<>0
									AND jkwitansi_stat_dok<>'Batal'
									AND jkwitansi_master='".$row->jkwitansi_master."'
								GROUP BY jkwitansi_master) AS vu_kw ON(vu_kw.jkwitansi_master=kwitansi_id)
							SET kwitansi_sisa=(kwitansi_nilai - ifnull(vu_kw.total_kwitansi,0))
							WHERE kwitansi_id='".$row->jkwitansi_master."'";
					$this->db->query($sqlu);
				}
			}
			
		//updating db.jual_gift_voucher ==> pembatalan
		$sqlu_jkwitansi = "UPDATE jual_gift_voucher JOIN master_jual_produk ON(jual_gift_voucher.jgift_voucher_ref=master_jual_produk.jproduk_nobukti)
			SET jual_gift_voucher.jgift_voucher_stat_dok = master_jual_produk.jproduk_stat_dok
			WHERE master_jual_produk.jproduk_id='$jproduk_id'";
		$this->db->query($sqlu_jkwitansi);

			/* UPDATE db.cetak_gift_voucher.gift_voucher_sisa <== dikembalikan sesuai dengan nilai kwitansi yg diambil di Faktur ini. */
			$sql = "SELECT jgift_voucher_master
				FROM jual_gift_voucher
				JOIN master_jual_produk ON(jgift_voucher_ref=jproduk_nobukti)
				WHERE jproduk_id='".$jproduk_id."'";
			$rs = $this->db->query($sql);
			if($rs->num_rows()){
				foreach($rs->result() as $row){
					//updating sisa kwitansi
					$sqlu = "UPDATE cetak_gift_voucher
								LEFT JOIN (SELECT sum(jgift_voucher_nilai) AS total_vg
									,jgift_voucher_master 
								FROM jual_gift_voucher
								WHERE jgift_voucher_master<>0
									AND jgift_voucher_stat_dok<>'Batal'
									AND jgift_voucher_master='".$row->jgift_voucher_master."'
								GROUP BY jgift_voucher_master) AS vu_vg ON(vu_vg.jgift_voucher_master=gift_voucher_id)
							SET gift_voucher_sisa=(gift_voucher_nilai - ifnull(vu_vg.total_vg,0))
							WHERE gift_voucher_id='".$row->jgift_voucher_master."'";
					$this->db->query($sqlu);
				}
			}
		
		//updating db.jual_transfer ==> pembatalan
		$sqlu_jtransfer = "UPDATE jual_transfer JOIN master_jual_produk ON(jual_transfer.jtransfer_ref=master_jual_produk.jproduk_nobukti)
			SET jual_transfer.jtransfer_stat_dok = master_jual_produk.jproduk_stat_dok
			WHERE master_jual_produk.jproduk_id='$jproduk_id'";
		$this->db->query($sqlu_jtransfer);

		//updating db.jual_tunai ==> pembatalan
		$sqlu_jtunai = "UPDATE jual_tunai JOIN master_jual_produk ON(jual_tunai.jtunai_ref=master_jual_produk.jproduk_nobukti)
			SET jual_tunai.jtunai_stat_dok = master_jual_produk.jproduk_stat_dok
			WHERE master_jual_produk.jproduk_id='$jproduk_id'";
		$this->db->query($sqlu_jtunai);

		//updating db.voucher_terima ==> pembatalan
		$sqlu_tvoucher = "UPDATE voucher_terima JOIN master_jual_produk ON(voucher_terima.tvoucher_ref=master_jual_produk.jproduk_nobukti)
			SET voucher_terima.tvoucher_stat_dok = master_jual_produk.jproduk_stat_dok
			WHERE master_jual_produk.jproduk_id='$jproduk_id'";
		$this->db->query($sqlu_tvoucher);

			return 1;
		}

	/*function membership_insert($jproduk_id){
		
		$sql="SELECT setmember_transhari, setmember2_transhari, setmember_periodeaktif, setmember_periodetenggang, 
			    setmember_transtenggang, setmember_pointhari
			FROM member_setup 
			LIMIT 1";
		$rs=$this->db->query($sql);
		if($rs->num_rows()){
			$rs_record=$rs->row_array();
			$min_trans_member_baru=$rs_record['setmember_transhari'];
			$min_trans_member2_baru=$rs_record['setmember2_transhari'];
			$periode_tenggang=$rs_record['setmember_periodetenggang'];
			$min_trans_tenggang=$rs_record['setmember_transtenggang'];
			$setmember_pointhari=$rs_record['setmember_pointhari'];
			$periode_aktif=$rs_record['setmember_periodeaktif'];
		}
		
		$sql="SELECT jproduk_cust, jproduk_tanggal
			FROM master_jual_produk
			WHERE jproduk_id='$jproduk_id'";
		$rs=$this->db->query($sql);
		if($rs->num_rows()){
			$rs_record=$rs->row_array();
			$cust_id = $rs_record['jproduk_cust'];
			$tanggal_transaksi = $rs_record['jproduk_tanggal'];
			
			$jproduk_total_trans=0;
			$jproduk_total_point=0;
			$jproduk_total_preward=0;
			
			$jpaket_total_trans=0;
			$jpaket_total_point=0;
			$jpaket_total_preward=0;
			
			$jrawat_total_trans=0;
			$jrawat_total_point=0;
			$jrawat_total_preward=0;
			
			$jkwitansi_total_trans=0;
			
			$cust_total_trans_now=0;
			$cust_total_point=0;
			$cust_total_preward=0;
			
			$trans_jproduk = "SELECT SUM(jproduk_totalbiaya) AS jproduk_total_trans, SUM(jproduk_point) AS jproduk_total_point, SUM(jproduk_preward) AS jproduk_total_preward
					FROM master_jual_produk
					WHERE jproduk_cust='".$cust_id."' AND jproduk_tanggal='".$tanggal_transaksi."' AND jproduk_stat_dok='Tertutup'
					GROUP BY jproduk_cust";
			$rs_trans_jproduk=$this->db->query($trans_jproduk);
			if($rs_trans_jproduk->num_rows()){
				$rs_trans_jproduk_record=$rs_trans_jproduk->row_array();
				$jproduk_total_trans=$rs_trans_jproduk_record['jproduk_total_trans'];
				$jproduk_total_point=$rs_trans_jproduk_record['jproduk_total_point'];
				$jproduk_total_preward=$rs_trans_jproduk_record['jproduk_total_preward'];
			}
			
			#NGGANTI INI
			$trans_jpaket = "SELECT SUM(jpaket_totalbiaya) AS jpaket_total_trans, SUM(jpaket_point) AS jpaket_total_point, 
							SUM(jpaket_preward) AS jpaket_total_preward
					FROM master_jual_paket
					WHERE jpaket_cust='".$cust_id."' AND jpaket_tanggal='".$tanggal_transaksi."' AND jpaket_stat_dok='Tertutup'
					GROUP BY jpaket_cust";
			$rs_trans_jpaket=$this->db->query($trans_jpaket);
			if($rs_trans_jpaket->num_rows()){
				$rs_trans_jpaket_record=$rs_trans_jpaket->row_array();
				$jpaket_total_trans=$rs_trans_jpaket_record['jpaket_total_trans'];
				$jpaket_total_point=$rs_trans_jpaket_record['jpaket_total_point'];
				$jpaket_total_preward=$rs_trans_jpaket_record['jpaket_total_preward'];
			}
			
			$trans_jrawat = "SELECT SUM(jrawat_totalbiaya) AS jrawat_total_trans, SUM(jrawat_point) AS jrawat_total_point,
						SUM(jrawat_preward) AS jrawat_total_preward
					FROM master_jual_rawat
					WHERE jrawat_cust='".$cust_id."' AND jrawat_tanggal='".$tanggal_transaksi."' AND jrawat_stat_dok='Tertutup'
					GROUP BY jrawat_cust";
			$rs_trans_jrawat=$this->db->query($trans_jrawat);
			if($rs_trans_jrawat->num_rows()){
				$rs_trans_jrawat_record=$rs_trans_jrawat->row_array();
				$jrawat_total_trans=$rs_trans_jrawat_record['jrawat_total_trans'];
				$jrawat_total_point=$rs_trans_jrawat_record['jrawat_total_point'];
				$jrawat_total_preward=$rs_trans_jrawat_record['jrawat_total_preward'];
			}
						
			$trans_jkwitansi = "SELECT SUM(kwitansi_nilai) AS jkwitansi_total_trans
						FROM cetak_kwitansi
						WHERE kwitansi_cust='$cust_id' AND kwitansi_tanggal='$tanggal_transaksi' AND kwitansi_status='Tertutup' AND kwitansi_cara <> 'retur'
						GROUP BY kwitansi_cust";
				$rs_trans_jkwitansi=$this->db->query($trans_jkwitansi);
				if($rs_trans_jkwitansi->num_rows()){
					$rs_trans_jkwitansi_record=$rs_trans_jkwitansi->row_array();
					$jkwitansi_total_trans=$rs_trans_jkwitansi_record['jkwitansi_total_trans'];
				}
				
			$cust_total_trans_now = $jproduk_total_trans + $jpaket_total_trans + $jrawat_total_trans + $jkwitansi_total_trans;
			$cust_total_point = $jproduk_total_point + $jpaket_total_point + $jrawat_total_point;
			$cust_total_preward = $jproduk_total_preward + $jpaket_total_preward + $jrawat_total_preward;
			
			//Cek apakah member atau tidak berdasarkan berdasarkan kondisi dibawah ini
			//1. Jika kolom cust_member_no di tabel customer nilainya NULL
			//2. Customer tersebut belum di insertkan ke dalam tabel member_temp
			//3. Total transaksi pada hari ini lebih dari yang telah di setting di database di tabel member_setup
			
			//Cek di tabel customer
			$sql_cust="SELECT * FROM customer WHERE cust_id='$cust_id'";
			$rs_cust=$this->db->query($sql_cust);
			$data_cust = $rs_cust->row_array();
			
			//Cek di tabel member
			$sql_member="SELECT * FROM member_temp WHERE membert_cust='$cust_id'";
			$rs_member=$this->db->query($sql_member);
		
			if($data_cust['cust_member_level'] < 2 && $cust_total_trans_now >= $min_trans_member2_baru && $min_trans_member2_baru > $min_trans_member_baru){
				if(empty($data_cust['cust_member_no']) && $rs_member->num_rows() == 0) {
				    $dti_membert=array(
					"membert_cust"=>$cust_id,
					"membert_register"=>$tanggal_transaksi,
					"membert_valid"=>'2030-12-31',	//Member baru per 1 januari 2014 member valid selamanya
					"membert_jenis"=>'baru',
					"membert_status"=>'Daftar'
				    );

				    $this->db->insert('member_temp', $dti_membert);
				} else {
					$sql_update_member_temp = "update member_temp set membert_register = now() where membert_cust = '$cust_id'";
					$update_member_temp = $this->db->query($sql_update_member_temp);
				}
				
				$sql_update_cust_member_level = "update customer set cust_member_level = 2 where cust_id = '$cust_id'";
				$update_cust_member_level = $this->db->query($sql_update_cust_member_level);

				return 1;
			} else {
				if(empty($data_cust['cust_member_no']) && $rs_member->num_rows() == 0 && $cust_total_trans_now >= $min_trans_member_baru) {
				    $dti_membert=array(
					"membert_cust"=>$cust_id,
					"membert_register"=>$tanggal_transaksi,
					"membert_valid"=>'2030-12-31',	//Member baru per 1 januari 2014 member valid selamanya
					"membert_jenis"=>'baru',
					"membert_status"=>'Daftar'
				    );

				    $this->db->insert('member_temp', $dti_membert);

					$sql_update_cust_member_level = "update customer set cust_member_level = 1 where cust_id = '$cust_id'";
					$update_cust_member_level = $this->db->query($sql_update_cust_member_level);

				    if($this->db->affected_rows()>-1)
					return 1;
				    else
					return 1;
				} else {
				    return 1;
				}
			//}
		}else{
			return 1;
		}
	}*/
	
	function stat_dok_tertutup_update($jproduk_id){
		$date_now = date('Y-m-d');
		$datetime_now = date('Y-m-d H:i:s');
		//* status dokumen menjadi tertutup setelah Faktur selesai di-cetak_jproduk /
		$sql = "SELECT jproduk_tanggal FROM master_jual_produk WHERE jproduk_id='".$jproduk_id."'";
		$rs = $this->db->query($sql);
		if($rs->num_rows()){
			$record = $rs->row_array();
			$jproduk_tanggal = $record['jproduk_tanggal'];
			/*
			if($jproduk_tanggal<>$date_now){
				$jproduk_date_update = $jproduk_tanggal;
			}else{
				$jproduk_date_update = $datetime_now;
			}
			*/
			
			$sql="UPDATE master_jual_produk
				SET jproduk_stat_dok='Tertutup'
					,jproduk_update='".@$_SESSION[SESSION_USERID]."'
					,jproduk_date_update='".$datetime_now."'
					,jproduk_revised=jproduk_revised+1
				WHERE jproduk_id='".$jproduk_id."'";
			$this->db->query($sql);
			if($this->db->affected_rows()>-1){
				return 1;
			}
		}else{
			return 1;
		}
	}
	
	//insert detail record
	function detail_detail_jual_produk_insert($array_dproduk_id ,$dproduk_master ,$array_dproduk_karyawan ,$array_dproduk_produk
											  ,$array_dproduk_satuan ,$array_dproduk_jumlah ,$array_dproduk_harga
											  ,$array_dproduk_diskon ,$array_dproduk_diskon_jenis ,$cetak_jproduk, $jproduk_cust){
		$date_now=date('Y-m-d');
		
		$size_array = sizeof($array_dproduk_produk) - 1;
		
		for($i = 0; $i < sizeof($array_dproduk_produk); $i++){
			$dproduk_id = $array_dproduk_id[$i];
			$dproduk_karyawan = $array_dproduk_karyawan[$i];
			$dproduk_produk = $array_dproduk_produk[$i];
			$dproduk_satuan = $array_dproduk_satuan[$i];
			$dproduk_jumlah = $array_dproduk_jumlah[$i];
			$dproduk_harga = $array_dproduk_harga[$i];
			$dproduk_diskon = $array_dproduk_diskon[$i];
			$dproduk_diskon_jenis = $array_dproduk_diskon_jenis[$i];
			
			$sql = "SELECT produk_point, produk_preward
				FROM detail_jual_produk
				    JOIN produk ON(dproduk_produk=produk_id)
				WHERE dproduk_id='".$dproduk_id."'";
			
			$rs = $this->db->query($sql);
			if($rs->num_rows()){
				$record = $rs->row_array();
				
				$produk_point = $record['produk_point'];
				$produk_preward = $record['produk_preward'];
				
				//* artinya: detail produk ini sudah diinsertkan ke db.detail_jual_produk /
				$dtu_dproduk = array(
					"dproduk_produk"=>$dproduk_produk,
					"dproduk_set_point"=>$produk_point,
					"dproduk_preward"=>$produk_preward,
					"dproduk_karyawan"=>$dproduk_karyawan,
					"dproduk_satuan"=>$dproduk_satuan, 
					"dproduk_jumlah"=>$dproduk_jumlah, 
					"dproduk_harga"=>$dproduk_harga, 
					"dproduk_diskon"=>$dproduk_diskon,
					"dproduk_diskon_jenis"=>$dproduk_diskon_jenis,
					"dproduk_creator"=>@$_SESSION[SESSION_USERID]
				);
			//	$this->db->query('LOCK TABLE detail_jual_produk WRITE');
				$this->db->where('dproduk_id', $dproduk_id);
				$this->db->update('detail_jual_produk', $dtu_dproduk);
			//	$this->db->query('UNLOCK TABLES');
			}else{
				$sql_produk = "SELECT produk_point, produk_preward FROM produk WHERE produk_id='".$dproduk_produk."'";
				$rs_produk = $this->db->query($sql_produk);
				$record_produk = $rs_produk->row_array();
				
				$produk_point = $record_produk['produk_point'];
				$produk_preward = $record_produk['produk_preward'];
				
				//* artinya: detail produk ini adalah penambahan detail baru /
				$dti_jproduk = array(
					"dproduk_master"=>$dproduk_master, 
					"dproduk_produk"=>$dproduk_produk,
					"dproduk_set_point"=>$produk_point,
					"dproduk_preward"=>$produk_preward,
					"dproduk_karyawan"=>$dproduk_karyawan,
					"dproduk_satuan"=>$dproduk_satuan, 
					"dproduk_jumlah"=>$dproduk_jumlah, 
					"dproduk_harga"=>$dproduk_harga, 
					"dproduk_diskon"=>$dproduk_diskon,
					"dproduk_diskon_jenis"=>$dproduk_diskon_jenis,
					"dproduk_creator"=>@$_SESSION[SESSION_USERID]
				);
				//$this->db->query('LOCK TABLE detail_jual_produk WRITE');
				$this->db->insert('detail_jual_produk', $dti_jproduk);
				//$this->db->query('UNLOCK TABLES');
			}
			
			if(($cetak_jproduk==1 || $cetak_jproduk==2) && $i==$size_array){
				/*proses cetak_jproduk*/
				$rs_stat_dok = $this->stat_dok_tertutup_update($dproduk_master);
				$rs_point_update = $this->member_point_update($dproduk_master, $jproduk_cust);
				//$rs_membership = $this->membership_insert($dproduk_master);
				$rs_piutang_update = $this->catatan_piutang_update($dproduk_master);

				return $dproduk_master;
			}else if(($cetak_jproduk<>1 || $cetak_jproduk<>2) && $i==$size_array){
				return 0;
			}
			
		}
		
	}
	//end of function
	
	//function for get list record
	function master_jual_produk_list($filter,$start,$end){
		$date_now=date('Y-m-d');

		$query = "SELECT jproduk_id, jproduk_nobukti, jproduk_nobukti_2, karyawan_no, karyawan_nama,
				CONCAT(cust_nama, ' (', cust_no, ')') as cust_nama_edit, 
				if(k.karyawan_nama is null,cust_nama, concat(cust_nama,' ( ',k.karyawan_nama,')')) as cust_nama, cust_no, cust_note, cust_member,
				member_no, member_valid, jproduk_cust, jproduk_tanggal, jproduk_diskon, jproduk_cashback,
				jproduk_cara, jproduk_cara2, jproduk_cara3, jproduk_bayar, jproduk_grooming, vjp.jproduk_totalbiaya,
				jproduk_keterangan, jproduk_ket_disk, jproduk_stat_dok, jproduk_creator, jproduk_date_create, jproduk_update,
				jproduk_date_update, jproduk_revised, jproduk_post
			FROM vu_jproduk vjp
			    LEFT JOIN karyawan k ON k.karyawan_id = vjp.jproduk_grooming";
		
		
		if ($filter<>""){
			$query .=eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " (jproduk_nobukti LIKE '%".addslashes($filter)."%' OR cust_nama LIKE '%".addslashes($filter)."%' OR cust_no LIKE '%".addslashes($filter)."%' )";
		}
		else {
			$query .= eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " date_format(jproduk_date_create,'%Y-%m-%d')='$date_now' OR (date_format(jproduk_date_create,'%Y-%m-%d')>'2017-01-01' AND jproduk_stat_dok = 'Terbuka')";
		}
		
		$query .= " ORDER BY jproduk_date_create DESC";
		
		$query_nbrows="SELECT jproduk_id, karyawan_no, karyawan_nama FROM master_jual_produk 
						LEFT JOIN customer ON(jproduk_cust=cust_id)
						LEFT JOIN karyawan ON(karyawan.karyawan_id = master_jual_produk .jproduk_grooming)";
		
		if ($filter<>""){
			$query_nbrows .=eregi("WHERE",$query_nbrows)? " AND ":" WHERE ";
			$query_nbrows .= " (jproduk_nobukti LIKE '%".addslashes($filter)."%' OR cust_nama LIKE '%".addslashes($filter)."%' OR cust_no LIKE '%".addslashes($filter)."%' )";
		}
		else {
			$query_nbrows .= eregi("WHERE",$query_nbrows)? " AND ":" WHERE ";
			//TAMPILKAN SEMUA YG MASIH TERBUKA SEJAK AWAL 2017, REQUEST IA
			$query_nbrows .= " date_format(jproduk_date_create,'%Y-%m-%d')='$date_now' OR (date_format(jproduk_date_create,'%Y-%m-%d')>'2017-01-01' AND jproduk_stat_dok = 'Terbuka')";
		}
		
		$result2 = $this->db->query($query_nbrows);
		$nbrows = $result2->num_rows();
		$limit = $query." LIMIT ".$start.",".$end;
		$result = $this->db->query($limit);  
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function master_jual_produk_update($jproduk_id ,$jproduk_nobukti ,$jproduk_cust ,$jproduk_tanggal ,$jproduk_stat_dok ,$jproduk_diskon
									   ,$jproduk_cara ,$jproduk_cara2 ,$jproduk_cara3 ,$jproduk_keterangan ,$jproduk_cashback,$potong_point
									   ,$jproduk_tunai_nilai ,$jproduk_tunai_nilai2 ,$jproduk_tunai_nilai3 ,$jproduk_vgrooming_nilai,$jproduk_voucher_no
									   ,$jproduk_voucher_cashback ,$jproduk_voucher_no2 ,$jproduk_voucher_cashback2 ,$jproduk_voucher_no3
									   ,$jproduk_voucher_cashback3 ,$jproduk_bayar ,$jproduk_subtotal ,$jproduk_total ,$jproduk_hutang
									   ,$jproduk_kwitansi_no ,$jproduk_kwitansi_nama ,$jproduk_kwitansi_nilai ,$jproduk_kwitansi_no2
									   ,$jproduk_kwitansi_nama2 ,$jproduk_kwitansi_nilai2 ,$jproduk_kwitansi_no3 ,$jproduk_kwitansi_nama3
									   ,$jproduk_kwitansi_nilai3 ,$jproduk_card_nama ,$jproduk_card_edc ,$jproduk_card_promo ,$jproduk_card_no ,$jproduk_card_nilai
									   ,$jproduk_card_nama2 ,$jproduk_card_edc2 ,$jproduk_card_promo2 ,$jproduk_card_no2 ,$jproduk_card_nilai2 ,$jproduk_card_nama3
									   ,$jproduk_card_edc3 ,$jproduk_card_promo3 ,$jproduk_card_no3 ,$jproduk_card_nilai3 ,$jproduk_cek_nama ,$jproduk_cek_no
									   ,$jproduk_cek_valid ,$jproduk_cek_bank ,$jproduk_cek_nilai ,$jproduk_cek_nama2 ,$jproduk_cek_no2
									   ,$jproduk_cek_valid2 ,$jproduk_cek_bank2 ,$jproduk_cek_nilai2 ,$jproduk_cek_nama3 ,$jproduk_cek_no3
									   ,$jproduk_cek_valid3 ,$jproduk_cek_bank3 ,$jproduk_cek_nilai3 ,$jproduk_transfer_bank
									   ,$jproduk_transfer_nama ,$jproduk_transfer_nilai ,$jproduk_transfer_bank2 ,$jproduk_transfer_nama2
									   ,$jproduk_transfer_nilai2 ,$jproduk_transfer_bank3 ,$jproduk_transfer_nama3 ,$jproduk_transfer_nilai3
									   ,$cetak_jproduk ,$jproduk_ket_disk
									   ,$array_dproduk_id ,$array_dproduk_produk ,$array_dproduk_satuan
									   ,$array_dproduk_jumlah ,$array_dproduk_harga ,$array_dproduk_diskon_jenis
									   ,$array_dproduk_diskon ,$array_dproduk_karyawan, $jproduk_grooming,$jual_kwitansi_id
									   , $jual_kwitansi_id2, $jual_kwitansi_id3, $dproduk_count, $dcount_dproduk_id,$jproduk_kwitansi_jenis,$jproduk_kwitansi_jenis2,$jproduk_kwitansi_jenis3){
		$date_now = date('Y-m-d');
		$datetime_now=date('Y-m-d H:i:s');
		
		$jproduk_revised=0;
		
		$jenis_transaksi = 'jual_produk';
		$bayar_date_create = $datetime_now;
		
		$sql = "SELECT dproduk_id, dproduk_produk
				FROM detail_jual_produk
					JOIN master_jual_produk ON(dproduk_master=jproduk_id)
				WHERE jproduk_nobukti='".$jproduk_nobukti."'";
		$rs = $this->db->query($sql);
		$rs_rows = $rs->num_rows();
		$rs = $rs->result_array();
		$t_arr = array();

		foreach($rs as $val){
			$t_arr[$val['dproduk_produk']] = $val;
		}
		
		foreach($array_dproduk_produk as $val){
			if(!empty($t_arr[$val]))
				unset($t_arr[$val]);
		}

		if(!empty($t_arr))
			return '-7';

		/*
		if(($dcount_dproduk_id<1) && ($dproduk_count>0) && ($rs_rows<>$dproduk_count)){
			return '-7';
		}
		*/
		
		$sql="SELECT jproduk_revised FROM master_jual_produk WHERE jproduk_id='$jproduk_id'";
		$rs=$this->db->query($sql);
		if($rs->num_rows()){
			$rs_record=$rs->row_array();
			$jproduk_revised=$rs_record["jproduk_revised"];
		}
		
		#cara bayar kuitansi bisa juga adalah cara bayar gift voucher			
		if($jproduk_cara=="kwitansi"){
			if($jproduk_kwitansi_jenis=="gv")
				$jproduk_cara='gift_voucher';
		}
		
		$data = array(
			"jproduk_tanggal"=>$jproduk_tanggal, 
			"jproduk_diskon"=>$jproduk_diskon,
			"jproduk_cashback"=>$jproduk_cashback,
			"potong_point"=>$potong_point,
			"jproduk_bayar"=>$jproduk_bayar,
			"jproduk_totalbiaya"=>$jproduk_total,
			"jproduk_grooming"=>$jproduk_grooming,
			"jproduk_cara"=>$jproduk_cara,
			"jproduk_keterangan"=>$jproduk_keterangan,
			"jproduk_ket_disk"=>$jproduk_ket_disk,
			"jproduk_update"=>@$_SESSION[SESSION_USERID],
			"jproduk_date_update"=>$datetime_now,
			"jproduk_revised"=>$jproduk_revised+1
		);
		
		if($cetak_jproduk==1 || $cetak_jproduk==2){
			$jproduk_nobukti_temp = $jproduk_nobukti;
			$jproduk_format_nobukti  = substr($jproduk_nobukti,0,5);
			$jproduk_tanggal_pattern=strtotime($jproduk_tanggal);

			
			$pattern=date("ymd",$jproduk_tanggal_pattern)."-";
			$jenis=date("ymd",$jproduk_tanggal_pattern);
			$jproduk_nobukti_new=$this->m_public_function->get_kode_kosong('master_jual_produk','jproduk_nobukti','jproduk_nobukti_compare',$pattern,11,$jenis,'jproduk_tanggal',$jproduk_tanggal);
			if($jproduk_nobukti_new==-1){ //jika tidak ada nomor faktur yang lompat, maka generate yang baru
				$pattern=date("ymd",$jproduk_tanggal_pattern)."-";
				$jproduk_nobukti=$this->m_public_function->get_kode_1('master_jual_produk','jproduk_nobukti',$pattern,11);
			}else{	//jika ada nomor faktur yang lompat, pakai nomor yang lompat itu
				$jproduk_nobukti = $jproduk_nobukti_new;
			}	
		
			$data['jproduk_stat_dok'] = 'Tertutup';
			$data['jproduk_nobukti'] = $jproduk_nobukti;	
		}else{
			$data['jproduk_stat_dok'] = 'Terbuka';
		}

		$data["jproduk_date_update"] = $datetime_now;
		
		if($jproduk_cara2!=null){
			if($jproduk_cara2=="kwitansi"){
				if($jproduk_kwitansi_jenis2=="gv")
					$jproduk_cara2='gift_voucher';		
			}
				
			if(($jproduk_kwitansi_nilai2<>'' && $jproduk_kwitansi_nilai2<>0)
			   || ($jproduk_card_nilai2<>'' && $jproduk_card_nilai2<>0)
			   || ($jproduk_cek_nilai2<>'' && $jproduk_cek_nilai2<>0)
			   || ($jproduk_transfer_nilai2<>'' && $jproduk_transfer_nilai2<>0)
			   || ($jproduk_tunai_nilai2<>'' && $jproduk_tunai_nilai2<>0)
			   || ($jproduk_voucher_cashback2<>'' && $jproduk_voucher_cashback2<>0)){
				$data["jproduk_cara2"]=$jproduk_cara2;
			}else{
				$data["jproduk_cara2"]=NULL;
			}
		}
		if($jproduk_cara3!=null){
			if($jproduk_cara3=="kwitansi"){
				if($jproduk_kwitansi_jenis3=="gv")
					$jproduk_cara3='gift_voucher';		
			}
				
			if(($jproduk_kwitansi_nilai3<>'' && $jproduk_kwitansi_nilai3<>0)
			   || ($jproduk_card_nilai3<>'' && $jproduk_card_nilai3<>0)
			   || ($jproduk_cek_nilai3<>'' && $jproduk_cek_nilai3<>0)
			   || ($jproduk_transfer_nilai3<>'' && $jproduk_transfer_nilai3<>0)
			   || ($jproduk_tunai_nilai3<>'' && $jproduk_tunai_nilai3<>0)
			   || ($jproduk_voucher_cashback3<>'' && $jproduk_voucher_cashback3<>0)){
				$data["jproduk_cara3"]=$jproduk_cara3;
			}else{
				$data["jproduk_cara3"]=NULL;
			}
		}
		
		//$this->db->query('LOCK TABLE master_jual_produk WRITE');
		$this->db->where('jproduk_id', $jproduk_id);
		$this->db->update('master_jual_produk', $data);
		$rs = $this->db->affected_rows();
		//$this->db->query('UNLOCK TABLES');
		if($rs>(-1)){
			/*
			if(($cetak_jproduk==1) && ($piutang_total>0)){
				$this->catatan_piutang_update($jproduk_nobukti ,$jproduk_cust ,$jproduk_tanggal ,$piutang_total);
			}*/
			
			$time_now = date('H:i:s');
			$bayar_date_create_temp = $jproduk_tanggal.' '.$time_now;
			$bayar_date_create = date('Y-m-d H:i:s', strtotime($bayar_date_create_temp));
			
			//delete all transaksi
			$sql="delete from jual_kwitansi where jkwitansi_ref='".$jproduk_nobukti."'";
			$this->db->query($sql);
			if($this->db->affected_rows()>-1){
				$sql="delete from jual_card where jcard_ref='".$jproduk_nobukti."'";
				$this->db->query($sql);
				if($this->db->affected_rows()>-1){
					$sql="delete from jual_cek where jcek_ref='".$jproduk_nobukti."'";
					$this->db->query($sql);
					if($this->db->affected_rows()>-1){
						$sql="delete from jual_transfer where jtransfer_ref='".$jproduk_nobukti."'";
						$this->db->query($sql);
						if($this->db->affected_rows()>-1){
							$sql="delete from jual_tunai where jtunai_ref='".$jproduk_nobukti."'";
							$this->db->query($sql);
							if($this->db->affected_rows()>-1){
							$sql="delete from jual_vgrooming where jvgrooming_ref='".$jproduk_nobukti."'";
							$this->db->query($sql);
								if($this->db->affected_rows()>-1){
									$sql="delete from voucher_terima where tvoucher_ref='".$jproduk_nobukti."'";
									$this->db->query($sql);
									if($this->db->affected_rows()>-1){
										$sql="delete from jual_gift_voucher where jgift_voucher_ref='".$jproduk_nobukti."'";
										$this->db->query($sql);
										if($this->db->affected_rows()>-1){
											if($jproduk_cara!=null || $jproduk_cara!=''){
												if($jproduk_kwitansi_nilai<>'' && $jproduk_kwitansi_nilai<>0){
													if($jproduk_kwitansi_jenis=="gv"){
														$result_bayar = $this->m_public_function->cara_bayar_giftvoucher_insert($jproduk_kwitansi_no
																									  ,$jproduk_kwitansi_nilai
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id);
													}else{
														$result_bayar = $this->m_public_function->cara_bayar_kwitansi_insert($jproduk_kwitansi_no
																									  ,$jproduk_kwitansi_nilai
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id);
													}												
												}elseif($jproduk_card_nilai<>'' && $jproduk_card_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_card_insert($jproduk_card_nama
																								  ,$jproduk_card_edc
																								  ,$jproduk_card_promo
																								  ,$jproduk_card_no
																								  ,$jproduk_card_nilai
																								  ,$jproduk_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak_jproduk);
												}elseif($jproduk_cek_nilai<>'' && $jproduk_cek_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_cek_insert($jproduk_cust
																								 ,$jproduk_cek_nama
																								 ,$jproduk_cek_no
																								 ,$jproduk_cek_valid
																								 ,$jproduk_cek_bank
																								 ,$jproduk_cek_nilai
																								 ,$jproduk_nobukti
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak_jproduk);
												}elseif($jproduk_transfer_nilai<>'' && $jproduk_transfer_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_transfer_insert($jproduk_transfer_bank
																									  ,$jproduk_transfer_nama
																									  ,$jproduk_transfer_nilai
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk);
												}elseif($jproduk_tunai_nilai<>'' && $jproduk_tunai_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_tunai_insert($jproduk_tunai_nilai
																								   ,$jproduk_nobukti
																								   ,$bayar_date_create
																								   ,$jenis_transaksi
																								   ,$cetak_jproduk);
												}elseif($jproduk_vgrooming_nilai<>'' && $jproduk_vgrooming_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_vgrooming_insert($jproduk_vgrooming_nilai
																								   ,$jproduk_nobukti
																								   ,$jproduk_grooming
																								   ,$jproduk_tanggal
																								   ,$bayar_date_create
																								   ,$jenis_transaksi
																								   ,$cetak_jproduk);
												}elseif($jproduk_voucher_cashback<>'' && $jproduk_voucher_cashback<>0){
													$result_bayar = $this->m_public_function->cara_bayar_voucher_insert($jproduk_voucher_no
																									 ,$jproduk_nobukti
																									 ,$jproduk_voucher_cashback
																									 ,$bayar_date_create
																									 ,$jenis_transaksi
																									 ,$cetak_jproduk);
												}
											}
											if($jproduk_cara2!=null || $jproduk_cara2!=''){
												if($jproduk_kwitansi_nilai2<>'' && $jproduk_kwitansi_nilai2<>0){
													if($jproduk_kwitansi_jenis2=="gv"){
														$result_bayar2 = $this->m_public_function->cara_bayar_giftvoucher_insert($jproduk_kwitansi_no2
																									  ,$jproduk_kwitansi_nilai2
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id2);
													}else{
														$result_bayar2 = $this->m_public_function->cara_bayar_kwitansi_insert($jproduk_kwitansi_no2
																									  ,$jproduk_kwitansi_nilai2
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id2);
													}
												}elseif($jproduk_card_nilai2<>'' && $jproduk_card_nilai2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_card_insert($jproduk_card_nama2
																								  ,$jproduk_card_edc2
																								  ,$jproduk_card_promo2
																								  ,$jproduk_card_no2
																								  ,$jproduk_card_nilai2
																								  ,$jproduk_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak_jproduk);
												}elseif($jproduk_cek_nilai2<>'' && $jproduk_cek_nilai2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_cek_insert($jproduk_cust
																								 ,$jproduk_cek_nama2
																								 ,$jproduk_cek_no2
																								 ,$jproduk_cek_valid2
																								 ,$jproduk_cek_bank2
																								 ,$jproduk_cek_nilai2
																								 ,$jproduk_nobukti
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak_jproduk);
												}elseif($jproduk_transfer_nilai2<>'' && $jproduk_transfer_nilai2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_transfer_insert($jproduk_transfer_bank2
																									  ,$jproduk_transfer_nama2
																									  ,$jproduk_transfer_nilai2
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk);
												}elseif($jproduk_tunai_nilai2<>'' && $jproduk_tunai_nilai2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_tunai_insert($jproduk_tunai_nilai2
																								   ,$jproduk_nobukti
																								   ,$bayar_date_create
																								   ,$jenis_transaksi
																								   ,$cetak_jproduk);
												}elseif($jproduk_voucher_cashback2<>'' && $jproduk_voucher_cashback2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_voucher_insert($jproduk_voucher_no2
																									 ,$jproduk_nobukti
																									 ,$jproduk_voucher_cashback2
																									 ,$bayar_date_create
																									 ,$jenis_transaksi
																									 ,$cetak_jproduk);
												}
											}
											if($jproduk_cara3!=null || $jproduk_cara3!=''){
												if($jproduk_kwitansi_nilai3<>'' && $jproduk_kwitansi_nilai3<>0){
													if($jproduk_kwitansi_jenis3=="gv"){
														$result_bayar3 = $this->m_public_function->cara_bayar_giftvoucher_insert($jproduk_kwitansi_no3
																									  ,$jproduk_kwitansi_nilai3
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id3);
													}else{
														$result_bayar3 = $this->m_public_function->cara_bayar_kwitansi_insert($jproduk_kwitansi_no3
																									  ,$jproduk_kwitansi_nilai3
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id3);
													}												
												}elseif($jproduk_card_nilai3<>'' && $jproduk_card_nilai3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_card_insert($jproduk_card_nama3
																								  ,$jproduk_card_edc3
																								  ,$jproduk_card_promo3
																								  ,$jproduk_card_no3
																								  ,$jproduk_card_nilai3
																								  ,$jproduk_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak_jproduk);
												}elseif($jproduk_cek_nilai3<>'' && $jproduk_cek_nilai3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_cek_insert($jproduk_cust
																								 ,$jproduk_cek_nama3
																								 ,$jproduk_cek_no3
																								 ,$jproduk_cek_valid3
																								 ,$jproduk_cek_bank3
																								 ,$jproduk_cek_nilai3
																								 ,$jproduk_nobukti
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak_jproduk);
												}elseif($jproduk_transfer_nilai3<>'' && $jproduk_transfer_nilai3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_transfer_insert($jproduk_transfer_bank3
																									  ,$jproduk_transfer_nama3
																									  ,$jproduk_transfer_nilai3
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk);
												}elseif($jproduk_tunai_nilai3<>'' && $jproduk_tunai_nilai3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_tunai_insert($jproduk_tunai_nilai3
																								   ,$jproduk_nobukti
																								   ,$bayar_date_create
																								   ,$jenis_transaksi
																								   ,$cetak_jproduk);
												}elseif($jproduk_voucher_cashback3<>'' && $jproduk_voucher_cashback3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_voucher_insert($jproduk_voucher_no3
																									 ,$jproduk_nobukti
																									 ,$jproduk_voucher_cashback3
																									 ,$bayar_date_create
																									 ,$jenis_transaksi
																									 ,$cetak_jproduk);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			$rs_dproduk_insert = $this->detail_detail_jual_produk_insert($array_dproduk_id ,$jproduk_id ,$array_dproduk_karyawan ,$array_dproduk_produk
																		 ,$array_dproduk_satuan ,$array_dproduk_jumlah ,$array_dproduk_harga
																		 ,$array_dproduk_diskon ,$array_dproduk_diskon_jenis ,$cetak_jproduk, $jproduk_cust);
			
			//INSERT AKTIFITAS_USER
			if($cetak_jproduk==1 || $cetak_jproduk==2){
				$stat_dok = 'Tertutup';
			}else{
				$stat_dok = 'Terbuka';
			}
			$this->m_public_function->aktifitas_user_insert('Penjualan Produk',$jproduk_id,$jproduk_nobukti,'Update Faktur',$stat_dok,'');
			
			return $rs_dproduk_insert;
		}
		else
			return '-1';
	}
	
	function master_jual_produk_create($jproduk_nobukti ,$jproduk_cust ,$jproduk_tanggal ,$jproduk_stat_dok ,$jproduk_diskon
									   ,$jproduk_cara ,$jproduk_cara2 ,$jproduk_cara3 ,$jproduk_keterangan ,$jproduk_cashback,$potong_point
									   ,$jproduk_tunai_nilai ,$jproduk_tunai_nilai2 ,$jproduk_tunai_nilai3 ,$jproduk_vgrooming_nilai,$jproduk_voucher_no
									   ,$jproduk_voucher_cashback ,$jproduk_voucher_no2 ,$jproduk_voucher_cashback2 ,$jproduk_voucher_no3
									   ,$jproduk_voucher_cashback3 ,$jproduk_bayar ,$jproduk_subtotal ,$jproduk_total ,$jproduk_hutang
									   ,$jproduk_kwitansi_no ,$jproduk_kwitansi_nama ,$jproduk_kwitansi_nilai ,$jproduk_kwitansi_no2
									   ,$jproduk_kwitansi_nama2 ,$jproduk_kwitansi_nilai2 ,$jproduk_kwitansi_no3 ,$jproduk_kwitansi_nama3
									   ,$jproduk_kwitansi_nilai3 ,$jproduk_card_nama, $jproduk_card_edc ,$jproduk_card_promo ,$jproduk_card_no ,$jproduk_card_nilai
									   ,$jproduk_card_nama2, $jproduk_card_edc2 ,$jproduk_card_promo2 ,$jproduk_card_no2 ,$jproduk_card_nilai2 ,$jproduk_card_nama3
									   ,$jproduk_card_edc3 ,$jproduk_card_promo3 ,$jproduk_card_no3 ,$jproduk_card_nilai3 ,$jproduk_cek_nama ,$jproduk_cek_no
									   ,$jproduk_cek_valid ,$jproduk_cek_bank ,$jproduk_cek_nilai ,$jproduk_cek_nama2 ,$jproduk_cek_no2
									   ,$jproduk_cek_valid2 ,$jproduk_cek_bank2 ,$jproduk_cek_nilai2 ,$jproduk_cek_nama3 ,$jproduk_cek_no3
									   ,$jproduk_cek_valid3 ,$jproduk_cek_bank3 ,$jproduk_cek_nilai3 ,$jproduk_transfer_bank
									   ,$jproduk_transfer_nama ,$jproduk_transfer_nilai ,$jproduk_transfer_bank2 ,$jproduk_transfer_nama2
									   ,$jproduk_transfer_nilai2 ,$jproduk_transfer_bank3 ,$jproduk_transfer_nama3 ,$jproduk_transfer_nilai3
									   ,$cetak_jproduk ,$jproduk_ket_disk
									   ,$array_dproduk_id ,$array_dproduk_produk ,$array_dproduk_satuan
									   ,$array_dproduk_jumlah ,$array_dproduk_harga ,$array_dproduk_diskon_jenis
									   ,$array_dproduk_diskon ,$array_dproduk_karyawan, $jproduk_grooming, $jual_kwitansi_id, $jual_kwitansi_id2, $jual_kwitansi_id3,$jproduk_kwitansi_jenis,$jproduk_kwitansi_jenis2,$jproduk_kwitansi_jenis3, $app_cust_member_no_baru, $member_ultah_baru, $app_cust_hp_baru, $app_cust_nama_baru){
									   
		$date_now = date('Y-m-d H:i:s');	
		$date_now = date('Y-m-d');
		$datetime_now = date('Y-m-d H:i:s');
		$jenis_transaksi = 'jual_produk';
		
		if($jproduk_cara=="kwitansi"){
			if($jproduk_kwitansi_jenis=="gv")
				$jproduk_cara='gift_voucher';
		}
			
		if($app_cust_nama_baru!=="" && $app_cust_member_no_baru!=='0'){
				$data_cust_baru=array(
				    "cust_no"=>NULL,
				    "cust_nama"=>$app_cust_nama_baru,
				    "cust_hp"=>$app_cust_hp_baru,
				    "cust_member_no"=>$app_cust_member_no_baru,
				    "cust_creator"=>$_SESSION[SESSION_USERID],
				    "cust_date_create"=>$date_now,
				    "cust_aktif"=>'Aktif'
				);
				$this->db->insert('customer', $data_cust_baru);
				if($this->db->affected_rows()){
					//if($app_cust_baru=='false')
					//{
					    if($app_cust_member_no_baru!='0')
						$sql="select cust_id from customer where cust_member_no='".$app_cust_member_no_baru."'";
					    
					    $rs = $this->db->query($sql);
					    $rs_record=$rs->row_array();
					    $app_customer=$rs_record["cust_id"];
					
						$jproduk_cust=$app_customer;
					
				}
		}
		
			$date_now = date('Y-m-d');
			$datetime_now = date('Y-m-d H:i:s');
			$jenis_transaksi = 'jual_produk';
			
			if($jproduk_cara=="kwitansi"){
				if($jproduk_kwitansi_jenis=="gv")
					$jproduk_cara='gift_voucher';
			}
			
			if($jproduk_cust=='UMUM')
				$jproduk_cust = 10;
			
			$data = array(
				"jproduk_nobukti_compare"=>$jproduk_nobukti, 
				"jproduk_cust"=>$jproduk_cust, 
				"jproduk_grooming"=>$jproduk_grooming,
				"jproduk_tanggal"=>$jproduk_tanggal, 
				"jproduk_diskon"=>$jproduk_diskon, 
				"jproduk_cashback"=>$jproduk_cashback,
				"potong_point"=>$potong_point,
				"jproduk_bayar"=>$jproduk_bayar,
				"jproduk_totalbiaya"=>$jproduk_total,
				"jproduk_cara"=>$jproduk_cara,
				"jproduk_keterangan"=>$jproduk_keterangan,
				"jproduk_ket_disk"=>$jproduk_ket_disk,
				"jproduk_creator"=>$_SESSION[SESSION_USERID]
			);
							
			if($cetak_jproduk==1 || $cetak_jproduk==2){
				$data['jproduk_stat_dok'] = 'Tertutup';	
							
			}else{
				$data['jproduk_stat_dok'] = 'Terbuka';
			}
			
			$data["jproduk_date_create"] = $datetime_now;
			
			if($jproduk_cara2!=null){
				if($jproduk_cara2=="kwitansi"){
					if($jproduk_kwitansi_jenis2=="gv")
						$jproduk_cara2='gift_voucher';
				}
				
				if(($jproduk_kwitansi_nilai2<>'' && $jproduk_kwitansi_nilai2<>0)
				   || ($jproduk_card_nilai2<>'' && $jproduk_card_nilai2<>0)
				   || ($jproduk_cek_nilai2<>'' && $jproduk_cek_nilai2<>0)
				   || ($jproduk_transfer_nilai2<>'' && $jproduk_transfer_nilai2<>0)
				   || ($jproduk_tunai_nilai2<>'' && $jproduk_tunai_nilai2<>0)
				   || ($jproduk_voucher_cashback2<>'' && $jproduk_voucher_cashback2<>0)){
					$data["jproduk_cara2"]=$jproduk_cara2;
				}else{
					$data["jproduk_cara2"]=NULL;
				}
			}
			if($jproduk_cara3!=null){
				if($jproduk_cara3=="kwitansi"){
					if($jproduk_kwitansi_jenis3=="gv")
						$jproduk_cara3='gift_voucher';
				}
				
				if(($jproduk_kwitansi_nilai3<>'' && $jproduk_kwitansi_nilai3<>0)
				   || ($jproduk_card_nilai3<>'' && $jproduk_card_nilai3<>0)
				   || ($jproduk_cek_nilai3<>'' && $jproduk_cek_nilai3<>0)
				   || ($jproduk_transfer_nilai3<>'' && $jproduk_transfer_nilai3<>0)
				   || ($jproduk_tunai_nilai3<>'' && $jproduk_tunai_nilai3<>0)
				   || ($jproduk_voucher_cashback3<>'' && $jproduk_voucher_cashback3<>0)){
					$data["jproduk_cara3"]=$jproduk_cara3;
				}else{
					$data["jproduk_cara3"]=NULL;
				}
			}
			
			/*get no_bukti*/
			$jproduk_tanggal_pattern=strtotime($jproduk_tanggal);
			$pattern=date("ymd",$jproduk_tanggal_pattern)."-";
			$jenis=date("ymd",$jproduk_tanggal_pattern);
			$jproduk_nobukti_new=$this->m_public_function->get_kode_kosong('master_jual_produk','jproduk_nobukti','jproduk_nobukti_compare',$pattern,11,$jenis,'jproduk_tanggal',$jproduk_tanggal);
			if($jproduk_nobukti_new==-1){ //jika tidak ada nomor faktur yang lompat, maka generate yang baru
					$pattern=date("ymd",$jproduk_tanggal_pattern)."-";
					$jproduk_nobukti=$this->m_public_function->get_kode_1('master_jual_produk','jproduk_nobukti',$pattern,11);
			}else{	//jika ada nomor faktur yang lompat, pakai nomor yang lompat itu
				$jproduk_nobukti = $jproduk_nobukti_new;
			}				
			/*eos*/

			$data['jproduk_nobukti'] = $jproduk_nobukti;
			$data['jproduk_nobukti_compare'] = $jproduk_nobukti;		
			
			//$this->db->query('LOCK TABLE master_jual_produk WRITE');
			$this->db->insert('master_jual_produk', $data);
			$jproduk_id = $this->db->insert_id();
			$rs = $this->db->affected_rows();
			//$this->db->query('UNLOCK TABLES');		

			if($rs>0){		
				/*
				if(($cetak_jproduk==1) && ($piutang_total>0)){
					$this->catatan_piutang_update($jproduk_nobukti ,$jproduk_cust ,$jproduk_tanggal ,$piutang_total);
				}*/
				
				$time_now = date('H:i:s');
				$bayar_date_create_temp = $jproduk_tanggal.' '.$time_now;
				$bayar_date_create = date('Y-m-d H:i:s', strtotime($bayar_date_create_temp));
				
				//delete all transaksi
				//$sql="delete from jual_kwitansi where jkwitansi_ref='".$jproduk_nobukti."'";
				//$this->db->query($sql);
				if($this->db->affected_rows()>-1){
					$sql="delete from jual_card where jcard_ref='".$jproduk_nobukti."'";
					$this->db->query($sql);
					if($this->db->affected_rows()>-1){
						$sql="delete from jual_cek where jcek_ref='".$jproduk_nobukti."'";
						$this->db->query($sql);
						if($this->db->affected_rows()>-1){
							$sql="delete from jual_transfer where jtransfer_ref='".$jproduk_nobukti."'";
							$this->db->query($sql);
							if($this->db->affected_rows()>-1){
								$sql="delete from jual_tunai where jtunai_ref='".$jproduk_nobukti."'";
								$this->db->query($sql);
								if($this->db->affected_rows()>-1){
									$sql="delete from voucher_terima where tvoucher_ref='".$jproduk_nobukti."'";
									$this->db->query($sql);
									if($this->db->affected_rows()>-1){
										$sql="delete from jual_gift_voucher where jgift_voucher_ref='".$jproduk_nobukti."'";
										$this->db->query($sql);
										if($this->db->affected_rows()>-1){
											if($jproduk_cara!=null || $jproduk_cara!=''){
												if($jproduk_kwitansi_nilai<>'' && $jproduk_kwitansi_nilai<>0){							
													if($jproduk_kwitansi_jenis=="gv"){
														$result_bayar = $this->m_public_function->cara_bayar_giftvoucher_insert($jproduk_kwitansi_no
																								  ,$jproduk_kwitansi_nilai
																								  ,$jproduk_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak_jproduk
																								  ,$jual_kwitansi_id);
													}else{
														$result_bayar = $this->m_public_function->cara_bayar_kwitansi_insert($jproduk_kwitansi_no
																								  ,$jproduk_kwitansi_nilai
																								  ,$jproduk_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak_jproduk
																								  ,$jual_kwitansi_id);
													}
												}elseif($jproduk_card_nilai<>'' && $jproduk_card_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_card_insert($jproduk_card_nama
																								  ,$jproduk_card_edc
																								  ,$jproduk_card_promo
																								  ,$jproduk_card_no
																								  ,$jproduk_card_nilai
																								  ,$jproduk_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak_jproduk);
												}elseif($jproduk_cek_nilai<>'' && $jproduk_cek_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_cek_insert($jproduk_cek_nama
																								 ,$jproduk_cek_no
																								 ,$jproduk_cek_valid
																								 ,$jproduk_cek_bank
																								 ,$jproduk_cek_nilai
																								 ,$jproduk_nobukti
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak_jproduk);
												}elseif($jproduk_transfer_nilai<>'' && $jproduk_transfer_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_transfer_insert($jproduk_transfer_bank
																									  ,$jproduk_transfer_nama
																									  ,$jproduk_transfer_nilai
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk);
												}elseif($jproduk_tunai_nilai<>'' && $jproduk_tunai_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_tunai_insert($jproduk_tunai_nilai
																								   ,$jproduk_nobukti
																								   ,$bayar_date_create
																								   ,$jenis_transaksi
																								   ,$cetak_jproduk);
												}elseif($jproduk_vgrooming_nilai<>'' && $jproduk_vgrooming_nilai<>0){
													$result_bayar = $this->m_public_function->cara_bayar_vgrooming_insert($jproduk_vgrooming_nilai
																								   ,$jproduk_nobukti
																								   ,$jproduk_grooming
																								   ,$jproduk_tanggal	   
																								   ,$bayar_date_create
																								   ,$jenis_transaksi
																								   ,$cetak_jproduk);
												}elseif($jproduk_voucher_cashback<>'' && $jproduk_voucher_cashback<>0){
													$result_bayar = $this->m_public_function->cara_bayar_voucher_insert($jproduk_voucher_no
																									 ,$jproduk_nobukti
																									 ,$jproduk_voucher_cashback
																									 ,$bayar_date_create
																									 ,$jenis_transaksi
																									 ,$cetak_jproduk);
												}
											}
											if($jproduk_cara2!=null || $jproduk_cara2!=''){
												if($jproduk_kwitansi_nilai2<>'' && $jproduk_kwitansi_nilai2<>0){
													if($jproduk_kwitansi_jenis2=="gv"){
														$result_bayar2 = $this->m_public_function->cara_bayar_giftvoucher_insert($jproduk_kwitansi_no2
																									  ,$jproduk_kwitansi_nilai2
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id2);
													}else{
														$result_bayar2 = $this->m_public_function->cara_bayar_kwitansi_insert($jproduk_kwitansi_no2
																									  ,$jproduk_kwitansi_nilai2
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id2);
													}
												}elseif($jproduk_card_nilai2<>'' && $jproduk_card_nilai2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_card_insert($jproduk_card_nama2
																								  ,$jproduk_card_edc2
																								  ,$jproduk_card_promo2
																								  ,$jproduk_card_no2
																								  ,$jproduk_card_nilai2
																								  ,$jproduk_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak_jproduk);
												}elseif($jproduk_cek_nilai2<>'' && $jproduk_cek_nilai2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_cek_insert($jproduk_cek_nama2
																								 ,$jproduk_cek_no2
																								 ,$jproduk_cek_valid2
																								 ,$jproduk_cek_bank2
																								 ,$jproduk_cek_nilai2
																								 ,$jproduk_nobukti
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak_jproduk);
												}elseif($jproduk_transfer_nilai2<>'' && $jproduk_transfer_nilai2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_transfer_insert($jproduk_transfer_bank2
																									  ,$jproduk_transfer_nama2
																									  ,$jproduk_transfer_nilai2
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk);
												}elseif($jproduk_tunai_nilai2<>'' && $jproduk_tunai_nilai2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_tunai_insert($jproduk_tunai_nilai2
																								   ,$jproduk_nobukti
																								   ,$bayar_date_create
																								   ,$jenis_transaksi
																								   ,$cetak_jproduk);
												}elseif($jproduk_voucher_cashback2<>'' && $jproduk_voucher_cashback2<>0){
													$result_bayar2 = $this->m_public_function->cara_bayar_voucher_insert($jproduk_voucher_no2
																									 ,$jproduk_nobukti
																									 ,$jproduk_voucher_cashback2
																									 ,$bayar_date_create
																									 ,$jenis_transaksi
																									 ,$cetak_jproduk);
												}
											}
											if($jproduk_cara3!=null || $jproduk_cara3!=''){
												if($jproduk_kwitansi_nilai3<>'' && $jproduk_kwitansi_nilai3<>0){
													if($jproduk_kwitansi_jenis3=="gv"){
														$result_bayar3 = $this->m_public_function->cara_bayar_giftvoucher_insert($jproduk_kwitansi_no3
																									  ,$jproduk_kwitansi_nilai3
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id3);
													}else{
														$result_bayar3 = $this->m_public_function->cara_bayar_kwitansi_insert($jproduk_kwitansi_no3
																									  ,$jproduk_kwitansi_nilai3
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk
																									  ,$jual_kwitansi_id3);
													}
												}elseif($jproduk_card_nilai3<>'' && $jproduk_card_nilai3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_card_insert($jproduk_card_nama3
																								  ,$jproduk_card_edc3
																								  ,$jproduk_card_promo3
																								  ,$jproduk_card_no3
																								  ,$jproduk_card_nilai3
																								  ,$jproduk_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak_jproduk);
												}elseif($jproduk_cek_nilai3<>'' && $jproduk_cek_nilai3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_cek_insert($jproduk_cek_nama3
																								 ,$jproduk_cek_no3
																								 ,$jproduk_cek_valid3
																								 ,$jproduk_cek_bank3
																								 ,$jproduk_cek_nilai3
																								 ,$jproduk_nobukti
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak_jproduk);
												}elseif($jproduk_transfer_nilai3<>'' && $jproduk_transfer_nilai3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_transfer_insert($jproduk_transfer_bank3
																									  ,$jproduk_transfer_nama3
																									  ,$jproduk_transfer_nilai3
																									  ,$jproduk_nobukti
																									  ,$bayar_date_create
																									  ,$jenis_transaksi
																									  ,$cetak_jproduk);
												}elseif($jproduk_tunai_nilai3<>'' && $jproduk_tunai_nilai3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_tunai_insert($jproduk_tunai_nilai3
																								   ,$jproduk_nobukti
																								   ,$bayar_date_create
																								   ,$jenis_transaksi
																								   ,$cetak_jproduk);
												}elseif($jproduk_voucher_cashback3<>'' && $jproduk_voucher_cashback3<>0){
													$result_bayar3 = $this->m_public_function->cara_bayar_voucher_insert($jproduk_voucher_no3
																									 ,$jproduk_nobukti
																									 ,$jproduk_voucher_cashback3
																									 ,$bayar_date_create
																									 ,$jenis_transaksi
																									 ,$cetak_jproduk);
												}
											}
										}
									}
								}
							}
						}
					}
				}
				
				$rs_dproduk_insert = $this->detail_detail_jual_produk_insert($array_dproduk_id ,$jproduk_id ,$array_dproduk_karyawan ,$array_dproduk_produk
																			 ,$array_dproduk_satuan ,$array_dproduk_jumlah ,$array_dproduk_harga
																			 ,$array_dproduk_diskon ,$array_dproduk_diskon_jenis ,$cetak_jproduk, $jproduk_cust);
				
				//INSERT AKTIFITAS_USER
				if($cetak_jproduk==1 || $cetak_jproduk==2){
					$stat_dok = 'Tertutup';
				}else{
					$stat_dok = 'Terbuka';
				}
				$this->m_public_function->aktifitas_user_insert('Penjualan Produk',$jproduk_id,$jproduk_nobukti,'Create Faktur',$stat_dok,'');
				
				return $rs_dproduk_insert;
			}
			else
				return '-1';
	}
	
	//fcuntion for delete record
	function master_jual_produk_delete($pkid){
		// You could do some checkups here and return '0' or other error consts.
		// Make a single query to delete all of the master_jual_produks at the same time :
		if(sizeof($pkid)<1){
			return '0';
		} else if (sizeof($pkid) == 1){
			$query = "DELETE FROM master_jual_produk WHERE jproduk_id = ".$pkid[0];
			$this->db->query($query);
		} else {
			$query = "DELETE FROM master_jual_produk WHERE ";
			for($i = 0; $i < sizeof($pkid); $i++){
				$query = $query . "jproduk_id= ".$pkid[$i];
				if($i<sizeof($pkid)-1){
					$query = $query . " OR ";
				}     
			}
			$this->db->query($query);
		}
		if($this->db->affected_rows()>0)
			return '1';
		else
			return '0';
	}
	
	//Delete detail_jual_produk
	function detail_jual_produk_delete($dproduk_id){
		$query = "DELETE FROM detail_jual_produk WHERE dproduk_id = ".$dproduk_id;
		$this->db->query($query);
		if($this->db->affected_rows()>0)
			return '1';
		else
			return '0';
	}
	
	function master_jual_produk_batal($jproduk_id, $jproduk_tanggal){
		/*$date = date('Y-m-d');
		$date_1 = '01';
		$date_2 = '02';
		$date_3 = '03';
		$month = substr($jproduk_tanggal,5,2);
		$year = substr($jproduk_tanggal,0,4);
		$begin=mktime(0,0,0,$month,1,$year);
		$nextmonth=strtotime("+1month",$begin);
		
		$month_next = substr(date("Y-m-d",$nextmonth),5,2);
		$year_next = substr(date("Y-m-d",$nextmonth),0,4);
		
		$tanggal_1 = $year_next.'-'.$month_next.'-'.$date_1;
		$tanggal_2 = $year_next.'-'.$month_next.'-'.$date_2;
		$tanggal_3 = $year_next.'-'.$month_next.'-'.$date_3;*/
		$datetime_now = date('Y-m-d H:i:s');
		$sql = "UPDATE master_jual_produk
			SET jproduk_stat_dok='Batal', jproduk_update='".@$_SESSION[SESSION_USERID]."', 
			    jproduk_date_update='".$datetime_now."', jproduk_revised=jproduk_revised+1
			WHERE jproduk_id='".$jproduk_id."' " ;
		
		$this->db->query($sql);
		if($this->db->affected_rows()){
			$result_piutang = $this->catatan_piutang_batal($jproduk_id);
			$result_cara_bayar = $this->cara_bayar_batal($jproduk_id);

			//INSERT AKTIFITAS_USER
			$sqldo='select jproduk_nobukti from master_jual_produk where jproduk_id='.$jproduk_id;
			$res = $this->db->query($sqldo);
			foreach($res->result() as $row){$jproduk_nobukti=$row->jproduk_nobukti;}
			$this->m_public_function->aktifitas_user_insert('Penjualan Produk',$jproduk_id,$jproduk_nobukti,'Update Faktur','Batal','');
							
				/* update eVoucher status batal di TH2 */							
			/*	$evoucher_batal = $this->m_evoucher->devoucher_pakai_batal($evoucher_no, $jproduk_id, 'master_jual_produk');
				if($evoucher_batal == '0') {
					$this->m_evoucher->devoucher_pakai_batal_pending($evoucher_no, $jproduk_id, 'master_jual_produk');
				}
			*/	/* EOf update eVoucher status batal di TH2 */
							
			return '1';
		}else{
			return '0';
		}
	}
	
	//function for advanced search record
	function master_jual_produk_search($jproduk_nobukti, $jproduk_cust, $jproduk_tanggal, $jproduk_tanggal_akhir, $jproduk_diskon, $jproduk_cara, $jproduk_keterangan, $jproduk_stat_dok,$jproduk_sortby, $start, $end){
		
		if($jproduk_sortby==''){
			$jproduk_sortby = 'jproduk_date_create';
		}
		
		$query = "SELECT jproduk_id, jproduk_nobukti, jproduk_nobukti_2,
				CONCAT(cust_nama, ' (', cust_no, ')') as cust_nama_edit, 
				if(karyawan_nama is null,cust_nama, concat(cust_nama,' ( ',karyawan_nama,')')) as cust_nama,
				 cust_no, cust_member, 
				 member_no, member_valid, karyawan.karyawan_no, karyawan.karyawan_nama,
				jproduk_cust, jproduk_tanggal, jproduk_diskon, jproduk_cashback, jproduk_cara, jproduk_cara2,
				jproduk_cara3, jproduk_bayar, vu_jproduk.jproduk_totalbiaya, jproduk_keterangan, jproduk_stat_dok,
				jproduk_creator, jproduk_date_create, jproduk_update, jproduk_date_update, jproduk_revised, jproduk_post,
				jproduk_grooming
			FROM vu_jproduk
			LEFT JOIN karyawan ON(karyawan.karyawan_id = vu_jproduk.jproduk_grooming)";
		
		if($jproduk_nobukti!=''){
			$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
			$query.= " jproduk_nobukti LIKE '%".$jproduk_nobukti."%'";
		};
		if($jproduk_cust!=''){
			$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
			$query.= " jproduk_cust = '".$jproduk_cust."'";
		};
		if($jproduk_tanggal!=''){
			$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
			$query.= " jproduk_tanggal>= '".$jproduk_tanggal."'";
		};
		if($jproduk_tanggal_akhir!=''){
			$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
			$query.= " jproduk_tanggal<= '".$jproduk_tanggal_akhir."'";
		};
/*			if($jproduk_diskon!=''){
			$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
			$query.= " jproduk_diskon LIKE '%".$jproduk_diskon."%'";
		};
*/			if($jproduk_cara!=''){
			$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
			$query.= " jproduk_cara LIKE '%".$jproduk_cara."%'";
		};
		if($jproduk_keterangan!=''){
			$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
			$query.= " jproduk_keterangan LIKE '%".$jproduk_keterangan."%'";
		};
		if($jproduk_stat_dok!=''){
			$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
			$query.= " jproduk_stat_dok LIKE '%".$jproduk_stat_dok."%'";
		};
		
		$query .= " ORDER BY  ".$jproduk_sortby."  DESC";
		
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		
		$limit = $query." LIMIT ".$start.",".$end;		
		$result = $this->db->query($limit);    
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	//function for print record
	function master_jual_produk_print($jproduk_nobukti, $jproduk_cust, $jproduk_tanggal, $jproduk_tanggal_akhir, $jproduk_diskon, $jproduk_cara, $jproduk_keterangan, $jproduk_stat_dok, $option, $filter){
		$jproduk_nobukti_field = "";
		
		//full query
		$query="SELECT jproduk_tanggal
				,jproduk_nobukti
				,jproduk_nobukti_2
				,cust_no
				,cust_nama
				,INSERT(INSERT(member_no,7,0,'-'),14,0,'-') AS no_member
				,vu_jproduk.jproduk_totalbiaya AS jproduk_totalbiaya
				,jproduk_bayar
				,jproduk_keterangan
				,jproduk_stat_dok
			FROM vu_jproduk";
		if($option=='LIST'){
			$query .=eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " (jproduk_nobukti LIKE '%".addslashes($filter)."%' OR cust_nama LIKE '%".addslashes($filter)."%' OR cust_no LIKE '%".addslashes($filter)."%' )";
			$query .= " ORDER BY jproduk_date_create DESC";
			$result = $this->db->query($query);
		} else if($option=='SEARCH'){
			if($jproduk_nobukti!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_nobukti LIKE '%".$jproduk_nobukti."%'";
			};
			if($jproduk_cust!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_cust = '".$jproduk_cust."'";
			};
			if($jproduk_tanggal!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_tanggal>= '".$jproduk_tanggal."'";
			};
			if($jproduk_tanggal_akhir!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_tanggal<= '".$jproduk_tanggal_akhir."'";
			};
			if($jproduk_cara!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_cara LIKE '%".$jproduk_cara."%'";
			};
			if($jproduk_keterangan!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_keterangan LIKE '%".$jproduk_keterangan."%'";
			};
			if($jproduk_stat_dok!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_stat_dok LIKE '%".$jproduk_stat_dok."%'";
			};
			$query .= " ORDER BY jproduk_date_create DESC";
			$result = $this->db->query($query);
		}
		return $result->result();
	}
	
	//function  for export to excel
	function master_jual_produk_export_excel($jproduk_nobukti, $jproduk_cust, $jproduk_tanggal, $jproduk_tanggal_akhir, $jproduk_diskon, $jproduk_cara, $jproduk_keterangan, $jproduk_stat_dok, $option, $filter){
		$jproduk_nobukti_2_field = "";
		
		//full query
		$query="SELECT jproduk_tanggal AS tanggal, date_format(jproduk_date_create, '%H:%i') AS jam
				,jproduk_nobukti AS no_faktur
				$jproduk_nobukti_2_field
				,cust_no AS no_cust
				,cust_nama AS customer
				,INSERT(INSERT(member_no,7,0,'-'),14,0,'-') AS no_member
				,vu_jproduk.jproduk_totalbiaya AS 'Total (Rp)'
				,jproduk_bayar AS 'Total Bayar (Rp)'
				,jproduk_keterangan AS keterangan
				,jproduk_stat_dok AS stat_dok
			FROM vu_jproduk";	
		
		if($option=='LIST'){
			$query .=eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " (jproduk_nobukti LIKE '%".addslashes($filter)."%' OR cust_nama LIKE '%".addslashes($filter)."%' OR cust_no LIKE '%".addslashes($filter)."%' )";
			$query .= " ORDER BY jproduk_date_create DESC";
			$result = $this->db->query($query);
		} else if($option=='SEARCH'){
			if($jproduk_nobukti!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_nobukti LIKE '%".$jproduk_nobukti."%'";
			};
			if($jproduk_cust!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_cust = '".$jproduk_cust."'";
			};
			if($jproduk_tanggal!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_tanggal>= '".$jproduk_tanggal."'";
			};
			if($jproduk_tanggal_akhir!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_tanggal<= '".$jproduk_tanggal_akhir."'";
			};
			if($jproduk_cara!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_cara LIKE '%".$jproduk_cara."%'";
			};
			if($jproduk_keterangan!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_keterangan LIKE '%".$jproduk_keterangan."%'";
			};
			if($jproduk_stat_dok!=''){
				$query.=eregi("WHERE",$query)?" AND ":" WHERE ";
				$query.= " jproduk_stat_dok LIKE '%".$jproduk_stat_dok."%'";
			};
		
			$query .= " ORDER BY jproduk_date_create DESC";
			$result = $this->db->query($query);
		}
		return $result;
	}
	
	function print_paper($jproduk_id){
		$sql="SELECT jproduk_tanggal, cust_member_no, cust_nama, cust_alamat, jproduk_nobukti, jproduk_nobukti_2, produk_nama, produk_kode,
			dproduk_jumlah, satuan_nama, dproduk_harga, dproduk_diskon, cust_preward_total,potong_point,
			(dproduk_harga*((100-dproduk_diskon)/100)) AS jumlah_subtotal, 
			jproduk_creator, jproduk_diskon, jproduk_cashback, jproduk_bayar,
			TIME(jproduk_date_create) AS jproduk_jam,
			IFNULL(karyawan_nama,'NA') AS jproduk_karyawan,
			IFNULL(karyawan_no,'NA') AS jproduk_karyawan_no
		FROM detail_jual_produk 
		    LEFT JOIN master_jual_produk ON(dproduk_master=jproduk_id) 
		    LEFT JOIN customer ON(jproduk_cust=cust_id) 
		    LEFT JOIN produk ON(dproduk_produk=produk_id) 
		    LEFT JOIN satuan ON(dproduk_satuan=satuan_id)
		    LEFT JOIN karyawan ON (jproduk_grooming = karyawan_id)
		WHERE dproduk_master='$jproduk_id' 
		ORDER BY dproduk_diskon ASC";
		$result = $this->db->query($sql);
		return $result;
	}
	
	function print_paper2($jproduk_id){
	
		$sql="SELECT 
			jproduk_tanggal, 
			cust_no, 
			cust_nama, 
			cust_alamat, 
			jproduk_nobukti, 
			jproduk_nobukti_pajak, 
			produk_nama, 
			dproduk_jumlah, 
			satuan_nama, 
			dproduk_harga, 
			dproduk_diskon, 
			(dproduk_harga*((100-dproduk_diskon)/100)) AS jumlah_subtotal, 
			jproduk_creator, 
			jproduk_diskon, 
			jproduk_cashback, 
			jproduk_bayar,
			TIME(jproduk_date_create) AS jproduk_jam,
			IFNULL(karyawan_nama,'NA') AS jproduk_karyawan,
			IFNULL(karyawan_no,'NA') AS jproduk_karyawan_no
		FROM detail_jual_produk 
		LEFT JOIN master_jual_produk ON(dproduk_master=jproduk_id) 
		LEFT JOIN customer ON(jproduk_cust=cust_id) 
		LEFT JOIN produk ON(dproduk_produk=produk_id) 
		LEFT JOIN satuan ON(dproduk_satuan=satuan_id)
		LEFT JOIN karyawan ON (jproduk_grooming = karyawan_id)
		WHERE dproduk_master='$jproduk_id' 
		ORDER BY dproduk_diskon ASC";
		$result = $this->db->query($sql);
		return $result;
	}
	
	function get_cara_bayar($jproduk_id){
		$sql="SELECT jproduk_nobukti, jproduk_cara FROM master_jual_produk WHERE jproduk_id='$jproduk_id'";
		$rs=$this->db->query($sql);
		if($rs->num_rows()){
			$record=$rs->row_array();
			$sql = "SELECT cek ,card ,kuitansi ,transfer ,tunai FROM vu_trans_produk WHERE no_bukti='".$record['jproduk_nobukti']."'";
			$rs = $this->db->query($sql);
			if($rs->num_rows()){
				return $rs->result();
			}else{
				return '';
			}
		}else{
			return '';
		}
		
	}
	
	function cara_bayar($jproduk_id){
		$sql="SELECT jproduk_nobukti, jproduk_cara FROM master_jual_produk WHERE jproduk_id='$jproduk_id'";
		$rs=$this->db->query($sql);
		if($rs->num_rows()){
			$record=$rs->row();
			$jproduk_nobukti = $record->jproduk_nobukti;
			if(($record->jproduk_cara !== NULL || $record->jproduk_cara !== '')){
				if($record->jproduk_cara == 'tunai'){
					$sql = "SELECT jtunai_id FROM jual_tunai WHERE jtunai_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					
					$sql="SELECT jproduk_nobukti, jproduk_cara, jtunai_nilai AS bayar_nilai FROM master_jual_produk LEFT JOIN jual_tunai ON(jtunai_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
					$rs=$this->db->query($sql);
					if($rs->num_rows()){
						return $rs->row();
					}else{
						return NULL;
					}
				}elseif($record->jproduk_cara == 'vgrooming'){
					$sql = "SELECT jvgrooming_id FROM jual_vgrooming WHERE jvgrooming_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					
					$sql="SELECT jproduk_nobukti, jproduk_cara, jvgrooming_nilai AS bayar_nilai FROM master_jual_produk LEFT JOIN jual_vgrooming ON(jvgrooming_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
					$rs=$this->db->query($sql);
					if($rs->num_rows()){
						return $rs->row();
					}else{
						return NULL;
					}
				}elseif($record->jproduk_cara == 'kwitansi'){
					$sql = "SELECT jkwitansi_id FROM jual_kwitansi WHERE jkwitansi_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					
					$sql="SELECT jproduk_nobukti, jproduk_cara, jkwitansi_nilai AS bayar_nilai FROM master_jual_produk LEFT JOIN jual_kwitansi ON(jkwitansi_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
					$rs=$this->db->query($sql);
					if($rs->num_rows()){
						return $rs->row();
					}else{
						return NULL;
					}
				}elseif($record->jproduk_cara == 'gift_voucher'){
                    $sql = "SELECT jgift_voucher_id FROM jual_gift_voucher WHERE jgift_voucher_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
                    
                    $sql="SELECT jproduk_nobukti, jproduk_cara, jgift_voucher_nilai AS bayar_nilai FROM master_jual_produk LEFT JOIN jual_gift_voucher ON(jgift_voucher_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
                    $rs=$this->db->query($sql);
                    if($rs->num_rows()){
                        return $rs->row();
                    }else{
                        return NULL;
                    }
				}elseif($record->jproduk_cara == 'card'){
					$sql = "SELECT jcard_id FROM jual_card WHERE jcard_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					
					$sql="SELECT jproduk_nobukti, jproduk_cara, jcard_nilai AS bayar_nilai FROM master_jual_produk LEFT JOIN jual_card ON(jcard_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
					$rs=$this->db->query($sql);
					if($rs->num_rows()){
						return $rs->row();
					}else{
						return NULL;
					}
				}elseif($record->jproduk_cara == 'cek/giro'){
					$sql = "SELECT jcek_id FROM jual_cek WHERE jcek_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					
					$sql="SELECT jproduk_nobukti, jproduk_cara, jcek_nilai AS bayar_nilai FROM master_jual_produk LEFT JOIN jual_cek ON(jcek_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
					$rs=$this->db->query($sql);
					if($rs->num_rows()){
						return $rs->row();
					}else{
						return NULL;
					}
				}elseif($record->jproduk_cara == 'transfer'){
					$sql = "SELECT jtransfer_id FROM jual_transfer WHERE jtransfer_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					
					$sql="SELECT jproduk_nobukti, jproduk_cara, jtransfer_nilai AS bayar_nilai FROM master_jual_produk LEFT JOIN jual_transfer ON(jtransfer_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
					$rs=$this->db->query($sql);
					if($rs->num_rows()){
						return $rs->row();
					}else{
						return NULL;
					}
				}elseif($record->jproduk_cara == 'voucher'){
					$sql = "SELECT tvoucher_id FROM voucher_terima WHERE tvoucher_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					
					$sql="SELECT jproduk_nobukti, jproduk_cara, tvoucher_nilai AS bayar_nilai FROM master_jual_produk LEFT JOIN voucher_terima ON(tvoucher_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
					$rs=$this->db->query($sql);
					if($rs->num_rows()){
						return $rs->row();
					}else{
						return NULL;
					}
				}
			}else{
				return NULL;
			}
		}else{
			return NULL;
		}
	}
	
	function cara_bayar2($jproduk_id){
		$sql="SELECT jproduk_nobukti, jproduk_cara, jproduk_cara2 FROM master_jual_produk WHERE jproduk_id='$jproduk_id'";
		$rs=$this->db->query($sql);
		if($rs->num_rows()){
			$record=$rs->row();
			$jproduk_nobukti = $record->jproduk_nobukti;
			if(($record->jproduk_cara2 !== NULL || $record->jproduk_cara2 !== '')){
				if($record->jproduk_cara2 == 'tunai'){
					$sql = "SELECT jtunai_id FROM jual_tunai WHERE jtunai_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara2, jtunai_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_tunai ON(jtunai_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()>=2){
						if($record->jproduk_cara == 'tunai')
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jtunai_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_tunai ON(jtunai_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						else
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jtunai_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_tunai ON(jtunai_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara2 == 'kwitansi'){
					$sql = "SELECT jkwitansi_id FROM jual_kwitansi WHERE jkwitansi_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara2, jkwitansi_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_kwitansi ON(jkwitansi_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()>=2){
						if($record->jproduk_cara == 'kwitansi')
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jkwitansi_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_kwitansi ON(jkwitansi_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						else	
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jkwitansi_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_kwitansi ON(jkwitansi_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara2 == 'card'){
					$sql = "SELECT jcard_id FROM jual_card WHERE jcard_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara2, jcard_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_card ON(jcard_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()>=2){
						if($record->jproduk_cara == 'card')
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jcard_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_card ON(jcard_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						else	
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jcard_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_card ON(jcard_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara2 == 'cek/giro'){
					$sql = "SELECT jcek_id FROM jual_cek WHERE jcek_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara2, jcek_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_cek ON(jcek_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()>=2){
						if($record->jproduk_cara == 'cek/giro')
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jcek_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_cek ON(jcek_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						else
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jcek_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_cek ON(jcek_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara2 == 'transfer'){
					$sql = "SELECT jtransfer_id FROM jual_transfer WHERE jtransfer_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara2, jtransfer_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_transfer ON(jtransfer_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()>=2){
						if($record->jproduk_cara == 'transfer')
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jtransfer_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_transfer ON(jtransfer_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						else	
							$sql="SELECT jproduk_nobukti, jproduk_cara2, jtransfer_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN jual_transfer ON(jtransfer_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara2 == 'voucher'){
					$sql = "SELECT tvoucher_id FROM voucher_terima WHERE tvoucher_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara2, tvoucher_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN voucher_terima ON(tvoucher_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()>=2){
						if($record->jproduk_cara == 'voucher')
							$sql="SELECT jproduk_nobukti, jproduk_cara2, tvoucher_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN voucher_terima ON(tvoucher_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						else	
							$sql="SELECT jproduk_nobukti, jproduk_cara2, tvoucher_nilai AS bayar2_nilai FROM master_jual_produk LEFT JOIN voucher_terima ON(tvoucher_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 0,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}
			}else{
				return NULL;
			}
		}else{
			return NULL;
		}
	}
	
	function cara_bayar3($jproduk_id){
		$sql="SELECT jproduk_nobukti, jproduk_cara3 FROM master_jual_produk WHERE jproduk_id='$jproduk_id'";
		$rs=$this->db->query($sql);
		if($rs->num_rows()){
			$record=$rs->row();
			$jproduk_nobukti = $record->jproduk_nobukti;
			if(($record->jproduk_cara3 !== NULL || $record->jproduk_cara3 !== '')){
				if($record->jproduk_cara3 == 'tunai'){
					$sql = "SELECT jtunai_id FROM jual_tunai WHERE jtunai_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jtunai_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_tunai ON(jtunai_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==2){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jtunai_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_tunai ON(jtunai_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==3){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jtunai_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_tunai ON(jtunai_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 2,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara3 == 'kwitansi'){
					$sql = "SELECT jkwitansi_id FROM jual_kwitansi WHERE jkwitansi_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jkwitansi_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_kwitansi ON(jkwitansi_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==2){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jkwitansi_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_kwitansi ON(jkwitansi_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==3){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jkwitansi_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_kwitansi ON(jkwitansi_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 2,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara3 == 'card'){
					$sql = "SELECT jcard_id FROM jual_card WHERE jcard_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jcard_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_card ON(jcard_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==2){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jcard_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_card ON(jcard_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==3){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jcard_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_card ON(jcard_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 2,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara3 == 'cek/giro'){
					$sql = "SELECT jcek_id FROM jual_cek WHERE jcek_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jcek_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_cek ON(jcek_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==2){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jcek_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_cek ON(jcek_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==3){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jcek_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_cek ON(jcek_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 2,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara3 == 'transfer'){
					$sql = "SELECT jtransfer_id FROM jual_transfer WHERE jtransfer_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jtransfer_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_transfer ON(jtransfer_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==2){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jtransfer_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_transfer ON(jtransfer_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==3){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, jtransfer_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN jual_transfer ON(jtransfer_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 2,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}elseif($record->jproduk_cara3 == 'voucher'){
					$sql = "SELECT tvoucher_id FROM voucher_terima WHERE tvoucher_ref='".$jproduk_nobukti."'";
					$rs = $this->db->query($sql);
					if($rs->num_rows()==1){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, tvoucher_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN voucher_terima ON(tvoucher_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id'";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==2){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, tvoucher_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN voucher_terima ON(tvoucher_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 1,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}else if($rs->num_rows()==3){
						$sql="SELECT jproduk_nobukti, jproduk_cara3, tvoucher_nilai AS bayar3_nilai FROM master_jual_produk LEFT JOIN voucher_terima ON(tvoucher_ref=jproduk_nobukti) WHERE jproduk_id='$jproduk_id' LIMIT 2,1";
						$rs=$this->db->query($sql);
						if($rs->num_rows()){
							return $rs->row();
						}else{
							return NULL;
						}
					}
				}
			}else{
				return NULL;
			}
		}else{
			return NULL;
		}
	}
	
	function iklan(){
		$sql="SELECT * from iklan_today";
		$result = $this->db->query($sql);
		return $result;
	}
	
}
?>