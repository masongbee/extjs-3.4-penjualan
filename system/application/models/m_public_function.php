<?

class M_public_function extends Model{	
	
	var $key_encryption = "asd";
	var $percent_point_reward = 1;
    
	function get_user_list_cc($query,$start,$end,$menu){
		$karyawan_id	= $_SESSION[SESSION_KARYAWAN];
		$cabang_value	= $this->m_public_function->get_cabang_value();
		$sql_search = "";
		$db_online = $this->m_public_function->get_db_online_name();

		if($query<>"")
			$sql_search = " AND (u.user_name LIKE '%".addslashes($query)."%') ";

		if(eregi('H',$this->m_security->get_access_group_by_kode('MENU_COURTESY'))){ /*jika memiliki hak akses, dapat melihat semua user*/
			if($menu=='cc'){
				$semua = "UNION ALL
					select '-1', '[Semua]', '[Semua]'";
			}else{
				$semua = "";
			}
			
			$sql = 
			   "SELECT u.user_karyawan as karyawan_id, u.user_name as karyawan_username, u.user_name as karyawan_nama
				FROM $db_online.users u
				WHERE 
					u.user_aktif='Aktif' 
					AND (u.user_groups IN (6,35,46,32,81))
					AND (substring(u.user_cabang, $cabang_value, 1) = '1')
					$sql_search
				UNION ALL
					select '0', '[Umum]', '[Umum]'
				$semua ";		
				
		}else{ /*jika tdk memiliki hak akses, user hanya dapat melihat miliknya sendiri dan user [Umum]*/
							
			$sql = 
			   "SELECT u.user_karyawan as karyawan_id, u.user_name as karyawan_username, u.user_name as karyawan_nama
				FROM $db_online.users u
				WHERE 
					u.user_aktif ='Aktif' 
					AND (u.user_karyawan = '".$karyawan_id."')
					AND (substring(u.user_cabang, $cabang_value, 1) = '1')
				UNION ALL
					select '0', '[Umum]', '[Umum]'";	
		}

		$sql .= " order by karyawan_username asc";
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		if(($end!=0)){
			$limit = $sql." LIMIT ".$start.",".$end;			
			$result = $this->db->query($limit);
		}
		if($nbrows>0){
			foreach($result->result() as $row)
				$arr[] = $row;

			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} 
		else 
			return '({"total":"0", "results":""})';			
	}		
		
	function get_satuan_detail_list($master_id){
		$sql="SELECT satuan_id,satuan_kode,satuan_nama FROM satuan";
		if($master_id<>""){
			$sql.=" WHERE satuan_id IN(SELECT dterima_satuan FROM detail_terima_beli WHERE dterima_master='".$master_id."')";
			$sql.=" OR satuan_id IN(SELECT dtbonus_satuan FROM detail_terima_bonus WHERE dtbonus_master='".$master_id."')";
		}
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_satuan_produk_list($selected_id){
		
		$sql="SELECT satuan_id,satuan_kode,satuan_nama FROM vu_satuan_konversi WHERE produk_aktif='Aktif'";
		
		if($selected_id!==""){
			$sql.=(eregi("WHERE",$sql)?" AND ":" WHERE ")." produk_id='".$selected_id."'";
		}
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_satuan_selected_list($selected_id){
		$sql="SELECT satuan_id,satuan_kode,satuan_nama FROM satuan";
		if($selected_id!=="")
		{
			$selected_id=substr($selected_id,0,strlen($selected_id)-1);
			$sql.=" WHERE satuan_id IN(".$selected_id.")";
		}

		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}

	function M_public_function(){
		parent::Model();
	}
	
	function encrypt($string){
	    $key = $this->key_encryption;	    
	    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));	    
	}
	
	function decrypt($encrypted){
	    $key = $this->key_encryption;
	    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	}
	
	function get_percent_point_reward(){
	    return $this->percent_point_reward;
	}
	
	function get_db_online_name(){
		$this->cabang = $this->load->database("ONLINEDBLOCAL", TRUE);
		return $this->cabang->database;
	}
	
	function get_cabang_brand(){
		$sql="select info_brand from info";
		$result=$this->db->query($sql);
		if($result->num_rows()){
			$data=$result->row();
			return $data->info_brand;
		}else{
			return '';
		}
	}
	
	function get_cabang_value(){
		$sql = "select c.cabang_value
				from info i
				LEFT JOIN cabang c ON c.cabang_id = i.info_cabang";
		$result=$this->db->query($sql);
		if($result->num_rows()){
			$data=$result->row();
			return $data->cabang_value;
		}else{
			return '';
		}
	}
	
	function cek_userpass($username, $password, $menu_id){
		$username = strtolower($username);
		$password = md5($password);
		
		$db_online=$this->get_db_online_name();
		
		$sql="select 
				user_name, user_passwd, vu_permissions.menu_id 
			from $db_online.users 
			LEFT JOIN vu_permissions ON users.user_groups=vu_permissions.perm_group
			where 
				perm_print='1' and menu_id=$menu_id
			and substr(user_cabang, (select cabang_value from info LEFT JOIN cabang ON(cabang_id = info_Cabang)),1)='1'
			and users.user_aktif='Aktif'
			and user_name='$username' 
			and user_passwd='$password'";
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		if($nbrows>0){
			return '1';
		}else{
			return '0';
		}
	}
	
	function add_date($time='',$value=0,$interval='day'){
		$time=mysql_to_unix($time);
		$year=date("Y",$time);
		$month=date("m",$time);
		$day=date("d",$time);
		$in_date=$year."-".$month."-".$day;
		$sql="SELECT DATE_ADD('$in_date', INTERVAL $value $interval) AS tanggal ";
		$result=$this->db->query($sql);
		if($result->num_rows()){
			$data=$result->row();
			return $data->tanggal;
		}else{
			return date('Y/m/d');
		}
	}

	function get_permission2($id, $menu){ //untuk permission Lain-lain 2
		$query = "select perm_group from vu_permissions where perm_ps = 1 and menu_id = ".$menu." and perm_group = ".$id."";				
		
		$result = $this->db->query($query);		
	$nbrows = $result->num_rows();
	return $nbrows;
	}
	
	function get_permission5($id, $menu){ //untuk permission Lain-lain 5
		$query = "select perm_group from vu_permissions where perm_unpost = 1 and menu_id = ".$menu." and perm_group = ".$id."";				
		$result = $this->db->query($query);		
	$nbrows = $result->num_rows();
	return $nbrows;
	}
	
	function penguncian_tgl_trans_setting($group_id,$menu){
		$query = "select perm_group from vu_permissions where perm_harga = 1 and menu_id = ".$menu." and perm_group = ".$group_id."";	
					
		$result = $this->db->query($query);		
		$nbrows = $result->num_rows();		
		if($nbrows==0){		//jika user yg login, permission 'lain-lain'nya dicentang 
			$query_tgl = "select trans_kunci_tgl from transaksi_setting;";	
			$result_tgl=$this->db->query($query_tgl);
			if($result_tgl->num_rows()){
				$data=$result_tgl->row();
				$nbrows = $data->trans_kunci_tgl;
			}					
		}			
		return $nbrows;
	}

	function get_permission_3bulan($group_id,$menu){ //untuk permission Lain-lain2
		if($menu==86 || $menu==448){ //jika lap penerimaan kas/lap jual produk semua referal, centangannya ada di kolom lain-lain
			$query = "select perm_group from vu_permissions where perm_print = 1 and menu_id = ".$menu." and perm_group = ".$group_id."";				
		}else{	//selain itu, centangannya ada di kolom lain-lain2
			$query = "select perm_group from vu_permissions where perm_harga = 1 and menu_id = ".$menu." and perm_group = ".$group_id."";	
		}	
		
		$result = $this->db->query($query);		
		$nbrows = $result->num_rows();		
		if($nbrows==0){		//jika user yg login, permission 'lain-lain'nya dicentang 
			//$query_tgl = "SELECT date_format(setup_periode_awal,'%Y-%m-%d') AS tanggal from akun_setup ";	
			$query_tgl = "select date_format(DATE_ADD(CURDATE(), INTERVAL -3 MONTH),'%Y-%m') AS tanggal;";	
			$result_tgl=$this->db->query($query_tgl);
			if($result_tgl->num_rows()){
				$data=$result_tgl->row();
				$nbrows = $data->tanggal;
				$nbrows = $nbrows.'-01';
			}					
		}			
		return $nbrows;
	}
	
	function get_permission($group_id,$menu){ //untuk permission Lain-lain
		$query = "select perm_group from vu_permissions where perm_harga = 1 and menu_id = ".$menu." and perm_group = ".$group_id."";				
		
		$result = $this->db->query($query);		
		$nbrows = $result->num_rows();		
		if($nbrows==0){		//jika user yg login, permission 'lain-lain'nya dicentang 
			$query_tgl = "SELECT date_format(setup_periode_awal,'%Y-%m-%d') AS tanggal from akun_setup ";	
			$result_tgl=$this->db->query($query_tgl);
			if($result_tgl->num_rows()){
				$data=$result_tgl->row();
				$nbrows = $data->tanggal;	
			}					
		}	
			
		return $nbrows;
	}
	
	function get_tahun_akun_setup(){
		$query	= "SELECT setup_periode_tahun FROM akun_setup";	
		$result	= $this->db->query($query);
		if($result->num_rows()){
			$data	= $result->row();
			$nbrows = $data->setup_periode_tahun;
		}								
			
		return $nbrows;
	}

	function get_gudang_nama($gudang_id){
		$sql="SELECT gudang_nama FROM gudang WHERE gudang_id='".$gudang_id."'";
		$query = $this->db->query($sql);
		if($query->num_rows())
		{
			$row=$query->row();
			return $row->gudang_nama;
		}else
			return 0;
	}
	
	function get_produk_nama($produk_id){
		$sql="SELECT produk_nama FROM produk WHERE produk_id='".$produk_id."'";
		$query = $this->db->query($sql);
		if($query->num_rows())
		{
			$row=$query->row();
			return $row->produk_nama;
		}else
			return 0;
	}
	
	function get_satuan_nama($satuan_id){
		$sql="SELECT satuan_nama FROM satuan WHERE satuan_id='".$satuan_id."'";
		$query = $this->db->query($sql);
		if($query->num_rows())
		{
			$row=$query->row();
			return $row->satuan_nama;
		}else
			return 0;
	}
	
	function get_customer_nama($cust_id){
		$sql="SELECT cust_nama, cust_title, cust_kelamin FROM customer WHERE cust_id='".$cust_id."'";
		$query = $this->db->query($sql);
		if($query->num_rows())
		{
			$row=$query->row();
			return $row->cust_nama.'<>'.$row->cust_kelamin.'<>'.$row->cust_title;
		}else
			return 0;
	}
	
	function get_bank_list(){
		$sql="SELECT * FROM bank_master WHERE mbank_aktif='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_group1_list($query,$start,$end){
		$sql="SELECT * FROM produk_group WHERE group_aktif='Aktif'";

		if($query<>""){
			$sql .=eregi("WHERE",$sql)? " AND ":" WHERE ";
			$sql .= " (group_nama LIKE '%".addslashes($query)."%' OR group_kode LIKE '%".addslashes($query)."%')";
		}
		
		$result = $this->db->query($sql);
		
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit); 
	
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_usergroups_list(){
		$sql="SELECT group_id,group_name FROM usergroups WHERE group_active='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_mbank_list(){
		$sql="SELECT mbank_id,mbank_nama FROM bank_master WHERE mbank_aktif='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_propinsi_list(){
		$sql="SELECT propinsi_nama FROM propinsi";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_info(){
		$sql="SELECT i.*, c.cabang_kode FROM info i INNER JOIN cabang c ON i.info_cabang = c.cabang_id";
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function get_evoucher_list($query,$start,$end){
			$query = "SELECT voucher_id, voucher_jenis, voucher_nama, voucher_point, voucher_kadaluarsa, voucher_cashback, voucher_mincash, voucher_diskon, voucher_promo, voucher_allproduk, voucher_allrawat FROM voucher LEFT JOIN voucher_kupon ON(kvoucher_master=voucher_id) WHERE voucher_jenis='reward' AND kvoucher_cust=0 GROUP BY voucher_id";
			$result = $this->db->query($query);
			$nbrows = $result->num_rows();
			$limit = $query." LIMIT ".$start.",".$end;			
			$result = $this->db->query($limit); 
		
			if($nbrows>0){
				foreach($result->result() AS $row){
					$arr[] = $row;
				}
				$jsonresult = json_encode($arr);
				return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
			} else {
				return '({"total":"0", "results":""})';
			}
	}
		
	function get_kwitansi_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql = "SELECT jkwitansi_id FROM jual_kwitansi WHERE jkwitansi_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT jkwitansi_id
						,kwitansi_no
						,jkwitansi_nilai
						,cust_nama
						,kwitansi_sisa
						,kwitansi_id
					FROM jual_kwitansi
					LEFT JOIN cetak_kwitansi ON(jkwitansi_master=kwitansi_id)
					LEFT JOIN customer ON(kwitansi_cust=cust_id)
					WHERE jkwitansi_ref='".$ref_id."'
					ORDER BY jkwitansi_id ASC
					LIMIT 0,1";
			}else{
				$sql="SELECT jkwitansi_id
						,kwitansi_no
						,jkwitansi_nilai
						,cust_nama
						,kwitansi_sisa
						,kwitansi_id
					FROM jual_kwitansi
					LEFT JOIN cetak_kwitansi ON(jkwitansi_master=kwitansi_id)
					LEFT JOIN customer ON(kwitansi_cust=cust_id)
					WHERE jkwitansi_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				if($cara_bayar_1 == "kwitansi")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jkwitansi_id
						,kwitansi_no
						,jkwitansi_nilai
						,cust_nama
						,kwitansi_sisa
						,kwitansi_id
					FROM jual_kwitansi
					LEFT JOIN cetak_kwitansi ON(jkwitansi_master=kwitansi_id)
					LEFT JOIN customer ON(kwitansi_cust=cust_id)
					WHERE jkwitansi_ref='".$ref_id."'
					ORDER BY jkwitansi_id ASC
					LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jkwitansi_id
						,kwitansi_no
						,jkwitansi_nilai
						,cust_nama
						,kwitansi_sisa
						,kwitansi_id
					FROM jual_kwitansi
					LEFT JOIN cetak_kwitansi ON(jkwitansi_master=kwitansi_id)
					LEFT JOIN customer ON(kwitansi_cust=cust_id)
					WHERE jkwitansi_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				if($cara_bayar_1 == "kwitansi" or $cara_bayar_2 == "kwitansi")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jkwitansi_id
						,kwitansi_no
						,jkwitansi_nilai
						,cust_nama
						,kwitansi_sisa
						,kwitansi_id
					FROM jual_kwitansi
					LEFT JOIN cetak_kwitansi ON(jkwitansi_master=kwitansi_id)
					LEFT JOIN customer ON(kwitansi_cust=cust_id)
					WHERE jkwitansi_ref='".$ref_id."'
					ORDER BY jkwitansi_id ASC
					LIMIT ".$limit.",1";
			}elseif($nbrows==3){
				if($cara_bayar_1 == "kwitansi" and $cara_bayar_2 == "kwitansi")
					$limit = 2;
				else
					$limit = 1;

				$sql="SELECT jkwitansi_id
						,kwitansi_no
						,jkwitansi_nilai
						,cust_nama
						,kwitansi_sisa
						,kwitansi_id
					FROM jual_kwitansi
					LEFT JOIN cetak_kwitansi ON(jkwitansi_master=kwitansi_id)
					LEFT JOIN customer ON(kwitansi_cust=cust_id)
					WHERE jkwitansi_ref='".$ref_id."'
					ORDER BY jkwitansi_id ASC
					LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jkwitansi_id
						,kwitansi_no
						,jkwitansi_nilai
						,cust_nama
						,kwitansi_sisa
						,kwitansi_id
					FROM jual_kwitansi
					LEFT JOIN cetak_kwitansi ON(jkwitansi_master=kwitansi_id)
					LEFT JOIN customer ON(kwitansi_cust=cust_id)
					WHERE jkwitansi_ref='".$ref_id."'";
			}
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_deposit_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql = "SELECT jdeposit_beli_id FROM jual_deposit_beli WHERE jdeposit_beli_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT jdeposit_beli_id
						,deposit_beli_no
						,jdeposit_beli_nilai
						,supplier_nama
						,deposit_beli_sisa
						,deposit_beli_id
					FROM jual_deposit_beli
					LEFT JOIN cetak_deposit_beli ON(jdeposit_beli_master=deposit_beli_id)
					LEFT JOIN supplier ON(deposit_beli_supplier=supplier_id)
					WHERE jdeposit_beli_ref='".$ref_id."'
					ORDER BY jdeposit_beli_id ASC
					LIMIT 0,1";
			}else{
				$sql="SELECT jdeposit_beli_id
						,deposit_beli_no
						,jdeposit_beli_nilai
						,supplier_nama
						,deposit_beli_sisa
						,deposit_beli_id
					FROM jual_deposit_beli
					LEFT JOIN cetak_deposit_beli ON(jdeposit_beli_master=deposit_beli_id)
					LEFT JOIN supplier ON(deposit_beli_supplier=supplier_id)
					WHERE jdeposit_beli_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				if($cara_bayar_1 == "kwitansi")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jdeposit_beli_id
						,deposit_beli_no
						,jdeposit_beli_nilai
						,cust_nama
						,deposit_beli_sisa
						,deposit_beli_id
					FROM jual_deposit_beli
					LEFT JOIN cetak_deposit_beli ON(jdeposit_beli_master=deposit_beli_id)
					LEFT JOIN supplier ON(deposit_beli_supplier=supplier_id)
					WHERE jdeposit_beli_ref='".$ref_id."'
					ORDER BY jdeposit_beli_id ASC
					LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jdeposit_beli_id
						,deposit_beli_no
						,jdeposit_beli_nilai
						,supplier_nama
						,deposit_beli_sisa
						,deposit_beli_id
					FROM jual_deposit_beli
					LEFT JOIN cetak_deposit_beli ON(jdeposit_beli_master=deposit_beli_id)
					LEFT JOIN supplier ON(deposit_beli_supplier=supplier_id)
					WHERE jdeposit_beli_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				if($cara_bayar_1 == "kwitansi" or $cara_bayar_2 == "kwitansi")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jdeposit_beli_id
						,deposit_beli_no
						,jdeposit_beli_nilai
						,supplier_nama
						,deposit_beli_sisa
						,deposit_beli_id
					FROM jual_deposit_beli
					LEFT JOIN cetak_deposit_beli ON(jdeposit_beli_master=deposit_beli_id)
					LEFT JOIN supplier ON(deposit_beli_supplier=supplier_id)
					WHERE jdeposit_beli_ref='".$ref_id."'
					ORDER BY jdeposit_beli_id ASC
					LIMIT ".$limit.",1";
			}elseif($nbrows==3){
				if($cara_bayar_1 == "kwitansi" and $cara_bayar_2 == "kwitansi")
					$limit = 2;
				else
					$limit = 1;

				$sql="SELECT jdeposit_beli_id
						,deposit_beli_no
						,jdeposit_beli_nilai
						,supplier_nama
						,deposit_beli_sisa
						,deposit_beli_id
					FROM jual_deposit_beli
					LEFT JOIN cetak_deposit_beli ON(jdeposit_beli_master=deposit_beli_id)
					LEFT JOIN supplier ON(deposit_beli_supplier=supplier_id)
					WHERE jdeposit_beli_ref='".$ref_id."'
					ORDER BY jdeposit_beli_id ASC
					LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jdeposit_beli_id
						,deposit_beli_no
						,jdeposit_beli_nilai
						,supplier_nama
						,deposit_beli_sisa
						,deposit_beli_id
					FROM jual_deposit_beli
					LEFT JOIN cetak_deposit_beli ON(jdeposit_beli_master=deposit_beli_id)
					LEFT JOIN supplier ON(deposit_beli_supplier=supplier_id)
					WHERE jdeposit_beli_ref='".$ref_id."'";
			}
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_gift_voucher_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql = "SELECT jgift_voucher_id FROM jual_gift_voucher WHERE jgift_voucher_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT jgift_voucher_id as jkwitansi_id
						,gift_voucher_no as kwitansi_no
						,jgift_voucher_nilai as jkwitansi_nilai
						,cust_nama as cust_nama
						,gift_voucher_sisa as kwitansi_sisa
						,gift_voucher_id as kwitansi_id
					FROM jual_gift_voucher
					LEFT JOIN cetak_gift_voucher ON(jgift_voucher_master=gift_voucher_id)
					LEFT JOIN customer ON(gift_voucher_cust=cust_id)
					WHERE jgift_voucher_ref='".$ref_id."'
					ORDER BY jgift_voucher_id ASC
					LIMIT 0,1";
			}else{
					$sql="SELECT jgift_voucher_id as jkwitansi_id
						,gift_voucher_no as kwitansi_no
						,jgift_voucher_nilai as jkwitansi_nilai
						,cust_nama as cust_nama
						,gift_voucher_sisa as kwitansi_sisa
						,gift_voucher_id as kwitansi_id
						FROM jual_gift_voucher
					LEFT JOIN cetak_gift_voucher ON(jgift_voucher_master=gift_voucher_id)
					LEFT JOIN customer ON(gift_voucher_cust=cust_id)
					WHERE jgift_voucher_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				if($cara_bayar_1 == "gift_voucher")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jgift_voucher_id as jkwitansi_id
						,gift_voucher_no as kwitansi_no
						,jgift_voucher_nilai as jkwitansi_nilai
						,cust_nama as cust_nama
						,gift_voucher_sisa as kwitansi_sisa
						,gift_voucher_id as kwitansi_id
					FROM jual_gift_voucher
					LEFT JOIN cetak_gift_voucher ON(jgift_voucher_master=gift_voucher_id)
					LEFT JOIN customer ON(gift_voucher_cust=cust_id)
					WHERE jgift_voucher_ref='".$ref_id."'
					ORDER BY jgift_voucher_id ASC
					LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jgift_voucher_id as jkwitansi_id
						,gift_voucher_no as kwitansi_no
						,jgift_voucher_nilai as jkwitansi_nilai
						,cust_nama as cust_nama
						,gift_voucher_sisa as kwitansi_sisa
						,gift_voucher_id as kwitansi_id
					FROM jual_gift_voucher
					LEFT JOIN cetak_gift_voucher ON(jgift_voucher_master=gift_voucher_id)
					LEFT JOIN customer ON(gift_voucher_cust=cust_id)
					WHERE jgift_voucher_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				if($cara_bayar_1 == "gift_voucher" or $cara_bayar_2 == "gift_voucher")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jgift_voucher_id as jkwitansi_id
						,gift_voucher_no as kwitansi_no
						,jgift_voucher_nilai as jkwitansi_nilai
						,cust_nama as cust_nama
						,gift_voucher_sisa as kwitansi_sisa
						,gift_voucher_id as kwitansi_id
					FROM jual_gift_voucher
					LEFT JOIN cetak_gift_voucher ON(jgift_voucher_master=gift_voucher_id)
					LEFT JOIN customer ON(gift_voucher_cust=cust_id)
					WHERE jgift_voucher_ref='".$ref_id."'
					ORDER BY jgift_voucher_id ASC
					LIMIT ".$limit.",1";
			}elseif($nbrows==3){
				if($cara_bayar_1 == "gift_voucher" and $cara_bayar_2 == "gift_voucher")
					$limit = 2;
				else
					$limit = 1;

				$sql="SELECT jgift_voucher_id as jkwitansi_id
						,gift_voucher_no as kwitansi_no
						,jgift_voucher_nilai as jkwitansi_nilai
						,cust_nama as cust_nama
						,gift_voucher_sisa as kwitansi_sisa
						,gift_voucher_id as kwitansi_id
					FROM jual_gift_voucher
					LEFT JOIN cetak_gift_voucher ON(jgift_voucher_master=gift_voucher_id)
					LEFT JOIN customer ON(gift_voucher_cust=cust_id)
					WHERE jgift_voucher_ref='".$ref_id."'
					ORDER BY jgift_voucher_id ASC
					LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jgift_voucher_id as jkwitansi_id
						,gift_voucher_no as kwitansi_no
						,jgift_voucher_nilai as jkwitansi_nilai
						,cust_nama as cust_nama
						,gift_voucher_sisa as kwitansi_sisa
						,gift_voucher_id as kwitansi_id
					FROM jual_gift_voucher
					LEFT JOIN cetak_gift_voucher ON(jgift_voucher_master=gift_voucher_id)
					LEFT JOIN customer ON(gift_voucher_cust=cust_id)
					WHERE jgift_voucher_ref='".$ref_id."'";
			}
		}
		//return $cara_bayar_1." - ".$cara_bayar_2." - ".$sql;
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	
	function aktifitas_user_insert($modul,$some_id,$no_bukti,$act,$stat_dok,$stat_terima){
		$datetime=date('Y-m-d H:i:s');
		$address=$_SERVER['REMOTE_ADDR'];
		$data=array(
		    'aktivitas_modul'=>$modul,
		    'aktivitas_username'=>$_SESSION[SESSION_USERID],
		    'aktivitas_address'=>$address,
		    'aktivitas_hostname'=>  gethostbyaddr($address),
		    'aktivitas_referid'=>$some_id,
		    'aktivitas_referno'=>$no_bukti,
		    'aktivitas_act'=>$act,
		    'aktivitas_status'=>$stat_dok,
		    'aktivitas_ket'=>$stat_terima,
		    'aktivitas_datetime'=>$datetime
		);
		
		$this->db = $this->load->database("default_log", TRUE);	
		$query=$this->db->insert('aktivitas_user', $data);
	}
	
	function get_card_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql = "SELECT jcard_id FROM jual_card WHERE jcard_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT jcard_id,jcard_no,jcard_nama,jcard_edc,jcard_edc_promo as jcard_promo,jcard_nilai FROM jual_card where jcard_ref='".$ref_id."' ORDER BY jcard_id ASC LIMIT 0,1";
			}else{
				$sql="SELECT jcard_id,jcard_no,jcard_nama,jcard_edc,jcard_edc_promo as jcard_promo,jcard_nilai FROM jual_card where jcard_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				if($cara_bayar_1 == "card")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jcard_id,jcard_no,jcard_nama,jcard_edc,jcard_edc_promo as jcard_promo,jcard_nilai FROM jual_card where jcard_ref='".$ref_id."' ORDER BY jcard_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jcard_id,jcard_no,jcard_nama,jcard_edc,jcard_edc_promo as jcard_promo,jcard_nilai FROM jual_card where jcard_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				if($cara_bayar_1 == "card" or $cara_bayar_2 == "card")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jcard_id,jcard_no,jcard_nama,jcard_edc,jcard_edc_promo as jcard_promo,jcard_nilai FROM jual_card where jcard_ref='".$ref_id."' ORDER BY jcard_id ASC LIMIT ".$limit.",1";
			}elseif($nbrows==3){
				if($cara_bayar_1 == "card" and $cara_bayar_2 == "card")
					$limit = 2;
				else
					$limit = 1;

				$sql="SELECT jcard_id,jcard_no,jcard_nama,jcard_edc,jcard_edc_promo as jcard_promo,jcard_nilai FROM jual_card where jcard_ref='".$ref_id."' ORDER BY jcard_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jcard_id,jcard_no,jcard_nama,jcard_edc,jcard_edc_promo as jcard_promo,jcard_nilai FROM jual_card where jcard_ref='".$ref_id."'";
			}
		}
		//$sql="SELECT jcard_id,jcard_no,jcard_nama,jcard_edc,jcard_nilai FROM jual_card where jcard_ref='".$ref_id."'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_cek_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql="SELECT jcek_id FROM jual_cek WHERE jcek_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT jcek_id,jcek_nama,jcek_no,jcek_valid,jcek_bank,jcek_nilai FROM jual_cek WHERE jcek_ref='".$ref_id."' ORDER BY jcek_id ASC LIMIT 0,1";
			}else{
				$sql="SELECT jcek_id,jcek_nama,jcek_no,jcek_valid,jcek_bank,jcek_nilai FROM jual_cek WHERE jcek_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				if($cara_bayar_1 == "cek")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jcek_id,jcek_nama,jcek_no,jcek_valid,jcek_bank,jcek_nilai FROM jual_cek WHERE jcek_ref='".$ref_id."'  ORDER BY jcek_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jcek_id,jcek_nama,jcek_no,jcek_valid,jcek_bank,jcek_nilai FROM jual_cek WHERE jcek_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				if($cara_bayar_1 == "cek" or $cara_bayar_2 == "cek")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jcek_id,jcek_nama,jcek_no,jcek_valid,jcek_bank,jcek_nilai FROM jual_cek WHERE jcek_ref='".$ref_id."'  ORDER BY jcek_id ASC LIMIT ".$limit.",1";
			}elseif($nbrows==3){
				if($cara_bayar_1 == "cek" and $cara_bayar_2 == "cek")
					$limit = 2;
				else
					$limit = 1;

				$sql="SELECT jcek_id,jcek_nama,jcek_no,jcek_valid,jcek_bank,jcek_nilai FROM jual_cek WHERE jcek_ref='".$ref_id."'  ORDER BY jcek_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jcek_id,jcek_nama,jcek_no,jcek_valid,jcek_bank,jcek_nilai FROM jual_cek WHERE jcek_ref='".$ref_id."'";
			}
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_transfer_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql="SELECT jtransfer_id FROM jual_transfer WHERE jtransfer_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT jtransfer_id,jtransfer_bank,jtransfer_nama,jtransfer_nilai FROM jual_transfer WHERE jtransfer_ref='".$ref_id."'  ORDER BY jtransfer_id ASC LIMIT 0,1";
			}else{
				$sql="SELECT jtransfer_id,jtransfer_bank,jtransfer_nama,jtransfer_nilai FROM jual_transfer WHERE jtransfer_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				if($cara_bayar_1 == "transfer")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jtransfer_id,jtransfer_bank,jtransfer_nama,jtransfer_nilai FROM jual_transfer WHERE jtransfer_ref='".$ref_id."' ORDER BY jtransfer_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jtransfer_id,jtransfer_bank,jtransfer_nama,jtransfer_nilai FROM jual_transfer WHERE jtransfer_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				if($cara_bayar_1 == "transfer" or $cara_bayar_2 == "transfer")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jtransfer_id,jtransfer_bank,jtransfer_nama,jtransfer_nilai FROM jual_transfer WHERE jtransfer_ref='".$ref_id."' ORDER BY jtransfer_id ASC LIMIT ".$limit.",1";
			}elseif($nbrows==3){
				if($cara_bayar_1 == "transfer" and $cara_bayar_2 == "transfer")
					$limit = 2;
				else
					$limit = 1;

				$sql="SELECT jtransfer_id,jtransfer_bank,jtransfer_nama,jtransfer_nilai FROM jual_transfer WHERE jtransfer_ref='".$ref_id."' ORDER BY jtransfer_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jtransfer_id,jtransfer_bank,jtransfer_nama,jtransfer_nilai FROM jual_transfer WHERE jtransfer_ref='".$ref_id."'";
			}
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_tunai_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql="SELECT jtunai_id FROM jual_tunai WHERE jtunai_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT jtunai_id,jtunai_nilai FROM jual_tunai WHERE jtunai_ref='".$ref_id."' ORDER BY jtunai_id ASC LIMIT 0,1";
			}else{
				$sql="SELECT jtunai_id,jtunai_nilai FROM jual_tunai WHERE jtunai_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				if($cara_bayar_1 == "tunai")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jtunai_id,jtunai_nilai FROM jual_tunai WHERE jtunai_ref='".$ref_id."' ORDER BY jtunai_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jtunai_id,jtunai_nilai FROM jual_tunai WHERE jtunai_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				if($cara_bayar_1 == "tunai" or $cara_bayar_2 == "tunai")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jtunai_id,jtunai_nilai FROM jual_tunai WHERE jtunai_ref='".$ref_id."' ORDER BY jtunai_id ASC LIMIT ".$limit.",1";
			}elseif($nbrows==3){
				if($cara_bayar_1 == "tunai" and $cara_bayar_2 == "tunai")
					$limit = 2;
				else
					$limit = 1;

				$sql="SELECT jtunai_id,jtunai_nilai FROM jual_tunai WHERE jtunai_ref='".$ref_id."' ORDER BY jtunai_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jtunai_id,jtunai_nilai FROM jual_tunai WHERE jtunai_ref='".$ref_id."'";
			}
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_vgrooming_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql="SELECT jvgrooming_id FROM jual_vgrooming WHERE jvgrooming_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT jvgrooming_id,jvgrooming_nilai FROM jual_vgrooming WHERE jvgrooming_ref='".$ref_id."' ORDER BY jvgrooming_id ASC LIMIT 0,1";
			}else{
				$sql="SELECT jvgrooming_id,jvgrooming_nilai FROM jual_vgrooming WHERE jvgrooming_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				if($cara_bayar_1 == "vgrooming")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jvgrooming_id,jvgrooming_nilai FROM jual_vgrooming WHERE jvgrooming_ref='".$ref_id."'  ORDER BY jvgrooming_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jvgrooming_id,jvgrooming_nilai FROM jual_vgrooming WHERE jvgrooming_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				if($cara_bayar_1 == "vgrooming" or $cara_bayar_2 == "vgrooming")
					$limit = 1;
				else
					$limit = 0;

				$sql="SELECT jvgrooming_id,jvgrooming_nilai FROM jual_vgrooming WHERE jvgrooming_ref='".$ref_id."' ORDER BY jvgrooming_id ASC LIMIT ".$limit.",1";
			}elseif($nbrows==3){
				if($cara_bayar_1 == "vgrooming" and $cara_bayar_2 == "vgrooming")
					$limit = 2;
				else
					$limit = 1;

				$sql="SELECT jvgrooming_id,jvgrooming_nilai FROM jual_vgrooming WHERE jvgrooming_ref='".$ref_id."'  ORDER BY jvgrooming_id ASC LIMIT ".$limit.",1";
			}else{
				$sql="SELECT jvgrooming_id,jvgrooming_nilai FROM jual_vgrooming WHERE jvgrooming_ref='".$ref_id."'";
			}
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_voucher_by_ref($ref_id ,$cara_bayar_ke, $cara_bayar_1="", $cara_bayar_2=""){
		$sql="SELECT tvoucher_id FROM voucher_terima WHERE tvoucher_ref='".$ref_id."'";
		$rs = $this->db->query($sql);
		$nbrows = $rs->num_rows();
		
		if($cara_bayar_ke==''){
			$cara_bayar_ke = 1;
		}
		
		if($cara_bayar_ke==1){
			if($nbrows>1){
				$sql="SELECT tvoucher_id,tvoucher_novoucher,tvoucher_nilai FROM voucher_terima WHERE tvoucher_ref='".$ref_id."' LIMIT 0,1";
			}else{
				$sql="SELECT tvoucher_id,tvoucher_novoucher,tvoucher_nilai FROM voucher_terima WHERE tvoucher_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==2){
			if($nbrows>1){
				$sql="SELECT tvoucher_id,tvoucher_novoucher,tvoucher_nilai FROM voucher_terima WHERE tvoucher_ref='".$ref_id."' LIMIT 1,1";
			}else{
				$sql="SELECT tvoucher_id,tvoucher_novoucher,tvoucher_nilai FROM voucher_terima WHERE tvoucher_ref='".$ref_id."'";
			}
		}elseif($cara_bayar_ke==3){
			if($nbrows==2){
				$sql="SELECT tvoucher_id,tvoucher_novoucher,tvoucher_nilai FROM voucher_terima WHERE tvoucher_ref='".$ref_id."' LIMIT 1,1";
			}elseif($nbrows==3){
				$sql="SELECT tvoucher_id,tvoucher_novoucher,tvoucher_nilai FROM voucher_terima WHERE tvoucher_ref='".$ref_id."' LIMIT 2,1";
			}else{
				$sql="SELECT tvoucher_id,tvoucher_novoucher,tvoucher_nilai FROM voucher_terima WHERE tvoucher_ref='".$ref_id."'";
			}
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_transfer_paket_by_ref($ref_id){
		$sql="SELECT jtransfer_id,jtransfer_bank,jtransfer_nilai FROM jual_transfer where jtransfer_ref='".$ref_id."'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
		
	function get_harga_produk($produk_id) {
		
		$query = "SELECT produk_harga from produk where produk_id='".$produk_id."'";
		$result = $this->db->query($query);
		if($result->num_rows()){
			$data=$result->row();
			$produk_harga=$data->produk_harga;
			return $produk_harga;
		}else{
			return '0';
		}
	}
	
	function get_member_by_cust($member_cust){
	
		$sql = "SELECT * from vu_customer where cust_id='".$member_cust."'";
		
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	
	function kuitansi_list($cust_id){
		
		$sql = "SELECT k.list_kw AS list_kw, h.list_hutang AS list_hutang, gv.list_gv AS list_gv
				FROM customer c
				#sisa kuitansi
				LEFT JOIN (
					SELECT group_concat('- ',kw.kwitansi_no, ' sebesar Rp ', kw.kwitansi_sisa, ' (', kw.kwitansi_keterangan, ')' SEPARATOR '<br>') AS list_kw, kw.kwitansi_cust
					FROM cetak_kwitansi kw
					WHERE 
						kw.kwitansi_cust = '".$cust_id."' 
						AND kw.kwitansi_status = 'Tertutup' 
						AND kw.kwitansi_sisa > 0
						AND date_format(kw.kwitansi_tanggal, '%Y') >= '2013'
				) k ON k.kwitansi_cust = c.cust_id
				#sisa hutang	
				LEFT JOIN (
					SELECT group_concat('- ',m.lpiutang_faktur, ' sebesar Rp ', m.lpiutang_sisa SEPARATOR '<br>') AS list_hutang, m.lpiutang_cust
					FROM master_lunas_piutang m
					WHERE m.lpiutang_cust = '".$cust_id."' 
						AND m.lpiutang_stat_dok <> 'Batal' 
						AND m.lpiutang_sisa > 0
				) h ON c.cust_id = h.lpiutang_cust
				#sisa Gift Voucher
				LEFT JOIN (
					SELECT group_concat('- ',gv.gift_voucher_no , ' sebesar Rp ', gv.gift_voucher_sisa, ' (', gv.gift_voucher_keterangan, ')' SEPARATOR '<br>') AS list_gv, gv.gift_voucher_cust
					FROM cetak_gift_voucher gv
					WHERE gv.gift_voucher_cust = '".$cust_id."'
						AND gv.gift_voucher_status <> 'Batal' 
						AND gv.gift_voucher_sisa > 0
				) AS gv ON c.cust_id = gv.gift_voucher_cust
				WHERE c.cust_id = '".$cust_id."'";
								
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql;		
		$result = $this->db->query($limit);  
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}		
	}

	function get_auto_cust_no($cust_id){
		$sql = "SELECT cust_no from customer where cust_id='".$cust_id."' and cust_aktif!='Tidak Aktif' order by cust_id desc limit 1";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	
	function get_customer_list($query,$start,$end){
		$sql="SELECT cust_id, cust_no, cust_nama, cust_member_no, cust_member_level, cust_preward_total, cust_note, cust_tgllahir,cust_alamat,cust_kota,cust_telprumah,cust_point,date_format(cust_tgllahir,'%Y-%m-%d') AS cust_tgllahir, cust_owner FROM customer WHERE cust_aktif='Aktif'";
		if($query<>""){
			$sql=$sql." and (/*cust_id = '".$query."' or*/ cust_no like '%".$query."%' or cust_alamat like '%".$query."%' or cust_nama like '%".$query."%' or cust_telprumah like '%".$query."%' or cust_telprumah2 like '%".$query."%' or cust_telpkantor like '%".$query."%' or cust_hp like '%".$query."%' or cust_hp2 like '%".$query."%' or cust_hp3 like '%".$query."%' or date_format(cust_tgllahir,'%d-%m') like '%".$query."%' ) ";
		}
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit);  
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_kwitansi_list($query,$start=0,$end=10,$kwitansi_cust){
		
		$sql = "SELECT * FROM(
					SELECT kwitansi_id
						,kwitansi_no
						,kwitansi_nilai
						,cust_no
						,cust_nama
						,cust_tgllahir
						,cust_alamat
						,kwitansi_sisa AS total_sisa
						,kwitansi_keterangan
						,'ku' as jenis
					FROM cetak_kwitansi
						#LEFT JOIN jual_kwitansi ON(jkwitansi_master=kwitansi_id)
						LEFT JOIN customer ON(kwitansi_cust=cust_id)
					WHERE kwitansi_status = 'Tertutup'
						AND kwitansi_sisa>0
						AND date_format(kwitansi_tanggal, '%Y') >= '2013'
					UNION

					SELECT gv.gift_voucher_id
						,gv.gift_voucher_no
						,gv.gift_voucher_nilai
						,c.cust_no
						,c.cust_nama
						,c.cust_tgllahir
						,c.cust_alamat
						,gv.gift_voucher_sisa AS total_sisa
						,gv.gift_voucher_keterangan
						,'gv' as jenis
					FROM cetak_gift_voucher gv
						#LEFT JOIN jual_gift_voucher jgv ON(jgv.jgift_voucher_master=gv.gift_voucher_id)
						LEFT JOIN customer c ON(gv.gift_voucher_cust=c.cust_id)
					WHERE gv.gift_voucher_status = 'Tertutup'
						AND gv.gift_voucher_sisa>0
				) AS TBL					";
		
		if($query<>""){
			$sql=$sql." WHERE (cust_no like '%".$query."%' or cust_nama like '%".$query."%' or cust_alamat like '%".$query."%' or kwitansi_no like '%".$query."%')";
		}
		if($kwitansi_cust<>""){
			$sql=$sql." AND kwitansi_cust='$kwitansi_cust'";
		}
		//$sql.=" GROUP BY kwitansi_id";
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit);  
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function card_edc_jenis_list($filter,$start,$end){
		$query =   "SELECT edc_jenis_id as produk_edc_jenis_id, edc_jenis_nama as produk_edc_jenis_nama FROM card_edc_jenis where edc_jenis_aktif='Aktif' ";
		
		// For simple search
		if ($filter<>"" && $query !=""){
			$query .=eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " (edc_jenis_nama LIKE '%".addslashes($filter)."%' )";
			$limit = $query;
		}
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		if($end=="") $end=15;
		$limit = $query." LIMIT ".$start.",".$end;
		$result = $this->db->query($limit);
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function card_edc_list($filter,$start,$end){
		$query =   "SELECT edc_id as fpiutang_edc_id, edc_nama as fpiutang_edc_nama FROM card_edc where edc_aktif='Aktif' ";
		
		// For simple search
		if ($filter<>"" && $query !=""){
			$query .=eregi("WHERE",$query)? " AND ":" WHERE ";
			$query .= " (edc_nama LIKE '%".addslashes($filter)."%' )";
			$limit = $query;
		}
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		if($end=="") $end=15;
		$limit = $query." LIMIT ".$start.",".$end;
		$result = $this->db->query($limit);
		
		if($nbrows>0){
			foreach($result->result() as $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_supplier_list($query,$start,$end){
		$sql="SELECT supplier_id,supplier_nama,supplier_alamat,supplier_kota,supplier_notelp, supplier_penagih FROM supplier where supplier_aktif='Aktif'";
		
		if($query<>""){
			$sql .=eregi("WHERE",$sql)? " AND ":" WHERE ";
			$sql .= " (supplier_nama LIKE '%".addslashes($query)."%')";
		}
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;		
		$result = $this->db->query($limit);  
		
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
		
	}
	
	function get_kode_1($table,$field,$pattern,$length, $db=""){
		$len_pattern=strlen($pattern);
		$len_lpad=$length-$len_pattern;
		$sql="select concat(LEFT(max(".$field."),".$len_pattern."),LPAD((right(max(".$field."),".$len_lpad.")+1),".$len_lpad.",0)) AS max_key from ".$table." where ".$field." like '".$pattern."%'";
		
		if($db == "") $db = $this->db;
		if($db == "TH2") $db = $this->load->database("TH2", TRUE);
		$query=$db->query($sql);
		
		if($query->num_rows()){
			$data=$query->row();
			$kode=$data->max_key;
			if(is_null($kode))
			{
				$pad="";
				for($i=1;$i<$len_lpad;$i++)
					$pad.="0";
				$kode=$pattern.$pad."1";
			}
			return $kode;
		}else{
			$pad="";
			for($i=1;$i<$len_lpad;$i++)
				$pad.="0";
			$kode=$pattern.$pad."1";
			return $kode;
		}
	}
	
	function get_kode_2($table,$field,$pattern_g,$pattern_j,$length){
		$len_pattern=strlen($pattern_g.$pattern_j);
		$len_lpad=$length-$len_pattern;
		$sql="select concat(LEFT(max(".$field."),".$len_pattern."),LPAD((right(max(".$field."),".$len_lpad.")+1),".$len_lpad.",0)) AS max_key from ".$table." where ".$field." like '".$pattern_g.$pattern_j."%'";
		$query=$this->db->query($sql);
		if($query->num_rows()){
			$data=$query->row();
			$kode=$data->max_key;
			if(is_null($kode))
			{
				$pad="";
				for($i=1;$i<$len_lpad;$i++)
					$pad.="0";
				$kode=$pattern_g.$pattern_j.$pad."1";
			}
			return $kode;
		}else{
			$pad="";
			for($i=1;$i<$len_lpad;$i++)
				$pad.="0";
			$kode=$pattern_g.$pattern_j.$pad."1";
			return $kode;
		}
	}
	
	function get_kode_kosong($table,$field,$field_to_compare,$pattern,$length,$jenis,$date_field,$tanggal=''){
		$len_pattern=strlen($pattern);
		$len_lpad=$length-$len_pattern;
		//$bulan=date("Y-m");
		$bulan=substr($tanggal,0,4)."-".substr($tanggal,5,2);
		
		//mencari nomor pada 'field_to_compare' yang tidak ada pada 'field' dan diambil yg paling kecil
		$sql="SELECT MIN(".$field_to_compare.") AS max_key
				FROM ".$table."
				WHERE ".$field_to_compare." <> '' AND ".$field_to_compare." LIKE '".$jenis."%'
				AND ".$field_to_compare." NOT IN (SELECT ".$field." FROM ".$table." WHERE ".$field." LIKE '".$jenis."%') AND DATE_FORMAT(".$date_field.",'%Y-%m') = '".$bulan."' ";
		
		$query=$this->db->query($sql);
		if($query->num_rows()){	//jika ditemukan nomor faktur yg lompat
			$data=$query->row();
			$kode=$data->max_key;
			if(is_null($kode)){	//jika data adalah null
				return -1;
			}else{
				return $kode;
			}			
		}else{
			return -1;
		}
	} 

	function get_satuan_list(){
		$sql="SELECT satuan_id,satuan_kode,satuan_nama FROM satuan where satuan_aktif='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_satuan_bydjproduk_list($djproduk_id){
		$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_harga FROM produk,satuan_konversi,satuan WHERE produk_id=konversi_produk AND konversi_satuan=satuan_id AND produk_id='$djproduk_id'";
		if($djproduk_id==0)
			$sql="SELECT satuan_id,satuan_nama,konversi_nilai,satuan_kode,konversi_default,produk_harga FROM produk,satuan_konversi,satuan WHERE produk_id=konversi_produk AND konversi_satuan=satuan_id";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_kategori_produk_list(){
		$sql="SELECT kategori_id,kategori_nama FROM kategori where kategori_jenis='produk' and kategori_aktif='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}

	function get_kategori_list(){
		$sql="SELECT kategori_id,kategori_nama,kategori_jenis FROM kategori where kategori_aktif='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	
	function get_user_karyawan_list($query,$start,$end){
		$sql="SELECT karyawan_id,karyawan_no,karyawan_nama,jabatan_nama FROM vu_karyawan,jabatan 
				WHERE jabatan_id=karyawan_jabatan AND karyawan_aktif='Aktif'
				AND karyawan_id IN(select user_karyawan from users)";
		if($query!=="")
			$sql.=" and (karyawan_id like '%".$query."%' or karyawan_no like '%".$query."%' or karyawan_nama like '%".$query."%'
						 or jabatan_nama like '%".$query."%')";
	
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;			
		$result = $this->db->query($limit); 
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_produk_group_list(){
		$sql="SELECT produk_id,produk_nama, produk_kategori,produk_group FROM produk where produk_aktif='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_produk_list($query,$start,$end){
		/*$sql="SELECT produk_id,produk_kode,produk_nama,produk_kategori,produk_harga,produk_group,produk_du,produk_dm
				,kategori_nama, group_nama, satuan_kode, satuan_nama 
				FROM produk,satuan,kategori,produk_group where satuan_id=produk_satuan and kategori_id=produk_kategori
				and produk_group=group_id and produk_aktif='Aktif'";*/
		$rs_rows=0;
		if(is_numeric($query)==true){
			$sql_dproduk="SELECT krawat_produk FROM perawatan_konsumsi WHERE krawat_master='$query'";
			$rs=$this->db->query($sql_dproduk);
			$rs_rows=$rs->num_rows();
		}
		
		$sql="select * from vu_produk";
		if($query<>"" && is_numeric($query)==false){
			$sql.=eregi("WHERE",$sql)? " AND ":" WHERE ";
			$sql.=" (produk_kode like '%".$query."%' or produk_nama like '%".$query."%' or satuan_nama like '%".$query."%' or kategori_nama like '%".$query."%' or group_nama like '%".$query."%') ";
		}else{
			if($rs_rows){
				$filter="";
				$sql.=eregi("WHERE",$sql)? " OR ":" WHERE ";
				foreach($rs->result() AS $row_dproduk){					
					$filter.="OR produk_id='".$row_dproduk->krawat_produk."' ";
				}
				$sql=$sql."(".substr($filter,2,strlen($filter)).")";
			}
		}
		
		/*if($query<>"")
			$sql.=" WHERE (produk_kode like '%".$query."%' or produk_nama like '%".$query."%' or satuan_nama like '%".$query."%'
						 or kategori_nama like '%".$query."%' or group_nama like '%".$query."%') ";*/
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		if($end!=0){
			$limit = $sql." LIMIT ".$start.",".$end;			
			$result = $this->db->query($limit);
		}
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_group_list($query,$start,$end){
		$sql="SELECT group_id,group_nama,group_duproduk,group_dmproduk,group_dm2produk,group_durawat,group_dmrawat,group_dm2rawat,group_dupaket,group_dmpaket,group_dm2paket FROM produk_group where group_aktif='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_group_produk_list($query,$start,$end){
		$sql="SELECT group_id,group_nama,group_duproduk,group_dmproduk,group_dm2produk,group_durawat,group_dmrawat,group_dm2rawat,group_dupaket,group_dmpaket,group_dm2paket,group_dultah,group_dcard,group_dkolega,group_dkeluarga,group_downer,group_dgrooming,group_dwartawan, group_dstaffdokter, group_dstaffnondokter
				FROM produk_group
				WHERE group_aktif='Aktif'";
		
		if($query<>""){
			$sql=$sql." and group_nama like '%".$query."%'";
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_jenis_produk_list($query,$start,$end){
		$sql="SELECT jenis_id,jenis_nama FROM jenis WHERE jenis_kelompok='produk' AND jenis_aktif='Aktif'";
		//$sql="SELECT jenis_id,jenis_nama FROM jenis WHERE jenis_kelompok='produk' AND jenis_aktif='Aktif' AND jenis_kategori='".$jenis_kategori."'";
		
		if($query<>""){
			$sql=$sql." and jenis_nama like '%".$query."%' ";
		}
		
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_kategori_jenis_produk_list(){
		$sql="SELECT kategori_id, kategori_nama FROM kategori where kategori_jenis = 'produk' AND kategori_aktif='Aktif'";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_group_by_id($group_id){
		$query = "SELECT * FROM produk_group WHERE group_id='".$group_id."' and group_aktif='Aktif'";
		
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_group2_by_id($jenis_id){
		$query = "SELECT * FROM jenis WHERE jenis_id='".$jenis_id."' and jenis_aktif='Aktif'";
		
		$result = $this->db->query($query);
		$nbrows = $result->num_rows();
		
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function get_gudang_list(){
		$sql="SELECT gudang_id,gudang_nama FROM gudang WHERE gudang_aktif='Aktif' AND gudang_id <> 99";
		$query = $this->db->query($sql);
		$nbrows = $query->num_rows();
		if($nbrows>0){
			foreach($query->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
	}
	
	function pengecekan_dokumen($tanggal_pengecekan){
	    $tanggal_3_hari_lalu = date('Y-m-d', strtotime("-30000 days"));
	    
	    if ($tanggal_pengecekan >= $tanggal_3_hari_lalu) {
			return '1';
	    }
	    else {
			return '0';
	    }
	}
	
	/*Pengecekan Dokumen utk stok, mengarah ke transaksi_setting */
	function pengecekan_dokumen_stok($tanggal_pengecekan){

		$sql_day = "SELECT DATEDIFF((select current_date),(SELECT mb_days from transaksi_setting)) AS mb_days";
		//mb_days berisi jumlah hari dr tgl hr ini dikurangi tgl yg ada di transaksi_setting(default tgl 1). Ex : skrg tgl 18, tgl di trans setting tgl 1 maka mb_days = 17
		$query_day= $this->db->query($sql_day);
		$data_day= $query_day->row();
		$day= $data_day->mb_days;
		
		$sql_tgl = "SELECT DATE_FORMAT(date_add('".$tanggal_pengecekan."',interval ".$day." day),'%Y-%m-%d') AS tanggal";			
		$query_tgl=$this->db->query($sql_tgl);
		if($query_tgl->num_rows()){
			$tgl=$query_tgl->row();
			$tanggal=$tgl->tanggal;
		}

		$date = date('Y-m-d');
		
		if ($date <= $tanggal || $tanggal_pengecekan == $date) 
		{
			return '1';
		
		}else
		{
			return '0';
		}
	}
	

	function get_tutup_bulanan(){
	
		$last_bln=''; $last_thn='';
		$sqlcek="select tbb.tbb_id, tbb.tbb_bulan, tbb.tbb_tahun from tutup_buku_bulanan tbb 
				where tbb.tbb_date_create=(select max(tbb_date_create) from tutup_buku_bulanan)";
		$rs=$this->db->query($sqlcek);
		if($rs->num_rows()){
			$data=$rs->row();
			$last_bln=$data->tbb_bulan;
			$last_thn=$data->tbb_tahun;
		}
		return $last_thn.'-'.$last_bln;
		
	}
	
	function cara_bayar_tunai_insert($jtunai_nilai
									,$jtunai_ref
									,$jtunai_date_create
									,$jenis_transaksi
									,$cetak){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			"jtunai_nilai"=>$jtunai_nilai,
			"jtunai_ref"=>$jtunai_ref,
			"jtunai_transaksi"=>$jenis_transaksi,
			"jtunai_date_create"=>$jtunai_date_create,
			"jtunai_stat_dok"=>$stat_dok
			);
		$this->db->insert('jual_tunai', $data);
		if($this->db->affected_rows()){
			return 1;
		}else{
			return 0;
		}
	}
	
	function cara_bayar_vgrooming_insert($jvgrooming_nilai,$jvgrooming_ref, $jvgrooming_date_create, $jenis_transaksi, $cetak){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			"jvgrooming_nilai"=>$jvgrooming_nilai,
			"jvgrooming_ref"=>$jvgrooming_ref,
			"jvgrooming_transaksi"=>$jenis_transaksi,
			"jvgrooming_date_create"=>$jvgrooming_date_create,
			"jvgrooming_stat_dok"=>$stat_dok
			);
		$this->db->insert('jual_vgrooming', $data);
		if($this->db->affected_rows()){
			return 1;
		}else{
			return 0;
		}
	}
	
	function cara_bayar_transfer_insert($jtransfer_bank
										,$jtransfer_nama
										,$jtransfer_nilai
										,$jtransfer_ref
										,$jtransfer_date_create
										,$jenis_transaksi
										,$cetak){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			"jtransfer_bank"=>$jtransfer_bank,
			"jtransfer_nama"=>$jtransfer_nama,
			"jtransfer_nilai"=>$jtransfer_nilai,
			"jtransfer_ref"=>$jtransfer_ref,
			"jtransfer_transaksi"=>$jenis_transaksi,
			"jtransfer_date_create"=>$jtransfer_date_create,
			"jtransfer_stat_dok"=>$stat_dok
			);
		$this->db->insert('jual_transfer', $data);
		if($this->db->affected_rows()){
			return 1;
		}else{
			return 0;
		}
	}
	
	function cara_bayar_card_insert($jcard_nama
									,$jcard_edc
									,$jcard_promo
									,$jcard_no
									,$jcard_nilai
									,$jcard_ref
									,$jcard_date_create
									,$jenis_transaksi
									,$cetak){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			"jcard_nama"=>$jcard_nama,
			"jcard_edc"=>$jcard_edc,
			"jcard_edc_promo"=>$jcard_promo,
			"jcard_no"=>$jcard_no,
			"jcard_nilai"=>$jcard_nilai,
			"jcard_ref"=>$jcard_ref,
			"jcard_transaksi"=>$jenis_transaksi,
			"jcard_creator"=>$_SESSION[SESSION_USERID],
			"jcard_date_create"=>$jcard_date_create,
			"jcard_update"=>$_SESSION[SESSION_USERID],
			"jcard_date_update"=>date('Y-m-d H:i:s'),
			"jcard_stat_dok"=>$stat_dok
			);
		$this->db->insert('jual_card', $data);
		if($this->db->affected_rows()){
			return 1;
		}else{
			return 0;
		}
	}
	
	function cara_bayar_cek_insert($jcek_nama
									,$jcek_no
									,$jcek_valid
									,$jcek_bank
									,$jcek_nilai
									,$jcek_ref
									,$jcek_date_create
									,$jenis_transaksi
									,$cetak){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			"jcek_nama"=>$jcek_nama,
			"jcek_no"=>$jcek_no,
			"jcek_valid"=>$jcek_valid,
			"jcek_bank"=>$jcek_bank,
			"jcek_nilai"=>$jcek_nilai,
			"jcek_ref"=>$jcek_ref,
			"jcek_transaksi"=>$jenis_transaksi,
			"jcek_date_create"=>$jcek_date_create,
			"jcek_stat_dok"=>$stat_dok
			);
		$this->db->insert('jual_cek', $data);
		if($this->db->affected_rows()){
			return 1;
		}else{
			return 0;
		}
	}
	
	function cara_bayar_kwitansi_insert($jkwitansi_master
										,$jkwitansi_nilai
										,$jkwitansi_ref
										,$jkwitansi_date_create
										,$jenis_transaksi
										,$cetak
										,$jual_kwitansi_id){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			//"jkwitansi_master"=>$jkwitansi_master,
			"jkwitansi_nilai"=>$jkwitansi_nilai,
			"jkwitansi_ref"=>$jkwitansi_ref,
			"jkwitansi_transaksi"=>$jenis_transaksi,
			"jkwitansi_date_create"=>$jkwitansi_date_create,
			"jkwitansi_creator"=>$_SESSION[SESSION_USERID],
			"jkwitansi_stat_dok"=>$stat_dok
		);
		
		/*Melakukan pengecekan, jika kwitansi_id ini ada, maka proses Insert/Create akan terisi dengan ID dari cetak_kwitansi, Jika tidak ada, maka tidak perlu diisikan*/
		$sql="SELECT kwitansi_id FROM cetak_kwitansi WHERE kwitansi_id='".$jkwitansi_master."'";
			$rs=$this->db->query($sql);
			if($rs->num_rows()) {
				$data["jkwitansi_master"]=$jkwitansi_master;
			}
		
		$sql_cek = "SELECT * FROM jual_kwitansi WHERE jkwitansi_id = '".$jual_kwitansi_id."' ";
		$rs_cek=$this->db->query($sql_cek);
			if($rs_cek->num_rows())
			{
				$this->db->where('jkwitansi_id', $jual_kwitansi_id);
				$this->db->update('jual_kwitansi', $data);
				//echo "iki update";
			}
			else
			{
				$this->db->insert('jual_kwitansi', $data);
				//echo "iki insert";
			}

			
		if($this->db->affected_rows()){
			//Jika Statusnya Tertutup, maka akan ada proses mengupdate Sisa Kwitansi (mengurangi sisa kwitansi dgn nominal kwitansi pd faktur)
			if($stat_dok=='Tertutup'){
				if(is_numeric($jkwitansi_master)==false)
				{
				$sql_kwitansi_id = "SELECT kwitansi_id from cetak_kwitansi WHERE kwitansi_no = '".$jkwitansi_master."'";
				$query_kw= $this->db->query($sql_kwitansi_id);
				$kw_id= $query_kw->row();
				$kwitansi_id= $kw_id->kwitansi_id;
				$jkwitansi_master = $kwitansi_id;
				
				}
				//masuk proses cetak faktur ==> untuk itu update db.cetak_kwitansi.kwitansi_sisa
				$sqlu = "UPDATE cetak_kwitansi,
							(SELECT sum(jkwitansi_nilai) AS total_kwitansi
								,jkwitansi_master 
							FROM jual_kwitansi
							WHERE jkwitansi_master<>0
								AND jkwitansi_stat_dok<>'Batal'
								AND jkwitansi_master='".$jkwitansi_master."'
							GROUP BY jkwitansi_master) AS vu_kw
						SET kwitansi_sisa=(kwitansi_nilai - vu_kw.total_kwitansi)
						WHERE vu_kw.jkwitansi_master=kwitansi_id
							AND kwitansi_id='".$jkwitansi_master."'";
				$this->db->query($sqlu);
				return 1;
			}
			//Jika Statusnya Batal, maka ada proses mengupdate Sisa Kwitansi (menambahkan/mengembalikan sisa dari kwitansi tersebut)
			else if($stat_dok=='Batal'){
			if(is_numeric($jkwitansi_master)==false)
				{
				$sql_kwitansi_id = "SELECT kwitansi_id from cetak_kwitansi WHERE kwitansi_no = '".$jkwitansi_master."'";
				$query_kw= $this->db->query($sql_kwitansi_id);
				$kw_id= $query_kw->row();
				$kwitansi_id= $kw_id->kwitansi_id;
				$jkwitansi_master = $kwitansi_id;
				
				}
				//masuk proses cetak faktur ==> untuk itu update db.cetak_kwitansi.kwitansi_sisa
				$sqlu = "UPDATE cetak_kwitansi,
							(SELECT sum(jkwitansi_nilai) AS total_kwitansi
								,jkwitansi_master 
							FROM jual_kwitansi
							WHERE jkwitansi_master<>0
								AND jkwitansi_stat_dok<>'Batal'
								AND jkwitansi_master='".$jkwitansi_master."'
							GROUP BY jkwitansi_master) AS vu_kw
						SET kwitansi_sisa=(kwitansi_nilai + vu_kw.total_kwitansi)
						WHERE vu_kw.jkwitansi_master=kwitansi_id
							AND kwitansi_id='".$jkwitansi_master."'";
				$this->db->query($sqlu);
				return 1;
			}
			
			else{
				return 1;
			}
			
		}else{
			return 0;
		}
	}
	
	function cara_bayar_deposit_beli_insert($jdeposit_beli_master
										,$jdeposit_beli_nilai
										,$jdeposit_beli_ref
										,$jdeposit_beli_date_create
										,$jenis_transaksi
										,$cetak
										,$jual_kwitansi_id){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			//"jdeposit_beli_master"=>$jdeposit_beli_master,
			"jdeposit_beli_nilai"=>$jdeposit_beli_nilai,
			"jdeposit_beli_ref"=>$jdeposit_beli_ref,
			"jdeposit_beli_transaksi"=>$jenis_transaksi,
			"jdeposit_beli_creator"=>$_SESSION[SESSION_USERID],
			"jdeposit_beli_date_create"=>$jdeposit_beli_date_create,
			"jdeposit_beli_stat_dok"=>$stat_dok
		);
		
		/*Melakukan pengecekan, jika deposit_beli_id ini ada, maka proses Insert/Create akan terisi dengan ID dari cetak_deposit_beli, Jika tidak ada, maka tidak perlu diisikan*/
		$sql="SELECT deposit_beli_id FROM cetak_deposit_beli WHERE deposit_beli_id='".$jdeposit_beli_master."'";
			$rs=$this->db->query($sql);
			if($rs->num_rows()) {
				$data["jdeposit_beli_master"]=$jdeposit_beli_master;
			}
		
		$sql_cek = "SELECT * FROM jual_deposit_beli WHERE jdeposit_beli_id = '".$jual_kwitansi_id."' ";
		$rs_cek=$this->db->query($sql_cek);
			if($rs_cek->num_rows())
			{
				$this->db->where('jdeposit_beli_id', $jual_kwitansi_id);
				$this->db->update('jual_deposit_beli', $data);
				//echo "iki update";
			}
			else
			{
				$this->db->insert('jual_deposit_beli', $data);
				//echo "iki insert";
			}

			
		if($this->db->affected_rows()){
			//Jika Statusnya Tertutup, maka akan ada proses mengupdate Sisa Kwitansi (mengurangi sisa kwitansi dgn nominal kwitansi pd faktur)
			if($stat_dok=='Tertutup'){
				if(is_numeric($jdeposit_beli_master)==false)
				{
				$sql_kwitansi_id = "SELECT deposit_beli_id from cetak_deposit_beli WHERE deposit_beli_no = '".$jdeposit_beli_master."'";
				$query_kw= $this->db->query($sql_kwitansi_id);
				$kw_id= $query_kw->row();
				$deposit_beli_id= $kw_id->deposit_beli_id;
				$jdeposit_beli_master = $deposit_beli_id;
				
				}
				//masuk proses cetak faktur ==> untuk itu update db.cetak_deposit_beli.deposit_beli_sisa
				$sqlu = "UPDATE cetak_deposit_beli,
							(SELECT sum(jdeposit_beli_nilai) AS total_deposit
								,jdeposit_beli_master 
							FROM jual_deposit_beli
							WHERE jdeposit_beli_master<>0
								AND jdeposit_beli_stat_dok<>'Batal'
								AND jdeposit_beli_master='".$jdeposit_beli_master."'
							GROUP BY jdeposit_beli_master) AS vu_dep
						SET deposit_beli_sisa=(deposit_beli_nilai - vu_dep.total_deposit)
						WHERE vu_dep.jdeposit_beli_master=deposit_beli_id
							AND deposit_beli_id='".$jdeposit_beli_master."'";
				$this->db->query($sqlu);
				return 1;
			}
			//Jika Statusnya Batal, maka ada proses mengupdate Sisa Kwitansi (menambahkan/mengembalikan sisa dari kwitansi tersebut)
			else if($stat_dok=='Batal'){
			if(is_numeric($jdeposit_beli_master)==false)
				{
				$sql_kwitansi_id = "SELECT deposit_beli_id from cetak_deposit_beli WHERE deposit_beli_no = '".$jdeposit_beli_master."'";
				$query_kw= $this->db->query($sql_kwitansi_id);
				$kw_id= $query_kw->row();
				$deposit_beli_id= $kw_id->deposit_beli_id;
				$jdeposit_beli_master = $deposit_beli_id;
				
				}
				//masuk proses cetak faktur ==> untuk itu update db.cetak_deposit_beli.deposit_beli_sisa
				$sqlu = "UPDATE cetak_deposit_beli,
							(SELECT sum(jdeposit_beli_nilai) AS total_deposit
								,jdeposit_beli_master 
							FROM jual_deposit_beli
							WHERE jdeposit_beli_master<>0
								AND jdeposit_beli_stat_dok<>'Batal'
								AND jdeposit_beli_master='".$jdeposit_beli_master."'
							GROUP BY jdeposit_beli_master) AS vu_dep
						SET deposit_beli_sisa=(deposit_beli_nilai + vu_dep.total_deposit)
						WHERE vu_dep.jdeposit_beli_master=deposit_beli_id
							AND deposit_beli_id='".$jdeposit_beli_master."'";
				$this->db->query($sqlu);
				return 1;
			}
			
			else{
				return 1;
			}
			
		}else{
			return 0;
		}
	}
	
	function cara_bayar_giftvoucher_insert($jgift_voucher_master
										,$jgift_voucher_nilai
										,$jgift_voucher_ref
										,$jgift_voucher_date_create
										,$jenis_transaksi
										,$cetak
										,$jual_gift_voucher_id){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			//"jgift_voucher_master"=>$jgift_voucher_master,
			"jgift_voucher_nilai"=>$jgift_voucher_nilai,
			"jgift_voucher_ref"=>$jgift_voucher_ref,
			"jgift_voucher_transaksi"=>$jenis_transaksi,
			"jgift_voucher_date_create"=>$jgift_voucher_date_create,
			"jgift_voucher_stat_dok"=>$stat_dok
		);
		
		/*Melakukan pengecekan, jika gift_voucher_id ini ada, maka proses Insert/Create akan terisi dengan ID dari cetak_gift_voucher, Jika tidak ada, maka tidak perlu diisikan*/
		$sql="SELECT gift_voucher_id FROM cetak_gift_voucher WHERE gift_voucher_id='".$jgift_voucher_master."'";
			$rs=$this->db->query($sql);
			if($rs->num_rows()) {
				$data["jgift_voucher_master"]=$jgift_voucher_master;
			}
		
		$sql_cek = "SELECT * FROM jual_gift_voucher WHERE jgift_voucher_id = '".$jual_gift_voucher_id."' ";
		$rs_cek=$this->db->query($sql_cek);
			if($rs_cek->num_rows())
			{
				$this->db->where('jgift_voucher_id', $jual_gift_voucher_id);
				$this->db->update('jual_gift_voucher', $data);
				//echo "iki update";
			}
			else
			{
				$this->db->insert('jual_gift_voucher', $data);
				//echo "iki insert";
			}

			
		if($this->db->affected_rows()){
			//Jika Statusnya Tertutup, maka akan ada proses mengupdate Sisa Kwitansi (mengurangi sisa kwitansi dgn nominal kwitansi pd faktur)
			if($stat_dok=='Tertutup'){
				if(is_numeric($jgift_voucher_master)==false)
				{
				$sql_gift_voucher_id = "SELECT gift_voucher_id from cetak_gift_voucher WHERE gift_voucher_no = '".$jgift_voucher_master."'";
				$query_gv= $this->db->query($sql_gift_voucher_id);
				$gv_id= $query_gv->row();
				$gift_voucher_id= $gv_id->gift_voucher_id;
				$jgift_voucher_master = $gift_voucher_id;
				
				}
				//masuk proses cetak faktur ==> untuk itu update db.cetak_gift_voucher.gift_voucher_sisa
				$sqlu = "UPDATE cetak_gift_voucher,
							(SELECT sum(jgift_voucher_nilai) AS total_gift_voucher
								,jgift_voucher_master 
							FROM jual_gift_voucher
							WHERE jgift_voucher_master<>0
								AND jgift_voucher_stat_dok<>'Batal'
								AND jgift_voucher_master='".$jgift_voucher_master."'
							GROUP BY jgift_voucher_master) AS vu_gv
						SET gift_voucher_sisa=(gift_voucher_nilai - vu_gv.total_gift_voucher)
						WHERE vu_gv.jgift_voucher_master=gift_voucher_id
							AND gift_voucher_id='".$jgift_voucher_master."'";
				$this->db->query($sqlu);
				return 1;
			}
			//Jika Statusnya Batal, maka ada proses mengupdate Sisa Kwitansi (menambahkan/mengembalikan sisa dari kwitansi tersebut)
			else if($stat_dok=='Batal'){
			if(is_numeric($jgift_voucher_master)==false)
				{
				$sql_gift_voucher_id = "SELECT gift_voucher_id from cetak_gift_voucher WHERE gift_voucher_no = '".$jgift_voucher_master."'";
				$query_gv= $this->db->query($sql_gift_voucher_id);
				$gv_id= $query_gv->row();
				$gift_voucher_id= $gv_id->gift_voucher_id;
				$jgift_voucher_master = $gift_voucher_id;
				
				}
				//masuk proses cetak faktur ==> untuk itu update db.cetak_gift_voucher.gift_voucher_sisa
				$sqlu = "UPDATE cetak_gift_voucher,
							(SELECT sum(jgift_voucher_nilai) AS total_gift_voucher
								,jgift_voucher_master 
							FROM jual_gift_voucher
							WHERE jgift_voucher_master<>0
								AND jgift_voucher_stat_dok<>'Batal'
								AND jgift_voucher_master='".$jgift_voucher_master."'
							GROUP BY jgift_voucher_master) AS vu_gv
						SET gift_voucher_sisa=(gift_voucher_nilai + vu_gv.total_gift_voucher)
						WHERE vu_gv.jgift_voucher_master=gift_voucher_id
							AND gift_voucher_id='".$jgift_voucher_master."'";
				$this->db->query($sqlu);
				return 1;
			}
			
			else{
				return 1;
			}
			
		}else{
			return 0;
		}
	}
	
	function cara_bayar_voucher_insert($tvoucher_novoucher
									   ,$tvoucher_ref
									   ,$tvoucher_nilai
									   ,$tvoucher_date_create
									   ,$jenis_transaksi
									   ,$cetak){
		$stat_dok = 'Terbuka';
		if($cetak==1 || $cetak==2){
			$stat_dok = 'Tertutup';
		}
		$data=array(
			"tvoucher_novoucher"=>$tvoucher_novoucher,
			"tvoucher_ref"=>$tvoucher_ref,
			"tvoucher_nilai"=>$tvoucher_nilai,
			"tvoucher_transaksi"=>$jenis_transaksi,
			"tvoucher_date_create"=>$tvoucher_date_create,
			"tvoucher_stat_dok"=>$stat_dok
			);
		$this->db->insert('voucher_terima', $data);
		if($this->db->affected_rows()){
			return 1;
		}else{
			return 0;
		}
	}
	
	function cara_bayar_ftpkpr_insert($jrawat_nobukti ,$jrawat_cara ,$jrawat_kwitansi_no ,$jrawat_kwitansi_nilai
                               ,$jrawat_card_nama ,$jrawat_card_edc ,$jrawat_card_promo ,$jrawat_card_no ,$jrawat_card_nilai
                               ,$jrawat_cek_nama ,$jrawat_cek_no ,$jrawat_cek_valid ,$jrawat_cek_bank ,$jrawat_cek_nilai
                               ,$jrawat_transfer_bank ,$jrawat_transfer_nama ,$jrawat_transfer_nilai
                               ,$jrawat_tunai_nilai
                               ,$jrawat_voucher_no ,$jrawat_voucher_cashback
                               ,$jrawat_cara2
                               ,$jrawat_kwitansi_no2 ,$jrawat_kwitansi_nilai2
                               ,$jrawat_card_nama2 ,$jrawat_card_edc2 ,$jrawat_card_promo2 ,$jrawat_card_no2 ,$jrawat_card_nilai2
                               ,$jrawat_cek_nama2 ,$jrawat_cek_no2 ,$jrawat_cek_valid2 ,$jrawat_cek_bank2 ,$jrawat_cek_nilai2
                               ,$jrawat_transfer_bank2 ,$jrawat_transfer_nama2 ,$jrawat_transfer_nilai2
							   ,$jrawat_tunai_nilai2
                               ,$jrawat_voucher_no2 ,$jrawat_voucher_cashback2
                               ,$jrawat_cara3
                               ,$jrawat_kwitansi_no3 ,$jrawat_kwitansi_nilai3
                               ,$jrawat_card_nama3 ,$jrawat_card_edc3 ,$jrawat_card_promo3 ,$jrawat_card_no3 ,$jrawat_card_nilai3
                               ,$jrawat_cek_nama3 ,$jrawat_cek_no3 ,$jrawat_cek_valid3 ,$jrawat_cek_bank3 ,$jrawat_cek_nilai3
                               ,$jrawat_transfer_bank3 ,$jrawat_transfer_nama3 ,$jrawat_transfer_nilai3
							   ,$jrawat_tunai_nilai3
                               ,$jrawat_voucher_no3 ,$jrawat_voucher_cashback3
                               ,$bayar_date_create ,$jenis_transaksi ,$cetak, $jual_kwitansi_id, $jual_kwitansi_id2, $jual_kwitansi_id3
							   ,$jrawat_vgrooming_nilai,$jrawat_kwitansi_jenis,$jrawat_kwitansi_jenis2,$jrawat_kwitansi_jenis3){
        //delete all transaksi
        $sql="delete from jual_kwitansi where jkwitansi_ref='".$jrawat_nobukti."'";
        $this->db->query($sql);
        if($this->db->affected_rows()>-1){
            $sql="delete from jual_card where jcard_ref='".$jrawat_nobukti."'";
            $this->db->query($sql);
            if($this->db->affected_rows()>-1){
                $sql="delete from jual_cek where jcek_ref='".$jrawat_nobukti."'";
                $this->db->query($sql);
                if($this->db->affected_rows()>-1){
                    $sql="delete from jual_transfer where jtransfer_ref='".$jrawat_nobukti."'";
                    $this->db->query($sql);
                    if($this->db->affected_rows()>-1){
                        $sql="delete from jual_tunai where jtunai_ref='".$jrawat_nobukti."'";
                        $this->db->query($sql);
						if($this->db->affected_rows()>-1){
							$sql="delete from jual_vgrooming where jvgrooming_ref='".$jrawat_nobukti."'";
							$this->db->query($sql);						
							if($this->db->affected_rows()>-1){
								$sql="delete from voucher_terima where tvoucher_ref='".$jrawat_nobukti."'";
								$this->db->query($sql);
								if($this->db->affected_rows()>-1){
									$sql="delete from jual_gift_voucher where jgift_voucher_ref='".$jrawat_nobukti."'";
									$this->db->query($sql);
									if($this->db->affected_rows()>-1){
										if($jrawat_cara!=null || $jrawat_cara!=''){
											if($jrawat_kwitansi_nilai<>'' && $jrawat_kwitansi_nilai<>0){
												if($jrawat_kwitansi_jenis=="gv"){
													$result_bayar = $this->cara_bayar_giftvoucher_insert($jrawat_kwitansi_no
																								  ,$jrawat_kwitansi_nilai
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak
																								  ,$jual_kwitansi_id);
												}else{
													$result_bayar = $this->cara_bayar_kwitansi_insert($jrawat_kwitansi_no
																								  ,$jrawat_kwitansi_nilai
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak
																								  ,$jual_kwitansi_id);
												}
											}elseif($jrawat_card_nilai<>'' && $jrawat_card_nilai<>0){
												$result_bayar = $this->cara_bayar_card_insert($jrawat_card_nama
																							  ,$jrawat_card_edc
																							  ,$jrawat_card_promo
																							  ,$jrawat_card_no
																							  ,$jrawat_card_nilai
																							  ,$jrawat_nobukti
																							  ,$bayar_date_create
																							  ,$jenis_transaksi
																							  ,$cetak);
											}elseif($jrawat_cek_nilai<>'' && $jrawat_cek_nilai<>0){
												$result_bayar = $this->cara_bayar_cek_insert($jrawat_cek_nama
																							 ,$jrawat_cek_no
																							 ,$jrawat_cek_valid
																							 ,$jrawat_cek_bank
																							 ,$jrawat_cek_nilai
																							 ,$jrawat_nobukti
																							 ,$bayar_date_create
																							 ,$jenis_transaksi
																							 ,$cetak);
											}elseif($jrawat_transfer_nilai<>'' && $jrawat_transfer_nilai<>0){
												$result_bayar = $this->cara_bayar_transfer_insert($jrawat_transfer_bank
																								  ,$jrawat_transfer_nama
																								  ,$jrawat_transfer_nilai
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak);
											}elseif($jrawat_tunai_nilai<>'' && $jrawat_tunai_nilai<>0){
												$result_bayar = $this->cara_bayar_tunai_insert($jrawat_tunai_nilai
																							   ,$jrawat_nobukti
																							   ,$bayar_date_create
																							   ,$jenis_transaksi
																							   ,$cetak);
											}elseif($jrawat_vgrooming_nilai<>'' && $jrawat_vgrooming_nilai<>0){
												$result_bayar = $this->cara_bayar_vgrooming_insert($jrawat_vgrooming_nilai
																							   ,$jrawat_nobukti
																							   ,$bayar_date_create
																							   ,$jenis_transaksi
																							   ,$cetak);
											}elseif($jrawat_voucher_cashback<>'' && $jrawat_voucher_cashback<>0){
												$result_bayar = $this->cara_bayar_voucher_insert($jrawat_voucher_no
																								 ,$jrawat_nobukti
																								 ,$jrawat_voucher_cashback
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak);
											}
										}
										if($jrawat_cara2!=null || $jrawat_cara2!=''){
											if($jrawat_kwitansi_nilai2<>'' && $jrawat_kwitansi_nilai2<>0){
												if($jrawat_kwitansi_jenis2=="gv"){
													$result_bayar2 = $this->cara_bayar_giftvoucher_insert($jrawat_kwitansi_no2
																								  ,$jrawat_kwitansi_nilai2
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak
																								  ,$jual_kwitansi_id2);
												}else{
													$result_bayar2 = $this->cara_bayar_kwitansi_insert($jrawat_kwitansi_no2
																								  ,$jrawat_kwitansi_nilai2
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak
																								  ,$jual_kwitansi_id2);
												}
											}elseif($jrawat_card_nilai2<>'' && $jrawat_card_nilai2<>0){
												$result_bayar2 = $this->cara_bayar_card_insert($jrawat_card_nama2
																							  ,$jrawat_card_edc2
																							  ,$jrawat_card_promo2
																							  ,$jrawat_card_no2
																							  ,$jrawat_card_nilai2
																							  ,$jrawat_nobukti
																							  ,$bayar_date_create
																							  ,$jenis_transaksi
																							  ,$cetak);
											}elseif($jrawat_cek_nilai2<>'' && $jrawat_cek_nilai2<>0){
												$result_bayar2 = $this->cara_bayar_cek_insert($jrawat_cek_nama2
																							 ,$jrawat_cek_no2
																							 ,$jrawat_cek_valid2
																							 ,$jrawat_cek_bank2
																							 ,$jrawat_cek_nilai2
																							 ,$jrawat_nobukti
																							 ,$bayar_date_create
																							 ,$jenis_transaksi
																							 ,$cetak);
											}elseif($jrawat_transfer_nilai2<>'' && $jrawat_transfer_nilai2<>0){
												$result_bayar2 = $this->cara_bayar_transfer_insert($jrawat_transfer_bank2
																								  ,$jrawat_transfer_nama2
																								  ,$jrawat_transfer_nilai2
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak);
											}elseif($jrawat_tunai_nilai2<>'' && $jrawat_tunai_nilai2<>0){
												$result_bayar2 = $this->cara_bayar_tunai_insert($jrawat_tunai_nilai2
																							   ,$jrawat_nobukti
																							   ,$bayar_date_create
																							   ,$jenis_transaksi
																							   ,$cetak);
											}elseif($jrawat_voucher_cashback2<>'' && $jrawat_voucher_cashback2<>0){
												$result_bayar2 = $this->cara_bayar_voucher_insert($jrawat_voucher_no2
																								 ,$jrawat_nobukti
																								 ,$jrawat_voucher_cashback2
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak);
											}
										}
										if($jrawat_cara3!=null || $jrawat_cara3!=''){
											if($jrawat_kwitansi_nilai3<>'' && $jrawat_kwitansi_nilai3<>0){
												if($jrawat_kwitansi_jenis3=="gv"){
													$result_bayar3 = $this->cara_bayar_giftvoucher_insert($jrawat_kwitansi_no3
																								  ,$jrawat_kwitansi_nilai3
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak
																								  ,$jual_kwitansi_id3);
												}else{
													$result_bayar3 = $this->cara_bayar_kwitansi_insert($jrawat_kwitansi_no3
																								  ,$jrawat_kwitansi_nilai3
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak
																								  ,$jual_kwitansi_id3);
												}
											}elseif($jrawat_card_nilai3<>'' && $jrawat_card_nilai3<>0){
												$result_bayar3 = $this->cara_bayar_card_insert($jrawat_card_nama3
																							  ,$jrawat_card_edc3
																							  ,$jrawat_card_promo3
																							  ,$jrawat_card_no3
																							  ,$jrawat_card_nilai3
																							  ,$jrawat_nobukti
																							  ,$bayar_date_create
																							  ,$jenis_transaksi
																							  ,$cetak);
											}elseif($jrawat_cek_nilai3<>'' && $jrawat_cek_nilai3<>0){
												$result_bayar3 = $this->cara_bayar_cek_insert($jrawat_cek_nama3
																							 ,$jrawat_cek_no3
																							 ,$jrawat_cek_valid3
																							 ,$jrawat_cek_bank3
																							 ,$jrawat_cek_nilai3
																							 ,$jrawat_nobukti
																							 ,$bayar_date_create
																							 ,$jenis_transaksi
																							 ,$cetak);
											}elseif($jrawat_transfer_nilai3<>'' && $jrawat_transfer_nilai3<>0){
												$result_bayar3 = $this->cara_bayar_transfer_insert($jrawat_transfer_bank3
																								  ,$jrawat_transfer_nama3
																								  ,$jrawat_transfer_nilai3
																								  ,$jrawat_nobukti
																								  ,$bayar_date_create
																								  ,$jenis_transaksi
																								  ,$cetak);
											}elseif($jrawat_tunai_nilai3<>'' && $jrawat_tunai_nilai3<>0){
												$result_bayar3 = $this->cara_bayar_tunai_insert($jrawat_tunai_nilai3
																							   ,$jrawat_nobukti
																							   ,$bayar_date_create
																							   ,$jenis_transaksi
																							   ,$cetak);
											}elseif($jrawat_voucher_cashback3<>'' && $jrawat_voucher_cashback3<>0){
												$result_bayar3 = $this->cara_bayar_voucher_insert($jrawat_voucher_no3
																								 ,$jrawat_nobukti
																								 ,$jrawat_voucher_cashback3
																								 ,$bayar_date_create
																								 ,$jenis_transaksi
																								 ,$cetak);
											}
										}
									}
								}
							}
						}
					}
                }
            }
        }
    }
	
	function get_piutang_cust_list($query,$start,$end){
		/*$sql="SELECT lpiutang_id
				,lpiutang_cust
				,cust_nama
				,cust_no
				,SUM(lpiutang_total) AS lpiutang_total
				,SUM(lpiutang_sisa) AS lpiutang_sisa
			FROM master_lunas_piutang
				LEFT JOIN customer ON(cust_id=lpiutang_cust)";*/
		$sql = "SELECT master_lunas_piutang.lpiutang_id
				,master_lunas_piutang.lpiutang_cust
				,cust_nama
				,cust_no
				,sum(lpiutang_total) AS lpiutang_total
				/*,(sum(lpiutang_total)) - ifnull(sum(vu_piutang_total_lunas.total_pelunasan),0) AS lpiutang_sisa*/
				,sum(lpiutang_sisa) AS lpiutang_sisa
			FROM master_lunas_piutang 
				LEFT JOIN vu_piutang_total_lunas ON(vu_piutang_total_lunas.dpiutang_master=master_lunas_piutang.lpiutang_id)
				LEFT JOIN customer ON(cust_id=master_lunas_piutang.lpiutang_cust)
				WHERE master_lunas_piutang.lpiutang_stat_dok = 'Terbuka' AND lpiutang_faktur_tanggal > '2010-07-20' ";
		
		if($query<>""){
			$sql .=eregi("WHERE",$sql)? " AND ":" WHERE ";
			$sql .= " (cust_nama LIKE '%".addslashes($query)."%' OR cust_no LIKE '%".addslashes($query)."%' )";
		}
		$sql.=" GROUP BY lpiutang_cust HAVING lpiutang_sisa > 0 ";
		
		$result = $this->db->query($sql);
		$nbrows = $result->num_rows();
		$limit = $sql." LIMIT ".$start.",".$end;		
		$result = $this->db->query($limit);  
		
		if($nbrows>0){
			foreach($result->result() AS $row){
				$arr[] = $row;
			}
			$jsonresult = json_encode($arr);
			return '({"total":"'.$nbrows.'","results":'.$jsonresult.'})';
		} else {
			return '({"total":"0", "results":""})';
		}
		
	}
	
	function cek_stok_cutoff($produk_id, $tgl_awal){
	
		$sql_produk="select p.produk_id, p.produk_kode, p.produk_nama, p.produk_tgl_nilai_saldo_awal,
						if( p.produk_tgl_nilai_saldo_awal>'$tgl_awal' , '1', '0') AS produk_cutoff
					from produk p where p.produk_id='$produk_id'";
		
		$query = $this->db->query($sql_produk);
		$data_produk = $query->row();
		
		return '({"status":"'.$data_produk->produk_cutoff.'","produk":"'.$data_produk->produk_nama.' ('.$data_produk->produk_kode.')","tanggal_cut":"'.date('d-m-Y',strtotime($data_produk->produk_tgl_nilai_saldo_awal)).'"})';
	}
	
	function cek_stok_mutasi_cutoff($produk_id, $group1_id, $group2_id, $tgl_awal, $opsi_produk, $jenis, $modul, $cabang=''){
		$t_info = $this->get_info();
		$t_brand = $this->get_cabang_brand();
		
		if($cabang == $t_info->info_nama or $cabang == '')
			$this->cabang = $this->db;
		else 
			$this->cabang = $this->load->database($cabang."2", TRUE);

		$sql_produk="SELECT p.produk_id, p.produk_kode, p.produk_nama, p.produk_tgl_nilai_saldo_awal,
						if( p.produk_tgl_nilai_saldo_awal>'$tgl_awal' , '1', '0') AS produk_cutoff
					FROM produk p WHERE p.produk_aktif='Aktif' ";
		
		if($jenis!=''){
			$sql_produk.=eregi("WHERE",$sql_produk)?" AND ":" WHERE ";
			$sql_produk.="	produk_kategori='".$jenis."' ";
		}
		if($opsi_produk=='group1' & $group1_id!=="" & $group1_id!==0){
			$sql_produk.=eregi("WHERE",$sql_produk)?" AND ":" WHERE ";
			$sql_produk.="	produk_group='".$group1_id."' ";
		}elseif($opsi_produk=='group2' & $group2_id!=="" & $group2_id!==0){
			$sql_produk.=eregi("WHERE",$sql_produk)?" AND ":" WHERE ";
			$sql_produk.="	produk_jenis='".$group2_id."' ";
		}elseif($opsi_produk=='produk' & $produk_id!=="" & $produk_id!==0){
			$sql_produk.=eregi("WHERE",$sql_produk)?" AND ":" WHERE ";
			$sql_produk.="	produk_id='".$produk_id."' ";
		}
		$query = $this->cabang->query($sql_produk);
		$nbrows = $query->num_rows();
		
		$str_ex='';	
		if($nbrows>0){
			foreach($query->result() AS $row){
				if($modul=="lama"){
					if($row->produk_cutoff=='0' && $row->produk_tgl_nilai_saldo_awal!='0000-00-00' && $row->produk_tgl_nilai_saldo_awal!=null){
						$str_ex.='{"produk":"'.$row->produk_nama.' ('.$row->produk_kode.')","tanggal_cut":"'.date('d-m-Y',strtotime($row->produk_tgl_nilai_saldo_awal)).'"},';
					}
				}else{
					if($row->produk_cutoff=='1' && $row->produk_tgl_nilai_saldo_awal!='0000-00-00' && $row->produk_tgl_nilai_saldo_awal!=null){
						$str_ex.='{"produk":"'.$row->produk_nama.' ('.$row->produk_kode.')","tanggal_cut":"'.date('d-m-Y',strtotime($row->produk_tgl_nilai_saldo_awal)).'"},';
					}
				}
			}
			if($str_ex){
				$str_ex='['.substr($str_ex,0,-1).']';
				return $str_ex;
			}else{
				return '[]';
			}
		}else{
			return '[]';
		}
	}	
}
?>