<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Cetak Penjualan Produk</title>
		<style type="text/css">
			body{
			margin:0px;margin-right:4px;
			}
			html,body,table,tr,td{
			font-family:Geneva, Arial, Helvetica, sans-serif;
			font-size:40px;
			}
			.title{
			font-size:40px;
			}
			.pagebreak {
			page-break-after: always;
			}
			
			@media print {
			html,body,table,tr,td {
			letter-spacing: 5px;
			}
			
			}
		</style>
	</head>
	<body onload="window.print();window.close(); ">
		<br>
		<?php
			function myheader($jproduk_tanggal ,$cust_member_no ,$cust_nama ,$cust_alamat ,$jproduk_nobukti, $f_jproduk_jam, $jproduk_karyawan, $jproduk_karyawan_no, $jproduk_creator, $total_brg, $cust_preward_total){
			?>
			
			<table style="font-size:30px" width="800px" height="85px" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="800px" valign="bottom">
						<table width="800px" border="0" cellspacing="0" cellpadding="0">					
							<tr>
								<td width="480px" align = "left">Faktur : <?=$jproduk_nobukti;?></td>
								<td width="350px" align = "left"><?=$f_jproduk_jam;?></td>
							</tr>							
							<tr>    								
								<td width="480px" align = "left">Ksr: <?=$_SESSION[SESSION_USERID];?></td>
								<td width="350px" align = "left">Qty: <?=$total_brg;?></td>
							</tr>	
							<tr>
								<td width="525px" align = "left"><?php if($cust_member_no<>0){?>Member : <?=$cust_member_no;?><?php }?></td>
								<td width="350px" align = "left"><?php if($cust_member_no<>0){?>Point: <?=floor($cust_preward_total);?><?php }?></td>
							</tr>	
							<tr>
								<td colspan="2" valign="top" align="center">------------------------------------------------------------</td>
							</tr>							
						</table>
					</td>
				</tr>
			</table>
			<?php
			}
		?>
		<?php
			function content_header(){
			?>
			<table width="800px" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="800px" valign="top"><table width="800px" border="0" cellspacing="0" cellpadding="0">
						<?php
						}
					?>
					<?php
						/* data header */
						$f_jproduk_tanggal = $jproduk_tanggal;
						$f_iklantoday_keterangan = $iklantoday_keterangan;
						$f_cust_member_no = $cust_member_no;
						$f_cust_nama = $cust_nama;
						$f_cust_alamat = $cust_alamat;
						$f_jproduk_nobukti = $jproduk_nobukti;
						$f_jproduk_jam = $jproduk_jam;
						$f_jproduk_karyawan = $jproduk_karyawan;
						$f_jproduk_karyawan_no = $jproduk_karyawan_no;
						$f_jproduk_creator = $jproduk_creator;
						
						
						/* data footer */
						$f_cara_bayar1 = $cara_bayar1;
						$f_nilai_bayar1 = $nilai_bayar1;
						$f_cara_bayar2 = $cara_bayar2;
						$f_nilai_bayar2 = $nilai_bayar2;
						$f_cara_bayar3 = $cara_bayar3;
						$f_nilai_bayar3 = $nilai_bayar3;
						$cust_preward_total = $cust_preward_total;
						
						$i=0;
						$total=0;
						$total_brg=0;
						$subtotal=0;
						$total_diskon=0;
						$total_diskon_tamb_tamb=0;
						$total_voucher=0;
						
						$dcount = sizeof($detail_jproduk);
						foreach($detail_jproduk as $list => $row){							
							$i+=1;
							$total_brg+= $row->dproduk_jumlah;
							if(($i%14)==1){
								myheader($f_jproduk_tanggal ,$f_cust_member_no ,$f_cust_nama ,$f_cust_alamat ,$f_jproduk_nobukti, $f_jproduk_jam, $f_jproduk_karyawan, $f_jproduk_karyawan_no, $f_jproduk_creator,$total_brg, $cust_preward_total);
								content_header();
							}
						?>
						<tr>
							<td width="800px" colspan="3"><?=$row->produk_nama;?><!--&nbsp;(<?//=$row->produk_kode?>)!--></td>
						</tr>
						<tr>
							<td width="350px">&nbsp;&nbsp;&nbsp;<?=$row->dproduk_jumlah;?> x <?=rupiah(($row->dproduk_harga));?></td>
							<td align="right">&nbsp;<?=rupiah(($row->dproduk_jumlah)*($row->dproduk_harga));?></td>
						</tr>
						<tr>
							<td width="350px">&nbsp;&nbsp;&nbsp;Disk&nbsp;<?=$row->dproduk_diskon?>% &nbsp;-<?=rupiah((($row->dproduk_diskon)/100)*$row->dproduk_harga);?></td>
						</tr>
						<?php 
							$subtotal+=(($row->dproduk_jumlah)*($row->dproduk_harga));
							$total_diskon+= (($row->dproduk_diskon)/100)*$row->dproduk_harga;
							
							if($i==$dcount){
								$total=$subtotal-$total_diskon-$jproduk_cashback-$potong_point;
								
								$total_diskon_tamb=($subtotal*($jproduk_diskon/100));
								$total_voucher= $jproduk_cashback;
								
								content_footer();
								myfooter($subtotal ,$total,$total_diskon ,$f_cara_bayar1 ,$f_nilai_bayar1 ,$f_cara_bayar2 ,$f_nilai_bayar2 ,$f_cara_bayar3 ,$f_nilai_bayar3
								,$total_voucher ,$total_diskon_tamb, $f_iklantoday_keterangan, $f_jproduk_jam, $f_jproduk_creator,$total_brg, $cust_preward_total, $potong_point,$cust_member_no);
							}elseif(($i>1) && ($i%14==0)){
								content_footer();
								echo "<div class='pagebreak'></div>";
							}
						}
					?>
					<?php
						function content_footer(){
						?>
					</table></td>
				</tr>  
			</table>
			<?php
			}
		?>
		<?php
			function myfooter($subtotal ,$total,$total_diskon ,$cara_bayar1 ,$nilai_bayar1 ,$cara_bayar2 ,$nilai_bayar2 ,$cara_bayar3 ,$nilai_bayar3
			,$total_voucher ,$total_diskon_tamb, $f_iklantoday_keterangan, $f_jproduk_jam, $f_jproduk_creator,$total_brg, $cust_preward_total, $potong_point,$cust_member_no){
			?>
			<table width="800px" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="30px" colspan="2" align="right">&nbsp;&nbsp; -------------</td>
				</tr>
				
				<tr>
					<td width="350px" align = "left">Sub Total</td>
					<td width="270px" align="right"><?=rupiah($subtotal);?></td>
				</tr>
				<tr>
					<td width="500px" align = "left"><?php if($total_diskon<>0){?>Total Diskon<?php }?></td>
					<td width="270px" align="right"><?php if($total_diskon<>0){?>-<?=rupiah($total_diskon);?><?php }?></td>
				</tr>	
				<tr>
					<td width="500px" align = "left"><?php if($total_voucher<>0){?>&nbsp;Potongan <?php }?></td>
					<td width="270px" align="right"><?php if($total_voucher<>0){?>-<?=rupiah($total_voucher);?><?php }?></td>
				</tr>
				<tr>
					<td width="500px" align = "left"><?php if($cust_member_no<>0 && $potong_point<>0){?>Tukar Poin <?php }?></td>
					<td width="270px" align="right"><?php if($cust_member_no<>0 && $potong_point<>0){?>-<?=rupiah($potong_point);?><?php }?></td>
				</tr>
				<tr>
					<td width="350px" align = "left"><?php if($total_diskon_tamb<>0){?>Diskon <?php }?></td>
					<td width="270px" align="right"><?php if($total_diskon_tamb<>0){?>-<?=rupiah($total_diskon_tamb);?><?php }?></td>
				</tr>
				<tr>
					<td height="30px" colspan="2" align="right">&nbsp;&nbsp; -------------</td>
				</tr>
				<tr>
					<td width="350px" align = "left">Grand Total</td>
					<td width="270px" align="right"><?=rupiah($total);?></td>
				</tr>
				<tr>	
					<td width="500px" align = "left"><?php if($cara_bayar1<>''){?><?=$cara_bayar1;?><?php }?></td>
					<td width="270px" align="right"><?php if($cara_bayar1<>''){?><?=rupiah($nilai_bayar1);?><?php }?></td>
				</tr>				
				<tr>	
					<td width="500px" align = "left"><?php if($cara_bayar2<>''){?><?=$cara_bayar2;?><?php }?></td>
					<td width="270px" align="right"><?php if($cara_bayar2<>''){?><?=rupiah($nilai_bayar2);?><?php }?></td>
				</tr>			
				<tr>	
					<td width="500px" align = "left"><?php if($cara_bayar3<>''){?><?=$cara_bayar3;?><?php }?></td>
					<td width="270px" align="right"><?php if($cara_bayar3<>''){?><?=rupiah($nilai_bayar3);?><?php }?></td>
				</tr>			
				<tr>
					<td align="center">&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td height="30px" colspan="2" align="center">*<?=$f_iklantoday_keterangan;?>*</td>
				</tr>
			</table>
			<?php
			}
		?>
	</body>
</html>
