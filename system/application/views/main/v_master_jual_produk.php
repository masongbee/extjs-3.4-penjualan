<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<style type="text/css">
        p { width:650px; }
		.search-item {
			font:normal 11px tahoma, arial, helvetica, sans-serif;
			padding:3px 10px 3px 10px;
			border:1px solid #fff;
			border-bottom:1px solid #eeeeee;
			white-space:normal;
			color:#555;
		}
		.search-item h3 {
			display:block;
			font:inherit;
			font-weight:bold;
			color:#222;
		}
		
		.search-item h3 span {
			float: right;
			font-weight:normal;
			margin:0 0 5px 5px;
			width:100px;
			display:block;
			clear:none;
		}
		.rmoney_hutang5 {text-align:left; font-size:20px; font-weight:bold; color:red}
		.rmoney5 {text-align:left; font-size:20px; font-weight:bold;}
    </style>
<script>
var master_jual_produk_DataStore;
var kwitansi_jual_produk_DataStore;
var card_jual_produk_DataStore;
var cek_jual_produk_DataStore;
var transfer_jual_produk_DataStore;

var master_jual_produk_ColumnModel;
var master_jual_produkListEditorGrid;
var master_jual_produk_confirmForm;
var master_jual_produk_confirmWindow;
var master_jual_produk_createForm;
var master_jual_produk_createWindow;
var master_jual_produk_searchForm;
var master_jual_produk_searchWindow;
var master_jual_produk_SelectedRow;
var master_jual_produk_ContextMenu;

var detail_jual_produk_DataStore;
var detail_jual_produkListEditorGrid;
var detail_jual_produk_ColumnModel;
var detail_jual_produk_proxy;
var detail_jual_produk_writer;
var detail_jual_produk_reader;
var editor_detail_jual_produk;
var arr_dproduk_id=[];

var app_cust_namaBaruField;
var member_no_baruField;
var app_cust_hpBaruField;
var member_ultah_baruField;
var app_cust_keteranganBaruField;

var jproduk_post2db = '';
var msg = '';
var jproduk_pageS=30;
var today = new Date().format('d-m-Y');
var today_cekvalid=new Date().format('Y-m-d');
var today_ultah_produk = new Date().format('m-d');
var this_year = new Date().format('Y');

var MIN_CREATE_DATE="<?php echo $this->m_public_function->get_permission_3bulan($_SESSION[SESSION_GROUPID],37); ?>";
var MAX_UNPOSTING="<?php echo $this->m_public_function->add_date(date('Y-m-d'),-365,'day') ?>";
var MAX_UNPOST_BULANAN="<?php echo $this->m_public_function->get_tutup_bulanan(); ?>";
var cabang = "<?php echo $this->m_public_function->get_cabang_brand(); ?>";

var jproduk_idField;
var jproduk_nobuktiField;
var jproduk_custField;
var jproduk_tanggalField;
var jproduk_diskonField;
var jproduk_ket_diskField;
var jproduk_bayarField;
var jproduk_caraField;
var jproduk_cara2Field;
var jproduk_cara3Field;
var jproduk_usernameField;
var jproduk_passwordField;
var jproduk_keteranganField;
var vgrooming_jual_produk_DataStore;
//tunai
var jproduk_tunai_nilaiField;
//tunai-2
var jproduk_tunai_nilai2Field;
//tunai-3
var jproduk_tunai_nilai3Field;
//voucher
var jproduk_voucher_noField;
var jproduk_voucher_cashbackField;
//voucher-2
var jproduk_voucher_no2Field;
var jproduk_voucher_cashback2Field;
//voucher-3
var jproduk_voucher_no3Field;
var jproduk_voucher_cashback3Field;

var jproduk_cashbackField;
var jproduk_potong_pointField;
var is_member=false;
//kwitansi
var jproduk_kwitansi_namaField;
var jproduk_kwitansi_nilaiField;
var jproduk_kwitansi_noField;
//kwitansi-2
var jproduk_kwitansi_nama2Field;
var jproduk_kwitansi_nilai2Field;
var jproduk_kwitansi_no2Field;
//kwitansi-3
var jproduk_kwitansi_nama3Field;
var jproduk_kwitansi_nilai3Field;
var jproduk_kwitansi_no3Field;

//card
var jproduk_card_namaField;
var jproduk_card_edcField;
var jproduk_card_promoField;
var jproduk_card_noField;
var jproduk_card_nilaiField;
//card-2
var jproduk_card_nama2Field;
var jproduk_card_edc2Field;
var jproduk_card_promo2Field;
var jproduk_card_no2Field;
var jproduk_card_nilai2Field;
//card-3
var jproduk_card_nama3Field;
var jproduk_card_edc3Field;
var jproduk_card_promo3Field;
var jproduk_card_no3Field;
var jproduk_card_nilai3Field;

//cek
var jproduk_cek_namaField;
var jproduk_cek_noField;
var jproduk_cek_validField;
var jproduk_cek_bankField;
var jproduk_cek_nilaiField;
//cek-2
var jproduk_cek_nama2Field;
var jproduk_cek_no2Field;
var jproduk_cek_valid2Field;
var jproduk_cek_bank2Field;
var jproduk_cek_nilai2Field;
//cek-3
var jproduk_cek_nama3Field;
var jproduk_cek_no3Field;
var jproduk_cek_valid3Field;
var jproduk_cek_bank3Field;
var jproduk_cek_nilai3Field;

//transfer
var jproduk_transfer_bankField;
var jproduk_transfer_namaField;
var jproduk_transfer_nilaiField;
//transfer-2
var jproduk_transfer_bank2Field;
var jproduk_transfer_nama2Field;
var jproduk_transfer_nilai2Field;
//transfer-3
var jproduk_transfer_bank3Field;
var jproduk_transfer_nama3Field;
var jproduk_transfer_nilai3Field;

var jproduk_idSearchField;
var jproduk_nobuktiSearchField;
var jproduk_custSearchField;
var jproduk_tanggalSearchField;
var jproduk_tanggal_akhirSearchField;
var jproduk_diskonSearchField;
var jproduk_caraSearchField;
var jproduk_keteranganSearchField;
var jproduk_stat_dokSearchField;
var dt= new Date();

var cetak_jproduk=0;
var looping=0;

var t_carabayar1 = "";
var t_carabayar2 = "";

var t_member_level = 0;
var t_member_type = '';

Ext.onReady(function(){
  	Ext.QuickTips.init();
	
	var map = new Ext.KeyMap( Ext.getBody(),[
		{	key: 35,	//END untuk FINAL
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_pembayaranWindow.isVisible()){ //if sedang ada di panel pembayaran (panel pembayaran terbuka/visible) 
					save_andPrint();					
				}else{
					master_jual_produk_pembayaranWindow.show();
					jproduk_uang_cfField.focus(true,500);
				}
			}
		},
		{	key: 112,	//pencet F1 untuk LIHAT DAFTAR 
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_createWindow.isVisible()==false){ //if tidak sedang di window LIST penjualan (LIST penjualan tidak terbuka), berarti sedang di menu ADD PENJUALAN PRODUK
					show_windowGrid();
				}else{
					display_form_window();
				}
			}
		},
		{	key: 113,	//pencet F2 untuk UBAH
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_createWindow.isVisible()==true){  //if sedang di window LIST penjualan
					master_jual_produk_confirm_update();
				}
			}
		},
		{	key: 114,	//pencet F3 untuk RESET
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_createWindow.isVisible()==false){ //if tidak sedang di window LIST penjualan (LIST penjualan tidak terbuka), berarti sedang di menu ADD PENJUALAN PRODUK
					jproduk_btn_cancel();
				}
			}
		},
		{	key: 115,	//pencet F4 untuk TUNDA
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_createWindow.isVisible()==false){ //if tidak sedang di window LIST penjualan (LIST penjualan tidak terbuka), berarti sedang di menu ADD PENJUALAN PRODUK
					save_button();
				}
				
			}
		},
		{	key: 46,	//pencet DELETE untuk HAPUS detail barang
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_createWindow.isVisible()==false){ //if tidak sedang di window LIST penjualan (LIST penjualan tidak terbuka), berarti sedang di menu ADD PENJUALAN PRODUK
					detail_jual_produk_confirm_delete();
				}
			}
		},
		{	key: 13,	//pencet ENTER untuk masuk ke GRID detail LIST PENJUALAN
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_createWindow.isVisible()==true){ //if sedang di window LIST penjualan
				
					//alert(master_jual_produkListEditorGrid.getSelectionModel().getSelected());
					master_jual_produkListEditorGrid.getSelectionModel().selectRow(0);
				}
			}
		},
		{	key: 38,	//pencet panah atas di GRID detail LIST PENJUALAN
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_createWindow.isVisible()==true){ //if sedang di window LIST penjualan
					var rec = master_jual_produkListEditorGrid.getSelectionModel().getSelected();
					var index = master_jual_produkListEditorGrid.getStore().indexOf(rec);
					master_jual_produkListEditorGrid.getSelectionModel().selectRow(index-1);
				}
			}
		},
		{	key: 40,	//pencet panah bawah di GRID detail LIST PENJUALAN
			//ctrl:true,
			fn: function(e, b){
				b.preventDefault();
				if(master_jual_produk_createWindow.isVisible()==true){ //if sedang di window LIST penjualan
					var rec = master_jual_produkListEditorGrid.getSelectionModel().getSelected();
					var index = master_jual_produkListEditorGrid.getStore().indexOf(rec);
					master_jual_produkListEditorGrid.getSelectionModel().selectRow(index+1);
				}
			}
		}
	]);
			
  	Ext.util.Format.comboRenderer = function(combo){
  	    return function(value){
  	        var record = combo.findRecord(combo.valueField, value);
  	        return record ? record.get(combo.displayField) : combo.valueNotFoundText;
  	    }
  	}

/*Function utk ReadOnly */
Ext.override(Ext.form.Field, {
    setReadOnly: function(readOnly) {
        if (readOnly == this.readOnly) {
            return;
        }
        this.readOnly = readOnly;
        if (readOnly) {
            this.el.dom.setAttribute('readOnly', true);
        } else {
            this.el.dom.removeAttribute('readOnly');
        }
    }
});	

	
	jproduk_cust_idField= new Ext.form.NumberField();
	jproduk_karyawan_idField = new Ext.form.NumberField();
	
	function jproduk_cetak(master_id){ 
		Ext.MessageBox.show({
		   msg: 'Sedang memproses data, mohon tunggu...',
		   progressText: 'proses...',
		   width:350,
		   wait:true
		});	
	
		Ext.Ajax.request({   
			waitMsg: 'Mohon tunggu...',
			url: 'index.php?c=c_master_jual_produk&m=print_paper',
			params: { jproduk_id : master_id}, 
			success: function(response){              
				var result=eval(response.responseText);
				switch(result){
				case 1:
					win = window.open('./jproduk_paper.html','Cetak Penjualan Produk','height=480,width=415,resizable=1,scrollbars=0, menubar=0');
					//jproduk_btn_cancel();
					Ext.MessageBox.hide();
					break;
				default:
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Unable to print the grid!',
						buttons: Ext.MessageBox.OK,
						animEl: 'save',
						icon: Ext.MessageBox.WARNING
					});
					break;
				}  
			},
			failure: function(response){
				var result=response.responseText;
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: 'Could not connect to the database. retry later.',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});		
			} 	                     
		});
	}
	
	function jproduk_cetak2(master_id){ 
		Ext.Ajax.request({   
			waitMsg: 'Mohon tunggu...',
			url: 'index.php?c=c_master_jual_produk&m=print_paper2',
			params: { jproduk_id : master_id}, 
			success: function(response){              
				var result=eval(response.responseText);
				switch(result){
				case 1:
					win = window.open('./jproduk_paper2.html','Cetak Penjualan Produk','height=480,width=415,resizable=1,scrollbars=0, menubar=0');
					jproduk_btn_cancel();
					break;
				default:
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Unable to print the grid!',
						buttons: Ext.MessageBox.OK,
						animEl: 'save',
						icon: Ext.MessageBox.WARNING
					});
					break;
				}  
			},
			failure: function(response){
				var result=response.responseText;
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: 'Could not connect to the database. retry later.',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});		
			} 	                     
		});
	}
	
	function jproduk_cetak_print_only(master_id){ 
		Ext.Ajax.request({   
			waitMsg: 'Mohon tunggu...',
			url: 'index.php?c=c_master_jual_produk&m=print_only',
			params: { jproduk_id : master_id}, 
			success: function(response){              
				var result=eval(response.responseText);
				switch(result){
				case 1:
					win = window.open('./jproduk_paper.html','Cetak Penjualan Produk','height=480,width=415,resizable=1,scrollbars=0, menubar=0');
					jproduk_btn_cancel();
					break;
				default:
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Unable to print the grid!',
						buttons: Ext.MessageBox.OK,
						animEl: 'save',
						icon: Ext.MessageBox.WARNING
					});
					break;
				}  
			},
			failure: function(response){
				var result=response.responseText;
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: 'Could not connect to the database. retry later.',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});		
			} 	                     
		});
	}
	
	function jproduk_cetak_print_only2(master_id,customer_id,tanggal, option){ 
		Ext.Ajax.request({   
			waitMsg: 'Mohon tunggu...',
			url: 'index.php?c=c_master_jual_produk&m=print_only2',
			params: { jproduk_id : master_id,jproduk_cust : customer_id ,jproduk_tanggal : tanggal, option : option}, 
			success: function(response){              
				var result=eval(response.responseText);
				switch(result){
				case 1:
					Ext.MessageBox.hide(); 
					win = window.open('./jproduk_paper2.html','Cetak Penjualan Produk','height=480,width=415,resizable=1,scrollbars=0, menubar=0');
					jproduk_btn_cancel();
					break;
				case 2:
					Ext.MessageBox.hide(); 
					win = window.open('./jproduk_print_fp.html','Cetak Penjualan Produk','height=480,width=1240,resizable=1,scrollbars=0, menubar=0');
					break;
				default:
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Unable to print the grid!',
						buttons: Ext.MessageBox.OK,
						animEl: 'save',
						icon: Ext.MessageBox.WARNING
					});
					break;
				}  
			},
			failure: function(response){
				var result=response.responseText;
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: 'Could not connect to the database. retry later.',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});		
			} 	                     
		});
	}

	jproduk_usernameField = new Ext.form.TextField({
		id: 'jproduk_usernameField',
		fieldLabel: 'Username',
		allowBlank: true,
		anchor: '95%'
	});
	
	jproduk_passwordField = new Ext.form.TextField({
		id: 'jproduk_passwordField',
		fieldLabel:'Password',
		anchor: '95%',
		inputType:'password',
		enableKeyEvents: true,
		allowBlank:true,
		listeners: {
            specialkey: function(field, e){
                if (e.getKey() == e.ENTER) {
                    cek_userpass();
                }
            }
        }
	});
	
	master_jual_produk_confirmForm = new Ext.FormPanel({
		labelAlign: 'left',
		bodyStyle:'padding:5px',
		autoHeight:true,
		//height:680,
		width: 350,       
		items: [jproduk_usernameField, jproduk_passwordField]
		,buttons: [
			{
				text: 'OK',
				ref: '../okeButton',
				handler: cek_userpass
			},
			{
				text: 'Batal',
				handler: function(){
					master_jual_produk_confirmWindow.hide();
				}
			}
		]
	});
	
	master_jual_produk_confirmWindow = new Ext.Window({
		id: 'master_jual_produk_confirmWindow',
		title: 'Verifikasi Pembatalan Faktur',
		closable:true,
		closeAction: 'hide',
		autoWidth: true,
		autoHeight: true,
		x: 400,
		y: 450,
		plain:true,
		layout: 'fit',
		modal: true,
		renderTo: 'elwindow_master_jual_produk_confirm',
		items: master_jual_produk_confirmForm
	});
	
	function cek_userpass(){
		if(jproduk_usernameField.getValue() && jproduk_passwordField.getValue()){
			Ext.MessageBox.show({
			   msg: 'Sedang memproses data, mohon tunggu...',
			   progressText: 'proses...',
			   width:350,
			   wait:true
			});
			Ext.Ajax.request({
				waitMsg: 'Silahkan Tunggu...',
				url: 'index.php?c=c_master_jual_produk&m=cek_userpass',
				params:{
					username : jproduk_usernameField.getValue(),
					password : jproduk_passwordField.getValue()
				},
				success:function(response){
					var result=eval(response.responseText);
					switch(result){
					case 1:
						master_jual_produk_create();
						master_jual_produk_confirmWindow.hide();
						break;
					default:
						Ext.Msg.alert('Gagal', 'Pembatalan Faktur tidak dapat dilakukan. Silakan cek username dan password yang anda masukkan.' );
						break;
					} 						
				},
				failure:function(response){
					Ext.MessageBox.show({
					   title: 'Error',
					   msg: 'Could not connect to the database. retry later.',
					   buttons: Ext.MessageBox.OK,
					   animEl: 'database',
					   icon: Ext.MessageBox.ERROR
					});	
				}
			});
		}else{
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Silakan lengkapi username atau password terlebih dahulu!',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
		}
	}
	
	jproduk_username_hutangField = new Ext.form.TextField({
		id: 'jproduk_username_hutangField',
		fieldLabel: 'Username',
		allowBlank: true,
		anchor: '95%'
	});
	
	jproduk_password_hutangField = new Ext.form.TextField({
		id: 'jproduk_password_hutangField',
		fieldLabel:'Password',
		anchor: '95%',
		inputType:'password',
		enableKeyEvents: true,
		allowBlank:true,
		listeners: {
            specialkey: function(field, e){
                if (e.getKey() == e.ENTER) {
                    cek_userpass_hutang();
                }
            }
        }
	});
	
	master_jual_produk_confirm_hutangForm = new Ext.FormPanel({
		labelAlign: 'left',
		bodyStyle:'padding:5px',
		autoHeight:true,
		//height:680,
		width: 350,       
		items: [jproduk_username_hutangField, jproduk_password_hutangField]
		,buttons: [
			{
				text: 'OK',
				ref: '../okeButton',
				handler: cek_userpass_hutang
			},
			{
				text: 'Batal',
				handler: function(){
					master_jual_produk_confirm_hutangWindow.hide();
					Ext.MessageBox.hide();
				}
			}
		]
	});
	
	master_jual_produk_confirm_hutangWindow = new Ext.Window({
		id: 'master_jual_produk_confirm_hutangWindow',
		title: 'Verifikasi Faktur Hutang',
		closable:true,
		closeAction: 'hide',
		autoWidth: true,
		autoHeight: true,
		x: 400,
		y: 450,
		plain:true,
		layout: 'fit',
		modal: true,
		renderTo: 'elwindow_master_jual_produk_confirm_hutang',
		items: master_jual_produk_confirm_hutangForm
	});
	
	function cek_userpass_hutang(){
		if(jproduk_username_hutangField.getValue() && jproduk_password_hutangField.getValue()){
			Ext.MessageBox.show({
			   msg: 'Sedang memproses data, mohon tunggu...',
			   progressText: 'proses...',
			   width:350,
			   wait:true
			});
			Ext.Ajax.request({
				waitMsg: 'Silahkan Tunggu...',
				url: 'index.php?c=c_master_jual_produk&m=cek_userpass',
				params:{
					username : jproduk_username_hutangField.getValue(),
					password : jproduk_password_hutangField.getValue()
				},
				success:function(response){
					var result=eval(response.responseText);
					switch(result){
					case 1:
						master_jual_produk_create();
						master_jual_produk_create.create_jual_produk('yes');
						master_jual_produk_confirm_hutangWindow.hide();
						break;
					default:
						Ext.Msg.alert('Gagal', 'Faktur tidak dapat disimpan. Silakan cek username dan password yang anda masukkan.' );
						break;
					} 						
				},
				failure:function(response){
					Ext.MessageBox.show({
					   title: 'Error',
					   msg: 'Could not connect to the database. retry later.',
					   buttons: Ext.MessageBox.OK,
					   animEl: 'database',
					   icon: Ext.MessageBox.ERROR
					});	
				}
			});
		}else{
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Silakan lengkapi username atau password terlebih dahulu!',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
		}
	}
  
	function pengecekan_dokumen(){
		if(jproduk_subTotalField.getValue() < jproduk_cashbackField.getValue()){
			Ext.MessageBox.show({
				   title: 'Warning',
				   msg: 'Data Penjualan Produk tidak bisa disimpan, karena jumlah Voucher melebihi Sub Total.',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'save',
				   icon: Ext.MessageBox.WARNING
				});
			return;
		}
	
		var jproduk_tanggal_create_date = "";
		if(jproduk_tanggalField.getValue()!== ""){jproduk_tanggal_create_date = jproduk_tanggalField.getValue().format('Y-m-d');} 
		if(jproduk_stat_dokField.getValue()!='Batal'){
			Ext.MessageBox.show({
			   msg: 'Sedang memproses data, mohon tunggu...',
			   progressText: 'proses...',
			   width:350,
			   wait:true
			});	
		}
		Ext.Ajax.request({  
			waitMsg: 'Silahkan Tunggu...',
			url: 'index.php?c=c_master_jual_produk&m=get_action',
			params: {
				task: "CEK",
				tanggal_pengecekan	: jproduk_tanggal_create_date
			}, 
			success: function(response){							
				var result=eval(response.responseText);
				switch(result){
					case 1:
						if (jproduk_diskonField.getValue()!=0 && jproduk_cashback_cfField.getValue()!=0){
							Ext.MessageBox.show({
								title: 'Warning',
								msg: 'Diskon tambahan dan Voucher hanya bisa diisi salah satu',
								buttons: Ext.MessageBox.OK,
								animEl: 'save',
								icon: Ext.MessageBox.WARNING
							});
						}else if(jproduk_stat_dokField.getValue()=='Batal'){
							jproduk_usernameField.setValue('');
							jproduk_passwordField.setValue('');
							master_jual_produk_confirmWindow.show();
						}
						else
						{
							master_jual_produk_create();
						}
						break;
					default:
						Ext.MessageBox.show({
						   title: 'Warning',
						   msg: 'Data Penjualan Produk tidak bisa disimpan, karena telah melebihi batas hari yang diperbolehkan ',
						   buttons: Ext.MessageBox.OK,
						   animEl: 'save',
						   icon: Ext.MessageBox.WARNING
						});
						break;
				}
			},
			failure: function(response){
				var result=response.responseText;
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: 'Could not connect to the database. retry later.',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});	
			}									    
		});   
	}
  
	function master_jual_produk_create(){
		var dproduk_count_create = detail_jual_produkListEditorGrid.getStore().getCount();
		var dproduk_produk_id="";
		var dproduk_diskon_ultah="false";
		var dproduk_diskon_bukan_tanpadiskon="false";
		for(i=0; i<detail_jual_produk_DataStore.getCount(); i++){
			detail_jual_produk_record=detail_jual_produk_DataStore.getAt(i);
			if(/^\d+$/.test(detail_jual_produk_record.data.dproduk_produk)){
				dproduk_produk_id="ada";
			}
			if(detail_jual_produk_record.data.dproduk_diskon_jenis=="Ultah"){
				dproduk_diskon_ultah="true";
			}
			if(detail_jual_produk_record.data.dproduk_diskon_jenis!=="Tanpa Diskon"){
				dproduk_diskon_bukan_tanpadiskon="true";
			}
		}
		
		var jproduk_id_for_cetak = 0;
		if(jproduk_idField.getValue()!== null){
			jproduk_id_for_cetak = jproduk_idField.getValue();
		}
		function create_jual_produk(btn){
			if(btn=='yes'){
				if(/*is_master_jual_produk_form_valid()
				   && */dproduk_produk_id=="ada"
				   && ((/*/^\d+$/.test(jproduk_custField.getValue()) &&*/ jproduk_post2db=="CREATE") || jproduk_post2db=="UPDATE")
				   && jproduk_stat_dokField.getValue()=='Terbuka'){
					var jproduk_id_create_pk=null; 
					var jproduk_nobukti_create=null; 
					var jproduk_grooming_create=0; 
					var jproduk_cust_create=null; 
					var jproduk_cust_memberno_create=null;
					var jproduk_tanggal_create_date=""; 
					var jproduk_diskon_create=null; 
					var jproduk_cara_create=null; 
					var jproduk_cara2_create=null; 
					var jproduk_cara3_create=null; 
					var jproduk_statdok_create=null;
					var jproduk_keterangan_create=null; 
					var jproduk_ket_disk_create=null; 
					//tunai
					var jproduk_tunai_nilai_create=null;
					//tunai-2
					var jproduk_tunai_nilai2_create=null;
					//tunai-3
					var jproduk_tunai_nilai3_create=null;
					//vgrooming
					var jproduk_vgrooming_nilai_create=null;
					//voucher
					var jproduk_voucher_no_create="";
					var jproduk_voucher_cashback_create=null;
					//voucher-2
					var jproduk_voucher_no2_create="";
					var jproduk_voucher_cashback2_create=null;
					//voucher-3
					var jproduk_voucher_no3_create="";
					var jproduk_voucher_cashback3_create=null;
					
					var jproduk_cashback_create=null;
					var jproduk_potong_point_create=null;
					//bayar
					var jproduk_subtotal_create=null;
					var jproduk_total_create=null;
					var jproduk_bayar_create=null;
					var jproduk_hutang_create=null;
					//kwitansi
					var jproduk_kwitansi_nama_create="";
					var jproduk_kwitansi_nomor_create="";
					var jproduk_kwitansi_nilai_create=null;
					//kwitansi-2
					var jproduk_kwitansi_nama2_create="";
					var jproduk_kwitansi_nomor2_create="";
					var jproduk_kwitansi_nilai2_create=null;
					//kwitansi-3
					var jproduk_kwitansi_nama3_create="";
					var jproduk_kwitansi_nomor3_create="";
					var jproduk_kwitansi_nilai3_create=null;
					//card
					var jproduk_card_nama_create="";
					var jproduk_card_edc_create="";
					var jproduk_card_promo_create="";
					var jproduk_card_no_create="";
					var jproduk_card_nilai_create=null;
					//card-2
					var jproduk_card_nama2_create="";
					var jproduk_card_edc2_create="";
					var jproduk_card_promo2_create="";
					var jproduk_card_no2_create="";
					var jproduk_card_nilai2_create=null;
					//card-3
					var jproduk_card_nama3_create="";
					var jproduk_card_edc3_create="";
					var jproduk_card_promo3_create="";
					var jproduk_card_no3_create="";
					var jproduk_card_nilai3_create=null;
					//cek
					var jproduk_cek_nama_create="";
					var jproduk_cek_nomor_create="";
					var jproduk_cek_valid_create="";
					var jproduk_cek_bank_create="";
					var jproduk_cek_nilai_create=null;
					//cek-2
					var jproduk_cek_nama2_create="";
					var jproduk_cek_nomor2_create="";
					var jproduk_cek_valid2_create="";
					var jproduk_cek_bank2_create="";
					var jproduk_cek_nilai2_create=null;
					//cek-3
					var jproduk_cek_nama3_create="";
					var jproduk_cek_nomor3_create="";
					var jproduk_cek_valid3_create="";
					var jproduk_cek_bank3_create="";
					var jproduk_cek_nilai3_create=null;
					//transfer
					var jproduk_transfer_bank_create="";
					var jproduk_transfer_nama_create="";
					var jproduk_transfer_nilai_create=null;
					//transfer-2
					var jproduk_transfer_bank2_create="";
					var jproduk_transfer_nama2_create="";
					var jproduk_transfer_nilai2_create=null;
					//transfer-3
					var jproduk_transfer_bank3_create="";
					var jproduk_transfer_nama3_create="";
					var jproduk_transfer_nilai3_create=null;
					
					var jproduk_vgrooming_nilai_create=0;
					var member_nama_baru_create="";
					var member_no_baru_create="";
					var member_hp_baru_create="";
					var member_ultah_baruField_create="";
					
					if(jproduk_idField.getValue()!== null){jproduk_id_create_pk = jproduk_idField.getValue();}else{jproduk_id_create_pk=get_jproduk_pk();} 
					if(jproduk_nobuktiField.getValue()!== null){jproduk_nobukti_create = jproduk_nobuktiField.getValue();}
					if((jproduk_post2db=="CREATE") && (jproduk_karyawanField.getValue()!== null)){
						jproduk_grooming_create = jproduk_karyawanField.getValue();
					}else if(jproduk_post2db=="UPDATE"){
						jproduk_grooming_create = jproduk_karyawan_idField.getValue();
					}				
					
					
					if((jproduk_post2db=="CREATE") && (jproduk_custField.getValue()!== null)){
						jproduk_cust_create = jproduk_custField.getValue();
					}else if(jproduk_post2db=="UPDATE"){
						jproduk_cust_create = jproduk_cust_idField.getValue();
					}
					//if(jproduk_cust_nomemberField.getValue()){jproduk_cust_memberno_create = jproduk_cust_nomemberField.getValue();}
					//if(jproduk_cust_nomemberField.getEl().dom.innerHTML){jproduk_cust_memberno_create = jproduk_cust_nomemberField.getEl().dom.innerHTML;}
					if(jproduk_tanggalField.getValue()!== ""){jproduk_tanggal_create_date = jproduk_tanggalField.getValue().format('Y-m-d');} 
					if(member_ultah_baruField.getValue()!== ""){member_ultah_baruField_create = member_ultah_baruField.getValue().format('Y-m-d');} 
					if(app_cust_namaBaruField.getValue()!== null){member_nama_baru_create = app_cust_namaBaruField.getValue();}  
					if(member_no_baruField.getValue()!== null){member_no_baru_create = member_no_baruField.getValue();}
					if(app_cust_hpBaruField.getValue()!== null){member_hp_baru_create = app_cust_hpBaruField.getValue();}
					if(jproduk_diskonField.getValue()!== null){jproduk_diskon_create = jproduk_diskonField.getValue();} 
					if(jproduk_caraField.getValue()!== null){jproduk_cara_create = jproduk_caraField.getValue();} 
					if(jproduk_cara2Field.getValue()!== null){jproduk_cara2_create = jproduk_cara2Field.getValue();} 
					if(jproduk_cara3Field.getValue()!== null){jproduk_cara3_create = jproduk_cara3Field.getValue();} 
					if(jproduk_stat_dokField.getValue()!== null){jproduk_statdok_create = jproduk_stat_dokField.getValue();} 
					if(jproduk_keteranganField.getValue()!== null){jproduk_keterangan_create = jproduk_keteranganField.getValue();}
					if(jproduk_ket_diskField.getValue()!== null){jproduk_ket_disk_create = jproduk_ket_diskField.getValue();} 				
					//tunai
					if(jproduk_tunai_nilaiField.getValue()!== null){jproduk_tunai_nilai_create = jproduk_tunai_nilaiField.getValue();}
					//tunai-2
					if(jproduk_tunai_nilai2Field.getValue()!== null){jproduk_tunai_nilai2_create = jproduk_tunai_nilai2Field.getValue();}
					//tunai-3
					if(jproduk_tunai_nilai3Field.getValue()!== null){jproduk_tunai_nilai3_create = jproduk_tunai_nilai3Field.getValue();}
					//voucher
					if(jproduk_voucher_noField.getValue()!== ""){jproduk_voucher_no_create = jproduk_voucher_noField.getValue();}
					if(jproduk_voucher_cashbackField.getValue()!== null){jproduk_voucher_cashback_create = jproduk_voucher_cashbackField.getValue();} 
					//voucher-2
					if(jproduk_voucher_no2Field.getValue()!== ""){jproduk_voucher_no2_create = jproduk_voucher_no2Field.getValue();} 
					if(jproduk_voucher_cashback2Field.getValue()!== null){jproduk_voucher_cashback2_create = jproduk_voucher_cashback2Field.getValue();} 
					//voucher-3
					if(jproduk_voucher_no3Field.getValue()!== ""){jproduk_voucher_no3_create = jproduk_voucher_no3Field.getValue();} 
					if(jproduk_voucher_cashback3Field.getValue()!== null){jproduk_voucher_cashback3_create = jproduk_voucher_cashback3Field.getValue();} 
					
					if(jproduk_cashbackField.getValue()!== null){jproduk_cashback_create = jproduk_cashbackField.getValue();} 
					if(jproduk_potong_pointField.getValue()!== null){jproduk_potong_point_create = jproduk_potong_pointField.getValue();} 
					//bayar
					if(jproduk_bayarField.getValue()!== null){jproduk_bayar_create = jproduk_bayarField.getValue();}
					if(jproduk_subTotalField.getValue()!== null){jproduk_subtotal_create = jproduk_subTotalField.getValue();} 
					if(jproduk_totalField.getValue()!== null){jproduk_total_create = jproduk_totalField.getValue();} 
					if(jproduk_hutangField.getValue()!== null){jproduk_hutang_create = jproduk_hutangField.getValue();} 
					//kwitansi value
					if(jproduk_kwitansi_namaField.getValue()!== ""){jproduk_kwitansi_nama_create = jproduk_kwitansi_namaField.getValue();} 
					if(jproduk_kwitansi_nilaiField.getValue()!== null){jproduk_kwitansi_nilai_create = jproduk_kwitansi_nilaiField.getValue();} 
					if(jproduk_kwitansi_nama2Field.getValue()!== ""){jproduk_kwitansi_nama2_create = jproduk_kwitansi_nama2Field.getValue();} 
					if(jproduk_kwitansi_nilai2Field.getValue()!== null){jproduk_kwitansi_nilai2_create = jproduk_kwitansi_nilai2Field.getValue();}
					
					if(jproduk_kwitansi_nama3Field.getValue()!== ""){jproduk_kwitansi_nama3_create = jproduk_kwitansi_nama3Field.getValue();} 
					if(jproduk_kwitansi_nilai3Field.getValue()!== null){jproduk_kwitansi_nilai3_create = jproduk_kwitansi_nilai3Field.getValue();} 
					//card value
					if(jproduk_card_namaField.getValue()!== ""){jproduk_card_nama_create = jproduk_card_namaField.getValue();} 
					if(jproduk_card_edcField.getValue()!==""){jproduk_card_edc_create = jproduk_card_edcField.getValue();}
					if(jproduk_card_promoField.getValue()!==""){jproduk_card_promo_create = jproduk_card_promoField.getValue();}
					if(jproduk_card_noField.getValue()!==""){jproduk_card_no_create = jproduk_card_noField.getValue();}
					if(jproduk_card_nilaiField.getValue()!==null){jproduk_card_nilai_create = jproduk_card_nilaiField.getValue();} 
					//card-2 value
					if(jproduk_card_nama2Field.getValue()!== ""){jproduk_card_nama2_create = jproduk_card_nama2Field.getValue();} 
					if(jproduk_card_edc2Field.getValue()!==""){jproduk_card_edc2_create = jproduk_card_edc2Field.getValue();} 
					if(jproduk_card_promo2Field.getValue()!==""){jproduk_card_promo2_create = jproduk_card_promo2Field.getValue();}
					if(jproduk_card_no2Field.getValue()!==""){jproduk_card_no2_create = jproduk_card_no2Field.getValue();}
					if(jproduk_card_nilai2Field.getValue()!==null){jproduk_card_nilai2_create = jproduk_card_nilai2Field.getValue();} 
					//card-3 value
					if(jproduk_card_nama3Field.getValue()!== ""){jproduk_card_nama3_create = jproduk_card_nama3Field.getValue();} 
					if(jproduk_card_edc3Field.getValue()!==""){jproduk_card_edc3_create = jproduk_card_edc3Field.getValue();} 
					if(jproduk_card_promo3Field.getValue()!==""){jproduk_card_promo3_create = jproduk_card_promo3Field.getValue();}
					if(jproduk_card_no3Field.getValue()!==""){jproduk_card_no3_create = jproduk_card_no3Field.getValue();}
					if(jproduk_card_nilai3Field.getValue()!==null){jproduk_card_nilai3_create = jproduk_card_nilai3Field.getValue();} 
					//cek value
					if(jproduk_cek_namaField.getValue()!== ""){jproduk_cek_nama_create = jproduk_cek_namaField.getValue();} 
					if(jproduk_cek_noField.getValue()!== ""){jproduk_cek_nomor_create = jproduk_cek_noField.getValue();} 
					if(jproduk_cek_validField.getValue()!== ""){jproduk_cek_valid_create = jproduk_cek_validField.getValue().format('Y-m-d');} 
					if(jproduk_cek_bankField.getValue()!== ""){jproduk_cek_bank_create = jproduk_cek_bankField.getValue();} 
					if(jproduk_cek_nilaiField.getValue()!== null){jproduk_cek_nilai_create = jproduk_cek_nilaiField.getValue();} 
					//cek-2 value
					if(jproduk_cek_nama2Field.getValue()!== ""){jproduk_cek_nama2_create = jproduk_cek_nama2Field.getValue();} 
					if(jproduk_cek_no2Field.getValue()!== ""){jproduk_cek_nomor2_create = jproduk_cek_no2Field.getValue();} 
					if(jproduk_cek_valid2Field.getValue()!== ""){jproduk_cek_valid2_create = jproduk_cek_valid2Field.getValue().format('Y-m-d');} 
					if(jproduk_cek_bank2Field.getValue()!== ""){jproduk_cek_bank2_create = jproduk_cek_bank2Field.getValue();} 
					if(jproduk_cek_nilai2Field.getValue()!== null){jproduk_cek_nilai2_create = jproduk_cek_nilai2Field.getValue();} 
					//cek-3 value
					if(jproduk_cek_nama3Field.getValue()!== ""){jproduk_cek_nama3_create = jproduk_cek_nama3Field.getValue();} 
					if(jproduk_cek_no3Field.getValue()!== ""){jproduk_cek_nomor3_create = jproduk_cek_no3Field.getValue();} 
					if(jproduk_cek_valid3Field.getValue()!== ""){jproduk_cek_valid3_create = jproduk_cek_valid3Field.getValue().format('Y-m-d');} 
					if(jproduk_cek_bank3Field.getValue()!== ""){jproduk_cek_bank3_create = jproduk_cek_bank3Field.getValue();} 
					if(jproduk_cek_nilai3Field.getValue()!== null){jproduk_cek_nilai3_create = jproduk_cek_nilai3Field.getValue();} 
					//transfer value
					if(jproduk_transfer_bankField.getValue()!== ""){jproduk_transfer_bank_create = jproduk_transfer_bankField.getValue();} 
					if(jproduk_transfer_namaField.getValue()!== ""){jproduk_transfer_nama_create = jproduk_transfer_namaField.getValue();}
					if(jproduk_transfer_nilaiField.getValue()!== null){jproduk_transfer_nilai_create = jproduk_transfer_nilaiField.getValue();} 
					//transfer-2 value
					if(jproduk_transfer_bank2Field.getValue()!== ""){jproduk_transfer_bank2_create = jproduk_transfer_bank2Field.getValue();} 
					if(jproduk_transfer_nama2Field.getValue()!== ""){jproduk_transfer_nama2_create = jproduk_transfer_nama2Field.getValue();}
					if(jproduk_transfer_nilai2Field.getValue()!== null){jproduk_transfer_nilai2_create = jproduk_transfer_nilai2Field.getValue();} 
					//transfer-3 value
					if(jproduk_transfer_bank3Field.getValue()!== ""){jproduk_transfer_bank3_create = jproduk_transfer_bank3Field.getValue();} 
					if(jproduk_transfer_nama3Field.getValue()!== ""){jproduk_transfer_nama3_create = jproduk_transfer_nama3Field.getValue();}
					if(jproduk_transfer_nilai3Field.getValue()!== null){jproduk_transfer_nilai3_create = jproduk_transfer_nilai3Field.getValue();}
					
					var dcount_dproduk_id_create = 0;
					for(i=0; i<detail_jual_produk_DataStore.getCount();i++){
						if(detail_jual_produk_DataStore.getAt(i).data.dproduk_id==0){
							dcount_dproduk_id_create = dcount_dproduk_id_create+1;
						}
					}

					if(jproduk_cashback_create !== '' && jproduk_cashback_create > 0 && jproduk_ket_disk_create == ''){
						master_jual_produk_createForm.jproduk_save.enable();
						master_jual_produk_createForm.jproduk_bayar.enable();
						master_jual_produk_pembayaranForm.jproduk_savePrint.enable();
						//master_jual_produk_createForm.jproduk_savePrint2.enable();
						
						Ext.MessageBox.show({
						   title: 'Warning',
						   msg: 'Data penjualan produk tidak bisa disimpan. Harap mengisi nomor voucher.',
						   buttons: Ext.MessageBox.OK,
						   animEl: 'save',
						   icon: Ext.MessageBox.WARNING
						});
					}else if((
					(jproduk_kwitansi_no_idField.getValue() !== "" && jproduk_kwitansi_no2_idField.getValue() !== "") || 
					(jproduk_kwitansi_no2_idField.getValue() !== "" && jproduk_kwitansi_no3_idField.getValue() !== "") || 
					(jproduk_kwitansi_no_idField.getValue() !== "" && jproduk_kwitansi_no3_idField.getValue() !== "")) && 
					((jproduk_kwitansi_no_idField.getValue() == jproduk_kwitansi_no2_idField.getValue()) || 
					(jproduk_kwitansi_no_idField.getValue() == jproduk_kwitansi_no3_idField.getValue()) || 
					(jproduk_kwitansi_no2_idField.getValue() == jproduk_kwitansi_no3_idField.getValue()))){					
						Ext.MessageBox.show({
						   title: 'Warning',
						   msg: 'Data penjualan produk tidak bisa disimpan. Nomor Kuitansi/Gift Voucher tidak boleh sama.',
						   buttons: Ext.MessageBox.OK,
						   animEl: 'save',
						   icon: Ext.MessageBox.WARNING
						});
					}else {
						//var cetak_jproduk = this.cetak_jproduk;
						var dproduk_id=[];
						var dproduk_produk=[];
						var dproduk_karyawan=[];
						var dproduk_satuan=[];
						var dproduk_jumlah=[];
						var dproduk_harga=[];
						var dproduk_diskon=[];
						var dproduk_diskon_jenis=[];
						var dcount = detail_jual_produk_DataStore.getCount() - 1;
						
						if(detail_jual_produk_DataStore.getCount()>0){
							for(i=0; i<detail_jual_produk_DataStore.getCount();i++){
								if((/^\d+$/.test(detail_jual_produk_DataStore.getAt(i).data.dproduk_produk))
								   && detail_jual_produk_DataStore.getAt(i).data.dproduk_produk!==undefined
								   && detail_jual_produk_DataStore.getAt(i).data.dproduk_produk!==''
								   && detail_jual_produk_DataStore.getAt(i).data.dproduk_produk!==0){
									
									dproduk_id.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_id);
									dproduk_produk.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_produk);
									
									if((detail_jual_produk_DataStore.getAt(i).data.dproduk_satuan==undefined)
									   || (detail_jual_produk_DataStore.getAt(i).data.dproduk_satuan=='')){
										dproduk_satuan.push(0);
									}else{
										dproduk_satuan.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_satuan);
									}
									
									if(detail_jual_produk_DataStore.getAt(i).data.dproduk_jumlah==undefined){
										dproduk_jumlah.push(0);
									}else{
										dproduk_jumlah.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_jumlah);
									}
									
									if(detail_jual_produk_DataStore.getAt(i).data.dproduk_harga==undefined){
										dproduk_harga.push(0);
									}else{
										dproduk_harga.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_harga);
									}
									
									if(detail_jual_produk_DataStore.getAt(i).data.dproduk_diskon_jenis==undefined){
										dproduk_diskon_jenis.push('');
									}else{
										dproduk_diskon_jenis.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_diskon_jenis);
									}
									
									if((detail_jual_produk_DataStore.getAt(i).data.dproduk_diskon==undefined)
									   || (detail_jual_produk_DataStore.getAt(i).data.dproduk_diskon=='')){
										dproduk_diskon.push(0);
									}else{
										dproduk_diskon.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_diskon);
									}
									
									if((detail_jual_produk_DataStore.getAt(i).data.dproduk_karyawan==undefined)
									   || (detail_jual_produk_DataStore.getAt(i).data.dproduk_karyawan=='')){
										dproduk_karyawan.push(0);
									}else{
										dproduk_karyawan.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_karyawan);
									}
								}
								
								if(i==dcount){
									var encoded_array_dproduk_id = Ext.encode(dproduk_id);
									var encoded_array_dproduk_produk = Ext.encode(dproduk_produk);
									var encoded_array_dproduk_satuan = Ext.encode(dproduk_satuan);
									var encoded_array_dproduk_jumlah = Ext.encode(dproduk_jumlah);
									var encoded_array_dproduk_harga = Ext.encode(dproduk_harga);
									var encoded_array_dproduk_diskon_jenis = Ext.encode(dproduk_diskon_jenis);
									var encoded_array_dproduk_diskon = Ext.encode(dproduk_diskon);
									var encoded_array_dproduk_karyawan = Ext.encode(dproduk_karyawan);
									
									Ext.Ajax.request({
										waitMsg: 'Mohon tunggu...',
										url: 'index.php?c=c_master_jual_produk&m=get_action',
										params: {
											task: jproduk_post2db,
											cetak_jproduk:cetak_jproduk,
											dproduk_count: dproduk_count_create,
											dcount_dproduk_id: dcount_dproduk_id_create,
											jproduk_id			: 	jproduk_id_create_pk, 
											jproduk_grooming	:	jproduk_grooming_create,
											jproduk_nobukti		: 	jproduk_nobukti_create, 
											jproduk_cust		: 	jproduk_cust_create, 
											jproduk_tanggal		: 	jproduk_tanggal_create_date, 
											jproduk_diskon		: 	jproduk_diskon_create, 
											jproduk_cara		: 	jproduk_cara_create, 
											jproduk_cara2		: 	jproduk_cara2_create, 
											jproduk_cara3		: 	jproduk_cara3_create, 
											jproduk_stat_dok	:	jproduk_statdok_create,
											jproduk_keterangan	: 	jproduk_keterangan_create, 
											jproduk_ket_disk	: 	jproduk_ket_disk_create, 
											jproduk_cashback	: 	jproduk_cashback_create,
											potong_point		: 	jproduk_potong_point_create,
											member_ultah_baru	: member_ultah_baruField_create,
											app_cust_nama_baru		: member_nama_baru_create,
											app_cust_member_no_baru		: member_no_baru_create,
											app_cust_hp_baru		: member_hp_baru_create,
											//tunai
											jproduk_tunai_nilai	:	jproduk_tunai_nilai_create,
											//tunai-2
											jproduk_tunai_nilai2	:	jproduk_tunai_nilai2_create,
											//tunai-3
											jproduk_tunai_nilai3	:	jproduk_tunai_nilai3_create,
											//vgrooming
											jproduk_vgrooming_nilai	:	jproduk_vgrooming_nilai_create,
											//voucher
											jproduk_voucher_no	:	jproduk_voucher_no_create,
											jproduk_voucher_cashback	:	jproduk_voucher_cashback_create,
											//voucher-2
											jproduk_voucher_no2	:	jproduk_voucher_no2_create,
											jproduk_voucher_cashback2	:	jproduk_voucher_cashback2_create,
											//voucher-3
											jproduk_voucher_no3	:	jproduk_voucher_no3_create,
											jproduk_voucher_cashback3	:	jproduk_voucher_cashback3_create,
											
											//bayar
											jproduk_bayar			: 	jproduk_bayar_create,
											jproduk_subtotal			: 	jproduk_subtotal_create,
											jproduk_total			: 	jproduk_total_create,
											jproduk_hutang		: 	jproduk_hutang_create,
											//kwitansi posting
											jual_kwitansi_id			: 	jproduk_jual_kwitansi_idField1.getValue(),
											jproduk_kwitansi_no			:	jproduk_kwitansi_no_idField.getValue(),
											jproduk_kwitansi_nama		:	jproduk_kwitansi_nama_create,
											jproduk_kwitansi_nilai		:	jproduk_kwitansi_nilai_create,
											jproduk_kwitansi_jenis		:	jproduk_flagField.getValue(),
											//kwitansi-2 posting
											jual_kwitansi_id2			: 	jproduk_jual_kwitansi_idField2.getValue(),
											jproduk_kwitansi_no2		:	jproduk_kwitansi_no2_idField.getValue(),
											jproduk_kwitansi_nama2		:	jproduk_kwitansi_nama2_create,
											jproduk_kwitansi_nilai2		:	jproduk_kwitansi_nilai2_create,
											jproduk_kwitansi_jenis2		:	jproduk_flag2Field.getValue(),
											//kwitansi-3 posting
											jual_kwitansi_id3			: 	jproduk_jual_kwitansi_idField3.getValue(),
											jproduk_kwitansi_no3		:	jproduk_kwitansi_no3_idField.getValue(),
											jproduk_kwitansi_nama3		:	jproduk_kwitansi_nama3_create,
											jproduk_kwitansi_nilai3		:	jproduk_kwitansi_nilai3_create,
											jproduk_kwitansi_jenis3		:	jproduk_flag3Field.getValue(),
											//card posting
											jproduk_card_nama	: 	jproduk_card_nama_create,
											jproduk_card_edc	:	jproduk_card_edc_create,
											jproduk_card_promo	:	jproduk_card_promo_create,
											jproduk_card_no		:	jproduk_card_no_create,
											jproduk_card_nilai	:	jproduk_card_nilai_create,
											//card-2 posting
											jproduk_card_nama2	: 	jproduk_card_nama2_create,
											jproduk_card_edc2	:	jproduk_card_edc2_create,
											jproduk_card_promo2	:	jproduk_card_promo2_create,
											jproduk_card_no2	:	jproduk_card_no2_create,
											jproduk_card_nilai2	:	jproduk_card_nilai2_create,
											//card-3 posting
											jproduk_card_nama3	: 	jproduk_card_nama3_create,
											jproduk_card_edc3	:	jproduk_card_edc3_create,
											jproduk_card_promo3	:	jproduk_card_promo3_create,
											jproduk_card_no3	:	jproduk_card_no3_create,
											jproduk_card_nilai3	:	jproduk_card_nilai3_create,
											//cek posting
											jproduk_cek_nama	: 	jproduk_cek_nama_create,
											jproduk_cek_no		:	jproduk_cek_nomor_create,
											jproduk_cek_valid	: 	jproduk_cek_valid_create,
											jproduk_cek_bank	:	jproduk_cek_bank_create,
											jproduk_cek_nilai	:	jproduk_cek_nilai_create,
											//cek-2 posting
											jproduk_cek_nama2	: 	jproduk_cek_nama2_create,
											jproduk_cek_no2		:	jproduk_cek_nomor2_create,
											jproduk_cek_valid2	: 	jproduk_cek_valid2_create,
											jproduk_cek_bank2	:	jproduk_cek_bank2_create,
											jproduk_cek_nilai2	:	jproduk_cek_nilai2_create,
											//cek-3 posting
											jproduk_cek_nama3	: 	jproduk_cek_nama3_create,
											jproduk_cek_no3		:	jproduk_cek_nomor3_create,
											jproduk_cek_valid3	: 	jproduk_cek_valid3_create,
											jproduk_cek_bank3	:	jproduk_cek_bank3_create,
											jproduk_cek_nilai3	:	jproduk_cek_nilai3_create,
											//transfer posting
											jproduk_transfer_bank	:	jproduk_transfer_bank_create,
											jproduk_transfer_nama	:	jproduk_transfer_nama_create,
											jproduk_transfer_nilai	:	jproduk_transfer_nilai_create,
											//transfer-2 posting
											jproduk_transfer_bank2	:	jproduk_transfer_bank2_create,
											jproduk_transfer_nama2	:	jproduk_transfer_nama2_create,
											jproduk_transfer_nilai2	:	jproduk_transfer_nilai2_create,
											//transfer-3 posting
											jproduk_transfer_bank3	:	jproduk_transfer_bank3_create,
											jproduk_transfer_nama3	:	jproduk_transfer_nama3_create,
											jproduk_transfer_nilai3	:	jproduk_transfer_nilai3_create,
											
											//Data Detail Penjualan Produk
											dproduk_id	: encoded_array_dproduk_id,
											dproduk_produk	: encoded_array_dproduk_produk,
											dproduk_satuan	: encoded_array_dproduk_satuan,
											dproduk_jumlah	: encoded_array_dproduk_jumlah,
											dproduk_harga	: encoded_array_dproduk_harga,
											dproduk_diskon_jenis	: encoded_array_dproduk_diskon_jenis,
											dproduk_diskon	: encoded_array_dproduk_diskon,
											dproduk_karyawan: encoded_array_dproduk_karyawan
										},
										success: function(response){
											Ext.MessageBox.hide();
											var result=eval(response.responseText);
											if(result == '-7'){
												jproduk_btn_cancel();
												Ext.MessageBox.show({
												   title: 'Warning',
												   msg: 'Detail Produk lepas tidak cocok dengan database. Silakan klik tombol Cancel terlebih dahulu, kemudian buka kembali data ini.',
												   buttons: Ext.MessageBox.OK,
												   animEl: 'save',
												   icon: Ext.MessageBox.WARNING
												});
											} else if(result==0){
												arr_dproduk_id.push(0);	//reset array pengecekan detail produk
												jproduk_btn_cancel();
												//Ext.MessageBox.alert(jproduk_post2db+' OK','Data penjualan produk berhasil disimpan');
												master_jual_produk_createWindow.hide();
												push_reset(0);
											}else if(result>0 && cetak_jproduk == 2){
												jproduk_cetak2(result);
												jproduk_btn_cancel();
												push_reset(jproduk_cust_create);
											}else if(result>0){
												//Final
												jproduk_cetak(result);
												jproduk_btn_cancel();
												push_reset(jproduk_cust_create);
												
											}else{
												jproduk_btn_cancel();
												Ext.MessageBox.show({
												   title: 'Warning',
												   msg: 'Data penjualan produk tidak bisa disimpan',
												   buttons: Ext.MessageBox.OK,
												   animEl: 'save',
												   icon: Ext.MessageBox.WARNING
												});
											}
										},
										failure: function(response){
											var result=response.responseText;
											Ext.MessageBox.show({
												   title: 'Error',
												   msg: 'Could not connect to the database. retry later.',
												   buttons: Ext.MessageBox.OK,
												   animEl: 'database',
												   icon: Ext.MessageBox.ERROR
											});
											jproduk_btn_cancel();
										}                      
									});
								}
							}
						}
					}

				}else if(jproduk_post2db=='UPDATE' && jproduk_stat_dokField.getValue()=='Tertutup'){
					if(cetak_jproduk==1){
						jproduk_cetak(jproduk_id_for_cetak);
						cetak_jproduk=0;
					}else if(cetak_jproduk==2){
						jproduk_cetak2(jproduk_id_for_cetak);
						cetak_jproduk=0;
					}
					jproduk_btn_cancel();
				}else if(jproduk_post2db=='UPDATE' && jproduk_stat_dokField.getValue()=='Batal'){
					//cust nomember
					//var jproduk_cust_memberno_batal=null;
					//if(jproduk_cust_nomemberField.getValue()){jproduk_cust_memberno_batal = jproduk_cust_nomemberField.getValue();}
					//if(jproduk_cust_nomemberField.getEl().dom.innerHTML){jproduk_cust_memberno_batal = jproduk_cust_nomemberField.getEl().dom.innerHTML;}
					
					Ext.Ajax.request({  
						waitMsg: 'Mohon  Tunggu...',
						url: 'index.php?c=c_master_jual_produk&m=get_action',
						params: {
							task: 'BATAL',
							jproduk_id	: jproduk_idField.getValue(),
							jproduk_tanggal : jproduk_tanggalField.getValue().format('Y-m-d')
						}, 
						success: function(response){             
							var result=eval(response.responseText);
							if(result==1){
								jproduk_post2db='CREATE';
								Ext.MessageBox.show({
									title: 'Konfirmasi',
									msg: 'Dokumen telah dibatalkan.',
									buttons: Ext.MessageBox.OK,
									animEl: 'save',
									icon: Ext.MessageBox.INFO
								});
								
								jproduk_btn_cancel();							
							}else{
								jproduk_post2db='CREATE';
								Ext.MessageBox.show({
								   title: 'Warning',
								   width: 400,
								   msg: 'Dokumen tidak bisa dibatalkan.',
								   buttons: Ext.MessageBox.OK,
								   animEl: 'save',
								   icon: Ext.MessageBox.WARNING
								});
								jproduk_btn_cancel();
							}
						},
						failure: function(response){
							jproduk_post2db='CREATE';
							var result=response.responseText;
							Ext.MessageBox.show({
								   title: 'Error',
								   msg: 'Could not connect to the database. retry later.',
								   buttons: Ext.MessageBox.OK,
								   animEl: 'database',
								   icon: Ext.MessageBox.ERROR
							});
							jproduk_btn_cancel();
						}                      
					});
				}else {
					//alert(dproduk_produk_id);
					if(dproduk_produk_id!="ada"){
						Ext.MessageBox.show({
							title: 'Warning',
							msg: 'Detail penjualan produk tidak boleh kosong',
							buttons: Ext.MessageBox.OK,
							minWidth: 250,
							animEl: 'save',
							icon: Ext.MessageBox.WARNING
						});
					}else {
						Ext.MessageBox.show({
							title: 'Warning',
							msg: 'Form anda belum lengkap',
							buttons: Ext.MessageBox.OK,
							animEl: 'save',
							icon: Ext.MessageBox.WARNING
						});
					}
				}
			}
		}
		master_jual_produk_create.create_jual_produk = create_jual_produk;
		
		if((jproduk_caraField.getValue() == "card" && (jproduk_card_namaField.getValue() == "" || jproduk_card_edcField.getValue() == "")
												   && (jproduk_card_nilaiField.getValue() != 0 || jproduk_card_nilaiField.getValue() != ""))
			|| (jproduk_cara2Field.getValue() == "card" && (jproduk_card_nama2Field.getValue() == "" || jproduk_card_edc2Field.getValue() == "")
														&& (jproduk_card_nilai2Field.getValue() != 0 || jproduk_card_nilai2Field.getValue() != ""))
			|| (jproduk_cara3Field.getValue() == "card" && (jproduk_card_nama3Field.getValue() == "" || jproduk_card_edc3Field.getValue() == "")
														&& (jproduk_card_nilai3Field.getValue() != 0 || jproduk_card_nilai3Field.getValue() != "")) ) {
			var cara_bayar = "";
			if(jproduk_caraField.getValue() == "card" && (jproduk_card_namaField.getValue() == "" || jproduk_card_edcField.getValue() == "")) cara_bayar = "Cara Bayar 1";
			if(jproduk_cara2Field.getValue() == "card" && (jproduk_card_nama2Field.getValue() == "" || jproduk_card_edc2Field.getValue() == "")) cara_bayar = "Cara Bayar 2";
			if(jproduk_cara3Field.getValue() == "card" && (jproduk_card_nama3Field.getValue() == "" || jproduk_card_edc3Field.getValue() == "")) cara_bayar = "Cara Bayar 3";
				if(jproduk_stat_dokField.getValue() != 'Batal'){
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Jenis Kartu atau EDC belum diisi.<br><br>Silakan periksa kembali <b>' + cara_bayar + '</b>',
						buttons: Ext.MessageBox.OK,
						animEl: 'save',
						icon: Ext.MessageBox.WARNING
					});			
				}
		} else if(jproduk_bayarField.getValue()>=0 && jproduk_bayarField.getValue()<=jproduk_totalField.getValue()){
			if(jproduk_bayarField.getValue()<jproduk_totalField.getValue()){ //jika bayar < total		
					if(cetak_jproduk==1 || cetak_jproduk==2){ //jika save and print maka baru otorisasi hutang
						jproduk_username_hutangField.setValue('');
						jproduk_password_hutangField.setValue('');
						master_jual_produk_confirm_hutangWindow.show();
					}else{
						create_jual_produk('yes');
					}
					//Ext.MessageBox.confirm('Confirmation','Terdapat kekurangan jumlah bayar.<br><br>Anda yakin untuk melanjutkan?', create_jual_produk);
			}else{ //jika bayar = total
				create_jual_produk('yes');
			}
		}else{ //jika bayar > total
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Terdapat kelebihan jumlah bayar.<br><br>Periksa kembali jumlah pembayaran.',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
		}
	}
	
	function save_andPrint(){
		
		master_jual_produk_pembayaranForm.jproduk_savePrint.disable();//nambah ini
		//master_jual_produk_createForm.jproduk_savePrint2.disable();//nambah ini

		cetak_jproduk=1;
		pengecekan_dokumen();
		jproduk_pesanLabel.setText('');
		jproduk_lunasLabel.setText('');
		jproduk_cust_priorityLabel.setText('');
		//appointment_custBaruGroup_reset();
		//appointment_custBaruField.setValue('');
		
		cbo_cust_jual_produk_DataStore.load({
		params: {cust_id	: 10},
			callback: function(opts, success, response)  {
					if (success) {
					  if(cbo_cust_jual_produk_DataStore.getCount()!=0){
						  var cust_nama = null;
						  cust_record=cbo_cust_jual_produk_DataStore.getAt(0).data;
						  jproduk_custField.setValue(cust_record.cust_nama);
					  }
					}
				}
		});
		//jproduk_cust_ultahField.setValue(null);
		//jproduk_cust_nomemberField.setText('');		
		//jproduk_cust_pointField.setText('');
		//Show, combobox customer dan hide field customer baru
		jproduk_custField.show();
		app_cust_namaBaruField.hide();
		
		//Enable checbox "Baru
		appointment_custBaruField.setDisabled(false)
		master_jual_produk_pembayaranWindow.hide();
	}
	
	function save_andPrint2(){
		master_jual_produk_pembayaranForm.jproduk_savePrint.disable();//nambah ini
		//master_jual_produk_createForm.jproduk_savePrint2.disable();//nambah ini
		cetak_jproduk=2;		
		pengecekan_dokumen();
		jproduk_pesanLabel.setText('');
		jproduk_lunasLabel.setText('');
		jproduk_cust_priorityLabel.setText('');
	}
	
	function save_button(){
		cbo_cust_jual_produk_DataStore.load({
		params: {cust_id	: 10},
			callback: function(opts, success, response)  {
					if (success) {
					  if(cbo_cust_jual_produk_DataStore.getCount()!=0){
						  var cust_nama = null;
						  cust_record=cbo_cust_jual_produk_DataStore.getAt(0).data;
						  jproduk_custField.setValue(cust_record.cust_nama);
					  }
					}
				}
		});
		jproduk_cust_ultahField.setValue(null);
		jproduk_cust_nomemberField.setText('');		
		jproduk_cust_pointField.setText('');	
		
		cetak_jproduk=0;
		pengecekan_dokumen();
	}
	
	function bayar_button(){
		master_jual_produk_pembayaranWindow.show();
		jproduk_uang_cfField.focus(true,500);
	
	}
	
	//function ini untuk melakukan print saja, tanpa perlu melakukan proses pengecekan dokumen.. 
	function print_only(){
		if(jproduk_idField.getValue()==''){
			Ext.MessageBox.show({
			msg: 'Faktur tidak dapat dicetak, karena data kosong',
			buttons: Ext.MessageBox.OK,
			animEl: 'save',
			icon: Ext.MessageBox.WARNING
		   });
		}
		else{
		cetak_jproduk=1;		
		var jproduk_id_for_cetak = 0;
		if(jproduk_idField.getValue()!== null){
			jproduk_id_for_cetak = jproduk_idField.getValue();
		}
		if(cetak_jproduk==1){
			jproduk_cetak_print_only(jproduk_id_for_cetak);
			cetak_jproduk=0;
		}
		}
	}
	
	function print_only2(option){
		var jproduk_cust_create = null;
	
		if(/^\d+$/.test(jproduk_custField.getValue())){
             jproduk_cust_create = jproduk_custField.getValue();
		}else{
             jproduk_cust_create = jproduk_cust_idField.getValue();
        }
		if(jproduk_tanggalField.getValue()!== ""){jproduk_tanggal_create_date = jproduk_tanggalField.getValue().format('Y-m-d');} 
		
		if(jproduk_idField.getValue()==''){
			Ext.MessageBox.show({
			msg: 'Faktur tidak dapat dicetak, karena data kosong',
			buttons: Ext.MessageBox.OK,
			animEl: 'save',
			icon: Ext.MessageBox.WARNING
		   });
		}
		else{
			cetak_jproduk=2;		
			var jproduk_id_for_cetak = 0;
			if(jproduk_idField.getValue()!== null){
				jproduk_id_for_cetak = jproduk_idField.getValue();
			}
			if(cetak_jproduk==2){
				jproduk_cetak_print_only2(jproduk_id_for_cetak,jproduk_cust_create, jproduk_tanggal_create_date, option);
				cetak_jproduk=0;
			}
		}
	}
  
	function get_jproduk_pk(){
		if(jproduk_post2db=='UPDATE')
			return master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_id');
		else 
			return 0;
	}
	
	function get_stat_dok(){
		if(jproduk_post2db=='UPDATE')
			return master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok');
		else 
			return 'Terbuka';
	}
	
	function kwitansi_jual_produk_reset_form(){
		jproduk_kwitansi_namaField.reset();
		jproduk_kwitansi_nilaiField.reset();
		jproduk_kwitansi_nilai_cfField.reset();
		jproduk_kwitansi_noField.reset();
		jproduk_kwitansi_no_idField.reset();
		jproduk_kwitansi_sisaField.reset();
		jproduk_kwitansi_namaField.setValue("");
		jproduk_kwitansi_nilaiField.setValue(null);
		jproduk_kwitansi_nilai_cfField.setValue(null);
		jproduk_kwitansi_noField.setValue("");
		jproduk_kwitansi_no_idField.setValue("");
		jproduk_kwitansi_sisaField.setValue(null);
		jproduk_jual_kwitansi_idField1.reset();
		jproduk_jual_kwitansi_idField1.setValue(null);
	}
	function kwitansi2_jual_produk_reset_form(){
		jproduk_kwitansi_nama2Field.reset();
		jproduk_kwitansi_nilai2Field.reset();
		jproduk_kwitansi_nilai2_cfField.reset();
		jproduk_kwitansi_no2Field.reset();
		jproduk_kwitansi_no2_idField.reset();
		jproduk_kwitansi_sisa2Field.reset();
		jproduk_kwitansi_nama2Field.setValue("");
		jproduk_kwitansi_nilai2Field.setValue(null);
		jproduk_kwitansi_nilai2_cfField.setValue(null);
		jproduk_kwitansi_no2Field.setValue("");
		jproduk_kwitansi_no2_idField.setValue("");
		jproduk_kwitansi_sisa2Field.setValue(null);
		jproduk_jual_kwitansi_idField2.reset();
		jproduk_jual_kwitansi_idField2.setValue(null);
	}
	function kwitansi3_jual_produk_reset_form(){
		jproduk_kwitansi_nama3Field.reset();
		jproduk_kwitansi_nilai3Field.reset();
		jproduk_kwitansi_nilai3_cfField.reset();
		jproduk_kwitansi_no3Field.reset();
		jproduk_kwitansi_no3_idField.reset();
		jproduk_kwitansi_sisa3Field.reset();
		jproduk_kwitansi_nama3Field.setValue("");
		jproduk_kwitansi_nilai3Field.setValue(null);
		jproduk_kwitansi_nilai3_cfField.setValue(null);
		jproduk_kwitansi_no3Field.setValue("");
		jproduk_kwitansi_no3_idField.setValue("");
		jproduk_kwitansi_sisa3Field.setValue(null);
		jproduk_jual_kwitansi_idField3.reset();
		jproduk_jual_kwitansi_idField3.setValue(null);
	}
	
	function card_jual_produk_reset_form(){
		jproduk_card_namaField.reset();
		jproduk_card_edcField.reset();
		jproduk_card_promoField.reset();
		jproduk_card_noField.reset();
		jproduk_card_nilaiField.reset();
		jproduk_card_nilai_cfField.reset();
		jproduk_card_namaField.setValue("");
		jproduk_card_edcField.setValue("");
		jproduk_card_promoField.setValue("");
		jproduk_card_noField.setValue("");
		jproduk_card_nilaiField.setValue(null);
		jproduk_card_nilai_cfField.setValue(null);
	}
	function card2_jual_produk_reset_form(){
		jproduk_card_nama2Field.reset();
		jproduk_card_edc2Field.reset();
		jproduk_card_promo2Field.reset();
		jproduk_card_no2Field.reset();
		jproduk_card_nilai2Field.reset();
		jproduk_card_nilai2_cfField.reset();
		jproduk_card_nama2Field.setValue("");
		jproduk_card_edc2Field.setValue("");
		jproduk_card_promo2Field.setValue("");
		jproduk_card_no2Field.setValue("");
		jproduk_card_nilai2Field.setValue(null);
		jproduk_card_nilai2_cfField.setValue(null);
	}
	function card3_jual_produk_reset_form(){
		jproduk_card_nama3Field.reset();
		jproduk_card_edc3Field.reset();
		jproduk_card_promo3Field.reset();
		jproduk_card_no3Field.reset();
		jproduk_card_nilai3Field.reset();
		jproduk_card_nilai3_cfField.reset();
		jproduk_card_nama3Field.setValue("");
		jproduk_card_edc3Field.setValue("");
		jproduk_card_promo3Field.setValue("");
		jproduk_card_no3Field.setValue("");
		jproduk_card_nilai3Field.setValue(null);
		jproduk_card_nilai3_cfField.setValue(null);
	}
	
	function cek_jual_produk_reset_form(){
		jproduk_cek_namaField.reset();
		jproduk_cek_noField.reset();
		jproduk_cek_validField.reset();
		jproduk_cek_bankField.reset();
		jproduk_cek_nilaiField.reset();
		jproduk_cek_nilai_cfField.reset();
		jproduk_cek_namaField.setValue(null);
		jproduk_cek_noField.setValue("");
		jproduk_cek_validField.setValue("");
		jproduk_cek_bankField.setValue("");
		jproduk_cek_nilaiField.setValue(null);
		jproduk_cek_nilai_cfField.setValue(null);
	}
	function cek2_jual_produk_reset_form(){
		jproduk_cek_nama2Field.reset();
		jproduk_cek_no2Field.reset();
		jproduk_cek_valid2Field.reset();
		jproduk_cek_bank2Field.reset();
		jproduk_cek_nilai2Field.reset();
		jproduk_cek_nilai2_cfField.reset();
		jproduk_cek_nama2Field.setValue(null);
		jproduk_cek_no2Field.setValue("");
		jproduk_cek_valid2Field.setValue("");
		jproduk_cek_bank2Field.setValue("");
		jproduk_cek_nilai2Field.setValue(null);
		jproduk_cek_nilai2_cfField.setValue(null);
	}
	function cek3_jual_produk_reset_form(){
		jproduk_cek_nama3Field.reset();
		jproduk_cek_no3Field.reset();
		jproduk_cek_valid3Field.reset();
		jproduk_cek_bank3Field.reset();
		jproduk_cek_nilai3Field.reset();
		jproduk_cek_nilai3_cfField.reset();
		jproduk_cek_nama3Field.setValue(null);
		jproduk_cek_no3Field.setValue("");
		jproduk_cek_valid3Field.setValue("");
		jproduk_cek_bank3Field.setValue("");
		jproduk_cek_nilai3Field.setValue(null);
		jproduk_cek_nilai3_cfField.setValue(null);
	}
	
	function transfer_jual_produk_reset_form(){
		jproduk_transfer_bankField.reset();
		jproduk_transfer_namaField.reset();
		jproduk_transfer_nilaiField.reset();
		jproduk_transfer_nilai_cfField.reset();
		jproduk_transfer_bankField.setValue("");
		jproduk_transfer_namaField.setValue(null);
		jproduk_transfer_nilaiField.setValue(null);
		jproduk_transfer_nilai_cfField.setValue(null);
	}
	function transfer2_jual_produk_reset_form(){
		jproduk_transfer_bank2Field.reset();
		jproduk_transfer_nama2Field.reset();
		jproduk_transfer_nilai2Field.reset();
		jproduk_transfer_nilai2_cfField.reset();
		jproduk_transfer_bank2Field.setValue("");
		jproduk_transfer_nama2Field.setValue(null);
		jproduk_transfer_nilai2Field.setValue(null);
		jproduk_transfer_nilai2_cfField.setValue(null);
	}
	function transfer3_jual_produk_reset_form(){
		jproduk_transfer_bank3Field.reset();
		jproduk_transfer_nama3Field.reset();
		jproduk_transfer_nilai3Field.reset();
		jproduk_transfer_nilai3_cfField.reset();
		jproduk_transfer_bank3Field.setValue("");
		jproduk_transfer_nama3Field.setValue(null);
		jproduk_transfer_nilai3Field.setValue(null);
		jproduk_transfer_nilai3_cfField.setValue(null);
	}

	function tunai_jual_produk_reset_form(){
		jproduk_tunai_nilaiField.reset();
		jproduk_tunai_nilaiField.setValue(null);
		jproduk_tunai_nilai_cfField.reset();
		jproduk_tunai_nilai_cfField.setValue(null);
	}
	function tunai2_jual_produk_reset_form(){
		jproduk_tunai_nilai2Field.reset();
		jproduk_tunai_nilai2Field.setValue(null);
		jproduk_tunai_nilai2_cfField.reset();
		jproduk_tunai_nilai2_cfField.setValue(null);
	}
	function tunai3_jual_produk_reset_form(){
		jproduk_tunai_nilai3Field.reset();
		jproduk_tunai_nilai3Field.setValue(null);
		jproduk_tunai_nilai3_cfField.reset();
		jproduk_tunai_nilai3_cfField.setValue(null);
	}
	
	function voucher_jual_produk_reset_form(){
		jproduk_voucher_noField.reset();
		jproduk_voucher_cashbackField.reset();
		jproduk_voucher_cashback_cfField.reset();
		jproduk_voucher_noField.setValue("");
		jproduk_voucher_cashbackField.setValue(null);
		jproduk_voucher_cashback_cfField.setValue(null);
	}
	function voucher2_jual_produk_reset_form(){
		jproduk_voucher_no2Field.reset();
		jproduk_voucher_cashback2Field.reset();
		jproduk_voucher_cashback2_cfField.reset();
		jproduk_voucher_no2Field.setValue("");
		jproduk_voucher_cashback2Field.setValue(null);
		jproduk_voucher_cashback2_cfField.setValue(null);
	}
	function voucher3_jual_produk_reset_form(){
		jproduk_voucher_no3Field.reset();
		jproduk_voucher_cashback3Field.reset();
		jproduk_voucher_cashback3_cfField.reset();
		jproduk_voucher_no3Field.setValue("");
		jproduk_voucher_cashback3Field.setValue(null);
		jproduk_voucher_cashback3_cfField.setValue(null);
	}
	
	function appointment_custBaruGroup_reset(){
 		//appointment_custBaruGroup.collapse(true);		
 		app_cust_namaBaruField.reset();
 		app_cust_namaBaruField.setValue(null);
 		member_no_baruField.reset();
 		member_no_baruField.setValue(null);
 		app_cust_hpBaruField.reset();
 		app_cust_hpBaruField.setValue(null);
		member_ultah_baruField.reset();
 		member_ultah_baruField.setValue(null);
 		app_cust_keteranganBaruField.reset();
 		app_cust_keteranganBaruField.setValue(null);				
 	}  
	
	function master_jual_produk_reset_form(){		
		detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
		jproduk_idField.reset();
		jproduk_idField.setValue(null);
		/*jproduk_karyawanField.reset();
		jproduk_karyawanField.setValue(null);
		jproduk_karyawan_idField.reset()
		jproduk_karyawan_idField.setValue(null);
		jproduk_nikkaryawanField.reset();
		jproduk_nikkaryawanField.setValue(null);
		jproduk_karyawan_levelField.reset();
		jproduk_karyawan_levelField.setValue(null);
		jproduk_karyawan_jabatanField.reset();
		jproduk_karyawan_jabatanField.setValue(null);*/
		
		jproduk_nobuktiField.reset();
		jproduk_nobuktiField.setValue(null);
		
		//set cust jd UMUM
		cbo_cust_jual_produk_DataStore.load({
		params: {cust_id	: 10},
			callback: function(opts, success, response)  {
					if (success) {
					  if(cbo_cust_jual_produk_DataStore.getCount()!=0){
						  var cust_nama = null;
						  cust_record=cbo_cust_jual_produk_DataStore.getAt(0).data;
						  jproduk_custField.setValue(cust_record.cust_nama);
					  }
					}
				}
		});
		
		jproduk_cust_ultahField.setValue(null);
		jproduk_cust_nomemberField.setText('');		
		jproduk_cust_pointField.setText('');	

		appointment_custBaruGroup_reset();
		appointment_custBaruField.setValue(false);
		
		//Show, combobox customer dan hide field customer baru
		jproduk_custField.show();
		app_cust_namaBaruField.hide();
		
		//Enable checbox "Baru
		appointment_custBaruField.setDisabled(false)		
		
		jproduk_cust_noteField.reset();
		jproduk_cust_noteField.setValue("");
		/*
		jproduk_cust_nomemberField.reset();
		jproduk_cust_nomemberField.setValue("");
		*/
	//	jproduk_cust_nomemberField.setText("");
		jproduk_ownerField.reset();
		jproduk_ownerField.setValue("");
		jproduk_tahun_ambil_disk_ultah.reset();
		jproduk_tahun_ambil_disk_ultah.setValue("");	
		jproduk_tgl_ambil_disk_ultah.reset();
		jproduk_tgl_ambil_disk_ultah.setValue("");		
		//jproduk_valid_memberField.setValue("");
		//jproduk_cust_ultahField.setValue("");
		jproduk_tanggalField.setValue(dt.format('Y-m-d'));
		jproduk_diskonField.reset();
		jproduk_diskonField.setValue(null);
		jproduk_stat_dokField.reset();
		jproduk_stat_dokField.setValue('Terbuka');
		jproduk_caraField.reset();
		jproduk_caraField.setValue(null);
		jproduk_cara2Field.reset();
		jproduk_cara2Field.setValue(null);
		jproduk_cara3Field.reset();
		jproduk_cara3Field.setValue(null);
		
		jproduk_cashbackField.reset();
		jproduk_cashbackField.setValue(null);
		jproduk_cashback_cfField.reset();
		jproduk_cashback_cfField.setValue(null);
		
		jproduk_potong_pointField.reset();
		jproduk_potong_pointField.setValue(null);
		jproduk_potong_pointcfField.reset();
		jproduk_potong_pointcfField.setValue(null);
		
		jproduk_ket_diskField.reset();
		jproduk_ket_diskField.setValue(null);
		
		jproduk_keteranganField.reset();
		jproduk_keteranganField.setValue(null);

		jproduk_subTotalField.reset();
		jproduk_subTotalField.setValue(null);
		jproduk_subTotal_cfField.reset();
		jproduk_subTotal_cfField.setValue(null);
		jproduk_subTotalLabel.reset();
		jproduk_subTotalLabel.setValue(null);

		jproduk_totalField.reset();
		jproduk_totalField.setValue(null);
		jproduk_total_cfField.reset();
		jproduk_total_cfField.setValue(null);
		jproduk_TotalLabel.reset();
		jproduk_TotalLabel.setValue(null);

		jproduk_hutangField.reset();
		jproduk_hutangField.setValue(null);
		jproduk_hutang_cfField.reset();
		jproduk_hutang_cfField.setValue(null);
		jproduk_HutangLabel.reset();
		jproduk_HutangLabel.setValue(null);

		jproduk_jumlahField.reset();
		jproduk_jumlahField.setValue(null);
		jproduk_jumlahLabel.reset();
		jproduk_jumlahLabel.setValue(null);
		
		jproduk_pesanLabel.setText("");

		tunai_jual_produk_reset_form();
		tunai2_jual_produk_reset_form();
		tunai3_jual_produk_reset_form();

		kwitansi_jual_produk_reset_form();
		kwitansi2_jual_produk_reset_form();
		kwitansi3_jual_produk_reset_form();

		card_jual_produk_reset_form();
		card2_jual_produk_reset_form();
		card3_jual_produk_reset_form();

		cek_jual_produk_reset_form();
		cek2_jual_produk_reset_form();
		cek3_jual_produk_reset_form();

		transfer_jual_produk_reset_form();
		transfer2_jual_produk_reset_form();
		transfer3_jual_produk_reset_form();

		voucher_jual_produk_reset_form();
		voucher2_jual_produk_reset_form();
		voucher3_jual_produk_reset_form();

		update_group_carabayar_jual_produk();
		update_group_carabayar2_jual_produk();
		update_group_carabayar3_jual_produk();

		jproduk_bayarField.reset();
		jproduk_bayarField.setValue(null);
		jproduk_bayar_cfField.reset();
		jproduk_bayar_cfField.setValue(null);
		jproduk_TotalBayarLabel.reset();
		jproduk_TotalBayarLabel.setValue(null);
		
		/* Enable if jproduk_post2db="CREATE" */
		jproduk_custField.setDisabled(false);
		jproduk_tanggalField.setDisabled(false);
		jproduk_karyawanField.setDisabled(true);
		
		jproduk_tanggalField.setDisabled(false);
		jproduk_keteranganField.setDisabled(false);
		master_cara_bayarTabPanel.setDisabled(false);
		detail_jual_produkListEditorGrid.setDisabled(false);
		jproduk_diskonField.setDisabled(false);
		jproduk_cashback_cfField.setDisabled(false);
		jproduk_potong_pointcfField.setDisabled(false);
		jproduk_ket_diskField.setDisabled(false);
		jproduk_stat_dokField.setDisabled(false);
		<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
		//detail_jual_produkListEditorGrid.djproduk_add.enable();
        detail_jual_produkListEditorGrid.djproduk_delete.enable();
        detail_jual_produkListEditorGrid.djproduk_add.enable();
        detail_jual_produkListEditorGrid.djproduk_add_produk.enable();
        detail_jual_produkListEditorGrid.djproduk_add_satuan.enable();
        detail_jual_produkListEditorGrid.djproduk_add_jumlah.enable();
        detail_jual_produkListEditorGrid.djproduk_add_harga.enable();
        detail_jual_produkListEditorGrid.djproduk_add_subtotal.enable();
        detail_jual_produkListEditorGrid.djproduk_add_jdiskon.enable();
        detail_jual_produkListEditorGrid.djproduk_add_diskon.enable();
        detail_jual_produkListEditorGrid.djproduk_add_subtotalnet.enable();
		
		detail_jual_produkListEditorGrid.djproduk_add_produk.reset();
		detail_jual_produkListEditorGrid.djproduk_add_produk.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_satuan.reset();
		detail_jual_produkListEditorGrid.djproduk_add_satuan.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_jumlah.reset();
		detail_jual_produkListEditorGrid.djproduk_add_jumlah.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_harga.reset();
		detail_jual_produkListEditorGrid.djproduk_add_harga.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_subtotal.reset();
		detail_jual_produkListEditorGrid.djproduk_add_subtotal.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_jdiskon.reset();
		detail_jual_produkListEditorGrid.djproduk_add_jdiskon.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_diskon.reset();
		detail_jual_produkListEditorGrid.djproduk_add_diskon.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_subtotalnet.reset();
		detail_jual_produkListEditorGrid.djproduk_add_subtotalnet.setValue(null);		
		<?php } ?>
		
		jproduk_caraField.setDisabled(false);
		master_jual_produk_tunaiGroup.setDisabled(false);
		master_jual_produk_cardGroup.setDisabled(false);
		master_jual_produk_cekGroup.setDisabled(false);
		master_jual_produk_kwitansiGroup.setDisabled(false);
		master_jual_produk_transferGroup.setDisabled(false);
		master_jual_produk_voucherGroup.setDisabled(false);
		
		jproduk_cara2Field.setDisabled(false);
		master_jual_produk_tunai2Group.setDisabled(false);
		master_jual_produk_card2Group.setDisabled(false);
		master_jual_produk_cek2Group.setDisabled(false);
		master_jual_produk_kwitansi2Group.setDisabled(false);
		master_jual_produk_transfer2Group.setDisabled(false);
		master_jual_produk_voucher2Group.setDisabled(false);
		
		jproduk_cara3Field.setDisabled(false);
		master_jual_produk_tunai3Group.setDisabled(false);
		master_jual_produk_card3Group.setDisabled(false);
		master_jual_produk_cek3Group.setDisabled(false);
		master_jual_produk_kwitansi3Group.setDisabled(false);
		master_jual_produk_transfer3Group.setDisabled(false);
		master_jual_produk_voucher3Group.setDisabled(false);
		
		combo_jual_produk.setDisabled(false);
		combo_satuan_produk.setDisabled(false);
		djumlah_beli_produkField.setDisabled(false);
		dharga_konversiField.setDisabled(false);
		dsub_totalField.setDisabled(false);
		djenis_diskonField.setDisabled(false);
		djumlah_diskonField.setDisabled(false);
		//dsub_total_netField.setDisabled(false);
		combo_reveral.setDisabled(false);
		dharga_defaultField.setDisabled(false);
				
		<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
		master_jual_produk_pembayaranForm.jproduk_savePrint.enable();
		//master_jual_produk_createForm.jproduk_savePrint2.enable();
		master_jual_produk_createForm.jproduk_save.enable();
		master_jual_produk_createForm.jproduk_bayar.enable();
		master_jual_produk_createForm.PrintOnlyButton.disable();
		//master_jual_produk_createForm.PrintOnlyButton2.disable();
		<?php } ?>
		
	}

	function detail_jual_produk_reset_form(){
		detail_jual_produkListEditorGrid.djproduk_add_produk.reset();
		detail_jual_produkListEditorGrid.djproduk_add_produk.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_satuan.reset();
		detail_jual_produkListEditorGrid.djproduk_add_satuan.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_jumlah.reset();
		detail_jual_produkListEditorGrid.djproduk_add_jumlah.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_harga.reset();
		detail_jual_produkListEditorGrid.djproduk_add_harga.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_subtotal.reset();
		detail_jual_produkListEditorGrid.djproduk_add_subtotal.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_jdiskon.reset();
		detail_jual_produkListEditorGrid.djproduk_add_jdiskon.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_diskon.reset();
		detail_jual_produkListEditorGrid.djproduk_add_diskon.setValue(null);
		detail_jual_produkListEditorGrid.djproduk_add_subtotalnet.reset();
		detail_jual_produkListEditorGrid.djproduk_add_subtotalnet.setValue(null);
	}
    
	function master_jual_produk_set_form(){
		
		var hutang_temp=0;
		var subtotal_field=0;
		var dproduk_jumlah_field=0;
		var total_field=0;
		var hutang_field=0;
		var diskon_field=0;
		var cashback_field=0;

		var catatan_cust=master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cust_note');
		var cust_id=master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cust_id');
				
		jproduk_idField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_id'));
		jproduk_nobuktiField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_nobukti'));
		/*jproduk_karyawanField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('karyawan_nama'));
		jproduk_karyawan_idField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_grooming'));
		jproduk_nikkaryawanField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('karyawan_no'));
		jproduk_karyawan_jabatanField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('karyawan_jabatan'));
		jproduk_karyawan_levelField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('karyawan_idlevel'));*/
		jproduk_custField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cust_edit'));
		jproduk_cust_idField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cust_id'));
		jproduk_tanggalField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_tanggal'));
		jproduk_caraField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cara'));
		jproduk_stat_dokField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok'));
		jproduk_cara2Field.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cara2'));
		jproduk_cara3Field.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cara3'));
		jproduk_diskonField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_diskon'));
		jproduk_ket_diskField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_ket_disk'));
		jproduk_cashbackField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cashback'));		
		jproduk_cashback_cfField.setValue(CurrencyFormatted(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cashback')));	
		jproduk_potong_pointField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('potong_point'));		
		jproduk_potong_pointcfField.setValue(CurrencyFormatted(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('potong_point')));
		jproduk_bayarField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_bayar'));
		jproduk_bayar_cfField.setValue(CurrencyFormatted(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_bayar')));
		jproduk_TotalBayarLabel.setValue(CurrencyFormatted(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_bayar')));
		jproduk_keteranganField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_keterangan'));
		t_carabayar1 = master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cara');
		t_carabayar2 = master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cara2');
		if(catatan_cust=="Y")
		{
		    load_catatan_customer(cust_id);
		}else{
		    jproduk_cust_noteField.setValue("");
		}
		
		for(i=0;i<detail_jual_produk_DataStore.getCount();i++){
			subtotal_field+=Math.round(detail_jual_produk_DataStore.getAt(i).data.dproduk_jumlah * detail_jual_produk_DataStore.getAt(i).data.dproduk_harga * ((100 - detail_jual_produk_DataStore.getAt(i).data.dproduk_diskon)/100));
			dproduk_jumlah_field+=parseInt(detail_jual_produk_DataStore.getAt(i).data.dproduk_jumlah);
		}
		if(jproduk_diskonField.getValue()!==""){
			diskon_field=jproduk_diskonField.getValue();
		}		
		if(jproduk_cashbackField.getValue()!==""){
			cashback_field=jproduk_cashbackField.getValue();
		}
		total_field=subtotal_field*(100-diskon_field)/100-cashback_field;
		
		jproduk_jumlahField.setValue(dproduk_jumlah_field);
		jproduk_jumlahLabel.setValue(dproduk_jumlah_field);
		jproduk_subTotalField.setValue(subtotal_field);
		jproduk_subTotal_cfField.setValue(CurrencyFormatted(subtotal_field));
		jproduk_subTotalLabel.setValue(CurrencyFormatted(subtotal_field));
		
		jproduk_totalField.setValue(total_field);
		jproduk_total_cfField.setValue(CurrencyFormatted(total_field));
		jproduk_TotalLabel.setValue(CurrencyFormatted(total_field));
		
		hutang_temp=total_field-jproduk_bayarField.getValue();
		jproduk_hutangField.setValue(hutang_temp);
		jproduk_hutang_cfField.setValue(CurrencyFormatted(hutang_temp));
		jproduk_HutangLabel.setValue(CurrencyFormatted(hutang_temp));
		
		//load_membership();
		update_group_carabayar_jual_produk();
		update_group_carabayar2_jual_produk();
		update_group_carabayar3_jual_produk();

		switch(jproduk_caraField.getValue()){
			case 'kwitansi':
				kwitansi_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 1
					},
					callback: function(opts, success, response)  {
						  if (success) {
							if(kwitansi_jual_produk_DataStore.getCount()){
								jproduk_kwitansi_record=kwitansi_jual_produk_DataStore.getAt(0).data;
								jproduk_kwitansi_no_idField.setValue(jproduk_kwitansi_record.kwitansi_id);
								jproduk_kwitansi_noField.setValue(jproduk_kwitansi_record.kwitansi_no);
								jproduk_kwitansi_namaField.setValue(jproduk_kwitansi_record.cust_nama);
								jproduk_kwitansi_sisaField.setValue(jproduk_kwitansi_record.kwitansi_sisa);
								jproduk_kwitansi_nilaiField.setValue(jproduk_kwitansi_record.jkwitansi_nilai);
								jproduk_kwitansi_nilai_cfField.setValue(CurrencyFormatted(jproduk_kwitansi_record.jkwitansi_nilai));
								jproduk_jual_kwitansi_idField1.setValue(jproduk_kwitansi_record.jkwitansi_id);
								master_jual_produk_kwitansiGroup.doLayout();
							}
						  }
					  }
				});
				break;
			case 'gift_voucher':
				gift_voucher_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 1
					},
					callback: function(opts, success, response)  {
						  if (success) {
							if(gift_voucher_jual_produk_DataStore.getCount()){
								jproduk_kwitansi_record=gift_voucher_jual_produk_DataStore.getAt(0).data;
								jproduk_kwitansi_no_idField.setValue(jproduk_kwitansi_record.kwitansi_id);
								jproduk_kwitansi_noField.setValue(jproduk_kwitansi_record.kwitansi_no);
								jproduk_kwitansi_namaField.setValue(jproduk_kwitansi_record.cust_nama);
								jproduk_kwitansi_sisaField.setValue(jproduk_kwitansi_record.kwitansi_sisa);
								jproduk_kwitansi_nilaiField.setValue(jproduk_kwitansi_record.jkwitansi_nilai);
								jproduk_kwitansi_nilai_cfField.setValue(CurrencyFormatted(jproduk_kwitansi_record.jkwitansi_nilai));
								jproduk_jual_kwitansi_idField1.setValue(jproduk_kwitansi_record.jkwitansi_id);
								master_jual_produk_kwitansiGroup.doLayout();
							}
						  }
					  }
				});
				break;
			case 'card' :
				card_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 1
					},
					callback: function(opts, success, response)  {
						 if (success) { 
							if(card_jual_produk_DataStore.getCount()){
								jproduk_card_record=card_jual_produk_DataStore.getAt(0).data;
								jproduk_card_namaField.setValue(jproduk_card_record.jcard_nama);
								jproduk_card_edcField.setValue(jproduk_card_record.jcard_edc);
								jproduk_card_promoField.setValue(jproduk_card_record.jcard_promo);
								jproduk_card_noField.setValue(jproduk_card_record.jcard_no);
								jproduk_card_nilaiField.setValue(jproduk_card_record.jcard_nilai);
								jproduk_card_nilai_cfField.setValue(CurrencyFormatted(jproduk_card_record.jcard_nilai));
							}
						 }
					}
				});
				break;
			case 'cek/giro':
				cek_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 1
					},
					callback: function(opts, success, response)  {
							if (success) {
								if(cek_jual_produk_DataStore.getCount()){
									jproduk_cek_record=cek_jual_produk_DataStore.getAt(0).data;
									jproduk_cek_namaField.setValue(jproduk_cek_record.jcek_nama);
									jproduk_cek_noField.setValue(jproduk_cek_record.jcek_no);
									jproduk_cek_validField.setValue(jproduk_cek_record.jcek_valid);
									jproduk_cek_bankField.setValue(jproduk_cek_record.jcek_bank);
									jproduk_cek_nilaiField.setValue(jproduk_cek_record.jcek_nilai);
									jproduk_cek_nilai_cfField.setValue(CurrencyFormatted(jproduk_cek_record.jcek_nilai));
								}
							}
					 	}
				  });
				break;								
			case 'transfer' :
				transfer_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 1
						},
					  	callback: function(opts, success, response)  {
							if (success) {
									if(transfer_jual_produk_DataStore.getCount()){
										jproduk_transfer_record=transfer_jual_produk_DataStore.getAt(0);
										jproduk_transfer_bankField.setValue(jproduk_transfer_record.data.jtransfer_bank);
										jproduk_transfer_namaField.setValue(jproduk_transfer_record.data.jtransfer_nama);
										jproduk_transfer_nilaiField.setValue(jproduk_transfer_record.data.jtransfer_nilai);
										jproduk_transfer_nilai_cfField.setValue(CurrencyFormatted(jproduk_transfer_record.data.jtransfer_nilai));
									}
							}
					 	}
				  });
				break;
			case 'tunai' :
				tunai_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 1
						},
					  	callback: function(opts, success, response)  {
							if (success) {
									if(tunai_jual_produk_DataStore.getCount()){
										jproduk_tunai_record=tunai_jual_produk_DataStore.getAt(0);
										jproduk_tunai_nilaiField.setValue(jproduk_tunai_record.data.jtunai_nilai);
										jproduk_tunai_nilai_cfField.setValue(CurrencyFormatted(jproduk_tunai_record.data.jtunai_nilai));
									}
							}
					 	}
				  });
				break;
			case 'voucher' :
				voucher_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 1
						},
					  	callback: function(opts, success, response)  {
							if (success) {
									if(voucher_jual_produk_DataStore.getCount()){
										jproduk_voucher_record=voucher_jual_produk_DataStore.getAt(0);
										jproduk_voucher_noField.setValue(jproduk_voucher_record.data.tvoucher_novoucher);
										jproduk_voucher_cashbackField.setValue(jproduk_voucher_record.data.tvoucher_nilai);
										jproduk_voucher_cashback_cfField.setValue(CurrencyFormatted(jproduk_voucher_record.data.tvoucher_nilai));
									}
							}
					 	}
				  });
				break;
		}

		switch(jproduk_cara2Field.getValue()){
			case 'kwitansi':
				kwitansi_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 2,
						cara_bayar_1: t_carabayar1,
						cara_bayar_2: t_carabayar2
					},
					callback: function(opts, success, response)  {
						  if (success) {
							if(kwitansi_jual_produk_DataStore.getCount()){
								jproduk_kwitansi_record=kwitansi_jual_produk_DataStore.getAt(0).data;
								jproduk_kwitansi_no2_idField.setValue(jproduk_kwitansi_record.kwitansi_id);
								jproduk_kwitansi_no2Field.setValue(jproduk_kwitansi_record.kwitansi_no);
								jproduk_kwitansi_nama2Field.setValue(jproduk_kwitansi_record.cust_nama);
								jproduk_kwitansi_sisa2Field.setValue(jproduk_kwitansi_record.kwitansi_sisa);
								jproduk_kwitansi_nilai2Field.setValue(jproduk_kwitansi_record.jkwitansi_nilai);
								jproduk_kwitansi_nilai2_cfField.setValue(CurrencyFormatted(jproduk_kwitansi_record.jkwitansi_nilai));
								jproduk_jual_kwitansi_idField2.setValue(jproduk_kwitansi_record.jkwitansi_id);
								
								jproduk_kwitansi_no2Field.setWidth(293);
								jproduk_kwitansi_nama2Field.setWidth(293);
								jproduk_kwitansi_sisa2Field.setWidth(293);
								jproduk_kwitansi_nilai2_cfField.setWidth(293);
								
								master_jual_produk_kwitansi2Group.doLayout();
							}
						  }
					  }
				});
				break;
			case 'gift_voucher':
				gift_voucher_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 2,
						cara_bayar_1: t_carabayar1,
						cara_bayar_2: t_carabayar2
					},
					callback: function(opts, success, response)  {
						  if (success) {
							if(gift_voucher_jual_produk_DataStore.getCount()){
								jproduk_kwitansi_record=gift_voucher_jual_produk_DataStore.getAt(0).data;
								jproduk_kwitansi_no2_idField.setValue(jproduk_kwitansi_record.kwitansi_id);
								jproduk_kwitansi_no2Field.setValue(jproduk_kwitansi_record.kwitansi_no);
								jproduk_kwitansi_nama2Field.setValue(jproduk_kwitansi_record.cust_nama);
								jproduk_kwitansi_sisa2Field.setValue(jproduk_kwitansi_record.kwitansi_sisa);
								jproduk_kwitansi_nilai2Field.setValue(jproduk_kwitansi_record.jkwitansi_nilai);
								jproduk_kwitansi_nilai2_cfField.setValue(CurrencyFormatted(jproduk_kwitansi_record.jkwitansi_nilai));
								jproduk_jual_kwitansi_idField2.setValue(jproduk_kwitansi_record.jkwitansi_id);
								
								jproduk_kwitansi_no2Field.setWidth(293);
								jproduk_kwitansi_nama2Field.setWidth(293);
								jproduk_kwitansi_sisa2Field.setWidth(293);
								jproduk_kwitansi_nilai2_cfField.setWidth(293);
								
								master_jual_produk_kwitansi2Group.doLayout();
							}
						  }
					  }
				});
				break;
			case 'card' :
				card_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 2,
						cara_bayar_1: t_carabayar1,
						cara_bayar_2: t_carabayar2
					},
					callback: function(opts, success, response)  {
						 if (success) { 
							 if(card_jual_produk_DataStore.getCount()){
								 jproduk_card_record=card_jual_produk_DataStore.getAt(0).data;
								 jproduk_card_nama2Field.setValue(jproduk_card_record.jcard_nama);
								 jproduk_card_edc2Field.setValue(jproduk_card_record.jcard_edc);
								 jproduk_card_promo2Field.setValue(jproduk_card_record.jcard_promo);
								 jproduk_card_no2Field.setValue(jproduk_card_record.jcard_no);
								 jproduk_card_nilai2Field.setValue(jproduk_card_record.jcard_nilai);
								 jproduk_card_nilai2_cfField.setValue(CurrencyFormatted(jproduk_card_record.jcard_nilai));
							 }
						 }
					}
				});
				break;
			case 'cek/giro':
				cek_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 2,
						cara_bayar_1: t_carabayar1,
						cara_bayar_2: t_carabayar2
					},
					callback: function(opts, success, response)  {
							if (success) {
								if(cek_jual_produk_DataStore.getCount()){
									jproduk_cek_record=cek_jual_produk_DataStore.getAt(0).data;
									jproduk_cek_nama2Field.setValue(jproduk_cek_record.jcek_nama);
									jproduk_cek_no2Field.setValue(jproduk_cek_record.jcek_no);
									jproduk_cek_valid2Field.setValue(jproduk_cek_record.jcek_valid);
									jproduk_cek_bank2Field.setValue(jproduk_cek_record.jcek_bank);
									jproduk_cek_nilai2Field.setValue(jproduk_cek_record.jcek_nilai);
									jproduk_cek_nilai2_cfField.setValue(CurrencyFormatted(jproduk_cek_record.jcek_nilai));
								}
							}
					 	}
				  });
				break;								
			case 'transfer' :
				transfer_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 2,
							cara_bayar_1: t_carabayar1,
							cara_bayar_2: t_carabayar2
						},
					  	callback: function(opts, success, response)  {
							if (success) {
								jproduk_transfer_record=transfer_jual_produk_DataStore.getAt(0);
									if(transfer_jual_produk_DataStore.getCount()){
										jproduk_transfer_record=transfer_jual_produk_DataStore.getAt(0);
										jproduk_transfer_bank2Field.setValue(jproduk_transfer_record.data.jtransfer_bank);
										jproduk_transfer_nama2Field.setValue(jproduk_transfer_record.data.jtransfer_nama);
										jproduk_transfer_nilai2Field.setValue(jproduk_transfer_record.data.jtransfer_nilai);
										jproduk_transfer_nilai2_cfField.setValue(CurrencyFormatted(jproduk_transfer_record.data.jtransfer_nilai));
									}
							}
					 	}
				  });
				break;
			case 'tunai' :
				tunai_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 2,
							cara_bayar_1: t_carabayar1,
							cara_bayar_2: t_carabayar2
						},
					  	callback: function(opts, success, response)  {
							if (success) {
									if(tunai_jual_produk_DataStore.getCount()){
										jproduk_tunai_record=tunai_jual_produk_DataStore.getAt(0);
										jproduk_tunai_nilai2Field.setValue(jproduk_tunai_record.data.jtunai_nilai);
										jproduk_tunai_nilai2_cfField.setValue(CurrencyFormatted(jproduk_tunai_record.data.jtunai_nilai));
									}
							}
					 	}
				  });
				break;
			case 'voucher' :
				voucher_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 2,
							cara_bayar_1: t_carabayar1,
							cara_bayar_2: t_carabayar2
						},
					  	callback: function(opts, success, response)  {
							if (success) {
									if(voucher_jual_produk_DataStore.getCount()){
										jproduk_voucher_record=voucher_jual_produk_DataStore.getAt(0);
										jproduk_voucher_no2Field.setValue(jproduk_voucher_record.data.tvoucher_novoucher);
										jproduk_voucher_cashback2Field.setValue(jproduk_voucher_record.data.tvoucher_nilai);
										jproduk_voucher_cashback2_cfField.setValue(CurrencyFormatted(jproduk_voucher_record.data.tvoucher_nilai));
									}
							}
					 	}
				  });
				break;
		}

		switch(jproduk_cara3Field.getValue()){
			case 'kwitansi':
				kwitansi_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 3,
						cara_bayar_1: t_carabayar1,
						cara_bayar_2: t_carabayar2
					},
					callback: function(opts, success, response)  {
						  if (success) {
							if(kwitansi_jual_produk_DataStore.getCount()){
								jproduk_kwitansi_record=kwitansi_jual_produk_DataStore.getAt(0).data;
								jproduk_kwitansi_no3_idField.setValue(jproduk_kwitansi_record.kwitansi_id);
								jproduk_kwitansi_no3Field.setValue(jproduk_kwitansi_record.kwitansi_no);
								jproduk_kwitansi_nama3Field.setValue(jproduk_kwitansi_record.cust_nama);
								jproduk_kwitansi_sisa3Field.setValue(jproduk_kwitansi_record.kwitansi_sisa);
								jproduk_kwitansi_nilai3Field.setValue(jproduk_kwitansi_record.jkwitansi_nilai);
								jproduk_kwitansi_nilai3_cfField.setValue(CurrencyFormatted(jproduk_kwitansi_record.jkwitansi_nilai));
								jproduk_jual_kwitansi_idField3.setValue(jproduk_kwitansi_record.jkwitansi_id);
								
								jproduk_kwitansi_no3Field.setWidth(293);
								jproduk_kwitansi_nama3Field.setWidth(293);
								jproduk_kwitansi_sisa3Field.setWidth(293);
								jproduk_kwitansi_nilai3_cfField.setWidth(293);
								
								master_jual_produk_kwitansi3Group.doLayout();
							}
						  }
					  }
				});
				break;
			case 'gift_voucher':
				gift_voucher_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 3,
						cara_bayar_1: t_carabayar1,
						cara_bayar_2: t_carabayar2
					},
					callback: function(opts, success, response)  {
						  if (success) {
							if(gift_voucher_jual_produk_DataStore.getCount()){
								jproduk_kwitansi_record=gift_voucher_jual_produk_DataStore.getAt(0).data;
								jproduk_kwitansi_no3_idField.setValue(jproduk_kwitansi_record.kwitansi_id);
								jproduk_kwitansi_no3Field.setValue(jproduk_kwitansi_record.kwitansi_no);
								jproduk_kwitansi_nama3Field.setValue(jproduk_kwitansi_record.cust_nama);
								jproduk_kwitansi_sisa3Field.setValue(jproduk_kwitansi_record.kwitansi_sisa);
								jproduk_kwitansi_nilai3Field.setValue(jproduk_kwitansi_record.jkwitansi_nilai);
								jproduk_kwitansi_nilai3_cfField.setValue(CurrencyFormatted(jproduk_kwitansi_record.jkwitansi_nilai));
								jproduk_jual_kwitansi_idField3.setValue(jproduk_kwitansi_record.jkwitansi_id);
								
								jproduk_kwitansi_no3Field.setWidth(293);
								jproduk_kwitansi_nama3Field.setWidth(293);
								jproduk_kwitansi_sisa3Field.setWidth(293);
								jproduk_kwitansi_nilai3_cfField.setWidth(293);
								
								master_jual_produk_kwitansi3Group.doLayout();
							}
						  }
					  }
				});
				break;
			case 'card' :
				card_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 3,
						cara_bayar_1: t_carabayar1,
						cara_bayar_2: t_carabayar2
					},
					callback: function(opts, success, response)  {
						 if (success) { 
							 if(card_jual_produk_DataStore.getCount()){
								 jproduk_card_record=card_jual_produk_DataStore.getAt(0).data;
								 jproduk_card_nama3Field.setValue(jproduk_card_record.jcard_nama);
								 jproduk_card_edc3Field.setValue(jproduk_card_record.jcard_edc);
								 jproduk_card_promo3Field.setValue(jproduk_card_record.jcard_promo);
								 jproduk_card_no3Field.setValue(jproduk_card_record.jcard_no);
								 jproduk_card_nilai3Field.setValue(jproduk_card_record.jcard_nilai);
								 jproduk_card_nilai3_cfField.setValue(CurrencyFormatted(jproduk_card_record.jcard_nilai));
								 
							 }
						 }
					}
				});
				break;
			case 'cek/giro':
				cek_jual_produk_DataStore.load({
					params : {
						no_faktur: jproduk_nobuktiField.getValue(),
						cara_bayar_ke: 3,
						cara_bayar_1: t_carabayar1,
						cara_bayar_2: t_carabayar2
					},
					callback: function(opts, success, response)  {
							if (success) {
								if(cek_jual_produk_DataStore.getCount()){
									jproduk_cek_record=cek_jual_produk_DataStore.getAt(0).data;
									jproduk_cek_nama3Field.setValue(jproduk_cek_record.jcek_nama);
									jproduk_cek_no3Field.setValue(jproduk_cek_record.jcek_no);
									jproduk_cek_valid3Field.setValue(jproduk_cek_record.jcek_valid);
									jproduk_cek_bank3Field.setValue(jproduk_cek_record.jcek_bank);
									jproduk_cek_nilai3Field.setValue(jproduk_cek_record.jcek_nilai);
									jproduk_cek_nilai3_cfField.setValue(CurrencyFormatted(jproduk_cek_record.jcek_nilai));
								}
							}
					 	}
				  });
				break;								
			case 'transfer' :
				transfer_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 3,
							cara_bayar_1: t_carabayar1,
							cara_bayar_2: t_carabayar2
						},
					  	callback: function(opts, success, response)  {
							if (success) {
								jproduk_transfer_record=transfer_jual_produk_DataStore.getAt(0);
									if(transfer_jual_produk_DataStore.getCount()){
										jproduk_transfer_record=transfer_jual_produk_DataStore.getAt(0);
										jproduk_transfer_bank3Field.setValue(jproduk_transfer_record.data.jtransfer_bank);
										jproduk_transfer_nama3Field.setValue(jproduk_transfer_record.data.jtransfer_nama);
										jproduk_transfer_nilai3Field.setValue(jproduk_transfer_record.data.jtransfer_nilai);
										jproduk_transfer_nilai3_cfField.setValue(CurrencyFormatted(jproduk_transfer_record.data.jtransfer_nilai));
									}
							}
					 	}
				  });
				break;
			case 'tunai' :
				tunai_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 3,
							cara_bayar_1: t_carabayar1,
							cara_bayar_2: t_carabayar2
						},
					  	callback: function(opts, success, response)  {
							if (success) {
									if(tunai_jual_produk_DataStore.getCount()){
										jproduk_tunai_record=tunai_jual_produk_DataStore.getAt(0);
										jproduk_tunai_nilai3Field.setValue(jproduk_tunai_record.data.jtunai_nilai);
										jproduk_tunai_nilai3_cfField.setValue(CurrencyFormatted(jproduk_tunai_record.data.jtunai_nilai));
									}
							}
					 	}
				  });
				break;
			case 'voucher' :
				voucher_jual_produk_DataStore.load({
						params : {
							no_faktur: jproduk_nobuktiField.getValue(),
							cara_bayar_ke: 3,
							cara_bayar_1: t_carabayar1,
							cara_bayar_2: t_carabayar2
						},
					  	callback: function(opts, success, response)  {
							if (success) {
									if(voucher_jual_produk_DataStore.getCount()){
										jproduk_voucher_record=voucher_jual_produk_DataStore.getAt(0);
										jproduk_voucher_no3Field.setValue(jproduk_voucher_record.data.tvoucher_novoucher);
										jproduk_voucher_cashback3Field.setValue(jproduk_voucher_record.data.tvoucher_nilai);
										jproduk_voucher_cashback3_cfField.setValue(CurrencyFormatted(jproduk_voucher_record.data.tvoucher_nilai));
									}
							}
					 	}
				  });
				break;
		}
        
        //Jika jproduk_post2db='UPDATE' dan jproduk_stat_dok='Tertutup' ==> detail_jual_produkListEditorGrid.djproduk_add di-disable
        if(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok')=='Tertutup'){
            <?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			//detail_jual_produkListEditorGrid.djproduk_add.disable();
            detail_jual_produkListEditorGrid.djproduk_delete.disable();
            detail_jual_produkListEditorGrid.djproduk_add.disable();
			detail_jual_produkListEditorGrid.djproduk_add_produk.disable();
			detail_jual_produkListEditorGrid.djproduk_add_satuan.disable();
			detail_jual_produkListEditorGrid.djproduk_add_jumlah.disable();
			detail_jual_produkListEditorGrid.djproduk_add_harga.disable();
			detail_jual_produkListEditorGrid.djproduk_add_subtotal.disable();
			detail_jual_produkListEditorGrid.djproduk_add_jdiskon.disable();
			detail_jual_produkListEditorGrid.djproduk_add_diskon.disable();
			detail_jual_produkListEditorGrid.djproduk_add_subtotalnet.disable();
			<?php } ?>
        }else if(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok')=='Terbuka'){
            <?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			//detail_jual_produkListEditorGrid.djproduk_add.enable();
            detail_jual_produkListEditorGrid.djproduk_delete.enable();
            detail_jual_produkListEditorGrid.djproduk_add.enable();
			detail_jual_produkListEditorGrid.djproduk_add_produk.enable();
			detail_jual_produkListEditorGrid.djproduk_add_satuan.enable();
			detail_jual_produkListEditorGrid.djproduk_add_jumlah.enable();
			detail_jual_produkListEditorGrid.djproduk_add_harga.enable();
			detail_jual_produkListEditorGrid.djproduk_add_subtotal.enable();
			detail_jual_produkListEditorGrid.djproduk_add_jdiskon.enable();
			detail_jual_produkListEditorGrid.djproduk_add_diskon.enable();
			detail_jual_produkListEditorGrid.djproduk_add_subtotalnet.enable();
			<?php } ?>
        }
		
		jproduk_stat_dokField.on("select",function(){
		var status_awal = master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok');
		if(status_awal =='Terbuka' && jproduk_stat_dokField.getValue()=='Tertutup')
		{
		Ext.MessageBox.show({
			msg: 'Dokumen tidak bisa ditutup. Gunakan Final untuk menutup dokumen',
			buttons: Ext.MessageBox.OK,
			animEl: 'save',
			icon: Ext.MessageBox.WARNING
		   });
		jproduk_stat_dokField.setValue('Terbuka');
		}
		
		else if(status_awal =='Tertutup' && jproduk_stat_dokField.getValue()=='Terbuka')
		{
		Ext.MessageBox.show({
			msg: 'Status yang sudah Tertutup tidak dapat diganti Terbuka',
			buttons: Ext.MessageBox.OK,
			animEl: 'save',
			icon: Ext.MessageBox.WARNING
		   });
		jproduk_stat_dokField.setValue('Tertutup');
		master_jual_produk_pembayaranForm.jproduk_savePrint.disable();
		//master_jual_produk_createForm.jproduk_savePrint2.disable();
		master_jual_produk_createForm.jproduk_save.disable();
		master_jual_produk_createForm.jproduk_bayar.disable();
		}
		
		else if(status_awal =='Tertutup' && jproduk_stat_dokField.getValue()=='Tertutup')
		{
			master_jual_produk_pembayaranForm.jproduk_savePrint.disable();
			//master_jual_produk_createForm.jproduk_savePrint2.disable();
			master_jual_produk_createForm.jproduk_save.disable();
			master_jual_produk_createForm.jproduk_bayar.disable();
		}
		
		else if(status_awal =='Batal' && jproduk_stat_dokField.getValue()=='Terbuka')
		{
		Ext.MessageBox.show({
			msg: 'Status yang sudah Tertutup tidak dapat diganti Terbuka',
			buttons: Ext.MessageBox.OK,
			animEl: 'save',
			icon: Ext.MessageBox.WARNING
		   });
		jproduk_stat_dokField.setValue('Tertutup');
		}
		
		else if(jproduk_stat_dokField.getValue()=='Batal')
		{
		Ext.MessageBox.confirm('Konfirmasi','Anda yakin untuk membatalkan dokumen ini?<br><br>Dokumen yang telah dibatalkan tidak dapat dikembalikan lagi.', jproduk_status_batal);
		master_jual_produk_createForm.jproduk_save.disable();
		master_jual_produk_createForm.jproduk_bayar.disable();
		master_jual_produk_pembayaranForm.jproduk_savePrint.disable();
		//master_jual_produk_createForm.jproduk_savePrint2.disable();
		}
        
        else if(status_awal =='Tertutup' && jproduk_stat_dokField.getValue()=='Tertutup'){
            <?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			master_jual_produk_pembayaranForm.jproduk_savePrint.disable();
			//master_jual_produk_createForm.jproduk_savePrint2.disable();
			master_jual_produk_createForm.jproduk_save.disable();
			master_jual_produk_createForm.jproduk_bayar.disable();
			master_jual_produk_createForm.PrintOnlyButton.disable();
			//master_jual_produk_createForm.PrintOnlyButton2.disable();
			<?php } ?>
        }
		});		
		
		if(jproduk_caraField.getValue()=='gift_voucher')
			jproduk_caraField.setValue('kwitansi');
		if(jproduk_cara2Field.getValue()=='gift_voucher')
			jproduk_cara2Field.setValue('kwitansi');
		if(jproduk_cara3Field.getValue()=='gift_voucher')
			jproduk_cara3Field.setValue('kwitansi');			
		
	}
	
	function jproduk_status_batal(btn){
	if(btn=='yes')
	{
		jproduk_stat_dokField.setValue('Batal');
		<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
        master_jual_produk_pembayaranForm.jproduk_savePrint.disable();
		//master_jual_produk_createForm.jproduk_savePrint2.disable();
		master_jual_produk_createForm.jproduk_save.enable();
		master_jual_produk_createForm.jproduk_bayar.enable();
		master_jual_produk_createForm.PrintOnlyButton.enable();
		//master_jual_produk_createForm.PrintOnlyButton2.enable();
		<?php } ?>
	}  
	else
		jproduk_stat_dokField.setValue(master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok'));
	}

	function master_jual_produk_set_updating(){
		if(jproduk_post2db=="UPDATE" && master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok')=="Terbuka"){
			jproduk_custField.setDisabled(true);
			jproduk_tanggalField.setDisabled(true);
			jproduk_keteranganField.setDisabled(false);
			jproduk_caraField.setDisabled(false);
			master_jual_produk_tunaiGroup.setDisabled(false);
			master_jual_produk_cardGroup.setDisabled(false);
			master_jual_produk_cekGroup.setDisabled(false);
			master_jual_produk_kwitansiGroup.setDisabled(false);
			master_jual_produk_transferGroup.setDisabled(false);
			master_jual_produk_voucherGroup.setDisabled(false);
			
			jproduk_cara2Field.setDisabled(false);
			master_jual_produk_tunai2Group.setDisabled(false);
			master_jual_produk_card2Group.setDisabled(false);
			master_jual_produk_cek2Group.setDisabled(false);
			master_jual_produk_kwitansi2Group.setDisabled(false);
			master_jual_produk_transfer2Group.setDisabled(false);
			master_jual_produk_voucher2Group.setDisabled(false);
			
			jproduk_cara3Field.setDisabled(false);
			master_jual_produk_tunai3Group.setDisabled(false);
			master_jual_produk_card3Group.setDisabled(false);
			master_jual_produk_cek3Group.setDisabled(false);
			master_jual_produk_kwitansi3Group.setDisabled(false);
			master_jual_produk_transfer3Group.setDisabled(false);
			master_jual_produk_voucher3Group.setDisabled(false);
			
			if(typeof(master_jual_produk_pembayaranForm.jproduk_savePrint) !== "undefined")	
				master_jual_produk_pembayaranForm.jproduk_savePrint.enable();
			
		//	if(typeof(master_jual_produk_createForm.jproduk_savePrint2) !== "undefined")		
			//	master_jual_produk_createForm.jproduk_savePrint2.enable();
			
			if(typeof(master_jual_produk_createForm.jproduk_save) !== "undefined")		
				master_jual_produk_createForm.jproduk_save.enable();
				
				if(typeof(master_jual_produk_createForm.jproduk_bayar) !== "undefined")		
				master_jual_produk_createForm.jproduk_bayar.enable();
			
			if(typeof(master_jual_produk_createForm.PrintOnlyButton) !== "undefined")	
				master_jual_produk_createForm.PrintOnlyButton.disable();
			
			//if(typeof(master_jual_produk_createForm.PrintOnlyButton2) !== "undefined")		
			//	master_jual_produk_createForm.PrintOnlyButton2.disable();

			combo_jual_produk.setDisabled(false);
			combo_satuan_produk.setDisabled(false);			
			djumlah_beli_produkField.setDisabled(false);
			dsub_totalField.setDisabled(false);
			djenis_diskonField.setDisabled(false);
			//dsub_total_netField.setDisabled(false);
			combo_reveral.setDisabled(false);
			dharga_defaultField.setDisabled(false);
            jproduk_diskonField.setDisabled(false);
            jproduk_cashback_cfField.setDisabled(false);
            jproduk_potong_pointcfField.setDisabled(false);
			jproduk_ket_diskField.setDisabled(false);
            jproduk_stat_dokField.setDisabled(false);
			
			djumlah_diskonField.setDisabled(true);
			dharga_konversiField.setDisabled(true);
		}
		if(jproduk_post2db=="UPDATE" && master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok')=="Tertutup"){
			jproduk_custField.setDisabled(true);
			jproduk_tanggalField.setDisabled(true);
			jproduk_keteranganField.setDisabled(true);
			jproduk_karyawanField.setDisabled(true);
			jproduk_nikkaryawanField.setDisabled(true);
			jproduk_caraField.setDisabled(true);
			master_jual_produk_tunaiGroup.setDisabled(true);
			master_jual_produk_cardGroup.setDisabled(true);
			master_jual_produk_cekGroup.setDisabled(true);
			master_jual_produk_kwitansiGroup.setDisabled(true);
			master_jual_produk_transferGroup.setDisabled(true);
			master_jual_produk_voucherGroup.setDisabled(true);
			
			jproduk_cara2Field.setDisabled(true);
			master_jual_produk_tunai2Group.setDisabled(true);
			master_jual_produk_card2Group.setDisabled(true);
			master_jual_produk_cek2Group.setDisabled(true);
			master_jual_produk_kwitansi2Group.setDisabled(true);
			master_jual_produk_transfer2Group.setDisabled(true);
			master_jual_produk_voucher2Group.setDisabled(true);
			
			jproduk_cara3Field.setDisabled(true);
			master_jual_produk_tunai3Group.setDisabled(true);
			master_jual_produk_card3Group.setDisabled(true);
			master_jual_produk_cek3Group.setDisabled(true);
			master_jual_produk_kwitansi3Group.setDisabled(true);
			master_jual_produk_transfer3Group.setDisabled(true);
			master_jual_produk_voucher3Group.setDisabled(true);
			
			if(typeof(master_jual_produk_pembayaranForm.jproduk_savePrint) !== "undefined")	
				master_jual_produk_pembayaranForm.jproduk_savePrint.disable();
			
			//if(typeof(master_jual_produk_createForm.jproduk_savePrint2) !== "undefined")	
			//	master_jual_produk_createForm.jproduk_savePrint2.disable();
			
			if(typeof(master_jual_produk_createForm.jproduk_save) !== "undefined")	
				master_jual_produk_createForm.jproduk_save.disable();
				
				if(typeof(master_jual_produk_createForm.jproduk_bayar) !== "undefined")	
				master_jual_produk_createForm.jproduk_bayar.disable();
			
			if(typeof(master_jual_produk_createForm.PrintOnlyButton) !== "undefined")	
				master_jual_produk_createForm.PrintOnlyButton.enable();
			
			//if(typeof(master_jual_produk_createForm.PrintOnlyButton2) !== "undefined")	
			//	master_jual_produk_createForm.PrintOnlyButton2.enable();
			
			combo_jual_produk.setDisabled(true);
			combo_satuan_produk.setDisabled(true);
			djumlah_beli_produkField.setDisabled(true);
			dharga_konversiField.setDisabled(true);
			dsub_totalField.setDisabled(true);
			djenis_diskonField.setDisabled(true);
			djumlah_diskonField.setDisabled(true);
			//dsub_total_netField.setDisabled(true);
			combo_reveral.setDisabled(true);
			dharga_defaultField.setDisabled(true);
			jproduk_diskonField.setDisabled(true);
			jproduk_cashback_cfField.setDisabled(true);
			jproduk_potong_pointcfField.setDisabled(true);
			jproduk_ket_diskField.setDisabled(true);
			jproduk_stat_dokField.setDisabled(false);
		}
		if(jproduk_post2db=="UPDATE" && master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_stat_dok')=="Batal"){
			jproduk_custField.setDisabled(true);
			jproduk_tanggalField.setDisabled(true);
			jproduk_keteranganField.setDisabled(true);
			jproduk_stat_dokField.setDisabled(true);
			jproduk_caraField.setDisabled(true);
			jproduk_karyawanField.setDisabled(true);
			jproduk_nikkaryawanField.setDisabled(true);
			master_jual_produk_tunaiGroup.setDisabled(true);
			master_jual_produk_cardGroup.setDisabled(true);
			master_jual_produk_cekGroup.setDisabled(true);
			master_jual_produk_kwitansiGroup.setDisabled(true);
			master_jual_produk_transferGroup.setDisabled(true);
			master_jual_produk_voucherGroup.setDisabled(true);
			
			jproduk_cara2Field.setDisabled(true);
			master_jual_produk_tunai2Group.setDisabled(true);
			master_jual_produk_card2Group.setDisabled(true);
			master_jual_produk_cek2Group.setDisabled(true);
			master_jual_produk_kwitansi2Group.setDisabled(true);
			master_jual_produk_transfer2Group.setDisabled(true);
			master_jual_produk_voucher2Group.setDisabled(true);
			
			jproduk_cara3Field.setDisabled(true);
			master_jual_produk_tunai3Group.setDisabled(true);
			master_jual_produk_card3Group.setDisabled(true);
			master_jual_produk_cek3Group.setDisabled(true);
			master_jual_produk_kwitansi3Group.setDisabled(true);
			master_jual_produk_transfer3Group.setDisabled(true);
			master_jual_produk_voucher3Group.setDisabled(true);
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			//detail_jual_produkListEditorGrid.djproduk_add.disable();
			detail_jual_produkListEditorGrid.djproduk_delete.disable();
			detail_jual_produkListEditorGrid.djproduk_add.disable();
			detail_jual_produkListEditorGrid.djproduk_add_produk.disable();
			detail_jual_produkListEditorGrid.djproduk_add_satuan.disable();
			detail_jual_produkListEditorGrid.djproduk_add_jumlah.disable();
			detail_jual_produkListEditorGrid.djproduk_add_harga.disable();
			detail_jual_produkListEditorGrid.djproduk_add_subtotal.disable();
			detail_jual_produkListEditorGrid.djproduk_add_jdiskon.disable();
			detail_jual_produkListEditorGrid.djproduk_add_diskon.disable();
			detail_jual_produkListEditorGrid.djproduk_add_subtotalnet.disable();
			<?php } ?>
			combo_jual_produk.setDisabled(true);
			combo_satuan_produk.setDisabled(true);
			djumlah_beli_produkField.setDisabled(true);
			dharga_konversiField.setDisabled(true);
			dsub_totalField.setDisabled(true);
			djenis_diskonField.setDisabled(true);
			djumlah_diskonField.setDisabled(true);
			//dsub_total_netField.setDisabled(true);
			combo_reveral.setDisabled(true);
			dharga_defaultField.setDisabled(true);
			jproduk_diskonField.setDisabled(true);
			jproduk_cashback_cfField.setDisabled(true);
			jproduk_potong_pointcfField.setDisabled(true);
			jproduk_ket_diskField.setDisabled(true);

			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			master_jual_produk_pembayaranForm.jproduk_savePrint.disable();
			//master_jual_produk_createForm.jproduk_savePrint2.disable();
			master_jual_produk_createForm.jproduk_save.disable();
			master_jual_produk_createForm.jproduk_bayar.disable();
			master_jual_produk_createForm.PrintOnlyButton.disable();
			//master_jual_produk_createForm.PrintOnlyButton2.disable();
			<?php } ?>
		}
	}
  
  /*  function load_membership(){
		var cust_id=0;
		if(jproduk_post2db=="CREATE"){
			cust_id=jproduk_custField.getValue();
		}else if(jproduk_post2db=="UPDATE"){
			cust_id=jproduk_cust_idField.getValue();
		}
		
		if(jproduk_custField.getValue()!=''){
			memberDataStore.load({
					params : { member_cust: cust_id},
					callback: function(opts, success, response)  {
						 if (success) {
							if(memberDataStore.getCount()){
								jproduk_member_record=memberDataStore.getAt(0).data;

								t_member_level = jproduk_member_record.cust_member_level;
								
								if(t_member_level == 2){
									t_member_type = "Priority";
								} else {
									t_member_type = "Regular";
								}

								if(t_member_level > 0)
									jproduk_cust_nomemberField.setText(jproduk_member_record.member_no + " (" + t_member_type + ")");
								else
									jproduk_cust_nomemberField.setText(jproduk_member_record.member_no);

								//jproduk_valid_memberField.setValue(jproduk_member_record.member_valid);
								jproduk_cust_ultahField.setValue(jproduk_member_record.cust_tgllahir);
								jproduk_ownerField.setValue(jproduk_member_record.cust_owner);	
								jproduk_tahun_ambil_disk_ultah.setValue(jproduk_member_record.tahun_ambil_disk_ultah_ft);		
								jproduk_tgl_ambil_disk_ultah.setValue(jproduk_member_record.tgl_ambil_disk_ultah_ft);										
									if (jproduk_cust_priorityField.getValue()=='*') {
										jproduk_cust_priorityLabel.setText("*");
									}
									else {
										jproduk_cust_priorityLabel.setText("");
									}	
								
							}
						}
					}
			}); 
		}
	}*/
	
	/*function load_karyawan(){
		var karyawan_id=0;
		if(jproduk_post2db=="CREATE"){
			karyawan_id=jproduk_karyawanField.getValue();
		}else if(jproduk_post2db=="UPDATE"){
			karyawan_id=jproduk_karyawan_idField.getValue();
		}
		
		if(jproduk_karyawanField.getValue()!=''){
			karyawanDataStore.load({
					params : { karyawan_id: karyawan_id},
					callback: function(opts, success, response)  {
						 if (success) {
							if(karyawanDataStore.getCount()){
								jproduk_karyawan_record=karyawanDataStore.getAt(0).data;
								jproduk_nikkaryawanField.setValue(jproduk_karyawan_record.karyawan_no);
								jproduk_karyawan_jabatanField.setValue(jproduk_karyawan_record.karyawan_jabatan);
								//jproduk_karyawan_levelField.setValue(jproduk_karyawan_record.karyawan_idlevel);
							}
						}
					}
			}); 
		}
	}*/
	
	function is_master_jual_produk_form_valid(){
		//return (jproduk_diskonField.isValid() && jproduk_karyawanField.isValid() );
	}
  
	function display_form_window(){
		master_jual_produk_reset_form();
		detail_jual_produk_DataStore.load({params: {master_id:-1}});
		
		cbo_cust_jual_produk_DataStore.load({
		params: {cust_id	: 10},
			callback: function(opts, success, response)  {
					if (success) {
					  if(cbo_cust_jual_produk_DataStore.getCount()!=0){
						  var cust_nama = null;
						  cust_record=cbo_cust_jual_produk_DataStore.getAt(0).data;
						  jproduk_custField.setValue(cust_record.cust_nama);
					  }
					}
				}
		});
		
		//jproduk_caraField.setValue("card");
		jproduk_caraField.setValue("tunai");
		//master_jual_produk_cardGroup.setVisible(true);
		master_jual_produk_tunaiGroup.setVisible(true);
		jproduk_card_promoField.setValue("Normal");
		master_cara_bayarTabPanel.setActiveTab(0);
		jproduk_post2db="CREATE";
		jproduk_diskonField.setValue(0);
		jproduk_cashbackField.setValue(0);
		jproduk_diskonField.allowBlank=true;
		jproduk_pesanLabel.setText('');
		jproduk_lunasLabel.setText('');
		jproduk_uangField.setValue(0);
		jproduk_uang_cfField.setValue(0);
		jproduk_cust_priorityLabel.setText('');
		master_jual_produk_createWindow.hide();
		
		appointment_custBaruGroup_reset();
		appointment_custBaruField.setValue(false);
		
		//Show, combobox customer dan hide field customer baru
		jproduk_custField.show();
		app_cust_namaBaruField.hide();
		
		//Enable checbox "Baru
		appointment_custBaruField.setDisabled(false)
	}
 
	function master_jual_produk_confirm_delete(){
		// only one master_jual_produk is selected here
		if(master_jual_produkListEditorGrid.selModel.getCount() == 1){
			Ext.MessageBox.confirm('Confirmation','Anda yakin untuk menghapus data ini?', master_jual_produk_delete);
		} else if(master_jual_produkListEditorGrid.selModel.getCount() > 1){
			Ext.MessageBox.confirm('Confirmation','Anda yakin untuk menghapus data ini?', master_jual_produk_delete);
		} else {
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Anda belum memilih data yang akan dihapus',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
		}
	}
  
	function master_jual_produk_confirm_update(){
		Ext.MessageBox.show({
			msg:   'Sedang memuat data, mohon tunggu...',
			progressText: 'proses...',
			width:300,
			wait:true
		});	
		
		//show combobox customer dan hide field customer baru
		jproduk_custField.show();
		app_cust_namaBaruField.hide();
		
		//Disable checbox "Baru""
		appointment_custBaruField.setValue(false);
		appointment_custBaruField.setDisabled(true);		
		appointment_custBaruGroup.hide();
		//appointment_statusWindow.hide();
		
		master_jual_produk_reset_form();
		jproduk_karyawanDataStore.load();
		/* only one record is selected here */
		if(master_jual_produkListEditorGrid.selModel.getCount() == 1) {
			cbo_dproduk_produkDataStore.load({
				params: {
					query: master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_id'),
					aktif: 'yesno'
				},
				callback: function(opts, success, response){
					cbo_dproduk_satuanDataStore.setBaseParam('produk_id', 0);
					cbo_dproduk_satuanDataStore.setBaseParam('query', master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_id'));
					cbo_dproduk_satuanDataStore.load({
						callback: function(opts, success, response){
							cbo_dproduk_reveralDataStore.load({
								callback: function(opts, success, response){
									detail_jual_produk_DataStore.load({
										params : {master_id : eval(get_jproduk_pk()), start:0, limit:50},
										callback: function(opts, success, response){
											if(success){
												master_jual_produk_set_form();
												master_jual_produk_set_updating();
												push_insert();
												arr_detail_produk_insert();
											}
										}
									});
								}
							});
						}
					});
				}
			});
			jproduk_post2db='UPDATE';
			master_cara_bayarTabPanel.setActiveTab(2);
			master_cara_bayarTabPanel.setActiveTab(1);
			master_cara_bayarTabPanel.setActiveTab(0);
			msg='updated';
			master_jual_produk_createWindow.hide();
		} else {
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Anda belum memilih data yang akan diedit',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
		}
	}
  
	function master_jual_produk_delete(btn){
		if(btn=='yes'){
			var selections = master_jual_produkListEditorGrid.selModel.getSelections();
			var prez = [];
			for(i = 0; i< master_jual_produkListEditorGrid.selModel.getCount(); i++){
				prez.push(selections[i].json.jproduk_id);
			}
			var encoded_array = Ext.encode(prez);
			Ext.Ajax.request({ 
				waitMsg: 'Mohon tunggu...',
				url: 'index.php?c=c_master_jual_produk&m=get_action', 
				params: { task: "DELETE", ids:  encoded_array }, 
				success: function(response){
					var result=eval(response.responseText);
					switch(result){
						case 1:  // Success : simply reload
							master_jual_produk_DataStore.reload();
							master_detail_jual_produk_DataStore.reload({params: {master_id: 0, start: 0, limit: jproduk_pageS}});
							break;
						default:
							Ext.MessageBox.show({
								title: 'Warning',
								msg: 'Could not delete the entire selection',
								buttons: Ext.MessageBox.OK,
								animEl: 'save',
								icon: Ext.MessageBox.WARNING
							});
							break;
					}
				},
				failure: function(response){
					var result=response.responseText;
					Ext.MessageBox.show({
					   title: 'Error',
					   msg: 'Could not connect to the database. retry later.',
					   buttons: Ext.MessageBox.OK,
					   animEl: 'database',
					   icon: Ext.MessageBox.ERROR
					});	
				}
			});
		}  
	}
	
	master_jual_produk_DataStore = new Ext.data.Store({
		id: 'master_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_action', 
			method: 'POST'
		}),
		baseParams:{task: "LIST"}, // parameter yang di $_POST ke Controller
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'jproduk_id'
		},[
			{name: 'jproduk_id', type: 'int', mapping: 'jproduk_id'}, 
			{name: 'jproduk_nobukti', type: 'string', mapping: 'jproduk_nobukti'}, 
			{name: 'jproduk_nobukti_2', type: 'string', mapping: 'jproduk_nobukti_2'}, 
			{name: 'jproduk_grooming', type: 'int', mapping: 'jproduk_grooming'},
			{name: 'jproduk_grooming_id', type: 'int', mapping: 'jproduk_grooming'},
			{name: 'karyawan_nama', type: 'string', mapping: 'karyawan_nama'},
			{name: 'karyawan_no', type: 'string', mapping: 'karyawan_no'},
			{name: 'jproduk_cust', type: 'string', mapping: 'cust_nama'}, 
			{name: 'jproduk_cust_edit', type: 'string', mapping: 'cust_nama_edit'}, 
			{name: 'jproduk_cust_note', type: 'string', mapping: 'cust_note'},
			{name: 'jproduk_cust_no', type: 'string', mapping: 'cust_no'}, 
			{name: 'jproduk_cust_member', type: 'string', mapping: 'cust_member'}, 
			{name: 'jproduk_cust_member_no', type: 'string', mapping: 'member_no'}, 
			{name: 'jproduk_cust_id', type: 'int', mapping: 'jproduk_cust'}, 
			{name: 'jproduk_tanggal', type: 'date', dateFormat: 'Y-m-d', mapping: 'jproduk_tanggal'}, 
			{name: 'jproduk_diskon', type: 'int', mapping: 'jproduk_diskon'}, 
			{name: 'jproduk_cashback', type: 'float', mapping: 'jproduk_cashback'},
			{name: 'jproduk_cara', type: 'string', mapping: 'jproduk_cara'}, 
			{name: 'jproduk_cara2', type: 'string', mapping: 'jproduk_cara2'}, 
			{name: 'jproduk_cara3', type: 'string', mapping: 'jproduk_cara3'}, 
			{name: 'jproduk_bayar', type: 'float', mapping: 'jproduk_bayar'}, 
			{name: 'jproduk_total', type: 'float', mapping: 'jproduk_totalbiaya'}, 
			{name: 'jproduk_keterangan', type: 'string', mapping: 'jproduk_keterangan'},
			{name: 'jproduk_ket_disk', type: 'string', mapping: 'jproduk_ket_disk'},
			{name: 'jproduk_stat_dok', type: 'string', mapping: 'jproduk_stat_dok'}, 			
			{name: 'jproduk_creator', type: 'string', mapping: 'jproduk_creator'}, 
			{name: 'jproduk_date_create', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'jproduk_date_create'}, 
			{name: 'jproduk_update', type: 'string', mapping: 'jproduk_update'}, 
			{name: 'jproduk_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'jproduk_date_update'}, 
			{name: 'jproduk_revised', type: 'int', mapping: 'jproduk_revised'},
			{name: 'jproduk_post', type: 'string', mapping: 'jproduk_post'}
		]),
		sortInfo:{field: 'jproduk_id', direction: "DESC"}
	});
	
	cbo_voucher_jual_produkDataStore = new Ext.data.Store({
		id: 'cbo_voucher_jual_produkDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_voucher_list', 
			method: 'POST'
		}),baseParams: {start:0, limit: 10},
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'voucher_nomor'
		},[
			{name: 'voucher_nomor', type: 'string', mapping: 'kvoucher_nomor'},
			{name: 'voucher_jenis', type: 'string', mapping: 'voucher_jenis'},
			{name: 'voucher_nama', type: 'string', mapping: 'voucher_nama'}, 
			{name: 'voucher_point', type: 'int', mapping: 'voucher_point'}, 
			{name: 'voucher_kadaluarsa', type: 'date', dateFormat: 'Y-m-d', mapping: 'voucher_kadaluarsa'}, 
			{name: 'voucher_cashback', type: 'float', mapping: 'voucher_cashback'}, 
			{name: 'voucher_mincash', type: 'float', mapping: 'voucher_mincash'}, 
			{name: 'voucher_diskon', type: 'int', mapping: 'voucher_diskon'}, 
			{name: 'voucher_promo', type: 'int', mapping: 'voucher_promo'}, 
			{name: 'voucher_allproduk', type: 'string', mapping: 'voucher_allproduk'}, 
			{name: 'voucher_allproduk', type: 'string', mapping: 'voucher_allproduk'}
		]),
		sortInfo:{field: 'voucher_nomor', direction: "ASC"}
	});
	
	cbo_cust_jual_produk_DataStore = new Ext.data.Store({
		id: 'cbo_cust_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_customer_list', 
			method: 'POST'
		}),
		baseParams:{start: 0, limit: 10 }, // parameter yang di $_POST ke Controller
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'cust_id'
		},[
			{name: 'cust_id', type: 'int', mapping: 'cust_id'},
			{name: 'cust_no', type: 'string', mapping: 'cust_no'},
			{name: 'cust_nama', type: 'string', mapping: 'cust_nama'},
			{name: 'cust_member_no', type: 'int', mapping: 'cust_member_no'},
			{name: 'cust_preward_total', type: 'float', mapping: 'cust_preward_total'},
			{name: 'cust_member_level', type: 'int', mapping: 'cust_member_level'},
			{name: 'cust_note', type: 'string', mapping: 'cust_note'},
			{name: 'cust_tgllahir', type: 'date', dateFormat: 'Y-m-d', mapping: 'cust_tgllahir'},
			{name: 'cust_owner', type: 'string', mapping: 'cust_owner'},			
			{name: 'cust_alamat', type: 'string', mapping: 'cust_alamat'},
			{name: 'cust_telprumah', type: 'string', mapping: 'cust_telprumah'}
		]),
		sortInfo:{field: 'cust_no', direction: "ASC"}
	});
	
	jproduk_karyawanDataStore = new Ext.data.Store({
		id: 'jproduk_karyawanDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_allkaryawan_list', 
			method: 'POST'
		}),baseParams: {start: 0, limit: 15 },
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total'
		},[
			{name: 'karyawan_display', type: 'string', mapping: 'karyawan_nama'},
			{name: 'karyawan_id', type: 'int', mapping: 'karyawan_id'},
			{name: 'karyawan_no', type: 'string', mapping: 'karyawan_no'},
			{name: 'karyawan_username', type: 'string', mapping: 'karyawan_username'},
			{name: 'karyawan_value', type: 'int', mapping: 'karyawan_id'}
		]),
		sortInfo:{field: 'karyawan_no', direction: "ASC"}
	});
	
	var karyawan_tpl = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
            '<span><b>{karyawan_display}</b> | {karyawan_no}</span>',
        '</div></tpl>'
    );
	
	cbo_kwitansi_jual_produk_DataStore = new Ext.data.Store({
		id: 'cbo_kwitansi_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_kwitansi_list', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'kwitansi_id'
		},[
			{name: 'ckwitansi_id', type: 'int', mapping: 'kwitansi_id'},
			{name: 'ckwitansi_no', type: 'string', mapping: 'kwitansi_no'},
			{name: 'ckwitansi_cust_no', type: 'string', mapping: 'cust_no'},
			{name: 'ckwitansi_cust_nama', type: 'string', mapping: 'cust_nama'},
			{name: 'ckwitansi_cust_alamat', type: 'string', mapping: 'cust_alamat'},
			{name: 'kwitansi_keterangan', type: 'string', mapping: 'kwitansi_keterangan'},
			{name: 'total_sisa', type: 'int', mapping: 'total_sisa'},
			{name: 'jenis', type: 'string', mapping: 'jenis'}
		]),
		sortInfo:{field: 'ckwitansi_no', direction: "ASC"}
	});
	
	kwitansi_jual_produk_DataStore = new Ext.data.Store({
		id: 'kwitansi_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_kwitansi_by_ref', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'jkwitansi_id'
		},[
			{name: 'jkwitansi_id', type: 'int', mapping: 'jkwitansi_id'},
			{name: 'kwitansi_no', type: 'string', mapping: 'kwitansi_no'},
			{name: 'jkwitansi_nilai', type: 'float', mapping: 'jkwitansi_nilai'},
			{name: 'kwitansi_sisa', type: 'float', mapping: 'kwitansi_sisa'},
			{name: 'cust_nama', type: 'string', mapping: 'cust_nama'},
			{name: 'kwitansi_id', type: 'int', mapping: 'kwitansi_id'}
		]),
		sortInfo:{field: 'jkwitansi_id', direction: "DESC"}
	});
	
	gift_voucher_jual_produk_DataStore = new Ext.data.Store({
		id: 'gift_voucher_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_gift_voucher_by_ref', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'jkwitansi_id'
		},[
			{name: 'jkwitansi_id', type: 'int', mapping: 'jkwitansi_id'},
			{name: 'kwitansi_no', type: 'string', mapping: 'kwitansi_no'},
			{name: 'jkwitansi_nilai', type: 'float', mapping: 'jkwitansi_nilai'},
			{name: 'cust_nama', type: 'string', mapping: 'cust_nama'},
			{name: 'kwitansi_sisa', type: 'float', mapping: 'kwitansi_sisa'},
			{name: 'kwitansi_id', type: 'int', mapping: 'kwitansi_id'}
		]),
		sortInfo:{field: 'jkwitansi_id', direction: "DESC"}
	});
	
	card_jual_produk_DataStore = new Ext.data.Store({
		id: 'card_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_card_by_ref', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'jcard_id'
		},[
			{name: 'jcard_id', type: 'int', mapping: 'jcard_id'}, 
			{name: 'jcard_no', type: 'string', mapping: 'jcard_no'},
			{name: 'jcard_nama', type: 'string', mapping: 'jcard_nama'},
			{name: 'jcard_edc', type: 'string', mapping: 'jcard_edc'},
			{name: 'jcard_promo', type: 'string', mapping: 'jcard_promo'},
			{name: 'jcard_nilai', type: 'float', mapping: 'jcard_nilai'}
		]),
		sortInfo:{field: 'jcard_id', direction: "DESC"}
	});
	
	cek_jual_produk_DataStore = new Ext.data.Store({
		id: 'cek_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_cek_by_ref', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'jcek_id'
		},[
			{name: 'jcek_id', type: 'int', mapping: 'jcek_id'}, 
			{name: 'jcek_nama', type: 'string', mapping: 'jcek_nama'},
			{name: 'jcek_no', type: 'string', mapping: 'jcek_no'},
			{name: 'jcek_valid', type: 'string', mapping: 'jcek_valid'}, 
			{name: 'jcek_bank', type: 'string', mapping: 'jcek_bank'},
			{name: 'jcek_nilai', type: 'double', mapping: 'jcek_nilai'}
		]),
		sortInfo:{field: 'jcek_id', direction: "DESC"}
	});
	
	transfer_jual_produk_DataStore = new Ext.data.Store({
		id: 'transfer_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_transfer_by_ref', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'jtransfer_id'
		},[
			{name: 'jtransfer_id', type: 'int', mapping: 'jtransfer_id'}, 
			{name: 'jtransfer_bank', type: 'int', mapping: 'jtransfer_bank'},
			{name: 'jtransfer_nama', type: 'string', mapping: 'jtransfer_nama'},
			{name: 'jtransfer_nilai', type: 'float', mapping: 'jtransfer_nilai'}
		]),
		sortInfo:{field: 'jtransfer_id', direction: "DESC"}
	});
	
	tunai_jual_produk_DataStore = new Ext.data.Store({
		id: 'tunai_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_tunai_by_ref', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'jtunai_id'
		},[
			{name: 'jtunai_id', type: 'int', mapping: 'jtunai_id'}, 
			{name: 'jtunai_nilai', type: 'float', mapping: 'jtunai_nilai'}
		]),
		sortInfo:{field: 'jtunai_id', direction: "DESC"}
	});
	
	jproduk_bankDataStore = new Ext.data.Store({
		id:'jproduk_bankDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_bank_list', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'mbank_id'
		},[
			{name: 'jproduk_bank_value', type: 'int', mapping: 'mbank_id'}, 
			{name: 'jproduk_bank_display', type: 'string', mapping: 'mbank_nama'}
		]),
		sortInfo:{field: 'jproduk_bank_display', direction: "DESC"}
		});
	
	voucher_jual_produk_DataStore = new Ext.data.Store({
		id: 'voucher_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_voucher_by_ref', 
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'tvoucher_id'
		},[
			{name: 'tvoucher_id', type: 'int', mapping: 'tvoucher_id'}, 
			{name: 'tvoucher_novoucher', type: 'string', mapping: 'tvoucher_novoucher'}, 
			{name: 'tvoucher_nilai', type: 'float', mapping: 'tvoucher_nilai'}
		]),
		sortInfo:{field: 'tvoucher_id', direction: "DESC"}
	});
	
	jproduk_diskon_promoDataStore = new Ext.data.Store({
		id: 'jproduk_diskon_promoDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_promo_onerow',
			method: 'POST'
		}),
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'tvoucher_id'
		},[
			{name: 'tvoucher_id', type: 'int', mapping: 'tvoucher_id'}, 
			{name: 'tvoucher_novoucher', type: 'string', mapping: 'tvoucher_novoucher'}, 
			{name: 'tvoucher_nilai', type: 'float', mapping: 'tvoucher_nilai'}
		]),
		sortInfo:{field: 'tvoucher_id', direction: "DESC"}
	});
       
	master_jual_produk_ColumnModel = new Ext.grid.ColumnModel(
		[{
			header: '#',
			readOnly: true,
			dataIndex: 'jproduk_id',
			width: 5,
			renderer: function(value, cell){
				cell.css = "readonlycell"; // Mengambil Value dari Class di dalam CSS 
				return value;
				},
			hidden: true
		},
		{
			header: '<div align="center">' + 'Tanggal' + '</div>',
			dataIndex: 'jproduk_tanggal',
			width: 80,	//150,
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y')/*,
			editor: new Ext.form.DateField({
				format: 'd-m-Y'
			})*/
		}, 
		{
			header: '<div align="center">' + 'No Faktur' + '</div>',
			dataIndex: 'jproduk_nobukti',
			width: 120,	//150,
			sortable: true,
			/*editor: new Ext.form.TextField({
				maxLength: 30
			}),*/
			renderer: function(value, cell, record){
				if(record.data.jproduk_post=='Y'){
					return '<span style="color:red;"><b>' + value + '</b></span>';
				}
				return value;
			}
		}, 		
		{
			header: '<div align="center">' + 'No Cust' + '</div>',
			dataIndex: 'jproduk_cust_no',
			width: 80,	//185,
			sortable: true,
			readOnly: true
		},
	/*	{
			header: '<div align="center">' + 'Catatan Customer' + '</div>',
			dataIndex: 'jproduk_cust_note',
			width: 50,	//185,
			readOnly: true,
			hidden: true
		},*/		
		{
			header: '<div align="center">' + 'Customer' + '</div>',
			dataIndex: 'jproduk_cust',
			width: 120,	//185,
			sortable: true,
			readOnly: true,
			renderer: function(value, cell, record){
				if(record.data.jproduk_cust_note=='Y'){
					return '<span style="color:black;"><b>' + value + '</b></span>';
				}
				return value;
			}
		}, 
		{
			header: '<div align="center">' + 'No Member' + '</div>',
			dataIndex: 'jproduk_cust_member_no',
			width: 100,	//185,
			sortable: true,
			readOnly: true
		},
		{
			header: '<div align="center">' + 'Total (Rp)' + '</div>',
			align: 'right',
			dataIndex: 'jproduk_total',
			width: 90,	//150,
			sortable: true,
			readOnly: true,
			renderer: function(val){
				return '<span>'+Ext.util.Format.number(val,'0,000')+'</span>';
			}
		}, 
		{
			header: '<div align="center">' + 'Pembayaran 1' + '</div>',
			dataIndex: 'jproduk_cara',
			width: 70,	//150,
			sortable: true
		}, 
		/*{
			header: '<div align="center">' + 'Bayar 1 (Rp)' + '</div>',
			align: 'right',
			dataIndex: 'jproduk_total',
			width: 80,	//150,
			sortable: true,
			readOnly: true,
			renderer: function(val){
				return '<span>'+Ext.util.Format.number(val,'0,000')+'</span>';
			}
		},*/
		{
			header: '<div align="center">' + 'Pembayaran 2' + '</div>',
			dataIndex: 'jproduk_cara2',
			width: 60,	//150,
			hidden:true,
			sortable: true
		}, 
		/*{
			header: '<div align="center">' + 'Bayar 2 (Rp)' + '</div>',
			align: 'right',
			dataIndex: 'jproduk_total',
			width: 80,	//150,
			sortable: true,
			readOnly: true,
			renderer: function(val){
				return '<span>'+Ext.util.Format.number(val,'0,000')+'</span>';
			}
		},*/
		{
			header: '<div align="center">' + 'Tot Bayar (Rp)' + '</div>',
			align: 'right',
			dataIndex: 'jproduk_bayar',
			width: 90,	//150,
			sortable: true,
			readOnly: true,
			renderer: function(val){
				return '<span>'+Ext.util.Format.number(val,'0,000')+'</span>';
			}
		},
		{
			header: '<div align="center">' + 'Keterangan' + '</div>',
			dataIndex: 'jproduk_keterangan',
			width: 150,	//150,
			sortable: true/*,
			editor: new Ext.form.TextField({
				maxLength: 250
			})*/
		}, 
		{
			header: '<div align="center">' + 'Stat Dok' + '</div>',
			dataIndex: 'jproduk_stat_dok',
			width: 60,	//150,
			sortable: true
		},
		{
			header: 'Creator',
			dataIndex: 'jproduk_creator',
			width: 150,
			sortable: true,
			hidden: true,
			readOnly: true
		}, 
		{
			header: 'Date Create',
			dataIndex: 'jproduk_date_create',
			width: 150,
			sortable: true,
			hidden: true,
			readOnly: true
		}, 
		{
			header: 'Update',
			dataIndex: 'jproduk_update',
			width: 150,
			sortable: true,
			hidden: true,
			readOnly: true
		}, 
		{
			header: 'Date Update',
			dataIndex: 'jproduk_date_update',
			width: 150,
			sortable: true,
			hidden: true,
			readOnly: true
		}, 
		{
			header: 'Revised',
			dataIndex: 'jproduk_revised',
			width: 150,
			sortable: true,
			hidden: true,
			readOnly: true
		}, 
		{
			header: 'Posting',
			dataIndex: 'jproduk_post',
			width: 50,
			sortable: true,
			hidden: true,
			readOnly: true
		}]
	);
	
	master_jual_produk_ColumnModel.defaultSortable= true;
    
	master_jual_produkListEditorGrid =  new Ext.grid.GridPanel({
		id: 'master_jual_produkListEditorGrid',
		el: 'fp_master_jual_produk',
		title: 'Daftar Penjualan Produk',
		autoHeight: true,
		store: master_jual_produk_DataStore, // DataStore
		cm: master_jual_produk_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		frame: true,
		trackMouseOver: false,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		viewConfig: { forceFit:true },
	  	width: 940, //1220,	//800,
		bbar: new Ext.PagingToolbar({
			pageSize: jproduk_pageS,
			store: master_jual_produk_DataStore,
			displayInfo: true
		}),		
		/* Tambah Control on ToolBar */
		tbar: [
		{
			text: 'Tambah (F1)',
			tooltip: 'Tambah data baru',
			id : 'Add_detail',
			iconCls:'icon-adds',    				// this is defined in our styles.css
			handler: display_form_window
		}, '-',{
			text: 'Ubah (F2)',
			tooltip: 'Ubah data terpilih',
			iconCls:'icon-update',
			handler: master_jual_produk_confirm_update   // Confirm before updating
		}, '-',/*{
			text: 'Hapus',
			tooltip: 'Hapus data terpilih',
			iconCls:'icon-delete',
			disabled: true,
			handler: master_jual_produk_confirm_delete   // Confirm before deleting
		}, '-', */{
			text: 'Pencarian',
			tooltip: 'Advanced Search',
			iconCls:'icon-search',
			handler: display_form_search_window 
		}, '-', 
			new Ext.app.SearchField({
			store: master_jual_produk_DataStore,
			params: {task: 'LIST',start: 0, limit: jproduk_pageS},
			listeners:{
				specialkey: function(f,e){
					if(e.getKey() == e.ENTER){
						master_jual_produk_DataStore.baseParams={task:'LIST',start: 0, limit: jproduk_pageS};
		            }
				},
				render: function(c){
				Ext.get(this.id).set({qtitle:'Search By'});
				Ext.get(this.id).set({qtip:'- Client Card<br>- Nama Cust<br>- No Faktur'});
				}
			},
			width: 120
		}),'-',{
			text: 'Refresh',
			tooltip: 'Refresh datagrid',
			handler: master_jual_produk_reset_search,
			iconCls:'icon-refresh'
		},'-',{
			text: 'Export Excel',
			tooltip: 'Export to Excel(.xls) Document',
			iconCls:'icon-xls',
			menu: {
				xtype: 'menu',
				plain: true,
				items: [{
					text: 'Penjualan Produk',
					tooltip: 'Export to Excel Penjualan Produk',
					iconCls:'icon-xls',
					handler: function(){
						master_jual_produk_export_excel('biasa');
					}
				}
				]
			}
		}, '-',{
			text: 'Print',
			tooltip: 'Print Document',
			iconCls:'icon-print',
			handler: master_jual_produk_print  
		}
		]
	});
	
	master_jual_produkListEditorGrid.on('rowclick', function (master_jual_produkListEditorGrid, rowIndex, eventObj) {
	    var recordMaster = master_jual_produkListEditorGrid.getSelectionModel().getSelected();
	    master_detail_jual_produk_DataStore.load({params : {master_id : recordMaster.get("jproduk_id"), start:0, limit:50} });
	});
	
	master_detail_jual_produk_DataStore = new Ext.data.Store({
		id: 'detail_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=detail_detail_jual_produk_list', 
			method: 'POST'
		}),baseParams: {master_id: 0, start: 0, limit: jproduk_pageS},
		reader: new Ext.data.JsonReader({
			    root: 'results',
			    totalProperty: 'total',
			    id: ''
			},[
				{name: 'dproduk_id', type: 'int', mapping: 'dproduk_id'}, 
				{name: 'dproduk_master', type: 'int', mapping: 'dproduk_master'}, 
				{name: 'dproduk_produk', type: 'int', mapping: 'dproduk_produk'}, 
				{name: 'produk_nama', type: 'string', mapping: 'produk_nama'},
				{name: 'dproduk_satuan', type: 'int', mapping: 'dproduk_satuan'}, 
				{name: 'satuan_nama', type: 'string', mapping: 'satuan_nama'},
				{name: 'dproduk_jumlah', type: 'int', mapping: 'dproduk_jumlah'}, 
				{name: 'dproduk_harga', type: 'float', mapping: 'dproduk_harga'}, 
				{name: 'dproduk_diskon', type: 'float', mapping: 'dproduk_diskon'},
				{name: 'dproduk_sales', type: 'string', mapping: 'dproduk_sales'},
				{name: 'dproduk_keterangan', type: 'string', mapping: 'dproduk_keterangan'},
				{name: 'dproduk_diskon_jenis', type: 'string', mapping: 'dproduk_diskon_jenis'},
				{name: 'nama_karyawan', type: 'string', mapping: 'karyawan_username'},
				{name: 'dproduk_karyawan', type: 'int', mapping: 'dproduk_karyawan'},
				{name: 'karyawan_username', type: 'string', mapping: 'karyawan_username'},
				{name: 'dproduk_subtotal', type: 'float', mapping: 'dproduk_subtotal'},
				{name: 'dproduk_subtotal_net', type: 'float', mapping: 'dproduk_subtotal_net'},
				{name: 'jproduk_bayar', type: 'float', mapping: 'jproduk_bayar'},
				{name: 'jproduk_diskon', type: 'int', mapping: 'jproduk_diskon'},
				{name: 'jproduk_cashback', type: 'float', mapping: 'jproduk_cashback'},
				{name: 'produk_harga_default', type: 'float', mapping: 'produk_harga_default'},
				{name: 'produk_kode', type: 'string', mapping: 'produk_kode'}
			]),
		sortInfo:{field: 'dproduk_id', direction: "ASC"}
	});

	master_detail_jual_produk_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'Left',
			header: 'ID',
			dataIndex: 'dproduk_id',
			hidden: true
		},
		{
			align : 'Left',
			header: '<div align="center">' + 'Produk' + '</div>',
			dataIndex: 'produk_nama',
			width: 280,
			sortable: false
		},
		{
			align :'Left',
			header: '<div align="center">' + 'Satuan' + '</div>',
			dataIndex: 'satuan_nama',
			width: 60,
			sortable: false
		},
		{
			align : 'Right',
			header: '<div align="center">' + 'Jml' + '</div>',
			dataIndex: 'dproduk_jumlah',
			width: 40,
			sortable: false
		},
		{
			align : 'Right',
			header: '<div align="center">' + 'Harga' + '</div>',
			dataIndex: 'dproduk_harga',
			width: 80,
			sortable: false
		}, 		
		/*{
			align: 'Right',
			header: '<div align="center">' + 'DPP (Rp)' + '</div>',
			dataIndex: 'dproduk_harga',
			width: 80,
			sortable: true,
			renderer: function(v, params, record){
				var record_dpp = record.data.dproduk_harga/1.1;
				record_dpp = (record_dpp>0?Math.round(record_dpp):0);
				return Ext.util.Format.number(record_dpp,'0,000');
			}
		},*/
		{
			align : 'Right',
			header: '<div align="center">' + 'Sub Total' + '</div>',
			dataIndex: 'dproduk_subtotal',
			width: 100,
			sortable: false,
			renderer: function(v, params, record){
				return Ext.util.Format.number(record.data.dproduk_jumlah*record.data.dproduk_harga,'0,000');
			}
		},
		{
			align : 'Left',
			header: '<div align="center">' + 'Jns Disk' + '</div>',
			dataIndex: 'dproduk_diskon_jenis',
			width: 80,
			sortable: false
		},
		{
			align : 'Right',
			header: '<div align="center">' + 'Disk (%)' + '</div>',
			dataIndex: 'dproduk_diskon',
			width: 60,
			sortable: false
		},
		{
			align :'Right',
			header: '<div align="center">' + 'Total' + '</div>',
			dataIndex: 'dproduk_subtotal_net',
			width: 120,
			sortable: false,
			renderer: function(v, params, record){
				var record_dtotal_net = record.data.dproduk_jumlah*record.data.dproduk_harga*((100-record.data.dproduk_diskon)/100);
				record_dtotal_net = (record_dtotal_net>0?Math.round(record_dtotal_net):0);
				return Ext.util.Format.number(record_dtotal_net,'0,000');
			}
		},
		/*{
			align: 'Right',
			header: '<div align="center">' + 'Sub Tot Net DPP (Rp)' + '</div>',
			dataIndex: 'dproduk_subtotal_net',
			width: 120,
			sortable: true,
			renderer: function(v, params, record){
				var record_subtot_dpp = (record.data.dproduk_jumlah*record.data.dproduk_harga*((100-record.data.dproduk_diskon)/100))/1.1;
				record_subtot_dpp = (record_subtot_dpp>0?Math.round(record_subtot_dpp):0);
				return Ext.util.Format.number(record_subtot_dpp,'0,000');
			}
		},
		{
			header: '<div align="center">' + 'Keterangan' + '</div>',
			dataIndex: 'dproduk_keterangan',
			width: 150,
			sortable: false
		},
		{
			align : 'Left',
			header: '<div align="center">' + 'Referal' + '</div>',
			dataIndex: 'karyawan_username',
			width: 160,
			sortable: false
		},*/
		{
			//field ini HARUS dimunculkan, utk penghitungan harga
			align : 'Right',
			//header: '<div align="center" style="color:white;">' + 'Harga Default' + '</div>',
			dataIndex: 'produk_harga_default',
			width: 0,
			sortable: false,
			hidden: false,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}
		]
	);
	
	master_detail_jual_produk_ColumnModel.defaultSortable= true;
	
	master_detail_jual_produkListEditorGrid =  new Ext.grid.GridPanel({
		id: 'master_detail_jual_produkListEditorGrid',
		el: 'fp_master_detail_jual_produk',
		title: 'Detail Penjualan Produk',
		autoHeight: true,
		width: 918,
		autoScroll: true,
		store: master_detail_jual_produk_DataStore, // DataStore
		cm: master_detail_jual_produk_ColumnModel, // Nama-nama Columns
		frame: true	  	
	});
	
	function load_catatan_customer1(){
		if(master_jual_produkListEditorGrid.selModel.getCount() == 1){	
			
			var cust_id2 = master_jual_produkListEditorGrid.getSelectionModel().getSelected().get('jproduk_cust_id');
			
			customer_catatan_DataStore.load({
				params : {note_customer : cust_id2},
				callback: function(opts, success, response)  {
					if (success) {
						if(customer_catatan_DataStore.getAt(0).data.note_all!=''){							
							Ext.MessageBox.show({
								   title: 'Catatan Customer',
								   msg: customer_catatan_DataStore.getAt(0).data.note_all,
								   buttons: Ext.MessageBox.OK,
								   animEl: 'save'
								   //icon: Ext.MessageBox.OK
								});							
						}
					}
				}
			}); 
		} 
		else {
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Tidak ada customer yang dipilih',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
		}				
	}
	 
	master_jual_produk_ContextMenu = new Ext.menu.Menu({
		id: 'master_jual_produk_ListEditorGridContextMenu',
		items: [
		{ 
			text: 'Ubah', tooltip: 'Ubah data terpilih', 
			iconCls:'icon-update',
			handler: master_jual_produk_editContextMenu 
		},
		{ 
			text: 'Print',
			tooltip: 'Print Document',
			iconCls:'icon-print',
			handler: master_jual_produk_print 
		},
		{ 
			text: 'Export Excel', 
			tooltip: 'Export to Excel(.xls) Document',
			iconCls:'icon-xls',
			handler: function(){
						master_jual_produk_export_excel('biasa');
					}
		}
		]
	}); 
	
	function onmaster_jual_produk_ListEditGridContextMenu(grid, rowIndex, e) {
		e.stopEvent();
		var coords = e.getXY();
		master_jual_produk_ContextMenu.rowRecord = grid.store.getAt(rowIndex);
		grid.selModel.selectRow(rowIndex);
		master_jual_produk_SelectedRow=rowIndex;
		master_jual_produk_ContextMenu.showAt([coords[0], coords[1]]);
  	}
	
	function master_jual_produk_editContextMenu(){
		//master_jual_produkListEditorGrid.startEditing(master_jual_produk_SelectedRow,1);
		master_jual_produk_confirm_update();
  	}
  	
	master_jual_produkListEditorGrid.addListener('rowcontextmenu', onmaster_jual_produk_ListEditGridContextMenu);
	
    var customer_jual_produk_tpl = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
            '<span><b>({cust_member_no}) {cust_nama}</b><br /></span>',
			'Tgl Lahir: {cust_tgllahir:date("j M Y")}<br/>',
            'Alamat: {cust_alamat}<br/>',
			'Telp Rumah: {cust_telprumah}<br>',			
        '</div></tpl>'
    );
	var voucher_jual_produk_tpl = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
            '<span><b>{voucher_nomor}</b>| {voucher_nama}<br/></span>',
			'Jenis: {voucher_jenis}&nbsp;&nbsp;&nbsp;[Nilai: {voucher_cashback}]',
		'</div></tpl>'
    );
	var kwitansi_jual_produk_tpl = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
            '<span><b>{ckwitansi_no}</b> <br/>',
			'a/n {ckwitansi_cust_nama} [ {ckwitansi_cust_no} ]<br/>',
			'{ckwitansi_cust_alamat}, <br>Sisa: <b>Rp. {total_sisa}</b>, <br>Ket : {kwitansi_keterangan} </span>',
		'</div></tpl>'
    );
	var produk_jual_produk_tpl = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
            '<span>({dproduk_produk_kode})<br><b>{dproduk_produk_display}</b>',
		'</div></tpl>'
    );
		
	jproduk_idField= new Ext.form.NumberField({
		id: 'jproduk_idField',
		allowNegatife : false,
		blankText: '0',
		allowBlank: false,
		allowDecimals: false,
		hidden: true,
		readOnly: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	jproduk_nobuktiField= new Ext.form.TextField({
		id: 'jproduk_nobuktiField',
		fieldLabel: 'No Faktur',
		emptyText : '(Auto)',
		readOnly:true,
		disabled : true,
		maxLength: 30
	});
	jproduk_custField= new Ext.form.ComboBox({
		id: 'jproduk_custField',
		fieldLabel: 'Customer',
		store: cbo_cust_jual_produk_DataStore,
		mode: 'remote',
		displayField:'cust_nama',
		valueField: 'cust_id',
		forceSelection: true,
        typeAhead: false,
        loadingText: 'Searching...',
        pageSize:10,
        hideTrigger:false,
        tpl: customer_jual_produk_tpl,
        itemSelector: 'div.search-item',
		triggerAction: 'all',
		lazyRender:true,
		listClass: 'x-combo-list-small',
		anchor: '95%'
	});
	
	/*Untuk ngecek value, apakah nilai nya * , jika iya, maka akan menampilkan / mengaktifkan label Priortiy */
	jproduk_cust_priorityField= new Ext.form.TextField({
		id: 'jproduk_cust_priorityField',
		readOnly: true
	});
	
	// Label ini jika aktif, akan menampilkan bintang (*) besar , dimana tujuan label ini adlh menunjukkan bahwa Customer tersebut adalah Customer High Priority
	jproduk_cust_priorityLabel= new Ext.form.Label({
		style: {
			marginLeft: '100px',
			fontSize: '35px',
			//color: '#006600',
			color: '#CC0000'
		}
	});
	
	jproduk_cust_nomemberLabel=new Ext.form.Label({ html: 'No Member: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'});
	
	jproduk_cust_pointLabel=new Ext.form.Label({ html: '&nbsp;&nbsp; Total Point: &nbsp;&nbsp;Rp&nbsp;'});
	/*
	jproduk_cust_nomemberField= new Ext.form.TextField({
		id: 'jproduk_cust_nomemberField',
		//fieldLabel: 'No Member',
		emptyText : '(Auto)',
		disabled : true,
		readOnly: true,
                value: ""
	});	
	*/
	jproduk_cust_nomemberField= new Ext.form.Label({
		id: 'jproduk_cust_nomemberField',
		autoHeight: true,
		style: {fontWeight:'bold',margin:'2px 0px'},
		value: ""
	});	
	
	jproduk_cust_pointField= new Ext.form.Label({
		id: 'jproduk_cust_pointField',
		autoHeight: true,
		style: {fontWeight:'bold',margin:'2px 0px'},
		value: ""
	});	
	
	app_cust_namaBaruField=new Ext.form.TextField({
		id: 'app_cust_namaBaruField',
		fieldLabel: 'Nama',
		maxLength: 30,
		enableKeyEvents: true,
		hidden: true,
		anchor: '90%'
	});

	member_no_baruField=new Ext.form.TextField({
		id: 'member_no_baruField',
		fieldLabel: 'No Member',
		//allowBlank : false,
		maxLength: 30,
		anchor: '90%',
		maskRe: /([0-9]+)$/
	});

	app_cust_hpBaruField=new Ext.form.TextField({
		id: 'app_cust_hpBaruField',
		fieldLabel: 'HP',
		maxLength: 30,
		//allowBlank : false,
		anchor: '90%',
		maskRe: /([0-9]+)$/
	});
	
	member_ultah_baruField= new Ext.form.DateField({
		id: 'member_ultah_baruField',
		fieldLabel: 'Tanggal Ultah',
		anchor: '90%',
		format : 'd-m-Y'
	});

	app_cust_keteranganBaruField= new Ext.form.TextArea({
		id: 'app_cust_keteranganBaruField',
		fieldLabel: 'Keterangan',
		maxLength: 250,
		anchor: '95%'
	});

	appointment_custBaruField=new Ext.form.Checkbox({
		id: 'check_baru',
		boxLabel: 'Baru',
		handler: function(node,checked){
			if (checked) {
				appointment_custBaruGroup.show();
				jproduk_custField.hide();
				jproduk_cust_nomemberLabel.hide();
				jproduk_cust_nomemberField.hide();
				jproduk_cust_pointLabel.hide();
				jproduk_cust_pointField.hide();
				jproduk_cust_ultahLabel.hide(); 
				jproduk_cust_ultahField.hide();
				app_cust_namaBaruField.show();
				
				appointment_custBaruGroup_reset();
				jproduk_custField.reset();
				jproduk_custField.setValue(null);
				jproduk_cust_ultahField.setValue(null);
				jproduk_cust_nomemberField.setText('');							
				jproduk_cust_pointField.setText('');							
			}else{
				app_cust_namaBaruField.hide();
				appointment_custBaruGroup.hide();
				jproduk_custField.show();
				jproduk_cust_nomemberLabel.show();
				jproduk_cust_nomemberField.show();
				jproduk_cust_pointLabel.show();
				jproduk_cust_pointField.show();
				jproduk_cust_ultahLabel.show(); 
				jproduk_cust_ultahField.show();
				
				cbo_cust_jual_produk_DataStore.load({
				params: {cust_id	: 10},
					callback: function(opts, success, response)  {
							if (success) {
							  if(cbo_cust_jual_produk_DataStore.getCount()!=0){
								  var cust_nama = null;
								  cust_record=cbo_cust_jual_produk_DataStore.getAt(0).data;
								  jproduk_custField.setValue(cust_record.cust_nama);
							  }
							}
						}
				});
				
				jproduk_cust_ultahField.setValue(null);
				jproduk_cust_nomemberField.setText('');			
				jproduk_cust_pointField.setText('');			
			}
		}
	});
	
	appointment_custBaruGroup = new Ext.form.FieldSet({
		title: 'Detail Member Baru',
		//checkboxToggle:true,
		autoHeight: true,
		layout:'column',
		hidden: true,
		//enableKeyEvents: true,
		collapsed: false,
		anchor: '95%',
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [app_cust_namaBaruField, member_no_baruField, app_cust_hpBaruField, member_ultah_baruField] 
			}/*,
			{
				columnWidth:0.5,
				layout: 'form',
				border:false,
				items: [app_cust_hpBaruField] 
			} */
		]
	
	});
	
	jproduk_ownerField= new Ext.form.TextField({
		id: 'jproduk_ownerField',
		//fieldLabel: 'No Member',
		emptyText : '(Auto)',
		disabled : true,
		readOnly: true,
		style: {margin:'2px 0px'},
        value: ""
	});
	
	jproduk_tahun_ambil_disk_ultah= new Ext.form.TextField({
		id: 'jproduk_tahun_ambil_disk_ultah',
		readOnly: true,
		disabled : true,
        value: ""
	});
	
	jproduk_tgl_ambil_disk_ultah= new Ext.form.TextField({
		id: 'jproduk_tgl_ambil_disk_ultah',
		readOnly: true,
		disabled : true,
        value: ""
	});
	
	jproduk_valid_memberLabel=new Ext.form.Label({ hidden: true, html: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Member Valid: &nbsp;&nbsp;'});	
	/*
	jproduk_valid_memberField= new Ext.form.DateField({
		id: 'jproduk_valid_memberField',
		//fieldLabel: 'Member Valid',
		emptyText : '(Auto)',
		hidden: true,
		readOnly: true,
		disabled : true,
		format : 'd-m-Y'
	});
	*/
	jproduk_cust_ultahLabel=new Ext.form.Label({ html: 'Tanggal Ultah : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'});
	
	jproduk_cust_ultahField= new Ext.form.DateField({
		id: 'jproduk_cust_ultahField',
		fieldLabel: 'Tanggal Ultah',
		emptyText : '(Auto)',
		readOnly: true,
		disabled : true,
		format : 'd-m-Y'
	});
	
	/*jproduk_cust_noteLabel=new Ext.form.Label({ html: 'Catatan Customer:<br>'});
	
	jproduk_cust_noteField= new Ext.form.TextArea({
		id: 'jproduk_cust_noteField',
		readOnly: true,
		disabled: true,
		style: {
		    marginLeft: '-105px', 
		    width: '250px', height: '65px'
		}
		
	});*/
	
	jproduk_cust_noteField= new Ext.form.TextArea({
		id: 'jproduk_cust_noteField',
		fieldLabel: 'Catatan Customer',
		readOnly: true,
		disabled: true,
		maxLength: 250,
		anchor: '80%'
	});
	
	jproduk_karyawanField= new Ext.form.ComboBox({
		id: 'jproduk_karyawanField',
		fieldLabel: 'Karyawan',
		store: jproduk_karyawanDataStore,
		mode: 'remote',
		displayField:'karyawan_display',
		valueField: 'karyawan_value',
        typeAhead: false,
        loadingText: 'Searching...',
        pageSize:10,
		allowBlank : false,
        hideTrigger:false,
        tpl: karyawan_tpl,
        itemSelector: 'div.search-item',
		triggerAction: 'all',
		lazyRender:true,
		listClass: 'x-combo-list-small',
		width: 340
	});
	
	jproduk_nikkaryawanField= new Ext.form.TextField({
		id: 'jproduk_nikkaryawanField',
		fieldLabel: 'NIK',
		emptyText : '(Auto)',
		disabled: true,
		renderer: function(value, cell, record){
				return value.substring(0,6) + '-' + value.substring(6,12) + '-' + value.substring(12);
			}
	});
	
	jproduk_tanggalField= new Ext.form.DateField({
		id: 'jproduk_tanggalField',
		fieldLabel: 'Tanggal',
		format : 'd-m-Y',
		maxValue: today
	});
	
	jproduk_diskonField= new Ext.form.NumberField({
		id: 'jproduk_diskonField',
		fieldLabel: 'Disk Tambahan (%)',
		allowNegatife : false,
		blankText: '0',
		emptyText: '0',
		allowDecimals: true,
		enableKeyEvents: true,
		width: 120,
		maxLength: 5,
		maskRe: /([0-9]+)$/
	});
	
	jproduk_ket_diskField= new Ext.form.TextField({
		id: 'jproduk_ket_disk',
		fieldLabel: 'No Voucher',
		//itemCls : 'rata_kanan',
		width: 120
	});

	jproduk_cashback_cfField= new Ext.form.TextField({
		id: 'jproduk_cashback_cfField',
		fieldLabel: 'Potongan',
		allowNegatife : false,
		enableKeyEvents: true,
		//itemCls: 'rata_kanan',
		width: 90,
		maskRe: /([0-9]+)$/
	});
	jproduk_cashbackField= new Ext.form.NumberField({
		id: 'jproduk_cashbackField',
		fieldLabel: 'Diskon (Rp)',
		allowNegatife : false,
		blankText: '0',
		emptyText: '0',
		enableKeyEvents: true,
		readOnly : true,
		allowDecimals: false,
		width: 120,
		maskRe: /([0-9]+)$/
	});
	
	jproduk_uang_cfField= new Ext.form.TextField({
		id: 'jproduk_uang_cfField',
		fieldLabel: '<span style="font-weight:bold; color:#006600"><font size=2>Jumlah Uang</span>',
		allowNegatife : false,
		enableKeyEvents: true,
		//itemCls: 'rata_kanan',
		width: 90,
		maskRe: /([0-9]+)$/
	});
	jproduk_uangField= new Ext.form.NumberField({
		id: 'jproduk_uangField',
		fieldLabel: '<span style="font-weight:bold; color:#006600"><font size=2>Jumlah Uang</span>',
		allowNegatife : false,
		blankText: '0',
		emptyText: '0',
		enableKeyEvents: true,
		readOnly : true,
		allowDecimals: false,
		width: 90,
		maskRe: /([0-9]+)$/
	});
	
	jproduk_potong_pointcfField= new Ext.form.TextField({
		id: 'jproduk_potong_pointcfField',
		fieldLabel: '<span style="font-weight:bold; color:#006600"><font size=2>Potong Point</span>',
		allowNegatife : false,
		enableKeyEvents: true,
		//itemCls: 'rata_kanan',
		width: 90,
		maskRe: /([0-9]+)$/
	});
	jproduk_potong_pointField= new Ext.form.NumberField({
		id: 'jproduk_potong_pointField',
		fieldLabel: '<span style="font-weight:bold; color:#006600"><font size=2>Potong Point</span>',
		allowNegatife : false,
		blankText: '0',
		emptyText: '0',
		enableKeyEvents: true,
		readOnly : true,
		allowDecimals: false,
		width: 90,
		maskRe: /([0-9]+)$/
	});
	
	kembalian_label= new Ext.form.DisplayField({
		fieldLabel : '<span style="font-weight:bold; color:#006600"><font size=2>Kembalian</span>',
		itemCls : 'rmoney5'
	});
	
	jproduk_uang_cfField.on("keyup",function(){
		var cf_value = jproduk_uang_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_uangField.setValue(cf_tonumber);
		
		var kembalian=cf_tonumber-jproduk_totalField.getValue();
		kembalian_label.setValue('<span style="font-weight:bold; color:#006600"><font size=4>'+CurrencyFormatted(kembalian)+'</span>');
		//jproduk_totalField.setValue(total_biaya_field);
		//jproduk_total_cfField.setValue(CurrencyFormatted(total_biaya_field));
		//load_total_biaya();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_uang_cfField.on("blur",function(){
		push_insert();
	});
	
	jproduk_caraField= new Ext.form.ComboBox({
		id: 'jproduk_caraField',
		fieldLabel: 'Cara Bayar',
		store:new Ext.data.SimpleStore({
			fields:['jproduk_cara_value', 'jproduk_cara_display'],
			data:[['tunai','Tunai'],['card','Kartu Kredit/Debit']/*,['kwitansi','Kuitansi/Gift Voucher'],['cek/giro','Cek/Giro'],['transfer','Transfer'],['vgrooming','Fasilitas Grooming']*/]
		}),
		mode: 'local',
		displayField: 'jproduk_cara_display',
		valueField: 'jproduk_cara_value',
		editable: false,
		//width: 100,
		anchor: '98%',
		triggerAction: 'all'	
	});
	
	jproduk_cara2Field= new Ext.form.ComboBox({
		id: 'jproduk_cara2Field',
		fieldLabel: 'Cara Bayar 2',
		store:new Ext.data.SimpleStore({
			fields:['jproduk_cara_value', 'jproduk_cara_display'],
			data:[['tunai','Tunai'],['card','Kartu Kredit/Debit']/*,['kwitansi','Kuitansi/Gift Voucher'],['cek/giro','Cek/Giro'],['transfer','Transfer']*/]
		}),
		mode: 'local',
		displayField: 'jproduk_cara_display',
		valueField: 'jproduk_cara_value',
		editable: false,
		//width: 100,
		anchor: '98%',
		triggerAction: 'all'	
	});
	
	jproduk_cara3Field= new Ext.form.ComboBox({
		id: 'jproduk_cara3Field',
		fieldLabel: 'Cara Bayar 3',
		store:new Ext.data.SimpleStore({
			fields:['jproduk_cara_value', 'jproduk_cara_display'],
			data:[['tunai','Tunai'],['kwitansi','Kuitansi/Gift Voucher'],['card','Kartu Kredit'],['cek/giro','Cek/Giro'],['transfer','Transfer']]
		}),
		mode: 'local',
		displayField: 'jproduk_cara_display',
		valueField: 'jproduk_cara_value',
		editable: false,
		width: 100,
		triggerAction: 'all'	
	});
		
	jproduk_stat_dokField= new Ext.form.ComboBox({
		id: 'jproduk_stat_dokField',
		fieldLabel: 'Status Dokumen',
		store:new Ext.data.SimpleStore({
			fields:['jproduk_stat_dok_value', 'jproduk_stat_dok_display'],
			data:[['Terbuka','Terbuka'],['Tertutup','Tertutup'],['Batal','Batal']]
		}),
		mode: 'local',
		displayField: 'jproduk_stat_dok_display',
		valueField: 'jproduk_stat_dok_value',
		editable: false,
		width: 100,
		triggerAction: 'all'	
	});
	
	produk_barcodeField= new Ext.form.TextField({
		id: 'produk_barcodeField',
		fieldLabel: 'Produk',
		emptyText : '',
		readOnly:true,
		disabled : false,
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/,
		anchor: '85%'
	});
	
	var lastKeyTime = new Date();
	var threshold = 300;
	produk_barcodeField.on('keyup', function(th, e){
		if(e.getKey()==13){
			pilih_produk();
		}
	});
	
	jproduk_keteranganField= new Ext.form.TextArea({
		id: 'jproduk_keteranganField',
		fieldLabel: 'Keterangan',
		maxLength: 250,
		anchor: '95%'
	});
	
	jproduk_voucher_noField= new Ext.form.TextField({
		id: 'jproduk_voucher_noField',
		fieldLabel: 'Nomor Voucher',
		maxLength: 10,
		anchor: '95%'
	});

	jproduk_voucher_cashback_cfField= new Ext.form.TextField({
		id: 'jproduk_voucher_cashback_cfField',
		fieldLabel: 'Nilai Cashback',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_voucher_cashbackField= new Ext.form.NumberField({
		id: 'jproduk_voucher_cashbackField',
		enableKeyEvents: true,
		fieldLabel: 'Nilai Cashback',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_voucherGroup= new Ext.form.FieldSet({
		title: 'Voucher',
		autoHeight: true,
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_voucher_noField,jproduk_voucher_cashback_cfField] 
			}
		]
	
	});
	
	jproduk_voucher_no2Field=new Ext.form.TextField({
		id: 'jproduk_voucher_no2Field',
		fieldLabel: 'Nomor Voucher',
		maxLength: 10,
		anchor: '95%'
	});
	
	jproduk_voucher_cashback2_cfField= new Ext.form.TextField({
		id: 'jproduk_voucher_cashback2_cfField',
		fieldLabel: 'Nilai Cashback',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_voucher_cashback2Field= new Ext.form.NumberField({
		id: 'jproduk_voucher_cashback2Field',
		enableKeyEvents: true,
		fieldLabel: 'Nilai Cashback',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_voucher2Group= new Ext.form.FieldSet({
		title: 'Voucher',
		autoHeight: true,
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_voucher_no2Field,jproduk_voucher_cashback2_cfField] 
			}
		]
	
	});
	
	jproduk_voucher_no3Field=new Ext.form.TextField({
		id: 'jproduk_voucher_no3Field',
		fieldLabel: 'Nomor Voucher',
		maxLength: 10,
		anchor: '95%'
	});
	
	jproduk_voucher_cashback3_cfField= new Ext.form.TextField({
		id: 'jproduk_voucher_cashback3_cfField',
		fieldLabel: 'Nilai Cashback',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_voucher_cashback3Field= new Ext.form.NumberField({
		id: 'jproduk_voucher_cashback3Field',
		enableKeyEvents: true,
		fieldLabel: 'Nilai Cashback',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_voucher3Group= new Ext.form.FieldSet({
		title: 'Voucher',
		autoHeight: true,
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_voucher_no3Field,jproduk_voucher_cashback3_cfField] 
			}
		]
	
	});
	
	jproduk_card_jenis_DataStore = new Ext.data.Store({
		id: 'jproduk_card_jenis_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=card_edc_jenis_list', 
			method: 'POST'
		}),
		baseParams:{task: "LIST", start:0, limit:jproduk_pageS}, // parameter yang di $_POST ke Controller
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'produk_edc_jenis_id'
		},[
			{name: 'produk_edc_jenis_id', type: 'int', mapping: 'produk_edc_jenis_id'},
			{name: 'produk_edc_jenis_nama', type: 'string', mapping: 'produk_edc_jenis_nama'}
		]),
		sortInfo:{field: 'produk_edc_jenis_id', direction: "ASC"}
	});
	
	jproduk_card_namaField= new Ext.form.ComboBox({
		id: 'jproduk_card_namaField',
		fieldLabel: 'Jenis Kartu',
		store: jproduk_card_jenis_DataStore,
		mode: 'remote',
		displayField: 'produk_edc_jenis_nama',
		valueField: 'produk_edc_jenis_nama',
		allowBlank: true,
		editable: false,
		//anchor: '50%',
		width: 123,
		triggerAction: 'all',
		lazyRenderer: true
	});
	
	jproduk_card_edc_DataStore = new Ext.data.Store({
		id: 'jproduk_card_edc_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=card_edc_list', 
			method: 'POST'
		}),
		baseParams:{task: "LIST", start:0, limit:jproduk_pageS}, // parameter yang di $_POST ke Controller
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'produk_edc_id'
		},[
			{name: 'produk_edc_id', type: 'int', mapping: 'produk_edc_id'},
			{name: 'produk_edc_nama', type: 'string', mapping: 'produk_edc_nama'}
		]),
		sortInfo:{field: 'produk_edc_id', direction: "ASC"}
	});
	
	jproduk_card_edcField= new Ext.form.ComboBox({
		id: 'jproduk_card_edcField',
		fieldLabel: 'EDC',
		store: jproduk_card_edc_DataStore,
		mode: 'remote',
		displayField: 'produk_edc_nama',
		valueField: 'produk_edc_nama',
		allowBlank: true,
		editable: false,
		width: 123,
		//anchor: '50%',
		triggerAction: 'all',
		lazyRenderer: true
	});

	jproduk_card_promo_DataStore = new Ext.data.Store({
		id: 'jproduk_card_promo_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=card_promo_list', 
			method: 'POST'
		}),
		baseParams:{task: "LIST", start:0, limit:jproduk_pageS}, // parameter yang di $_POST ke Controller
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'produk_promo_id'
		},[
			{name: 'produk_promo_id', type: 'int', mapping: 'produk_promo_id'},
			{name: 'produk_promo_nama', type: 'string', mapping: 'produk_promo_nama'}
		]),
		sortInfo:{field: 'produk_promo_id', direction: "ASC"}
	});
	
	jproduk_card_promoField= new Ext.form.ComboBox({
		id: 'jproduk_card_promoField',
		fieldLabel: 'Promo',
		store: jproduk_card_promo_DataStore,
		mode: 'remote',
		displayField: 'produk_promo_nama',
		valueField: 'produk_promo_nama',
		allowBlank: true,
		//anchor: '60%',
		width: 140,
		triggerAction: 'all',
		lazyRenderer: true
	});
	
	jproduk_card_noField= new Ext.form.TextField({
		id: 'jproduk_card_noField',
		fieldLabel: 'No Kartu',
		maxLength: 30,
		enableKeyEvents: true,
		width: 123
		//anchor: '95%'
	});
	
	var str_cek_length = 0;
	var str_value = '';
	jproduk_card_noField.on('keyup', function(th, e){
		var t_value = this.getRawValue();
		var t_length = t_value.length;

		if(t_value.substr(1, 1) == 'B' && t_value.substr(18, 1) == '^'){
			str_value = t_value.substr(2, 16);
			str_cek_length = 1;
		}

		if(str_cek_length == 1){
			jproduk_card_noField.reset();
			jproduk_card_noField.setValue('');
			jproduk_card_noField.setValue(str_value);
		}
		
		if(e.getKey() == e.BACKSPACE && str_cek_length == 1){
			str_value = '';
			str_cek_length = 0;

			switch (t_length){
			   case 15:
				   jproduk_card_noField.setValue(jproduk_card_noField.getValue().substr(0, 15));
				   break;
			   default: 
				   jproduk_card_noField.setValue('');
				   break;
			}
		}
		
		if(t_length > 16 && t_value.substr(0, 2) !== '%B'){ //16 digit number card
			var simbol = ["'", '"', '?', '!', ';', '#', '@', '$', '%', '^', '&', '*'];
			
			if(simbol.indexOf(t_value.substr(0, 1)) == '-1')
				jproduk_card_noField.setValue(t_value.substr(0, 16));
			else
				jproduk_card_noField.setValue(t_value.substr(1, 16));
		}
	});

	jproduk_card_noField.on('blur', function(th, e){
		str_value = '';
		str_cek_length = 0;
	});
	
	jproduk_card_nilai_cfField= new Ext.form.TextField({
		id: 'jproduk_card_nilai_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		//anchor: '95%',
		maskRe: /([0-9]+)$/,
		width: 123
	});
	jproduk_card_nilaiField= new Ext.form.NumberField({
		id: 'jproduk_card_nilaiField',
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		//anchor: '95%',
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/,
		width: 123
	});

	master_jual_produk_cardGroup= new Ext.form.FieldSet({	
		id:'master_jual_produk_cardGroup',
		title: 'Credit Card/Debit',
		layout: 'form',
		frame: false,
		hidden:true,
		collapsible: true,
		anchor: '98%',
		items:[jproduk_card_namaField, jproduk_card_edcField, jproduk_card_noField, jproduk_card_nilai_cfField]
	});	

	jproduk_card_nama2Field= new Ext.form.ComboBox({
		id: 'jproduk_card_nama2Field',
		fieldLabel: 'Jenis Kartu',
		store: jproduk_card_jenis_DataStore,
		mode: 'remote',
		displayField: 'produk_edc_jenis_nama',
		valueField: 'produk_edc_jenis_nama',
		allowBlank: true,
		editable: false,
		//anchor: '50%',
		width: 123,
		triggerAction: 'all',
		lazyRenderer: true
	});	
	
	jproduk_card_edc2Field= new Ext.form.ComboBox({
		id: 'jproduk_card_edc2Field',
		fieldLabel: 'EDC',
		store: jproduk_card_edc_DataStore,
		mode: 'remote',
		displayField: 'produk_edc_nama',
		valueField: 'produk_edc_nama',
		allowBlank: true,
		editable: false,
		//anchor: '50%',
		width: 123,
		triggerAction: 'all',
		lazyRenderer: true
	});
	
	jproduk_card_promo2Field= new Ext.form.ComboBox({
		id: 'jproduk_card_promo2Field',
		fieldLabel: 'Promo',
		store: jproduk_card_promo_DataStore,
		mode: 'remote',
		displayField: 'produk_promo_nama',
		valueField: 'produk_promo_nama',
		allowBlank: true,
		//anchor: '50%',
		width: 140,
		triggerAction: 'all',
		lazyRenderer: true
	});	
	
	jproduk_card_no2Field= new Ext.form.TextField({
		id: 'jproduk_card_no2Field',
		fieldLabel: 'No Kartu',
		maxLength: 30,
		enableKeyEvents: true,
		//anchor: '95%'
		width: 123
	});
	
	var str_cek_length = 0;
	var str_value = '';
	jproduk_card_no2Field.on('keyup', function(th, e){
		var t_value = this.getRawValue();
		var t_length = t_value.length;

		if(t_value.substr(1, 1) == 'B' && t_value.substr(18, 1) == '^'){
			str_value = t_value.substr(2, 16);
			str_cek_length = 1;
		}
		
		if(str_cek_length == 1){
			jproduk_card_no2Field.reset();
			jproduk_card_no2Field.setValue('');
			jproduk_card_no2Field.setValue(str_value);
		}
		
		if(e.getKey() == e.BACKSPACE && str_cek_length == 1){
			str_value = '';
			str_cek_length = 0;

			switch (t_length){
			   case 15:
				   jproduk_card_no2Field.setValue(jproduk_card_no2Field.getValue().substr(0, 15));
				   break;
			   default: 
				   jproduk_card_no2Field.setValue('');
				   break;
			}
		}
		
		if(t_length > 16 && t_value.substr(0, 2) !== '%B'){ //16 digit number card
			var simbol = ["'", '"', '?', '!', ';', '#', '@', '$', '%', '^', '&', '*'];
			
			if(simbol.indexOf(t_value.substr(0, 1)) == '-1')
				jproduk_card_no2Field.setValue(t_value.substr(0, 16));
			else
				jproduk_card_no2Field.setValue(t_value.substr(1, 16));
		}
	});
	jproduk_card_no2Field.on('blur', function(th, e){
		str_value = '';
		str_cek_length = 0;
	});
	
	jproduk_card_nilai2_cfField= new Ext.form.TextField({
		id: 'jproduk_card_nilai2_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		//anchor: '95%',
		width: 123,
		maskRe: /([0-9]+)$/
	});
	jproduk_card_nilai2Field= new Ext.form.NumberField({
		id: 'jproduk_card_nilai2Field',
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		//anchor: '95%',
		width: 123,
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/
	});

	master_jual_produk_card2Group= new Ext.form.FieldSet({	
		id:'master_jual_produk_card2Group',
		title: 'Credit Card/Debit',
		layout: 'form',
		frame: false,
		collapsible: true,
		hidden: true,
		anchor: '98%',
		items:[jproduk_card_nama2Field, jproduk_card_edc2Field, jproduk_card_no2Field, jproduk_card_nilai2_cfField]
	});	
	
	jproduk_card_nama3Field= new Ext.form.ComboBox({
		id: 'jproduk_card_nama3Field',
		fieldLabel: 'Jenis Kartu',
		store: jproduk_card_jenis_DataStore,
		mode: 'remote',
		displayField: 'produk_edc_jenis_nama',
		valueField: 'produk_edc_jenis_nama',
		allowBlank: true,
		editable: false,
		//anchor: '50%',
		width: 140,
		triggerAction: 'all',
		lazyRenderer: true
	});
	
	jproduk_card_edc3Field= new Ext.form.ComboBox({
		id: 'jproduk_card_edc3Field',
		fieldLabel: 'EDC',
		store: jproduk_card_edc_DataStore,
		mode: 'remote',
		displayField: 'produk_edc_nama',
		valueField: 'produk_edc_nama',
		allowBlank: true,
		editable: false,
		//anchor: '50%',
		width: 110,
		triggerAction: 'all',
		lazyRenderer: true
	});
	
	jproduk_card_promo3Field= new Ext.form.ComboBox({
		id: 'jproduk_card_promo3Field',
		fieldLabel: 'Promo',
		store: jproduk_card_promo_DataStore,
		mode: 'remote',
		displayField: 'produk_promo_nama',
		valueField: 'produk_promo_nama',
		allowBlank: true,
		//anchor: '50%',
		width: 140,
		triggerAction: 'all',
		lazyRenderer: true
	});		
	
	jproduk_card_no3Field= new Ext.form.TextField({
		id: 'jproduk_card_no3Field',
		fieldLabel: 'No Kartu',
		maxLength: 30,
		enableKeyEvents: true,
		//anchor: '95%'
		width: 320
	});
	
	var str_cek_length = 0;
	var str_value = '';
	jproduk_card_no3Field.on('keyup', function(th, e){
		var t_value = this.getRawValue();
		var t_length = t_value.length;

		if(t_value.substr(1, 1) == 'B' && t_value.substr(18, 1) == '^'){
			str_value = t_value.substr(2, 16);
			str_cek_length = 1;
		}
		
		if(str_cek_length == 1){
			jproduk_card_no3Field.reset();
			jproduk_card_no3Field.setValue('');
			jproduk_card_no3Field.setValue(str_value);
		}
		
		if(e.getKey() == e.BACKSPACE && str_cek_length == 1){
			str_value = '';
			str_cek_length = 0;

			switch (t_length){
			   case 15:
				   jproduk_card_no3Field.setValue(jproduk_card_no3Field.getValue().substr(0, 15));
				   break;
			   default: 
				   jproduk_card_no3Field.setValue('');
				   break;
			}
		}
		
		if(t_length > 16 && t_value.substr(0, 2) !== '%B'){ //16 digit number card
			var simbol = ["'", '"', '?', '!', ';', '#', '@', '$', '%', '^', '&', '*'];
			
			if(simbol.indexOf(t_value.substr(0, 1)) == '-1')
				jproduk_card_no3Field.setValue(t_value.substr(0, 16));
			else
				jproduk_card_no3Field.setValue(t_value.substr(1, 16));
		}
	});
	jproduk_card_no3Field.on('blur', function(th, e){
		str_value = '';
		str_cek_length = 0;
	});
	
	jproduk_card_nilai3_cfField= new Ext.form.TextField({
		id: 'jproduk_card_nilai3_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		//anchor: '95%',
		width: 320,
		maskRe: /([0-9]+)$/ 
	});
	jproduk_card_nilai3Field= new Ext.form.NumberField({
		id: 'jproduk_card_nilai3Field',
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		//anchor: '95%',
		width: 320,
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_card3Group= new Ext.form.FieldSet({	
		id:'master_jual_produk_card3Group',
		title: 'Credit Card',
		layout: 'column',
		frame: false,
		collapsible: true,
		hidden: true,
		anchor: '98%',
		items:[{layout: 'column',
				 border: false,
				 columnWidth:2.7,
				 items:[{layout: 'form',
						 border: false,
						 columnWidth:0.21,
						 labelWidth: 85,
						 bodyStyle:'padding:0px',
						 items:[jproduk_card_nama3Field] 
					   },
					   {layout: 'form',
						border: false,
						columnWidth:0.2,
						labelAlign: 'right',
						labelWidth: 49,
						bodyStyle:'padding:0px',
						items:[jproduk_card_edc3Field] 
					   }]
				},
				{layout: 'form',
				border:false,
				columnWidth:1,
				labelWidth: 85,
				items: [/*jproduk_card_promo3Field,*/jproduk_card_no3Field, jproduk_card_nilai3_cfField]							
				}
				]
	});	
	
	jproduk_cek_namaField= new Ext.form.TextField({
		id: 'jproduk_cek_namaField',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		anchor: '95%'
	});
	
	jproduk_cek_noField= new Ext.form.TextField({
		id: 'jproduk_cek_noField',
		fieldLabel: 'No Cek/Giro',
		allowBlank: true,
		anchor: '95%',
		maxLength: 50
	});
	
	jproduk_cek_validField= new Ext.form.DateField({
		id: 'jproduk_cek_validField',
		allowBlank: true,
		fieldLabel: 'Valid',
		format: 'Y-m-d'
	});
	
	jproduk_cek_bankField= new Ext.form.ComboBox({
		id: 'jproduk_cek_bankField',
		fieldLabel: 'Bank',
		store: jproduk_bankDataStore,
		mode: 'remote',
		displayField: 'jproduk_bank_display',
		valueField: 'jproduk_bank_value',
		allowBlank: true,
		anchor: '50%',
		triggerAction: 'all',
		lazyRenderer: true,
		renderer: Ext.util.Format.comboRenderer(jproduk_cek_bankField)
	});
	
	jproduk_cek_nilai_cfField= new Ext.form.TextField({
		id: 'jproduk_cek_nilai_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_cek_nilaiField= new Ext.form.NumberField({
		id: 'jproduk_cek_nilaiField',
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		anchor: '95%',
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_cekGroup = new Ext.form.FieldSet({
		title: 'Check/Giro',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_cek_namaField,jproduk_cek_noField,jproduk_cek_validField,jproduk_cek_bankField,jproduk_cek_nilai_cfField] 
			}
		]
	
	});

	jproduk_cek_nama2Field= new Ext.form.TextField({
		id: 'jproduk_cek_nama2Field',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		anchor: '95%'
	});
	
	jproduk_cek_no2Field= new Ext.form.TextField({
		id: 'jproduk_cek_no2Field',
		fieldLabel: 'No Cek/Giro',
		allowBlank: true,
		anchor: '95%',
		maxLength: 50
	});
	
	jproduk_cek_valid2Field= new Ext.form.DateField({
		id: 'jproduk_cek_valid2Field',
		allowBlank: true,
		fieldLabel: 'Valid',
		format: 'Y-m-d'
	});
	
	jproduk_cek_bank2Field= new Ext.form.ComboBox({
		id: 'jproduk_cek_bank2Field',
		fieldLabel: 'Bank',
		store: jproduk_bankDataStore,
		mode: 'remote',
		displayField: 'jproduk_bank_display',
		valueField: 'jproduk_bank_value',
		allowBlank: true,
		anchor: '50%',
		triggerAction: 'all',
		lazyRenderer: true,
		renderer: Ext.util.Format.comboRenderer(jproduk_cek_bankField)
	});
	
	jproduk_cek_nilai2_cfField= new Ext.form.TextField({
		id: 'jproduk_cek_nilai2_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_cek_nilai2Field= new Ext.form.NumberField({
		id: 'jproduk_cek_nilai2Field',
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		anchor: '95%',
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_cek2Group = new Ext.form.FieldSet({
		title: 'Check/Giro',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_cek_nama2Field,jproduk_cek_no2Field,jproduk_cek_valid2Field,jproduk_cek_bank2Field,jproduk_cek_nilai2_cfField] 
			}
		]
	
	});
	
	jproduk_cek_nama3Field= new Ext.form.TextField({
		id: 'jproduk_cek_nama3Field',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		anchor: '95%'
	});
	
	jproduk_cek_no3Field= new Ext.form.TextField({
		id: 'jproduk_cek_no3Field',
		fieldLabel: 'No Cek/Giro',
		allowBlank: true,
		anchor: '95%',
		maxLength: 50
	});
	
	jproduk_cek_valid3Field= new Ext.form.DateField({
		id: 'jproduk_cek_valid3Field',
		allowBlank: true,
		fieldLabel: 'Valid',
		format: 'Y-m-d'
	});
	
	jproduk_cek_bank3Field= new Ext.form.ComboBox({
		id: 'jproduk_cek_bank3Field',
		fieldLabel: 'Bank',
		store: jproduk_bankDataStore,
		mode: 'remote',
		displayField: 'jproduk_bank_display',
		valueField: 'jproduk_bank_value',
		allowBlank: true,
		anchor: '50%',
		triggerAction: 'all',
		lazyRenderer: true,
		renderer: Ext.util.Format.comboRenderer(jproduk_cek_bankField)
	});
	
	jproduk_cek_nilai3_cfField= new Ext.form.TextField({
		id: 'jproduk_cek_nilai3_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_cek_nilai3Field= new Ext.form.NumberField({
		id: 'jproduk_cek_nilai3Field',
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		anchor: '95%',
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_cek3Group = new Ext.form.FieldSet({
		title: 'Check/Giro',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_cek_nama3Field,jproduk_cek_no3Field,jproduk_cek_valid3Field,jproduk_cek_bank3Field,jproduk_cek_nilai3_cfField] 
			}
		]
	
	});
	
	jproduk_transfer_bankField= new Ext.form.ComboBox({
		id: 'jproduk_transfer_bankField',
		fieldLabel: 'Bank',
		store: jproduk_bankDataStore,
		mode: 'remote',
		displayField: 'jproduk_bank_display',
		valueField: 'jproduk_bank_value',
		allowBlank: true,
		anchor: '50%',
		triggerAction: 'all',
		lazyRenderer: true,
		renderer: Ext.util.Format.comboRenderer(jproduk_transfer_bankField)
	});

	jproduk_transfer_namaField= new Ext.form.TextField({
		id: 'jproduk_transfer_namaField',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		anchor: '95%',
		maxLength: 50
	});
	
	jproduk_transfer_nilai_cfField= new Ext.form.TextField({
		id: 'jproduk_transfer_nilai_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_transfer_nilaiField= new Ext.form.NumberField({
		id: 'jproduk_transfer_nilaiField',
		enableKeyEvents: true,
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_transfer_infoLabel=new Ext.form.Label({
		html: "<font size=1>*Cara bayar Transfer adalah cara bayar yang dilakukan customer dengan mengirimkan uang ke rekening perusahaan SEBELUM transaksi dilakukan."
	});

	master_jual_produk_transferGroup= new Ext.form.FieldSet({
		title: 'Transfer',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_transfer_bankField,jproduk_transfer_namaField,jproduk_transfer_nilai_cfField, master_jual_produk_transfer_infoLabel] 
			}
		]
	
	});

	jproduk_transfer_bank2Field= new Ext.form.ComboBox({
		id: 'jproduk_transfer_bank2Field',
		fieldLabel: 'Bank',
		store: jproduk_bankDataStore,
		mode: 'remote',
		displayField: 'jproduk_bank_display',
		valueField: 'jproduk_bank_value',
		allowBlank: true,
		anchor: '50%',
		triggerAction: 'all',
		lazyRenderer: true
	});

	jproduk_transfer_nama2Field= new Ext.form.TextField({
		id: 'jproduk_transfer_nama2Field',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		anchor: '95%',
		maxLength: 50
	});
	
	jproduk_transfer_nilai2_cfField= new Ext.form.TextField({
		id: 'jproduk_transfer_nilai2_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_transfer_nilai2Field= new Ext.form.NumberField({
		id: 'jproduk_transfer_nilai2Field',
		fieldLabel: 'Jumlah (Rp)',
		enableKeyEvents: true,
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_transfer_info2Label=new Ext.form.Label({
		html: "<font size=1>*Cara bayar Transfer adalah cara bayar yang dilakukan customer dengan mengirimkan uang ke rekening perusahaan SEBELUM transaksi dilakukan."
	});

	master_jual_produk_transfer2Group= new Ext.form.FieldSet({
		title: 'Transfer',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_transfer_bank2Field,jproduk_transfer_nama2Field,jproduk_transfer_nilai2_cfField, master_jual_produk_transfer_info2Label] 
			}
		]
	
	});

	jproduk_transfer_bank3Field= new Ext.form.ComboBox({
		id: 'jproduk_transfer_bank3Field',
		fieldLabel: 'Bank',
		store: jproduk_bankDataStore,
		mode: 'remote',
		displayField: 'jproduk_bank_display',
		valueField: 'jproduk_bank_value',
		allowBlank: true,
		anchor: '50%',
		triggerAction: 'all',
		lazyRenderer: true
	});

	jproduk_transfer_nama3Field= new Ext.form.TextField({
		id: 'jproduk_transfer_nama3Field',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		anchor: '95%',
		maxLength: 50
	});
	
	jproduk_transfer_nilai3_cfField= new Ext.form.TextField({
		id: 'jproduk_transfer_nilai3_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_transfer_nilai3Field= new Ext.form.NumberField({
		id: 'jproduk_transfer_nilai3Field',
		fieldLabel: 'Jumlah (Rp)',
		enableKeyEvents: true,
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	master_jual_produk_transfer_info3Label=new Ext.form.Label({
		html: "<font size=1>*Cara bayar Transfer adalah cara bayar yang dilakukan customer dengan mengirimkan uang ke rekening perusahaan SEBELUM transaksi dilakukan."
	});

	master_jual_produk_transfer3Group= new Ext.form.FieldSet({
		title: 'Transfer',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_transfer_bank3Field,jproduk_transfer_nama3Field,jproduk_transfer_nilai3_cfField, master_jual_produk_transfer_info3Label] 
			}
		]
	
	});
	
	jproduk_tunai_nilai_cfField= new Ext.form.TextField({
		id: 'jproduk_tunai_nilai_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '98%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_tunai_nilaiField= new Ext.form.NumberField({
		id: 'jproduk_tunai_nilaiField',
		enableKeyEvents: true,
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		anchor: '98%',
		maskRe: /([0-9]+)$/
	});

	master_jual_produk_tunaiGroup = new Ext.form.FieldSet({
		title: 'Tunai',
		autoHeight: true,
		collapsible: true,
		layout:'column',
		anchor: '98%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_tunai_nilai_cfField] 
			}
		]
	
	});
	
	jproduk_tunai_nilai2_cfField= new Ext.form.TextField({
		id: 'jproduk_tunai_nilai2_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '98%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_tunai_nilai2Field= new Ext.form.NumberField({
		id: 'jproduk_tunai_nilai2Field',
		enableKeyEvents: true,
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		anchor: '98%',
		maskRe: /([0-9]+)$/
	});

	master_jual_produk_tunai2Group = new Ext.form.FieldSet({
		title: 'Tunai',
		autoHeight: true,
		collapsible: true,
		layout:'column',
		anchor: '98%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_tunai_nilai2_cfField] 
			}
		]
	
	});
	
	jproduk_tunai_nilai3_cfField= new Ext.form.TextField({
		id: 'jproduk_tunai_nilai3_cfField',
		fieldLabel: 'Jumlah (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_tunai_nilai3Field= new Ext.form.NumberField({
		id: 'jproduk_tunai_nilai3Field',
		enableKeyEvents: true,
		fieldLabel: 'Jumlah (Rp)',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});

	master_jual_produk_tunai3Group = new Ext.form.FieldSet({
		title: 'Tunai',
		autoHeight: true,
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_tunai_nilai3_cfField] 
			}
		]
	
	});
	
	jproduk_kwitansi_namaField= new Ext.form.TextField({
		id: 'jproduk_kwitansi_namaField',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		readOnly: true,
		anchor: '95%'
	});
	
	jproduk_kwitansi_nilai_cfField= new Ext.form.TextField({
		id: 'jproduk_kwitansi_nilai_cfField',
		fieldLabel: 'Diambil (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_kwitansi_nilaiField= new Ext.form.NumberField({
		id: 'jproduk_kwitansi_nilaiField',
		enableKeyEvents: true,
		fieldLabel: 'Diambil (Rp)',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	/*Field ini dipakai untuk menyimpan ID dari tabel jual_kwitansi, dimana ID ini berperan untuk mengetahui apakah kwitansi tersebut akan di Insert atau di Update */
	jproduk_jual_kwitansi_idField1= new Ext.form.NumberField();
	jproduk_jual_kwitansi_idField2= new Ext.form.NumberField();
	jproduk_jual_kwitansi_idField3= new Ext.form.NumberField();
	
	jproduk_kwitansi_no_idField= new Ext.form.NumberField();
	jproduk_kwitansi_noField= new Ext.form.ComboBox({
		id: 'jproduk_kwitansi_noField',
		fieldLabel: 'Nomor KU/GV',
		store: cbo_kwitansi_jual_produk_DataStore,
		mode: 'remote',
		displayField:'ckwitansi_no',
		valueField: 'ckwitansi_id',
        typeAhead: false,
        loadingText: 'Searching...',
        pageSize:10,
        hideTrigger:false,
        tpl: kwitansi_jual_produk_tpl,
        itemSelector: 'div.search-item',
		triggerAction: 'all',
		lazyRender:true,
		listClass: 'x-combo-list-small',
		//queryDelay:720,
		anchor: '95%'
		
	});
	jproduk_flagField= new Ext.form.TextField({
		id: 'jproduk_flagField',
		fieldLabel: 'Jenis',
		allowBlank: true,
		hidden : true,
		anchor: '95%'
	});
	jproduk_kwitansi_sisaField= new Ext.form.NumberField({
		id: 'jproduk_kwitansi_sisaField',
		fieldLabel: 'Sisa (Rp)',
		readOnly: true,
		anchor: '95%'
	});
	
	jproduk_kwitansi_noField.on("select",function(){
		jproduk_kwitansi_nilaiField.reset();
		jproduk_kwitansi_nilaiField.setValue(null);
		jproduk_kwitansi_nilai_cfField.reset();
		jproduk_kwitansi_nilai_cfField.setValue(null);
		
		j=cbo_kwitansi_jual_produk_DataStore.findExact('ckwitansi_id',jproduk_kwitansi_noField.getValue(),0);
		if(j>-1){
			jproduk_flagField.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.jenis);
			jproduk_kwitansi_namaField.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.ckwitansi_cust_nama);
			jproduk_kwitansi_sisaField.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.total_sisa);
			jproduk_kwitansi_no_idField.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.ckwitansi_id);
			
			if(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.jenis == 'gv'){
				Ext.MessageBox.show({
					title: 'Warning',
					msg: 'Untuk penggunaan Gift Voucher, ubah Jenis Diskon produk menjadi <b>Tanpa Diskon</b>',
					buttons: Ext.MessageBox.OK,
					animEl: 'save',
					icon: Ext.MessageBox.WARNING
				});
			}
		}
	});
	
	jproduk_kwitansi_nama2Field= new Ext.form.TextField({
		id: 'jproduk_kwitansi_nama2Field',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		readOnly: true,
		anchor: '95%'
	});
	
	jproduk_kwitansi_nilai2_cfField= new Ext.form.TextField({
		id: 'jproduk_kwitansi_nilai2_cfField',
		fieldLabel: 'Diambil (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_kwitansi_nilai2Field= new Ext.form.NumberField({
		id: 'jproduk_kwitansi_nilai2Field',
		enableKeyEvents: true,
		fieldLabel: 'Diambil (Rp)',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	jproduk_kwitansi_no2_idField= new Ext.form.NumberField();
	jproduk_kwitansi_no2Field= new Ext.form.ComboBox({
		id: 'jproduk_kwitansi_no2Field',
		fieldLabel: 'Nomor KU/GV',
		store: cbo_kwitansi_jual_produk_DataStore,
		mode: 'remote',
		displayField:'ckwitansi_no',
		valueField: 'ckwitansi_id',
        typeAhead: false,
        loadingText: 'Searching...',
        pageSize:10,
        hideTrigger:false,
        tpl: kwitansi_jual_produk_tpl,
        itemSelector: 'div.search-item',
		triggerAction: 'all',
		lazyRender:true,
		listClass: 'x-combo-list-small',
		//queryDelay:720,
		anchor: '95%'
	});
	jproduk_flag2Field= new Ext.form.TextField({
		id: 'jproduk_flag2Field',
		fieldLabel: 'Jenis',
		allowBlank: true,
		hidden : true,
		anchor: '95%'
	});
	jproduk_kwitansi_sisa2Field= new Ext.form.NumberField({
		id: 'jproduk_kwitansi_sisa2Field',
		fieldLabel: 'Sisa (Rp)',
		readOnly: true,
		anchor: '95%'
	});
	
	jproduk_kwitansi_no2Field.on("select",function(){
		jproduk_kwitansi_nilai2Field.reset();
		jproduk_kwitansi_nilai2Field.setValue(null);
		jproduk_kwitansi_nilai2_cfField.reset();
		jproduk_kwitansi_nilai2_cfField.setValue(null);
		
		j=cbo_kwitansi_jual_produk_DataStore.findExact('ckwitansi_id',jproduk_kwitansi_no2Field.getValue(),0);
		if(j>-1){
			jproduk_flag2Field.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.jenis);
			jproduk_kwitansi_nama2Field.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.ckwitansi_cust_nama);
			jproduk_kwitansi_sisa2Field.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.total_sisa);
			jproduk_kwitansi_no2_idField.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.ckwitansi_id);
			
			if(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.jenis == 'gv'){
				Ext.MessageBox.show({
					title: 'Warning',
					msg: 'Untuk penggunaan Gift Voucher, ubah Jenis Diskon produk menjadi <b>Tanpa Diskon</b>',
					buttons: Ext.MessageBox.OK,
					animEl: 'save',
					icon: Ext.MessageBox.WARNING
				});
			}
		}
	});
	
	jproduk_kwitansi_nama3Field= new Ext.form.TextField({
		id: 'jproduk_kwitansi_nama3Field',
		fieldLabel: 'Atas Nama',
		allowBlank: true,
		readOnly: true,
		anchor: '95%'
	});
	
	jproduk_kwitansi_nilai3_cfField= new Ext.form.TextField({
		id: 'jproduk_kwitansi_nilai3_cfField',
		fieldLabel: 'Diambil (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		anchor: '95%',
		maskRe: /([0-9]+)$/ 
	});
	jproduk_kwitansi_nilai3Field= new Ext.form.NumberField({
		id: 'jproduk_kwitansi_nilai3Field',
		enableKeyEvents: true,
		fieldLabel: 'Diambil (Rp)',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	jproduk_kwitansi_no3_idField= new Ext.form.NumberField();
	jproduk_kwitansi_no3Field= new Ext.form.ComboBox({
		id: 'jproduk_kwitansi_no3Field',
		fieldLabel: 'Nomor KU/GV',
		store: cbo_kwitansi_jual_produk_DataStore,
		mode: 'remote',
		displayField:'ckwitansi_no',
		valueField: 'ckwitansi_id',
        typeAhead: false,
        loadingText: 'Searching...',
        pageSize:10,
        hideTrigger:false,
        tpl: kwitansi_jual_produk_tpl,
        itemSelector: 'div.search-item',
		triggerAction: 'all',
		lazyRender:true,
		listClass: 'x-combo-list-small',
		//queryDelay:720,
		anchor: '95%'
	});
	jproduk_flag3Field= new Ext.form.TextField({
		id: 'jproduk_flag3Field',
		fieldLabel: 'Jenis',
		allowBlank: true,
		hidden : true,
		anchor: '95%'
	});
	jproduk_kwitansi_sisa3Field= new Ext.form.NumberField({
		id: 'jproduk_kwitansi_sisa3Field',
		fieldLabel: 'Sisa (Rp)',
		readOnly: true,
		anchor: '95%'
	});
	
	jproduk_kwitansi_no3Field.on("select",function(){
		jproduk_kwitansi_nilai3Field.reset();
		jproduk_kwitansi_nilai3Field.setValue(null);
		jproduk_kwitansi_nilai3_cfField.reset();
		jproduk_kwitansi_nilai3_cfField.setValue(null);
		
		j=cbo_kwitansi_jual_produk_DataStore.findExact('ckwitansi_id',jproduk_kwitansi_no3Field.getValue(),0);
		if(j>-1){
			jproduk_flag3Field.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.jenis);
			jproduk_kwitansi_nama3Field.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.ckwitansi_cust_nama);
			jproduk_kwitansi_sisa3Field.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.total_sisa);
			jproduk_kwitansi_no3_idField.setValue(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.ckwitansi_id);
			
			if(cbo_kwitansi_jual_produk_DataStore.getAt(j).data.jenis == 'gv'){
				Ext.MessageBox.show({
					title: 'Warning',
					msg: 'Untuk penggunaan Gift Voucher, ubah Jenis Diskon produk menjadi <b>Tanpa Diskon</b>',
					buttons: Ext.MessageBox.OK,
					animEl: 'save',
					icon: Ext.MessageBox.WARNING
				});
			}
		}
	});
	
	master_jual_produk_kwitansiGroup = new Ext.form.FieldSet({
		title: 'Kuitansi/Gift Voucher',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_kwitansi_noField,jproduk_kwitansi_namaField,jproduk_kwitansi_sisaField,jproduk_kwitansi_nilai_cfField] 
			}
		]
	
	});
	
	master_jual_produk_kwitansi2Group = new Ext.form.FieldSet({
		title: 'Kuitansi/Gift Voucher',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_kwitansi_no2Field,jproduk_kwitansi_nama2Field,jproduk_kwitansi_sisa2Field,jproduk_kwitansi_nilai2_cfField] 
			}
		]
	
	});
	
	master_jual_produk_kwitansi3Group = new Ext.form.FieldSet({
		title: 'Kuitansi/Gift Voucher',
		collapsible: true,
		layout:'column',
		anchor: '95%',
		hidden: true,
		items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_kwitansi_no3Field,jproduk_kwitansi_nama3Field,jproduk_kwitansi_sisa3Field,jproduk_kwitansi_nilai3_cfField] 
			}
		]
	
	});
	
	//* Bayar
	jproduk_jumlahLabel= new Ext.form.DisplayField({
		fieldLabel : 'Jumlah Barang'//,
		//itemCls : 'rata_kanan'
	});
	jproduk_subTotalLabel= new Ext.form.DisplayField({
		fieldLabel : 'Sub Total'//,
		//itemCls : 'rata_kanan'
		//itemCls : 'rmoney2'
	});
	jproduk_TotalLabel= new Ext.form.DisplayField({
		fieldLabel : '<span style="font-weight:bold"><font size=2>Total</span>',
		itemCls : 'rmoney5'
	});
	
	jproduk_TotalBayarLabel= new Ext.form.DisplayField({
		fieldLabel : '<span style="font-weight:bold"><font size=2>Total Bayar</span>',
		itemCls : 'rmoney5'
	});
	
	jproduk_HutangLabel= new Ext.form.DisplayField({
		id: 'jproduk_HutangLabel',
		fieldLabel : '<span style="font-weight:bold"><font size=2>Hutang</span>',
		itemCls : 'rmoney_hutang5'
	});
	
	jproduk_jumlahField= new Ext.form.NumberField({
		id: 'jproduk_jumlahField',
		fieldLabel: 'Jumlah Item',
		allowBlank: true,
		readOnly: true,
		allowDecimals: false,
		width: 40,
		maxLength: 50,
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/
	});
	
	jproduk_subTotal_cfField= new Ext.form.TextField({
		id: 'jproduk_subTotal_cfField',
		fieldLabel: 'Sub Total (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		allowDecimals : false,
		itemCls: 'rmoney',
		width: 120,
		maskRe: /([0-9]+)$/ 
	});
	
	jproduk_subTotalField= new Ext.form.NumberField({
		id: 'jproduk_subTotalField',
		enableKeyEvents: true,
		fieldLabel: 'Sub Total (Rp)',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});
	
	//SUB TOTAL DPP
	jproduk_dpp_Field= new Ext.form.NumberField({
		id: 'jproduk_dpp_Field',
		fieldLabel: 'Sub Total DPP (Rp)',
		valueRenderer: 'numberToCurrency',
		readOnly: true,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		width: 120
	});
	jproduk_dpp_cfField= new Ext.form.TextField({
		id: 'jproduk_dpp_cfField',
		allowNegatife : false,
		enableKeyEvents: true,
		allowDecimals : false,
		itemCls: 'rmoney',
		width: 120,
		readOnly : true,
		maskRe: /([0-9]+)$/ 
	});

	jproduk_total_cfField= new Ext.form.TextField({
		id: 'jproduk_total_cfField',
		fieldLabel: '<span style="font-weight:bold">Total (Rp)</span>',
		allowNegatife : false,
		enableKeyEvents: true,
		allowDecimals : false,
		itemCls: 'rmoney',
		width: 120,
		readOnly : true,
		maskRe: /([0-9]+)$/ 
	});
	jproduk_totalField= new Ext.form.NumberField({
		id: 'jproduk_totalField',
		enableKeyEvents: true,
		fieldLabel: '<span style="font-weight:bold">Total (Rp)</span>',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});

	jproduk_bayar_cfField= new Ext.form.TextField({
		id: 'jproduk_bayar_cfField',
		fieldLabel: 'Total Bayar (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		width: 120,
		readOnly : true,
		maskRe: /([0-9]+)$/ 
	});
	jproduk_bayarField= new Ext.form.NumberField({
		id: 'jproduk_bayarField',
		enableKeyEvents: true,
		fieldLabel: 'Total Bayar (Rp)',
		allowDecimals : false,
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});

	jproduk_hutang_cfField= new Ext.form.TextField({
		id: 'jproduk_hutang_cfField',
		fieldLabel: 'Hutang (Rp)',
		allowNegatife : false,
		enableKeyEvents: true,
		itemCls: 'rmoney',
		width: 120,
		maskRe: /([0-9]+)$/ 
	});
	jproduk_hutangField= new Ext.form.NumberField({
		id: 'jproduk_hutangField',
		enableKeyEvents: true,
		fieldLabel: 'Hutang (Rp)',
		allowBlank: true,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});

	jproduk_pesanLabel= new Ext.form.Label({
		style: {
			marginLeft: '0px',
			fontSize: '18px',
			style: {fontWeight:'bold'},
			color: '#CC0000'
		}
	});
	jproduk_lunasLabel= new Ext.form.Label({
		style: {
			//marginLeft: '105px',
			fontSize: '18px',
			style: {fontWeight:'bold'},
			color: '#006600'
		}
	});
	
	master_cara_bayarTabPanel = new Ext.TabPanel({
		plain:true,
		activeTab: 0,
		frame: true,
		height: 220,
		width: 290,
		defaults:{bodyStyle:'padding:10px'},
		items:[{
                title:'Cara Bayar 1',
                layout:'form',
				frame: true,
                defaults: {width: 250},
                defaultType: 'textfield',
                items: [jproduk_caraField,master_jual_produk_tunaiGroup,master_jual_produk_cardGroup]
            },{
                title:'Cara Bayar 2',
                layout:'form',
				frame: true,
                defaults: {width: 250},
                defaultType: 'textfield',
                items: [jproduk_cara2Field, master_jual_produk_tunai2Group,master_jual_produk_card2Group]
            }]
	});
	
	master_jual_produk_bayarGroup = new Ext.form.FieldSet({
		title: '-',
		autoHeight: true,
		width: 580,
		collapsible: true,
		layout:'column',
		frame: true,
		items:[
			   {
				columnWidth:0.55,
				layout: 'form',
				border:false,
				items: [master_cara_bayarTabPanel] 
			}
			,{
				columnWidth:0.45,
				labelWidth: 100,
				layout: 'form',
				bodyStyle:'padding-left:20px',
    			labelPad: 0,
				baseCls: 'x-plain',
				border:false,
				labelAlign: 'left',
				items: [jproduk_jumlahLabel,jproduk_subTotalLabel, jproduk_potong_pointcfField,
			  jproduk_TotalLabel,  jproduk_pesanLabel, jproduk_lunasLabel,{xtype: 'spacer',height:10}, jproduk_uang_cfField, kembalian_label] 
			}
			]
	});
	
	master_jual_produk_masterGroup = new Ext.form.FieldSet({
		title: 'Master',
		autoHeight: true,
		collapsible: true,
		layout:'column',
		items:[
			{
				columnWidth:0.5,
				layout: 'form',
				border:false,
				items: [jproduk_nobuktiField, jproduk_custField,appointment_custBaruField,appointment_custBaruGroup,
						{
							columnWidth:0.5,
							layout: 'column',
							border:false,
							items:[jproduk_cust_nomemberLabel, jproduk_cust_nomemberField,jproduk_valid_memberLabel, jproduk_cust_pointLabel, jproduk_cust_pointField]
						}, {xtype: 'spacer',height:5},
						{
							columnWidth:0.5,
							layout: 'column',
							border:false,
							items:[jproduk_cust_ultahLabel, jproduk_cust_ultahField,jproduk_cust_priorityLabel]
						}
						] 
			},			
			{
				columnWidth:0.5,
				layout: 'form',
				border:false,
				items: [jproduk_tanggalField, jproduk_stat_dokField, produk_barcodeField] 
			}
			]
	});
	
		
	var detail_jual_produk_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: ''
	},[
			{name: 'dproduk_id', type: 'int', mapping: 'dproduk_id'}, 
			{name: 'dproduk_master', type: 'int', mapping: 'dproduk_master'}, 
			{name: 'dproduk_produk', type: 'int', mapping: 'dproduk_produk'}, 
			{name: 'dproduk_satuan', type: 'int', mapping: 'dproduk_satuan'}, 
			{name: 'dproduk_jumlah', type: 'int', mapping: 'dproduk_jumlah'}, 
			{name: 'dproduk_harga', type: 'float', mapping: 'dproduk_harga'}, 
			{name: 'dproduk_diskon', type: 'float', mapping: 'dproduk_diskon'},
			{name: 'dproduk_sales', type: 'string', mapping: 'dproduk_sales'},
			{name: 'dproduk_diskon_jenis', type: 'string', mapping: 'dproduk_diskon_jenis'},
			{name: 'dproduk_keterangan', type: 'string', mapping: 'dproduk_keterangan'},
			{name: 'nama_karyawan', type: 'string', mapping: 'karyawan_username'},
			{name: 'dproduk_karyawan', type: 'int', mapping: 'dproduk_karyawan'},
			{name: 'dproduk_subtotal', type: 'float', mapping: 'dproduk_subtotal'},
			{name: 'dproduk_subtotal_net', type: 'float', mapping: 'dproduk_subtotal_net'},
			{name: 'jproduk_bayar', type: 'float', mapping: 'jproduk_bayar'},
			{name: 'jproduk_diskon', type: 'int', mapping: 'jproduk_diskon'},
			{name: 'jproduk_cashback', type: 'float', mapping: 'jproduk_cashback'},
			{name: 'produk_harga_default', type: 'float', mapping: 'produk_harga_default'}
	]);
	
	var detail_jual_produk_writer = new Ext.data.JsonWriter({
		encode: true,
		writeAllFields: false
	});
	
	detail_jual_produk_DataStore = new Ext.data.Store({
		id: 'detail_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=detail_detail_jual_produk_list', 
			method: 'POST'
		}),baseParams: {start: 0, limit: jproduk_pageS},
		reader: detail_jual_produk_reader,
		sortInfo:{field: 'dproduk_id', direction: "ASC"}
	});
	
	//function ngisi ke array untuk pengecekan detail produk
	function arr_detail_produk_insert(){
		if(typeof detail_jual_produk_DataStore!=='undefined'){
			var jml_dproduk = detail_jual_produk_DataStore.getCount();
			for(var i=0;i<jml_dproduk;i++){
				arr_dproduk_id.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_id);
			}
		}		
	}
	
	function push_insert(){
		    var jmlpro=0;
			var jmlrawat=0;
			var jmlapk=0;
			var jmlpaket=0;
		    
			var all_item=[];
			var all_jenis=[];
		    var all_jumlah=[];
		    var all_hrg_sat=[];
		    var all_diskon=[];
			
			var potong_point=0;
			var produk_voucher=0;
			var rawat_voucher=0;
			var paket_voucher=0;
			
			if((jproduk_post2db=="CREATE") && (jproduk_custField.getValue()!== null)){
				var produk_cust = jproduk_custField.getValue();
			}else if(jproduk_post2db=="UPDATE"){
				var produk_cust = jproduk_cust_idField.getValue();
			}
			var rawat_cust='';
			var paket_cust='';
			
			if(typeof detail_jual_paket_DataStore!=='undefined'){
				if((jproduk_post2db=="CREATE") && (jproduk_custField.getValue()!== null)){
					paket_cust = jproduk_custField.getValue();
				}else if(jproduk_post2db=="UPDATE"){
					paket_cust = jproduk_cust_idField.getValue();
				}
				if(paket_cust==produk_cust){
					jmlpaket=detail_jual_paket_DataStore.getCount();
					for(var i=0;i<jmlpaket;i++){
					all_item.push(detail_jual_paket_DataStore.getAt(i).data.dpaket_paket);
					all_jumlah.push(detail_jual_paket_DataStore.getAt(i).data.dpaket_jumlah);
					all_hrg_sat.push(detail_jual_paket_DataStore.getAt(i).data.dpaket_harga);
					all_diskon.push(detail_jual_paket_DataStore.getAt(i).data.dpaket_diskon);
					all_jenis.push('Jual Paket');
					}
					if(jproduk_cashback_cfField.getValue()!==null && jproduk_cashback_cfField.getValue()){
					paket_voucher=convertToNumber(jproduk_cashback_cfField.getValue());
					}
				}
			}
			if(typeof detail_jual_rawat_DataStore!=='undefined' && detail_ambil_paketDataStore!=='undefined'){
				if(/^\d+$/.test(jproduk_custField.getValue())){
					rawat_cust = jproduk_custField.getValue();
				}else{
					rawat_cust = jproduk_cust_idField.getValue();
				}
				if(rawat_cust==produk_cust){
					jmlrawat=detail_jual_rawat_DataStore.getCount();
					if(jmlrawat>0){
						for(var i=0;i<jmlrawat;i++){
							all_item.push(detail_jual_rawat_DataStore.getAt(i).data.drawat_rawat);
							all_jumlah.push(detail_jual_rawat_DataStore.getAt(i).data.drawat_jumlah);
							all_hrg_sat.push(detail_jual_rawat_DataStore.getAt(i).data.drawat_harga);
							all_diskon.push(detail_jual_rawat_DataStore.getAt(i).data.drawat_diskon);
							all_jenis.push('Perawatan');
						}
					}
					if(jmlapk>0){
						for(var j=0;j<jmlapk;j++){
							all_item.push(detail_ambil_paketDataStore.getAt(j).data.rawat_id);
							all_jumlah.push(detail_ambil_paketDataStore.getAt(j).data.dapaket_jumlah);
							all_hrg_sat.push(0);
							all_diskon.push(0);
							all_jenis.push('Ambil Paket');
						}
					}
					if(jrawat_cashback_cfField.getValue()!==null && jrawat_cashback_cfField.getValue()){
					rawat_voucher+=convertToNumber(jrawat_cashback_cfField.getValue());
					}
					if(jrawat_cashback_cf_medisField.getValue()!==null && jrawat_cashback_cf_medisField.getValue()){
					rawat_voucher+=convertToNumber(jrawat_cashback_cf_medisField.getValue());
					}
				}
			}
			if(typeof detail_jual_produk_DataStore!=='undefined'){
			/*	if((jproduk_post2db=="CREATE") && (jproduk_custField.getValue()!== null)){
					produk_cust = jproduk_custField.getValue();
				}else if(jproduk_post2db=="UPDATE"){
					produk_cust = jproduk_cust_idField.getValue();
				}	*/
				if(produk_cust){
					jmlpro=detail_jual_produk_DataStore.getCount();
					for(var i=0;i<jmlpro;i++){
					all_item.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_produk);
					all_jumlah.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_jumlah);
					all_hrg_sat.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_harga);
					all_diskon.push(detail_jual_produk_DataStore.getAt(i).data.dproduk_diskon);
					all_jenis.push('Jual Produk');
					}
					if(jproduk_cashback_cfField.getValue()!==null && jproduk_cashback_cfField.getValue()){
					produk_voucher=convertToNumber(jproduk_cashback_cfField.getValue());
					}	
					if(jproduk_potong_pointcfField.getValue()!==null && jproduk_potong_pointcfField.getValue()){
					potong_point=convertToNumber(jproduk_potong_pointcfField.getValue());
					}
				}
			}
		    
		    var arr_all_item=Ext.encode(all_item);
			var arr_all_jenis=Ext.encode(all_jenis);
		    var arr_all_jumlah=Ext.encode(all_jumlah);
		    var arr_all_hrg_sat=Ext.encode(all_hrg_sat);
		    var arr_all_diskon=Ext.encode(all_diskon);
		    
		    Ext.Ajax.request({
			url: 'index.php?c=c_master_jual_produk&m=push_insert',
			params: {
				id_cust		: produk_cust,
			    id_item		: arr_all_item,
				item_jenis	: arr_all_jenis,
			    jumlah_beli	: arr_all_jumlah,
			    harga_satuan: arr_all_hrg_sat,
			    diskon		: arr_all_diskon,
			    potong_point: potong_point,
			    vcr_produk	: produk_voucher,
				vcr_rawat	: rawat_voucher,
				vcr_paket	: paket_voucher,
			    count		: jmlpro+jmlrawat+jmlapk+jmlpaket
			},
			success: function(response){
				Ext.MessageBox.hide();
			},
			failure: function(response){
			    var result=response.responseText;
			    Ext.MessageBox.show({
				   title: 'Error',
				   msg: 'Could not connect to the database. retry later.',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
			    });
			}
		    });
	}
	
	function push_reset(produk_cust){
			 Ext.Ajax.request({
			url: 'index.php?c=c_master_jual_produk&m=push_reset',
			params: {
				id_cust		: produk_cust,
			},
			success: function(response){},
			failure: function(response){
			    var result=response.responseText;
			    Ext.MessageBox.show({
				   title: 'Error',
				   msg: 'Could not connect to the database. retry later.',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
			    });
			}
		    });
	}
		
	var editor_detail_jual_produk= new Ext.ux.grid.RowEditor({
		saveText: 'Update'
	});

	editor_detail_jual_produk.on({
		beforeedit: function(){
			combo_jual_produk.setDisabled(false);
			combo_satuan_produk.setDisabled(false);			
			var produk_rec_sel = detail_jual_produkListEditorGrid.getSelectionModel().getSelected();

			for(i=0;i<arr_dproduk_id.length;i++) {
				if(produk_rec_sel.get("dproduk_id") == arr_dproduk_id[i] && arr_dproduk_id[i] != ''){
					combo_jual_produk.setDisabled(true);
					combo_satuan_produk.setDisabled(true);
					return;
				}
			}
			cbo_dproduk_satuanDataStore.load({params: {produk_id: '', start: 0, limit: 15}});
		},		
		afteredit: function(thisEditor, obj, rec, index){
			check_produk(rec.data,index);
			
			//reset datastore agar ketika selesai update row dan mengganti jenis diskon tidak error
			cbo_dproduk_produkDataStore.load({params: {query: '', start: 0, limit: 15}});
		},
		canceledit: function(thisEditor, obj, rec, index){
			detail_jual_produk_DataStore.rejectChanges();
			detail_jual_produkListEditorGrid.getView().refresh();
		}		
	});
	
	// fungsi untuk cek satuan dan harga produk sesuai master produk
	function check_produk(data,index){
		
		var dataproduk;
		var valid = false;
		Ext.Ajax.request({
			waitMsg: 'Silahkan Tunggu...',
			url: 'index.php?c=c_master_jual_produk&m=get_satuan_bydjproduk_list',
			params: { 
				produk_id : data.dproduk_produk,
				satuan_id : data.dproduk_satuan
			},
			callback: function(opts, success, response){
				
				if(success){
					// merubah json string menjadi object
					dataproduk = eval(response); 
					
					// ambil data yang direturn controller di object responseText
					dataproduk = eval(dataproduk.responseText);
					
					for(i=0;i<dataproduk.results.length;i++) { // apabila satuan lebih dari satu
						var t_data = dataproduk.results[i];
						
						if(t_data.produk_edit_harga == 1){
							if(data.dproduk_satuan == t_data.satuan_id) {
								valid = true;
								break;
							}	
						} else {
							if(data.dproduk_satuan == t_data.satuan_id && data.dproduk_harga == t_data.produk_harga) {
								valid = true;
								break;
							}	
						}
						
					}
										
					if(valid === false){
						Ext.MessageBox.show({
							title: 'Peringatan',
							msg: 'Harga atau satuan produk <b>tidak sesuai</b> dengan master data.<br><br>Mohon periksa kembali.',
							buttons: Ext.MessageBox.OK,
							animEl: 'save',
							icon: Ext.MessageBox.WARNING
						});	
						detail_jual_produk_DataStore.removeAt(index);
					} else {
						if(typeof(data.dproduk_karyawan) == "number" || data.dproduk_karyawan === ""){
							push_insert();
							detail_jual_produk_DataStore.commitChanges();
						} else {
							Ext.MessageBox.show({
								title: 'Peringatan',
								msg: "Referal '" + data.dproduk_karyawan + "' <b>tidak ada</b> di master data.<br><br>Silahkan pilih referal yang benar.",
								buttons: Ext.MessageBox.OK,
								animEl: 'save',
								icon: Ext.MessageBox.WARNING
							});
							detail_jual_produk_DataStore.removeAt(index);
						}
					}
				}
			}
		});
	}
	
	cbo_dproduk_produkDataStore = new Ext.data.Store({
		id: 'cbo_dproduk_produkDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_produk_list', 
			method: 'POST'
		}),baseParams: {aktif: 'yes', start: 0, limit: 15 },
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'produk_id'
		},[
			{name: 'dproduk_produk_value', type: 'int', mapping: 'produk_id'},
			{name: 'dproduk_produk_harga', type: 'float', mapping: 'produk_harga'},
			{name: 'dproduk_produk_kode', type: 'string', mapping: 'produk_kode'},
			{name: 'dproduk_produk_satuan', type: 'string', mapping: 'satuan_kode'},
			{name: 'dproduk_produk_group', type: 'string', mapping: 'group_nama'},
			{name: 'dproduk_produk_kategori', type: 'string', mapping: 'kategori_nama'},
			{name: 'dproduk_produk_du', type: 'float', mapping: 'produk_du'},
			{name: 'dproduk_produk_dm', type: 'float', mapping: 'produk_dm'},
			{name: 'dproduk_produk_dm2', type: 'float', mapping: 'produk_dm2'},
			{name: 'dproduk_produk_dultah', type: 'float', mapping: 'produk_dultah'},
			{name: 'dproduk_produk_dcard', type: 'float', mapping: 'produk_dcard'},
			{name: 'dproduk_produk_dkolega', type: 'float', mapping: 'produk_dkolega'},
			{name: 'dproduk_produk_dkeluarga', type: 'float', mapping: 'produk_dkeluarga'},
			{name: 'dproduk_produk_downer', type: 'float', mapping: 'produk_downer'},
			{name: 'dproduk_produk_dgrooming', type: 'float', mapping: 'produk_dgrooming'},
			{name: 'dproduk_produk_dwartawan', type: 'float', mapping: 'produk_dwartawan'},
			{name: 'dproduk_produk_dstaffdokter', type: 'float', mapping: 'produk_dstaffdokter'},
			{name: 'dproduk_produk_dstaffnondokter', type: 'float', mapping: 'produk_dstaffnondokter'},
			{name: 'dproduk_produk_dpromo', type: 'float', mapping: 'produk_dpromo'},
			{name: 'dproduk_produk_dpromo2', type: 'float', mapping: 'produk_dpromo2'},
			{name: 'dproduk_produk_dcompliment', type: 'float', mapping: 'produk_dcompliment'},
			{name: 'dproduk_produk_display', type: 'string', mapping: 'produk_nama'},
			{name: 'dproduk_edit_harga', type: 'float', mapping: 'produk_edit_harga'},
			{name: 'djproduk_satuan_nilai', type: 'float', mapping: 'djkonversi_nilai'},
			{name: 'djproduk_satuan_value', type: 'int', mapping: 'djkonversi_satuan'}			
		]),
		sortInfo:{field: 'dproduk_produk_display', direction: "ASC"}
	});
	
	cbo_dproduk_reveralDataStore = new Ext.data.Store({
		id: 'cbo_dproduk_reveralDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_reveral_list', 
			method: 'POST'
		}),baseParams: {start: 0, limit: 100 },
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total'
		},[
			{name: 'karyawan_display', type: 'string', mapping: 'karyawan_nama'},
			{name: 'karyawan_no', type: 'string', mapping: 'karyawan_no'},
			{name: 'karyawan_username', type: 'string', mapping: 'karyawan_username'},
			{name: 'karyawan_value', type: 'int', mapping: 'karyawan_id'}
		]),
		sortInfo:{field: 'karyawan_no', direction: "ASC"}
	});
	var reveral_tpl = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
            '<span><b>{karyawan_display}</b> ({karyawan_username})</span>',
        '</div></tpl>'
    );
	
	cbo_dproduk_satuanDataStore = new Ext.data.Store({
		id: 'cbo_dproduk_satuanDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_satuan_bydjproduk_list2', 
			method: 'POST'
		}),baseParams: {start: 0, limit: 15 },
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'satuan_id'
		},[
			{name: 'djproduk_satuan_value', type: 'int', mapping: 'satuan_id'},
			{name: 'djproduk_satuan_nama', type: 'string', mapping: 'satuan_nama'},
			{name: 'djproduk_satuan_kode', type: 'string', mapping: 'satuan_kode'},
			{name: 'djproduk_satuan_nilai', type: 'float', mapping: 'konversi_nilai'},
			{name: 'djproduk_satuan_display', type: 'string', mapping: 'satuan_kode'},
			{name: 'djproduk_satuan_default', type: 'string', mapping: 'konversi_default'},
			{name: 'djproduk_satuan_harga', type: 'float', mapping: 'produk_harga'}
		]),
		sortInfo:{field: 'djproduk_satuan_default', direction: "DESC"}
	});
	
	
	cbo_dproduk_satuan_tbarDataStore = new Ext.data.Store({
		id: 'cbo_dproduk_satuan_tbarDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_satuan_bydjproduk_list', 
			method: 'POST'
		}),baseParams: {start: 0, limit: 15 },
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'satuan_id'
		},[
			{name: 'djproduk_satuan_value', type: 'int', mapping: 'satuan_id'},
			{name: 'djproduk_satuan_nama', type: 'string', mapping: 'satuan_nama'},
			{name: 'djproduk_satuan_kode', type: 'string', mapping: 'satuan_kode'},
			{name: 'djproduk_satuan_nilai', type: 'float', mapping: 'konversi_nilai'},
			{name: 'djproduk_satuan_display', type: 'string', mapping: 'satuan_kode'},
			{name: 'djproduk_satuan_default', type: 'string', mapping: 'konversi_default'},
			{name: 'djproduk_satuan_harga', type: 'float', mapping: 'produk_harga'}
		]),
		sortInfo:{field: 'djproduk_satuan_default', direction: "DESC"}
	});
	
	var produk_jual_produk_tpl2 = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
            '<span>({djproduk_satuan_kode})<br><b>{djproduk_satuan_display}</b>',
		'</div></tpl>'
    );
	
	memberDataStore = new Ext.data.Store({
		id: 'memberDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_member_by_cust', 
			method: 'POST'
		}),
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'member_id'
		},[
			{name: 'cust_no', type: 'string', mapping: 'cust_no'},
			{name: 'cust_member_level', type: 'int', mapping: 'cust_member_level'},
			{name: 'member_id', type: 'int', mapping: 'member_id'},
			{name: 'cust_preward_total', type: 'float', mapping: 'cust_preward_total'},
			{name: 'cust_member_no', type: 'int', mapping: 'cust_member_no'},
			{name: 'member_no', type: 'string', mapping: 'member_no'},
			{name: 'member_valid', type: 'date', dateFormat: 'Y-m-d', mapping: 'member_valid'}, 
			{name: 'member_point' , type: 'int', mapping: 'member_point'},
			{name: 'member_jenis' , type: 'string', mapping: 'member_jenis'},
			{name: 'member_aktif' , type: 'string', mapping: 'member_aktif'},
			{name: 'cust_tgllahir' , type: 'date', dateFormat: 'Y-m-d', mapping: 'cust_tgllahir'},
			{name: 'cust_owner' , type: 'string', mapping: 'cust_owner'},
			{name: 'tahun_ambil_disk_ultah_ft' , type: 'int', mapping: 'tahun_ambil_disk_ultah_ft'},		
			{name: 'tgl_ambil_disk_ultah_ft' , type: 'date', dateFormat: 'Y-m-d', mapping: 'tgl_ambil_disk_ultah_ft'},				
			{name: 'flag', type: 'int', mapping: 'flag'} //berisi 0 jika selama range hari ultah + 14 blum ada pembelian produk dengan diskon ultah, berisi 1 jika sudah ada			
		]),
		sortInfo:{field: 'member_id', direction: "ASC"}
	});
	
	karyawanDataStore = new Ext.data.Store({
		id: 'karyawanDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_nik', 
			method: 'POST'
		}),
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'karyawan_id'
		},[
			{name: 'karyawan_id', type: 'int', mapping: 'karyawan_id'},
			{name: 'karyawan_no', type: 'string', mapping: 'karyawan_no'},
			{name: 'karyawan_jabatan', type: 'int', mapping: 'karyawan_jabatan'},
			{name: 'karyawan_idlevel', type: 'int', mapping: 'karyawan_idlevel'}
		]),
		sortInfo:{field: 'karyawan_id', direction: "ASC"}
	});
	
	var combo_jual_produk=new Ext.form.ComboBox({
		store: cbo_dproduk_produkDataStore,
		mode: 'remote',
		displayField: 'dproduk_produk_display',
		valueField: 'dproduk_produk_value',
		typeAhead: false,
		loadingText: 'Searching...',
		pageSize:jproduk_pageS,
		hideTrigger:false,
		tpl: produk_jual_produk_tpl,
		itemSelector: 'div.search-item',
		triggerAction: 'all',
		lazyRender:true,
		enableKeyEvents: true,
		readOnly: true,
		editable: false,
		listClass: 'x-combo-list-small',
		anchor: '95%'
	});
	
	var combo_reveral=new Ext.form.ComboBox({
		store: cbo_dproduk_reveralDataStore,
		mode: 'remote',
		displayField: 'karyawan_display',
		valueField: 'karyawan_value',
		typeAhead: false,
		loadingText: 'Searching...',
		pageSize:jproduk_pageS,
		hideTrigger:false,
		tpl: reveral_tpl,
		itemSelector: 'div.search-item',
		triggerAction: 'all',
		lazyRender:true,
		listClass: 'x-combo-list-small',
		anchor: '95%'
	});
	
	var combo_satuan_produk=new Ext.form.ComboBox({
		store: cbo_dproduk_satuanDataStore,
		mode:'local',
		typeAhead: true,
		displayField: 'djproduk_satuan_display',
		valueField: 'djproduk_satuan_value',
		triggerAction: 'all',
		allowBlank : true,
		anchor: '95%'
	});
	
	dproduk_idField=new Ext.form.NumberField();
	stok_realField=new Ext.form.NumberField();
	stok_minField=new Ext.form.NumberField();
	djproduk_satuan_nilaiField=new Ext.form.NumberField();	

	combo_jual_produk.on('select',function(){
		var j=cbo_dproduk_produkDataStore.findExact('dproduk_produk_value',combo_jual_produk.getValue(),0);
		var insert;				
		dproduk_idField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_value);				
		var produk_id = dproduk_idField.getValue();
		
		//Untuk me-lock screen sementara, menunggu data selesai di-load ==> setelah selesai di-load, hide Ext.MessageBox.show() di bawah ini
		detail_jual_produkListEditorGrid.setDisabled(true);
		editor_detail_jual_produk.disable();
		
		//cek stok yang dapat dijual saat produk dipilih (stok akhir - stok minimal)
		Ext.Ajax.request({
			url: 'index.php?c=c_master_jual_produk&m=cek_sisa_stok',
			params: {produk_id:produk_id},
			timeout: 60000,
			success: function(response){
			var result=eval(response.responseText);
				stok_realField.setValue(result.stok_akhir);
				stok_minField.setValue(result.stok_min);
				//REMARK untuk setiap klinik kecuali Malang, karena baru Malang yang menggunakan stok minimal
				
				if(stok_minField.getValue() !== -1){
					if(stok_realField.getValue()-1 <= stok_minField.getValue()){					
						Ext.MessageBox.confirm('Confirmation', 'Sisa stok akan mencapai batas minimum.<br>(Stok akhir saat ini: '+stok_realField.getValue()+')<br><br>Anda yakin untuk melanjutkan?', function(btn){
							  if (btn == 'no'){
								djumlah_beli_produkField.setValue('0');
							  }
							});
					}
				}
				cek_saldo(produk_id, 'yes');
			},
			failure: function(response){
				detail_jual_produkListEditorGrid.setDisabled(false);
				editor_detail_jual_produk.enable();			
			}
		});			
			
		function cek_saldo(produk_id, btn){
			if(btn=='yes'){
				var editable_harga_produk = 0;
				/*Di cek, apakah produk_edit_harga dari produk ini 0 / 1, jika 1 maka bisa di edit2 harganya, jika 0, maka ga bisa di edit2 harganya */
				editable_harga_produk = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_edit_harga;
				if(editable_harga_produk == 1){
					dharga_konversiField.setDisabled(false);
					dharga_konversiField.setReadOnly(false);
				}
				else if(editable_harga_produk == 0){
					dharga_konversiField.setDisabled(true);
					dharga_konversiField.setReadOnly(true);	
				}
	
				dharga_defaultField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_harga);
				var djumlah_diskon = 0;
				
				//(jproduk_cust_ultahField.getValue()!== null) || (jproduk_cust_ultahField.getValue() !=="") || (jproduk_cust_ultahField.getValue().format('Y-m-d')!=='0000-00-00') || (jproduk_cust_ultahField.getValue().format('Y-m-d')!=="")

				/*if(jproduk_cust_ultahField.getValue()!==""){
					//	var post_ultah_produk = jproduk_cust_ultahField.getValue().add(Date.DAY, +14).format('m-d'); //sebelumnya +7
					//	var pre_ultah_produk = jproduk_cust_ultahField.getValue().add(Date.DAY, +0).format('m-d'); //sebelumnya -7
						var bulan_ultah_produk = jproduk_cust_ultahField.getValue().format('m');
					//	today_ultah_produk = new Date().format('m-d');
						var thismonth_ultah_produk = new Date().format('m');
					
				}*/

				//* Check no_member JIKA <>"" ==> jenis-diskon=DM /
							/*
				if(pre_ultah_produk <= today_ultah_produk && post_ultah_produk >= today_ultah_produk) //Check jika customer ultah H-7 dan H+7 maka akan mendapatkan diskon ultah 100%
				{
						djenis_diskonField.setValue('Ultah');
						djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dultah;
						djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dultah);
						djumlah_diskonField.setDisabled(true);
				}
				*/
				//if(jproduk_cust_nomemberField.getValue()!=="" && today_cekvalid <= jproduk_valid_memberField.getValue().format('Y-m-d')){					    
				//if(jproduk_cust_nomemberField.getEl().dom.innerHTML !== undefined && today_cekvalid <= jproduk_valid_memberField.getValue().format('Y-m-d')){					    
				/*if(jproduk_cust_nomemberField.getEl().dom.innerHTML !== undefined && jproduk_cust_nomemberField.getEl().dom.innerHTML !== ""){
					if (jproduk_custField.getValue()=='9'){
						if(cabang == 'ESC' || cabang == 'ESCL' || cabang == 'PUR'){
							djenis_diskonField.setValue('Grooming');
							djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dgrooming
							djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dgrooming);
						}else{
							djenis_diskonField.setValue('Tanpa Diskon');
							djumlah_diskonField.setValue(0);
							djumlah_diskonField.setDisabled(true);	
						}
						//djenis_diskonField.setValue('Grooming');
						//djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dgrooming
						//djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dgrooming);	
					}else{
						//if((pre_ultah_produk <= today_ultah_produk && post_ultah_produk >= today_ultah_produk && jproduk_tahun_ambil_disk_ultah.getValue() < this_year)){
						if(((bulan_ultah_produk == thismonth_ultah_produk && typeof(bulan_ultah_produk) !== "undefined") && jproduk_tahun_ambil_disk_ultah.getValue() < this_year)){
							djenis_diskonField.setValue('Ultah');
							djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dultah;
							djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dultah);
							djumlah_diskonField.setDisabled(true);
						}
						else{
							if(t_member_level > 1) {
								djenis_diskonField.setValue('Member 2');
								djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dm2;
								djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dm2);
							} else {
								if(jproduk_ownerField.getValue()!=='F'){
									djenis_diskonField.setValue('Owner');
									djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_downer;
									djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_downer);
								}else{
									djenis_diskonField.setValue('Member');
									djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dm;
									djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dm);
								}
							}
							
							djumlah_diskonField.setDisabled(true);
						}
					}
				}else if(jproduk_ownerField.getValue()!=='F'){
					//if(pre_ultah_produk <= today_ultah_produk && post_ultah_produk >= today_ultah_produk && jproduk_tahun_ambil_disk_ultah.getValue() < this_year){
					if((bulan_ultah_produk == thismonth_ultah_produk && typeof(bulan_ultah_produk) !== "undefined") && jproduk_tahun_ambil_disk_ultah.getValue() < this_year){
						djenis_diskonField.setValue('Ultah');
						djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dultah;
						djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dultah);
						djumlah_diskonField.setDisabled(true);
					}
					else{
						djenis_diskonField.setValue('Owner');
						djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_downer;
						djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_downer);
						djumlah_diskonField.setDisabled(true);
					}
				}else if (jproduk_custField.getValue()=='9'){
					if(cabang == 'ESC' || cabang == 'ESCL' || cabang == 'PUR'){
						djenis_diskonField.setValue('Grooming');
						djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dgrooming
						djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dgrooming);
					}else{
						djenis_diskonField.setValue('Tanpa Diskon');
						djumlah_diskonField.setValue(0);
						djumlah_diskonField.setDisabled(true);	
					}
					//djenis_diskonField.setValue('Grooming');
					//djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dgrooming;
					//djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dgrooming);
				}else{*/
					djenis_diskonField.setValue('Umum');
					djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_du;
					djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_du);
					djumlah_diskonField.setDisabled(true);
			//	}
				
				djumlah_beli_produkField.setValue(1);
				var nilai_default=0;
				var dtotal_konversi_field = 0;
				var dsub_total_field = 0;
				var dtotal_net_field = 0;
				nilai_default=cbo_dproduk_produkDataStore.getAt(j).data.djproduk_satuan_nilai;
				//combo_satuan_produk.setValue(cbo_dproduk_produkDataStore.getAt(j).data.djproduk_satuan_value);
				
				if(nilai_default===1){
					dtotal_konversi_field = nilai_default*dharga_defaultField.getValue();
					dtotal_konversi_field = (dtotal_konversi_field>0?Math.round(dtotal_konversi_field):0);
					dharga_konversiField.setValue(dtotal_konversi_field);
					
					dsub_total_field = djumlah_beli_produkField.getValue()*dtotal_konversi_field;
					dsub_total_field = (dsub_total_field>0?Math.round(dsub_total_field):0);
					dsub_totalField.setValue(dsub_total_field);
					
					dtotal_net_field = ((100-djumlah_diskon)/100)*djumlah_beli_produkField.getValue()*(nilai_default*dharga_defaultField.getValue());
					dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
					dsub_total_netField.setValue(dtotal_net_field);
					detail_jual_produkListEditorGrid.setDisabled(false);
					editor_detail_jual_produk.enable();
					
				}else {
					dtotal_konversi_field = (nilai_default*(1/nilai_default))*dharga_defaultField.getValue();
					dtotal_konversi_field = (dtotal_konversi_field>0?Math.round(dtotal_konversi_field):0);
					dharga_konversiField.setValue(dtotal_konversi_field);
					
					dsub_total_field = djumlah_beli_produkField.getValue()*dtotal_konversi_field;
					dsub_total_field = (dsub_total_field>0?Math.round(dsub_total_field):0);
					dsub_totalField.setValue(dsub_total_field);
					
					dtotal_net_field = ((100-djumlah_diskon)/100)*djumlah_beli_produkField.getValue()*((nilai_default*(1/nilai_default))*dharga_defaultField.getValue());
					dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
					dsub_total_netField.setValue(dtotal_net_field);
					detail_jual_produkListEditorGrid.setDisabled(false);
					editor_detail_jual_produk.enable();
				}
				
			/*	cbo_dproduk_satuanDataStore.load({
					params: {produk_id:dproduk_idField.getValue()},
					callback: function(opts, success, response){
						if(success){
							var st=cbo_dproduk_satuanDataStore.findExact('djproduk_satuan_default','true',0);
							if(cbo_dproduk_satuanDataStore.getCount()>=0){
								combo_satuan_produk.setValue(cbo_dproduk_satuanDataStore.getAt(st).data.djproduk_satuan_value);
							}
						}
					}
				}); */
			
				window.refresh(); //Fungsi ini adalah memancing error, dimana tujuannya agar harga dan satuan pada detail muncul. Setelah muncul error ini, maka akan ada console.clear() agar tidak membingungkan user.. 
			}else{
				combo_jual_produk.setValue('');
				dsub_totalField.setValue('0');
				dsub_total_netField.setValue('0');
				combo_satuan_produk.setValue('');
				dharga_konversiField.setValue('0');
				djumlah_diskonField.setValue('0');
				djenis_diskonField.setValue('');
				dharga_defaultField.setValue('');
			}
		}
	});
	
	function pilih_produk(){
		Ext.MessageBox.show({
			msg:   'Sedang memuat data, mohon tunggu...',
			progressText: 'proses...',
			width:300,
			wait:true
		});
		
		//ambil data dari lokal
		Ext.Ajax.request({
			waitMsg: 'Please wait...',
			url: 'index.php?c=c_master_jual_produk&m=get_produk_list_barcode',
			params:{
				member_no: produk_barcodeField.getValue()
			},
			success: function(response){
				var result=eval(response.responseText);
				var index=result.results.length-1;
				/*if(index >= 0){
					mcard_cust_idField.setValue(result.results[index].cust_id);
					mcard_namaField.setValue(result.results[index].cust_nama);
					mcard_alamatField.setValue(result.results[index].cust_alamat);
					mcard_kotaField.setValue(result.results[index].cust_kota);
					mcard_tgl_lahirField.setValue(result.results[index].cust_tgllahir);
					nilai_poin_tukar_rupiah_cfField.setValue(0);
				}else{
					mcard_cust_idField.setValue('');
					mcard_namaField.setValue('');
					mcard_alamatField.setValue('');
					mcard_kotaField.setValue('');
					mcard_tgl_lahirField.setValue('');
					nilai_poin_tukar_rupiah_cfField.setValue(0);
				}*/
				//eof
				var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
					dproduk_id	:0,
					dproduk_produk	:result.results[index].produk_nama,
					dproduk_satuan	:result.results[index].djkonversi_satuan,
					dproduk_jumlah	:1,
					dproduk_harga	:result.results[index].produk_harga,
					dproduk_subtotal:result.results[index].produk_harga,
					//dproduk_diskon_jenis: result.results[index].produk_id,
					dproduk_diskon	:0,
					dproduk_subtotal_net:result.results[index].produk_harga,
					dproduk_karyawan:0,
					produk_harga_default:result.results[index].produk_harga
				});
				
				cbo_dproduk_satuanDataStore.setBaseParam('produk_id', result.results[index].produk_id);
				cbo_dproduk_satuanDataStore.load({
					callback: function(r,opt,success){
						if(success==true){
							detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
							combo_jual_produk.setValue(result.results[index].produk_id);
							detail_jual_produk_DataStore.commitChanges();
						}
					}
				});
				
				produk_barcodeField.focus(true,500);
				Ext.MessageBox.hide();				
				
			},
			failure: function(response){
				var result=response.responseText;
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: 'Tidak dapat terhubung dengan database server',
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});		
			}
		});
	}

	temp_konv_nilai=new Ext.form.NumberField({
		readOnly: true,
		allowDecimals: true,
		decimalPrecision: 9
	});

	combo_satuan_produk.on('focus', function(){
		cbo_dproduk_satuanDataStore.setBaseParam('produk_id',combo_jual_produk.getValue());
		cbo_dproduk_satuanDataStore.load();
	});
	combo_satuan_produk.on('select', function(){
		var j=cbo_dproduk_satuanDataStore.findExact('djproduk_satuan_value',combo_satuan_produk.getValue(),0);
		var jt=cbo_dproduk_satuanDataStore.findExact('djproduk_satuan_default','true',0);
		var nilai_terpilih=0;
		var nilai_default=0;
		var dtotal_konversi_field = 0;
		var dsub_total_field = 0;
		var dtotal_net_field = 0;
		
		if(cbo_dproduk_satuanDataStore.getCount()>=0){
			if(cbo_dproduk_satuanDataStore.getAt(j).data.djproduk_satuan_default=="true"){
				//Harga_Produk=harga yg tercantum di Master Produk tanpa proses bagi/kali
				djproduk_satuan_nilaiField.setValue(1);
				
				dtotal_konversi_field = 1*dharga_defaultField.getValue();
				dtotal_konversi_field = (dtotal_konversi_field>0?Math.round(dtotal_konversi_field):0);
				dharga_konversiField.setValue(dtotal_konversi_field);
				
				dsub_total_field = djumlah_beli_produkField.getValue()*(1*dharga_defaultField.getValue());
				dsub_total_field = (dsub_total_field>0?Math.round(dsub_total_field):0);
				dsub_totalField.setValue(dsub_total_field);
				
				dtotal_net_field = ((100-djumlah_diskonField.getValue())/100)*djumlah_beli_produkField.getValue()*(1*dharga_defaultField.getValue());
				dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
				dsub_total_netField.setValue(dtotal_net_field);
			}else if(cbo_dproduk_satuanDataStore.getAt(j).data.djproduk_satuan_default=="false"){
				//ambil satuan_nilai dr satuan_id yg terpilih, ambil satuan_nilai dr satuan_default=true
				//jika [satuan_nilai dr satuan_default=true] === 1 => Harga_Produk=[satuan_nilai dr satuan_id yg terpilih]*data.djproduk_satuan_harga
				//jika [satuan_nilai dr satuan_default=true] !== 1 AND [satuan_nilai dr satuan_default=true] < [satuan_nilai dr satuan_id yg terpilih] => Harga_Produk=([satuan_nilai dr satuan_id yg terpilih]/[satuan_nilai dr satuan_default=true])*data.djproduk_satuan_harga 
				//jika [satuan_nilai dr satuan_default=true] !== 1 AND [satuan_nilai dr satuan_default=true] > [satuan_nilai dr satuan_id yg terpilih] => Harga_Produk=data.djproduk_satuan_harga/[satuan_nilai dr satuan_default=true]
				nilai_terpilih=cbo_dproduk_satuanDataStore.getAt(j).data.djproduk_satuan_nilai;
				nilai_default=cbo_dproduk_satuanDataStore.getAt(jt).data.djproduk_satuan_nilai;
				if(nilai_default===1){
					djproduk_satuan_nilaiField.setValue(cbo_dproduk_satuanDataStore.getAt(j).data.djproduk_satuan_nilai);
					
					dtotal_konversi_field = cbo_dproduk_satuanDataStore.getAt(j).data.djproduk_satuan_nilai*dharga_defaultField.getValue();
					dtotal_konversi_field = (dtotal_konversi_field>0?Math.round(dtotal_konversi_field):0);
					dharga_konversiField.setValue(dtotal_konversi_field);
					
					dsub_total_field = djumlah_beli_produkField.getValue()*(cbo_dproduk_satuanDataStore.getAt(j).data.djproduk_satuan_nilai*dharga_defaultField.getValue());
					dsub_total_field = (dsub_total_field>0?Math.round(dsub_total_field):0);
					dsub_totalField.setValue(dsub_total_field);
					
					dtotal_net_field = ((100-djumlah_diskonField.getValue())/100)*djumlah_beli_produkField.getValue()*(cbo_dproduk_satuanDataStore.getAt(j).data.djproduk_satuan_nilai*dharga_defaultField.getValue());
					dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
					dsub_total_netField.setValue(dtotal_net_field);
				}else if(nilai_default!==1 && nilai_default<nilai_terpilih){
					djproduk_satuan_nilaiField.setValue(nilai_terpilih/nilai_default);
					
					dtotal_konversi_field = (nilai_terpilih/nilai_default)*dharga_defaultField.getValue();
					dtotal_konversi_field = (dtotal_konversi_field>0?Math.round(dtotal_konversi_field):0);
					dharga_konversiField.setValue(dtotal_konversi_field);
					
					dsub_total_field = djumlah_beli_produkField.getValue()*((nilai_terpilih/nilai_default)*dharga_defaultField.getValue());
					dsub_total_field = (dsub_total_field>0?Math.round(dsub_total_field):0);
					dsub_totalField.setValue(dsub_total_field);
					
					dtotal_net_field = ((100-djumlah_diskonField.getValue())/100)*djumlah_beli_produkField.getValue()*((nilai_terpilih/nilai_default)*dharga_defaultField.getValue());
					dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
					dsub_total_netField.setValue(dtotal_net_field);
				}else if(nilai_default!==1 && nilai_default>nilai_terpilih){
					djproduk_satuan_nilaiField.setValue(1/nilai_default);
					
					dtotal_konversi_field = (1/nilai_default)*dharga_defaultField.getValue();
					dtotal_konversi_field = (dtotal_konversi_field>0?Math.round(dtotal_konversi_field):0);
					dharga_konversiField.setValue(dtotal_konversi_field);
					
					dsub_total_field = djumlah_beli_produkField.getValue()*((1/nilai_default)*dharga_defaultField.getValue());
					dsub_total_field = (dsub_total_field>0?Math.round(dsub_total_field):0);
					dsub_totalField.setValue(dsub_total_field);
					
					dtotal_net_field = ((100-djumlah_diskonField.getValue())/100)*djumlah_beli_produkField.getValue()*((1/nilai_default)*dharga_defaultField.getValue());
					dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
					dsub_total_netField.setValue(dtotal_net_field);
				}
			}
		}
	});
	
	var djenis_diskonField = new Ext.form.ComboBox({
		store:new Ext.data.SimpleStore({
			fields:['diskon_jenis_value'],
			data:[['Tanpa Diskon'],['Member'],['Promo']/*,['Member 2'],['Ultah'],['Card'],['Kolega'],['Owner'],['Grooming'],['Umum'],['Promo 2']*/,['Free']]
		}),
		mode: 'local',
		displayField: 'diskon_jenis_value',
		valueField: 'diskon_jenis_value',
		allowBlank: true,
		anchor: '50%',
		triggerAction: 'all',
		lazyRenderer: true
	});
	djenis_diskonField.on('select', function(){
		var dtotal_net_field = 0;
		var djumlah_beli_produk = djumlah_beli_produkField.getValue();
		var j=cbo_dproduk_produkDataStore.findExact('dproduk_produk_value',combo_jual_produk.getValue(),0);
		var djenis_diskon = 0;		
		
		if(djenis_diskonField.getValue()=='Umum'){
			djenis_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_du;
			djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_du);
			djumlah_diskonField.setReadOnly(true);
			djumlah_diskonField.setDisabled(true); //default
		}else if(djenis_diskonField.getValue()=='Card'){
			djenis_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dcard;
			djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dcard);
			djumlah_diskonField.setDisabled(false);
		}else if(djenis_diskonField.getValue()=='Promo'){
			djenis_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dpromo;
			djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dpromo);
			djumlah_diskonField.setReadOnly(true);
			djumlah_diskonField.setDisabled(true); //default				
		}else if(djenis_diskonField.getValue()=='Promo 2'){
			djenis_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dpromo2;
			djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dpromo2);
			djumlah_diskonField.setReadOnly(true);
			djumlah_diskonField.setDisabled(true); //default				
		}else if(djenis_diskonField.getValue()=='Compliment'){
			djenis_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dcompliment;
			djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dcompliment);
			djumlah_diskonField.setReadOnly(true);
			djumlah_diskonField.setDisabled(true); //default
			/*untuk YESS*///djumlah_diskonField.setDisabled(false); /*eof YESS*/ 
		}
		else{
			djumlah_diskonField.setValue(0);
			djumlah_diskonField.setDisabled(true);
		}
		
		dtotal_net_field = ((100-djenis_diskon)/100) * (djumlah_beli_produk*cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_harga);
		dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
		dsub_total_netField.setValue(dtotal_net_field);
	});
	
	var djumlah_diskonField = new Ext.form.NumberField({
		id : 'djumlah_diskonField',
		name : 'djumlah_diskonField',
		allowDecimals: true,
		allowNegative: false,
		maxLength: 5,
		enableKeyEvents: true,
		readOnly : true,
		maskRe: /([0-9]+)$/
	});
	
	djumlah_diskonField.on('keyup', function(){
		var sub_total_net = ((100-djumlah_diskonField.getValue())/100)*dsub_totalField.getValue();
		sub_total_net = (sub_total_net>0?Math.round(sub_total_net):0);
		dsub_total_netField.setValue(sub_total_net);
	});
	
	var djumlah_beli_produkField = new Ext.form.NumberField({
		allowDecimals: false,
		allowNegative: false,
		maxLength: 11,
		enableKeyEvents: true,
		maskRe: /([0-9]+)$/
	});
	djumlah_beli_produkField.on('keyup', function(){
		var dtotal_net_field = 0;
		var sub_total = djumlah_beli_produkField.getValue()*dharga_konversiField.getValue();
		
		//REMARK untuk setiap klinik kecuali Malang, karena baru Malang yang menggunakan stok minimal
		if(stok_minField.getValue() !== -1){
			if((stok_realField.getValue()- djumlah_beli_produkField.getValue())-1 < stok_minField.getValue()){
				Ext.MessageBox.confirm('Confirmation', 'Sisa stok akan mencapai batas minimum.<br>(Stok akhir saat ini: '+stok_realField.getValue()+')<br><br>Anda yakin untuk melanjutkan?', function(btn){
					  if (btn == 'no'){
						djumlah_beli_produkField.setValue('0');
					  }
					});
			}
		}
		dsub_totalField.setValue(sub_total);
		dtotal_net_field = ((100-djumlah_diskonField.getValue())/100)*sub_total;
		dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
		dsub_total_netField.setValue(dtotal_net_field);
	});
	
	var dharga_konversiField = new Ext.form.NumberField({
		allowDecimals: false,
		allowNegative: false,
		maxLength: 11,
		readOnly: true,
		maskRe: /([0-9]+)$/
	});
	
	var dharga_defaultField = new Ext.form.NumberField({
		allowDecimals: false,
		allowNegative: false,
		maxLength: 11,
		readOnly: true,
		maskRe: /([0-9]+)$/
	});
	
	var dsub_totalField = new Ext.form.NumberField({
		allowDecimals: false,
		allowNegative: false,
		maxLength: 11,
		readOnly: true,
		editable: false,
		maskRe: /([0-9]+)$/
	});
	
	var dsub_total_netField = new Ext.form.NumberField({
		allowDecimals: false,
		allowNegative: false,
		maxLength: 11,
		readOnly: true,
		disabled: true,
		maskRe: /([0-9]+)$/
	});

	dpp_Field=new Ext.form.NumberField();
	subtot_dpp_Field=new Ext.form.NumberField();
	
	detail_jual_produk_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'Left',
			header: 'ID',
			dataIndex: 'dproduk_id',
			hidden: true
		},
		{
			align : 'Left',
			header: '<div align="center">' + 'Produk' + '</div>',
			dataIndex: 'dproduk_produk',
			width: 250,
			sortable: false,
			allowBlank: false,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			editor: combo_jual_produk,
			<?php } ?>
			renderer: Ext.util.Format.comboRenderer(combo_jual_produk)
		},
		{
			align :'Left',
			header: '<div align="center">' + 'Satuan' + '</div>',
			dataIndex: 'dproduk_satuan',
			width: 50,
			sortable: false,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			editor: combo_satuan_produk,
			<?php } ?>
			renderer: Ext.util.Format.comboRenderer(combo_satuan_produk)
		},
		{
			align : 'Right',
			header: '<div align="center">' + 'Jml' + '</div>',
			dataIndex: 'dproduk_jumlah',
			width: 40,
			sortable: false,
			renderer: Ext.util.Format.numberRenderer('0,000')
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			,
			editor: djumlah_beli_produkField
			<?php } ?>
		},
		{
			align : 'Right',
			header: '<div align="center">' + 'Harga' + '</div>',
			dataIndex: 'dproduk_harga',
			width: 70,
			sortable: false,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			editor: dharga_konversiField,
			<?php } ?>
			renderer: Ext.util.Format.numberRenderer('0,000')
		},
		/*{
			align: 'Right',
			header: '<div align="center">' + 'DPP (Rp)' + '</div>',
			dataIndex: 'dproduk_harga',
			width: 80,
			sortable: true,
			renderer: function(v, params, record){
				var record_dpp = record.data.dproduk_harga/1.1;
				record_dpp = (record_dpp>0?Math.round(record_dpp):0);
				return Ext.util.Format.number(record_dpp,'0,000');
			}
		},*/
		{
			align : 'Right',
			header: '<div align="center">' + 'Sub Total' + '</div>',
			dataIndex: 'dproduk_subtotal',
			width: 70,
			sortable: false,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			editor: dsub_totalField,
			<?php } ?>
			renderer: function(v, params, record){
				return Ext.util.Format.number(record.data.dproduk_jumlah*record.data.dproduk_harga,'0,000');
			}
		},
		{
			align : 'Left',
			header: '<div align="center">' + 'Jns Disk' + '</div>',
			dataIndex: 'dproduk_diskon_jenis',
			width: 70,
			sortable: false
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			,
			editor: djenis_diskonField
			<?php } ?>
		},
		{
			align : 'Right',
			header: '<div align="center">' + 'Disk (%)' + '</div>',
			dataIndex: 'dproduk_diskon',
			width: 50,
			sortable: false,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			editor: djumlah_diskonField
			<?php } ?>
		},
		{
			align :'Right',
			header: '<div align="center">' + 'Total' + '</div>',
			dataIndex: 'dproduk_subtotal_net',
			width: 70,
			sortable: false,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			editor: dsub_total_netField,
			<?php } ?>
			renderer: function(v, params, record){
				var record_dtotal_net = record.data.dproduk_jumlah*record.data.dproduk_harga*((100-record.data.dproduk_diskon)/100);
				record_dtotal_net = (record_dtotal_net>0?Math.round(record_dtotal_net):0);
				return Ext.util.Format.number(record_dtotal_net,'0,000');
			}
		},
		{
			xtype:'actioncolumn',
			width:50,
			hidden:true,
			items: [{
				iconCls: 'icon-delete',
				tooltip: 'Hapus',
				handler: function(grid, rowIndex, colIndex) {
					grid.getStore().removeAt(rowIndex);
				},
				scope: this
			}]
		},
		/*{
			align: 'Right',
			header: '<div align="center">' + 'Sub Tot Net DPP (Rp)' + '</div>',
			dataIndex: 'dproduk_subtotal_net',
			width: 120,
			sortable: true,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALRAWAT'))){ ?>
			editor: subtot_dpp_Field,
			<?php } ?>
			renderer: function(v, params, record){
				var record_subtot_dpp = (record.data.dproduk_jumlah*record.data.dproduk_harga*((100-record.data.dproduk_diskon)/100))/1.1;
				record_subtot_dpp = (record_subtot_dpp>0?Math.round(record_subtot_dpp):0);
				return Ext.util.Format.number(record_subtot_dpp,'0,000');
			}
		},
		{
			header: '<div align="center">' + 'Keterangan' + '</div>',
			dataIndex: 'dproduk_keterangan',
			width: 150,
			sortable: false
		},
		{
			align : 'Left',
			header: '<div align="center">' + 'Referal' + '</div>',
			dataIndex: 'dproduk_karyawan',
			width: 150,
			sortable: false,
			allowBlank: false,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			editor: combo_reveral,
			<?php } ?>
			renderer: Ext.util.Format.comboRenderer(combo_reveral)
		},*/
		{
			//field ini HARUS dimunculkan, utk penghitungan harga
			align : 'Right',
			//header: '<div align="center" style="color:white;">' + 'Harga Default' + '</div>',
			dataIndex: 'produk_harga_default',
			width: 0,
			sortable: false,
			hidden: false,
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			editor: dharga_defaultField,
			<?php } ?>
			renderer: Ext.util.Format.numberRenderer('0,000')
		}
		]
	);
	detail_jual_produk_ColumnModel.defaultSortable= true;

	detail_jual_produkListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'detail_jual_produkListEditorGrid',
		el: 'fp_detail_jual_produk',
		title: 'Detail Penjualan Produk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:green;"><b>INSERT</b></span> = input barang &nbsp;&nbsp; <span style="color:red;"><b>DELETE</b></span> = hapus barang',
		height: 250,
		width: 700,	//918,
		autoScroll: true,
		store: detail_jual_produk_DataStore, // DataStore
		colModel: detail_jual_produk_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		enableColumnMove:false,
		region: 'center',
		margins: '0 5 5 5',
		<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
		//plugins: [editor_detail_jual_produk],
		clicksToEdit:2, // 2xClick untuk bisa meng-Ubah inLine Data
		<?php } ?>
		frame: true,
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		viewConfig: { forceFit:false},
		bbar: new Ext.PagingToolbar({
			store: detail_jual_produk_DataStore,
			displayInfo: true
		})
		<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
		,
		/* Tambah Control on ToolBar */
		tbar: [
		{
			/*text: 'Tambah',
			tooltip: 'Tambah data baru',
			iconCls:'icon-adds',    				// this is defined in our styles.css
			ref : '../djproduk_add',
			handler: detail_jual_produk_add*/
			
			
			/*xtype:'textfield',
            emptyText: 'Cari',
            enableKeyEvents: true,
            listeners: {                   
                'keypress': function(field,event){
                    if (event.getKey() == event.ENTER){
                        alert("ok");
                    }
                }                                   
            }*/
			
			/*xtype     : 'combo',
			width     : 120,
			store     : [
				cbo_dproduk_produkDataStore
			]*/
			
			xtype:'combo',
			fieldLabel:'Produk',
			name:'produk_id_tbar',
			id:'produk_id_tbar',
			ref : '../djproduk_add_produk',
			queryMode:'remote',
			store:cbo_dproduk_produkDataStore,
			autoSelect:true,
			forceSelection:true,
			width: 250,
		   
			mode: 'remote',
			displayField: 'dproduk_produk_display',
			valueField: 'dproduk_produk_value',
			typeAhead: false,
			loadingText: 'Searching...',
			pageSize:jproduk_pageS,
			hideTrigger:false,
			tpl: produk_jual_produk_tpl,
			itemSelector: 'div.search-item',
			triggerAction: 'all',
			minChars: 999,
			lazyRender:true,
			enableKeyEvents: true,
			listClass: 'x-combo-list-small',
			anchor: '95%',
			listeners:{
				specialkey: function(combo, e){
					if (e.getKey() == e.ENTER) { //fungsi tombol ENTER di combo produk
						if ((combo.getRawValue()).length == 13 && isNumber(combo.getRawValue())) { //jika isian adalah kode barcode yang discan
							e.stopEvent();
							// Ext.MessageBox.show({
							// 	msg: 'Searching data, please wait...',
							// 	progressText: 'Searching...',
							// 	width:300,
							// 	wait:true,
							// 	waitConfig: {interval:200}
							// });

							combo.getStore().load({
								params: {query: combo.getRawValue()},
								scope: this,
								callback: function(records, operation, success){
									if (success) {
										// Ext.MessageBox.hide();
										combo.collapse();

										add_dproduk2grid(records[0].data);

										detail_jual_produk_reset_form();
									};
								}
							});
						} else { //jika isan adalah selain kode barcode
							e.stopEvent();
							// Ext.MessageBox.show({
							// 	msg: 'Searching data, please wait...',
							// 	progressText: 'Searching...',
							// 	width:300,
							// 	wait:true,
							// 	waitConfig: {interval:200}
							// });

							combo.getStore().load({
								params: {query: combo.getRawValue()},
								scope: this,
								callback: function(records, operation, success){
									if (success) {
										// Ext.MessageBox.hide();

										combo.expand();
										combo.focus(false, true);
									};
								}
							});
						};
						
					}
					
				},
				'keyup': function(combo, record){
					// detail_jual_produkListEditorGrid.djproduk_add_produk.reset();
					// detail_jual_produkListEditorGrid.djproduk_add_produk.setValue(null);
					if(this.getRawValue() == ""){
						this.setValue();
						Ext.getCmp('satuan_id_tbar').setValue();
						//alert("sini");
						//alert(this.getRawValue());
					}
				},
				'keypress': function(field,event){
					//alert(event.getKey());
                    if (event.getKey() == 45){ //tombol INSERT untuk INPUT produk ke list produk //tombol INSERT untuk INPUT produk ke list produk
                        //alert("ok");
						
						var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
							dproduk_id	:0,
							dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
							dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
							dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
							dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
							dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
							dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
							dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
							dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
							dproduk_karyawan:0,
							produk_harga_default:Ext.getCmp('harga_tbar').getValue()
						});
						
						cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
						cbo_dproduk_satuanDataStore.load({
							callback: function(r,opt,success){
								if(success==true){
									detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
									combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
									detail_jual_produk_DataStore.commitChanges();
								}
							}
						});
						
						detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
                    }
					
                },
				'select': function(combo, record) {
					//alert(record.data.djproduk_satuan_value);
					//cbo_dproduk_satuan_tbarDataStore.load();
					//Ext.getCmp('satuan_id').setValue(record.data.djproduk_satuan_value);
					//Ext.ComponentQuery.query('#satuan_id').setValue(record.data.djproduk_satuan_value);
					cbo_dproduk_satuan_tbarDataStore.baseParams = { produk_id: this.getValue() };
					cbo_dproduk_satuan_tbarDataStore.load({
						//params: {produk_id: this.getValue()},
						callback: function(r,opt,success){
							if(success==true){
								Ext.getCmp('satuan_id_tbar').setValue(record.data.djproduk_satuan_value);
							}
						}
					});					
					
					
					
					//cek stok yang dapat dijual saat produk dipilih (stok akhir - stok minimal)
					Ext.Ajax.request({
						url: 'index.php?c=c_master_jual_produk&m=cek_sisa_stok',
						params: {produk_id:this.getValue()},
						timeout: 60000,
						success: function(response){
						var result=eval(response.responseText);
							stok_realField.setValue(result.stok_akhir);
							stok_minField.setValue(result.stok_min);
							
							if(stok_minField.getValue() !== -1){
								if(stok_realField.getValue()-1 <= stok_minField.getValue()){					
									Ext.MessageBox.confirm('Confirmation', 'Sisa stok akan mencapai batas minimum.<br>(Stok akhir saat ini: '+stok_realField.getValue()+')<br><br>Anda yakin untuk melanjutkan?', function(btn){
										  if (btn == 'no'){
												Ext.getCmp('produk_id_tbar').setValue('');
												Ext.getCmp('satuan_id_tbar').setValue('');											
										  }
										});
								}
							}
							cek_saldo(Ext.getCmp('produk_id_tbar').getValue(), 'yes');
						},
						failure: function(response){
							//detail_jual_produkListEditorGrid.setDisabled(false);
							//editor_detail_jual_produk.enable();			
						}
					});			
					
					var konversi_field;
					function cek_saldo(produk_id, btn){
						if(btn=='yes'){
							/*var editable_harga_produk = 0;
							editable_harga_produk = cbo_dproduk_produkDataStore.getAt(0).data.dproduk_edit_harga;
							if(editable_harga_produk == 1){
								dharga_konversiField.setDisabled(false);
								dharga_konversiField.setReadOnly(false);
							}
							else if(editable_harga_produk == 0){
								dharga_konversiField.setDisabled(true);
								dharga_konversiField.setReadOnly(true);	
							}*/
				
							Ext.getCmp('harga_tbar').setValue(record.data.dproduk_produk_harga);
							var djumlah_diskon = 0;							
							
							if(jproduk_cust_nomemberField.getEl().dom.innerHTML!==""){
								Ext.getCmp('jdiskon_tbar').setValue('Member');
								Ext.getCmp('diskon_tbar').setValue(record.data.dproduk_produk_dm);
							}else{
								Ext.getCmp('jdiskon_tbar').setValue('Tanpa Diskon');
								Ext.getCmp('diskon_tbar').setValue('0');
							}
							
							//djumlah_diskon = record.data.dproduk_produk_du;
							//Ext.getCmp('diskon_tbar').setValue(record.data.dproduk_produk_du);
							//djumlah_diskonField.setDisabled(true);
							
							Ext.getCmp('jumlah_tbar').setValue(1);
							var nilai_default=0;
							var dtotal_konversi_field = 0;
							var dsub_total_field = 0;
							var dtotal_net_field = 0;
							nilai_default=record.data.djproduk_satuan_nilai;
							cbo_dproduk_satuan_tbarDataStore.load({
								callback: function(r,opt,success){
									if(success==true){
										Ext.getCmp('satuan_id_tbar').setValue(record.data.djproduk_satuan_value);
									}
								}
							});
							
							if(nilai_default===1){
								dtotal_konversi_field = nilai_default*parseFloat(Ext.getCmp('harga_tbar').getValue());
								dtotal_konversi_field = (dtotal_konversi_field>0?Math.round(dtotal_konversi_field):0);
								konversi_field=dtotal_konversi_field;
								
								
								dsub_total_field = parseInt(Ext.getCmp('jumlah_tbar').getValue())*dtotal_konversi_field;
								dsub_total_field = (dsub_total_field>0?Math.round(dsub_total_field):0);
								Ext.getCmp('subtotal_tbar').setValue(dsub_total_field);
								
								dtotal_net_field = ((100-djumlah_diskon)/100)*parseInt(Ext.getCmp('jumlah_tbar').getValue())*(nilai_default*parseFloat(Ext.getCmp('harga_tbar').getValue()));
								dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
								Ext.getCmp('subtotal_net_tbar').setValue(dtotal_net_field);
								//detail_jual_produkListEditorGrid.setDisabled(false);
								//editor_detail_jual_produk.enable();
								
							}else {
								dtotal_konversi_field = (nilai_default*(1/nilai_default))*parseFloat(Ext.getCmp('harga_tbar').getValue());
								dtotal_konversi_field = (dtotal_konversi_field>0?Math.round(dtotal_konversi_field):0);
								konversi_field=dtotal_konversi_field;
							
								dsub_total_field = parseInt(Ext.getCmp('jumlah_tbar').getValue())*dtotal_konversi_field;
								dsub_total_field = (dsub_total_field>0?Math.round(dsub_total_field):0);
								Ext.getCmp('subtotal_tbar').setValue(dsub_total_field);
								
								dtotal_net_field = ((100-djumlah_diskon)/100)*parseInt(Ext.getCmp('jumlah_tbar').getValue())*((nilai_default*(1/nilai_default))*parseFloat(Ext.getCmp('harga_tbar').getValue()));
								dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
								Ext.getCmp('subtotal_net_tbar').setValue(dtotal_net_field);
								//detail_jual_produkListEditorGrid.setDisabled(false);
								//editor_detail_jual_produk.enable();
							}
							
						
							window.refresh(); //Fungsi ini adalah memancing error, dimana tujuannya agar harga dan satuan pada detail muncul. Setelah muncul error ini, maka akan ada console.clear() agar tidak membingungkan user.. 
						}else{
							Ext.getCmp('satuan_id_tbar').setValue('');
							Ext.getCmp('subtotal_tbar').setValue('0');
							Ext.getCmp('subtotal_net_tbar').setValue('0');
							Ext.getCmp('satuan_id_tbar').setValue('');
							konversi_field.setValue('0');
							Ext.getCmp('diskon_tbar').setValue('0');
							Ext.getCmp('jdiskon_tbar').setValue('');
							Ext.getCmp('harga_tbar').setValue('');
						}
					}
					
					
				}
			}
			
		}, '-',{
			xtype:'combo',
			fieldLabel:'Satuan',
			name:'satuan_id_tbar',
			id:'satuan_id_tbar',
			ref : '../djproduk_add_satuan',
			itemId: 'satuan_id_tbar',
			
			store: cbo_dproduk_satuan_tbarDataStore,
			mode:'remote',
			typeAhead: true,
			displayField: 'djproduk_satuan_display',
			valueField: 'djproduk_satuan_value',
			allowBlank : true,
			width: 50,
		   
			loadingText: 'Searching...',
			pageSize:jproduk_pageS,
			hideTrigger:false,
			tpl: produk_jual_produk_tpl2,
			itemSelector: 'div.search-item',
			triggerAction: 'all',
			lazyRender:true,
			enableKeyEvents: true,
			listClass: 'x-combo-list-small',
			anchor: '95%',
			listeners:{
				'keypress': function(field,event){
                    if (event.getKey() == 45){ //tombol INSERT untuk INPUT produk ke list produk
                        //alert("ok");
						var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
							dproduk_id	:0,
							dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
							dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
							dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
							dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
							dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
							dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
							dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
							dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
							dproduk_karyawan:0,
							produk_harga_default:Ext.getCmp('harga_tbar').getValue()
						});
						
						cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
						cbo_dproduk_satuanDataStore.load({
							callback: function(r,opt,success){
								if(success==true){
									detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
									combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
									detail_jual_produk_DataStore.commitChanges();
								}
							}
						});
						
						detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
                    }
					
                },
				'select': function(combo, record) {
					//alert("okayy2");
				}
			}
			
		}, '-',{
			xtype:'textfield',
			fieldLabel:'Jumlah',
			name:'jumlah_tbar',
			id:'jumlah_tbar',
			ref : '../djproduk_add_jumlah',
			anchor: '95%',
			width: 30,
			value:0,
			enableKeyEvents: true,
			listeners:{
				'keypress': function(field,event){
                    if (event.getKey() == 45){ //tombol INSERT untuk INPUT produk ke list produk
                        //alert("ok");
						var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
							dproduk_id	:0,
							dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
							dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
							dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
							dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
							dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
							dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
							dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
							dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
							dproduk_karyawan:0,
							produk_harga_default:Ext.getCmp('harga_tbar').getValue()
						});
						
						cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
						cbo_dproduk_satuanDataStore.load({
							callback: function(r,opt,success){
								if(success==true){
									detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
									combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
									detail_jual_produk_DataStore.commitChanges();
								}
							}
						});
						
						detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
                    }
					
                },
				'change': function(combo, record) {
					//alert("okayy3");
					Ext.getCmp('subtotal_tbar').setValue(parseInt(Ext.getCmp('jumlah_tbar').getValue())*parseFloat(Ext.getCmp('harga_tbar').getValue()));
					var sub_total_net = ((100-parseInt(Ext.getCmp('diskon_tbar').getValue()))/100)*parseFloat(Ext.getCmp('subtotal_tbar').getValue());
					sub_total_net = (sub_total_net>0?Math.round(sub_total_net):0);
					Ext.getCmp('subtotal_net_tbar').setValue(sub_total_net);
				}
			}
		
		}, '-',{
			xtype:'textfield',
			fieldLabel:'Harga',
			name:'harga_tbar',
			id:'harga_tbar',
			ref : '../djproduk_add_harga',
			anchor: '95%',
			width: 50,
			enableKeyEvents: true,
			listeners:{
				'keypress': function(field,event){
                    if (event.getKey() == 45){ //tombol INSERT untuk INPUT produk ke list produk
                        //alert("ok");
						var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
							dproduk_id	:0,
							dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
							dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
							dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
							dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
							dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
							dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
							dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
							dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
							dproduk_karyawan:0,
							produk_harga_default:Ext.getCmp('harga_tbar').getValue()
						});
						
						cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
						cbo_dproduk_satuanDataStore.load({
							callback: function(r,opt,success){
								if(success==true){
									detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
									combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
									detail_jual_produk_DataStore.commitChanges();
								}
							}
						});
						
						detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
                    }
				
                },
				'change': function(combo, record) {
					//alert("okayy4");
				}
			}
		
		}, '-',{
			xtype:'textfield',
			fieldLabel:'Sub Total',
			name:'subtotal_tbar',
			id:'subtotal_tbar',
			ref : '../djproduk_add_subtotal',
			anchor: '95%',
			width: 50,
			enableKeyEvents: true,
			listeners:{
				'keypress': function(field,event){
                    if (event.getKey() == 45){ //tombol INSERT untuk INPUT produk ke list produk
                        //alert("ok");
						var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
							dproduk_id	:0,
							dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
							dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
							dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
							dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
							dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
							dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
							dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
							dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
							dproduk_karyawan:0,
							produk_harga_default:Ext.getCmp('harga_tbar').getValue()
						});
						
						cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
						cbo_dproduk_satuanDataStore.load({
							callback: function(r,opt,success){
								if(success==true){
									detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
									combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
									detail_jual_produk_DataStore.commitChanges();
								}
							}
						});
						
						detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
                    }
				
                },
				'change': function(combo, record) {
					//alert("okayy5");
				}
			}
		
		}, '-',{
			xtype:'combo',
			
			
			
			store:new Ext.data.SimpleStore({
			fields:['diskon_jenis_value'],
				data:[['Tanpa Diskon'],['Member'],['Promo'],['Free']]
			}),
			mode: 'local',
			displayField: 'diskon_jenis_value',
			valueField: 'diskon_jenis_value',
			ref : '../djproduk_add_jdiskon',
			allowBlank: true,
			anchor: '95%',
			triggerAction: 'all',
			lazyRenderer: true,
			
			
			autoSelect:true,
			forceSelection:true,
			
			
			fieldLabel:'Jenis Diskon',
			name:'jdiskon_tbar',
			id:'jdiskon_tbar',
			anchor: '95%',
			width: 50,
			enableKeyEvents: true,
			listeners:{
				'keypress': function(field,event){
                    if (event.getKey() == 45){ //tombol INSERT untuk INPUT produk ke list produk
                        //alert("ok");
						var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
							dproduk_id	:0,
							dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
							dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
							dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
							dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
							dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
							dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
							dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
							dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
							dproduk_karyawan:0,
							produk_harga_default:Ext.getCmp('harga_tbar').getValue()
						});
						
						cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
						cbo_dproduk_satuanDataStore.load({
							callback: function(r,opt,success){
								if(success==true){
									detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
									combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
									detail_jual_produk_DataStore.commitChanges();
								}
							}
						});
						
						detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
                    }
					
                },
				'select': function(combo, record) {
					
					//alert("okayy6");
					var dtotal_net_field = 0;
					var djumlah_beli_produk = djumlah_beli_produkField.getValue();
					//cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
					var j=cbo_dproduk_produkDataStore.findExact('dproduk_produk_value',Ext.getCmp('produk_id_tbar').getValue(),0);
					var djenis_diskon = 0;		
					
					if(Ext.getCmp('jdiskon_tbar').getValue()=='Promo'){
						djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dpromo;
						//djumlah_diskonField.setValue(record.data.dproduk_produk_du);
						Ext.getCmp('diskon_tbar').setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dpromo);
						djumlah_diskonField.setReadOnly(true);
						djumlah_diskonField.setDisabled(true); //default
					}else if(Ext.getCmp('jdiskon_tbar').getValue()=='Member'){
						djumlah_diskon = cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dm;
						//djumlah_diskonField.setValue(record.data.dproduk_produk_du);
						Ext.getCmp('diskon_tbar').setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dm);
						djumlah_diskonField.setReadOnly(true);
						djumlah_diskonField.setDisabled(true); //default
					}else if(Ext.getCmp('jdiskon_tbar').getValue()=='Free'){
						djenis_diskon = 100;
						//djumlah_diskonField.setValue(cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_dcompliment);
						//djumlah_diskonField.setValue(100);
						Ext.getCmp('diskon_tbar').setValue('100');
						djumlah_diskonField.setReadOnly(true);
						djumlah_diskonField.setDisabled(true); //default
						/*untuk YESS*///djumlah_diskonField.setDisabled(false); /*eof YESS*/ 
					}
					else{
						//djumlah_diskonField.setValue(0);
						djenis_diskon = 0;
						Ext.getCmp('diskon_tbar').setValue('0');						
						djumlah_diskonField.setDisabled(true);
					}
					
					dtotal_net_field = ((100-djenis_diskon)/100) * (djumlah_beli_produk*cbo_dproduk_produkDataStore.getAt(j).data.dproduk_produk_harga);
					dtotal_net_field = (dtotal_net_field>0?Math.round(dtotal_net_field):0);
					dsub_total_netField.setValue(dtotal_net_field);
				}				
			}
		
		}, '-',{
			xtype:'textfield',
			fieldLabel:'Persen Diskon',
			name:'diskon_tbar',
			id:'diskon_tbar',
			ref : '../djproduk_add_diskon',
			anchor: '95%',
			width: 25,
			enableKeyEvents: true,
			listeners:{
				'keypress': function(field,event){
                    if (event.getKey() == 45){ //tombol INSERT untuk INPUT produk ke list produk
                        //alert("ok");
						var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
							dproduk_id	:0,
							dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
							dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
							dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
							dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
							dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
							dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
							dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
							dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
							dproduk_karyawan:0,
							produk_harga_default:Ext.getCmp('harga_tbar').getValue()
						});
						
						cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
						cbo_dproduk_satuanDataStore.load({
							callback: function(r,opt,success){
								if(success==true){
									detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
									combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
									detail_jual_produk_DataStore.commitChanges();
								}
							}
						});
						
						detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
                    }
					
                },
				'change': function(combo, record) {
					//alert("okayy7");
				}
			}
		
		}, '-',{
			xtype:'textfield',
			fieldLabel:'Sub Total Net',
			name:'subtotal_net_tbar',
			id:'subtotal_net_tbar',
			ref : '../djproduk_add_subtotalnet',
			anchor: '95%',
			width: 50,
			enableKeyEvents: true,
			listeners:{
				'keypress': function(field,event){
                    if (event.getKey() == 45){ //tombol INSERT untuk INPUT produk ke list produk
                        //alert("ok");
						var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
							dproduk_id	:0,
							dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
							dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
							dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
							dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
							dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
							dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
							dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
							dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
							dproduk_karyawan:0,
							produk_harga_default:Ext.getCmp('harga_tbar').getValue()
						});
						
						cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
						cbo_dproduk_satuanDataStore.load({
							callback: function(r,opt,success){
								if(success==true){
									detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
									combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
									detail_jual_produk_DataStore.commitChanges();
								}
							}
						});
						
						detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
                    }
					
                },
				'change': function(combo, record) {
					//alert("okayy8");
				}
			}
		
		}
		,'-',{
			//text: 'Tambah',
			tooltip: 'Tambahkan Produk',
			iconCls:'icon-adds',
			ref : '../djproduk_add',
			disabled: false,
			handler: function(){
				var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
					dproduk_id	:0,
					dproduk_produk	:Ext.getCmp('produk_id_tbar').getValue(),
					dproduk_satuan	:Ext.getCmp('satuan_id_tbar').getValue(),
					dproduk_jumlah	:Ext.getCmp('jumlah_tbar').getValue(),
					dproduk_harga	:Ext.getCmp('harga_tbar').getValue(),
					dproduk_subtotal:Ext.getCmp('subtotal_tbar').getValue(),
					dproduk_diskon_jenis: Ext.getCmp('jdiskon_tbar').getValue(),
					dproduk_diskon	:Ext.getCmp('diskon_tbar').getValue(),
					dproduk_subtotal_net:Ext.getCmp('subtotal_net_tbar').getValue(),
					dproduk_karyawan:0,
					produk_harga_default:Ext.getCmp('harga_tbar').getValue()
				});
				
				cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
				cbo_dproduk_satuanDataStore.load({
					callback: function(r,opt,success){
						if(success==true){
							detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
							combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
							detail_jual_produk_DataStore.commitChanges();
						}
					}
				});
				
				detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);	
			}
		}
		,'-',{
			//text: 'Hapus',
			tooltip: 'Hapus detail data terpilih',
			iconCls:'icon-delete',
			ref : '../djproduk_delete',
			disabled: false,
			handler: detail_jual_produk_confirm_delete
		}
		]
		<?php } ?>
	});
	
	function detail_update(oGrid_event)
	{
		detail_jual_produk_DataStore.commitChanges();
		detail_jual_produk_DataStore.reload();	
		//load_dstore_jproduk();
	}
	
	detail_jual_produkListEditorGrid.on('afteredit', detail_update);

	function detail_jual_produk_add(){
		//djumlah_diskonField.setReadOnly(true);
		djumlah_diskonField.setDisabled(true);
		dharga_konversiField.setDisabled(true);
		
		//jika detail sudah ada 1, maka referal akan mengikuti row sebelumnya
		if(detail_jual_produkListEditorGrid.selModel.getCount() == 1)
		{
			// temp ini berfungsi utk menyimpan ID dari referal yang terakhir kali diinput. Jika program di Refresh / Cancel, maka akan kembali ke kondisi semula
			var temp_produk = combo_reveral.getValue(1);
			var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
				dproduk_id	:0,
				dproduk_produk	:'',
				dproduk_satuan	:'',
				dproduk_jumlah	:1,
				dproduk_harga	:0,
				dproduk_subtotal:0,
				dproduk_diskon_jenis: 'Tanpa Diskon',
				dproduk_diskon	:0,
				dproduk_subtotal_net:0,
				dproduk_karyawan:temp_produk,
				produk_harga_default:0
			});
			editor_detail_jual_produk.stopEditing();
			detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
			detail_jual_produkListEditorGrid.getSelectionModel().selectRow(0);
			editor_detail_jual_produk.startEditing(0);
		}else{
			var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
				dproduk_id	:0,
				dproduk_produk	:'',
				dproduk_satuan	:'',
				dproduk_jumlah	:1,
				dproduk_harga	:0,
				dproduk_subtotal:0,
				dproduk_diskon_jenis: 'Tanpa Diskon',
				dproduk_diskon	:0,
				dproduk_subtotal_net:0,
				dproduk_karyawan:'',
				produk_harga_default:0
			});
			editor_detail_jual_produk.stopEditing();
			detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
			detail_jual_produkListEditorGrid.getView().refresh();
			detail_jual_produkListEditorGrid.getSelectionModel().selectRow(0);
			editor_detail_jual_produk.startEditing(0);
		}
	}
	
	function detail_jual_produk_confirm_delete(){
		if(detail_jual_produkListEditorGrid.selModel.getCount() == 1){
			Ext.MessageBox.confirm('Confirmation','Anda yakin untuk menghapus data ini?', detail_jual_produk_delete);
		}else {
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Tidak ada data yang dipilih untuk dihapus',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
		}
	}
	
	function detail_jual_produk_delete(btn){
		if(btn=='yes'){
            var selections = detail_jual_produkListEditorGrid.getSelectionModel().getSelections();
			for(var i = 0, record; record = selections[i]; i++){
                if(record.data.dproduk_id==''){
                    detail_jual_produk_DataStore.remove(record);
					load_dstore_jproduk();
                }else if((/^\d+$/.test(record.data.dproduk_id))){
                    //Hapus dari db.detail_jual_produk
                    Ext.MessageBox.show({
                        title: 'Silahkan Tunggu',
                        msg: 'Loading items...',
                        progressText: 'Initializing...',
                        width:300,
                        wait:true,
                        waitConfig: {interval:200},
                        closable:false
                    });
                    detail_jual_produk_DataStore.remove(record);
                    Ext.Ajax.request({ 
                        waitMsg: 'Silahkan Tunggu',
                        url: 'index.php?c=c_master_jual_produk&m=get_action', 
                        params: { task: "DDELETE", dproduk_id:  record.data.dproduk_id }, 
                        success: function(response){
                            var result=eval(response.responseText);
                            switch(result){
                                case 1:  // Success : simply reload
									load_dstore_jproduk();
                                    Ext.MessageBox.hide();
                                    break;
                                default:
                                    Ext.MessageBox.hide();
                                    Ext.MessageBox.show({
                                        title: 'Warning',
                                        msg: 'Could not delete the entire selection',
                                        buttons: Ext.MessageBox.OK,
                                        animEl: 'save',
                                        icon: Ext.MessageBox.WARNING
                                    });
                                    break;
                            }
                        },
                        failure: function(response){
                            Ext.MessageBox.hide();
                            var result=response.responseText;
                            Ext.MessageBox.show({
                               title: 'Error',
                               msg: 'Could not connect to the database. retry later.',
                               buttons: Ext.MessageBox.OK,
                               animEl: 'database',
                               icon: Ext.MessageBox.ERROR
                            });	
                        }
                    });
                }
			}
			push_insert();
		} 
	}
	
	function update_group_carabayar_jual_produk(){
		var value=jproduk_caraField.getValue();
		master_jual_produk_tunaiGroup.setVisible(false);
		master_jual_produk_cardGroup.setVisible(false);
		master_jual_produk_cekGroup.setVisible(false);
		master_jual_produk_transferGroup.setVisible(false);
		master_jual_produk_kwitansiGroup.setVisible(false);
		master_jual_produk_voucherGroup.setVisible(false);
		
		//RESET Nilai di Cara Bayar-1
		jproduk_tunai_nilaiField.reset();
		jproduk_tunai_nilai_cfField.reset();
		jproduk_card_nilaiField.reset();
		jproduk_card_nilai_cfField.reset();
		jproduk_cek_nilaiField.reset();
		jproduk_cek_nilai_cfField.reset();
		jproduk_transfer_nilaiField.reset();
		jproduk_transfer_nilai_cfField.reset();
		jproduk_kwitansi_nilaiField.reset();
		jproduk_kwitansi_nilai_cfField.reset();
		jproduk_voucher_cashbackField.reset();
		
		if(value=='card'){
			master_jual_produk_cardGroup.setVisible(true);
			kwitansi_jual_produk_reset_form();
			
		}else if(value=='cek/giro'){
			master_jual_produk_cekGroup.setVisible(true);
			kwitansi_jual_produk_reset_form();
		}else if(value=='transfer'){
			master_jual_produk_transferGroup.setVisible(true);
			kwitansi_jual_produk_reset_form();
		}else if(value=='kwitansi'|| value=='gift_voucher'){
			master_jual_produk_kwitansiGroup.setVisible(true);
		}else if(value=='voucher'){
			master_jual_produk_voucherGroup.setVisible(true);
			kwitansi_jual_produk_reset_form();
		}else if(value=='tunai'){
			master_jual_produk_tunaiGroup.setVisible(true);
			kwitansi_jual_produk_reset_form();
		}
	}
	
	function update_group_carabayar2_jual_produk(){
		var value=jproduk_cara2Field.getValue();
		master_jual_produk_tunai2Group.setVisible(false);
		master_jual_produk_card2Group.setVisible(false);
		master_jual_produk_cek2Group.setVisible(false);
		master_jual_produk_transfer2Group.setVisible(false);
		master_jual_produk_kwitansi2Group.setVisible(false);
		master_jual_produk_voucher2Group.setVisible(false);
		//RESET Nilai di Cara Bayar-1
		jproduk_tunai_nilai2Field.reset();
		jproduk_tunai_nilai2_cfField.reset();
		jproduk_card_nilai2Field.reset();
		jproduk_card_nilai2_cfField.reset();
		jproduk_cek_nilai2Field.reset();
		jproduk_cek_nilai2_cfField.reset();
		jproduk_transfer_nilai2Field.reset();
		jproduk_transfer_nilai2_cfField.reset();
		jproduk_kwitansi_nilai2Field.reset();
		jproduk_kwitansi_nilai2_cfField.reset();
		jproduk_voucher_cashback2Field.reset();
		
		if(value=='card'){
			master_jual_produk_card2Group.setVisible(true);
			kwitansi2_jual_produk_reset_form();			
		}else if(value=='cek/giro'){
			master_jual_produk_cek2Group.setVisible(true);
			kwitansi2_jual_produk_reset_form();
		}else if(value=='transfer'){
			master_jual_produk_transfer2Group.setVisible(true);
			kwitansi2_jual_produk_reset_form();
		}else if(value=='kwitansi'|| value=='gift_voucher'){
			master_jual_produk_kwitansi2Group.setVisible(true);
		}else if(value=='voucher'){
			master_jual_produk_voucher2Group.setVisible(true);
			kwitansi2_jual_produk_reset_form();
		}else if(value=='tunai'){
			master_jual_produk_tunai2Group.setVisible(true);
			kwitansi2_jual_produk_reset_form();
		}
	}
	
	function update_group_carabayar3_jual_produk(){
		var value=jproduk_cara3Field.getValue();
		master_jual_produk_tunai3Group.setVisible(false);
		master_jual_produk_card3Group.setVisible(false);
		master_jual_produk_cek3Group.setVisible(false);
		master_jual_produk_transfer3Group.setVisible(false);
		master_jual_produk_kwitansi3Group.setVisible(false);
		master_jual_produk_voucher3Group.setVisible(false);
		//RESET Nilai di Cara Bayar-1
		jproduk_tunai_nilai3Field.reset();
		jproduk_tunai_nilai3_cfField.reset();
		jproduk_card_nilai3Field.reset();
		jproduk_card_nilai3_cfField.reset();
		jproduk_cek_nilai3Field.reset();
		jproduk_cek_nilai3_cfField.reset();
		jproduk_transfer_nilai3Field.reset();
		jproduk_transfer_nilai3_cfField.reset();
		jproduk_kwitansi_nilai3Field.reset();
		jproduk_kwitansi_nilai3_cfField.reset();
		jproduk_voucher_cashback3Field.reset();
		
		if(value=='card'){
			master_jual_produk_card3Group.setVisible(true);
			kwitansi3_jual_produk_reset_form();
		}else if(value=='cek/giro'){
			master_jual_produk_cek3Group.setVisible(true);
			kwitansi3_jual_produk_reset_form();
		}else if(value=='transfer'){
			master_jual_produk_transfer3Group.setVisible(true);
			kwitansi3_jual_produk_reset_form();
		}else if(value=='kwitansi'|| value=='gift_voucher'){
			master_jual_produk_kwitansi3Group.setVisible(true);
		}else if(value=='voucher'){
			master_jual_produk_voucher3Group.setVisible(true);
			kwitansi3_jual_produk_reset_form();
		}else if(value=='tunai'){
			master_jual_produk_tunai3Group.setVisible(true);
			kwitansi3_jual_produk_reset_form();
		}
	}
	
	function load_total_bayar_updating(){
		var update_total_field=0;
		var update_hutang_field=0;
		var jproduk_bayar_temp=jproduk_bayarField.getValue();
		var total_bayar=0;

		var transfer_nilai=0;
		var transfer_nilai2=0;
		var transfer_nilai3=0;
		var kwitansi_nilai=0;
		var kwitansi_nilai2=0;
		var kwitansi_nilai3=0;
		var card_nilai=0;
		var card_nilai2=0;
		var card_nilai3=0;
		var cek_nilai=0;
		var cek_nilai2=0;
		var cek_nilai3=0;
		var voucher_nilai=0;
		var voucher_nilai2=0;
		var voucher_nilai3=0;
		var vgrooming_nilai=0;
		
		transfer_nilai=jproduk_transfer_nilaiField.getValue();
		if(/^\d+$/.test(transfer_nilai))
			transfer_nilai=jproduk_transfer_nilaiField.getValue();
		else
			transfer_nilai=0;

		transfer_nilai2=jproduk_transfer_nilai2Field.getValue();
		if(/^\d+$/.test(transfer_nilai2))
			transfer_nilai2=jproduk_transfer_nilai2Field.getValue();
		else
			transfer_nilai2=0;
		
		transfer_nilai3=jproduk_transfer_nilai3Field.getValue();
		if(/^\d+$/.test(transfer_nilai3))
			transfer_nilai3=jproduk_transfer_nilai3Field.getValue();
		else
			transfer_nilai3=0;
		
		kwitansi_nilai=jproduk_kwitansi_nilaiField.getValue();
		if(/^\d+$/.test(kwitansi_nilai))
			kwitansi_nilai=jproduk_kwitansi_nilaiField.getValue();
		else
			kwitansi_nilai=0;
		
		kwitansi_nilai2=jproduk_kwitansi_nilai2Field.getValue();
		if(/^\d+$/.test(kwitansi_nilai2))
			kwitansi_nilai2=jproduk_kwitansi_nilai2Field.getValue();
		else
			kwitansi_nilai2=0;
		
		kwitansi_nilai3=jproduk_kwitansi_nilai3Field.getValue();
		if(/^\d+$/.test(kwitansi_nilai3))
			kwitansi_nilai3=jproduk_kwitansi_nilai3Field.getValue();
		else
			kwitansi_nilai3=0;
		
		card_nilai=jproduk_card_nilaiField.getValue();
		if(/^\d+$/.test(card_nilai))
			card_nilai=jproduk_card_nilaiField.getValue();
		else
			card_nilai=0;
		
		card_nilai2=jproduk_card_nilai2Field.getValue();
		if(/^\d+$/.test(card_nilai2))
			card_nilai2=jproduk_card_nilai2Field.getValue();
		else
			card_nilai2=0;
		
		card_nilai3=jproduk_card_nilai3Field.getValue();
		if(/^\d+$/.test(card_nilai3))
			card_nilai3=jproduk_card_nilai3Field.getValue();
		else
			card_nilai3=0;
		
		cek_nilai=jproduk_cek_nilaiField.getValue();
		if(/^\d+$/.test(cek_nilai))
			cek_nilai=jproduk_cek_nilaiField.getValue();
		else
			cek_nilai=0;
		
		cek_nilai2=jproduk_cek_nilai2Field.getValue();
		if(/^\d+$/.test(cek_nilai2))
			cek_nilai2=jproduk_cek_nilai2Field.getValue();
		else
			cek_nilai2=0;
		
		cek_nilai3=jproduk_cek_nilai3Field.getValue();
		if(/^\d+$/.test(cek_nilai3))
			cek_nilai3=jproduk_cek_nilai3Field.getValue();
		else
			cek_nilai3=0;
		
		voucher_nilai=jproduk_voucher_cashbackField.getValue();
		if(/^\d+$/.test(voucher_nilai))
			voucher_nilai=jproduk_voucher_cashbackField.getValue();
		else
			voucher_nilai=0;
		
		voucher_nilai2=jproduk_voucher_cashback2Field.getValue();
		if(/^\d+$/.test(voucher_nilai2))
			voucher_nilai2=jproduk_voucher_cashback2Field.getValue();
		else
			voucher_nilai2=0;
		
		voucher_nilai3=jproduk_voucher_cashback3Field.getValue();
		if(/^\d+$/.test(voucher_nilai3))
			voucher_nilai3=jproduk_voucher_cashback3Field.getValue();
		else
			voucher_nilai3=0;

		tunai_nilai=jproduk_tunai_nilaiField.getValue();
		if(/^\d+$/.test(tunai_nilai))
			tunai_nilai=jproduk_tunai_nilaiField.getValue();
		else
			tunai_nilai=0;

		tunai_nilai2=jproduk_tunai_nilai2Field.getValue();
		if(/^\d+$/.test(tunai_nilai2))
			tunai_nilai2=jproduk_tunai_nilai2Field.getValue();
		else
			tunai_nilai2=0;

		tunai_nilai3=jproduk_tunai_nilai3Field.getValue();
		if(/^\d+$/.test(tunai_nilai3))
			tunai_nilai3=jproduk_tunai_nilai3Field.getValue();
		else
			tunai_nilai3=0;

		total_bayar=transfer_nilai+transfer_nilai2+transfer_nilai3+kwitansi_nilai+kwitansi_nilai2+kwitansi_nilai3+card_nilai+card_nilai2+card_nilai3+cek_nilai+cek_nilai2+cek_nilai3+voucher_nilai+voucher_nilai2+voucher_nilai3+tunai_nilai+tunai_nilai2+tunai_nilai3+vgrooming_nilai;
		
		update_total_field=jproduk_subTotalField.getValue()*((100-jproduk_diskonField.getValue())/100)-jproduk_cashbackField.getValue();
		jproduk_totalField.setValue(update_total_field);
		jproduk_total_cfField.setValue(CurrencyFormatted(update_total_field));
		jproduk_TotalLabel.setValue(CurrencyFormatted(update_total_field));

		jproduk_bayarField.setValue(total_bayar);
		jproduk_bayar_cfField.setValue(CurrencyFormatted(total_bayar));
		jproduk_TotalBayarLabel.setValue(CurrencyFormatted(total_bayar));
		
		update_hutang_field=update_total_field-total_bayar;
		jproduk_hutangField.setValue(update_hutang_field);
		jproduk_hutang_cfField.setValue(CurrencyFormatted(update_hutang_field));
		jproduk_HutangLabel.setValue(CurrencyFormatted(update_hutang_field));

		jproduk_diskonField.setValue(jproduk_diskonField.getValue());
		jproduk_cashbackField.setValue(jproduk_cashbackField.getValue());
		jproduk_cashback_cfField.setValue(CurrencyFormatted(jproduk_cashbackField.getValue()));
		jproduk_potong_pointField.setValue(jproduk_potong_pointField.getValue());
		jproduk_potong_pointcfField.setValue(CurrencyFormatted(jproduk_potong_pointField.getValue()));
		
		if(total_bayar>update_total_field){
			jproduk_pesanLabel.setText("Lebih Bayar");
		}else if(total_bayar<update_total_field){
			jproduk_pesanLabel.setText("Kurang Bayar");
		}else if(total_bayar==update_total_field){
			jproduk_pesanLabel.setText("");
		}
		if(total_bayar==update_total_field){
			jproduk_lunasLabel.setText("LUNAS");
		}else if(total_bayar!==update_total_field){
			jproduk_lunasLabel.setText("");
		}
	}
	
	function load_dstore_jproduk(){
		/*
		 * yang terlibat adalah:
		 * 1. Grid Detail Pembelian
		 * 2. Sub Total Biaya
		 * 3. Disk Tambahan (%)
		 * 4. Voucher (Rp)
		 * 5. Total Biaya
		 * 6. Total Bayar
		 * 7. Total Hutang
		*/
		dharga_konversiField.setDisabled(true);
		dharga_konversiField.setReadOnly(true);
		
		var disk_tambahan_field = jproduk_diskonField.getValue();
		if(disk_tambahan_field==''){
			disk_tambahan_field = 0;
		}
		
		var voucher_rp_field = jproduk_cashbackField.getValue();
		if(voucher_rp_field==''){
			voucher_rp_field = 0;
		}
		
		var potongpoint_rp_field = jproduk_potong_pointField.getValue();
		if(potongpoint_rp_field==''){
			potongpoint_rp_field = 0;
		}
		
		//var total_bayar_field = jproduk_bayarField.getValue();
		var jumlah_item = 0;
		var sub_total_field = 0;
		var total_biaya_field = 0;
		var total_hutang_field = 0;
		
		for(i=0;i<detail_jual_produk_DataStore.getCount();i++){
			jumlah_item+=parseInt(detail_jual_produk_DataStore.getAt(i).data.dproduk_jumlah);
			sub_total_field+=detail_jual_produk_DataStore.getAt(i).data.dproduk_jumlah * detail_jual_produk_DataStore.getAt(i).data.dproduk_harga * ((100 - detail_jual_produk_DataStore.getAt(i).data.dproduk_diskon)/100);
		}
		jproduk_jumlahField.setValue(jumlah_item);
		jproduk_jumlahLabel.setValue(jumlah_item);
		jproduk_subTotalField.setValue(sub_total_field);
		jproduk_subTotal_cfField.setValue(CurrencyFormatted(sub_total_field));
		jproduk_subTotalLabel.setValue(CurrencyFormatted(sub_total_field));
		
		total_biaya_field = sub_total_field * ((100 - disk_tambahan_field)/100) - voucher_rp_field-potongpoint_rp_field;
		total_biaya_field = (total_biaya_field>0?Math.round(total_biaya_field):0);
		jproduk_totalField.setValue(total_biaya_field);
		jproduk_total_cfField.setValue(CurrencyFormatted(total_biaya_field));
	jproduk_TotalLabel.setValue(CurrencyFormatted(total_biaya_field));	
		
		jproduk_tunai_nilaiField.setValue(total_biaya_field);
		jproduk_tunai_nilai_cfField.setValue(CurrencyFormatted(total_biaya_field));
		jproduk_bayarField.setValue(total_biaya_field);
		//sssini loh
		var total_bayar_field = jproduk_bayarField.getValue();
		
		total_bayar_field=(total_bayar_field>0?Math.round(total_bayar_field):0);
		jproduk_bayarField.setValue(total_bayar_field);
		jproduk_bayar_cfField.setValue(CurrencyFormatted(total_bayar_field));
		jproduk_TotalBayarLabel.setValue(CurrencyFormatted(total_bayar_field));

		total_hutang_field = total_biaya_field - total_bayar_field;
		jproduk_hutangField.setValue(total_hutang_field);
		jproduk_hutang_cfField.setValue(CurrencyFormatted(total_hutang_field));
		jproduk_HutangLabel.setValue(CurrencyFormatted(total_hutang_field));
		//console.clear(); // mengclear console, agar tidak membingungkan user ketika terjadi error pada firebug yg disebabkan "enter" keypress saat input detail
	}
	
	function load_total_biaya(){
		/*
		 * Field-field yang terlibat adalah:
		 * 1. Sub Total Biaya
		 * 2. Disk Tambahan (%)
		 * 3. Voucher (Rp)
		 * 4. Total Biaya
		 * 5. Total Bayar
		 * 6. Total Hutang
		 * 7. Notifikasi Kelebihan Bayar
		*/
		var sub_total_biaya_field = jproduk_subTotalField.getValue();
		var disk_tambahan_field = jproduk_diskonField.getValue();
		var voucher_rp_field = jproduk_cashbackField.getValue();
		var potong_point_rp_field = jproduk_potong_pointField.getValue();
		var total_bayar_field = jproduk_bayarField.getValue();
		
		if(disk_tambahan_field==''){
			disk_tambahan_field = 0;
		}
		
		if(voucher_rp_field==''){
			voucher_rp_field = 0;
		}
		
		if(potong_point_rp_field==''){
			potong_point_rp_field = 0;
		}
		
		var total_biaya_field = 0;
		var total_hutang_field = 0;
		
		total_biaya_field += sub_total_biaya_field * ((100 - disk_tambahan_field)/100) - voucher_rp_field - potong_point_rp_field;
		total_biaya_field = (total_biaya_field>0?Math.round(total_biaya_field):0);
		jproduk_totalField.setValue(total_biaya_field);
		jproduk_total_cfField.setValue(CurrencyFormatted(total_biaya_field));
		jproduk_TotalLabel.setValue(CurrencyFormatted(total_biaya_field));
		
		jproduk_tunai_nilaiField.setValue(total_biaya_field);
		jproduk_tunai_nilai_cfField.setValue(CurrencyFormatted(total_biaya_field));
		
		jproduk_bayarField.setValue(total_biaya_field);
		total_hutang_field = total_biaya_field - jproduk_tunai_nilaiField;
		jproduk_hutangField.setValue(total_hutang_field);
		jproduk_hutang_cfField.setValue(CurrencyFormatted(total_hutang_field));
		jproduk_HutangLabel.setValue(CurrencyFormatted(total_hutang_field));
	}
	
	function load_total_bayar(){
		/*
		 * Field-field yang terlibat adalah:
		 * 1. Cara Bayar
		 * 2. Total Biaya
		 * 3. Total Bayar
		 * 4. Total Hutang
		*/
		var total_hutang_field = 0;
		var total_bayar_field = 0;
		var total_biaya_field = jproduk_totalField.getValue();
		var transfer_nilai=0;
		var transfer_nilai2=0;
		var transfer_nilai3=0;
		var kwitansi_nilai=0;
		var kwitansi_nilai2=0;
		var kwitansi_nilai3=0;
		var card_nilai=0;
		var card_nilai2=0;
		var card_nilai3=0;
		var cek_nilai=0;
		var cek_nilai2=0;
		var cek_nilai3=0;
		var voucher_nilai=0;
		var voucher_nilai2=0;
		var voucher_nilai3=0;
		var vgrooming_nilai=0;
		
		transfer_nilai=jproduk_transfer_nilaiField.getValue();
		if(/^\d+$/.test(transfer_nilai))
			transfer_nilai=jproduk_transfer_nilaiField.getValue();
		else
			transfer_nilai=0;
		
		transfer_nilai2=jproduk_transfer_nilai2Field.getValue();
		if(/^\d+$/.test(transfer_nilai2))
			transfer_nilai2=jproduk_transfer_nilai2Field.getValue();
		else
			transfer_nilai2=0;
		
		transfer_nilai3=jproduk_transfer_nilai3Field.getValue();
		if(/^\d+$/.test(transfer_nilai3))
			transfer_nilai3=jproduk_transfer_nilai3Field.getValue();
		else
			transfer_nilai3=0;
		
		kwitansi_nilai=jproduk_kwitansi_nilaiField.getValue();
		if(/^\d+$/.test(kwitansi_nilai))
			kwitansi_nilai=jproduk_kwitansi_nilaiField.getValue();
		else
			kwitansi_nilai=0;
		
		kwitansi_nilai2=jproduk_kwitansi_nilai2Field.getValue();
		if(/^\d+$/.test(kwitansi_nilai2))
			kwitansi_nilai2=jproduk_kwitansi_nilai2Field.getValue();
		else
			kwitansi_nilai2=0;
		
		kwitansi_nilai3=jproduk_kwitansi_nilai3Field.getValue();
		if(/^\d+$/.test(kwitansi_nilai3))
			kwitansi_nilai3=jproduk_kwitansi_nilai3Field.getValue();
		else
			kwitansi_nilai3=0;
		
		card_nilai=jproduk_card_nilaiField.getValue();
		if(/^\d+$/.test(card_nilai))
			card_nilai=jproduk_card_nilaiField.getValue();
		else
			card_nilai=0;
		
		card_nilai2=jproduk_card_nilai2Field.getValue();
		if(/^\d+$/.test(card_nilai2))
			card_nilai2=jproduk_card_nilai2Field.getValue();
		else
			card_nilai2=0;
		
		card_nilai3=jproduk_card_nilai3Field.getValue();
		if(/^\d+$/.test(card_nilai3))
			card_nilai3=jproduk_card_nilai3Field.getValue();
		else
			card_nilai3=0;
		
		cek_nilai=jproduk_cek_nilaiField.getValue();
		if(/^\d+$/.test(cek_nilai))
			cek_nilai=jproduk_cek_nilaiField.getValue();
		else
			cek_nilai=0;
		
		cek_nilai2=jproduk_cek_nilai2Field.getValue();
		if(/^\d+$/.test(cek_nilai2))
			cek_nilai2=jproduk_cek_nilai2Field.getValue();
		else
			cek_nilai2=0;
		
		cek_nilai3=jproduk_cek_nilai3Field.getValue();
		if(/^\d+$/.test(cek_nilai3))
			cek_nilai3=jproduk_cek_nilai3Field.getValue();
		else
			cek_nilai3=0;
		
		voucher_nilai=jproduk_voucher_cashbackField.getValue();
		if(/^\d+$/.test(voucher_nilai))
			voucher_nilai=jproduk_voucher_cashbackField.getValue();
		else
			voucher_nilai=0;
		
		voucher_nilai2=jproduk_voucher_cashback2Field.getValue();
		if(/^\d+$/.test(voucher_nilai2))
			voucher_nilai2=jproduk_voucher_cashback2Field.getValue();
		else
			voucher_nilai2=0;
		
		voucher_nilai3=jproduk_voucher_cashback3Field.getValue();
		if(/^\d+$/.test(voucher_nilai3))
			voucher_nilai3=jproduk_voucher_cashback3Field.getValue();
		else
			voucher_nilai3=0;

		tunai_nilai=jproduk_tunai_nilaiField.getValue();
		if(/^\d+$/.test(tunai_nilai))
			tunai_nilai=jproduk_tunai_nilaiField.getValue();
		else
			tunai_nilai=0;

		tunai_nilai2=jproduk_tunai_nilai2Field.getValue();
		if(/^\d+$/.test(tunai_nilai2))
			tunai_nilai2=jproduk_tunai_nilai2Field.getValue();
		else
			tunai_nilai2=0;

		tunai_nilai3=jproduk_tunai_nilai3Field.getValue();
		if(/^\d+$/.test(tunai_nilai3))
			tunai_nilai3=jproduk_tunai_nilai3Field.getValue();
		else
			tunai_nilai3=0;
			
		
		total_bayar_field=transfer_nilai+transfer_nilai2+transfer_nilai3+kwitansi_nilai+kwitansi_nilai2+kwitansi_nilai3+card_nilai+card_nilai2+card_nilai3+cek_nilai+cek_nilai2+cek_nilai3+voucher_nilai+voucher_nilai2+voucher_nilai3+tunai_nilai+tunai_nilai2+tunai_nilai3;
		total_bayar_field=(total_bayar_field>0?Math.round(total_bayar_field):0);
		jproduk_bayarField.setValue(total_bayar_field);
		jproduk_bayar_cfField.setValue(CurrencyFormatted(total_bayar_field));
		jproduk_TotalBayarLabel.setValue(CurrencyFormatted(total_bayar_field));

		total_hutang_field=total_biaya_field-total_bayar_field;
		total_hutang_field=(total_hutang_field>0?Math.round(total_hutang_field):0);
		jproduk_hutangField.setValue(total_hutang_field);
		jproduk_hutang_cfField.setValue(CurrencyFormatted(total_hutang_field));
		jproduk_HutangLabel.setValue(CurrencyFormatted(total_hutang_field));
		
		if(total_bayar_field>total_biaya_field){
			jproduk_pesanLabel.setText("Lebih Bayar");
		}else if(total_bayar_field<total_biaya_field){
			jproduk_pesanLabel.setText("Kurang Bayar");
		}else if(total_bayar_field==total_biaya_field){
			jproduk_pesanLabel.setText("");
		}
		if(total_bayar_field==total_biaya_field){
			jproduk_lunasLabel.setText("LUNAS");
		}else if(total_bayar_field!==total_biaya_field){
			jproduk_lunasLabel.setText("");
		}
	}
	
	//event on update of detail data store
	detail_jual_produk_DataStore.on("update",load_dstore_jproduk);
	jproduk_diskonField.on("keyup",function(){
		if(this.getRawValue()>100){
			this.setRawValue(100);
		}
		load_total_biaya();
	});
	jproduk_cashback_cfField.on("keyup",function(){
		var cf_value = jproduk_cashback_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_cashbackField.setValue(cf_tonumber);
		load_total_biaya();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_cashback_cfField.on("blur",function(){
		push_insert();
	});
	
	jproduk_potong_pointcfField.on("keyup",function(){
		var cf_value = jproduk_potong_pointcfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_potong_pointField.setValue(cf_tonumber);
		load_total_biaya();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_potong_pointcfField.on("blur",function(){
		push_insert();
	});
	//kwitansi
	jproduk_kwitansi_nilai_cfField.on("keyup",function(){
		var cf_value = jproduk_kwitansi_nilai_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		if(cf_tonumber>jproduk_kwitansi_sisaField.getValue()){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Maaf, Jumlah yang Anda ambil melebihi dari Sisa Kuitansi.',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
			cf_tonumber = jproduk_kwitansi_sisaField.getValue();
			jproduk_kwitansi_nilaiField.setValue(cf_tonumber);
			load_total_bayar();
			
			var number_tocf = CurrencyFormatted(cf_tonumber);
			this.setRawValue(number_tocf);
		}else{
			jproduk_kwitansi_nilaiField.setValue(cf_tonumber);
			load_total_bayar();
			
			var number_tocf = CurrencyFormatted(cf_value);
			this.setRawValue(number_tocf);
		}
	});
	jproduk_kwitansi_nilai2_cfField.on("keyup",function(){
		var cf_value = jproduk_kwitansi_nilai2_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		if(cf_tonumber>jproduk_kwitansi_sisa2Field.getValue()){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Maaf, Jumlah yang Anda ambil melebihi dari Sisa Kuitansi.',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
			cf_tonumber = jproduk_kwitansi_sisa2Field.getValue();
			jproduk_kwitansi_nilai2Field.setValue(cf_tonumber);
			load_total_bayar();
			
			var number_tocf = CurrencyFormatted(cf_tonumber);
			this.setRawValue(number_tocf);
		}else{
			jproduk_kwitansi_nilai2Field.setValue(cf_tonumber);
			load_total_bayar();
			
			var number_tocf = CurrencyFormatted(cf_value);
			this.setRawValue(number_tocf);
		}
	});
	jproduk_kwitansi_nilai3_cfField.on("keyup",function(){
		var cf_value = jproduk_kwitansi_nilai3_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		if(cf_tonumber>jproduk_kwitansi_sisa3Field.getValue()){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Maaf, Jumlah yang Anda ambil melebihi dari Sisa Kuitansi.',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
			cf_tonumber = jproduk_kwitansi_sisa3Field.getValue();
			jproduk_kwitansi_nilai3Field.setValue(cf_tonumber);
			load_total_bayar();
			
			var number_tocf = CurrencyFormatted(cf_tonumber);
			this.setRawValue(number_tocf);
		}else{
			jproduk_kwitansi_nilai3Field.setValue(cf_tonumber);
			load_total_bayar();
			
			var number_tocf = CurrencyFormatted(cf_value);
			this.setRawValue(number_tocf);
		}
	});
	//card
	jproduk_card_nilai_cfField.on("keyup",function(){
		var cf_value = jproduk_card_nilai_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_card_nilaiField.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_card_nilai2_cfField.on("keyup",function(){
		var cf_value = jproduk_card_nilai2_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_card_nilai2Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_card_nilai3_cfField.on("keyup",function(){
		var cf_value = jproduk_card_nilai3_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_card_nilai3Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	//cek/giro
	jproduk_cek_nilai_cfField.on("keyup",function(){
		var cf_value = jproduk_cek_nilai_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_cek_nilaiField.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_cek_nilai2_cfField.on("keyup",function(){
		var cf_value = jproduk_cek_nilai2_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_cek_nilai2Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_cek_nilai3_cfField.on("keyup",function(){
		var cf_value = jproduk_cek_nilai3_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_cek_nilai3Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	//transfer
	jproduk_transfer_nilai_cfField.on("keyup",function(){
		var cf_value = jproduk_transfer_nilai_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_transfer_nilaiField.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_transfer_nilai2_cfField.on("keyup",function(){
		var cf_value = jproduk_transfer_nilai2_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_transfer_nilai2Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_transfer_nilai3_cfField.on("keyup",function(){
		var cf_value = jproduk_transfer_nilai3_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_transfer_nilai3Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	//voucher
	jproduk_voucher_cashbackField.on("keyup",function(){if(jproduk_post2db=="CREATE"){load_total_bayar();}else if(jproduk_post2db=="UPDATE"){load_total_bayar_updating();}});
	jproduk_voucher_cashback2Field.on("keyup",function(){if(jproduk_post2db=="CREATE"){load_total_bayar();}else if(jproduk_post2db=="UPDATE"){load_total_bayar_updating();}});
	jproduk_voucher_cashback3Field.on("keyup",function(){if(jproduk_post2db=="CREATE"){load_total_bayar();}else if(jproduk_post2db=="UPDATE"){load_total_bayar_updating();}});
	//tunai
	jproduk_tunai_nilai_cfField.on("keyup",function(){
		var cf_value = jproduk_tunai_nilai_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_tunai_nilaiField.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_tunai_nilai2_cfField.on("keyup",function(){
		var cf_value = jproduk_tunai_nilai2_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_tunai_nilai2Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_tunai_nilai3_cfField.on("keyup",function(){
		var cf_value = jproduk_tunai_nilai3_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_tunai_nilai3Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	
	jproduk_voucher_cashback_cfField.on("keyup",function(){
		var cf_value = jproduk_voucher_cashback_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_voucher_cashbackField.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_voucher_cashback2_cfField.on("keyup",function(){
		var cf_value = jproduk_voucher_cashback2_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_voucher_cashback2Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	jproduk_voucher_cashback3_cfField.on("keyup",function(){
		var cf_value = jproduk_voucher_cashback3_cfField.getValue();
		var cf_tonumber = convertToNumber(cf_value);
		jproduk_voucher_cashback3Field.setValue(cf_tonumber);
		load_total_bayar();
		
		var number_tocf = CurrencyFormatted(cf_value);
		this.setRawValue(number_tocf);
	});
	
	//jproduk_caraField.on("select",update_group_carabayar_jual_produk);
	//jproduk_cara2Field.on("select",update_group_carabayar2_jual_produk);
	//jproduk_cara3Field.on("select",update_group_carabayar3_jual_produk);
	/*Sistem baru, dimana setiap kali user mengganti cara bayar, maka akan meng-reset ulang total perhitungan, hal ini dilakukan agar tidak terjadi bug(cara bayar sudah diisi nominal,lalu diganti ke cara bayar lain, lalu sudah mengurangi total biaya dan mengisi total bayar, sehingga ketika di save / print maka total bayar / total biaya sudah terakumulasi, namun cara bayarnya masih 0 (ditabel jual2 masih 0). Coba nyalakan comment diatas utk membuktikan bug tersebut */
	jproduk_caraField.on("select",function(){		
		if(jproduk_stat_dokField.getValue()=='Terbuka'){
			jproduk_ket_diskField.setDisabled(false);
			jproduk_cashback_cfField.setDisabled(false);
			jproduk_potong_pointcfField.setDisabled(false);
							
			jproduk_cara2Field.setDisabled(false);
			master_jual_produk_tunai2Group.setDisabled(false);
			master_jual_produk_card2Group.setDisabled(false);
			master_jual_produk_cek2Group.setDisabled(false);
			master_jual_produk_kwitansi2Group.setDisabled(false);
			master_jual_produk_transfer2Group.setDisabled(false);
			master_jual_produk_voucher2Group.setDisabled(false);
			
			jproduk_cara3Field.setDisabled(false);
			master_jual_produk_tunai3Group.setDisabled(false);
			master_jual_produk_card3Group.setDisabled(false);
			master_jual_produk_cek3Group.setDisabled(false);
			master_jual_produk_kwitansi3Group.setDisabled(false);
			master_jual_produk_transfer3Group.setDisabled(false);
			master_jual_produk_voucher3Group.setDisabled(false);		
		}
		
		update_group_carabayar_jual_produk();
		load_total_biaya();
		load_total_bayar();
		load_dstore_jproduk();
		jproduk_card_promoField.setValue("Normal");
	});
	jproduk_cara2Field.on("select",function(){
		update_group_carabayar2_jual_produk();
		load_total_biaya();
		load_total_bayar();
		load_dstore_jproduk();
		jproduk_card_promo2Field.setValue("Normal");
	});
	jproduk_cara3Field.on("select",function(){
		update_group_carabayar3_jual_produk();
		load_total_biaya();
		load_total_bayar();
		load_dstore_jproduk();
		jproduk_card_promo3Field.setValue("Normal");
	});
	
	var produk_list_kwitansi_DataStore = new Ext.data.Store({
		id: 'produk_list_kwitansi_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=kuitansi_list', 
			method: 'POST'
		}),
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'cust_id'
		},[
			{name: 'list_kw', type: 'string', mapping: 'list_kw'},
			{name: 'list_hutang', type: 'string', mapping: 'list_hutang'},
			{name: 'list_gv', type: 'string', mapping: 'list_gv'}						
		])
	});
	
	jproduk_custField.on("select",function(){
		var cust_id=jproduk_custField.getValue();
		if(cust_id!==0){
			var t_idx =cbo_cust_jual_produk_DataStore.findExact('cust_id',jproduk_custField.getValue(),0);
			t_member_level = cbo_cust_jual_produk_DataStore.getAt(t_idx).data.cust_member_level;

			memberDataStore.load({
					params : { member_cust: cust_id},
					callback: function(opts, success, response)  {
						 if (success) {
							if(memberDataStore.getCount()){
								jproduk_member_record=memberDataStore.getAt(0).data;
								if(jproduk_member_record.cust_no === ""){
									push_reset(0);
									jproduk_btn_cancel();
									Ext.MessageBox.show({
										   title: 'Warning',
										   msg: 'Customer belum memiliki no Client Card. Harap melakukan update data customer terlebih dahulu.',
										   buttons: Ext.MessageBox.OK,
										   animEl: 'save',
										   icon: Ext.MessageBox.WARNING
									});
								} else {
									if(t_member_level == 2){
										t_member_type = "Priority";
									} else {
										//t_member_type = "Regular";
										t_member_type = "";
									}

									if(t_member_level > 0)
										jproduk_cust_nomemberField.setText(jproduk_member_record.member_no + " (" + t_member_type + ")");
									else
										jproduk_cust_nomemberField.setText(jproduk_member_record.member_no);
										jproduk_cust_pointField.setText(CurrencyFormatted(jproduk_member_record.cust_preward_total));

									/*jproduk_ownerField.setValue(jproduk_member_record.cust_owner);
									jproduk_tahun_ambil_disk_ultah.setValue(jproduk_member_record.tahun_ambil_disk_ultah_ft);
									jproduk_tgl_ambil_disk_ultah.setValue(jproduk_member_record.tgl_ambil_disk_ultah_ft);*/
									//jproduk_valid_memberField.setValue(jproduk_member_record.member_valid);
									jproduk_cust_ultahField.setValue(jproduk_member_record.cust_tgllahir);
									
									if(jproduk_cust_ultahField.getValue()!==""){
									//	var post_ultah_produk = jproduk_cust_ultahField.getValue().add(Date.DAY, +14).format('m-d'); //sebelumnya +7
									//	var pre_ultah_produk = jproduk_cust_ultahField.getValue().add(Date.DAY, +0).format('m-d'); //sebelumnya -7
										var bulan_ultah_produk = jproduk_cust_ultahField.getValue().format('m');
									//	today_ultah_produk = new Date().format('m-d');
										var thismonth_ultah_produk = new Date().format('m');
										
									}
									
									//if(pre_ultah_produk <= today_ultah_produk && post_ultah_produk >= today_ultah_produk){
									if(bulan_ultah_produk == thismonth_ultah_produk && typeof(bulan_ultah_produk) !== "undefined"){
										Ext.example.msg('Informasi', 'Customer berulang tahun bulan ini', 'ok');
									}
									
									/*if(jproduk_tahun_ambil_disk_ultah.getValue() == this_year ){	//tahun ambil trakhir = tahun ini
									Ext.MessageBox.show({
										   title: 'INFORMASI',
										   msg: 'Diskon Ultah tahun ini telah dipakai pada tanggal '+jproduk_tgl_ambil_disk_ultah.getValue().format('d-m-Y'),
										   buttons: Ext.MessageBox.OK,
										   animEl: 'save'
										   //icon: Ext.MessageBox.OK
										});
									}
								
									if (cust_id== '9'){
										jproduk_karyawanField.setDisabled(false);
										jproduk_cust_priorityLabel.setText("");
										jproduk_groomingGroup.expand();
									}
									else if (cust_id !== '9' && jproduk_cust_priorityField.getValue()=='*') {
										jproduk_cust_priorityLabel.setText("*");
										jproduk_karyawanField.setDisabled(true);
										jproduk_groomingGroup.collapse();
									}
									else {
										jproduk_karyawanField.setDisabled(true);
										jproduk_karyawanField.setValue(null);
										jproduk_nikkaryawanField.setValue(null);
										jproduk_karyawan_levelField.setValue(null);
										jproduk_karyawan_jabatanField.setValue(null);
										jproduk_groomingGroup.collapse();*/
										jproduk_cust_priorityLabel.setText("");
										
									//}
								}

							}else{
								//jproduk_cust_nomemberField.setValue("");
								//jproduk_cust_nomemberField.setText("");
								//jproduk_valid_memberField.setValue("");
								jproduk_cust_ultahField.setValue("");
								jproduk_cust_priorityLabel.setText("");
								jproduk_ownerField.setValue("");
								jproduk_tahun_ambil_disk_ultah.setValue("");
								jproduk_tgl_ambil_disk_ultah.setValue("");
							}
						}
					}
			}); 
			
			/*produk_list_kwitansi_DataStore.load({
				params: {cust_id:cust_id},
				callback: function(opts, success, response)  {
					if (success) {
						//if(produk_list_kwitansi_DataStore.getAt(0).data.list_kw!=''){
						var kuitansi = '<b> <br><br> Customer memiliki Kuitansi : </b><br>'+ produk_list_kwitansi_DataStore.getAt(0).data.list_kw;
						var hutang = '<b> Customer memiliki Hutang : </b><br>'+ produk_list_kwitansi_DataStore.getAt(0).data.list_hutang;
						var gift_voucher = '<b> <br><br> Customer memiliki Gift Voucher : </b><br>'+ produk_list_kwitansi_DataStore.getAt(0).data.list_gv;
						
						var text = '';
						if(produk_list_kwitansi_DataStore.getAt(0).data.list_hutang!==''){
							text+=hutang;
						}
						if(produk_list_kwitansi_DataStore.getAt(0).data.list_kw!==''){
							text+=kuitansi;
						}						
						if(produk_list_kwitansi_DataStore.getAt(0).data.list_gv!==''){
							text+=gift_voucher;
						}			
						
						if(text!==''){
							var result=eval(response.responseText);
								Ext.MessageBox.show({
								   title: 'INFORMASI',
								   msg: text,
								   buttons: Ext.MessageBox.OK,
								   animEl: 'save',
								   fn: function(btn) {
										if (btn == 'ok') {
											cek_owner();
										}
									}
								});	
						}else{
							cek_owner();						
						}
					}
				}
			});*/
		
		}
		
		cbo_cust=cbo_cust_jual_produk_DataStore.findExact('cust_id',jproduk_custField.getValue(),0);
		if(cbo_cust>-1){
			//cbo_kwitansi_jual_produk_DataStore.load({params: {kwitansi_cust: cbo_cust_jual_produk_DataStore.getAt(cbo_cust).data.cust_id}});
			jproduk_cek_namaField.setValue(cbo_cust_jual_produk_DataStore.getAt(cbo_cust).data.cust_nama);
			jproduk_cek_nama2Field.setValue(cbo_cust_jual_produk_DataStore.getAt(cbo_cust).data.cust_nama);
			jproduk_cek_nama3Field.setValue(cbo_cust_jual_produk_DataStore.getAt(cbo_cust).data.cust_nama);
			jproduk_transfer_namaField.setValue(cbo_cust_jual_produk_DataStore.getAt(cbo_cust).data.cust_nama);
			jproduk_transfer_nama2Field.setValue(cbo_cust_jual_produk_DataStore.getAt(cbo_cust).data.cust_nama);
			jproduk_transfer_nama3Field.setValue(cbo_cust_jual_produk_DataStore.getAt(cbo_cust).data.cust_nama);
		}
		
		var catatan_cust=cbo_cust_jual_produk_DataStore.getAt(cbo_cust).data.cust_note;
		if(catatan_cust=="Y")
		{
		    load_catatan_customer(cust_id);
		}
		else{
		    jproduk_cust_noteField.setValue("");
		}
	});
	
	var customer_catatan_DataStore = new Ext.data.Store({
		id: 'customer_catatan_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_master_jual_produk&m=get_auto_catatan_customer2', 
			method: 'POST'
		}),
			reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'note_id'
		},[
			{name: 'note_all', type: 'string', mapping: 'note_all'}						
		])
	});
	
	function load_catatan_customer(cust_id2){
	    	
	    customer_catatan_DataStore.load({
		params : {note_customer : cust_id2},
		callback: function(opts, success, response)  {
			if (success) {
				if(customer_catatan_DataStore.getAt(0).data.note_all!=''){							
					jproduk_cust_noteField.setValue(customer_catatan_DataStore.getAt(0).data.note_all);
				}
			}
		}
	    }); 
	}
	
	function show_windowGrid(){
		master_jual_produk_DataStore.load({
			params: {start: 0, limit: jproduk_pageS},
			callback: function(opts, success, response){
				if(success){
					master_detail_jual_produk_DataStore.load();
					master_jual_produk_createWindow.show();
				}
			}
		});	// load DataStore
	}
	
	master_jual_produk_createForm = new Ext.FormPanel({
		title: 'Penjualan Produk',
		labelAlign: 'left',
		el: 'form_produk_addEdit',
		bodyStyle:'padding:5px',
		autoHeight:true,
		width: 	720,	//940,
		frame: true,
		items: [master_jual_produk_masterGroup/*,jproduk_groomingGroup*/, detail_jual_produkListEditorGrid/*,master_jual_produk_bayarGroup*/]
		,
		buttons: [
			{
				text: '<span style="font-weight:bold">(F1) Lihat Daftar</span>',
				handler: show_windowGrid
			},
			{
				text: 'Print Copy',
				ref: '../PrintOnlyButton',
				handler: print_only
			},
			
			{
				xtype:'spacer',
				width: 270
			},
			{
				text: '(F3) Reset',
				handler: function(){
					push_reset(0);
					jproduk_btn_cancel();
				}
			},
			{
				text: '(F4) Tunda',
				ref: '../jproduk_save',
				handler: save_button
			},
			{
				text: '(END) BAYAR',
				ref: '../jproduk_bayar',
				handler: bayar_button
			}/*
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			,
			
			{
				text: 'Final',
				ref: '../jproduk_savePrint',
				handler: save_andPrint
			},
			
			{
				text: 'Cancel',
				handler: function(){
					push_reset(0);
					jproduk_btn_cancel();
				}
			}
			<?php } ?>*/
		]
	});
	
	master_jual_produk_createWindow= new Ext.Window({
		id: 'master_jual_produk_createWindow',
		title: 'Daftar Penjualan Produk',
		closable:true,
		closeAction: 'hide',
		width: 940,
		autoHeight: true,
		x:0,
		y:0,
		plain:true,
		layout: 'fit',
		modal: true,
		renderTo: 'elwindow_master_jual_produk_create',
		items: [master_jual_produkListEditorGrid, master_detail_jual_produkListEditorGrid]
	});
	
	function master_jual_produk_list_search(){
		var jproduk_nobukti_search=null;
		var jproduk_cust_search=null;
		var jproduk_tanggal_search_date="";
		var jproduk_tanggal_akhir_search_date="";
		var jproduk_diskon_search=null;
		var jproduk_cara_search=null;
		var jproduk_keterangan_search=null;
		var jproduk_sortby_search=null;

		if(jproduk_nobuktiSearchField.getValue()!==null){jproduk_nobukti_search=jproduk_nobuktiSearchField.getValue();}
		if(jproduk_custSearchField.getValue()!==null){jproduk_cust_search=jproduk_custSearchField.getValue();}
		if(jproduk_tanggalSearchField.getValue()!==""){jproduk_tanggal_search_date=jproduk_tanggalSearchField.getValue().format('Y-m-d');}
		if(jproduk_tanggal_akhirSearchField.getValue()!==""){jproduk_tanggal_akhir_search_date=jproduk_tanggal_akhirSearchField.getValue().format('Y-m-d');}
		if(jproduk_diskonSearchField.getValue()!==null){jproduk_diskon_search=jproduk_diskonSearchField.getValue();}
		if(jproduk_caraSearchField.getValue()!==null){jproduk_cara_search=jproduk_caraSearchField.getValue();}
		if(jproduk_keteranganSearchField.getValue()!==null){jproduk_keterangan_search=jproduk_keteranganSearchField.getValue();}
		if(jproduk_stat_dokSearchField.getValue()!==null){jproduk_stat_dok_search=jproduk_stat_dokSearchField.getValue();}
		if(jproduk_sortbySearchField.getValue()!==null){jproduk_sortby_search=jproduk_sortbySearchField.getValue();}
		
		master_jual_produk_DataStore.baseParams = {
			task: 'SEARCH',
			start: 0,
			limit: jproduk_pageS,
			jproduk_nobukti		: jproduk_nobukti_search, 
			jproduk_cust		: jproduk_cust_search, 
			jproduk_tanggal		: jproduk_tanggal_search_date, 
			jproduk_tanggal_akhir	: jproduk_tanggal_akhir_search_date, 
			jproduk_diskon		: jproduk_diskon_search, 
			jproduk_cara		: jproduk_cara_search, 
			jproduk_keterangan	: jproduk_keterangan_search, 
			jproduk_stat_dok	: jproduk_stat_dok_search,
			jproduk_sortby   : jproduk_sortby_search
		};
		master_jual_produk_DataStore.reload({params: {start: 0, limit: jproduk_pageS}});
		master_detail_jual_produk_DataStore.reload({params: {master_id: 0, start: 0, limit: jproduk_pageS}});
	}
		
	function master_jual_produk_reset_search(){
		master_jual_produk_DataStore.baseParams = { task: 'LIST' };
		master_jual_produk_DataStore.reload({params: {start: 0, limit: jproduk_pageS}});
		master_detail_jual_produk_DataStore.reload({params: {master_id: 0, start: 0, limit: jproduk_pageS}});
	};

	function master_jual_produk_reset_SearchForm(){
		jproduk_nobuktiSearchField.reset();
		jproduk_custSearchField.reset();
		jproduk_tanggalSearchField.reset();
		jproduk_tanggal_akhirSearchField.reset();
		jproduk_tanggal_akhirSearchField.setValue(today);
		jproduk_diskonSearchField.reset();
		jproduk_caraSearchField.reset();
		jproduk_keteranganSearchField.reset();
		jproduk_stat_dokSearchField.reset();
	}
	
	jproduk_idSearchField= new Ext.form.NumberField({
		id: 'jproduk_idSearchField',
		fieldLabel: 'Jproduk Id',
		allowNegatife : false,
		blankText: '0',
		allowDecimals: false,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});

	jproduk_nobuktiSearchField= new Ext.form.TextField({
		id: 'jproduk_nobuktiSearchField',
		fieldLabel: 'No Faktur',
		maxLength: 30
	});

	jproduk_custSearchField= new Ext.form.ComboBox({
		id: 'jproduk_custSearchField',
		fieldLabel: 'Customer',
		store: cbo_cust_jual_produk_DataStore,
		mode: 'remote',
		displayField:'cust_nama',
		valueField: 'cust_id',
        typeAhead: false,
        loadingText: 'Searching...',
        pageSize:10,
        hideTrigger:false,
        tpl: customer_jual_produk_tpl,
        itemSelector: 'div.search-item',
		triggerAction: 'all',
		lazyRender:true,
		listClass: 'x-combo-list-small',
		anchor: '95%'
	});

	jproduk_tanggalSearchField= new Ext.form.DateField({
		id: 'jproduk_tanggalSearchField',
		fieldLabel: 'Tanggal',
		format : 'd-m-Y',
		minValue: MIN_CREATE_DATE
	});
	
	jproduk_tanggal_akhirSearchField= new Ext.form.DateField({
		id: 'jproduk_tanggal_akhirSearchField',
		fieldLabel: 's/d',
		format : 'd-m-Y',
		minValue: MIN_CREATE_DATE
	});

	jproduk_diskonSearchField= new Ext.form.NumberField({
		id: 'jproduk_diskonSearchField',
		fieldLabel: 'Diskon',
		allowNegatife : false,
		blankText: '0',
		allowDecimals: false,
		anchor: '95%',
		maskRe: /([0-9]+)$/
	});

	jproduk_caraSearchField= new Ext.form.ComboBox({
		id: 'jproduk_caraSearchField',
		fieldLabel: 'Cara Bayar',
		store:new Ext.data.SimpleStore({
			fields:['value', 'jproduk_cara'],
			data:[['tunai','Tunai'],['card','Kartu Kredit/Debit']/*,['kwitansi','Kuitansi/Gift Voucher'],['cek/giro','Cek/Giro'],['transfer','Transfer'],['vgrooming','Fasilitas Grooming']*/]
		}),
		mode: 'local',
		displayField: 'jproduk_cara',
		valueField: 'value',
		width: 96,
		triggerAction: 'all'	 
	});

	jproduk_keteranganSearchField= new Ext.form.TextArea({
		id: 'jproduk_keteranganSearchField',
		fieldLabel: 'Keterangan',
		maxLength: 250,
		anchor: '95%'	
	});
	jproduk_stat_dokSearchField= new Ext.form.ComboBox({
		id: 'jproduk_stat_dokSearchField',
		fieldLabel: 'Status Dokumen',
		store:new Ext.data.SimpleStore({
			fields:['value', 'jproduk_stat_dok'],
			data:[['Terbuka','Terbuka'], ['Tertutup','Tertutup'], ['Batal','Batal']]
		}),
		mode: 'local',
		displayField: 'jproduk_stat_dok',
		valueField: 'value',
		width: 96,
		triggerAction: 'all'
	});
	
	jproduk_sortbySearchField= new Ext.form.ComboBox({
		id: 'jproduk_sortbySearchField',
		fieldLabel: 'Urutkan',
		store:new Ext.data.SimpleStore({
			fields:['jproduk_sortby_value', 'jproduk_sortby_display'],
			data:[['jproduk_nobukti','No Faktur'], ['jproduk_date_create','Tgl dibuat']]
		}),
		mode: 'local',
		displayField: 'jproduk_sortby_display',
		valueField: 'jproduk_sortby_value',
		width: 96,
		triggerAction: 'all'
	});

	master_jual_produk_searchForm = new Ext.FormPanel({
		labelAlign: 'left',
		bodyStyle:'padding:5px',
		autoHeight:true,
		width: 500,        
		items: [{
			layout:'column',
			border:false,
			items:[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				items: [jproduk_nobuktiSearchField, jproduk_custSearchField, 
					{
						layout:'column',
						border:false,
						items:[
						{
							columnWidth:0.45,
							layout: 'form',
							border:false,
							defaultType: 'datefield',
							items: [						
								jproduk_tanggalSearchField
							]
						},
						{
							columnWidth:0.30,
							layout: 'form',
							border:false,
							labelWidth:30,
							defaultType: 'datefield',
							items: [						
								jproduk_tanggal_akhirSearchField
							]
						}						
				        ]
					},
				    jproduk_caraSearchField, 
				    jproduk_keteranganSearchField,
				    jproduk_stat_dokSearchField,
					jproduk_sortbySearchField
				] 
			}
			]
		}]
		,
		buttons: [{
				text: 'Cari',
				handler: master_jual_produk_list_search
			},{
				text: 'Close',
				handler: function(){
					master_jual_produk_searchWindow.hide();
				}
			}
		]
	});
	 
	master_jual_produk_searchWindow = new Ext.Window({
		title: 'Pencarian Penjualan Produk',
		closable:true,
		closeAction: 'hide',
		autoWidth: true,
		autoHeight: true,
		plain:true,
		layout: 'fit',
		x: 0,
		y: 0,
		modal: true,
		renderTo: 'elwindow_master_jual_produk_search',
		items: master_jual_produk_searchForm
	});
    
	function display_form_search_window(){
		if(!master_jual_produk_searchWindow.isVisible()){
			master_jual_produk_reset_SearchForm();
			master_jual_produk_searchWindow.show();
		} else {
			master_jual_produk_searchWindow.toFront();
		}
	}
	
	master_jual_produk_pembayaranForm = new Ext.FormPanel({
		title: '',
		labelAlign: 'left',
		el: 'form_produk_addBayar',
		bodyStyle:'padding:5px',
		closeAction: 'hide',
		autoHeight:true,
		width: 	600,	//940,
		frame: true,
		items: [master_jual_produk_bayarGroup]
		,
		buttons: [
			
			{
				xtype:'spacer',
				width: 250
			}
			<?php if(eregi('U|C',$this->m_security->get_access_group_by_kode('MENU_JUALPRODUK'))){ ?>
			,
			{
				text: '(END) Final',
				ref: '../jproduk_savePrint',
				handler: save_andPrint
			},
			{
				text: 'Cancel',
				handler: function(){
					push_reset(0);
					jproduk_btn_cancel_bayar();
				}
			}
			<?php } ?>
		]
	});
	
	master_jual_produk_pembayaranWindow = new Ext.Window({
		title: 'Pembayaran',
		closable:true,
		closeAction: 'hide',
		autoWidth: true,
		autoHeight: true,
		plain:true,
		layout: 'fit',
		x: 720,
		y: 10,
		modal: true,
		renderTo: 'elwindow_master_jual_produk_pembayaran',
		items: master_jual_produk_pembayaranForm
	});
    
	function master_jual_produk_print(){
		var searchquery = "";
		var jproduk_nobukti_print=null;
		var jproduk_cust_print=null;
		var jproduk_tanggal_print_date="";
		var jproduk_tanggal_akhir_print_date="";
		var jproduk_diskon_print=null;
		var jproduk_cara_print=null;
		var jproduk_keterangan_print=null;
		var win;              
		// check if we do have some search data...
		if(master_jual_produk_DataStore.baseParams.query!==null){searchquery = master_jual_produk_DataStore.baseParams.query;}
		if(master_jual_produk_DataStore.baseParams.jproduk_nobukti!==null){jproduk_nobukti_print = master_jual_produk_DataStore.baseParams.jproduk_nobukti;}
		if(master_jual_produk_DataStore.baseParams.jproduk_cust!==null){jproduk_cust_print = master_jual_produk_DataStore.baseParams.jproduk_cust;}
		if(master_jual_produk_DataStore.baseParams.jproduk_tanggal!==""){jproduk_tanggal_print_date = master_jual_produk_DataStore.baseParams.jproduk_tanggal;}
		if(master_jual_produk_DataStore.baseParams.jproduk_tanggal_akhir!==""){jproduk_tanggal_akhir_print_date = master_jual_produk_DataStore.baseParams.jproduk_tanggal_akhir;}
		if(master_jual_produk_DataStore.baseParams.jproduk_diskon!==null){jproduk_diskon_print = master_jual_produk_DataStore.baseParams.jproduk_diskon;}
		if(master_jual_produk_DataStore.baseParams.jproduk_cara!==null){jproduk_cara_print = master_jual_produk_DataStore.baseParams.jproduk_cara;}
		if(master_jual_produk_DataStore.baseParams.jproduk_keterangan!==null){jproduk_keterangan_print = master_jual_produk_DataStore.baseParams.jproduk_keterangan;}
		if(master_jual_produk_DataStore.baseParams.jproduk_stat_dok!==null){jproduk_stat_dok_print = master_jual_produk_DataStore.baseParams.jproduk_stat_dok;}

		Ext.Ajax.request({   
		waitMsg: 'Mohon tunggu...',
		url: 'index.php?c=c_master_jual_produk&m=get_action',
		params: {
			task: "PRINT",
		  	query: searchquery,                    		// if we are doing a quicksearch, use this
			jproduk_nobukti		: jproduk_nobukti_print, 
			jproduk_cust		: jproduk_cust_print, 
			jproduk_tanggal		: jproduk_tanggal_print_date, 
			jproduk_tanggal_akhir	: jproduk_tanggal_akhir_print_date, 
			jproduk_diskon		: jproduk_diskon_print, 
			jproduk_cara		: jproduk_cara_print, 
			jproduk_keterangan	: jproduk_keterangan_print, 
			jproduk_stat_dok	: jproduk_stat_dok_print,
		  	currentlisting: master_jual_produk_DataStore.baseParams.task // this tells us if we are searching or not
		}, 
		success: function(response){              
		  	var result=eval(response.responseText);
		  	switch(result){
		  	case 1:
				win = window.open('./print/master_jual_produklist.html','master_jual_produklist','height=400,width=900,resizable=1,scrollbars=1, menubar=1');
				break;
		  	default:
				Ext.MessageBox.show({
					title: 'Warning',
					msg: 'Unable to print the grid!',
					buttons: Ext.MessageBox.OK,
					animEl: 'save',
					icon: Ext.MessageBox.WARNING
				});
				break;
		  	}  
		},
		failure: function(response){
		  	var result=response.responseText;
			Ext.MessageBox.show({
			   title: 'Error',
			   msg: 'Could not connect to the database. retry later.',
			   buttons: Ext.MessageBox.OK,
			   animEl: 'database',
			   icon: Ext.MessageBox.ERROR
			});		
		} 	                     
		});
	}
	
	function master_jual_produk_export_excel(opsi_excel){
		var searchquery = "";
		var jproduk_nobukti_2excel=null;
		var jproduk_cust_2excel=null;
		var jproduk_tanggal_2excel_date="";
		var jproduk_tanggal_akhir_2excel_date="";
		var jproduk_diskon_2excel=null;
		var jproduk_cara_2excel=null;
		var jproduk_keterangan_2excel=null;
		var win;              
		// check if we do have some search data...
		if(master_jual_produk_DataStore.baseParams.query!==null){searchquery = master_jual_produk_DataStore.baseParams.query;}
		if(master_jual_produk_DataStore.baseParams.jproduk_nobukti!==null){jproduk_nobukti_2excel = master_jual_produk_DataStore.baseParams.jproduk_nobukti;}
		if(master_jual_produk_DataStore.baseParams.jproduk_cust!==null){jproduk_cust_2excel = master_jual_produk_DataStore.baseParams.jproduk_cust;}
		if(master_jual_produk_DataStore.baseParams.jproduk_tanggal!==""){jproduk_tanggal_2excel_date = master_jual_produk_DataStore.baseParams.jproduk_tanggal;}
		if(master_jual_produk_DataStore.baseParams.jproduk_tanggal_akhir!==""){jproduk_tanggal_akhir_2excel_date = master_jual_produk_DataStore.baseParams.jproduk_tanggal_akhir;}
		if(master_jual_produk_DataStore.baseParams.jproduk_diskon!==null){jproduk_diskon_2excel = master_jual_produk_DataStore.baseParams.jproduk_diskon;}
		if(master_jual_produk_DataStore.baseParams.jproduk_cara!==null){jproduk_cara_2excel = master_jual_produk_DataStore.baseParams.jproduk_cara;}
		if(master_jual_produk_DataStore.baseParams.jproduk_keterangan!==null){jproduk_keterangan_2excel = master_jual_produk_DataStore.baseParams.jproduk_keterangan;}
		if(master_jual_produk_DataStore.baseParams.jproduk_stat_dok!==null){jproduk_stat_dok_2excel = master_jual_produk_DataStore.baseParams.jproduk_stat_dok;}
		
		Ext.Ajax.request({   
		waitMsg: 'Mohon tunggu...',
		url: 'index.php?c=c_master_jual_produk&m=get_action',
		params: {
			task: "EXCEL",
		  	query: searchquery,                    		// if we are doing a quicksearch, use this
			jproduk_nobukti		: jproduk_nobukti_2excel, 
			jproduk_cust		: jproduk_cust_2excel, 
			jproduk_tanggal		: jproduk_tanggal_2excel_date, 
			jproduk_tanggal_akhir	: jproduk_tanggal_akhir_2excel_date, 
			jproduk_diskon		: jproduk_diskon_2excel, 
			jproduk_cara		: jproduk_cara_2excel, 
			jproduk_keterangan	: jproduk_keterangan_2excel, 
			jproduk_stat_dok	: jproduk_stat_dok_2excel,
			opsi_excel			: opsi_excel,
		  	currentlisting: master_jual_produk_DataStore.baseParams.task // this tells us if we are searching or not
		},
		success: function(response){              
		  	var result=eval(response.responseText);
		  	switch(result){
		  	case 1:
				win = window.location=('./export2excel.php');
				break;
		  	default:
				Ext.MessageBox.show({
					title: 'Warning',
					msg: 'Unable to convert excel the grid!',
					buttons: Ext.MessageBox.OK,
					animEl: 'save',
					icon: Ext.MessageBox.WARNING
				});
				break;
		  	}  
		},
		failure: function(response){
		  	var result=response.responseText;
			Ext.MessageBox.show({
			   title: 'Error',
			   msg: 'Could not connect to the database. retry later.',
			   buttons: Ext.MessageBox.OK,
			   animEl: 'database',
			   icon: Ext.MessageBox.ERROR
			});    
		} 	                     
		});
	}
	
	function jproduk_btn_cancel_bayar(){
		master_jual_produk_pembayaranWindow.hide();
	}
	
	function jproduk_btn_cancel(){
		master_jual_produk_reset_form();
		detail_jual_produk_DataStore.load({params: {master_id:-1}});
		//jproduk_caraField.setValue("card");
		jproduk_caraField.setValue("tunai");
		jproduk_card_promoField.setValue("Normal");
		//master_jual_produk_cardGroup.setVisible(true);
		master_jual_produk_tunaiGroup.setVisible(true);
		master_cara_bayarTabPanel.setActiveTab(0);
		jproduk_post2db="CREATE";
		jproduk_diskonField.setValue(0);
		jproduk_cashbackField.setValue(0);		
		jproduk_potong_pointField.setValue(0);		
		jproduk_diskonField.allowBlank=true;
		jproduk_pesanLabel.setText('');
		jproduk_lunasLabel.setText('');
		jproduk_cust_priorityLabel.setText('');
		jproduk_cust_noteField.setValue("");
		jproduk_uangField.setValue(0);
		jproduk_uang_cfField.setValue(0);
		kembalian_label.setValue("");
		detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
	}
	
	function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); };

	//fungsi untuk menambahkan data dari form detail ke grid detail
	function add_dproduk2grid(data){
		var idxDProduk = detail_jual_produk_DataStore.findExact('dproduk_produk', data.dproduk_produk_value, 0);

		var jdiskon_tbar = "Tanpa Diskon";
		var diskon_tbar = 0;
		if(jproduk_cust_nomemberField.getEl().dom.innerHTML!==""){
			jdiskon_tbar = "Member";
			diskon_tbar = data.dproduk_produk_dm;
		}

		var subtotal_net_tbar;
		if (data.djproduk_satuan_nilai == 1) {
			subtotal_net_tbar = ((100 - diskon_tbar) / 100) * parseFloat(data.dproduk_produk_harga);
			subtotal_net_tbar = (subtotal_net_tbar>0?Math.round(subtotal_net_tbar):0);
		} else {
			subtotal_net_tbar = ((100 - diskon_tbar) / 100) * ((data.djproduk_satuan_nilai*(1/data.djproduk_satuan_nilai))*parseFloat(data.dproduk_produk_harga));
			subtotal_net_tbar = (subtotal_net_tbar>0?Math.round(subtotal_net_tbar):0);
		}

		var edit_detail_jual_produk= new detail_jual_produkListEditorGrid.store.recordType({
			dproduk_id	:0,
			dproduk_produk	:data.dproduk_produk_value,
			dproduk_satuan	:data.djproduk_satuan_value,
			dproduk_jumlah	:1,
			dproduk_harga	:data.dproduk_produk_harga,
			dproduk_subtotal:1*data.dproduk_produk_harga,
			dproduk_diskon_jenis: jdiskon_tbar,
			dproduk_diskon	:diskon_tbar,
			dproduk_subtotal_net:subtotal_net_tbar,
			dproduk_karyawan:0,
			produk_harga_default:data.dproduk_produk_harga
		});

		if (idxDProduk>(-1)) {
			record = detail_jual_produk_DataStore.getAt(idxDProduk);
			console.log("index record yang sama....");
			console.log(record.data);
			record.set('dproduk_jumlah', record.data.dproduk_jumlah + 1);
			record.commit();
		} else {
			cbo_dproduk_satuanDataStore.setBaseParam('produk_id', Ext.getCmp('produk_id_tbar').getValue());
			cbo_dproduk_satuanDataStore.load({
				callback: function(r,opt,success){
					if(success==true){
						detail_jual_produk_DataStore.insert(0, edit_detail_jual_produk);
						combo_jual_produk.setValue(Ext.getCmp('produk_id_tbar').getValue());
						detail_jual_produk_DataStore.commitChanges();
					}
				}
			});
		}
		
		detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
	};
	
	//master_jual_produk_card2Group.on("click",function(){
	//	if(jproduk_cara2Field.getValue() !== "") master_jual_produk_card2Group.setVisible(false);
	//}
	
	function pertamax(){	
		jproduk_post2db="CREATE";
		jproduk_stat_dokField.setValue('Terbuka');
		jproduk_tanggalField.setValue(dt.format('Y-m-d'));
		master_jual_produk_createForm.render();	
		//jproduk_caraField.setValue('card');
		jproduk_caraField.setValue('tunai');
		jproduk_card_promoField.setValue("Normal");
		//master_jual_produk_cardGroup.setVisible(true);
		master_jual_produk_tunaiGroup.setVisible(true);
		jproduk_cashbackField.setValue(0);
		jproduk_potong_pointField.setValue(0);
		jproduk_uangField.setValue(0);
		jproduk_uang_cfField.setValue(0);
		jproduk_diskonField.setValue(0);
		jproduk_diskonField.allowBlank=true;
		jproduk_karyawanField.setDisabled(true);
		detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
		
	}
	pertamax();
	
	$login = <?=$_SESSION[SESSION_GROUPID];?>;   
	cbo_cust_jual_produk_DataStore.load({
	params: {cust_id	: 10},
		callback: function(opts, success, response)  {
				if (success) {
				  if(cbo_cust_jual_produk_DataStore.getCount()!=0){
					  var cust_nama = null;
					  cust_record=cbo_cust_jual_produk_DataStore.getAt(0).data;
					  jproduk_custField.setValue(cust_record.cust_nama);
				  }
				}
			}
	});
		
	detail_jual_produkListEditorGrid.djproduk_add_produk.focus(true,500);
});
	</script>
<body>
<div>
	<div class="col">
        <div id="fp_master_jual_produk"></div>
		<div id="fp_master_detail_jual_produk"></div>
		<div id="fp_detail_jual_produk"></div>
		<div id="elwindow_master_jual_produk_confirm"></div>
		<div id="elwindow_master_jual_produk_confirm_hutang"></div>
		<div id="elwindow_master_jual_produk_create"></div>
        <div id="elwindow_master_jual_produk_search"></div>
        <div id="elwindow_master_jual_produk_pembayaran"></div>
        <div id="form_produk_addEdit"></div>
        <div id="form_produk_addBayar"></div>
    </div>
</div>
</body>