<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<style type="text/css">
	p { width:650px; }
	.search-item {
		font:normal 11px tahoma, arial, helvetica, sans-serif;
		padding:3px 10px 3px 10px;
		border:1px solid #fff;
		border-bottom:1px solid #eeeeee;
		white-space:normal;
		color:#555;
	}
	.search-item h3 {
		display:block;
		font:inherit;
		font-weight:bold;
		color:#222;
	}
	
	.search-item h3 span {
		float: right;
		font-weight:normal;
		margin:0 0 5px 5px;
		width:100px;
		display:block;	
		clear:none;
	}
</style>
<script>

var today=new Date().format('Y-m-d');
var yesterday=new Date().add(Date.DAY, -1).format('Y-m-d');
var thisday=new Date().format('d');
var thismonth=new Date().format('m');
var thisyear=new Date().format('Y');
var welcome_chart_bulan_awal	= "";
var welcome_chart_tahun_awal 	= "";
var welcome_chart_bulan_akhir 	= "";
var welcome_chart_tahun_akhir 	= "";
var tgl_awal = "";
var tgl_akhir = "";
var main_cabang = "<?php $cabang_brand=$this->m_public_function->get_cabang_brand(); echo $cabang_brand; ?>";
var cabang_value = "<?php $cabang_value=$this->m_public_function->get_cabang_value(); echo $cabang_value; ?>";

<?
$idForm=24;
?>

Ext.apply(Ext.form.VTypes, {
    daterange : function(val, field) {
        var date = field.parseDate(val);

        if(!date){
            return;
        }
        if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            var start = Ext.getCmp(field.startDateField);
            start.setMaxValue(date);
            start.validate();
            this.dateRangeMax = date;
        } 
        else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            var end = Ext.getCmp(field.endDateField);
            end.setMinValue(date);
            end.validate();
            this.dateRangeMin = date;
        }
        return true;
    }
});
<?
$tahun="[";
for($i=(date('Y')-4);$i<=date('Y');$i++){
	$tahun.="['$i'],";
}
$tahun=substr($tahun,0,strlen($tahun)-1);
$tahun.="]";
$bulan="";
?>
Ext.onReady(function(){
  Ext.QuickTips.init();

	welcome_chartDataStore = new Ext.data.Store({
		id: 'welcome_chartDataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action', 
			method: 'POST',
			timeout: 1200000
		}),
		baseParams:{task: "LIST",start:0}, // parameter yang di $_POST ke Controller
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: ''
		},[
		/* dataIndex => insert into rekap_penjualanColumnModel, Mapping => for initiate table column */
			{name: 'td_kategori', type: 'string', mapping: 'td_kategori'},
			{name: 'td_target', type: 'float', mapping: 'td_target'},
			{name: 'td_pencapaian', type: 'float', mapping: 'td_pencapaian'},
			{name: 'td_persen', type: 'float', mapping: 'td_persen'},
			{name: 'td_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'td_date_update'}
		]),
		//sortInfo:{field: 'tot_net', direction: "DESC"}
	});
	
	welcome_chartColumnModel = new Ext.grid.ColumnModel(
		[{	
			align : 'Left',
			header: '<div align="center">' + 'Kategori' + '</div>',
			dataIndex: 'td_kategori',
			readOnly: true,
			width: 80,	//55,
			sortable: true,
			renderer: function(value, cell, record){
				if(value == 'Net Sales Estetik' && main_cabang == 'PUR')
					value = 'Net Sales Gym';
				
				return value;
			}
		},{	
			align : 'Right',
			header: '<div align="center">' + 'Target' + '</div>',
			dataIndex: 'td_target',
			renderer: Ext.util.Format.numberRenderer('0,000'),
			readOnly: true,
			width: 60,
			sortable: true
		},{	
			align : 'Right',
			header: '<div align="center">' + 'Pencapaian' + '</div>',
			dataIndex: 'td_pencapaian',
			renderer: Ext.util.Format.numberRenderer('0,000'),
			readOnly: true,
			width: 60,	//55,
			sortable: true
		},{	
			align : 'Right',
			header: '<div align="center">' + '(%)' + '</div>',
			dataIndex: 'td_persen',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
			readOnly: true,
			width: 30,	//55,
			sortable: true
		}
	]);
	
	welcome_chartColumnModel.defaultSortable= true;

	welcome_chart_bulan_awalField=new Ext.form.ComboBox({
		id:'welcome_chart_bulan_awalField',
		fieldLabel:'Periode ',
		store:new Ext.data.SimpleStore({
			fields:['value', 'display'],
			data:[['01','Januari'],['02','Pebruari'],['03','Maret'],['04','April'],['05','Mei'],['06','Juni'],['07','Juli'],['08','Agustus'],['09','September'],['10','Oktober'],['11','Nopember'],['12','Desember']]
		}),
		mode: 'local',
		displayField: 'display',
		valueField: 'value',
		value: thismonth,
		width: 85,
		triggerAction: 'all'
	});
	
	welcome_chart_bulan_awalField.on('select', function(){
		var t_periode_awal = welcome_chart_bulan_awalField.getValue() + "-" + welcome_chart_tahun_awalField.getValue();
		var t_periode_akhir = welcome_chart_bulan_akhirField.getValue() + "-" + welcome_chart_tahun_akhirField.getValue();

		if(t_periode_awal == t_periode_akhir){
			welcome_chart_rekap_jual_orderField.enable();
		} else {
			welcome_chart_rekap_jual_orderField.setValue('rp');
			welcome_chart_rekap_jual_orderField.disable();
		}
	});

	welcome_chart_tahun_awalField=new Ext.form.ComboBox({
		id:'welcome_chart_tahun_awalField',
		store:new Ext.data.SimpleStore({
			fields:['tahun'],
			data: <?php echo $tahun; ?>
		}),
		mode: 'local',
		displayField: 'tahun',
		valueField: 'tahun',
		value: thisyear,
		width: 60,
		triggerAction: 'all'
	});

	welcome_chart_tahun_awalField.on('select', function(){
		var t_periode_awal = welcome_chart_bulan_awalField.getValue() + "-" + welcome_chart_tahun_awalField.getValue();
		var t_periode_akhir = welcome_chart_bulan_akhirField.getValue() + "-" + welcome_chart_tahun_akhirField.getValue();

		if(t_periode_awal == t_periode_akhir){
			welcome_chart_rekap_jual_orderField.enable();
		} else {
			welcome_chart_rekap_jual_orderField.setValue('rp');
			welcome_chart_rekap_jual_orderField.disable();
		}
	});

	welcome_chart_bulan_akhirField=new Ext.form.ComboBox({
		id:'welcome_chart_bulan_akhirField',
		fieldLabel:'s/d',
		store:new Ext.data.SimpleStore({
			fields:['value', 'display'],
			data:[['01','Januari'],['02','Pebruari'],['03','Maret'],['04','April'],['05','Mei'],['06','Juni'],['07','Juli'],['08','Agustus'],['09','September'],['10','Oktober'],['11','Nopember'],['12','Desember']]
		}),
		mode: 'local',
		displayField: 'display',
		valueField: 'value',
		value: thismonth,
		width: 85,
		triggerAction: 'all'
	});
	
	welcome_chart_bulan_akhirField.on('select', function(){
		var t_periode_awal = welcome_chart_bulan_awalField.getValue() + "-" + welcome_chart_tahun_awalField.getValue();
		var t_periode_akhir = welcome_chart_bulan_akhirField.getValue() + "-" + welcome_chart_tahun_akhirField.getValue();

		if(t_periode_awal == t_periode_akhir){
			welcome_chart_rekap_jual_orderField.enable();
		} else {
			welcome_chart_rekap_jual_orderField.setValue('rp');
			welcome_chart_rekap_jual_orderField.disable();
		}
	});

	welcome_chart_tahun_akhirField=new Ext.form.ComboBox({
		id:'welcome_chart_tahun_akhirField',
		store:new Ext.data.SimpleStore({
			fields:['tahun'],
			data: <?php echo $tahun; ?>
		}),
		mode: 'local',
		displayField: 'tahun',
		valueField: 'tahun',
		value: thisyear,
		width: 60,
		triggerAction: 'all'
	});
	
	welcome_chart_tahun_akhirField.on('select', function(){
		var t_periode_awal = welcome_chart_bulan_awalField.getValue() + "-" + welcome_chart_tahun_awalField.getValue();
		var t_periode_akhir = welcome_chart_bulan_akhirField.getValue() + "-" + welcome_chart_tahun_akhirField.getValue();

		if(t_periode_awal == t_periode_akhir){
			welcome_chart_rekap_jual_orderField.enable();
		} else {
			welcome_chart_rekap_jual_orderField.setValue('rp');
			welcome_chart_rekap_jual_orderField.disable();
		}
	});

	welcome_chart_rekap_jual_orderField=new Ext.form.ComboBox({
		id:'welcome_chart_rekap_jual_orderField',
		fieldLabel:' ',
		emptyText : 'Urutkan',
		store:new Ext.data.SimpleStore({
			fields:['value', 'display'],
			data:[['rp','Rp'],['jml','Jml']/*,['jml2','Jml (Rp>0)']*/]
		}),
		mode: 'local',
		displayField: 'display',
		valueField: 'value',
		value: thismonth,
		width: 80,
		triggerAction: 'all'
	});

	welcome_chart_rekap_jual_orderField.on('select', function(){
		if(welcome_chart_rekap_jual_orderField.getValue() == 'jml2'){
			Ext.MessageBox.show({
			   title: 'Info',
			   msg: 'Opsi Jml (Rp>0) hanya berlaku untuk Top 10 Penjualan Produk.',
			   buttons: Ext.MessageBox.OK,
			   animEl: 'database',
			   icon: Ext.MessageBox.INFO
			});
		}
	});

	welcome_chart_btnSearch=new Ext.Button({
		text: 'Tampilkan',
		iconCls:'icon-search',
		handler: dashboard_search
	});

	welcome_chart_btnUpdate=new Ext.Button({
		text: 'Hitung',
		iconCls:'icon-refresh',
		handler: dashboard_update
	});

	function cek_periode(fn_name){
		var t_periode_awal = welcome_chart_bulan_awalField.getValue() + "-" + welcome_chart_tahun_awalField.getValue();
		var t_periode_akhir = welcome_chart_bulan_akhirField.getValue() + "-" + welcome_chart_tahun_akhirField.getValue();

		if(t_periode_awal != t_periode_akhir){
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Hitung ulang hanya dapat dilakukan pada periode yang sama.',
				buttons: Ext.MessageBox.OK,
				animEl: 'save',
				icon: Ext.MessageBox.WARNING
			});
			return false;
		} else {
			cek_tutup_buku(fn_name);
		}		
	}

	function cek_tutup_buku(fn_name){
		Ext.Ajax.request({
			waitMsg: 'Please Wait...',
			url: 'index.php?c=c_welcome_chart&m=get_action',
			params: {
				task: 'CEK_TUTUP_BUKU',						
				method: 'POST',
				bulan	: welcome_chart_bulan_awalField.getValue(),
				tahun	: welcome_chart_tahun_awalField.getValue()
			},
			success: function(result, request){
				var check = eval(result.responseText);

				if(check.status > 0){
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Hitung ulang tidak dapat dilakukan karena sudah dilakukan tutup buku pada periode tersebut hingga ' + check.last,
						buttons: Ext.MessageBox.OK,
						animEl: 'save',
						icon: Ext.MessageBox.WARNING
					});
					return false;
				} else {
					if(fn_name == 'dashboard_update')
						dashboard_update();
					else if(fn_name == 'welcome_chart_update')
						welcome_chart_update();
					/*else if(fn_name == 'welcome_chart_kontribusi_update')
						welcome_chart_kontribusi_update();*/
					else if(fn_name == 'welcome_chart_netsales_linier_update')
						welcome_chart_netsales_linier_update();
					else if(fn_name == 'welcome_chart_rekap_jual_update')
						welcome_chart_rekap_jual_update();
					/*else if(fn_name == 'welcome_chart_top_spender_update')
						welcome_chart_top_spender_update();*/
					else if(fn_name == 'welcome_chart_stok_minimum_update')
						welcome_chart_stok_minimum_update();
				}
			},
			failure: function(response){
				Ext.MessageBox.hide();
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: "Failed Connection",
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});
			}
		});
	}

	function welcome_chart_update(mode){
		Ext.MessageBox.show({
			msg: 'Sedang memproses data Pencapaian Target, mohon tunggu...',
			progressText: 'proses...',
			width:350,
			wait:true
		});

		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		welcome_chartDataStore.baseParams = {
						task		: 'RECALC',
						bulan_awal	: welcome_chart_bulan_awal,
						tahun_awal	: welcome_chart_tahun_awal,
						bulan_akhir	: welcome_chart_bulan_akhir,
						tahun_akhir	: welcome_chart_tahun_akhir						
		};
		welcome_chartDataStore.reload({
			callback: function(r,opt,success){
				welcome_chartDataStore.baseParams = {
								task		: 'UPDATE',
								bulan_awal	: welcome_chart_bulan_awal,
								tahun_awal	: welcome_chart_tahun_awal,
								bulan_akhir	: welcome_chart_bulan_akhir,
								tahun_akhir	: welcome_chart_tahun_akhir						
				};
				welcome_chartDataStore.reload({
					callback: function(r,opt,success){
						Ext.MessageBox.hide();
						welcome_chart_search();

						if(mode == 'all')
							welcome_chart_rekap_jual_update(mode);	
							welcome_chart_stok_minimum_update(mode);	
						
					}
				});
			}
		});
	}

	function welcome_chart_search(loading){
		
		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }
		
		if(loading){
			Ext.MessageBox.show({
				msg: 'Sedang memproses data, mohon tunggu...',
				progressText: 'proses...',
				width:350,
				wait:true
			});
		}

		welcome_chartDataStore.baseParams = {
			task		: 'SEARCH',
			bulan_awal	: welcome_chart_bulan_awal,
			tahun_awal	: welcome_chart_tahun_awal,
			bulan_akhir	: welcome_chart_bulan_akhir,
			tahun_akhir	: welcome_chart_tahun_akhir						
		};
		welcome_chartDataStore.reload({
			callback: function(r, opt, success){
				if(loading)
					Ext.MessageBox.hide();

				welcome_chartListEditorGrid.setTitle('Pencapaian Target');
				welcome_chart_netsales_linier.setTitle('Net Sales');
				welcome_chart_proporsi.setTitle('Proporsi');
				if(welcome_chartDataStore.getCount() > 0){
					var t_data = welcome_chartDataStore.getAt(0).data;

					if(t_data.td_date_update){
						welcome_chartListEditorGrid.setTitle('Pencapaian Target <span style="font-size:10px;float: right;">Last update: ' + t_data.td_date_update.format('d-m-Y H:i') + '</span>');
						welcome_chart_netsales_linier.setTitle('Net Sales <span style="font-size:10px;float: right;">Last update: ' + t_data.td_date_update.format('d-m-Y H:i') + '</span>');
						welcome_chart_proporsi.setTitle('Proporsi <span style="font-size:10px;float: right;">Last update: ' + t_data.td_date_update.format('d-m-Y H:i') + '</span>');
					}
				}

				welcome_chart_proporsi.update("<iframe width='50%' height='100%' style='float: left' frameborder='0' src='<?=base_url();?>print/proporsi_netsales_graph.php'></iframe>&nbsp;<iframe width='50%' height='100%' style='float: left; margin-top: -15px' frameborder='0' src='<?=base_url();?>print/proporsi_kunjungan_graph.php'></iframe>");
			}
		});

		/* Ditutup sementara karena dashboard yang baru tidak menampilkan (hide) gauge chart

		//NETSALES
		Ext.Ajax.request({
			waitMsg: 'Please Wait...',
			url: 'index.php?c=c_welcome_chart&m=get_action',
			params: {
				task: 'CHART_NETSALES',						
				method: 'POST',
				bulan_awal	: welcome_chart_bulan_awalField.getValue(),
				tahun_awal	: welcome_chart_tahun_awalField.getValue(),
				bulan_akhir	: welcome_chart_bulan_akhirField.getValue(),
				tahun_akhir	: welcome_chart_tahun_akhirField.getValue()
			},
			success: function(result, request){
				var hasil=eval(result.responseText);
				if (hasil > 0 )
				{
					welcome_chart_netsales.update("<iframe frameborder='0' width='100%' height='100%' src='http://"+"<? echo $_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT']?>"+"/mis2/index.php?c=c_dashboard_chart&n=total&nilai="+hasil+"'></iframe>");					
				}
			},
			failure: function(response){
				Ext.MessageBox.hide();
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: FAILED_CONNECTION,
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});
			}
		});

		//NETSALES MEDIS
		Ext.Ajax.request({
			waitMsg: 'Please Wait...',
			url: 'index.php?c=c_welcome_chart&m=get_action',
			params: {
				task: 'CHART_NETSALES_MEDIS',						
				method: 'POST',
				bulan_awal	: welcome_chart_bulan_awalField.getValue(),
				tahun_awal	: welcome_chart_tahun_awalField.getValue(),
				bulan_akhir	: welcome_chart_bulan_akhirField.getValue(),
				tahun_akhir	: welcome_chart_tahun_akhirField.getValue()
			},
			success: function(result, request){
				var hasil=eval(result.responseText);
				if (hasil > 0 )
				{
					welcome_chart_netsales_medis.update("<iframe frameborder='0' width='100%' height='100%' src='http://"+"<? echo $_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT']?>"+"/mis2/index.php?c=c_dashboard_chart&n=total&nilai="+hasil+"'></iframe>");					
				}
			},
			failure: function(response){
				Ext.MessageBox.hide();
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: FAILED_CONNECTION,
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});
			}
		});		

		//NETSALES ESTETIK
		Ext.Ajax.request({
			waitMsg: 'Please Wait...',
			url: 'index.php?c=c_welcome_chart&m=get_action',
			params: {
				task: 'CHART_NETSALES_ESTETIK',						
				method: 'POST',
				bulan_awal	: welcome_chart_bulan_awalField.getValue(),
				tahun_awal	: welcome_chart_tahun_awalField.getValue(),
				bulan_akhir	: welcome_chart_bulan_akhirField.getValue(),
				tahun_akhir	: welcome_chart_tahun_akhirField.getValue()
			},
			success: function(result, request){
				var hasil=eval(result.responseText);
				if (hasil > 0 )
				{
					welcome_chart_netsales_estetik.update("<iframe frameborder='0' width='100%' height='100%' src='http://"+"<? echo $_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT']?>"+"/mis2/index.php?c=c_dashboard_chart&n=total&nilai="+hasil+"'></iframe>");					
				}
			},
			failure: function(response){
				Ext.MessageBox.hide();
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: FAILED_CONNECTION,
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});
			}
		});

		//NETSALES PRODUK
		Ext.Ajax.request({
			waitMsg: 'Please Wait...',
			url: 'index.php?c=c_welcome_chart&m=get_action',
			params: {
				task: 'CHART_NETSALES_PRODUK',						
				method: 'POST',
				bulan_awal	: welcome_chart_bulan_awalField.getValue(),
				tahun_awal	: welcome_chart_tahun_awalField.getValue(),
				bulan_akhir	: welcome_chart_bulan_akhirField.getValue(),
				tahun_akhir	: welcome_chart_tahun_akhirField.getValue()
			},
			success: function(result, request){										
				var hasil=eval(result.responseText);
				if (hasil > 0 )
				{
					welcome_chart_netsales_produk.update("<iframe frameborder='0' width='100%' height='100%' src='http://"+"<? echo $_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT']?>"+"/mis2/index.php?c=c_dashboard_chart&n=total&nilai="+hasil+"'></iframe>");					
				}
			},
			failure: function(response){
				Ext.MessageBox.hide();
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: FAILED_CONNECTION,
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});
			}
		});

		//KUNJUNGAN
		Ext.Ajax.request({
			waitMsg: 'Please Wait...',
			url: 'index.php?c=c_welcome_chart&m=get_action',
			params: {
				task: 'CHART_KUNJUNGAN',						
				method: 'POST',
				bulan_awal	: welcome_chart_bulan_awalField.getValue(),
				tahun_awal	: welcome_chart_tahun_awalField.getValue(),
				bulan_akhir	: welcome_chart_bulan_akhirField.getValue(),
				tahun_akhir	: welcome_chart_tahun_akhirField.getValue()
			},
			success: function(result, request){
				var hasil=eval(result.responseText);
				if (hasil > 0 )
				{
					welcome_chart_kunjungan.update("<iframe frameborder='0' width='100%' height='100%' src='http://"+"<? echo $_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT']?>"+"/mis2/index.php?c=c_dashboard_chart&n=total&nilai="+hasil+"'></iframe>");					
				};
			},
			failure: function(response){
				//Ext.MessageBox.hide();
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: FAILED_CONNECTION,
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});
			}
		}); 
		
		if(main_cabang !== 'MAC' && main_cabang !== 'MACL'){
			//KUNJUNGAN LAMA
			Ext.Ajax.request({
				waitMsg: 'Please Wait...',
				url: 'index.php?c=c_welcome_chart&m=get_action',
				params: {
					task: 'CHART_KUNJUNGAN_LAMA',						
					method: 'POST',
					bulan_awal	: welcome_chart_bulan_awalField.getValue(),
					tahun_awal	: welcome_chart_tahun_awalField.getValue(),
					bulan_akhir	: welcome_chart_bulan_akhirField.getValue(),
					tahun_akhir	: welcome_chart_tahun_akhirField.getValue()
				},
				success: function(result, request){
					var hasil=eval(result.responseText);
					if (hasil > 0 )
					{
						welcome_chart_kunjungan_lama.update("<iframe frameborder='0' width='100%' height='100%' src='http://"+"<? echo $_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT']?>"+"/mis2/index.php?c=c_dashboard_chart&n=total&nilai="+hasil+"'></iframe>");					
					};
				},
				failure: function(response){
					//Ext.MessageBox.hide();
					Ext.MessageBox.show({
					   title: 'Error',
					   msg: FAILED_CONNECTION,
					   buttons: Ext.MessageBox.OK,
					   animEl: 'database',
					   icon: Ext.MessageBox.ERROR
					});
				}
			}); 

			//KUNJUNGAN BARU
			Ext.Ajax.request({
				waitMsg: 'Please Wait...',
				url: 'index.php?c=c_welcome_chart&m=get_action',
				params: {
					task: 'CHART_KUNJUNGAN_BARU',						
					method: 'POST',
					bulan_awal	: welcome_chart_bulan_awalField.getValue(),
					tahun_awal	: welcome_chart_tahun_awalField.getValue(),
					bulan_akhir	: welcome_chart_bulan_akhirField.getValue(),
					tahun_akhir	: welcome_chart_tahun_akhirField.getValue()
				},
				success: function(result, request){
					var hasil=eval(result.responseText);
					if (hasil > 0 )
					{
						welcome_chart_kunjungan_baru.update("<iframe frameborder='0' width='100%' height='100%' src='http://"+"<? echo $_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT']?>"+"/mis2/index.php?c=c_dashboard_chart&n=total&nilai="+hasil+"'></iframe>");					
					};
				},
				failure: function(response){
					//Ext.MessageBox.hide();
					Ext.MessageBox.show({
					   title: 'Error',
					   msg: FAILED_CONNECTION,
					   buttons: Ext.MessageBox.OK,
					   animEl: 'database',
					   icon: Ext.MessageBox.ERROR
					});
				}
			}); 

			//SPENDING AVG
			Ext.Ajax.request({
				waitMsg: 'Please Wait...',
				url: 'index.php?c=c_welcome_chart&m=get_action',
				params: {
					task: 'CHART_SPENDING_AVG',						
					method: 'POST',
					bulan_awal	: welcome_chart_bulan_awalField.getValue(),
					tahun_awal	: welcome_chart_tahun_awalField.getValue(),
					bulan_akhir	: welcome_chart_bulan_akhirField.getValue(),
					tahun_akhir	: welcome_chart_tahun_akhirField.getValue()
				},
				success: function(result, request){
					var hasil=eval(result.responseText);
					if (hasil > 0 )
					{
						welcome_chart_spending_avg.update("<iframe frameborder='0' width='100%' height='100%' src='http://"+"<? echo $_SERVER['SERVER_ADDR'].":".$_SERVER['SERVER_PORT']?>"+"/mis2/index.php?c=c_dashboard_chart&n=total&nilai="+hasil+"'></iframe>");					
					};
				},
				failure: function(response){
					//Ext.MessageBox.hide();
					Ext.MessageBox.show({
					   title: 'Error',
					   msg: FAILED_CONNECTION,
					   buttons: Ext.MessageBox.OK,
					   animEl: 'database',
					   icon: Ext.MessageBox.ERROR
					});
				}
			}); 
		}*/
	}
	
	function welcome_chart_netsales_linier_update(loading){
		if(loading){
			Ext.MessageBox.show({
				msg: 'Sedang memproses data Grafik Net Sales, mohon tunggu...',
				progressText: 'proses...',
				width:350,
				wait:true
			});
		}
		
		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		Ext.Ajax.request({
			waitMsg: 'Please Wait...',
			url: 'index.php?c=c_lap_netsales&m=get_action',
			timeout: 1200000,
			params: {
				task : 'CHART_DASHBOARD',						
				method : 'POST',
				bulan : "<?php echo date('m') ?>",
				periode : 'tanggal',
				tahun : "<?php echo date('Y') ?>",
				tgl_awal : tgl_awal,
				tgl_akhir : tgl_akhir
			},
			success: function(result, request){
				if(loading)
					Ext.MessageBox.hide();
				
				var hasil=eval(result.responseText);
				if (hasil > 0 )
				{
					welcome_chart_netsales_linier.update("<iframe frameborder='0' width='100%' height='100%' src='<?=base_url();?>print/lap_netsales_graph.php'></iframe>");					
				};
			},
			failure: function(response){
				//Ext.MessageBox.hide();
				Ext.MessageBox.show({
				   title: 'Error',
				   msg: "Failed Connection",
				   buttons: Ext.MessageBox.OK,
				   animEl: 'database',
				   icon: Ext.MessageBox.ERROR
				});
			}
		});	
	}

	function welcome_chart_rekap_jual_update(mode){
		Ext.MessageBox.show({
			msg: 'Sedang memproses data Top 10 Penjualan, mohon tunggu...',
			progressText: 'proses...',
			width:350,
			wait:true
		});

		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		var t_order = welcome_chart_rekap_jual_orderField.getValue();


		welcome_chart_rekap_jual_produk_DataStore.baseParams = {
			task: 'REKAP_JUAL_UPDATE', 
			rekap_penjualan_jenis: 'Produk', 
			rekap_penjualan_periode: 'tanggal', 
			rekap_penjualan_tglapp_end: tgl_akhir,
			rekap_penjualan_tglapp_start: tgl_awal,
			start:0, 
			limit: 10,
			orderby: t_order				
		};
		welcome_chart_rekap_jual_produk_DataStore.reload({
			callback: function(r,opt,success){
				/*welcome_chart_rekap_jual_rawat_DataStore.baseParams = {
					task: 'REKAP_JUAL_UPDATE', 
					rekap_penjualan_jenis: 'Perawatan_dan_Paket', 
					rekap_penjualan_periode: 'tanggal', 
					rekap_penjualan_tglapp_end: tgl_akhir,
					rekap_penjualan_tglapp_start: tgl_awal,
					start:0, 
					limit: 10,
					orderby: t_order				

				};*/
				Ext.MessageBox.hide();
				/*welcome_chart_rekap_jual_rawat_DataStore.reload({
					callback: function(r,opt,success){
						welcome_chart_rekap_jual_paket_DataStore.baseParams = {
							task: 'REKAP_JUAL_UPDATE', 
							rekap_penjualan_jenis: 'Paket', 
							rekap_penjualan_periode: 'tanggal', 
							rekap_penjualan_tglapp_end: tgl_akhir,
							rekap_penjualan_tglapp_start: tgl_awal,
							start:0, 
							limit: 10,
							orderby: t_order				
						};
						welcome_chart_rekap_jual_paket_DataStore.reload({
							callback: function(r,opt,success){
								Ext.MessageBox.hide();
								welcome_chart_rekap_(mode == 'all')
									welcome_chart_top_spender_update(mode);
							}
						});
					}
				});*/
			}
		});
	}

	function welcome_chart_rekap_jual_search(loading){

		if(loading){
			Ext.MessageBox.show({
				msg: 'Sedang memproses data, mohon tunggu...',
				progressText: 'proses...',
				width:350,
				wait:true
			});
		}
		
		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		var t_order = welcome_chart_rekap_jual_orderField.getValue();


		welcome_chart_rekap_jual_produk_DataStore.baseParams = {
			task: 'REKAP_JUAL', 
			rekap_penjualan_jenis: 'Produk', 
			rekap_penjualan_periode: 'tanggal', 
			rekap_penjualan_tglapp_end: tgl_akhir,
			rekap_penjualan_tglapp_start: tgl_awal,
			start:0, 
			limit: 10,
			orderby: t_order				

		};
		welcome_chart_rekap_jual_produk_DataStore.reload({
			callback: function(r,opt,success){
				welcome_chart_rekap_jualForm.setTitle('Top 10 Penjualan');
				if(welcome_chart_rekap_jual_produk_DataStore.getCount() > 0){
					var t_data = welcome_chart_rekap_jual_produk_DataStore.getAt(0).data;

					if(t_data.trjp_date_update)
						welcome_chart_rekap_jualForm.setTitle('Top 10 Penjualan <span style="font-size:10px;float: right;">Last update: ' + t_data.trjp_date_update.format('d-m-Y H:i') + '</span>');
				}				

				/*welcome_chart_rekap_jual_rawat_DataStore.baseParams = {
					task: 'REKAP_JUAL', 
					rekap_penjualan_jenis: 'Perawatan_dan_Paket', 
					rekap_penjualan_periode: 'tanggal', 
					rekap_penjualan_tglapp_end: tgl_akhir,
					rekap_penjualan_tglapp_start: tgl_awal,
					start:0, 
					limit: 10,
					orderby: t_order				

				};
				welcome_chart_rekap_jual_rawat_DataStore.reload({
					callback: function(r,opt,success){
						welcome_chart_rekap_jual_paket_DataStore.baseParams = {
							task: 'REKAP_JUAL', 
							rekap_penjualan_jenis: 'Paket', 
							rekap_penjualan_periode: 'tanggal', 
							rekap_penjualan_tglapp_end: tgl_akhir,
							rekap_penjualan_tglapp_start: tgl_awal,
							start:0, 
							limit: 10,
							orderby: t_order				

						};
						welcome_chart_rekap_jual_paket_DataStore.reload({
							callback: function(r,opt,success){
								if(loading)
									Ext.MessageBox.hide();
							}
						});
					}
				});*/
			}
		});
	}

	/*function welcome_chart_top_spender_update(mode){
		Ext.MessageBox.show({
			msg: 'Sedang memproses data Top 10 Spender, mohon tunggu...',
			progressText: 'proses...',
			width:350,
			wait:true
		});

		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		welcome_chart_top_spender_net_sales_DataStore.baseParams = {
			task: 'TOP_SPENDER_NETSALES_UPDATE', 
			trawat_tglend: tgl_akhir,
			trawat_tglstart: tgl_awal,
			start:0, 
			limit: 10
		};
		welcome_chart_top_spender_net_sales_DataStore.reload({
			callback: function(r,opt,success){
				welcome_chart_top_spender_cash_in_DataStore.baseParams = {
					task: 'TOP_SPENDER_CASHIN_UPDATE',
					trawat_tglapp_end: tgl_akhir,
					trawat_tglapp_start: tgl_awal,
					start:0, 
					limit: 10
				};
				welcome_chart_top_spender_cash_in_DataStore.reload({
					callback: function(r,opt,success){
						Ext.MessageBox.hide();
						welcome_chart_top_spender_search();

						if(mode == 'all')
							welcome_chart_kontribusi_update(mode);
					}
				});
			}
		});
	}

	function welcome_chart_top_spender_search(loading){

		if(loading){
			Ext.MessageBox.show({
				msg: 'Sedang memproses data, mohon tunggu...',
				progressText: 'proses...',
				width:350,
				wait:true
			});
		}

		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		welcome_chart_top_spender_net_sales_DataStore.baseParams = {
			task: 'TOP_SPENDER_NETSALES', 
			trawat_tglend: tgl_akhir,
			trawat_tglstart: tgl_awal,
			start:0, 
			limit: 10
		};
		welcome_chart_top_spender_net_sales_DataStore.reload({
			callback: function(r,opt,success){
				welcome_chart_top_spenderForm.setTitle('Top 10 Spender');
				if(welcome_chart_top_spender_net_sales_DataStore.getCount() > 0){
					var t_data = welcome_chart_top_spender_net_sales_DataStore.getAt(0).data;

					if(t_data.ttsn_date_update)
						welcome_chart_top_spenderForm.setTitle('Top 10 Spender <span style="font-size:10px;float: right;">Last update: ' + t_data.ttsn_date_update.format('d-m-Y H:i') + '</span>');					
				}

				welcome_chart_top_spender_cash_in_DataStore.baseParams = {
					task: 'TOP_SPENDER_CASHIN', 
					trawat_tglapp_end: tgl_akhir,
					trawat_tglapp_start: tgl_awal,
					start:0, 
					limit: 10
				};
				welcome_chart_top_spender_cash_in_DataStore.reload({
					callback: function(r,opt,success){
						if(loading)
							Ext.MessageBox.hide();
					}
				});
			}
		});
	}

	function welcome_chart_kontribusi_search(loading){

		if(loading){
			Ext.MessageBox.show({
				msg: 'Sedang memproses data, mohon tunggu...',
				progressText: 'proses...',
				width:350,
				wait:true
			});
		}

		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		welcome_chart_kontribusi_medis_DataStore.baseParams = {
			task: 'KONTRIBUSI_MEDIS',
			tgl_akhir: tgl_akhir,
			tgl_awal: tgl_awal,
			start:0, 
			limit: 1000				
		};
		welcome_chart_kontribusi_medis_DataStore.reload({
			callback: function(r,opt,success){
				welcome_chart_kontribusiForm.setTitle('Kontribusi & Produktifitas');
				if(welcome_chart_kontribusi_medis_DataStore.getCount() > 0){
					var t_data = welcome_chart_kontribusi_medis_DataStore.getAt(0).data;

					if(t_data.tkm_date_update)
						welcome_chart_kontribusiForm.setTitle('Kontribusi & Produktifitas <span style="font-size:10px;float: right;">Last update: ' + t_data.tkm_date_update.format('d-m-Y H:i') + '</span>');
				}				

				welcome_chart_kontribusi_non_medis_DataStore.baseParams = {
					task: 'KONTRIBUSI_NON_MEDIS', 
					tgl_akhir: tgl_akhir,
					tgl_awal: tgl_awal,
					start:0, 
					limit: 1000				
				};
				welcome_chart_kontribusi_non_medis_DataStore.reload({
					callback: function(r,opt,success){
						welcome_chart_kontribusi_produk_DataStore.baseParams = {
							task: 'KONTRIBUSI_PRODUK',
							tgl_akhir: tgl_akhir,
							tgl_awal: tgl_awal,
							start:0, 
							limit: 1000				
						};
						welcome_chart_kontribusi_produk_DataStore.reload({
							callback: function(r,opt,success){
								if(loading)
									Ext.MessageBox.hide();
							}
						});
					}
				});
			}
		});
	}

	function welcome_chart_kontribusi_update(mode){
		Ext.MessageBox.show({
			msg: 'Sedang memproses data Kontribusi & Produktifitas, mohon tunggu...',
			progressText: 'proses...',
			width:350,
			wait:true
		});

		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		welcome_chart_kontribusi_medis_DataStore.baseParams = {
			task: 'KONTRIBUSI_MEDIS_UPDATE',
			tgl_akhir: tgl_akhir,
			tgl_awal: tgl_awal,
			start:0, 
			limit: 1000				
		};
		welcome_chart_kontribusi_medis_DataStore.reload({
			callback: function(r,opt,success){
				welcome_chart_kontribusi_non_medis_DataStore.baseParams = {
					task: 'KONTRIBUSI_NON_MEDIS_UPDATE', 
					tgl_akhir: tgl_akhir,
					tgl_awal: tgl_awal,
					start:0, 
					limit: 1000				
				};
				welcome_chart_kontribusi_non_medis_DataStore.reload({
					callback: function(r,opt,success){
						welcome_chart_kontribusi_produk_DataStore.baseParams = {
							task: 'KONTRIBUSI_PRODUK_UPDATE',
							tgl_akhir: tgl_akhir,
							tgl_awal: tgl_awal,
							start:0, 
							limit: 1000				
						};
						welcome_chart_kontribusi_produk_DataStore.reload({
							callback: function(r,opt,success){
								Ext.MessageBox.hide();
								welcome_chart_kontribusi_search();

								if(mode == 'all')
									welcome_chart_stok_minimum_update(mode);
							}
						});
					}
				});
			}
		});	
	}*/

	function welcome_chart_stok_minimum_update(mode){
		Ext.MessageBox.show({
			msg: 'Sedang memproses data Stok Minimum, mohon tunggu...',
			progressText: 'proses...',
			width:350,
			wait:true
		});

		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		welcome_chart_stok_minimum_DataStore.baseParams = {
			task: "STOK_MINIMUM_UPDATE", 
			group1_id: '',
			group2_id: '',
			produk_id: '',
			query: '',
			tahun: '<?php echo date("Y") ?>',
			bulan: '<?php echo date("m") ?>',
			gudang: 2, 
			jenis: 1, 
			mutasi_jumlah: 'Semua', 
			opsi_produk: 'all', 
			opsi_satuan: 'default', 
			periode: 'tanggal', 
			stok_akhir: 'Semua', 
			stok_awal: 'Semua', 
			stok_keluar: '> 0', 
			stok_masuk: '> 0', 
			start:0, 
			limit: 10,
			tanggal_end: tgl_akhir,
			tanggal_start: tgl_awal
		};
		welcome_chart_stok_minimum_DataStore.reload({
			callback: function(r,opt,success){
				Ext.MessageBox.hide();
				welcome_chart_stok_minimum_search();

				if(mode == 'all')
					welcome_chart_netsales_linier_update(true);
			}
		});
	}

	function welcome_chart_stok_minimum_search(loading){

		if(loading){
			Ext.MessageBox.show({
				msg: 'Sedang memproses data, mohon tunggu...',
				progressText: 'proses...',
				width:350,
				wait:true
			});
		}

		if(welcome_chart_bulan_awalField.getValue()!==""){welcome_chart_bulan_awal = welcome_chart_bulan_awalField.getValue(); }
		if(welcome_chart_tahun_awalField.getValue()!==""){welcome_chart_tahun_awal = welcome_chart_tahun_awalField.getValue(); }
		if(welcome_chart_bulan_akhirField.getValue()!==""){welcome_chart_bulan_akhir = welcome_chart_bulan_akhirField.getValue(); }
		if(welcome_chart_tahun_akhirField.getValue()!==""){welcome_chart_tahun_akhir = welcome_chart_tahun_akhirField.getValue(); }

		tgl_awal = welcome_chart_tahun_awal + '-' + welcome_chart_bulan_awal + '-01';
		tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';

		var t_date = new Date(welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31'); 
		if(isValidDate(t_date))
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-31';
		else
			tgl_akhir = welcome_chart_tahun_akhir + '-' + welcome_chart_bulan_akhir + '-30';

		welcome_chart_stok_minimum_DataStore.baseParams = {
			task: "STOK_MINIMUM", 
			group1_id: '',
			group2_id: '',
			produk_id: '',
			query: '',
			tahun: '<?php echo date("Y") ?>',
			bulan: '<?php echo date("m") ?>',
			gudang: 2, 
			jenis: 1, 
			mutasi_jumlah: 'Semua', 
			opsi_produk: 'all', 
			opsi_satuan: 'default', 
			periode: 'tanggal', 
			stok_akhir: 'Semua', 
			stok_awal: 'Semua', 
			stok_keluar: '> 0', 
			stok_masuk: '> 0', 
			start:0, 
			limit: 10,
			tanggal_end: tgl_akhir,
			tanggal_start: tgl_awal
		};
		welcome_chart_stok_minimum_DataStore.reload({
			callback: function(r,opt,success){
				if(loading)
					Ext.MessageBox.hide();

				welcome_chart_stok_minimumListEditorGrid.setTitle('Stok Minimum Produk');
				if(welcome_chart_stok_minimum_DataStore.getCount() > 0){
					var t_data = welcome_chart_stok_minimum_DataStore.getAt(0).data;

					if(t_data.tsm_date_update)
						welcome_chart_stok_minimumListEditorGrid.setTitle('Stok Minimum Produk <span style="font-size:10px;float: right;">Last update: ' + t_data.tsm_date_update.format('d-m-Y H:i') + '</span>');
				}
			}
		});
	}
	
	function welcome_chart_jatuh_tempo_update(mode){
		Ext.MessageBox.show({
			msg: 'Sedang memproses data Stok Minimum, mohon tunggu...',
			progressText: 'proses...',
			width:350,
			wait:true
		});

		welcome_chart_jatuh_tempo_DataStore.baseParams = {
			task: "JATUH_TEMPO_UPDATE"
		};
		welcome_chart_jatuh_tempo_DataStore.reload({
			callback: function(r,opt,success){
				Ext.MessageBox.hide();
				welcome_chart_jatuh_tempo_search();

				if(mode == 'all')
					welcome_chart_netsales_linier_update(true);
			}
		});
	}

	function welcome_chart_jatuh_tempo_search(loading){

		if(loading){
			Ext.MessageBox.show({
				msg: 'Sedang memproses data, mohon tunggu...',
				progressText: 'proses...',
				width:350,
				wait:true
			});
		}

		welcome_chart_jatuh_tempo_DataStore.baseParams = {
			task: "JATUH_TEMPO"
		};
		welcome_chart_jatuh_tempo_DataStore.reload({
			callback: function(r,opt,success){
				if(loading)
					Ext.MessageBox.hide();

				welcome_chart_jatuh_tempoListEditorGrid.setTitle('Jatuh Tempo Hutang');
				if(welcome_chart_jatuh_tempo_DataStore.getCount() > 0){
					var t_data = welcome_chart_jatuh_tempo_DataStore.getAt(0).data;

					if(t_data.tsm_date_update)
						welcome_chart_jatuh_tempoListEditorGrid.setTitle('Jatuh Tempo Hutang <span style="font-size:10px;float: right;">Last update: ' + t_data.tsm_date_update.format('d-m-Y H:i') + '</span>');
				}
			}
		});
	}

	function welcome_chart_ultah_search(loading){

		if(loading){
			Ext.MessageBox.show({
				msg: 'Sedang memproses data, mohon tunggu...',
				progressText: 'proses...',
				width:350,
				wait:true
			});
		}

		welcome_chart_ultah_DataStore.baseParams = {
			task: "ULTAH", 
			tanggal: thisday,
			bulan: thismonth			
		};
		welcome_chart_ultah_DataStore.reload({
			callback: function(r,opt,success){
				if(loading)
					Ext.MessageBox.hide();

				welcome_chart_ultahListEditorGrid.setTitle('Ultah Member');
				if(welcome_chart_ultah_DataStore.getCount() > 0){
					var t_data = welcome_chart_ultah_DataStore.getAt(0).data;

					if(t_data.tsm_date_update)
						welcome_chart_ultahListEditorGrid.setTitle('Jatuh Tempo Hutang <span style="font-size:10px;float: right;">Last update: ' + t_data.tsm_date_update.format('d-m-Y H:i') + '</span>');
				}
			}
		});
	}

	function dashboard_search(loading){

		if(loading != false)
			loading = true;

		welcome_chart_search(loading);		
		welcome_chart_netsales_linier_update(loading);
		welcome_chart_rekap_jual_search(loading);
		//welcome_chart_top_spender_search(loading);
		//welcome_chart_kontribusi_search(loading);	
		welcome_chart_stok_minimum_search(loading);	
		welcome_chart_jatuh_tempo_search(loading);	
		welcome_chart_ultah_search(loading);	
	}

	function dashboard_update(){
		welcome_chart_update('all');
	}

	function goToLapKunjungan(){
		mainPanel.loadClass('<?php echo "?c=c_lap_kunjungan";?>', '<?php echo "Laporan Jumlah Kunjungan"; ?>');
	}

	function goToLapNetSales(){
		mainPanel.loadClass('<?php echo "?c=c_lap_netsales";?>', '<?php echo "Laporan Net Sales"; ?>');
	}

	function goToLapRekapJual(){
		mainPanel.loadClass('<?php echo "?c=c_report_rekap_penjualan";?>', '<?php echo "Laporan Rekap Penjualan"; ?>');
	}

	/*function goToLapTopSpenderNetSales(){
		mainPanel.loadClass('<?php echo "?c=c_report_top_spender_netsales";?>', '<?php echo "Laporan Top Spender Net Sales"; ?>');
	}

	function goToLapTopSpenderCashIn(){
		mainPanel.loadClass('<?php echo "?c=c_report_top_spender";?>', '<?php echo "Laporan Top Spender Cash In"; ?>');
	}

	function goToLapJumTindakanMedis(){
		mainPanel.loadClass('<?php echo "?c=c_lap_jum_tindakan_all_dokter";?>', '<?php echo "Laporan Jumlah Tindakan Medis Semua Referal"; ?>');
	}

	function goToLapJumTindakanEstetik(){
		mainPanel.loadClass('<?php echo "?c=c_lap_jum_tindakan_all_therapis";?>', '<?php echo "Laporan Jumlah Tindakan Estetik Semua Referal"; ?>');
	}*/

	function goToLapJumPenjualanProduk(){
		mainPanel.loadClass('<?php echo "?c=c_lap_jum_all_produk";?>', '<?php echo "Laporan Jumlah Penjualan Produk Semua Referal"; ?>');
	}

	/*function goToLapJumCustomerDokter(){
		mainPanel.loadClass('<?php echo "?c=c_lap_jum_cust_dokter";?>', '<?php echo "Laporan Jumlah Customer per Dokter"; ?>');
	}*/

	function goToLapStok(){
		mainPanel.loadClass('<?php echo "?c=c_stok_mutasi_new";?>', '<?php echo "Laporan Stok Rekap Barang"; ?>');
	}

	welcome_chartListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chartListEditorGrid',
		el: 'fp_welcome_chart_list',
		title: 'Pencapaian Target',
		autoHeight: true,
		store: welcome_chartDataStore, // DataStore
		cm: welcome_chartColumnModel, // Nama-nama Columns
		enableColLock:false,
		frame: true,
		//clicksToEdit:2, // 2xClick untuk bisa meng-Edit inLine Data
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		viewConfig: { forceFit:true },
	  	width: 565/*, 
		tbar: [
			{
				text: 'Hitung Ulang',
				tooltip: 'Hitung Ulang',
				iconCls:'icon-refresh',
				handler: function(){
					cek_periode('welcome_chart_update');
				}
			}
			
		]*/
	});
	welcome_chartListEditorGrid.render();				
				
	function is_valid_form(){
	}

	function isValidDate(d) {
	  	if(Object.prototype.toString.call(d) !== "[object Date]")
	    	return false;
	  	return !isNaN(d.getTime());
	}

	var welcome_chart_kunjungan =	new Ext.form.FormPanel ({
		title: 'Gauge Chart : Kunjungan',
		resizeable: true,
		id: 'welcome_chart_kunjungan',
		el: 'elwindow_welcome_chart_kunjungan',
		width: 315,
		height: 240,
		layout: 'fit',
		hidden: true,
		frame: true,
		html: "<iframe frameborder='0' width='100%' height='100%' src=''></iframe>",
		autoDestroy: true,
	});
	welcome_chart_kunjungan.render();
	
	var welcome_chart_kunjungan_lama =	new Ext.form.FormPanel ({
		title: 'Gauge Chart : Kunjungan Cust Lama',
		resizeable: true,
		id: 'welcome_chart_kunjungan_lama',
		el: 'elwindow_welcome_chart_kunjungan_lama',
		width: 315,
		height: 240,
		layout: 'fit',
		hidden: true,
		frame: true,
		html: "<iframe frameborder='0' width='100%' height='100%' src=''></iframe>",
		autoDestroy: true,
	});
	welcome_chart_kunjungan_lama.render();
	
	var welcome_chart_kunjungan_baru =	new Ext.form.FormPanel ({
		title: 'Gauge Chart : Kunjungan Cust Baru',
		resizeable: true,
		id: 'welcome_chart_kunjungan_baru',
		el: 'elwindow_welcome_chart_kunjungan_baru',
		width: 315,
		height: 240,
		layout: 'fit',
		hidden: true,
		frame: true,
		html: "<iframe frameborder='0' width='100%' height='100%' src=''></iframe>",
		autoDestroy: true,
	});
	welcome_chart_kunjungan_baru.render();
	
	var welcome_chart_spending_avg =	new Ext.form.FormPanel ({
		title: 'Gauge Chart : Spending Avg',
		resizeable: true,
		id: 'welcome_chart_spending_avg',
		el: 'elwindow_welcome_chart_spending_avg',
		width: 315,
		height: 240,
		layout: 'fit',
		hidden: true,
		frame: true,
		html: "<iframe frameborder='0' width='100%' height='100%' src=''></iframe>",
		autoDestroy: true,
	});
	welcome_chart_spending_avg.render();
	
	var welcome_chart_netsales =	new Ext.form.FormPanel ({
		title: 'Gauge Chart : Total Penjualan',
		resizeable: true,
		id: 'welcome_chart_netsales',
		el: 'elwindow_welcome_chart_netsales',
		width: 315,
		height: 240,
		layout: 'fit',
		hidden: true,
		frame: true,
		html: "<iframe frameborder='0' width='100%' height='100%' src=''></iframe>",
		autoDestroy: true,
	});
	welcome_chart_netsales.render();
	
	var welcome_chart_netsales_medis =	new Ext.form.FormPanel ({
		title: 'Gauge Chart : Net Sales',
		resizeable: true,
		id: 'welcome_chart_netsales_medis',
		el: 'elwindow_welcome_chart_netsales_medis',
		width: 315,
		height: 240,
		layout: 'fit',
		hidden: true,
		frame: true,
		html: "<iframe frameborder='0' width='100%' height='100%' src=''></iframe>",
		autoDestroy: true,
	});
	welcome_chart_netsales_medis.render();
		
	var welcome_chart_netsales_estetik =	new Ext.form.FormPanel ({
		title: 'Gauge Chart : Net Sales Estetik',
		resizeable: true,
		id: 'welcome_chart_netsales_estetik',
		el: 'elwindow_welcome_chart_netsales_estetik',
		width: 315,
		height: 240,
		layout: 'fit',
		hidden: true,
		frame: true,
		html: "<iframe frameborder='0' width='100%' height='100%' src=''></iframe>",
		autoDestroy: true,
	});
	welcome_chart_netsales_estetik.render();
	
	var welcome_chart_netsales_produk =	new Ext.form.FormPanel ({
		title: 'Gauge Chart : Penjualan Produk',
		resizeable: true,
		id: 'welcome_chart_netsales_produk',
		el: 'elwindow_welcome_chart_netsales_produk',
		width: 315,
		height: 240,
		layout: 'fit',
		hidden: true,
		frame: true,
		html: "<iframe frameborder='0' width='100%' height='100%' src=''></iframe>",
		autoDestroy: true,
	});
	welcome_chart_netsales_produk.render();
		
	welcome_chart_searchForm = new Ext.FormPanel({
		autoDestroy: true,
		autoHeight:true,
		id: 'welcome_chart_searchForm',
		labelAlign: 'left',
		renderTo: 'elwindow_welcome_chart_search',
		title: 'Pencarian',
		layout: 'fit',
		frame: true,
		width: 975,  
		items: [{
				layout:'column',
				border:false,
				items:[
					{
						columnWidth:0.6,
						layout: 'form',
						border:false,
						items: [
							{
								layout: 'column',
								border: false,
								items:[
									{
								   		layout: 'form',
								   		labelWidth: 50,
										border: false,
										bodyStyle:'padding-left:3px',
										items:[welcome_chart_bulan_awalField]
								   	}, {
								   		layout: 'form',
								   		labelWidth: 1,
										border: false,
										items:[welcome_chart_tahun_awalField]
								   	}, {
								   		layout: 'form',
										border: false,
										labelWidth: 15,
										bodyStyle:'padding-left:6px',
										labelSeparator: ' ', 
										items:[welcome_chart_bulan_akhirField]
								   	}, {
								   		layout: 'form',
								   		labelWidth: 1,
										border: false,
										items:[welcome_chart_tahun_akhirField]
								   	}, {
								   		layout: 'form',
								   		labelWidth: 1,
										border: false,
										bodyStyle:'padding-left:3px',
										items:[welcome_chart_btnUpdate]
								   	}, {
								   		layout: 'form',
								   		labelWidth: 1,
										border: false,
										bodyStyle:'padding-left:3px',
										items:[welcome_chart_btnSearch]
								   	}
								]
							}
						]
					}
				]
		}]
	});
	welcome_chart_searchForm.render();

	welcome_chart_netsales_linier = new Ext.form.FormPanel ({
		title: 'Net Sales',
		id: 'welcome_chart_netsales_linier',
		el: 'elwindow_welcome_chart_netsales_linier',
        width: 500,
		height: 470,
		layout: 'fit',
		frame: true,
		autoDestroy: true,
		tbar: [
			{
				text: 'Hitung Ulang',
				tooltip: 'Hitung Ulang',
				iconCls:'icon-refresh',
				handler: function(){
					cek_periode('welcome_chart_netsales_linier_update');
				}
			}
		]
	});
	//welcome_chart_netsales_linier.render();
	
	welcome_chart_proporsi = new Ext.form.FormPanel ({
		title: 'Proporsi',
		id: 'welcome_chart_proporsi',
		el: 'elwindow_welcome_chart_proporsi',
        width: 500,
		height: 300,
		layout: 'fit',
		frame: true,
		autoDestroy: true
	});
	//welcome_chart_proporsi.render();
	
	var welcome_chart_rekap_jual_produk_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: 'kode'
	},[
			{name: 'trjp_kode', type: 'string', mapping: 'trjp_kode'},
			{name: 'trjp_nama', type: 'string', mapping: 'trjp_nama'},
			{name: 'trjp_tot_jum_item', type: 'int', mapping: 'trjp_tot_jum_item'},
			{name: 'trjp_tot_net', type: 'int', mapping: 'trjp_tot_net'},
			{name: 'trjp_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'trjp_date_update'}
	]);

	welcome_chart_rekap_jual_produk_DataStore = new Ext.data.Store({
		id: 'welcome_chart_rekap_jual_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action',
			method: 'POST',
			timeout: 1200000
		}),
		reader: welcome_chart_rekap_jual_produk_reader,
		baseParams:{task: 'REKAP_JUAL', rekap_penjualan_jenis: 'Produk', rekap_penjualan_periode: 'tanggal', start:0, limit: 10}
	});

	welcome_chart_rekap_jual_produk_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'left',
			header: '<div align="center">' + 'Produk' + '</div>',
			dataIndex: 'trjp_nama',
			width: 250,
			sortable: true,
			allowBlank : false
		},
		{
			align : 'center',
			header: '<div align="center">' + 'Jml' + '</div>',
			dataIndex: 'trjp_tot_jum_item',
			width: 35,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + '(Rp)' + '</div>',
			dataIndex: 'trjp_tot_net',
			width: 70,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}]
	);
	welcome_chart_rekap_jual_produk_ColumnModel.defaultSortable= true;

	welcome_chart_rekap_jual_produkListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_rekap_jual_produkListEditorGrid',
		el: 'fp_welcome_chart_rekap_jual_produk',
		title: 'Produk',
		height: 250,
		width: 400,
		autoScroll: true,
		store: welcome_chart_rekap_jual_produk_DataStore, // DataStore
		colModel: welcome_chart_rekap_jual_produk_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		region: 'center',
        margins: '0 5 5 5',
		frame: true
	});

	var welcome_chart_rekap_jual_rawat_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: 'kode'
	},[
			{name: 'trjr_kode', type: 'string', mapping: 'trjr_kode'},
			{name: 'trjr_nama', type: 'string', mapping: 'trjr_nama'},
			{name: 'trjr_tot_jum_item', type: 'int', mapping: 'trjr_tot_jum_item'},
			{name: 'trjr_tot_net', type: 'int', mapping: 'trjr_tot_net'},
			{name: 'trjr_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'trjr_date_update'}
	]);

	welcome_chart_rekap_jual_rawat_DataStore = new Ext.data.Store({
		id: 'welcome_chart_rekap_jual_rawat_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action',
			method: 'POST',
			timeout: 1200000
		}),
		reader: welcome_chart_rekap_jual_rawat_reader,
		baseParams:{task: 'REKAP_JUAL', rekap_penjualan_jenis: 'Perawatan_dan_Paket', rekap_penjualan_periode: 'tanggal', start:0, limit: 10}
	});

	welcome_chart_rekap_jual_rawat_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'left',
			header: '<div align="center">' + 'Perawatan' + '</div>',
			dataIndex: 'trjr_nama',
			width: 195,
			sortable: true,
			allowBlank : false
		},
		{
			align : 'center',
			header: '<div align="center">' + 'Jml' + '</div>',
			dataIndex: 'trjr_tot_jum_item',
			width: 40,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + '(Rp)' + '</div>',
			dataIndex: 'trjr_tot_net',
			width: 75,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}]
	);
	welcome_chart_rekap_jual_rawat_ColumnModel.defaultSortable= true;

	welcome_chart_rekap_jual_rawatListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_rekap_jual_rawatListEditorGrid',
		el: 'fp_welcome_chart_rekap_jual_rawat',
		title: 'Perawatan',
		height: 250,
		width: 350,
		autoScroll: true,
		store: welcome_chart_rekap_jual_rawat_DataStore, // DataStore
		colModel: welcome_chart_rekap_jual_rawat_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		region: 'center',
        margins: '0 5 5 5',
		frame: true
	});

	var welcome_chart_rekap_jual_paket_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: 'kode'
	},[
			{name: 'trjk_kode', type: 'string', mapping: 'trjk_kode'},
			{name: 'trjk_nama', type: 'string', mapping: 'trjk_nama'},
			{name: 'trjk_tot_jum_item', type: 'int', mapping: 'trjk_tot_jum_item'},
			{name: 'trjk_tot_net', type: 'int', mapping: 'trjk_tot_net'},
			{name: 'trjk_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'trjk_date_update'}
	]);

	welcome_chart_rekap_jual_paket_DataStore = new Ext.data.Store({
		id: 'welcome_chart_rekap_jual_paket_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action',
			method: 'POST',
			timeout: 1200000
		}),
		reader: welcome_chart_rekap_jual_paket_reader,
		baseParams:{task: 'REKAP_JUAL', rekap_penjualan_jenis: 'Paket', rekap_penjualan_periode: 'tanggal', start:0, limit: 10}
	});

	welcome_chart_rekap_jual_paket_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'left',
			header: '<div align="center">' + 'Paket' + '</div>',
			dataIndex: 'trjk_nama',
			width: 195,
			sortable: true,
			allowBlank : false
		},
		{
			align : 'center',
			header: '<div align="center">' + 'Jml' + '</div>',
			dataIndex: 'trjk_tot_jum_item',
			width: 40,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + '(Rp)' + '</div>',
			dataIndex: 'trjk_tot_net',
			width: 75,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}]
	);
	welcome_chart_rekap_jual_paket_ColumnModel.defaultSortable= true;

	welcome_chart_rekap_jual_paketListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_rekap_jual_paketListEditorGrid',
		el: 'fp_welcome_chart_rekap_jual_paket',
		title: 'Paket',
		height: 250,
		width: 350,
		autoScroll: true,
		store: welcome_chart_rekap_jual_paket_DataStore, // DataStore
		colModel: welcome_chart_rekap_jual_paket_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		region: 'center',
        margins: '0 5 5 5',
		frame: true
	});

	var welcome_chart_tab_rekap_jual = new Ext.TabPanel({
		activeTab: 0,
		width: 350,
		items: [welcome_chart_rekap_jual_produkListEditorGrid]
	});

	welcome_chart_rekap_jualForm = new Ext.FormPanel({
		title: 'Top 10 Penjualan',
		id: 'welcome_chart_rekap_jualForm',
		el: 'elwindow_welcome_chart_rekap_jual',
        width: 400,
		height: 350,
		layout: 'fit',
		frame: true,
		autoDestroy: true,
		items: [welcome_chart_tab_rekap_jual],
		tbar: [
			/*'<b><font color=black>Urutkan : </b>', 
			welcome_chart_rekap_jual_orderField,'-',
			{
				text: 'Hitung Ulang',
				tooltip: 'Hitung Ulang',
				iconCls:'icon-refresh',
				handler: function(){
					cek_periode('welcome_chart_rekap_jual_update');
					welcome_chart_rekap_jual_update();
				}
			},'-',	*/
			{
				text: 'Detail',
				tooltip: 'Detail',
				iconCls:'icon-folder_go',
				menu: {
					xtype: 'menu',
					plain: true,
					items: [
						{
							text: 'Laporan Rekap Penjualan',
							handler: goToLapRekapJual
						}
					]
				}
			}
		]
	});
	welcome_chart_rekap_jualForm.render();

	var welcome_chart_top_spender_cash_in_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: 'cust_no'
	},[
			{name: 'ttsc_cust_no', type: 'string', mapping: 'ttsc_cust_no'},
			{name: 'ttsc_cust_nama', type: 'string', mapping: 'ttsc_cust_nama'},
			{name: 'ttsc_total', type: 'int', mapping: 'ttsc_total'},
			{name: 'ttsc_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'ttsc_date_update'}
	]);

	welcome_chart_top_spender_cash_in_DataStore = new Ext.data.Store({
		id: 'welcome_chart_top_spender_cash_in_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action',
			method: 'POST',
			timeout: 1200000
		}),
		reader: welcome_chart_top_spender_cash_in_reader,
		baseParams:{task: 'TOP_SPENDER_CASHIN', start:0, limit: 10}
	});

	welcome_chart_top_spender_cash_in_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'left',
			header: '<div align="center">' + 'Customer' + '</div>',
			dataIndex: 'ttsc_cust_nama',
			width: 235,
			sortable: true,
			allowBlank : false
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Cash In (Rp)' + '</div>',
			dataIndex: 'ttsc_total',
			width: 75,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}]
	);
	welcome_chart_top_spender_cash_in_ColumnModel.defaultSortable= true;

	welcome_chart_top_spender_cash_inListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_top_spender_cash_inListEditorGrid',
		el: 'fp_welcome_chart_top_spender_cash_in',
		title: 'Cash In',
		height: 250,
		width: 350,
		autoScroll: true,
		store: welcome_chart_top_spender_cash_in_DataStore, // DataStore
		colModel: welcome_chart_top_spender_cash_in_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		region: 'center',
        margins: '0 5 5 5',
		frame: true
	});

	var welcome_chart_top_spender_net_sales_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: 'cust_no'
	},[
			{name: 'ttsn_cust_no', type: 'string', mapping: 'ttsn_cust_no'},
			{name: 'ttsn_cust_nama', type: 'string', mapping: 'ttsn_cust_nama'},
			{name: 'ttsn_total', type: 'int', mapping: 'ttsn_total'},
			{name: 'ttsn_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'ttsn_date_update'}
	]);

	welcome_chart_top_spender_net_sales_DataStore = new Ext.data.Store({
		id: 'welcome_chart_top_spender_net_sales_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action',
			method: 'POST',
			timeout: 1200000
		}),
		reader: welcome_chart_top_spender_net_sales_reader,
		baseParams:{task: 'TOP_SPENDER_NETSALES', start:0, limit: 10}
	});

	welcome_chart_top_spender_net_sales_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'left',
			header: '<div align="center">' + 'Customer' + '</div>',
			dataIndex: 'ttsn_cust_nama',
			width: 230,
			sortable: true,
			allowBlank : false
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Net Sales (Rp)' + '</div>',
			dataIndex: 'ttsn_total',
			width: 80,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}]
	);
	welcome_chart_top_spender_net_sales_ColumnModel.defaultSortable= true;

	welcome_chart_top_spender_net_salesListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_top_spender_net_salesListEditorGrid',
		el: 'fp_welcome_chart_top_spender_net_sales',
		title: 'Net Sales',
		height: 250,
		width: 350,
		autoScroll: true,
		store: welcome_chart_top_spender_net_sales_DataStore, // DataStore
		colModel: welcome_chart_top_spender_net_sales_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		region: 'center',
        margins: '0 5 5 5',
		frame: true
	});

	var welcome_chart_tab_top_spender = new Ext.TabPanel({
		activeTab: 0,
		width: 350,
		items: [welcome_chart_top_spender_net_salesListEditorGrid, welcome_chart_top_spender_cash_inListEditorGrid]
	});

	/*welcome_chart_top_spenderForm = new Ext.FormPanel({
		title: 'Top 10 Spender',
		id: 'welcome_chart_top_spenderForm',
		el: 'elwindow_welcome_chart_top_spender',
        width: 360,
		height: 250,
		layout: 'fit',
		frame: true,
		autoDestroy: true,
		items: [welcome_chart_tab_top_spender],
		tbar: [
			{
				text: 'Hitung Ulang',
				tooltip: 'Hitung Ulang',
				iconCls:'icon-refresh',
				handler: function(){
					cek_periode('welcome_chart_top_spender_update');
				}
			},'-',	
			{
				text: 'Detail',
				tooltip: 'Detail',
				iconCls:'icon-folder_go',
				menu: {
					xtype: 'menu',
					plain: true,
					items: [
						{
							text: 'Laporan Top Spender Net Sales',
							handler: goToLapTopSpenderNetSales
						},		
						{
							text: 'Laporan Top Spender Cash In',
							handler: goToLapTopSpenderCashIn
						}
					]
				}
			}
		]
	});*/
	//welcome_chart_top_spenderForm.render();

	var welcome_chart_kontribusi_medis_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: 'karyawan_id'
	},[
			{name: 'tkm_karyawan_id', type: 'string', mapping: 'tkm_karyawan_id'},
			{name: 'tkm_karyawan_nama', type: 'string', mapping: 'tkm_karyawan_nama'},
			{name: 'tkm_kontribusi_rp', type: 'int', mapping: 'tkm_kontribusi_rp'},
			{name: 'tkm_kontribusi_persen', type: 'float', mapping: 'tkm_kontribusi_persen'},
			{name: 'tkm_total_cust', type: 'int', mapping: 'tkm_total_cust'},
			{name: 'tkm_spending_avg', type: 'int', mapping: 'tkm_spending_avg'},
			{name: 'tkm_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'tkm_date_update'}
	]);

	welcome_chart_kontribusi_medis_DataStore = new Ext.data.Store({
		id: 'welcome_chart_kontribusi_medis_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action',
			method: 'POST',
			timeout: 1200000
		}),
		reader: welcome_chart_kontribusi_medis_reader,
		baseParams:{task: 'KONTRIBUSI_MEDIS', start:0, limit: 1000}
	});

	welcome_chart_kontribusi_medis_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'left',
			header: '<div align="center">' + 'Referal' + '</div>',
			dataIndex: 'tkm_karyawan_nama',
			width: 250,
			sortable: true,
			allowBlank : false
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Kontribusi (Rp)' + '</div>',
			dataIndex: 'tkm_kontribusi_rp',
			width: 120,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Kontribusi (%)' + '</div>',
			dataIndex: 'tkm_kontribusi_persen',
			width: 100,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Tot Cust' + '</div>',
			dataIndex: 'tkm_total_cust',
			width: 80,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Spending Avg (Rp)' + '</div>',
			dataIndex: 'tkm_spending_avg',
			width: 120,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}]
	);
	welcome_chart_kontribusi_medis_ColumnModel.defaultSortable= true;

	welcome_chart_kontribusi_medisListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_kontribusi_medisListEditorGrid',
		el: 'fp_welcome_chart_kontribusi_medis',
		title: 'Medis',
		height: 250,
		width: 700,
		autoScroll: true,
		store: welcome_chart_kontribusi_medis_DataStore, // DataStore
		colModel: welcome_chart_kontribusi_medis_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		region: 'center',
        margins: '0 5 5 5',
		frame: true
	});

	var welcome_chart_kontribusi_non_medis_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: 'kode'
	},[
			{name: 'tknm_karyawan_id', type: 'string', mapping: 'tknm_karyawan_id'},
			{name: 'tknm_karyawan_nama', type: 'string', mapping: 'tknm_karyawan_nama'},
			{name: 'tknm_kontribusi_rp', type: 'int', mapping: 'tknm_kontribusi_rp'},
			{name: 'tknm_kontribusi_persen', type: 'float', mapping: 'tknm_kontribusi_persen'},
			{name: 'tknm_total_cust', type: 'int', mapping: 'tknm_total_cust'},
			{name: 'tknm_spending_avg', type: 'int', mapping: 'tknm_spending_avg'},
			{name: 'tknm_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'tknm_date_update'}
	]);

	welcome_chart_kontribusi_non_medis_DataStore = new Ext.data.Store({
		id: 'welcome_chart_kontribusi_non_medis_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action',
			method: 'POST',
			timeout: 1200000
		}),
		reader: welcome_chart_kontribusi_non_medis_reader,
		baseParams:{task: 'KONTRIBUSI_NON_MEDIS', start:0, limit: 10}
	});

	welcome_chart_kontribusi_non_medis_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'left',
			header: '<div align="center">' + 'Referal' + '</div>',
			dataIndex: 'tknm_karyawan_nama',
			width: 250,
			sortable: true,
			allowBlank : false
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Kontribusi (Rp)' + '</div>',
			dataIndex: 'tknm_kontribusi_rp',
			width: 120,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Kontribusi (%)' + '</div>',
			dataIndex: 'tknm_kontribusi_persen',
			width: 100,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Tot Cust' + '</div>',
			dataIndex: 'tknm_total_cust',
			width: 80,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Spending Avg (Rp)' + '</div>',
			dataIndex: 'tknm_spending_avg',
			width: 120,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}]
	);
	welcome_chart_kontribusi_non_medis_ColumnModel.defaultSortable= true;

	welcome_chart_kontribusi_non_medisListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_kontribusi_non_medisListEditorGrid',
		el: 'fp_welcome_chart_kontribusi_non_medis',
		title: 'Estetik',
		height: 250,
		width: 700,
		autoScroll: true,
		store: welcome_chart_kontribusi_non_medis_DataStore, // DataStore
		colModel: welcome_chart_kontribusi_non_medis_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		region: 'center',
        margins: '0 5 5 5',
		frame: true
	});

	var welcome_chart_kontribusi_produk_reader=new Ext.data.JsonReader({
		root: 'results',
		totalProperty: 'total',
		id: 'kode'
	},[
			{name: 'tkp_karyawan_id', type: 'string', mapping: 'tkp_karyawan_id'},
			{name: 'tkp_karyawan_nama', type: 'string', mapping: 'tkp_karyawan_nama'},
			{name: 'tkp_kontribusi_rp', type: 'int', mapping: 'tkp_kontribusi_rp'},
			{name: 'tkp_kontribusi_persen', type: 'float', mapping: 'tkp_kontribusi_persen'},
			{name: 'tkp_total_cust', type: 'int', mapping: 'tkp_total_cust'},
			{name: 'tkp_spending_avg', type: 'int', mapping: 'tkp_spending_avg'},
			{name: 'tkp_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'tkp_date_update'}
	]);

	welcome_chart_kontribusi_produk_DataStore = new Ext.data.Store({
		id: 'welcome_chart_kontribusi_produk_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action',
			method: 'POST',
			timeout: 1200000
		}),
		reader: welcome_chart_kontribusi_produk_reader,
		baseParams:{task: 'KONTRIBUSI_PRODUK', start:0, limit: 10}
	});

	welcome_chart_kontribusi_produk_ColumnModel = new Ext.grid.ColumnModel(
		[
		{
			align : 'left',
			header: '<div align="center">' + 'Referal' + '</div>',
			dataIndex: 'tkp_karyawan_nama',
			width: 250,
			sortable: true,
			allowBlank : false
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Kontribusi (Rp)' + '</div>',
			dataIndex: 'tkp_kontribusi_rp',
			width: 120,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Kontribusi (%)' + '</div>',
			dataIndex: 'tkp_kontribusi_persen',
			width: 100,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Tot Cust' + '</div>',
			dataIndex: 'tkp_total_cust',
			width: 80,
			sortable: true
		},
		{
			align : 'right',
			header: '<div align="center">' + 'Spending Avg (Rp)' + '</div>',
			dataIndex: 'tkp_spending_avg',
			width: 120,
			sortable: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		}]
	);
	welcome_chart_kontribusi_produk_ColumnModel.defaultSortable= true;

	welcome_chart_kontribusi_produkListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_kontribusi_produkListEditorGrid',
		el: 'fp_welcome_chart_kontribusi_produk',
		title: 'Produk',
		height: 250,
		width: 700,
		autoScroll: true,
		store: welcome_chart_kontribusi_produk_DataStore, // DataStore
		colModel: welcome_chart_kontribusi_produk_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		region: 'center',
        margins: '0 5 5 5',
		frame: true
	});

	var welcome_chart_tab_rekap_jual = new Ext.TabPanel({
		activeTab: 0,
		width: 350,
		items: [welcome_chart_kontribusi_medisListEditorGrid, welcome_chart_kontribusi_non_medisListEditorGrid, welcome_chart_kontribusi_produkListEditorGrid]
	});

	welcome_chart_kontribusiForm = new Ext.FormPanel({
		title: 'Kontribusi & Produktifitas',
		id: 'welcome_chart_kontribusiForm',
		el: 'elwindow_welcome_chart_kontribusi',
        width: 720,
		height: 250,
		layout: 'fit',
		frame: true,
		autoDestroy: true,
		items: [welcome_chart_tab_rekap_jual],
		tbar: [
			{
				text: 'Hitung Ulang',
				tooltip: 'Hitung Ulang',
				iconCls:'icon-refresh',
				handler: function(){
					cek_periode('welcome_chart_kontribusi_update');
				}
			},'-',	
			{
				text: 'Detail',
				tooltip: 'Detail',
				iconCls:'icon-folder_go',
				menu: {
					xtype: 'menu',
					plain: true,
					items: [
						/*{
							text: 'Laporan Jumlah Tindakan Medis Semua Referal',
							handler: goToLapJumTindakanMedis
						},		
						{
							text: 'Laporan Jumlah Tindakan Estetik Semua Referal',
							handler: goToLapJumTindakanEstetik
						},	*/	
						{
							text: 'Laporan Jumlah Penjualan Produk Semua Referal',
							handler: goToLapJumPenjualanProduk
						}/*,		
						{
							text: 'Laporan Jumlah Customer per Dokter',
							handler: goToLapJumCustomerDokter
						}*/
					]
				}
			}
		]
	});
	//welcome_chart_kontribusiForm.render();

	welcome_chart_stok_minimum_DataStore = new Ext.data.Store({
		id: 'welcome_chart_stok_minimum_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action', 
			method: 'POST',
			timeout: 1200000
		}),
		baseParams: {
			task: "STOK_MINIMUM", 
			group1_id: '',
			group2_id: '',
			produk_id: '',
			query: '',
			tahun: '<?php echo date("Y") ?>',
			bulan: '<?php echo date("m") ?>',
			gudang: 2, 
			jenis: 1, 
			mutasi_jumlah: 'Semua', 
			opsi_produk: 'all', 
			opsi_satuan: 'default', 
			periode: 'tanggal', 
			stok_akhir: 'Semua', 
			stok_awal: 'Semua', 
			stok_keluar: '> 0', 
			stok_masuk: '> 0', 
			start:0, 
			limit: 10
		}, 
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'produk_id'
		},[
			{name: 'tsm_produk_id', type: 'int', mapping: 'tsm_produk_id'},
			{name: 'tsm_produk_kode', type: 'string', mapping: 'tsm_produk_kode'},
			{name: 'tsm_produk_nama', type: 'string', mapping: 'tsm_produk_nama'},
			{name: 'tsm_satuan_nama', type: 'string', mapping: 'tsm_satuan_nama'},
			{name: 'tsm_stok_akhir', type: 'float', mapping: 'tsm_stok_akhir'},
			{name: 'tsm_stok_min', type: 'float', mapping: 'tsm_stok_min'},
			{name: 'tsm_stok_kekurangan', type: 'float', mapping: 'tsm_stok_kekurangan'},
			{name: 'tsm_date_update', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: 'tsm_date_update'}
		])
	});
	
	welcome_chart_stok_minimum_ColumnModel = new Ext.grid.ColumnModel(
		[{	
			align : 'Left',
			header: '<div align="center">' + 'Produk' + '</div>',
			dataIndex: 'tsm_produk_nama',
			readOnly: true,
			width: 400,	//55,
			sortable: true
		},{	
			align : 'Left',
			header: '<div align="center">' + 'Satuan' + '</div>',
			dataIndex: 'tsm_satuan_nama',
			readOnly: true,
			width: 60,	//55,
			sortable: true
		},{	
			align : 'Right',
			header: '<div align="center">' + 'Stok Akhir' + '</div>',
			dataIndex: 'tsm_stok_akhir',
			renderer: Ext.util.Format.numberRenderer('0,000'),
			readOnly: true,
			width: 80,
			sortable: true
		},{	
			align : 'Right',
			header: '<div align="center">' + 'Stok Min' + '</div>',
			dataIndex: 'tsm_stok_min',
			renderer: Ext.util.Format.numberRenderer('0,000'),
			readOnly: true,
			width: 80,	//55,
			sortable: true
		},{	
			align : 'Right',
			header: '<div align="center">' + 'Kekurangan' + '</div>',
			dataIndex: 'tsm_stok_kekurangan',
			renderer: Ext.util.Format.numberRenderer('0,000.00'),
			readOnly: true,
			width: 80,	//55,
			sortable: true,
			renderer: function(value, cell){
				if(value >= 0)
					value = '-';
				else
					value = Ext.util.Format.number((value * -1),'0,000'); 

				return value;
			}
		}
	]);
	
	welcome_chart_stok_minimum_ColumnModel.defaultSortable= true;

	welcome_chart_stok_minimumListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_stok_minimumListEditorGrid',
		el: 'fp_welcome_chart_stok_minimum',
		title: 'Stok Minimum Produk',
		store: welcome_chart_stok_minimum_DataStore, // DataStore
		cm: welcome_chart_stok_minimum_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		frame: true,
		//clicksToEdit:2, // 2xClick untuk bisa meng-Edit inLine Data
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		viewConfig: { forceFit:true },
	  	width: 565, 
	  	height: 200,
		/* Add Control on ToolBar */
		tbar: [
			/*{
				text: 'Hitung Ulang',
				tooltip: 'Hitung Ulang',
				iconCls:'icon-refresh',
				handler: function(){
					cek_periode('welcome_chart_stok_minimum_update');
				}
			},'-',	*/
			{
				text: 'Detail',
				tooltip: 'Detail',
				iconCls:'icon-folder_go',
				menu: {
					xtype: 'menu',
					plain: true,
					items: [
						{
							text: 'Laporan Stok Rekap Barang',
							handler: goToLapStok
						}
					]
				}
			}
		]
	});
	welcome_chart_stok_minimumListEditorGrid.render();			

	welcome_chart_jatuh_tempo_DataStore = new Ext.data.Store({
		id: 'welcome_chart_jatuh_tempo_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action', 
			method: 'POST',
			timeout: 1200000
		}),
		baseParams: {
			task: "JATUH_TEMPO", 
			group1_id: '',
			group2_id: '',
			produk_id: '',
			query: '',
			tahun: '<?php echo date("Y") ?>',
			bulan: '<?php echo date("m") ?>',
			gudang: 2, 
			jenis: 1, 
			mutasi_jumlah: 'Semua', 
			opsi_produk: 'all', 
			opsi_satuan: 'default', 
			periode: 'tanggal', 
			stok_akhir: 'Semua', 
			stok_awal: 'Semua', 
			stok_keluar: '> 0', 
			stok_masuk: '> 0', 
			start:0, 
			limit: 10
		}, 
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'invoice_id'
		},[
			{name: 'invoice_id', type: 'int', mapping: 'invoice_id'}, 
			{name: 'invoice_no', type: 'string', mapping: 'invoice_no'}, 
			{name: 'invoice_no_auto', type: 'string', mapping: 'invoice_no_auto'}, 
			{name: 'invoice_supplier', type: 'string', mapping: 'supplier_nama'}, 
			{name: 'invoice_supplier_id', type: 'int', mapping: 'supplier_id'}, 
			{name: 'invoice_noterima', type: 'string', mapping: 'terima_no'}, 
			{name: 'invoice_tanggal', type: 'date', dateFormat: 'Y-m-d', mapping: 'invoice_tanggal'}, 
			{name: 'invoice_diskon', type: 'int', mapping: 'invoice_diskon'},
			{name: 'invoice_cashback', type: 'float', mapping: 'invoice_cashback'},
			{name: 'invoice_biaya', type: 'float', mapping: 'invoice_biaya'},
			{name: 'invoice_uangmuka', type: 'float', mapping: 'invoice_uangmuka'},
			{name: 'invoice_jumlah', type: 'float', mapping: 'invoice_total_barang'},
			{name: 'invoice_total', type: 'float', mapping: 'invoice_total_tagihan'},
			{name: 'invoice_jatuhtempo', type: 'date', dateFormat: 'Y-m-d', mapping: 'invoice_jatuhtempo'}, 
			{name: 'invoice_jatuh_tempo_hari', type: 'int', mapping: 'invoice_jatuh_tempo_hari'}, 
			{name: 'invoice_penagih', type: 'string', mapping: 'invoice_penagih'},
			{name: 'invoice_keterangan', type: 'string', mapping: 'invoice_keterangan'},
			{name: 'invoice_bank', type: 'string', mapping: 'invoice_bank'},
			{name: 'invoice_status', type: 'string', mapping: 'invoice_status'},  			
			{name: 'invoice_post', type: 'string', mapping: 'invoice_post'}, 
			{name: 'invoice_creator', type: 'string', mapping: 'invoice_creator'}, 
			{name: 'invoice_date_create', type: 'date', dateFormat: 'Y-m-d', mapping: 'invoice_date_create'}, 
			{name: 'invoice_update', type: 'string', mapping: 'invoice_update'}, 
			{name: 'invoice_date_update', type: 'date', dateFormat: 'Y-m-d', mapping: 'invoice_date_update'}, 
			{name: 'invoice_revised', type: 'int', mapping: 'invoice_revised'},
			{name: 'invoice_terbayar', type: 'float', mapping: 'invoice_terbayar'}
		]),
		sortInfo:{field: 'invoice_id', direction: "DESC"}
	});
	
	welcome_chart_jatuh_tempo_ColumnModel = new Ext.grid.ColumnModel(
		[{
			header: '#',
			readOnly: true,
			dataIndex: 'invoice_id',
			width: 40,
			renderer: function(value, cell){
				cell.css = "readonlycell"; // Mengambil Value dari Class di dalam CSS 
				return value;
				},
			hidden: true
		},
		{
			header: '<div align="center">Tanggal</div>',
			dataIndex: 'invoice_tanggal',
			width: 70,	//100,
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			readOnly: true
		},
		
		{
			header: '<div align="center">' + 'No PT' + '</div>',
			dataIndex: 'invoice_no_auto',
			width: 100,	//150,
			sortable: true,
			readOnly: true,
			renderer: function(value, cell, record){
				if(record.data.invoice_post=='Y'){
					return '<span style="color:red;"><b>' + value + '</b></span>';
				}
				return value;
			}
		}, 
		{
			header: '<div align="center">Supplier</div>',
			dataIndex: 'invoice_supplier',
			width: 200,	//250,
			sortable: true,
			readOnly: true
		}, 
		{
			header: '<div align="center">Sub Total (Rp)</div>',
			align: 'right',
			dataIndex: 'invoice_total',
			width: 100,
			sortable: true,
			hidden: true,
			readOnly: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		},
		{
			header: '<div align="center">Terbayar (Rp)</div>',
			align: 'right',
			dataIndex: 'invoice_terbayar',
			width: 100,
			sortable: true,
			hidden: true,
			readOnly: true,
			renderer: Ext.util.Format.numberRenderer('0,000')
		},		
		{
			header: '<div align="center">Total Nilai (Rp)</div>',
			align: 'right',
			width:80,
			sortable: true,
			readOnly: true,
			renderer: function(v, params, record){
					invoice_total_nilai=Ext.util.Format.number((record.data.invoice_total-(record.data.invoice_diskon*record.data.invoice_total/100)+record.data.invoice_biaya-record.data.invoice_cashback),"0,000.00");
                    return '<span>' + invoice_total_nilai+ '</span>';
            }
		},
		{
			header: 'Jatuh Tempo',
			dataIndex: 'invoice_jatuhtempo',
			width: 70,	//100,
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			readOnly: true
		}
	]);	
	welcome_chart_jatuh_tempo_ColumnModel.defaultSortable= true;

	welcome_chart_jatuh_tempoListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_jatuh_tempoListEditorGrid',
		el: 'fp_welcome_chart_jatuh_tempo',
		title: 'Jatuh Tempo Hutang',
		store: welcome_chart_jatuh_tempo_DataStore, // DataStore
		cm: welcome_chart_jatuh_tempo_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		frame: true,
		//clicksToEdit:2, // 2xClick untuk bisa meng-Edit inLine Data
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		viewConfig: { forceFit:true },
	  	width: 565, 
	  	height: 200
	});
	welcome_chart_jatuh_tempoListEditorGrid.render();		
	
	welcome_chart_ultah_DataStore = new Ext.data.Store({
		id: 'welcome_chart_ultah_DataStore',
		proxy: new Ext.data.HttpProxy({
			url: 'index.php?c=c_welcome_chart&m=get_action', 
			method: 'POST',
			timeout: 1200000
		}),
		baseParams: {
			task: "ULTAH", 
			tanggal: thisday,
			bulan: thismonth
		}, 
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'cust_id'
		},[
			{name: 'cust_id', type: 'int', mapping: 'cust_id'}, 
			{name: 'cust_nama', type: 'string', mapping: 'cust_nama'}, 
			{name: 'cust_tgllahir', type: 'date', dateFormat: 'Y-m-d', mapping: 'cust_tgllahir'}
		]),
		sortInfo:{field: 'cust_id', direction: "DESC"}
	});
	
	welcome_chart_ultah_ColumnModel = new Ext.grid.ColumnModel(
		[{
			header: '#',
			readOnly: true,
			dataIndex: 'cust_id',
			width: 40,
			renderer: function(value, cell){
				cell.css = "readonlycell"; // Mengambil Value dari Class di dalam CSS 
				return value;
				},
			hidden: true
		},
		{
			header: '<div align="center">' + 'Nama Member' + '</div>',
			dataIndex: 'cust_nama',
			width: 120,	//150,
			sortable: true,
			readOnly: true
		},
		{
			header: 'Tgl Lahir',
			dataIndex: 'cust_tgllahir',
			width: 70,	//100,
			sortable: true,
			renderer: Ext.util.Format.dateRenderer('d-m-Y'),
			readOnly: true
		}
	]);	
	welcome_chart_ultah_ColumnModel.defaultSortable= true;

	welcome_chart_ultahListEditorGrid =  new Ext.grid.EditorGridPanel({
		id: 'welcome_chart_ultahListEditorGrid',
		el: 'fp_welcome_chart_ultah',
		title: 'Member sedang Berulang Tahun',
		store: welcome_chart_ultah_DataStore, // DataStore
		cm: welcome_chart_ultah_ColumnModel, // Nama-nama Columns
		enableColLock:false,
		frame: true,
		//clicksToEdit:2, // 2xClick untuk bisa meng-Edit inLine Data
		selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
		viewConfig: { forceFit:true },
	  	width: 300, 
	  	height: 500
	});
	welcome_chart_ultahListEditorGrid.render();	
		
	welcome_chart_rekap_jual_orderField.setValue('rp');


	dashboard_search(false);
	
});
	</script>
<body>
<div>
	<div class="col">
 		<table cellpadding="10">
			<tr>
				<td colspan="2"><div id="elwindow_welcome_chart_search"></div></td>
				<td rowspan="4" valign="top"><div id="fp_welcome_chart_ultah"></div></td>
			</tr>
			<tr>
				<td valign="top" width="500">
					<table cellpadding="10">
						<tr>
							<td>
								<div id="fp_welcome_chart_jatuh_tempo"></div>
							</td>
						</tr>
						<tr>
							<td><div id="fp_welcome_chart_list"></div></td>				
						</tr>
						<tr>
							<td>
								<div id="fp_welcome_chart_stok_minimum"></div>
							</td>
						</tr>					
				    </table>	
				</td>
				<td valign="top">
					<table cellpadding="10">
						<tr>
							<td>
								<div id="elwindow_welcome_chart_rekap_jual"></div>
								<div id="fp_welcome_chart_rekap_jual_rawat"></div>
								<div id="fp_welcome_chart_rekap_jual_produk"></div>
								<div id="fp_welcome_chart_rekap_jual_paket"></div>
								<div id="elwindow_welcome_chart_netsales"></div>
							</td>
							<td>
								<div id="elwindow_welcome_chart_top_spender"></div>
								<div id="fp_welcome_chart_top_spender_cash_in"></div>
								<div id="fp_welcome_chart_top_spender_net_sales"></div>
								<div id="elwindow_welcome_chart_kunjungan"></div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div id="elwindow_welcome_chart_kontribusi"></div>
								<div id="fp_welcome_chart_kontribusi_medis"></div>
								<div id="fp_welcome_chart_kontribusi_non_medis"></div>
								<div id="fp_welcome_chart_kontribusi_produk"></div>
							</td>
						</tr>
						
						<tr>				
							<td><div id="elwindow_welcome_chart_netsales_medis"></div></td>
							<td><div id="elwindow_welcome_chart_kunjungan_lama"></div></td>
						</tr>
						<tr>
							<td><div id="elwindow_welcome_chart_netsales_estetik"></div></td>
							<td><div id="elwindow_welcome_chart_kunjungan_baru"></div></td>
						</tr>
						<tr>
							<td><div id="elwindow_welcome_chart_netsales_produk"></div></td>
							<td><div id="elwindow_welcome_chart_spending_avg"></div></td>			
						</tr>
				    </table>
				</td>				
				<td valign="top" width="500">
					<table cellpadding="10">
						<tr>
							<td><div id="fp_welcome_chart_list"></div></td>				
						</tr>
				    </table>	
				</td>
			</tr>
		</table>
    </div>
</div>
</body>