<?php include('v_include.php');
	setcookie('user',$_SESSION[SESSION_USERID]);
?>
<!DOCTYPE html>
<html>
<head>
	<title>
		<?php 
			if(!empty($_SESSION['cabang_nama']))
				echo $_SESSION['cabang_nama'];
			else
				echo $title; 
		?>
	</title>
	<?=css_asset('ext-all.css');?>
	<?=css_asset('custom.css');?>
	<?=css_asset('docs.css','main');?>
	<? //=css_asset('forms.css');?>
	<?=css_asset('mmcal.css');?>
   	<?=css_asset('xtheme-black.css');?>
	<?//=css_asset('file-upload.css');?>
	<?=css_asset('MultiSelect.css');?>
	<?=css_asset('GroupSummary.css');?>
	<?=css_asset('ColumnNodeUI.css');?>
	<?=css_asset('examples.css');?>
	<?=css_asset('LockingGridView.css');?>
	
	<?=css_asset('Ext.ux.form.FileUploadField.css');?>
	<?=css_asset('AwesomeUploader.css');?>
	<?=css_asset('AwesomeUploaderProgressBar.css');?>
      
	<?=js_asset('ext-base.js');?>
	<?=js_asset('ext-all.js');?>
	<?=js_asset('App.js');?>
	<?=js_asset('TabCloseMenu.js');?>
	<?//=js_asset('FileUploadField.js');?>
	<?=js_asset('SearchField.js');?>
	<?=js_asset('RowExpander.js');?>
	<?=js_asset('RowEditor.js');?>
	<?=js_asset('CheckColumn.js');?>
	<?=js_asset('ColumnNodeUI.js');?>
	<?=js_asset('MultiSelect.js');?>
	<?=js_asset('ItemSelector.js');?>
	<?=js_asset('InputTextMask.js');?>
	<?=js_asset('currency.js');?>
	<?=js_asset('GroupSummary.js');?>
	<?=js_asset('examples.js');?>
	<?=js_asset('jquery-1.6.4.min.js');?>
	<?=js_asset('LockingGridView.js');?>
	<?=js_asset('collapsedHorizontal.js');?>
	<?=js_asset('overrides.js');?>
	
	<?=js_asset('Ext.ux.form.FileUploadField.js');?>
	<?=js_asset('Ext.ux.XHRUpload.js');?>
	<?=js_asset('swfupload.js');?>
	<?=js_asset('Ext.ux.AwesomeUploaderLocalization.js');?>
	<?=js_asset('Ext.ux.AwesomeUploader.js');?>
	
	
	
<script type="text/javascript">
	var s = document.createElement("script");
	s.type = 'text/javascript';
	s.src = "./assets/js/locale/ext-lang-id.js";
	s.charset = "ascii";
	document.getElementsByTagName("head")[0].appendChild(s);
	
</script>
    <?=js_asset('PagingMemoryProxy.js');?>
  	<link rel="shortcut icon" href="<?=base_url();?>favicon.ico" />
	<link rel="apple-touch-icon" href="<?=base_url();?>favicon.png" />
	
	<style type="text/css">
	html, body {
        font:normal 11px tahoma, arial, helvetica, sans-serif;
        margin:0;
        padding:0;
        border:0 none;
        overflow:hidden;
        height:100%;
    }
	p {
	    margin:5px;
	}
    .style1 {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: 14px;
	}
    .style2 {
		font-size: large;
		font-family: "Courier New", Courier, monospace;
	}
	.style5 {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		color: #3BB9FF;
	}
	<?php
	if(count($menus)){
		foreach($menus as $allmenu){
			$icon_file=file_exists(image_asset_url("icons/".$allmenu["menu_iconmenu"])) && $allmenu["menu_iconmenu"]!=="" ? image_asset_url("icons/".$allmenu["menu_iconmenu"]):image_asset_url("icons/grid.png");
	echo "\t .icon-".$allmenu["menu_id"]."{ background-image:url(".$icon_file.") !important; } \n";
		}
	}
	
	if(count($submenus)){
		foreach($submenus as $allmenu){
			$icon_file=file_exists(image_asset_url("icons/".$allmenu["menu_iconmenu"])) && $allmenu["menu_iconmenu"]!=="" ? image_asset_url("icons/".$allmenu["menu_iconmenu"]):image_asset_url("icons/grid.png");
	echo "\t .icon-".$allmenu["menu_id"]."{ background-image:url(".$icon_file.") !important; } \n";
		}
	}
	
	?>
    </style>
	
<script>
 
Ext.onReady(function(){
		autoRefresh();
	});

	function form_logout(){
	    var log=confirm("Apakah anda yakin akan keluar?");
	    if(log==true){
			Ext.MessageBox.show({
				msg:   'Proses logout, mohon tunggu...',
				progressText: 'proses...',
				width:300,
				wait:true
			});
			
			Ext.Ajax.request({
				url: 'index.php?c=c_login&m=logout',
				params: {
				username: '<?=$_SESSION[SESSION_USERID];?>'
				},
				success: function(){
					Ext.MessageBox.hide();
					window.location='index.php'
				}
			});
	    }
	}
</script> 	
   
	<?php $this->load->view('menus')?>
</head>
<body>
	<div id="loading-mask" style=""></div>
	<div id="loading">
		<div class="loading-indicator">
			<img src="<?=image_asset_url('extanim32.gif','main');?>" width="32" height="32" style="margin-right:8px;" align="absmiddle"/>
			Loading...
		</div>
	</div> 
  	<div id="north">
    	<div class="api-title style1 style2" style="float:left; margin-left:10px;">
			<p class="style5"><font style="font-weight:bold; font-size:11px"><? $info=$this->m_public_function->get_info(); if($info!==""){ echo $info->info_nama; }?></font>, <font style="font-size:11px"><?php if($info!==""){ echo $info->info_alamat;  }?>&nbsp;&nbsp;</font>
            </p>
		</div>
		<div style="float:right; margin-right:10px; margin-top:5px; color:#3BB9FF; font-size:11px"> 
        <font style="font-weight:bold; font-size:11px"><?=$_SESSION[SESSION_USERID]." [".$_SESSION[SESSION_GROUPNAMA]."]";?></font>&nbsp;
 		[<a  onclick="form_logout()" style="color:#3BB9FF; cursor: pointer; font-size:11px">Keluar</a>]
        </div>
  	</div>
  	<div id="props-panel" style="width:200px;height:200px;overflow:hidden;"></div>
    <div id="main"></div>
    <script>
    
		if(typeof(EventSource)!=="undefined")
		{
			var ip_addr='<?=str_replace('.', '_', $_SERVER['REMOTE_ADDR']);?>';
			var source=new EventSource("<?=base_url();?>push/userlog_"+ip_addr+".php");
			source.onmessage=function(event)
			{
				if(event.data==''){
					window.location='index.php';
				}
			};
		}
	
	var auto_refresh = "";
	var val_timeOut = 3599500;
	var val_interval = 300000;
	function autoRefresh(){
		var auto_refresh = setInterval(function(){
			clearInterval(auto_refresh);
		}, val_interval);	
	}
	
    </script>
</body>
</html>