<?php setcookie('user',''); ?>
<!DOCTYPE html>


<html>
<head>
<title>
	<?php 
			echo 'localhost';
	?>
</title>
<link rel="shortcut icon" href="<?=base_url();?>favicon.ico" />
<link rel="apple-touch-icon" href="<?=base_url();?>favicon.png" />
<style>
.withbg {
	background-image:url('./uploads/background.jpg');
	background-size:100%;
	background-repeat:no-repeat;
	width: 100%;
	height: 100%;
	position: absolute;
}

</style>
<?=css_asset('ext-all.css');?>
<?=css_asset('xtheme-black.css');?>
<?=css_asset('docs.css','main');?>
<?=js_asset('ext-base.js');?>
<?=js_asset('ext-all.js');?>
<script type="text/javascript">
	var s = document.createElement("script");
	s.type = 'text/javascript';
	s.src = "<?=base_url();?>/assets/js/locale/ext-lang-id.js";
	s.charset = "ascii";
	document.getElementsByTagName("head")[0].appendChild(s);
</script>
<?=js_asset('PagingMemoryProxy.js');?>
<?=js_asset('login.js','login');?>
</head>
<body>
    <div class="withbg"></div>
    <script>
    
	if(typeof(EventSource)!=="undefined")
	{
		var ip_addr='<?=str_replace('.', '_', $_SERVER['REMOTE_ADDR']);?>';
		var source=new EventSource("<?=base_url();?>push/userlog_"+ip_addr+".php");
		source.onmessage=function(event)
		{
			if(event.data!=''){
				window.location='index.php';
			}
		};
	}

    </script>
</body>
</html>